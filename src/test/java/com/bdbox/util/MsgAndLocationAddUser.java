package com.bdbox.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.print.attribute.HashAttributeSet;


/**
 * 为盒子消息与盒子位置添加对应的用户
 * @author jlj
 *
 */
public class MsgAndLocationAddUser {
	
	public static void main(String[] args) {
		//获取连接
		Connection conn = getConnection();
		try{
			Map<Long, Long> map = new HashMap<Long, Long>();
			
			//查询所有盒子
			Statement stm = conn.createStatement();
			stm.executeQuery("select id,boxSerialNumber,cardNumber,userId,entUserId from b_box ");
			ResultSet rs = stm.getResultSet();
			while(rs.next()){
				System.out.print("ID:"+rs.getLong("id"));
				System.out.print("\t盒子序列号:"+rs.getString("boxSerialNumber"));
				System.out.print("\t北斗卡号:"+rs.getString("cardNumber"));
				System.out.print("\t用户ID:"+rs.getLong("userId"));
				System.out.print("\t企业ID:"+rs.getLong("entUserId"));
				map.put(rs.getLong("id"), rs.getLong("userId"));
				System.out.println();
			}
			
			//修改盒子位置
			boxLocationAddUser(conn, map);
			//修改盒子消息
			boxMessageAddUser(conn, map);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 获取数据库的连接
	 * @return
	 */
	public static Connection getConnection(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://123.57.43.25:3306/bdbox?characterEncoding=UTF-8";
			Connection sourceConn = DriverManager.getConnection(url,"root", "vps123");
			return sourceConn;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	/**
	 * 北斗盒子后台系统，为盒子消息添加对应的用户字段值
	 */
	public static void boxMessageAddUser(Connection conn,Map<Long,Long> map){
		try {
			int len = map.size();
			int num = 1;
			
			Set<Long> set = map.keySet();
			for(Long key:set){
				if(key != null){
					if(map.get(key)!=null && Integer.valueOf(map.get(key).toString())>0){
						//修改盒子发送消息对应的发送用户字段
						String fromsql = "update  b_message_box t set t.fromUserId="+map.get(key)+"  where t.fromBoxId="+key;
						Statement fromstm = conn.createStatement();
						fromstm.execute(fromsql);
						//修改盒子接收消息对应的接收用户字段
						String toSql = "update  b_message_box t set t.toUserId="+map.get(key)+"  where t.toBoxId="+key;
						Statement ToStm = conn.createStatement();
						ToStm.execute(toSql);
					}
					
				}
				if(num%100==0){
					System.out.println("修改消息进度："+num+"/"+len);
				}
				if(len == num){
					System.out.println("修改消息进度："+num+"/"+len);
					System.out.println("修改消息完成！！！");
				}
				++num;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 北斗盒子后台系统，为盒子位置添加对应的用户字段值
	 */
	public static void boxLocationAddUser(Connection conn,Map<Long,Long> map){
		//修改盒子位置对应的用户字段
		try {
			int len = map.size();
			int num = 1;
			
			Set<Long> set = map.keySet();
			for(Long key:set){
				if(key != null){
					if(map.get(key)!=null && Integer.valueOf(map.get(key).toString())>0){
						String sql = "update  b_location_box t set userId="+map.get(key)+"  where t.boxId="+key;
						Statement stm = conn.createStatement();
						stm.execute(sql);
					}
					
				}
				if(num%100==0){
					System.out.println("修改位置进度："+num+"/"+len);
				}
				if(len == num){
					System.out.println("修改位置进度："+num+"/"+len);
					System.out.println("修改位置完成！！！");
				}
				++num;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

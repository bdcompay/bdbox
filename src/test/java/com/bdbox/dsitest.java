package com.bdbox;

import java.util.Calendar;

import com.bdbox.control.ControlBd;
import com.bdbox.entity.BoxMessage;
import com.qizhi.dsi.DsiClient;
import com.qizhi.dto.CardLocationDto;
import com.qizhi.dto.CardMessageDto;
import com.qizhi.dto.CardReceiptDto;
import com.qizhi.dto.SmsMessageDto;
import com.qizhi.listener.BdDataListener;

public class dsitest {
	private static Integer revcount = 0;
	private static Long start = (long) 0;

	public static void main(String[] args) {
		
		ControlBd controlBd= new ControlBd();
		CardMessageDto cardMessageDto=new CardMessageDto();
		cardMessageDto.setContent("06F1031505262058150000018704000000000000000000000000FFFFCED2D2D1BEADB0B2C8ABB5BDB4EFC4BFB5C4B5D8A1A3");
		cardMessageDto.setFromCardNumber("131258");
		cardMessageDto.setMsgId(0);
		try {
		  BoxMessage boxMessage=	controlBd.packCardMessageDtoToBoxMessage(cardMessageDto);
		  System.out.println(boxMessage.getContent());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public final static String castHexStringToDcmStringGalaxyTude(
			String strHexTude, int intEffect) {
		String strFloatTude = null;
		Long lngNum = Long.parseLong(strHexTude, 16);
		if (lngNum == 0) {
			return "0";
		}
		String strDcmTude = String.valueOf(lngNum);
		int intLen = strDcmTude.length();
		String strReal = strDcmTude.substring(intLen - intEffect);
		String strDecimals = strDcmTude.substring(0, intLen - intEffect);
		strFloatTude = strDecimals + "." + strReal;
		return strFloatTude;
	}
	public static void test() {
		start = Calendar.getInstance().getTimeInMillis();
		DsiClient dsiClient = new DsiClient();
		try {
			dsiClient.createConnect("M7JthO5J2", true,null,null, new BdDataListener() {
				public void processCardReceipt(CardReceiptDto cardReceipt) {
					// TODO Auto-generated method stub
					synchronized (revcount) {
						revcount++;
					}
					long end = Calendar.getInstance().getTimeInMillis();
					System.out.println("收到回执数据，ID：" + cardReceipt.getId()
							+ " 发送者：" + cardReceipt.getFromCardNumber()
							+ " 接收者：" + cardReceipt.getToCardNumber()
							+ "，总收到数据量：" + revcount + " 平均速度：" + (end - start)
							/ revcount + "ms/条");
				}

				public void processCardMessage(CardMessageDto cardMessage) {
					// TODO Auto-generated method stub

					synchronized (revcount) {
						revcount++;
					}
					long end = Calendar.getInstance().getTimeInMillis();
					System.out.println("收到短报文数据，ID：" + cardMessage.getId()
							+ "发送者：" + cardMessage.getFromCardNumber()
							+ " 接收者：" + cardMessage.getToCardNumber()
							+ "总收到数据量：" + revcount + " 平均速度：" + (end - start)
							/ revcount + "ms/条");

				}

				public void processCardLocation(CardLocationDto cardLocation) {
					synchronized (revcount) {
						revcount++;
					}
					long end = Calendar.getInstance().getTimeInMillis();
					System.out.println("收到定位数据，ID：" + cardLocation.getId()
							+ "发送者：" + cardLocation.getFromCardNumber()
							+ "总收到数据量：" + revcount + " 平均速度：" + (end - start)
							/ revcount + "ms/条");
				}

				public void processSmsMessage(SmsMessageDto smsMessageDto) {
					// TODO Auto-generated method stub
					long end = Calendar.getInstance().getTimeInMillis();
					System.out.println("收到手机短信，ID：" + smsMessageDto.getId()
							+ "发送者：" + smsMessageDto.getFromSmsNumber()
							+ "总收到数据量：" + revcount + " 平均速度：" + (end - start)
							/ revcount + "ms/条");
				}

			});
			// dsiClient.sendCardMessage("131001", "3132333435");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// dsiClient.sendCardMessage("306329", "3132333435");
		dsiClient.sendSmsMessage("15521260255", "测试");
		Thread thread = new Thread() {
			public void run() {
				while (true) {
					try {
						sleep(10);
						// dsiClient.sendSmsMessage("15521260255", "你好啊");
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		};
		thread.start();
	}
}

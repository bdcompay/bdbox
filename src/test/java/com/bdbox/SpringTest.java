package com.bdbox;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.beans.factory.annotation.Autowired;

import com.bdbox.dao.BoxDao;
import com.bdbox.dao.BoxMessageDao;
import com.bdbox.dao.UserDao;
import com.bdbox.service.BoxMessageService;

public class SpringTest extends AbstractTestSpring {

	@Autowired
	private BoxMessageService boxMessageService;

	@Autowired
	private BoxMessageDao boxMessageDao;

	@Autowired
	private UserDao userDao;

	@Autowired
	private BoxDao boxDao;

	// /**
	// * 批量导入盒子
	// */
	// @Test
	// public void test02(){
	// PostMethod postMethod = new
	// PostMethod("http://192.168.31.201:8080/bdbox/getBoxCardNumber.do");
	// NameValuePair[] nameValuePairs = new NameValuePair[2];
	// nameValuePairs[0] = new NameValuePair("cardNumber","123456");
	// nameValuePairs[1] = new
	// NameValuePair("systemCode","ids2j3jdunjejh2i32jdjalalghywu020mvgwe872e3iu");
	// postMethod.setRequestBody(nameValuePairs);
	// try {
	// new HttpClient().executeMethod(postMethod);
	// System.out.println("请求结果："+postMethod.getResponseBodyAsString());
	// } catch (HttpException e) {
	// e.printStackTrace();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	//
	// }

	// @Test
	public void test05() {

		// List<BoxMessage> boxMessages =
		// boxMessageService.getUnReadBoxMessage((long) 4,null);
		// System.out.println(boxMessages.size());

		// 用户1
		// User user = userDao.get((long) 9);
		// Box box = boxDao.get((long) 4);
		//
		// //用户2
		// User user2 = userDao.get((long) 10);
		// Box box2 = boxDao.get((long) 3);
		//
		// BoxMessage boxMessage = new BoxMessage();
		// boxMessage.setAltitude(122.1);
		// boxMessage.setContent("测试内容");
		// boxMessage.setCreatedTime(Calendar.getInstance());
		// boxMessage.setDataStatusType(DataStatusType.SENT);
		// boxMessage.setDirection(12.1);
		// boxMessage.setFromBox(box2);//发送盒子
		// boxMessage.setFromUser(user2);//发送id
		// boxMessage.setLatitude(12.1);
		// boxMessage.setLongitude(1.21);
		// boxMessage.setMsgId(1);
		// boxMessage.setMsgIoType(MsgIoType.IO_BOXGETPARTNERLOCATION);
		// boxMessage.setMsgType(MsgType.MSG_SAFE);
		// boxMessage.setSpeed(321.2);
		// boxMessage.setStatusDescription("描述");
		// boxMessage.setToBox(box);//接收盒子
		// boxMessage.setToUser(user);//接收用户
		// boxMessageDao.save(boxMessage);

		// for (int i = 0; i < 5; i++) {
		// BoxMessage boxMessage = new BoxMessage();
		// boxMessage.setAltitude(122.1);
		// boxMessage.setContent("测试内容"+i);
		// boxMessage.setCreatedTime(Calendar.getInstance());
		// boxMessage.setDataStatusType(DataStatusType.SENT);
		// boxMessage.setDirection(12.1);
		// boxMessage.setFromBox(box);//发送盒子
		// boxMessage.setFromUser(user);//发送id
		// boxMessage.setLatitude(12.1);
		// boxMessage.setLongitude(1.21);
		// boxMessage.setMsgId(i);
		// boxMessage.setMsgIoType(MsgIoType.IO_BOXGETPARTNERLOCATION);
		// boxMessage.setMsgType(MsgType.MSG_SAFE);
		// boxMessage.setSpeed(321.2);
		// boxMessage.setStatusDescription("描述"+i);
		// boxMessage.setToBox(box2);//接收盒子
		// boxMessage.setToUser(user2);//接收用户
		//
		// boxMessageDao.save(boxMessage);
		// }
		//
		// for (int i = 0; i < 5; i++) {
		// BoxMessage boxMessage = new BoxMessage();
		// boxMessage.setAltitude(122.1);
		// boxMessage.setContent("测试内容"+i);
		// boxMessage.setCreatedTime(Calendar.getInstance());
		// boxMessage.setDataStatusType(DataStatusType.SENT);
		// boxMessage.setDirection(12.1);
		// boxMessage.setFromBox(box2);//发送盒子
		// boxMessage.setFromUser(user2);//发送id
		// boxMessage.setLatitude(12.1);
		// boxMessage.setLongitude(1.21);
		// boxMessage.setMsgId(i);
		// boxMessage.setMsgIoType(MsgIoType.IO_BOXGETPARTNERLOCATION);
		// boxMessage.setMsgType(MsgType.MSG_SAFE);
		// boxMessage.setSpeed(321.2);
		// boxMessage.setStatusDescription("描述"+i);
		// boxMessage.setToBox(box);//接收盒子
		// boxMessage.setToUser(user);//接收用户
		// boxMessageDao.save(boxMessage);
		// }

	}

	/**
	 * 测试用户意见反馈
	 */
	public void test() {
		PostMethod postMethod = new PostMethod(
				"http://115.29.192.148/bdbox/postSuggestion");// 请求地址
		NameValuePair[] nameValuePairs = new NameValuePair[3];// 设置参数
		nameValuePairs[0] = new NameValuePair("systemCode",
				"ids2j3jdunjejh2i32jdjalalghywu020mvgwe872e3iu");// 系统调用码
		nameValuePairs[1] = new NameValuePair("suggestionContent", "没意见");// 意见反馈
		nameValuePairs[2] = new NameValuePair("mobkey", "123213");// 验证码
		postMethod.setRequestBody(nameValuePairs);// 设置请求体
		try {
			new HttpClient().executeMethod(postMethod);
			// 执行请求
			System.out.println(postMethod.getResponseBodyAsString());// 打印返回结果
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

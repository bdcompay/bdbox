package com.bdbox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class WebTestSupport extends AbstractTest {
	private static final String URL = "http://127.0.0.1:8080/bdbox";
	//private static final String URL = "http://121.199.61.41:9888/bdmob";
	private static HttpClient httpClient;

	@BeforeClass
	public static void init() {
		httpClient = new DefaultHttpClient();
	}

	protected String doPost(String name, JSONObject obj) {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPostReq = new HttpPost(URL + name);
			String jsonString=obj.toString();
			StringEntity s = new StringEntity(jsonString,"utf-8");
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent(),"utf-8"));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				return result.toString();
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	protected String doPost() {
		JSONObject obj = new JSONObject();
		obj.put("mob", "15013232860");
		obj.put("mobKeyType", "MOBKEY_REGISTER");
		String url="http://127.0.0.1:8989/bdmob/getKey";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPostReq = new HttpPost(url);
			String jsonString=obj.toString();
			StringEntity s = new StringEntity(jsonString,"utf-8");
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				return result.toString();
			} else {
				return null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	protected String doPost(String name, String string)
			throws UnsupportedEncodingException {
		HttpPost post = new HttpPost(URL + name);

		// HttpEntity stringEntity = new StringEntity(string,
		// ContentType.APPLICATION_JSON);

		StringEntity s = new StringEntity(string);
		s.setContentEncoding("UTF-8");
		s.setContentType("application/json");
		post.setEntity(s);
		HttpResponse response;
		String resp = null;
		try {
			response = httpClient.execute(post);

			HttpEntity responseEntity = response.getEntity();
			resp = EntityUtils.toString(responseEntity);
		} catch (ClientProtocolException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.debug(e);
		}

		return resp;
	}

	protected String doGet(String string) {
		HttpGet get = new HttpGet(URL + string);
		String resp = null;
		HttpResponse response = null;
		try {
			response = httpClient.execute(get);
			HttpEntity responseEntity = response.getEntity();
			resp = EntityUtils.toString(responseEntity);
		} catch (ClientProtocolException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		return resp;
	}

	@AfterClass
	public static void close() {
	}
}

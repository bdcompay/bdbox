package com.bdbox;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import com.bdsdk.util.CommonMethod;

public class TestApi2 {

	public static void main(String[] args) {
		String strData = null;
		// TODO Auto-generated method stub
		// 01600001ADA5010018000000000D021A 00 000200B4 00020001
		// 7CAE4776B9A8804B48005808 专线定位
		// 01600001AED501000F000000000D021A 040002001D0002001D100600 专线回执
		// 01600001AF16010053000000000D021A03010100000200AA000200AB100C1000C0A44D3030355330524559325A324534392E364634312E313B
		// 专线短报文(代码)
		// 01600001AF16020053000000000D021A 03010100000200AA000200AB100C1000C0
		// A44D3030355330524559325A324534392E364634312E313B
		// 030101000002001100020011100C1000B0
		// A4677364666773646667736273656467796466676466 两个数据段 专线短报文（代码）
		// testApi("01600001AF16010053000000000D021A03010100000200AA000200AB100C1000C0A44D3030355330524559325A324534392E364634312E313B");
		// testApi("01600001AED501000F000000000D021A040002001D0002001D100600");

		// strData =
		// "2444575858001C07FEE86000000000000000711A2307170A0209004B00337B";//
		// RD定位
		// strData =
		// "2454585858002507FEE86002000C00000088A4000406BE28270160D8AD005007D0543D0017";//GPS(广嘉协议)
		// strData =
		// "2454585858001B07FEE86001FCC100000038A4B4F3BCD2BAC300B2";//北斗短信(广嘉协议)
		// strData =
		// "2454585858002C07FEE86001FCC0000000C0A40001B4F3BCD2BAC30006BE28270160D8AD005007D0543D0030";//北斗短信+GPS定位(广嘉协议)
		// strData =
		// "2454585858002A07FEE860020005000000B0A40002C5F6D7B20006BE28270160D8AD005007D0543D0079";//报警+GPS定位(广嘉协议)
		// strData =
		// "2454585858002007FEE86002000A00000060A4FF0F0F532456F6C7D8B910005B";//数据透传(广嘉协议)
		// strData =
		// "2454585858002307FEE86002000600000078A40003CAD5B5BDB1A8BEAFD0C5CFA200C6";//短信回执(广嘉协议)
		// strData =
		// "2454585858002F07FEE860020008000000D8A423313530313332333238363023B1B1B6B7D6D5B6CB2CC4E3BAC30036";//终端到手机(广嘉协议)
		// strData =
		// "2454585858001707FEE8620200A800000020 C4E3BAC3 0098";//标准汉字普通短信
		// strData =
		// "2454585858001707FEE8600200A80000 0024 1234567890 00CC";//标准代码普通短信
		// testApi("01600B09341E04011C000000000E0B06030101000001FF4D0007FF1F0E221E01E801594300141106144500594D0C82F2040000040000488F00009D0300000000000000000000000000000000000000000000000000000000000000000000030101000001FF260001FF260E221F0274A40E0B060E22600C600C000000000000F06C361760220D252401FF264341544F4E444556303030303030303030303030FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0030101000004ABC800020F400E221F0270F02445303433354331303030374232303134313130363134333432333037353030303030334532313334343433313935303030463145414331454144314541433145414430303030303030300D0A0400027499000275410E221F");
		// testApi(strData);
		// "2454585858 001B 07FEE8 60 01FCC1 0000 0038 A4B4F3BCD2BAC300 B2";//北斗短信(广嘉协议)
		// 海聊登录(15013232860 123456)
		strData = "2454585858002107FEE8600200BA000000680105037EDBC0DC3132333435360000";
		
		//北斗到手机的短信（海聊）
		//strData = "2454585858003004AC89600200BA000000E0160106150204141128037EDBC0DCCDBCC0EBB2BBBFAAC1E1C1E1BDE3008C";
		//pullBdData(strData);
		 //popBdData();
	}

	public static void pullBdData(String dataStr) {
		String uri = "http://121.199.61.41/bdmob/pullBdData.do";
		//String uri = "http://localhost:8989/bdmob/pullBdData.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPostReq = new HttpPost(uri);
			JSONObject obj = new JSONObject();
			obj.put("bdChannel", "DEVICE_40");
			// obj.put("bdChannel", "DEVICE_LINE");
			obj.put("dataStr", dataStr);
			String aaString = obj.toString();
			StringEntity s = new StringEntity(aaString);
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {

			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	// http://14.146.102.17:8989/bdmob/weixin
	public static void popBdData() {
		String uri = "http://127.0.0.1:8080/bdcbus/popData.do";
		// String uri = "http://127.0.0.1:7080/bdmob/popData";
		// String uri = "http://14.146.48.100:8080/bdcbus/popBdData";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPostReq = new HttpPost(uri);
			JSONObject obj = new JSONObject();
			// obj.put("bdChannel", "DEVICE_40");
			obj.put("serverCardNumber", "524008");
			obj.put("channelGroupName", "bdmobgroup");
			String aaString = obj.toString();
			StringEntity s = new StringEntity(aaString);
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}
}

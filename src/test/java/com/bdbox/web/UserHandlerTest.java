package com.bdbox.web;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import com.bdbox.WebTestSupport;
import com.bdsdk.util.CommonMethod;

public class UserHandlerTest extends WebTestSupport {

	/**
	 * 获取验证码，包括注册，修改密码，忘记密码，更改资料
	 */
	@Test
	public void getKey() {
		String uri = "http://127.0.0.1:8080/bdbox/getKey.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("username", "15013232860"));
			// MOBKEY_REGISTER，MOBKEY_RESETPWD，MOBKEY_FORGETPWD，MOBKEY_CHANGEINFO
			nvps.add(new BasicNameValuePair("mobKeyTypeStr", "MOBKEY_CHANGEINFO"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());
			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	/**
	 * 完成注册
	 */
	@Test
	public void registerFinish() {
		String uri = "http://127.0.0.1:8080/bdbox/registerFinish.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("username", "15013232860"));
			nvps.add(new BasicNameValuePair("password", "123456"));
			nvps.add(new BasicNameValuePair("mobkey", "169456"));
			nvps.add(new BasicNameValuePair("name", "旅者"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());
			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	/**
	 * 用户登录
	 */
	@Test
	public void userLogin() {
		String uri = "http://127.0.0.1:8080/bdbox/login.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("username", "15013232860"));
			nvps.add(new BasicNameValuePair("password", "123456"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());
			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	@Test
	public void changeUserInfo() {
		String uri = "http://127.0.0.1:8080/bdbox/changeUserInfo.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("username", "15013232860"));
			nvps.add(new BasicNameValuePair("password", "123456"));
			nvps.add(new BasicNameValuePair("name", "去萌化1"));
			nvps.add(new BasicNameValuePair("mobkey", "339183"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());
			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	@Test
	public void changePassword() {
		String uri = "http://127.0.0.1:8080/bdbox/changePassword.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("username", "15013232860"));
			nvps.add(new BasicNameValuePair("password", "123456789"));
			nvps.add(new BasicNameValuePair("mobkey", "371813"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());
			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	@Test
	public void getUserInfo() {
		String uri = "http://127.0.0.1:8080/bdbox/getUserInfo.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("username", "15013232860"));
			nvps.add(new BasicNameValuePair("systemCode",
					"ids2j3jdunjejh2i32jdjalalghywu020mvgwe872e3iu"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());
			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}
	
	/**
	 * 企业用户查询盒子位置
	 */
	@Test
	public void entUserQueryBoxLocaltion() {
		String uri = "http://127.0.0.1:8080/bdbox/entUserQueryLocation.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("ids", "110007,"));
			// MOBKEY_REGISTER，MOBKEY_RESETPWD，MOBKEY_FORGETPWD，MOBKEY_CHANGEINFO
			nvps.add(new BasicNameValuePair("userPowerKey", "xdI43w7w26"));
			nvps.add(new BasicNameValuePair("page", "1"));
			nvps.add(new BasicNameValuePair("pageSize", "10"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());
			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}
	
	/**
	 * 企业用户查询盒子消息
	 */
	@Test
	public void entUserQueryBoxMessage() {
		String uri = "http://127.0.0.1:8080/bdbox/entUserQueryReceiveMessage.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("ids", "110007,"));
			// MOBKEY_REGISTER，MOBKEY_RESETPWD，MOBKEY_FORGETPWD，MOBKEY_CHANGEINFO
			nvps.add(new BasicNameValuePair("userPowerKey", "xdI43w7w26"));
			nvps.add(new BasicNameValuePair("pageSize", "10"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());
			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}
	
	/**
	 * 用户查询盒子位置
	 */
	@Test
	public void userQueryBoxLocaltion() {
		String uri = "http://127.0.0.1:8080/bdbox/userQueryLocation.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
//			nvps.add(new BasicNameValuePair("ids", "110007,"));
			// MOBKEY_REGISTER，MOBKEY_RESETPWD，MOBKEY_FORGETPWD，MOBKEY_CHANGEINFO
			nvps.add(new BasicNameValuePair("username", "13760750107"));
			nvps.add(new BasicNameValuePair("password", "123456"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());
			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}
	
	/**
	 * 用户查询盒子消息
	 */
	@Test
	public void userQueryBoxMessage() {
		String uri = "http://127.0.0.1:8080/bdbox/userQueryReceiveMessage.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
//			nvps.add(new BasicNameValuePair("ids", "110007,"));
			// MOBKEY_REGISTER，MOBKEY_RESETPWD，MOBKEY_FORGETPWD，MOBKEY_CHANGEINFO
			nvps.add(new BasicNameValuePair("username", "13760750107"));
			nvps.add(new BasicNameValuePair("password", "123456"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());
			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

}

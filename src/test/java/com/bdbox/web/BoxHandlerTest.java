package com.bdbox.web;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import com.bdbox.WebTestSupport;
import com.bdsdk.util.CommonMethod;

public class BoxHandlerTest extends WebTestSupport {
	@Test
	public void sendCode() {
		String uri = "http://beidouhui.cn/User/sendCode.html";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("type", "verify"));
			nvps.add(new BasicNameValuePair("mobile", "15521260255"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	/**
	 * 北斗盒子出货助手入库北斗盒子信息
	 */
	@Test
	public void putinBox() {
		String uri = "http://115.29.192.148/bdbox/putinBox.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("cardNumber", "131360"));
			nvps.add(new BasicNameValuePair("cardTypeStr", "LEVEL_3"));
			nvps.add(new BasicNameValuePair("freq", "20"));
			nvps.add(new BasicNameValuePair("snNumber", "QZ01100012"));
			nvps.add(new BasicNameValuePair("boxSerialNumber", "100107"));
			nvps.add(new BasicNameValuePair("systemCode",
					"ids2j3jdunjejh2i32jdjalalghywu020mvgwe872e3iu"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));

			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	/**
	 * 绑定北斗盒子
	 */
	@Test
	public void bindBox() {
		String uri = "http://127.0.0.1:8080/bdbox/bindBox.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("userId", "1"));
			nvps.add(new BasicNameValuePair("snNumber", "QZ01100012"));
			nvps.add(new BasicNameValuePair("boxSerialNumber", "100107"));
			nvps.add(new BasicNameValuePair("systemCode",
					"ids2j3jdunjejh2i32jdjalalghywu020mvgwe872e3iu"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));

			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	/**
	 * 解绑北斗盒子
	 */
	@Test
	public void unbindBox() {
		String uri = "http://127.0.0.1:8080/bdbox/unbindBox.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("userId", "1"));
			nvps.add(new BasicNameValuePair("snNumber", "QZ01100012"));
			nvps.add(new BasicNameValuePair("boxSerialNumber", "100107"));
			nvps.add(new BasicNameValuePair("systemCode",
					"ids2j3jdunjejh2i32jdjalalghywu020mvgwe872e3iu"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));

			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	/**
	 * 获取北斗盒子信息
	 */
	@Test
	public void getBox() {
		String uri = "http://11:8080/bdbox/getBox.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("boxSerialNumber", "100107"));
			nvps.add(new BasicNameValuePair("systemCode",
					"ids2j3jdunjejh2i32jdjalalghywu020mvgwe872e3iu"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));

			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	/**
	 * 更改北斗盒子信息
	 */
	@Test
	public void updateBox() {
		String uri = "http://127.0.0.1:8080/bdbox/updateBox.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("userId", "1"));
			nvps.add(new BasicNameValuePair("name", "酥清风"));
			nvps.add(new BasicNameValuePair("idNumber", "441424198903122618"));
			nvps.add(new BasicNameValuePair("snNumber", "QZ01100012"));
			nvps.add(new BasicNameValuePair("boxSerialNumber", "100107"));
			nvps.add(new BasicNameValuePair("systemCode",
					"ids2j3jdunjejh2i32jdjalalghywu020mvgwe872e3iu"));
			httpPostReq.setEntity(new UrlEncodedFormEntity(nvps, "utf8"));

			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}
}

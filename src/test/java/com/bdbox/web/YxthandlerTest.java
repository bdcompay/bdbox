package com.bdbox.web;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Test;

import com.bdbox.WebTestSupport;
import com.bdsdk.util.CommonMethod;

public class YxthandlerTest extends WebTestSupport {

	/**
	 * 用户获取家人的参数信息
	 */
	@Test
	public void sosYunyinCallback() {
		String uri = "http://14.146.24.198:8081/bdbox/sosYunyinCallback.do";
//		String uri = "http://localhost:808/bdbox/getFamily.do?boxSerialNumber=100158&cardNumber=307175";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			
			JSONObject obj = new JSONObject();
			StringEntity s = new StringEntity(obj.toString());
			s.setContentEncoding("utf-8");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}
	
	
	
	public static void main(String[] args){
		new YxthandlerTest().sosYunyinCallback();
		
	}
}

package com.bdbox.web;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Test;

import com.bdbox.api.dto.DialrecordDto;
import com.bdsdk.util.CommonMethod;

public class SosDialrecordHandlerTest {
	/**
	 * 用户获取待拨记录
	 */
	@Test
	public void getDialrecords() {
//		String uri = "http://123.57.176.207/bdbox/getDialrecords.do";
		String uri = "http://127.0.0.1:8081/bdbox/getDialrecords.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			
			JSONObject obj = new JSONObject();
			StringEntity s = new StringEntity(obj.toString());
			s.setContentEncoding("utf-8");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());
				JSONObject obj1 = JSONObject.fromObject(result.toString());
				JSONArray obj2 = obj1.getJSONArray("result");
				for(int i=0;i<obj2.size();i++){
					JSONObject obj3=obj2.getJSONObject(i);
					System.out.println(obj3.toString());
					DialrecordDto dialrecordDto=new DialrecordDto();
				}
			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	/**
	 * 用户获取家人的参数信息
	 */
	@Test
	public void getFamily() {
//		String uri = "http://123.57.176.207/bdbox/getFamily.do?boxSerialNumber=100214&cardNumber=189424";
		String uri = "http://localhost:8081/bdbox/getFamilyll.do?boxSerialNumber=100158&cardNumber=307175";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			
			JSONObject obj = new JSONObject();
			StringEntity s = new StringEntity(obj.toString());
			s.setContentEncoding("utf-8");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	public static void main(String[] args){
		new SosDialrecordHandlerTest().getDialrecords();
//		new SosDialrecordHandlerTest().getFamily();
	}
}

package com.bdbox.web;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.junit.Test;

import com.alibaba.fastjson.JSON;
import com.bdbox.WebTestSupport;
import com.bdbox.api.dto.BoxLocationDto;
import com.bdbox.api.dto.FamilyDto;
import com.bdbox.api.dto.PartnerDto;
import com.bdbox.api.dto.PostApiParam;
import com.bdbox.constant.MsgType;
import com.bdbox.util.HttpPostRequester;
import com.bdsdk.util.BdCommonMethod;
import com.bdsdk.util.CommonMethod;
import com.google.gson.Gson;

public class ApiHandlerTest extends WebTestSupport {

	/**
	 * 用户更新家人的参数信息
	 */
	@Test
	public void updateFamily() {
		String uri = "http://localhost:8081/bdbox/updateFamily.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			FamilyDto familyDto1 = new FamilyDto();
			familyDto1.setId((long)2781);
			familyDto1.setName("联通1");
			familyDto1.setPhone("15521260255");			
			familyDto1.setMail("ayanwork@126.com");
			FamilyDto familyDto2 = new FamilyDto();
			familyDto2.setId((long)2782);
			familyDto2.setName("移动2");
			familyDto2.setPhone("15013232860");
			FamilyDto familyDto3 = new FamilyDto();
			familyDto3.setId((long)2783);
			familyDto3.setName("外星人");
			familyDto3.setPhone("13416266617");
			List list = new ArrayList(); // 把两个对象放进list中
			list.add(familyDto1);
			list.add(familyDto2);
//			list.add(familyDto3);

			JSONArray jsonArray = JSONArray.fromObject(list); // 把list放进json数组
			Map map = new HashMap();
			map.put("boxSerialNumber", "100158");
			map.put("cardNumber", "307175");
			map.put("familyDtos", list); // 把json数组做为一个值放进Map中
			Gson gson = new Gson();

			String aaString = gson.toJson(map);
			;
			System.out.println(aaString);
			StringEntity s = new StringEntity(aaString, "utf-8");
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	/**
	 * 用户获取家人的参数信息
	 */
	@Test
	public void getFamily() {
		String uri = "https://123.57.176.207:6493/bdbox/getFamily.do?boxSerialNumber=110004&cardNumber=131260";
//		String uri = "http://localhost:8081/bdbox/getFamily.do?boxSerialNumber=100158&cardNumber=307175";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			
			JSONObject obj = new JSONObject();
			StringEntity s = new StringEntity(obj.toString());
			s.setContentEncoding("utf-8");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	/**
	 * 删除家人的参数信息
	 */
	@Test
	public void deleteFamily() {
		String uri = "http://115.29.192.148/bdbox/deleteFamily.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			Map map = new HashMap();
			map.put("id", "100122");
			Gson gson = new Gson();
			String aaString = gson.toJson(map);
			;
			StringEntity s = new StringEntity(aaString, "utf-8");
			s.setContentEncoding("UTF-8");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	/**
	 * 用户更新队友的参数信息
	 */
	@Test
	public void updatePartner() {
		String uri = "http://127.0.0.1:8080/bdbox/updatePartner.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);

			PartnerDto partnerDto = new PartnerDto();
			partnerDto.setName("爸爸");
			partnerDto.setBoxSerialNumber("100101");
			PartnerDto partnerDto1 = new PartnerDto();
			partnerDto1.setName("爸爸5");
			partnerDto1.setBoxSerialNumber("100102");
			PartnerDto partnerDto2 = new PartnerDto();
			partnerDto2.setName("爸爸2");
			partnerDto2.setBoxSerialNumber("100103");
			List list = new ArrayList(); // 把两个对象放进list中
			list.add(partnerDto);
			list.add(partnerDto1);
			list.add(partnerDto2);
			JSONArray jsonArray = JSONArray.fromObject(list); // 把list放进json数组
			Map map = new HashMap();
			map.put("boxSerialNumber", "100100");
			map.put("cardNumber", "131258");
			map.put("partnerDtos", jsonArray); // 把json数组做为一个值放进Map中
			JSONObject jsonObject = JSONObject.fromObject(map); // 把map放进json对象中

			String aaString = jsonObject.toString();
			StringEntity s = new StringEntity(aaString, "utf-8");
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	/**
	 * 用户获取队友参数信息
	 */
	@Test
	public void getPartner() {
		String uri = "http://127.0.0.1:8080/bdbox/getPartner.do?boxSerialNumber=100100&cardNumber=131258";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			JSONObject obj = new JSONObject();
			StringEntity s = new StringEntity(obj.toString());
			s.setContentEncoding("UTF-8");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}

	@Test
	public void checkbox() {

	}

	
	@Test
	public void testCheckAppUpdate() {
//		String uri = "http://115.29.192.148/bdbox/checkAppUpdate";
		String uri = "http://localhost:8080/bdbox/checkAppUpdate";
		try {
			HttpClient httpClient = new DefaultHttpClient();

			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			JSONObject obj = new JSONObject();

			obj.put("appType", "BOX_ANDROID");
			System.out.println(obj.toString());
			StringEntity s = new StringEntity(obj.toString());
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}
	
	
	/**
	 * 用户获取蓝牙密码
	 */
	@Test
	public void getBluetoothCode() {
		String uri = "http://123.57.176.207/bdbox/getBluetoothCode.do?boxSerialNumber=100144&machineId=307175";
//		String uri = "http://localhost:808/bdbox/getBluetoothCode.do?boxSerialNumber=100158&machineId=307175";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			
			JSONObject obj = new JSONObject();
			StringEntity s = new StringEntity(obj.toString());
			s.setContentEncoding("utf-8");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}
	
	//加密算法
	public String encryption(){
		
		String ymima="123456";//原密码
		//随机产生6个字节位加密因子
		Date d=new Date();
		SimpleDateFormat sd=new SimpleDateFormat("yyMMddhhmmss");
		String jmiy=sd.format(d);//加密因子
		
		System.out.println("加密因子："+jmiy);
		byte[] key=ymima.getBytes();//原密码字节
		byte[] A=BdCommonMethod.castHexStringToByte(jmiy);//加密因子字节
		byte[] Xkey=new byte[6];
		//利用加密因子对key[]逐字节一一进行异或运算
		for(int i=0;i<6;i++){
			Xkey[i]=(byte)(key[i]^A[i]);
		}
		System.out.println(BdCommonMethod.castBytesToString(Xkey));
		//再将每个字节的高4位和低四位进行交换， 得到新密文Xkey[K6,K5,K4,K3,K2,K1].
		for(int i=0;i<6;i++){
			//高四位
			int h4=(Xkey[i]<<4)&0xF0;
			//低四位
			int l4=(Xkey[i]>>4)&0x0F;
			
			Xkey[i]=(byte)(l4+h4);
		}
		System.out.println("Xkey:"+BdCommonMethod.castBytesToString(Xkey));
		
		//对加密因子A[]进行取反, 得到新的加密因子数组XA[A6,A5,A4,A3,A2,A1]。
		byte[] XA=new byte[6];
		for(int i=0;i<6;i++){
			XA[i]=(byte)(~A[i]);
		}
		System.out.println("XA:"+BdCommonMethod.castBytesToString(XA));
		
		//将密码和加密因子按照如下规则排列,得到新的总体密文 SecretData[  K6,A1,  K5,A2,  K4,A3, K3,A4,  K2,A5  K1,A6 ]
		byte[] SecretData=new byte[12];
		for(int i=0;i<6;i++){
			SecretData[i*2]=(byte)(Xkey[i]);
			SecretData[i*2+1]=(byte)(XA[5-i]);
		}
		System.out.println("SecretData:"+BdCommonMethod.castBytesToString(SecretData));
		
		return BdCommonMethod.castBytesToString(SecretData);
		
		
	}
	
	
	public static byte[] sendBluetoothPassword(String password) {
        byte contentLen = 24;
        String strHexLen = BdCommonMethod.castDcmStringToHexString(String.valueOf(contentLen), 2);
        String contentHex = "01";//命令
        String strTemp = "24444C5050" + strHexLen + "000000" + contentHex + password;
        String strHex = BdCommonMethod.getCheckCode(strTemp);
        byte[] bdata = BdCommonMethod.castHexStringToByte(strHex);
        return bdata;
    }
	
	
	@Test
	public void test() {
		System.out.println(CommonMethod
				.Time6byteStringTo8byteString("141129180930"));
	}
	
	
	public static final String getCheckCode(String strProtocol)
    {
        String strRet = "";
        strProtocol.replace(" ", "");
        byte chrCheckCode = 0;
        for(int i = 0; i < strProtocol.length(); i += 2)
        {
            char chrTmp = '\0';
            chrTmp = strProtocol.charAt(i);
            if(chrTmp != ' ')
            {
                byte chTmp1 = (byte)(BdCommonMethod.castCharToHexByte(chrTmp) << 4);
                chrTmp = strProtocol.charAt(i + 1);
                byte chTmp2 = (byte)(chTmp1 + (BdCommonMethod.castCharToHexByte(chrTmp) & 15));
                if(i == 0)
                    chrCheckCode = chTmp2;
                else
                    chrCheckCode ^= chTmp2;
            }
            String strHexCheckCode = String.format("%x", new Object[] {
                    Byte.valueOf(chrCheckCode)
                });
                strHexCheckCode = strHexCheckCode.toUpperCase();
                if(strHexCheckCode.length() != 2)
                    if(strHexCheckCode.length() > 2)
                        strHexCheckCode = strHexCheckCode.substring(strHexCheckCode.length() - 2);
                    else
                    if(strHexCheckCode.length() < 2 && strHexCheckCode.length() > 0)
                        strHexCheckCode = (new StringBuilder("0")).append(strHexCheckCode).toString();
            
            System.out.println(strHexCheckCode+"      "+strProtocol.substring(i+2, i+4));
        }

        
//        strRet = (new StringBuilder(String.valueOf(strProtocol))).append(strHexCheckCode).toString();
        return strRet;
    }

	
	
	/**
	 * 测试上传缓存
	 */
	@Test
	public void saveBoxCacheLocations() {
		String uri = "http://123.57.176.207:89/bdbox/saveBoxCacheLocations.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);

			BoxLocationDto boxLocationDto = new BoxLocationDto();
			boxLocationDto.setAltitude(1.00);
			boxLocationDto.setBoxSerialNumber("100143");
			boxLocationDto.setLatitude("23.11");
			boxLocationDto.setLongitude("113.11");
			boxLocationDto.setLocationSource(MsgType.MSG_TASK);
			List list = new ArrayList(); // 把两个对象放进list中
			list.add(boxLocationDto);
			JSONArray jsonArray = JSONArray.fromObject(list); // 把list放进json数组
			Map map = new HashMap();
			map.put("boxSerialNumber", "100143");
			map.put("cardNumber", "131260");
			map.put("boxLocationDtos", jsonArray); // 把json数组做为一个值放进Map中
			JSONObject jsonObject = JSONObject.fromObject(map); // 把map放进json对象中

			String aaString = jsonObject.toString();
			StringEntity s = new StringEntity(aaString, "utf-8");
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}
	
	@Test
	public void sendBoxMessageTest(){
		String url="http://127.0.0.1:8080/bdbox/sendBoxMessage.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(url);

			Map map = new HashMap();
			map.put("toBoxSerialNumber", "110016");
			map.put("toCardNumber", "259780");
			map.put("content", "这是测试数据"); 
			map.put("userPowerKey", "RE70uLOq27");
			JSONObject jsonObject = JSONObject.fromObject(map); // 把map放进json对象中

			String aaString = jsonObject.toString();
			StringEntity s = new StringEntity(aaString, "utf-8");
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				System.out.println(result.toString());

			} else {
				System.out.println(resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			System.out.println(CommonMethod.getTrace(e));
		}
	}
	
	public static void main(String[] args){
		new ApiHandlerTest().getFamily();;
//		
////		byte[] a=new ApiHandlerTest().sendBluetoothPassword("C601DAD765C5A0998162ECA3");
////		System.out.println(BdCommonMethod.castBytesToString(a));
////		new ApiHandlerTest().encryption();
//		
//		String s="24444C5050001800000001027F00C899CC705520CD6BEE70";
//		String ss=getCheckCode(s);
//		System.out.println(ss);
		
	}
}


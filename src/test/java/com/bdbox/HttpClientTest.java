package com.bdbox;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.ParseException;
import java.util.List;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.bdbox.api.handler.UserHandler;
import com.bdbox.constant.UserStatusType;
import com.bdbox.control.ControlMail;
import com.bdbox.dao.BoxMessageDao;
import com.bdbox.dao.UserDao;
import com.bdbox.entity.BoxMessage;
import com.bdbox.entity.User;
import com.bdbox.service.BoxLocationService;
import com.bdbox.service.BoxService;
import com.bdbox.service.ApplyService;
import com.bdbox.service.UserService;
import com.bdbox.util.MailAuthenticator;
import com.bdbox.util.MailUtil;
import com.bdsdk.util.CommonMethod;



public class HttpClientTest extends AbstractTestSpring {

	/**
	 * 验证码
	 * @throws IOException
	 */
//	@Test
	public void test01() throws IOException {
		
		PostMethod postMethod = new PostMethod("http://192.168.31.201:8080/bdbox/getKey.do");
		NameValuePair[] nameValuePairs = new NameValuePair[2];
		nameValuePairs[0] = new NameValuePair("username","13726772942");
		nameValuePairs[1] = new NameValuePair("mobKeyTypeStr","MOBKEY_REGISTER");
		postMethod.setRequestBody(nameValuePairs);
		
		try {
			//执行请求
			new HttpClient().executeMethod(postMethod);
			//输出响应结果
			System.out.println(postMethod.getResponseBodyAsString());
		} catch (HttpException e) {
			System.err.println("Http连接异常");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IO异常");
			e.printStackTrace();
		} finally {
			//释放连接
			postMethod.releaseConnection();
		}
	}

//	@Test
	public void test02(){
        String url = "jdbc:mysql://localhost:3306/bdbox";  
        String driver = "com.mysql.jdbc.Driver";  
        try{  
            Class.forName(driver);  
        }catch(Exception e){  
            System.out.println("无法加载驱动");  
        }  
          
try {  
        Connection con = DriverManager.getConnection(url,"root","123456");  
        if(!con.isClosed()){
        	System.out.println("success");  
        }else{
        	System.out.println("失败");
        }
    } catch (Exception e) {  
        // TODO Auto-generated catch block  
        e.printStackTrace();  
    }  
	}
	
	
	@Value("${email.emailImap}")
	private String emailImap;
	@Value("${email.emailSmtp}")
	private String emailSmtp;
	@Value("${email.emailUsername}")
	private String emailUsername;
	@Value("${email.emailPassword}")
	private String emailPassword;
	@Autowired
	private ControlMail controlMail;
	//@Test
	public void test3(){
		
		
		String mobKey = CommonMethod.getMobKey(6);//获取验证码
		//标题
		String subject = "【北斗盒子】会员验证码确认";
		//内容
		String content = "<div style=\"width:700px;margin:0 auto;border-bottom:1px solid #ccc;margin-bottom:30px;margin-top:50px;\"></div><div style=\"width:680px;padding:0 10px;margin:0 auto;\"><div style=\"line-height:1.5;font-size:14px;margin-bottom:25px;color:#4d4d4d;\">"
				+ "<strong style=\"display:block;margin-bottom:15px;\">亲爱的会员：<span style=\"color:#f60;font-size: 16px;\"></span>您好！</strong><strong style=\"display:block;margin-bottom:15px;\">"
				+ "您正在北斗盒子官网操作，请在输入框中输入验证码："
				+ "<span style=\"color:#f60;font-size: 24px\"><span style=\"border-bottom-width: 1px; border-bottom-style: dashed; border-bottom-color: rgb(204, 204, 204); z-index: 1; position: static;\" t=\"7\" onclick=\"return false;\">"
				+ mobKey+"</span></span>，以完成操作。</strong></div><div style=\"margin-bottom:30px;\"><small style=\"display:block;margin-bottom:20px;font-size:12px;\"><p style=\"color:#747474;\">"
				+ "注意：此操作可能会修改您的密码、登录邮箱或绑定手机。如非本人操作，请及时登录并修改密码以保证帐户安全<br>（工作人员不会向你索取此验证码，请勿泄漏！)</p></small>"
				+ "</div></div><div style=\"width:700px;margin:0 auto;\"><div style=\"padding:10px 10px 0;border-top:1px solid #ccc;color:#747474;margin-bottom:20px;line-height:1.3em;font-size:12px;\">"
				+ "<p>此为系统邮件，请勿回复<br>请保管好您的邮箱，避免账号被他人盗用</p><p>北斗盒子版权所有</p></div></div>";
		//发送
		controlMail.sendMailErrorMessage("634675228@qq.com", subject, content);
	}
	
	
    /** 
     *  
     * @param emails 
     * @param subject 
     * @param text 
     * @throws MessagingException 
     * @throws MailException 
     * @throws UnsupportedEncodingException 
     */  
	@Test
    public void sentEmails() throws MailException, MessagingException, UnsupportedEncodingException {  
    	JavaMailSenderImpl senderImpl  =   new  JavaMailSenderImpl(); 
    	   // 设定mail server  
    	    senderImpl.setHost("smtp.163.com");//邮件传输协议
    	    senderImpl.setUsername("lesliecheung401@163.com" ) ;  // 发送者邮箱
    	    senderImpl.setPassword("leslie401" ) ;  //密码
    	     
    	     // 建立邮件消息  
    		MimeMessage mailMessage = senderImpl.createMimeMessage();
    		// 为防止乱码，添加编码集设置
    		MimeMessageHelper messageHelper = new MimeMessageHelper(mailMessage,"UTF-8");
    	    
    		String mobKey = CommonMethod.getMobKey(6);//获取验证码
    		//标题
    		String subject = "【北斗盒子】会员验证码确认";
    		//内容
    		String content = "<div style=\"width:700px;margin:0 auto;border-bottom:1px solid #ccc;margin-bottom:30px;margin-top:50px;\"></div><div style=\"width:680px;padding:0 10px;margin:0 auto;\"><div style=\"line-height:1.5;font-size:14px;margin-bottom:25px;color:#4d4d4d;\">"
    				+ "<strong style=\"display:block;margin-bottom:15px;\">亲爱的会员：<span style=\"color:#f60;font-size: 16px;\"></span>您好！</strong><strong style=\"display:block;margin-bottom:15px;\">"
    				+ "您正在北斗盒子官网操作，请在输入框中输入验证码："
    				+ "<span style=\"color:#f60;font-size: 24px\"><span style=\"border-bottom-width: 1px; border-bottom-style: dashed; border-bottom-color: rgb(204, 204, 204); z-index: 1; position: static;\" t=\"7\" onclick=\"return false;\">"
    				+ mobKey+"</span></span>，以完成操作。</strong></div><div style=\"margin-bottom:30px;\"><small style=\"display:block;margin-bottom:20px;font-size:12px;\"><p style=\"color:#747474;\">"
    				+ "注意：此操作可能会修改您的密码、登录邮箱或绑定手机。如非本人操作，请及时登录并修改密码以保证帐户安全<br>（工作人员不会向你索取此验证码，请勿泄漏！)</p></small>"
    				+ "</div></div><div style=\"width:700px;margin:0 auto;\"><div style=\"padding:10px 10px 0;border-top:1px solid #ccc;color:#747474;margin-bottom:20px;line-height:1.3em;font-size:12px;\">"
    				+ "<p>此为系统邮件，请勿回复<br>请保管好您的邮箱，避免账号被他人盗用</p><p>北斗盒子版权所有</p></div></div>";
    	    
			// 设置收件人，寄件人 用数组发送多个邮件
    	     // String[] array = new String[]    {"sun111@163.com","sun222@sohu.com"};    
    	     // mailMessage.setTo(array);  
    		messageHelper.setTo( "634675228@qq.com" ); 
    		//messageHelper.setFrom("lesliecheung401@163.com")//收件人
    		messageHelper.setFrom(new InternetAddress(MimeUtility.encodeText("北斗盒子")+"<lesliecheung401@163.com>")); //收件人
    		messageHelper.setSubject( subject ); 
    		messageHelper.setText(content,true); //true开启html
    	    
    	    
    	 Properties prop  =   new  Properties() ;
    	 prop.put( " mail.smtp.auth " ,  " true " ) ;  //  将这个参数设为true，让服务器进行认证,认证用户名和密码是否正确 
    	 prop.put( " mail.smtp.timeout " ,  " 25000 " ) ; //超时设置
    	 senderImpl.setJavaMailProperties(prop);  
    	 
    	// 添加验证
 		MailAuthenticator auth = new MailAuthenticator("lesliecheung401@163.com","leslie401");
 		Session session = Session.getDefaultInstance(prop, auth);
 		senderImpl.setSession(session);
    	 
    	     // 发送邮件  
    	    senderImpl.send(mailMessage); 
    	     
    	    System.out.println( " 邮件发送成功.. " ); 
    }  
    
    /************************/
    
    //@Test
    public void sentEmail(){  
        
            // 设定mail server  
                JavaMailSenderImpl senderImpl  =   new  JavaMailSenderImpl(); 
            senderImpl.setHost("smtp.163.com");//邮件传输协议
            senderImpl.setUsername("lesliecheung401@163.com" ) ;  // 发送者邮箱
            senderImpl.setPassword("leslie401" ) ;  //密码
             
             // 建立邮件消息  
                MimeMessage mailMessage = senderImpl.createMimeMessage();
                // 为防止乱码，添加编码集设置
                MimeMessageHelper messageHelper = new MimeMessageHelper(mailMessage,"UTF-8");
            
                try {//收件人
                        // 设置收件人，寄件人 用数组发送多个邮件
                        // String[] array = new String[]    {"sun111@163.com","sun222@sohu.com"};    
                // mailMessage.setTo(array);  
                                messageHelper.setTo( "634675228@qq.com" );
                        } catch (MessagingException e) {
                                e.printStackTrace();
                                System.err.println("收件人错误");
                        } 
                try {//收件人
                        //messageHelper.setFrom("lesliecheung401@163.com")
                                messageHelper.setFrom(new InternetAddress(MimeUtility.encodeText("北斗盒子")+"<lesliecheung401@163.com>"));
                        } catch (AddressException e) {
                                e.printStackTrace();
                                System.err.println("发件人消息编码错误");
                        } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                                System.err.println("发件人编码错误");
                        } catch (MessagingException e) {
                                e.printStackTrace();
                                System.err.println("发件人错误");
                        } 
                
                try {//标题
                                messageHelper.setSubject( "【北斗盒子】会员验证码确认" );
                        } catch (MessagingException e) {
                                e.printStackTrace();
                                System.err.println("标题错误");
                        } 
                
                try {//内容
                                messageHelper.setText("<h1>内容</h1>",true);//true开启html
                        } catch (MessagingException e) {
                                e.printStackTrace();
                                System.err.println("内容错误");
                        }
            
            
         Properties prop  =   new  Properties() ;
         prop.put( " mail.smtp.auth " ,  " true " ) ;  //  将这个参数设为true，让服务器进行认证,认证用户名和密码是否正确 
         prop.put( " mail.smtp.timeout " ,  " 25000 " ) ; //超时设置
         senderImpl.setJavaMailProperties(prop);  
         
        // 添加验证
                MailAuthenticator auth = new MailAuthenticator("lesliecheung401@163.com","leslie401");
                Session session = Session.getDefaultInstance(prop, auth);
                senderImpl.setSession(session);
         
         // 发送邮件  
         senderImpl.send(mailMessage); 
    }  
    
    //@Test
    public void test04(){
    	
    	MailUtil.sendMail("smtp.163.com", "lesliecheung401@163.com", "lesliecheung401@163.com", "leslie401", "634675228@qq.com", "65456465", "<h1>内容</h1>");
    }
    
    @Autowired
    private UserHandler userHandler;
    @Autowired
    private UserService userService;
    @Autowired
    private ControlMail contiolMail;
    @Autowired
    private UserDao userDao;
	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;
   // @Test
	public void test03(){
		User user = userDao.queryUser(null, null, null, UserStatusType.NORMAL, null, "634675228@qq.com");
		if(user!=null){
			String mobKey = CommonMethod.getMobKey(6);//获取验证码

			
			//标题
			String subject = "【北斗盒子】会员验证码确认";
			//内容
			String content = "<div style=\"width:700px;margin:0 auto;border-bottom:1px solid #ccc;margin-bottom:30px;margin-top:50px;\"></div><div style=\"width:680px;padding:0 10px;margin:0 auto;\"><div style=\"line-height:1.5;font-size:14px;margin-bottom:25px;color:#4d4d4d;\">"
					+ "<strong style=\"display:block;margin-bottom:15px;\">亲爱的会员：<span style=\"color:#f60;font-size: 16px;\"></span>您好！</strong><strong style=\"display:block;margin-bottom:15px;\">"
					+ "您正在北斗盒子官网操作，请在输入框中输入验证码："
					+ "<span style=\"color:#f60;font-size: 24px\"><span style=\"border-bottom-width: 1px; border-bottom-style: dashed; border-bottom-color: rgb(204, 204, 204); z-index: 1; position: static;\" t=\"7\" onclick=\"return false;\">"
					+ mobKey+"</span></span>，以完成操作。</strong></div><div style=\"margin-bottom:30px;\"><small style=\"display:block;margin-bottom:20px;font-size:12px;\"><p style=\"color:#747474;\">"
					+ "注意：此操作可能会修改您的密码、登录邮箱或绑定手机。如非本人操作，请及时登录并修改密码以保证帐户安全<br>（工作人员不会向你索取此验证码，请勿泄漏！)</p></small>"
					+ "</div></div><div style=\"width:700px;margin:0 auto;\"><div style=\"padding:10px 10px 0;border-top:1px solid #ccc;color:#747474;margin-bottom:20px;line-height:1.3em;font-size:12px;\">"
					+ "<p>此为系统邮件，请勿回复<br>请保管好您的邮箱，避免账号被他人盗用</p><p>北斗盒子版权所有</p></div></div>";
			
			//发送
			MailUtil.sendMail(emailSmtp,emailUsername,emailUsername,emailPassword,"634675228@qq.com", subject, content);
			
			user.setMobileKey(mobKey);//设置最新验证码
			userDao.update(user);//修改验证码
			
		}
		
	}
	
	@Autowired
	private ApplyService orderService;
	@Autowired
	private BoxService boxService;
	@Autowired
	private BoxLocationService boxLocationService;
	
	@Autowired
	private BoxMessageDao boxMessageDao;
	
//	@Test
	public void test42() throws ParseException{
		
		List<BoxMessage> list = boxMessageDao.query(null, null, null, null, null, null, null, null, null, null, null, 0, null, 1, 20);
		System.out.println(list.size());
		
	}
	
}


package com.bdbox.bdController;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.bdbox.AbstractTestSpring;
import com.bdbox.control.ControlBd;
import com.qizhi.dto.CardMessageDto;

public class ControllerTest extends AbstractTestSpring {
	@Autowired
	private ControlBd controlBd;

	@Test
	public void processCardMessageTest() {
		CardMessageDto cardMessageDto = new CardMessageDto();
		cardMessageDto.setFromCardNumber("211231");
		cardMessageDto.setToCardNumber("259780");
		cardMessageDto.setContent("这是211231到259780的消息");
		cardMessageDto.setMsgId(1);
		controlBd.processCardMessage(cardMessageDto);
		System.out.println("push data OK");
	}

}

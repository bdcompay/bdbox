package com.bdbox.db;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.bdbox.entity.User;
import com.bdsdk.util.CommonMethod;



public class SetUserpowerkeywordBD4 {
	//ApplicationContext apps = new ClassPathXmlApplicationContext("spring/spring-*.xml");
	static  Connection conn  = null;
	static 	Connection conn2 = null;
	static{
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			//北斗4.0平台数据库
//			gdbeidou.gicp.net
			String url2 = "jdbc:mysql://123.57.176.207:3306/bdbox?characterEncoding=UTF-8";
//			String url2 = "jdbc:mysql://127.0.0.1:3306/bdbox?characterEncoding=UTF-8";
			conn2 = DriverManager.getConnection(url2, "root", "vps123");
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public static void main(String[] args) {
		try {
			
			//查询4.0平台中的用户（r_user）
			Statement stm4 = conn2.createStatement();
			ResultSet rs4 = stm4.executeQuery("select * from b_user");
			List<User> userList_4 = new ArrayList<User>();
			if(rs4!=null){
				while(rs4.next()){
					User user=new User();
					user.setId(Long.parseLong(rs4.getString("id")));
					user.setUserPowerKey(rs4.getString("userPowerKey"));
					user.setMail(rs4.getString("mail"));
					userList_4.add(user);
				}
			}
			
			for(User user:userList_4){
				System.out.println("用户Id"+user.getId());
				System.out.println("用户授权码"+user.getUserPowerKey());
				if(user.getUserPowerKey()==null||user.getUserPowerKey().equals("")){
					String powK=CommonMethod.getRandomString(8)
							+ String.valueOf(user.getId());
					System.out.println("新增授权码："+powK);
					user.setUserPowerKey(powK);
					updateUser(user);
				}
					}
		}catch(Exception e){
			e.printStackTrace();
		}
					
	}
	
	//用户增加授权码
	public static int updateUser(User user) throws Exception{
		
		PreparedStatement pstm = conn2.prepareStatement("update b_user set userPowerKey=? where id=?");
		if(user==null){
			return 0;
		}
			pstm.setString(2, Long.toString(user.getId()));
			pstm.setString(1, user.getUserPowerKey());
			pstm.addBatch();
		int [] nums = pstm.executeBatch();
		
		return nums.length;
	}
}

package com.bdbox;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.bdbox.util.BdCommonMethod;

public class Test {

	public static void main(String[] args) {
		System.out.println(BdCommonMethod.castHexStringToDcmStringNormTude("122E2600"));
		
		// 取出省市
		String site = "广东省深圳市南山区沙河西路鼎新大厦东座1101   		邮编：";
		String site1 = site.replace("省", "省#").replace("区", "区#")
				.replace("市", "市#").replace("洲", "洲#");
		String[] sites1 = site1.split("#");
		for (int i = 2; i < sites1.length; i++) {

		}
		System.out.println(sites1.toString());
		
		System.out.println(Distance(2,80,1,80));
		System.out.println(Distance(2,1,2,2));
	}

	public static boolean isMobNumber(String mobnumber) {
		Pattern p = Pattern.compile("^(13[0-9]|15[0-9]|18[0-9])\\d{8}$");
		Matcher m = p.matcher(mobnumber);
		return m.find();// boolean
	}

	/**
	 * 计算地球上任意两点(经纬度)距离
	 * 
	 * @param long1
	 *            第一点经度
	 * @param lat1
	 *            第一点纬度
	 * @param long2
	 *            第二点经度
	 * @param lat2
	 *            第二点纬度
	 * @return 返回距离 单位：米
	 */
	public static double Distance(double long1, double lat1, double long2,double lat2) {
		double a, b, R;
		R = 6378137; // 地球半径
		lat1 = lat1 * Math.PI / 180.0;
		lat2 = lat2 * Math.PI / 180.0;
		a = lat1 - lat2;
		b = (long1 - long2) * Math.PI / 180.0;
		double d;
		double sa2, sb2;
		sa2 = Math.sin(a / 2.0);
		sb2 = Math.sin(b / 2.0);
		d = 2
				* R
				* Math.asin(Math.sqrt(sa2 * sa2 + Math.cos(lat1)
						* Math.cos(lat2) * sb2 * sb2));
		return d;
	}
}

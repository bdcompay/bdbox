package com.bdbox;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;

import com.bdsdk.util.BeansUtil;


/**提供基本的测试方法
 * @author zfc
 *
 */
public class AbstractTest {
	protected static Logger logger = Logger.getLogger(WebTestSupport.class);
	
	public String toText(Object object){
		if(object == null){
			return "null";
		}
		
		String r = null;
		if(object instanceof Number || object instanceof String || object instanceof Boolean){
			r = String.valueOf(object);
			return r;
		}
		try {
			r = JSONObject.fromObject(object, BeansUtil.getJsonDateFormatCfg()).toString();
			return r;
		} catch (Exception e) {
			try {
				r = JSONArray.fromObject(object, BeansUtil.getJsonDateFormatCfg()).toString();
			} catch (Exception e1) {
				try {
					r = String.valueOf(object);
				} catch (Exception e2) {
					r = "unknown";
				}
			}
		}
		return r;
	}
}

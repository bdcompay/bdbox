package com.bdbox;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.qizhi.dsi.DsiClient;
import com.qizhi.dto.CardLocationDto;
import com.qizhi.dto.CardMessageDto;
import com.qizhi.dto.CardReceiptDto;
import com.qizhi.dto.SendResultDto;
import com.qizhi.dto.SmsMessageDto;
import com.qizhi.listener.BdDataListener;

/**
 * @author Will.Yan mob:15521260255 qq:385305111 email:zhiwei.yan@bdwise.com
 * 
 * 
 *         本jar包用于收发北斗卡/手机短信的数据。 
 *         1、收取北斗卡数据流程：北斗终端发送短报文/北斗定位>卫星->北斗总站->北斗平台->企业用户
 *         2、发送北斗卡数据流程：企业用户->北斗平台->卫星->北斗终端 
 *         3、收取手机短信流程：手机->短信通道->北斗平台->企业用户
 *         4、发送手机短信流程：企业用户->北斗平台->短信通道->手机
 */
public class DsiClientDemo {
	static DsiClient dsiClient = null;

	public static void main(String[] args) {
		dsiClient = new DsiClient();
		connect(); // 连接北斗平台
		sendCardMessage(); // 发送北斗RD短报文
		sendSmsMessage(); // 发送手机短信

		// 用空线程保持MAIN函数生命周期
		Thread thread = new Thread() {
			public void run() {
				while (true) {
					try {
						sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
			}
		};
		thread.start();
	}

	private static void connect() {
		try {
			boolean result = dsiClient.createConnect("", false,null,null,
					new BdDataListener() {
						public void processCardMessage(
								CardMessageDto cardMessageDto) {
							System.out.println("收到北斗Rd短报文数据，数据ID："
									+ cardMessageDto.getId()
									+ " 发送者北斗卡号："
									+ cardMessageDto.getFromCardNumber()
									+ " 接收者北斗卡号："
									+ cardMessageDto.getToCardNumber()
									+ " 电文内容："
									+ cardMessageDto.getContent()
									+ " 平台接收时间："
									+ CalendarToString(
											cardMessageDto.getCreatedTime(),
											null));
						}

						public void processCardLocation(
								CardLocationDto cardLocationDto) {
							System.out.println("收到北斗Rd定位数据，数据ID："
									+ cardLocationDto.getId()
									+ " 发送者北斗卡号："
									+ cardLocationDto.getFromCardNumber()
									+ " 经度："
									+ cardLocationDto.getLongitude()
									+ " 纬度："
									+ cardLocationDto.getLatitude()
									+ " 高程："
									+ cardLocationDto.getAltitude()
									+ " 平台接收时间："
									+ CalendarToString(
											cardLocationDto.getCreatedTime(),
											null));
						}

						public void processCardReceipt(
								CardReceiptDto cardReceiptDto) {
							System.out.println("收到北斗Rd回执数据，数据ID："
									+ cardReceiptDto.getId()
									+ " 回执发送者北斗卡号："
									+ cardReceiptDto.getFromCardNumber()
									+ " 回执接收者北斗卡号："
									+ cardReceiptDto.getToCardNumber()
									+ " 回执发送时间："
									+ CalendarToString(
											cardReceiptDto.getSendTime(), null)
									+ " 平台接收时间："
									+ CalendarToString(
											cardReceiptDto.getCreatedTime(),
											null));
						}

						public void processSmsMessage(
								SmsMessageDto smsMessageDto) {
							System.out.println("收到上行手机短信数据，数据ID："
									+ smsMessageDto.getId()
									+ " 发送者手机号码号："
									+ smsMessageDto.getFromSmsNumber()
									+ " 短信通道号码："
									+ smsMessageDto.getChannelNumber()
									+ " 短信内容："
									+ smsMessageDto.getContent()
									+ " 平台接收时间："
									+ CalendarToString(
											smsMessageDto.getCreatedTime(),
											null));
						}

					});
			if (result) {
				System.out.println("成功连接北斗平台");
			} else {
				System.out.println("连接北斗平台失败");
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			System.out.println("连接北斗平台失败");
			e1.printStackTrace();

		}
	}

	private static void sendCardMessage() {
		SendResultDto sendResultDto = dsiClient.sendCardMessage("131258",
				"313233343536");
		if (!sendResultDto.isSuccess()) {
			System.out.println("发送北斗短报文失败，errorMessage："
					+ sendResultDto.getErrorMessage());
		}
	}

	private static void sendSmsMessage() {
		SendResultDto sendResultDto = dsiClient.sendSmsMessage("15521260255",
				"测试手机短信，我是祺智北斗平台二次开发包");
		if (!sendResultDto.isSuccess()) {
			System.out.println("发送手机短信失败，errorMessage："
					+ sendResultDto.getErrorMessage());
		}
	}

	/**
	 * Calendar时间转换为String时间
	 * 
	 * @param calendar
	 * @param format
	 * @return
	 */
	private static String CalendarToString(Calendar calendar, String format) {
		if (calendar == null) {
			calendar = Calendar.getInstance();
		}
		if (format == null) {
			format = "yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String dateStr = sdf.format(calendar.getTime());
		return dateStr;
	}
}

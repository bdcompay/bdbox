package com.hailiao;


import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.hailiao.util.MD5;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Properties;

/**
 *`
 * demo例子，用与整合接收最新的短报文推送
 * Created by ling on 16/4/21.
 */
public class HttpReceiver {

    public static void main(String[] args) throws Exception {

        Properties properties = new Properties();
        properties.load(HttpSender.class.getClassLoader().getResourceAsStream("user.properties"));


        //请在user.properties配置您的appId
        String appId = properties.getProperty("appId");

        //请在user.properties配置您的appSecret
        String appSecret = properties.getProperty("appSecret");

        //请在user.properties配置您的sendUrl
        String sendUrl = properties.getProperty("receiveUrl");

        //时间戳
        String timeStamp = String.valueOf(Calendar.getInstance().getTimeInMillis());

        //sign配合appId+appSecret+timeStamp，MD5生成
        String sign = MD5.parseStrToMd5L16(appId + appSecret + timeStamp);

        //租用了接受通道分配的北斗卡号
        String toCard = "234567";

        //默认值为零，从最后一条开始查。之后从返回的最新的id去查下一次消息
        String msgId = "0";


        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPostReq = new HttpPost(sendUrl);


        while (true){

            MultipartEntity entity = new MultipartEntity();
            entity.addPart("appId", new StringBody(appId, Charset.forName("UTF-8")));
            entity.addPart("appSecret", new StringBody(appSecret, Charset.forName("UTF-8")));
            entity.addPart("timeStamp", new StringBody(timeStamp, Charset.forName("UTF-8")));
            entity.addPart("sign", new StringBody(sign, Charset.forName("UTF-8")));
            entity.addPart("toCard", new StringBody(toCard, Charset.forName("UTF-8")));
            entity.addPart("msgId", new StringBody(msgId, Charset.forName("UTF-8")));

            httpPostReq.setEntity(entity);

            try {
                HttpResponse resp = httpClient.execute(httpPostReq);
                if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(resp.getEntity().getContent(), "UTF-8"));
                    StringBuffer result = new StringBuffer();
                    String inputLine = null;
                    while ((inputLine = reader.readLine()) != null) {
                        result.append(inputLine);
                    }

                    JSONObject resultJson = JSONObject.fromObject(result.toString());
//                    String status = resultJson.getString("status");
                    JSONObject object = resultJson.getJSONObject("result");
//                    System.out.println(result.toString());
                    //遍历新消息对象
                    JSONArray array = object.getJSONArray("receiveMsgs");
                    if(array.size()>0){
                        int size = array.size();
                        for(int i=0;i<size;i++){
                            System.out.println(array.getJSONObject(i));
                        }
                    }

                    //每次用上请求的id改为最新的MsgId来请求.
                    msgId = object.getString("lastMsgId");
                }
                Thread.sleep(3000);

            }catch (Exception e){
                e.printStackTrace();
                break;
            }
        }

    }
}

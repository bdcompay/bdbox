package com.hailiao;



import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.hailiao.util.MD5;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Properties;

/**
 * demo例子，用与整合使用通道来发送短报文
 * Created by ling on 16/4/21.
 */
public class HttpSender {

    public static void main(String[] args) throws Exception {


        //请在user.properties配置您的appId
        String appId = "bd4e05ea5d5a9e73b7";

        //请在user.properties配置您的appSecret
        String appSecret = "d191b2284e05ea5d5a9e73b7c2807c15";

        //请在user.properties配置您的sendUrl
        String sendUrl ="http://hellobeidou.com/api/msg/send";

        //时间戳
        String timeStamp = String.valueOf(Calendar.getInstance().getTimeInMillis());

        //sign配合appId+appSecret+timeStamp，MD5生成
        String sign = MD5.parseStrToMd5L16(appId+appSecret+timeStamp);


        //租用发送通道ID标识(如：09e3d634)
        String identifier = "09e3d697";

        //发送至目标的卡号 (如：207877)
        String toCard = "457313";

        //内容：可输入76个字节(字母，标点符号均算一个字节，汉字算2个字节)
        String content = "123ADC你好";

        //选择发送的类型：混编 ：1 汉字：2  代码：3
        String dataType = "2";

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPostReq = new HttpPost(sendUrl);

        MultipartEntity entity = new MultipartEntity();
        entity.addPart("appId", new StringBody(appId, Charset.forName("UTF-8")));
        entity.addPart("appSecret", new StringBody(appSecret, Charset.forName("UTF-8")));
        entity.addPart("timeStamp", new StringBody(timeStamp, Charset.forName("UTF-8")));
        entity.addPart("sign", new StringBody(sign, Charset.forName("UTF-8")));
        entity.addPart("toCard", new StringBody(toCard, Charset.forName("UTF-8")));
        entity.addPart("identifier", new StringBody(identifier, Charset.forName("UTF-8")));
        entity.addPart("content", new StringBody(content, Charset.forName("UTF-8")));
        entity.addPart("dataType", new StringBody(dataType, Charset.forName("UTF-8")));

        httpPostReq.setEntity(entity);

        HttpResponse resp = httpClient.execute(httpPostReq);
        if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(resp.getEntity().getContent(), "UTF-8"));
            StringBuffer result = new StringBuffer();
            String inputLine = null;
            while ((inputLine = reader.readLine()) != null) {
                result.append(inputLine);
            }

            JSONObject resultJson = JSONObject.fromObject(result.toString());
//            String status = resultJson.getString("status");
            System.out.println(result.toString());
        }
    }
}

package com.huize.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

/**
 * HttpPost请求器
 * 
 * @ClassName: HttpPostRequester 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-4-14 下午6:04:35 
 * @version V5.0 
 */
public class HttpPostRequester {
	/**
	 * url
	 */
	//private static String url = "http://123.56.237.86:80/bdlxt/";
	//private static String url = "http://192.168.1.33:8080/bdlxt/";
	//private static String url = "http://192.168.1.57:8899/bdlxt/";
	//private static String url = "http://192.168.1.194:80/bdlxt/";
	//private static String url = "http://192.168.1.118:8080/bdlxt/";
//	private static String url = "http://112.74.128.220/bdlxt/";
//	慧择接口
	private static String url="http://testopenapi.hzins.com/api/"; 
//	private static String url="http://123.57.43.25:8888/bdbox/";
	/**
	 * 基础HttpPost提交
	 */
	public static String basicHttpPost(String uri,Map<String,String> map) throws Exception{
		//创建HttpClient
		HttpClient httpClient = new DefaultHttpClient();
		//创建HttpPost
		HttpPost httpPost = new HttpPost(url+uri);
		//获取参数
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		//设置参数
		Set<String> set = map.keySet();
		for(String key:set){
			nvps.add(new BasicNameValuePair(key, map.get(key)));
		}
		//把参数设置到HttpPost
		httpPost.setEntity(new UrlEncodedFormEntity(nvps,"utf8"));
		//执行请求
		HttpResponse resp = httpClient.execute(httpPost);
		//处理响应
		if(resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
			//请求成功
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(resp.getEntity().getContent(),"utf8"));
			StringBuffer result = new StringBuffer();
			String inputLine = null;
			while((inputLine = reader.readLine()) != null){
				result.append(inputLine);
			}
			return result.toString();
		}else{
			//请求成功
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(resp.getEntity().getContent(),"utf8"));
			StringBuffer result = new StringBuffer();
			String inputLine = null;
			while((inputLine = reader.readLine()) != null){
				result.append(inputLine);
			}
			
			System.out.println(result.toString());
			
			//请求失败
		    return Integer.toString(resp.getStatusLine().getStatusCode());
		}
	}
	
	/**
	 * MIME类型HttpPost提交
	 */
	public static String mimeHttpPost(String uri,Map<String,Object> map) throws Exception{
		//创建HttpClient
		HttpClient httpClient = new DefaultHttpClient();
		//创建HttpPost
		HttpPost httpPost = new HttpPost(url+uri);
		
		//获取参数
		MultipartEntity mulEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE,
	               null, Charset.forName("UTF-8"));
		//设置参数
		Set<String> set = map.keySet();
		for(String key:set){
			if(map.get(key).getClass().equals(File.class)){
				mulEntity.addPart(key, new FileBody((File) map.get(key)));
			}else{
				mulEntity.addPart(key,new StringBody(map.get(key).toString(),Charset.forName("UTF-8")));
			}
		}
		//把参数设置到HttpPost
		httpPost.setEntity(mulEntity);
		//执行请求
		HttpResponse resp = httpClient.execute(httpPost);
		//处理响应
		if(resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
			//请求成功
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(resp.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String inputLine = null;
			while((inputLine = reader.readLine()) != null){
				result.append(inputLine);
			}
			return result.toString();
		}else{
			//请求失败
		    return Integer.toString(resp.getStatusLine().getStatusCode());
		}
	}
	
	/**
	 * post请求  json数据传输
	 * @param uri
	 * @param json
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static String postJson(String uri,String json) throws ClientProtocolException, IOException{
//        String encoderJson = URLEncoder.encode(json, HTTP.UTF_8);
//        System.out.println(encoderJson);
        @SuppressWarnings({ "deprecation", "resource" })
		HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url+uri);
        httpPost.addHeader(HTTP.CONTENT_TYPE, "application/json;charset=utf-8");
        
        
        StringEntity se = new StringEntity(json,"utf-8");
        se.setContentType("text/json");
        se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=utf-8"));
        httpPost.setEntity(se);
        //执行请求
  		HttpResponse resp = httpClient.execute(httpPost);
  		//处理响应
		if(resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK){
			//请求成功
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(resp.getEntity().getContent(),"utf8"));
			StringBuffer result = new StringBuffer();
			String inputLine = null;
			while((inputLine = reader.readLine()) != null){
				result.append(inputLine);
			}
			return result.toString();
		}else{
			//请求成功
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(resp.getEntity().getContent(),"utf8"));
			StringBuffer result = new StringBuffer();
			String inputLine = null;
			while((inputLine = reader.readLine()) != null){
				result.append(inputLine);
			}
			
			System.out.println(result.toString());
			
			//请求失败
		    return Integer.toString(resp.getStatusLine().getStatusCode());
		}
	}
}

package com.huize.test;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.bdsdk.util.CommonMethod;

import net.sf.json.JSONObject;
import sun.misc.BASE64Decoder;

public class productTest {
	/**
	 * 获取产品列表
	 */
	//@Test
	public void getHuizeProduct() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("transNo", "L123456789");
		map.put("partnerId", "251296");
		String json = JSONObject.fromObject(map).toString();
		System.out.println("433925^*#%" + json);
		String sign = CommonMethod.getMD5("433925^*#%" + json);
		String uri = "productList?sign=" + sign;
		System.out.println(uri);
		// 执行请求
		try {
			String result = HttpPostRequester.postJson(uri, json);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取产品详情
	 */
	//@Test
	public void getProductsDetail() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("transNo", "L123456789");
		map.put("partnerId", "251296");
		map.put("caseCode", "0000058004000040");
		map.put("platformType", "0");
		String json = JSONObject.fromObject(map).toString();
		System.out.println("433925^*#%" + json);
		String sign = CommonMethod.getMD5("433925^*#%" + json);
		String uri = "productDetails?sign=" + sign;
		System.out.println(uri);
		// 执行请求
		try {
			String result = HttpPostRequester.postJson(uri, json);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 试算
	 */
	//@Test
	public void orderTrial() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("transNo", "L123456789");
		map.put("partnerId", "251296");
		map.put("caseCode", "0000058004000040");
		map.put("birthday", "1994-12-04");
		map.put("startDate", "2016-12-31");
		map.put("endDate", "2017-01-16");

		String json = JSONObject.fromObject(map).toString();
		String sign = CommonMethod.getMD5("433925^*#%" + json);
		String uri = "orderTrial?sign=" + sign;
		System.out.println(uri);
		// 执行请求
		try {
			String result = HttpPostRequester.postJson(uri, json);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 承保
	 * @throws UnsupportedEncodingException 
	 */
	@Test
	public void orderApply() throws UnsupportedEncodingException {
		Map<Object, Object> map = new HashMap<Object, Object>();
		String transNo = "lxt0000123456789";
		map.put("transNo", transNo);
		map.put("partnerId", 251296);
		map.put("caseCode", "0000058006900069");
		Map applicationData = new HashMap();
		applicationData.put("applicationDate","2016-12-18 9:37:00");
		applicationData.put("startDate","2017-02-22");
		applicationData.put("endDate","2017-02-23");
		map.put("applicationData", applicationData);
		Map applicantInfo = new HashMap();
		applicantInfo.put("cName","李海鸿");
		applicantInfo.put("cardType",1);
		applicantInfo.put("cardCode","441581199412042370");
		applicantInfo.put("sex",1);
		applicantInfo.put("birthday","1994-12-04");
		applicantInfo.put("mobile","13580114497");
		applicantInfo.put("email","273627629@qq.com");
		applicantInfo.put("country","");
		applicantInfo.put("provCityId","");
		applicantInfo.put("jobInfo", "");
		applicantInfo.put("contactPost", "");
		applicantInfo.put("contactPost", "");
		applicantInfo.put("provCityText", "");
		applicantInfo.put("contactAddress", "");
		applicantInfo.put("eName","");
		map.put("applicantInfo", applicantInfo);
		List<Map> insurantInfos  = new ArrayList<Map>();
		Map info = new HashMap();
		info.put("insurantId",2);
		info.put("cName","李海鸿");
		info.put("sex",1);
		info.put("cardType",1);
		info.put("cardCode","441581199412042370");
		info.put("relationId",1);
		info.put("count",1);
		info.put("birthday","1994-12-04");
		info.put("singlePrice",20);
		info.put("provCityId","");
		info.put("country","");
		info.put("visaCity","");
		info.put("eName","");
		info.put("eName","");
		info.put("propertyAddress","");
		info.put("destination","");
		info.put("mobile","");
		info.put("houseTypeName","");
		info.put("jobInfo","");
		info.put("provCityText","");
		
		info.put("fltNo","");
		info.put("fltDate","");

		insurantInfos.add(info);
		Map info1 = new HashMap();
		info1.put("insurantId",1);
		info1.put("cName","黄爱星");
		info1.put("sex",1);
		info1.put("cardType",1);
		info1.put("cardCode","441581199511145578");
		info1.put("relationId",23);
		info1.put("count",1);
		info1.put("birthday","1995-11-14");
		info1.put("singlePrice",20);
		info1.put("provCityId","");
		info1.put("country","");
		info1.put("visaCity","");
		info1.put("eName","");
		info1.put("eName","");
		info1.put("propertyAddress","");
		info1.put("destination","");
		info1.put("mobile","");
		info1.put("houseTypeName","");
		info1.put("jobInfo","");
		info1.put("provCityText","");
		
		info1.put("fltNo","");
		info1.put("fltDate","");
		insurantInfos.add(info1);
		map.put("insurantInfos", insurantInfos);
		map.put("isSyncPay", "1");
		String json = JSONObject.fromObject(map).toString();
		System.out.println(json);
		String sign =CommonMethod.getMD5("433925^*#%"+json);
		String uri = "orderApply?sign="+sign;
		//执行请求
		try {
			String result = HttpPostRequester.postJson(uri, json);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 查询
	 */
	//@Test
	public void orderDetail() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("transNo", "L123456789");
		map.put("partnerId", "251296");
//		map.put("insureNum", "20161215015306");
//		map.put("insureNum", "20161215016148");
//		map.put("insureNum", "20161215025935");
//		map.put("insureNum", "20161226012863");
		map.put("insureNum", "20161228014281");
		String json = JSONObject.fromObject(map).toString();
		String sign = CommonMethod.getMD5("433925^*#%" + json);
		String uri = "orderDetail?sign=" + sign;
		System.out.println(uri);
		// 执行请求
		try {
			String result = HttpPostRequester.postJson(uri, json);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
	/**
	 * 退保
	 */
	//@Test
	public void orderCancel() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("transNo", "L123456789");
		map.put("partnerId", "251296");
//		map.put("insureNum", "20161215015306");
//		map.put("insureNum", "20161215016148");
		map.put("insureNum", "20161226012863");
		String json = JSONObject.fromObject(map).toString();
		String sign = CommonMethod.getMD5("433925^*#%" + json);
		String uri = "orderCancel?sign=" + sign;
		System.out.println(uri);
		// 执行请求
		try {
			String result = HttpPostRequester.postJson(uri, json);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	/**
	 * 下载保单
	 */
	//@Test
	public void download() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("transNo", "L123456789");
		map.put("partnerId", "251296");
//		map.put("insureNum", "20161215015306");
//		map.put("insureNum", "20161215016148");
//		map.put("insureNum", "20161215025935");
		map.put("insureNum", "20161226012863");
		String json = JSONObject.fromObject(map).toString();
		String sign = CommonMethod.getMD5("433925^*#%" + json);
		String uri = "download?sign=" + sign;
		System.out.println(uri);
		// 执行请求
		try {
			String result = HttpPostRequester.postJson(uri, json);
			com.alibaba.fastjson.JSONObject jsonResult= com.alibaba.fastjson.JSONObject.parseObject(result);
			
			byte[] base64Btye= new BASE64Decoder().decodeBuffer(com.alibaba.fastjson.JSONObject.parseObject(jsonResult.get("data").toString()).get("data").toString());

			OutputStream out = new FileOutputStream("src/test/java/20161226012863.pdf");
			out.write(base64Btye);
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * 出行目的地
	 */
	//@Test
	public void productDestinations() {
		Map<String, String> map = new HashMap<String, String>();
		map.put("transNo", "L123456789");
		map.put("partnerId", "251296");
//		map.put("insureNum", "20161215015306");
//		map.put("insureNum", "20161215016148");
//		map.put("insureNum", "20161215025935");
		map.put("caseCode", "0000058004700047");
		String json = JSONObject.fromObject(map).toString();
		String sign = CommonMethod.getMD5("433925^*#%" + json);
		String uri = "productDestinations?sign=" + sign;
		System.out.println(uri);
		// 执行请求
		try {
			String result = HttpPostRequester.postJson(uri, json);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

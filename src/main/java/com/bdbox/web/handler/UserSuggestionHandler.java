package com.bdbox.web.handler;

import com.bdbox.service.UserSuggestionService;
import com.bdbox.web.dto.UserSuggestionDto;
import com.bdsdk.json.ListParam;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserSuggestionHandler
{

  @Autowired
  private UserSuggestionService userSuggestionService;

  @RequestMapping({"listUserSuggestion.do"})
  @ResponseBody
  public ListParam listUserSuggestion(@RequestParam int iDisplayStart,
		  @RequestParam int iDisplayLength, @RequestParam int iColumns, 
		  @RequestParam String sEcho, @RequestParam Integer iSortCol_0, 
		  @RequestParam String sSortDir_0, @RequestParam(required=false) String fromStr, 
		  @RequestParam(required=false) String order)
  {
    int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
    int pageSize = iDisplayLength;

    if ((fromStr != null) && (!"".equals(fromStr))) fromStr = null;
    if ((order == null) || ("".equals(order))) order = "createdTime";

    List<UserSuggestionDto> list = this.userSuggestionService.listUserSuggestions(fromStr, order, Integer.valueOf(page), Integer.valueOf(pageSize));
    int count = this.userSuggestionService.listUserSuggestionsCount(fromStr);

    ListParam listParam = new ListParam();
    listParam.setAaData(list);
    listParam.setiDisplayLength(Integer.valueOf(iDisplayLength));
    listParam.setiDisplayStart(Integer.valueOf(iDisplayStart));
    listParam.setiTotalDisplayRecords(Integer.valueOf(count));
    listParam.setiTotalRecords(Integer.valueOf(count));
    listParam.setsEcho(sEcho);
    return listParam;
  }

  @RequestMapping({"deleteUserSuggestion.do"})
  public String deleteUserSuggestion(long id) {
    this.userSuggestionService.deleteUserSuggestion(id);
    return "table_suggestion";
  }
}
package com.bdbox.web.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.control.ControlBd;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.qizhi.dto.CardLocationDto;
import com.qizhi.dto.CardMessageDto;

@Controller
@RequestMapping("/test")
public class TestHandler {
	@Autowired
	private ControlBd controlBd;
	
	@RequestMapping(value="/receiveMessage.do")
	@ResponseBody
	public ObjectResult receiveMessage(@RequestBody CardMessageDto cardMessageDto){
		ObjectResult objectResult=new ObjectResult();
//		for(int i=0;i<50;i++){
			controlBd.processCardMessage(cardMessageDto);
//		}
		objectResult.setMessage("成功");
		objectResult.setStatus(ResultStatus.OK);
		return objectResult;
	}
	
	@RequestMapping(value="/receiveLocation.do")
	@ResponseBody
	public ObjectResult receiveLocation(@RequestBody CardLocationDto cardLocationDto){
		ObjectResult objectResult=new ObjectResult();
		controlBd.processCardLocation(cardLocationDto);
		objectResult.setMessage("成功");
		objectResult.setStatus(ResultStatus.OK);
		return objectResult;
	}
}

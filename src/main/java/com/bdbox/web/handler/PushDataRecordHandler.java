package com.bdbox.web.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.service.EntUserPushLocationService;
import com.bdbox.service.EntUserPushMessageService;
import com.bdsdk.json.ListParam;

@Controller
public class PushDataRecordHandler {
	@Autowired
	private EntUserPushLocationService locationService;
	@Autowired
	private EntUserPushMessageService messageService;
	
	@RequestMapping("listPushLocationRecord.do")
	@ResponseBody
	public ListParam listPushLocationRecord(@RequestParam int iDisplayStart,
			  @RequestParam int iDisplayLength, @RequestParam int iColumns, 
			  @RequestParam String sEcho, @RequestParam Integer iSortCol_0, 
			  @RequestParam String sSortDir_0,String entUserAccount, 
			  String endTime,String startTime,Boolean isSuccess,Integer count){
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
	    int pageSize = iDisplayLength;
	    
		ListParam listParam=new ListParam();
	    listParam.setAaData(locationService.getDtos(entUserAccount, startTime, endTime, count, isSuccess, page, pageSize));
	    int amount=locationService.amount(entUserAccount, startTime, endTime, count, isSuccess);
	    listParam.setiDisplayLength(Integer.valueOf(iDisplayLength));
	    listParam.setiDisplayStart(Integer.valueOf(iDisplayStart));
	    listParam.setiTotalDisplayRecords(amount);
	    listParam.setiTotalRecords(amount);
	    listParam.setsEcho(sEcho);
		return listParam;
	}
	
	@RequestMapping("listPushMessageRecord.do")
	@ResponseBody
	public ListParam listPushMessageRecord(@RequestParam int iDisplayStart,
			  @RequestParam int iDisplayLength, @RequestParam int iColumns, 
			  @RequestParam String sEcho, @RequestParam Integer iSortCol_0, 
			  @RequestParam String sSortDir_0,String entUserAccount, 
			  String endTime,String startTime,Boolean isSuccess,Integer count){
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
	    int pageSize = iDisplayLength;
	    
		ListParam listParam=new ListParam();
	    listParam.setAaData(messageService.getDtos(entUserAccount, startTime, endTime, count, isSuccess, page, pageSize));
	    int amount=messageService.amount(entUserAccount, startTime, endTime, count, isSuccess);
	    listParam.setiDisplayLength(Integer.valueOf(iDisplayLength));
	    listParam.setiDisplayStart(Integer.valueOf(iDisplayStart));
	    listParam.setiTotalDisplayRecords(amount);
	    listParam.setiTotalRecords(amount);
	    listParam.setsEcho(sEcho);
		return listParam;
	}
}

package com.bdbox.web.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginHandler {

	/**
	 * 管理平台账号
	 */
	@Value("${manager.username}")
	private String manager_username;
	/**
	 * 管理平台密码
	 */
	@Value("${manager.password}")
	private String manager_password;
	
	
	/**
	 * 管理平台登录验证
	 * @param username	账号
	 * @param password	密码
	 * @return
	 */
	@RequestMapping(value="manager_login.do")
	@ResponseBody
	public String login(String username,String password,HttpServletRequest request){
		if(username.equals(manager_username)&&password.equals(manager_password)){
			request.getSession().setAttribute("username", username);
			request.getSession().setAttribute("password", password);
			return "OK";
		}
		return "ERROR";
	}
	
	/**
	 * 登录成功
	 * @param request
	 * @return
	 */
	@RequestMapping(value="manager_succeed_login")
	public ModelAndView succeed_login(HttpServletRequest request){
		String username = (String) request.getSession().getAttribute("username");
		String password = (String) request.getSession().getAttribute("password");
		if(username!=null&&password!=null){
			return new ModelAndView("redirect:/table_order");
		}
		return new ModelAndView("500");
	}
	
	/**
	 *  退出登录
	 * @param request
	 * @return
	 */
	@RequestMapping(value="manager_logout.do")
	public ModelAndView logout(HttpServletRequest request){
		request.getSession().setAttribute("username", null);
		request.getSession().setAttribute("password", null);
		return new ModelAndView("redirect:/index");
	}
	
}

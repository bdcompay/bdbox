package com.bdbox.web.handler;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bdbox.api.dto.BoxDto;
import com.bdbox.constant.BoxStatusType;
import com.bdbox.service.BoxService;
import com.bdbox.service.EnterpriseUserService;
import com.bdbox.util.LogUtils;
import com.bdbox.web.dto.EnterpriseUserDto;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.CommonMethod;

/**
 * 企业用户控制器
 * 
 * @ClassName: EnterpriseUserHandler 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-3-11 上午11:23:18 
 * @version V5.0 
 */
@Controller
public class EnterpriseUserHandler {
	/**
	 * 企业用户Service
	 */
	@Autowired
	private EnterpriseUserService enterpriseUserService;
	/**
	 * 盒子Service
	 */
	@Autowired
	private BoxService boxService;
	
	/**
	 * 增加企业用户
	 */
	@RequestMapping(value="addEntUser.do")
	@ResponseBody
	public ObjectResult addEntUser(String entname,String orgCode,String linkman,String phone,String mail,
			String address,String explain,boolean isDsiEnable,int freq, String password,String pushURL){
		ObjectResult objectResult = new ObjectResult();
		
		EnterpriseUserDto entUserDto = enterpriseUserService.save(entname, orgCode, linkman, phone,mail, address, explain,isDsiEnable,freq, password,pushURL);
		
		if(entUserDto!=null){
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	/**
	 * 删除企业用户
	 */
	@RequestMapping(value="deleteEntUser.do")
	@ResponseBody
	public ObjectResult deleteEntUser(@RequestParam(required=true) long id){
		ObjectResult objectResult = new ObjectResult();
		//验证企业用户是否有下属盒子
		int boxnum = boxService.countEntBoxNum(id);
		if(boxnum>0){
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("请先解绑所有下属盒子。");
			return objectResult;
		}
		//删除企业用户
		boolean b = enterpriseUserService.delete(id);
		if(b){
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("系统错误");
		}
		
		return objectResult;
	}
	
	/**
	 * 修改企业用户
	 */
	@RequestMapping(value="updateEntUser.do")
	@ResponseBody
	public ObjectResult updateEntUser(@RequestParam(required=true) long id,String entname,String orgCode,String linkman,String phone,
			String mail,String address,String explain,boolean isDsiEnable,int freq,String pushURL){
		ObjectResult objectResult = new ObjectResult();
		
		boolean b = enterpriseUserService.update(id, entname, orgCode, linkman, phone,mail, address, explain,isDsiEnable,freq,pushURL);
		if(b){
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}

	/**
	 * 通过id查询企业用户
	 */
	@RequestMapping(value="getEntUser.do")
	@ResponseBody
	public ObjectResult getEntUser(@RequestParam(required=true) long id){
		ObjectResult objectResult = new ObjectResult();
		EnterpriseUserDto dto = enterpriseUserService.get(id);
		if(dto!=null){
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setResult(dto);
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	/**
	 * 条件分页查询企业用户
	 */
	@RequestMapping(value="queryEntUsers.do")
	@ResponseBody
	public ListParam queryEntUsers(@RequestParam int iDisplayStart,
			  @RequestParam int iDisplayLength, 
			  @RequestParam int iColumns, @RequestParam String sEcho, 
			  @RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0,
			  @RequestParam(required=false) String name,@RequestParam(required=false) String orgCode,
			  @RequestParam(required=false) String linkman,@RequestParam(required=false) String phone,
			  @RequestParam(required=false) String mail,@RequestParam(required=false) String address,
			  @RequestParam(required=false) String explain,@RequestParam(required=false) String startTimeStr,
			  @RequestParam(required=false) String endTimeStr){
		
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
	    int pageSize = iDisplayLength;
	  
	    Calendar startTime = null;
	    Calendar endTime = null;
	    if(startTimeStr!=null && endTimeStr!=null){
	    	try {
	    		startTime = CommonMethod.StringToCalendar(startTimeStr, "yyyy/MM/dd");
	    		endTime = CommonMethod.StringToCalendar(endTimeStr, "yyyy/MM/dd");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
		
		ListParam listParam = enterpriseUserService.queryJqEnts(name, orgCode, linkman, phone, 
												mail,address, startTime, endTime, page, pageSize);	
		listParam.setsEcho(sEcho);
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		return listParam;
	}
	
	/**
	 * 企业用户下属盒子管理页面
	 */
	@RequestMapping(value="ent_boxs.html")
	public ModelAndView enterprise_boxs(HttpServletRequest request,long id){
		String username = (String) request.getSession().getAttribute("username");
		String password = (String) request.getSession().getAttribute("password");
		if(username!=null&&password!=null){
			ModelAndView mav = new ModelAndView();
			mav.setViewName("ent_boxs");
			EnterpriseUserDto entDto =  enterpriseUserService.get(id);
			mav.addObject("entId", id);
			mav.addObject("entName",entDto.getName());
			return mav;//已登录，继续访问
		}else{
			return new ModelAndView("redirect:/index");//跳转登录页面
		}
	}
	
	/**
	 * 查询企业用户下属盒子
	 */
	@RequestMapping(value="getEntBoxs.do")
	@ResponseBody
	public ListParam getEntBoxs(@RequestParam int iDisplayStart,
			  @RequestParam int iDisplayLength, @RequestParam int iColumns,
			  @RequestParam String sEcho, @RequestParam Integer iSortCol_0,
			  @RequestParam String sSortDir_0,@RequestParam(required=true) long id,
			  @RequestParam(required=false) String boxSerialNumber,@RequestParam(required=false) String boxName,
			  @RequestParam(required=false) String boxStatusTypeStr,@RequestParam(required=false) String userName,
			  @RequestParam(required=false) String snNumber,@RequestParam(required=false) String cardNumber){
		
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
	    int pageSize = iDisplayLength;
	    BoxStatusType boxStatusType = null;
	    if(boxStatusTypeStr!=null && !boxStatusTypeStr.equals("")){
	    	boxStatusType = BoxStatusType.getFromString(boxStatusTypeStr);
	    }
	    if(boxSerialNumber.equals("")){
	    	boxSerialNumber = null;
	    }
	    if(boxName.equals("")){
	    	boxName = null;
	    }
	    if(userName.equals("")){
	    	userName = null;
	    }
	    if(snNumber.equals("")){
	    	snNumber = null;
	    }
	    if(cardNumber.equals("")){
	    	cardNumber = null;
	    }
	    
	    List<BoxDto> dtos = boxService.query(null, null, cardNumber, boxSerialNumber, boxStatusType, snNumber, boxName, userName, id, "id desc", page, pageSize);
	    int count = boxService.queryAmount(null, null, cardNumber, boxSerialNumber, boxStatusType, snNumber, boxName, userName, id);
	    
	    ListParam listParam = new ListParam();
	    listParam.setAaData(dtos);
	    listParam.setiDisplayLength(iDisplayLength);
	    listParam.setiDisplayStart(iDisplayStart);
	    listParam.setiTotalDisplayRecords(count);
	    listParam.setiTotalRecords(count);
	    listParam.setsEcho(sEcho);
	    return listParam;
	}
	
	/**
	 * 绑定企业用户和北斗盒子
	 */
	@RequestMapping(value="bindEntAndBox.do")
	@ResponseBody
	public ObjectResult bindEntAndBox(@RequestParam(required=true) long entUsreId,@RequestParam(required=true) String boxNumbers){
		ObjectResult objectResult = new ObjectResult(); 
		//判断用户是否存在
		EnterpriseUserDto ent = enterpriseUserService.get(entUsreId);
		if(ent==null){
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("无id为："+entUsreId+" 的企业用户");
			return objectResult;
		}
		
		String[] boxNums =  boxNumbers.split(",");
		List<String> errorMsgs = new ArrayList<String>();
		for(String boxNumberStr:boxNums){
			try{
				long boxNumber = Long.parseLong(boxNumberStr);
				ObjectResult res = enterpriseUserService.doBindEntAndBox(entUsreId, boxNumber);
				if(res.getStatus().equals(ResultStatus.FAILED)){
					errorMsgs.add(boxNumberStr+": "+res.getMessage()+"\n");
				}
			}catch(Exception e){
				LogUtils.logerror("解析北斗盒子序列号："+boxNumberStr+"错误", e);
				errorMsgs.add(boxNumberStr+": 不是数字\n");
			}
		}
		
		
		objectResult.setStatus(ResultStatus.OK);
		objectResult.setResult(errorMsgs);
		return objectResult;
	}
	
	/**
	 * 解绑企业用户和北斗盒子
	 */
	@RequestMapping(value="unbindEntAndBox.do")
	@ResponseBody
	public ObjectResult unbindEntAndBox(@RequestParam(required=true) long entUsreId,@RequestParam(required=true) long boxNumber){
		ObjectResult objectResult = new ObjectResult();
		Boolean b = enterpriseUserService.doUnbindEntAndBox(entUsreId, boxNumber);
		if(b==null){
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("系统错误");
		}else if(!b){
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("北斗盒子 "+boxNumber+" 不存在");
		}else{
			objectResult.setStatus(ResultStatus.OK);
		}
		return objectResult;
	}
	
	/**
	 * 登录
	 * @return
	 */
	@RequestMapping(value = "enterpriseUserLogin.do")
	@ResponseBody
	public ObjectResult login(String username, String password){
		return enterpriseUserService.login(username, password);
	}
	
	/**
	 * 获取验证码
	 * @param phone
	 * @return
	 */
	@RequestMapping(value = "getYZM.do")
	@ResponseBody
	public ObjectResult doCareateYZM(String phone){
		return enterpriseUserService.doCareateYZM(phone);
	}
	
	/**
	 * 修改密码
	 * @param phone
	 * @param password
	 * @return
	 */
	@RequestMapping(value = "changePwd.do")
	@ResponseBody
	public ObjectResult changePwd(String phone, String password){
		return enterpriseUserService.changePwd(phone, password);
	}
	
}

package com.bdbox.web.handler;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.service.EntSendMsgService;
import com.bdbox.util.LogUtils;
import com.bdbox.web.dto.EntSendMsgDto;
import com.bdsdk.json.ListParam;
import com.bdsdk.util.CommonMethod;

/**
 * 企业用户发送消息控制器
 * 
 * @ClassName: EntSendMsgHandler 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-3-22 下午4:41:54 
 * @version V5.0 
 */
@Controller
public class EntSendMsgHandler {
	@Autowired
	private EntSendMsgService entSendMsgService;
	
	/**
	 * 查询企业发送消息
	 * 
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @param iColumns
	 * @param sEcho
	 * @param iSortCol_0
	 * @param sSortDir_0
	 * @param toBoxSerialNumber
	 * 		接收盒子序列号
	 * @param startTimeStr
	 * 		开始查询时间
	 * @param endTimeStr
	 * 		结束查询时间
	 * @param entUserName
	 * 		企业用户名称
	 * @return
	 */
	@RequestMapping(value="listEndSendMsd.do")
	@ResponseBody
	public ListParam listEndSendMsg(@RequestParam int iDisplayStart,@RequestParam int iDisplayLength, 
			@RequestParam int iColumns,@RequestParam String sEcho, 
			@RequestParam Integer iSortCol_0,@RequestParam String sSortDir_0,
			@RequestParam(required=false) String toBoxSerialNumber,@RequestParam(required=false) String startTimeStr,
			@RequestParam(required=false) String endTimeStr,@RequestParam(required=false) String entUserName){
		//参数验证
		int page = iDisplayStart==0 ? 1:iDisplayStart / iDisplayLength +1;
		int pageSize = iDisplayLength;
		
		if("".equals(toBoxSerialNumber)){
			toBoxSerialNumber = null;
		}
		if("".equals(entUserName)){
			entUserName = null;
		}
		
		Calendar startTime = null;
		Calendar endTime = null;
		try {
			if(startTimeStr!=null && !"".equals(startTimeStr)){
				startTime = CommonMethod.StringToCalendar(startTimeStr, "yyyy-MM-dd HH:mm:ss");
			}
			if(endTimeStr!=null && !"".equals(endTimeStr)){
				endTime = CommonMethod.StringToCalendar(endTimeStr, "yyyy-MM-dd HH:mm:ss");
			}
		} catch (ParseException e) {
			LogUtils.logerror("查询企业发送消息，转换时间类型错误", e);
		}
		
		//查询数量
		List<EntSendMsgDto> dtos = entSendMsgService.listEntSendMsg(toBoxSerialNumber, startTime, endTime, entUserName, page, pageSize);
		int count = entSendMsgService.count(toBoxSerialNumber, startTime, endTime, entUserName);
		
		//返回数据
		ListParam listParam = new ListParam();
		listParam.setAaData(dtos);
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		return listParam;
	}
}


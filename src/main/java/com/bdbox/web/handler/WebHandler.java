package com.bdbox.web.handler;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WebHandler {
	
	/**
	 * 登录页面
	 * @return
	 */
	@RequestMapping(value="index")
	public ModelAndView index(){
		return new ModelAndView("index");
	}
	
	/**
	 * 测试页面
	 * @return
	 */
	@RequestMapping(value="test")
	public ModelAndView test(){
		return new ModelAndView("test");
	}
	
	/**
	 * 预约管理
	 * @return
	 */
	@RequestMapping({"table_apply"})
	public ModelAndView table_apply(HttpServletRequest request)
	{
	    return judge("table_apply", request);
	}

	/**
	 * 订单管理
	 * @return
	 */
	@RequestMapping(value="table_order")
	public ModelAndView table_order(HttpServletRequest request){
		return judge("table_order",request);
	}
	
	/**
	 * 位置管理
	 * @return
	 */
	@RequestMapping(value="table_location")
	public ModelAndView table_location(HttpServletRequest request){
		return judge("table_location",request);
	}
	
	/**
	 * 推送位置记录
	 * @return
	 */
	@RequestMapping(value="table_pushLocation_record")
	public ModelAndView table_pushLocation(HttpServletRequest request){
		return judge("table_pushLocation_record",request);
	}
	
	/**
	 * 推送消息记录
	 * @return
	 */
	@RequestMapping(value="table_pushMessage_record")
	public ModelAndView table_pushMessage(HttpServletRequest request){
		return judge("table_pushMessage_record",request);
	}
	
	/**
	 * 消息管理
	 * @return
	 */
	@RequestMapping(value="table_message")
	public ModelAndView table_message(HttpServletRequest request){
		return judge("table_message",request);
	}
	/**
	 * 盒子管理
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_box")
	public ModelAndView table_box(HttpServletRequest request){ 
 		return judge("table_box",request);
	}
	
	@RequestMapping(value="table_box2")
	public ModelAndView table_box2(HttpServletRequest request,
			@RequestParam(required = false) String userName){
		ModelAndView mav = new ModelAndView();
		if(userName!=null){
			mav.addObject("boxname", userName); 
		} 
		mav.setViewName("table_box2");
		return mav;
 		//return judge("table_box",request);
	}
	
	/**
	 * 用户管理
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_user")
	public ModelAndView table_user(HttpServletRequest request){
		return judge("table_user",request);
	}
	
	/**
	 * 意见反馈
	 * @return
	 */
	@RequestMapping({"table_suggestion"})
	public ModelAndView table_suggestion(HttpServletRequest request)
	{
	    return judge("table_suggestion", request);
	}
	
	/**
	 * 盒子入库
	 * @param request
	 * @return
	 */
	@RequestMapping(value="boxStorage")
	public ModelAndView boxStorage(HttpServletRequest request){
		return judge("boxStorage",request);
	}
	
	/**
	 * 产品管理
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_product.html")
	public ModelAndView table_product(HttpServletRequest request){
		return judge("table_product",request);
	}
	/**
	 * 产品管理
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_returns.html")
	public ModelAndView table_returns(HttpServletRequest request){
		return judge("table_returns",request);
	}
	
	/**************************************************其它页面**************************************************/
	
	@RequestMapping(value="404")
	public ModelAndView error404(){
		return new ModelAndView("404");
	}
	
	@RequestMapping(value="500")
	public ModelAndView error500(){
		return new ModelAndView("500");
	}
	
	@RequestMapping(value="ie")
	public ModelAndView ie(){
		return new ModelAndView("ie");
	}
	
	/**
	 * 判断当前是否登录
	 * @param view
	 * @param request
	 * @return
	 */
	private ModelAndView judge(String view,HttpServletRequest request){
		String username = (String) request.getSession().getAttribute("username");
		String password = (String) request.getSession().getAttribute("password");
		if(username!=null&&password!=null){
			return new ModelAndView(view);//已登录，继续访问
		}else{
			return new ModelAndView("redirect:/index");//跳转登录页面
		}
	}
	
	/**
	 * Sdk用户下发
	 * @return
	 */
	@RequestMapping({"table_SdkUserSend"})
	public ModelAndView table_SdkUserSend(HttpServletRequest request)
	{
	    return judge("table_SdkUserSend", request);
	}
	
	/**
	 * 产品活动管理
	 * @return
	 */
	@RequestMapping({"table_activity"})
	public ModelAndView table_activity(HttpServletRequest request)
	{
	    return judge("table_activity", request);
	}
	
	/**
	 * 租用订单管理
	 * @return
	 */
	@RequestMapping(value="table_rentOrder")
	public ModelAndView table_rentOrder(HttpServletRequest request){
		return judge("table_rentOrder",request);
	}
	
	/**
	 * 实名制审核
	 * @return
	 */
	@RequestMapping(value="table_TheSystemAudit")
	public ModelAndView table_TheSystemAudit(HttpServletRequest request){
		return judge("table_TheSystemAudit",request);
	}
	
	/**
	 * 产品管理
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_rentReturns.html")
	public ModelAndView table_rentReturns(HttpServletRequest request){
		return judge("table_rentReturns",request);
	}
	
	/**
	 * 产品管理
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_referee.html")
	public ModelAndView table_referee(HttpServletRequest request){
		return judge("table_referee",request);
	}
	
	/**
	 * 产品管理
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_UserRegistration.html")
	public ModelAndView table_UserRegistration(HttpServletRequest request){
		return judge("table_UserRegistration",request);
	}
	
	/**
	 * 增票资质审核
	 * @return
	 */
	@RequestMapping(value="table_ticketCheck")
	public ModelAndView table_ticketCheck(HttpServletRequest request){
		return judge("table_ticketCheck",request);
	}
	
	/**
	 * 北斗码管理
	 * @return
	 */
	@RequestMapping(value="table_bdCood")
	public ModelAndView table_bdCood(HttpServletRequest request){
		return judge("table_bdCood",request);
	}
	
	/**
	 * 推荐人
	 * @return
	 */
	@RequestMapping(value="table_bdCoodUser")
	public ModelAndView table_bdCoodUser(HttpServletRequest request){
		return judge("table_bdCoodUser",request);
	}
	
	/**
	 * 北斗码管理详情页
	 * @return
	 */
	@RequestMapping(value="table_bdCood_detail.html")
	public ModelAndView table_bdCood(HttpServletRequest request,String refereespeople){
		ModelAndView mav = new ModelAndView(); 
		if(refereespeople!=null&&!refereespeople.equals("")){ 
			mav.addObject("refereesname", refereespeople);  
		 }
		mav.setViewName("table_bdCood_detail");
		return mav;
		//return judge("table_bdCood",request);
	}
	
	/**
	 * 用户注册记录页面
	 * @return
	 */
	@RequestMapping(value="table_userreglog.html")
	public ModelAndView userRegLog(HttpServletRequest request){
		return judge("table_userreglog",request);
	}
	
	/**
	 * 公告管理
	 * @return
	 */
	@RequestMapping(value="table_announcement.html")
	public ModelAndView table_announcement(HttpServletRequest request){
		return judge("table_announcement",request);
	}
	
	/**
	 * 统计用户注册数量页面
	 * @return
	 */
	@RequestMapping(value="statistic_userreg.html")
	public ModelAndView statistic_userreg(HttpServletRequest request){
		return judge("statistics_userReg",request);
	}
	/**
	 * 统计用户消息页面
	 * @return
	 */
	@RequestMapping(value="statistic_usermsg.html")
	public ModelAndView statistic_usermsg(HttpServletRequest request){
		return judge("statistics_userMsg",request);
	}
	/**
	 * 查询文件下载记录页面
	 * @return
	 */
	@RequestMapping(value="table_fileDownloadLog.html")
	public ModelAndView tableFileDownloadLog(HttpServletRequest request){
		return judge("table_fileDownloadLog",request);
	}
	/**
	 * 统计文件下载数量页面
	 * @return
	 */
	@RequestMapping(value="statistic_fileDownloadLog.html")
	public ModelAndView statisticFileDownloadLog(HttpServletRequest request){
		return judge("statistics_fileDownloadLog",request);
	}
	
	/**
	 * FAQ管理
	 * @return
	 */
	@RequestMapping(value="table_FAQ.html")
	public ModelAndView table_FAQ(HttpServletRequest request){
		return judge("table_FAQ",request);
	}
	
	/**
	 * 收件人信息管理
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_recipients.html")
	public ModelAndView recipients(HttpServletRequest request){
		return judge("table_recipients",request);
	}
	
	/**
	 * 企业用户管理页面
	 * @return
	 */
	@RequestMapping(value="table_entUser.html")
	public ModelAndView table_entuser(HttpServletRequest request){
		return judge("table_entUser",request);
	}
	/**
	 * 管理员通知管理
	 * @return
	 */
	@RequestMapping(value="table_Notification.html")
	public ModelAndView table_Notification(HttpServletRequest request){
		return judge("table_Notification",request);
	}
	 
	/**
	 * 客户通知管理
	 * @return
	 */
	@RequestMapping(value="table_customer.html")
	public ModelAndView table_customer(HttpServletRequest request){
		return judge("table_customer",request);
	}
	
	/**
 
	 * 用户提交的faq后台管理页面
	 * @return
	 */
	@RequestMapping(value="table_userquestion.html")
	public ModelAndView table_userquestion(HttpServletRequest request){
		return judge("table_userquestion",request);
	}
	/**
	 * 企业发送的消息
	 * @return
	 */
	@RequestMapping(value="table_entsendmsg.html")
	public ModelAndView table_entSendMsg(HttpServletRequest request){
		return judge("table_entSendMsg",request);
 	}
	
	/**
	 * 轨迹回放页面
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_cardlocreplay.html")
	public ModelAndView table_cardlocreplay(HttpServletRequest request){
		return judge("table_cardlocreplay",request);
	}
	
	/**
	 * 家人号码限制管理页面
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_phoneRestrict.html")
	public ModelAndView table_phoneRestrict(HttpServletRequest request){
		return judge("table_phoneRestrict",request);
	}
	
	/**
	 * 统计企业周使用数据页面
	 * @param request
	 * @return
	 */
	@RequestMapping(value="statistic_entWentData.html")
	public ModelAndView statistic_entWentData(HttpServletRequest request){
		return judge("statistic_entWentData",request);
	}
	/**
	 * 短信通知记录页面
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_messageRecord.html")
	public ModelAndView table_messageRecord(HttpServletRequest request){
		return judge("table_messageRecord",request);
	}
	
	/**
	 * 周销售统计页面
	 * @param request
	 * @return
	 */
	@RequestMapping(value="statistics_market.html")
	public ModelAndView statistics_market(HttpServletRequest request){
		return judge("statistics_market",request);
	}
	
	/**
	 * 保险类型管理页面
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_insuranceType.html")
	public ModelAndView table_insuranceType(HttpServletRequest request){
		return judge("table_insuranceType",request);
	}
	
	/**
	 * 保险产品管理页面
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_insurance.html")
	public ModelAndView table_Insurance(HttpServletRequest request){
		return judge("table_insurance",request);
	}
	
	/**
	 * 添加保险页面
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_addinsurance.html")
	public ModelAndView table_addinsurance(HttpServletRequest request){
		return judge("table_addinsurance",request);
	}
	
	/**
	 * 添加保险页面
	 * @param request
	 * @return
	 */
	@RequestMapping(value="table_insurancePlan.html")
	public ModelAndView table_insurancePlan(String id, HttpServletRequest request){
		String username = (String) request.getSession().getAttribute("username");
		String password = (String) request.getSession().getAttribute("password");
		if(username!=null&&password!=null){
			ModelAndView model = new ModelAndView();
			model.addObject("id", id);
			model.setViewName("table_insurancePlan");
			return model;
		}else{
			return new ModelAndView("redirect:/index");//跳转登录页面
		}
		
	}
}

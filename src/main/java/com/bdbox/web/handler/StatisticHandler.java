package com.bdbox.web.handler;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.service.EntStatDailyService;
import com.bdbox.service.EnterpriseUserService;
import com.bdbox.util.LogUtils;
import com.bdbox.util.RemindDateUtils;
import com.bdbox.web.dto.EntStatDailyDto;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;

/**
 * 统计控制器
 * @author jlj
 *
 */
@Controller
public class StatisticHandler {
	
	@Autowired
	private EntStatDailyService entStatDailyService;
	
	
	//统计企业周数据页面
	
	
	/**
	 * 分页统计企业周使用数量
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @param iColumns
	 * @param sEcho
	 * @param iSortCol_0
	 * @param sSortDir_0
	 * @param weekName
	 * @return
	 */
	@RequestMapping(value="statEntDate.do")
	@ResponseBody
	public ListParam statEntDate(@RequestParam int iDisplayStart,@RequestParam int iDisplayLength, 
			@RequestParam int iColumns,@RequestParam String sEcho, 
			@RequestParam Integer iSortCol_0,@RequestParam String sSortDir_0,
			@RequestParam(required=false) String weekName){
		//参数验证
		int page = iDisplayStart==0 ? 1:iDisplayStart / iDisplayLength +1;
		int pageSize = iDisplayLength;
		
		//获取周起始时间
		Calendar startTime = null;
		try {
			startTime = RemindDateUtils.getWeekStartTime(weekName);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LogUtils.logerror("分页统计企业周使用数量解析周名错误", e);
		}
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		if(startTime == null){
			return listParam;
		}
		
		//本周的最后一天
		Calendar entTime = (Calendar)startTime.clone();
		entTime.add(Calendar.DATE, 6);
		
		//查询
		List<EntStatDailyDto> dtos = entStatDailyService.statisticData(startTime, entTime, null, page, pageSize);
		//全部企业用户
		int count = entStatDailyService.count(startTime, entTime, null);
		//返回数据
		listParam.setAaData(dtos);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		return listParam;
	} 
	
	/**
	 * 分页查询企业周使用统计数量
	 * @param weekName
	 * @param entId
	 * @return
	 */
	@RequestMapping(value="getEntWeekStatDate.do")
	@ResponseBody
	public ObjectResult getEntWeekStatDate(
			@RequestParam(required=false) int page,
			@RequestParam(required=false) int pageSize,
			@RequestParam(required=false) String weekName){
		ObjectResult objectResult = new ObjectResult();
		//参数验证
		page = page==0?1:page;
		pageSize = pageSize==0?10:pageSize;
		//获取周起始时间
		Calendar startTime = null;
		try {
			startTime = RemindDateUtils.getWeekStartTime(weekName);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LogUtils.logerror("分页统计企业周使用数量解析周名错误", e);
		}
		if(startTime == null){
			objectResult.setMessage("weekName参数错误");
			objectResult.setStatus(ResultStatus.UNSUPPORTED);
			objectResult.setResult(null);
			return objectResult;
		}
		
		//本周的最后一天
		Calendar entTime = (Calendar)startTime.clone();
		entTime.add(Calendar.DATE, 6);
		
		//查询
		List<EntStatDailyDto> dtos = entStatDailyService.statisticData(startTime, entTime, null, page, pageSize);
		//全部企业用户
		int count = entStatDailyService.count(startTime, entTime, null);
		
		if(dtos==null || dtos.size()==0){
			objectResult.setMessage("无本周数据");
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setResult(dtos);
		}else{
			objectResult.setMessage(String.valueOf(count));
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setResult(dtos);
		}
		return objectResult;
	}
	
	
	/**
	 * 统计企业周使用数量
	 * @param weekName
	 * @param entId
	 * @return
	 */
	@RequestMapping(value="statEntWeekDate.do")
	@ResponseBody
	public ObjectResult statEntWeekDate(
			@RequestParam(required=false) String weekName,
			@RequestParam(required=false) long entId){
		
		return entStatDailyService.getByWeekName(weekName, entId);
	}
	
	
}

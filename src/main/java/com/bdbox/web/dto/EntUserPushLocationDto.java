package com.bdbox.web.dto;

/**
 * @author dezhi.zhang
 * @createTime 2017/01/20
 */
public class EntUserPushLocationDto {
	/**
	 * 主键ID
	 */
	private Long id;
	/**
	 * 企业用户账号
	 */
	private String entUser;
	/**
	 * 位置信息
	 */
	private String content;
	/**
	 * 推送次数
	 */
	private Integer count;
	/**
	 * 是否推送成功
	 */
	private Boolean isSuccess;
	/**
	 * 错误信息
	 */
	private String error;
	/**
	 * 推送时间
	 */
	private String createTime;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEntUser() {
		return entUser;
	}
	public void setEntUser(String entUser) {
		this.entUser = entUser;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Boolean getIsSuccess() {
		return isSuccess;
	}
	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}

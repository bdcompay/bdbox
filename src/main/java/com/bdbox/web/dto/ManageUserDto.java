package com.bdbox.web.dto;
/**
 * 该Dto是用户类，只用于管理平台
 * @author WF
 *
 */
public class ManageUserDto {

	private long id;
	/**
	 * 手机号
	 */
	private String username;
	/**
	 * 邮箱
	 */
	private String mail;
	/**
	 * 用户类型
	 */
	private String userType;
	/**
	 * 用户状态
	 */
	private String userStatusType;
	/**
	 * 创建时间
	 */
	private String createdTime;
	/**
	 * 最后登录时间
	 */
	private String updateTime;
	/**
	 * 真实姓名
	 */
	private String name;
	/**
	 * 出生日期
	 */
	private String birthDate;
	/**
	 * 性别
	 */
	private String sex;
	/**
	 * 居住地
	 */
	private String residence;
	/**
	 * 职业
	 */
	private String job;
	/**
	 * 爱好
	 */
	private String hobby;
	/**
	 * 用于表格功能
	 */
	private String manager;
	/**
	 * 是否启用网络数据转发服务
	 */
	private String isDsiEnable;
	/**
	 * 发送频度
	 */
	private String freq;
	/**
	 * 用户的权限Key，用户网络数据转发的验证
	 */
	private String userPowerKey;
	public String getBirthDate() {
		return birthDate;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public String getHobby() {
		return hobby;
	}
	public long getId() {
		return id;
	}
	public String getJob() {
		return job;
	}
	public String getMail() {
		return mail;
	}

	public String getManager() {
		return manager;
	}

	public String getName() {
		return name;
	}

	public String getResidence() {
		return residence;
	}

	public String getUpdateTime() {
		return updateTime;
	}
	public String getUsername() {
		return username;
	}
	public String getUserStatusType() {
		return userStatusType;
	}
	public String getUserType() {
		return userType;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public void setResidence(String residence) {
		this.residence = residence;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setUserStatusType(String userStatusType) {
		this.userStatusType = userStatusType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getIsDsiEnable() {
		return isDsiEnable;
	}
	public void setIsDsiEnable(String isDsiEnable) {
		this.isDsiEnable = isDsiEnable;
	}
	public String getFreq() {
		return freq;
	}
	public void setFreq(String freq) {
		this.freq = freq;
	}
	public String getUserPowerKey() {
		return userPowerKey;
	}
	public void setUserPowerKey(String userPowerKey) {
		this.userPowerKey = userPowerKey;
	}
	
}

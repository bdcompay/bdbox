package com.bdbox.web.dto;

import com.bdbox.entity.EnterpriseUser;

/**
 * 统计企业每日使用数量DTO
 * @author jlj
 *
 */
public class EntStatDailyDto {
	/**
	 * 开始时间
	 */
	private String startDateStr;
	/**
	 * 结束时间
	 */
	private String endDateStr;
	/**
	 * 发送数量
	 */
	private int boxSendNumber;
	/**
	 * 企业用户
	 */
	private EnterpriseUser enterpriseUser;
	
	public String getStartDateStr() {
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	public String getEndDateStr() {
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	public int getBoxSendNumber() {
		return boxSendNumber;
	}
	public void setBoxSendNumber(int boxSendNumber) {
		this.boxSendNumber = boxSendNumber;
	}
	public EnterpriseUser getEnterpriseUser() {
		return enterpriseUser;
	}
	public void setEnterpriseUser(EnterpriseUser enterpriseUser) {
		this.enterpriseUser = enterpriseUser;
	}
	@Override
	public String toString() {
		return "EntStatDailyDto [startDateStr=" + startDateStr
				+ ", endDateStr=" + endDateStr + ", boxSendNumber="
				+ boxSendNumber + ", enterpriseUser=" + enterpriseUser + "]";
	}
}

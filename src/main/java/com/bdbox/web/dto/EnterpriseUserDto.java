package com.bdbox.web.dto;


/**
 * 企业用户DTO
 * 
 * @ClassName: EnterpriseUserDto 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-3-11 上午10:58:34 
 * @version V5.0 
 */
public class EnterpriseUserDto {
	/**
	 * id
	 */
	private long id;
	/**
	 * 企业名称
	 */
	private String name;
	/**
	 * 企业执照代码
	 */
	private String orgCode;
	/**
	 * 联系人
	 */
	private String linkman;
	/**
	 * 联系电话
	 */
	private String phone;
	/**
	 * 下属盒子数量
	 */
	private int boxNum;
	/**
	 * 邮箱
	 */
	private String mail;
	/**
	 * 地址
	 */
	private String address;
	/**
	 * 创建时间
	 */
	private String createdTimeStr;
	/**
	 * 备注说明
	 */
	private String explain1;
	/**
	 * 是否启用网络数据转发服务
	 */
	private Boolean isDsiEnable = false;
	
	/**
	 * 用户的权限Key，用户网络数据转发的验证
	 */
	private String userPowerKey;
	
	/**
	 * 发送频度
	 */
	private int freq;
	
	/**
	 * 最新请求发送盒子消息时间
	 */
	private String lastSendBoxMessageTimeStr;
	/**
	 * 数据推送的URL接口
	 */
	private String pushURL;
	
	/******************************  分割线        *************************************/
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrgCode() {
		return orgCode;
	}
	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}
	public String getLinkman() {
		return linkman;
	}
	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCreatedTimeStr() {
		return createdTimeStr;
	}
	public void setCreatedTimeStr(String createdTimeStr) {
		this.createdTimeStr = createdTimeStr;
	}
	public String getExplain1() {
		return explain1;
	}
	public void setExplain1(String explain1) {
		this.explain1 = explain1;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public int getBoxNum() {
		return boxNum;
	}
	public void setBoxNum(int boxNum) {
		this.boxNum = boxNum;
	}
	public Boolean getIsDsiEnable() {
		return isDsiEnable;
	}
	public void setIsDsiEnable(Boolean isDsiEnable) {
		this.isDsiEnable = isDsiEnable;
	}
	public String getUserPowerKey() {
		return userPowerKey;
	}
	public void setUserPowerKey(String userPowerKey) {
		this.userPowerKey = userPowerKey;
	}
	public int getFreq() {
		return freq;
	}
	public void setFreq(int freq) {
		this.freq = freq;
	}
	public String getLastSendBoxMessageTimeStr() {
		return lastSendBoxMessageTimeStr;
	}
	public void setLastSendBoxMessageTimeStr(String lastSendBoxMessageTimeStr) {
		this.lastSendBoxMessageTimeStr = lastSendBoxMessageTimeStr;
	}
	public String getPushURL() {
		return pushURL;
	}
	public void setPushURL(String pushURL) {
		this.pushURL = pushURL;
	}
}

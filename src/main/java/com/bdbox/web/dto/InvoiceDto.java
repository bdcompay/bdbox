package com.bdbox.web.dto;

import com.bdbox.entity.Invoice;

public class InvoiceDto extends Invoice{

	private String createdTime;
	
	private String morendto;


	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getMorendto() {
		return morendto;
	}

	public void setMorendto(String morendto) {
		this.morendto = morendto;
	}
	
	
}

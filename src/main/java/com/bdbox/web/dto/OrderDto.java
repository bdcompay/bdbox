package com.bdbox.web.dto;

public class OrderDto
{
  private Long id;
  private String user;
  private String name;
  private String site;
  private String versions;
  private String explain;
  private String createdTime;
  private String buyTime;
  private String completeTime;
  private Long phone;
  private String color;
  private String orderNo;
  private String trade_no;
  private String email;
  private String productName;
  private String productUrl;
  private Double price;
  private Integer count;
  private String productType;
  private String manager;
  private String dealStatus;
  private String minImg;
  private String qrcodeTime;
  private String QRCode;
  private String handle;
  private String rentStartTime;
  private String rentEndTime;
  private String sumantecedent;
  private String numberdate;  //租用天数
  private String surplusdate; //剩余天数
  private String deduct ; //预扣租金
  private String transactionType;
  private String method;//计算方式
  private String leavemessage;//卖家留言
  private String numb;//下单天数和超期天数的总和,如果订单没超期则以下单天数为准
  private String expressNumber;//发货物流订单号
  private String productId;//产品id
  private Long invoice;//发票id
  private String companyName;//发票抬头
  private String invoiceType;//发票类型
  private String sehnyutaishu;//剩余归还的盒子台数
  private String expiretime;//到期时间
  private String summony;//合计租金
  private String renlnameorder; //是否是实名制订单
  private String bdcood;//北斗码（北斗券）
  private String bdcoodname;//北斗码（北斗券）推荐人
  private String zfType;
  private Double payAmount;



 
  public String getBuyTime()
  {
    return this.buyTime;
  }
  public String getColor() {
    return this.color;
  }

  public String getCompleteTime() {
    return this.completeTime;
  }
  public Integer getCount() {
    return this.count;
  }

  public String getCreatedTime() {
    return this.createdTime;
  }

  public String getDealStatus() {
    return this.dealStatus;
  }

  public String getEmail() {
    return this.email;
  }

  public String getExplain() {
    return this.explain;
  }

  public long getId() {
    return this.id.longValue();
  }

  public String getManager() {
    return this.manager;
  }

  public String getMinImg() {
    return this.minImg;
  }

  public String getName() {
    return this.name;
  }

  public String getOrderNo() {
    return this.orderNo;
  }

  public Long getPhone() {
    return this.phone;
  }

  public Double getPrice() {
    return this.price;
  }

  public String getProductName() {
    return this.productName;
  }

  public String getProductType() {
    return this.productType;
  }

  public String getProductUrl() {
    return this.productUrl;
  }

  public String getSite() {
    return this.site;
  }

  public String getTrade_no() {
    return this.trade_no;
  }

  public String getUser() {
    return this.user;
  }

  public String getVersions() {
    return this.versions;
  }

  public void setBuyTime(String buyTime) {
    this.buyTime = buyTime;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public void setCompleteTime(String completeTime) {
    this.completeTime = completeTime;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public void setCreatedTime(String createdTime) {
    this.createdTime = createdTime;
  }

  public void setDealStatus(String dealStatus) {
    this.dealStatus = dealStatus;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setExplain(String explain) {
    this.explain = explain;
  }

  public void setId(long id) {
    this.id = Long.valueOf(id);
  }

  public void setId(Long id) {
    this.id = id;
  }

  public void setManager(String manager) {
    this.manager = manager;
  }

  public void setMinImg(String minImg) {
    this.minImg = minImg;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public void setPhone(Long phone) {
    this.phone = phone;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public void setProductType(String productType) {
    this.productType = productType;
  }

  public void setProductUrl(String productUrl) {
    this.productUrl = productUrl;
  }

  public void setSite(String site) {
    this.site = site;
  }

  public void setTrade_no(String trade_no) {
    this.trade_no = trade_no;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public void setVersions(String versions) {
    this.versions = versions;
  }

  public String getQRCode() {
    return this.QRCode;
  }

  public void setQRCode(String QRCode) {
    this.QRCode = QRCode;
  }

  public String getQrcodeTime() {
    return this.qrcodeTime;
  }

  public void setQrcodeTime(String qrcodeTime) {
    this.qrcodeTime = qrcodeTime;
  }
  
  public String getHandle() {
		return handle;
	}
  
  public void setHandle(String handle) {
		this.handle = handle;
	}
	public String getRentStartTime() {
		return rentStartTime;
	}
	
	public void setRentStartTime(String rentStartTime) {
		this.rentStartTime = rentStartTime;
	}
	
	public String getRentEndTime() {
		return rentEndTime;
	}
	
	public void setRentEndTime(String rentEndTime) {
		this.rentEndTime = rentEndTime;
	}
	
	public String getSumantecedent() {
		return sumantecedent;
	}
	
	public void setSumantecedent(String sumantecedent) {
		this.sumantecedent = sumantecedent;
	}
	
	public String getNumberdate() {
		return numberdate;
	}
	
	public void setNumberdate(String numberdate) {
		this.numberdate = numberdate;
	}
	
	public String getTransactionType() {
		return transactionType;
	}
	
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getSurplusdate() {
		return surplusdate;
	}
	public void setSurplusdate(String surplusdate) {
		this.surplusdate = surplusdate;
	}
	public String getDeduct() {
		return deduct;
	}
	public void setDeduct(String deduct) {
		this.deduct = deduct;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getLeavemessage() {
		return leavemessage;
	}
	public void setLeavemessage(String leavemessage) {
		this.leavemessage = leavemessage;
	}
	public String getNumb() {
		return numb;
	}
	public void setNumb(String numb) {
		this.numb = numb;
	}
	public String getExpressNumber() {
		return expressNumber;
	}
	public void setExpressNumber(String expressNumber) {
		this.expressNumber = expressNumber;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public Long getInvoice() {
		return invoice;
	}
	public void setInvoice(Long invoice) {
		this.invoice = invoice;
	}
	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getSehnyutaishu() {
		return sehnyutaishu;
	}
	public void setSehnyutaishu(String sehnyutaishu) {
		this.sehnyutaishu = sehnyutaishu;
	}
	public String getExpiretime() {
		return expiretime;
	}
	public void setExpiretime(String expiretime) {
		this.expiretime = expiretime;
	}
	public String getSummony() {
		return summony;
	}
	public void setSummony(String summony) {
		this.summony = summony;
	}
	public String getRenlnameorder() {
		return renlnameorder;
	}
	public void setRenlnameorder(String renlnameorder) {
		this.renlnameorder = renlnameorder;
	}
	public String getBdcood() {
		return bdcood;
	}
	public void setBdcood(String bdcood) {
		this.bdcood = bdcood;
	}
	public String getBdcoodname() {
		return bdcoodname;
	}
	public void setBdcoodname(String bdcoodname) {
		this.bdcoodname = bdcoodname;
	}
	public String getZfType() {
		return zfType;
	}
	public void setZfType(String zfType) {
		this.zfType = zfType;
	}
	public Double getPayAmount() {
		return payAmount;
	}
	public void setPayAmount(Double payAmount) {
		this.payAmount = payAmount;
	}
}
package com.bdbox.web.dto;

/**
 * 企业用户发送消息的DTO对象
 * 
 * @ClassName: EntSendMsgDto 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-3-22 下午3:46:22 
 * @version V5.0 
 */
public class EntSendMsgDto {
	/**
	 * ID
	 */
	private long id;
	/**
	 * 接收盒子序列号
	 */
	private String toBoxSerialNumber;
	/**
	 * 消息内容
	 */
	private String content;
	/**
	 * 创建时间
	 */
	private String createdTimeStr;
	/**
	 * 所属企业的名称
	 */
	private String entUserName;
	
	/*******************************  分割线      ********************************/
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getToBoxSerialNumber() {
		return toBoxSerialNumber;
	}
	public void setToBoxSerialNumber(String toBoxSerialNumber) {
		this.toBoxSerialNumber = toBoxSerialNumber;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreatedTimeStr() {
		return createdTimeStr;
	}
	public void setCreatedTimeStr(String createdTimeStr) {
		this.createdTimeStr = createdTimeStr;
	}
	public String getEntUserName() {
		return entUserName;
	}
	public void setEntUserName(String entUserName) {
		this.entUserName = entUserName;
	}
}

package com.bdbox.web.dto;

public class UserSuggestionDto
{
  private long id;
  private String fromStr;
  private String content;
  private String createdTime;
  private String manager;
  private String username;
  private String phone;
	private String qq;


  public String getManager()
  {
    return this.manager;
  }

  public void setManager(String manager) {
    this.manager = manager;
  }

  public long getId() {
    return this.id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getFromStr() {
    return this.fromStr;
  }

  public void setFromStr(String fromStr) {
    this.fromStr = fromStr;
  }

  public String getContent() {
    return this.content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getCreatedTime() {
    return this.createdTime;
  }

  public void setCreatedTime(String createdTime) {
    this.createdTime = createdTime;
  }

  public String getUsername() {
		return username;
	}
	
  public void setUsername(String username) {
		this.username = username;
	}
	
   public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getQq() {
		return qq;
	}
	
	public void setQq(String qq) {
		this.qq = qq;
	}	
  
  
}
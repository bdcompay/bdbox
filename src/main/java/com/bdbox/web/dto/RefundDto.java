package com.bdbox.web.dto;

public class RefundDto {
	private Long id;
	private Long orderId;
	private Double price;
	private Integer count;
	private String applyTime;
	private String refundTime;
	private String explain;
	private String refundStatus;
	private String refundNo;
	private String orderNo;
	private String manager;
	private String returnType;


	public String getManager() {
		return this.manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public String getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getApplyTime() {
		return this.applyTime;
	}

	public Integer getCount() {
		return this.count;
	}

	public String getExplain() {
		return this.explain;
	}

	public Long getId() {
		return this.id;
	}

	public Long getOrderId() {
		return this.orderId;
	}

	public Double getPrice() {
		return this.price;
	}

	public String getRefundStatus() {
		return this.refundStatus;
	}

	public String getRefundTime() {
		return this.refundTime;
	}

	public void setApplyTime(String applyTime) {
		this.applyTime = applyTime;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}

	public void setRefundTime(String refundTime) {
		this.refundTime = refundTime;
	}

	public String getRefundNo() {
		return refundNo;
	}

	public void setRefundNo(String refundNo) {
		this.refundNo = refundNo;
	}

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}
	
	

}
package com.bdbox.web.dto;

public class ExpressDto
{
  private Long id;
  private Long orderId;
  private String expressName;
  private String expressNumber;
  private String expressStatus;
  private String expressMessage;
  private String sendTime;
  private String completionTime;

  public String getCompletionTime()
  {
    return this.completionTime;
  }
  public String getExpressMessage() {
    return this.expressMessage;
  }
  public String getExpressName() {
    return this.expressName;
  }
  public String getExpressNumber() {
    return this.expressNumber;
  }
  public String getExpressStatus() {
    return this.expressStatus;
  }
  public Long getId() {
    return this.id;
  }
  public Long getOrderId() {
    return this.orderId;
  }
  public String getSendTime() {
    return this.sendTime;
  }
  public void setCompletionTime(String completionTime) {
    this.completionTime = completionTime;
  }
  public void setExpressMessage(String expressMessage) {
    this.expressMessage = expressMessage;
  }
  public void setExpressName(String expressName) {
    this.expressName = expressName;
  }
  public void setExpressNumber(String expressNumber) {
    this.expressNumber = expressNumber;
  }
  public void setExpressStatus(String expressStatus) {
    this.expressStatus = expressStatus;
  }
  public void setId(Long id) {
    this.id = id;
  }
  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }
  public void setSendTime(String sendTime) {
    this.sendTime = sendTime;
  }
}
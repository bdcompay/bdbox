package com.bdbox.web.dto;


public class RentRecordDto {
	
	private Long id;
	
	/**
	 * 所属用户
	 */
	private String user;
	
	/**
	 * 押金
	 */
	private String antecedent;
	
	/**
	 * 租金
	 */
	private String rental;
	
	/**
	 * 总押金
	 */
	private String sumAntecedent;
	
	/**
	 * 总租金
	 */
	private String sumRental;
	
	/**
	 * 买家留言
	 */
	private String leaveMessage;
	
	/**
	 * 租用天数
	 */
	private String rentDays;
	
	/**
	 * 租用记录生成时间
	 */
	private String recordTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getAntecedent() {
		return antecedent;
	}

	public void setAntecedent(String antecedent) {
		this.antecedent = antecedent;
	}

	public String getRental() {
		return rental;
	}

	public void setRental(String rental) {
		this.rental = rental;
	}

	public String getSumAntecedent() {
		return sumAntecedent;
	}

	public void setSumAntecedent(String sumAntecedent) {
		this.sumAntecedent = sumAntecedent;
	}

	public String getSumRental() {
		return sumRental;
	}

	public void setSumRental(String sumRental) {
		this.sumRental = sumRental;
	}

	public String getLeaveMessage() {
		return leaveMessage;
	}

	public void setLeaveMessage(String leaveMessage) {
		this.leaveMessage = leaveMessage;
	}

	public String getRentDays() {
		return rentDays;
	}

	public void setRentDays(String rentDays) {
		this.rentDays = rentDays;
	}

	public String getRecordTime() {
		return recordTime;
	}

	public void setRecordTime(String recordTime) {
		this.recordTime = recordTime;
	}
	
}

package com.bdbox.web.dto;

import java.util.Calendar;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Index;

import com.bdbox.entity.User;

public class SdkUserSendDto {

	private long id;
	/**
	 * 消息ID
	 */
	private String msgId;
	/**
	 * 发送用户
	 */
	 
	private String fromUser;
	/**
	 * 接收盒子
	 */
 	private String boxSerialNumber;

	/**
	 * 数据内容
	 */
	private String content;

	/**
	 * 创建时间
	 */
 	private String createdTime;
	
	/**
	 * 发送时间
	 */
 	private String sendTime ;
	/**
	 * 是否需要收到回执
	 */
	private String isReceived;
	/**
	 * 是否需要也阅读回执
	 */
	private String isRead;
	/**
	 * 操作
	 */
	 private String manager;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	public String getFromUser() {
		return fromUser;
	}
	public void setFromUser(String fromUser) {
		this.fromUser = fromUser;
	}
	public String getBoxSerialNumber() {
		return boxSerialNumber;
	}
	public void setBoxSerialNumber(String boxSerialNumber) {
		this.boxSerialNumber = boxSerialNumber;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	public String getSendTime() {
		return sendTime;
	}
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}
	public String getIsReceived() {
		return isReceived;
	}
	public void setIsReceived(String isReceived) {
		this.isReceived = isReceived;
	}
	public String getIsRead() {
		return isRead;
	}
	public void setIsRead(String isRead) {
		this.isRead = isRead;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	
	
}

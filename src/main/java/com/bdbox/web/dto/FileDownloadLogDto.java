package com.bdbox.web.dto;

import java.util.Calendar;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.Index;

public class FileDownloadLogDto  {
	/**
	 * id
	 */
	private long id;
	/**
	 * 文件名
	 */
	private String filename;
	/**
	 * 下载时间
	 */
	private String downloadTimeStr;
	/**
	 * 状态
	 */
	private boolean status;
	/**
	 * 耗时
	 */
	private long timeConsumig;
	/**
	 * 下载ip
	 */
	private String ip;
	/**
	 * ip归属地
	 */
	private String ipArea;
	/**
	 * 创建时间
	 */
	private String createdtimeStr;
	/**
	 * 用户
	 */
	private String username;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getDownloadTimeStr() {
		return downloadTimeStr;
	}
	public void setDownloadTimeStr(String downloadTimeStr) {
		this.downloadTimeStr = downloadTimeStr;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public long getTimeConsumig() {
		return timeConsumig;
	}
	public void setTimeConsumig(long timeConsumig) {
		this.timeConsumig = timeConsumig;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getIpArea() {
		return ipArea;
	}
	public void setIpArea(String ipArea) {
		this.ipArea = ipArea;
	}
	public String getCreatedtimeStr() {
		return createdtimeStr;
	}
	public void setCreatedtimeStr(String createdtimeStr) {
		this.createdtimeStr = createdtimeStr;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}

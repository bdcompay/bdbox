package com.bdbox.web.dto;

import com.bdbox.entity.QualificationCheck;

public class QualificationCheckDto extends QualificationCheck{

	private String createdTime;

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
}

package com.bdbox.web.dto; 
 

public class ActivityDto {
 
	  /**
	   * id
	   */ 
	  private Long id; 
	  /**
	   * 活动类型
	   */ 
	  private String status;
	  
	  /**
	   * 抢购开始时间
	   */ 
	  private String startTime;
	  
	  /**
	   * 抢购结束时间
	   */
 	  private String endTime;
 	  /**
	   * 备注
	   */
 	  private String remark;
 	  /**
	   * 操作
	   */
 	  private String manager;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getManager() {
		return manager;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
 	  
 	  

}

package com.bdbox.wx.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Hashtable;
import sun.misc.BASE64Encoder;

public class QRCodeUtil {
    /**

     * 编码

     * @param contents

     * @param width

     * @param height

     * @param imgPath

     */

    public String encode(String contents, int width, int height, String imgPath) {

        String ok = "";
        Hashtable<Object, Object> hints = new Hashtable<Object, Object>();
        // 指定纠错等级
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        // 指定编码格式
        try {

            BitMatrix byteMatrix;
            byteMatrix = new MultiFormatWriter().encode(new String(contents.getBytes("UTF-8"),"iso-8859-1"), BarcodeFormat.QR_CODE, width, height);
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(byteMatrix, "png", bao);

            ok = Base64Code(bao.toByteArray());
//            createImage(ok);

        } catch (Exception e) {
            e.printStackTrace();

        }
        return ok;
    }

    public String  Base64Code(byte[] b) {
        BASE64Encoder encoder = new BASE64Encoder();
        String codeBase64 = "";
        StringBuilder pictureBuffer = new StringBuilder();
        pictureBuffer.append(encoder.encode(b));
        codeBase64 = pictureBuffer.toString();
        return codeBase64;
    }

    /**

     * @param args

     */

    public static void main(String[] args) {

        String imgPath = "/Users/ling/Downloads/ok.png";


        String contents = "weixin://wxpay/bizpayurl?pr=rqzctjJ";

        int width = 150, height = 150;

        QRCodeUtil handler = new QRCodeUtil();

        handler.encode(contents, width, height, imgPath);
        System.out.println("successful");

    }

    public static String getCode(String code_url){

        String imgPath = "/Users/ling/Downloads/ok.png";


        String contents = code_url;

        int width = 200, height = 200;

        QRCodeUtil handler = new QRCodeUtil();

        return handler.encode(contents, width, height, imgPath);
    }
}
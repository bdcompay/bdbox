package com.bdbox.wx.util;

import java.io.BufferedReader;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

/**
 * 放到servlet或者spring controller 里面使用
 * Created by ling on 15/7/5.
 */
public class NotifyUtil {
	
	public static String XMLSUCCEED =  "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
            + "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";

    public static String XMLFAIL = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
            + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";

    //判断是否支付成功，返回boolan值给控制层判断后作逻辑操作
    public static boolean isPaySucceed(HttpServletRequest request) throws Exception {

        String inputLine;
        String notityXml = "";

        try {
            while ((inputLine = request.getReader().readLine()) != null) {
                notityXml += inputLine;
            }
            request.getReader().close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map map = CommonUtil.xmlToArray(notityXml);


        if("SUCCESS".equals(map.get("result_code"))){
            return true;
        }else{
            return false;
        }
    }
}

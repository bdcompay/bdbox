package com.bdbox.wx.impl;

import com.bdbox.wx.api.WxAccessTokenService;
import com.bdbox.wx.bean.WxApiConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Map;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class WxAccessTokenServiceImpl
  implements WxAccessTokenService
{
  private WxApiConfig wxApiConfig;

  public void setWxApiConfig(WxApiConfig wxApiConfig)
  {
    this.wxApiConfig = wxApiConfig;
  }

  public String getToken()
  {
    return getToken(true);
  }

  public String getToken(boolean isCache)
  {
    WxAccessTokenServiceData data = WxAccessTokenServiceData.getInstance();
    if ((isCache) && 
      (0L != data.getExpires()) && (data.getExpires() > new Date().getTime())) {
      return data.getAccessToken();
    }

    CloseableHttpClient httpClient = HttpClients.createDefault();
    try {
      URI uri = new URIBuilder().setScheme("https").setHost("api.weixin.qq.com").setPath("/cgi-bin/token")
        .setParameter("grant_type", "client_credential").setParameter("appid", this.wxApiConfig.getAppid())
        .setParameter("secret", this.wxApiConfig.getSecret()).build();
      HttpGet httpGet = new HttpGet(uri);
      CloseableHttpResponse execute = httpClient.execute(httpGet);
      if (execute != null) {
        String content = EntityUtils.toString(execute.getEntity());
        Map map = (Map)new ObjectMapper().readValue(content, Map.class);
        if (map.get("errcode") == null) {
          data.setAccessToken((String)map.get("access_token"));
          int expires = ((Integer)map.get("expires_in")).intValue();
          data.setExpires(expires * 1000 + new Date().getTime());
        } else {
          System.out.println("获取微信 accessToken 时遇到了错误：" + new ObjectMapper().writeValueAsString(map));
        }
        System.out.println("成功获取 accessToken，" + new ObjectMapper().writeValueAsString(map));
        return data.getAccessToken();
      }
    } catch (URISyntaxException|IOException e) {
      System.out.println("An exception " + e + "has been throw in getToken" + "(" + isCache + ")");
    } finally {
      try {
        if (httpClient != null)
          httpClient.close();
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
    try
    {
      if (httpClient != null)
        httpClient.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }

    return data.getAccessToken();
  }
}
package com.bdbox.wx.impl;

public class WxAccessTokenServiceData
{
  private static WxAccessTokenServiceData ourInstance = new WxAccessTokenServiceData();
  private String accessToken;
  private long expires;

  public static WxAccessTokenServiceData getInstance()
  {
    return ourInstance;
  }

  public String getAccessToken()
  {
    return this.accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public long getExpires() {
    return this.expires;
  }

  public void setExpires(long expires) {
    this.expires = expires;
  }
}
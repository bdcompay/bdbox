package com.bdbox.wx.impl;

import com.bdbox.util.LogUtils;
import com.bdbox.wx.api.WxUnifiedOrder;
import com.bdbox.wx.bean.WxPayConf;
import com.bdbox.wx.util.CommonUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.net.ssl.SSLContext;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jdom.JDOMException;

public class WxUnifiedOrderImpl
  implements WxUnifiedOrder
{
  private WxPayConf wxPayConf;

  public void setWxPayConf(WxPayConf wxPayConf)
  {
    this.wxPayConf = wxPayConf;
  }

  public String getJsPayPackage(SortedMap<Object, Object> parameters)
  {
    parameters.put("appid", this.wxPayConf.getAppid());
    parameters.put("mch_id", this.wxPayConf.getMch_id());
    parameters.put("nonce_str", CommonUtil.createNoncestr());
    parameters.put("spbill_create_ip", this.wxPayConf.getSpbill_create_ip());
    parameters.put("notify_url", this.wxPayConf.getNotify_url());
    parameters.put("trade_type", "JSAPI");
    parameters.put("sign", CommonUtil.getSign("UTF-8", parameters, this.wxPayConf.getKey()));
    Map map = null;
    try {
      map = CommonUtil.xmlToArray(CommonUtil.httpsRequest(this.wxPayConf.getUNIFIED_ORDER_URL(), "POST", CommonUtil.arrayToXml(parameters)));
    } catch (JDOMException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    String packages = "prepay_id=" + map.get("prepay_id");

    SortedMap<Object, Object> finalPKG = new TreeMap();

    finalPKG.put("appId", this.wxPayConf.getAppid());
    finalPKG.put("timeStamp", String.valueOf(System.currentTimeMillis() / 1000L));
    finalPKG.put("nonceStr", parameters.get("nonce_str"));
    finalPKG.put("package", packages);
    finalPKG.put("signType", "MD5");

    String finalsign = CommonUtil.getSign("UTF-8", finalPKG, this.wxPayConf.getKey());

    String pkg = "\"appId\":\"" + this.wxPayConf.getAppid() + "\",\"timeStamp\":\"" + finalPKG.get("timeStamp") + 
      "\",\"nonceStr\":\"" + parameters.get("nonce_str") + "\",\"package\":\"" + 
      packages + "\",\"signType\" : \"MD5" + "\",\"paySign\":\"" + 
      finalsign + "\"";
    return pkg;
  }

  public String getDynamicCodeUrl(SortedMap<Object, Object> parameters)
  {
    parameters.put("appid", this.wxPayConf.getAppid());
    parameters.put("mch_id", this.wxPayConf.getMch_id());

    parameters.put("nonce_str", CommonUtil.createNoncestr());
    parameters.put("spbill_create_ip", this.wxPayConf.getSpbill_create_ip());
    parameters.put("notify_url", this.wxPayConf.getNotify_url());
    parameters.put("trade_type", "NATIVE");
    parameters.put("sign", CommonUtil.getSign("UTF-8", parameters, this.wxPayConf.getKey()));
    String code_url = CommonUtil.httpsRequest(this.wxPayConf.getUNIFIED_ORDER_URL(), "POST", CommonUtil.arrayToXml(parameters));
    LogUtils.loginfo(code_url);
    return code_url;
  }

	@Override
	public String queyOrder(String wxOrderNo,String trade_no) {
		SortedMap<Object, Object> parameters = new TreeMap();
	    if(wxOrderNo!=null && !wxOrderNo.equals("")){
	    	parameters.put("transaction_id", wxOrderNo);
	    }else{
	    	if(trade_no==null || trade_no.equals("")) return null;
	    	parameters.put("out_trade_no", trade_no);
	    }
	    parameters.put("appid", this.wxPayConf.getAppid());
	    parameters.put("mch_id", this.wxPayConf.getMch_id());
	    parameters.put("nonce_str", CommonUtil.createNoncestr());
	    parameters.put("sign", CommonUtil.getSign("UTF-8", parameters, this.wxPayConf.getKey()));
	    
	    long call_before = System.currentTimeMillis();
	    String result = CommonUtil.httpsRequest("https://api.mch.weixin.qq.com/pay/orderquery", "POST", CommonUtil.arrayToXml(parameters));
	    long call_after = System.currentTimeMillis();
	    long time = call_after-call_before;
	    //report(result,"https://api.mch.weixin.qq.com/pay/closeorder",time);
	    LogUtils.loginfo("查询订单响应时间: "+time+"毫秒,结果数据: "+result);
		return result;
	}

	@Override
	public String closeOrder(String trade_no) {
		SortedMap<Object, Object> parameters = new TreeMap();
		parameters.put("appid", this.wxPayConf.getAppid());
	    parameters.put("mch_id", this.wxPayConf.getMch_id());
	    parameters.put("out_trade_no", trade_no);
	    parameters.put("nonce_str", CommonUtil.createNoncestr());
	    parameters.put("sign", CommonUtil.getSign("UTF-8", parameters, this.wxPayConf.getKey()));
	    
	    long call_before = System.currentTimeMillis();
	    String result = CommonUtil.httpsRequest("https://api.mch.weixin.qq.com/pay/closeorder", "POST", CommonUtil.arrayToXml(parameters));
        long call_after = System.currentTimeMillis();
	    long time = call_after-call_before;
	    //report(result,"https://api.mch.weixin.qq.com/pay/closeorder",time);
	    LogUtils.loginfo("关闭订单响应时间: "+time+"毫秒,结果数据: "+result);
		return result;
	}
	
	@Override
	public String refund(String wxOrderNo, String out_trade_no,String refund_no,String total_fee,String refund_fee) {
		SortedMap<Object, Object> parameters = new TreeMap();
		parameters.put("appid", this.wxPayConf.getAppid());
	    parameters.put("mch_id", this.wxPayConf.getMch_id());
	    parameters.put("nonce_str", CommonUtil.createNoncestr());
	    if(wxOrderNo!=null && !wxOrderNo.equals("")){
	    	parameters.put("transaction_id", wxOrderNo);
	    }else if(out_trade_no!=null && !out_trade_no.equals("")){
	    	if(out_trade_no==null || out_trade_no.equals("")) return null;
	    	parameters.put("out_trade_no", out_trade_no);
	    }
	    parameters.put("out_refund_no", refund_no);
	    parameters.put("total_fee", total_fee);
	    parameters.put("refund_fee", refund_fee);
	    parameters.put("op_user_id", this.wxPayConf.getMch_id());
	    parameters.put("refund_account", "REFUND_SOURCE_RECHARGE_FUNDS");
	    
	    parameters.put("sign", CommonUtil.getSign("UTF-8", parameters, this.wxPayConf.getKey()));
	    
		String result = null;
	    try{
		    //指定读取证书格式为PKCS12
		    KeyStore keyStore = KeyStore.getInstance("PKCS12");
		    //读取本机存放的PKCS12证书文件
		    FileInputStream instream = new FileInputStream(new File(wxPayConf.getCertLocalPath()));
		    try {
			    //指定PKCS12的密码(商户ID)
			    keyStore.load(instream, this.wxPayConf.getMch_id().toCharArray());
		    } finally {
		    	instream.close();
		    }
		    SSLContext sslcontext = SSLContexts.custom()
		    .loadKeyMaterial(keyStore, this.wxPayConf.getMch_id().toCharArray()).build();
		    //指定TLS版本
		    SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
		    sslcontext,new String[] { "TLSv1" },null,
		    SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
		    
		    //设置httpclient的SSLSocketFactory
		    CloseableHttpClient httpclient = HttpClients.custom()
		    .setSSLSocketFactory(sslsf)
		    .build();
		    
		    try {
	            HttpPost httpPost = new HttpPost("https://api.mch.weixin.qq.com/secapi/pay/refund");
	            
	            //得指明使用UTF-8编码，否则到API服务器XML的中文不能被成功识别
	            StringEntity postEntity = new StringEntity(CommonUtil.arrayToXml(parameters), "UTF-8");
	            httpPost.addHeader("Content-Type", "text/xml");
	            httpPost.setEntity(postEntity);

	            //System.out.println("executing request " + httpPost.getRequestLine());

	            long call_before = System.currentTimeMillis();
	            CloseableHttpResponse response =   httpclient.execute(httpPost);
	            long call_after = System.currentTimeMillis();
	    	    long time = call_after-call_before;
	    	    //report(result,"https://api.mch.weixin.qq.com/secapi/pay/refund",time);
	            
	            try {
	                HttpEntity entity = response.getEntity();
	                if (entity != null) {
	                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(entity.getContent()));
	                    String text;
	                    StringBuffer buffer = new StringBuffer();
	                    while ((text = bufferedReader.readLine()) != null) {
	                    	buffer.append(text);
	                    }
	                    result = buffer.toString();
	                }
	                LogUtils.loginfo("申请退款响应时间: "+time+"毫秒,结果数据: "+result);
	                EntityUtils.consume(entity);
	            } finally {
	                response.close();
	            }
	        } finally {
	            httpclient.close();
	        }
		   
			return result;
	    }catch(Exception e){
	    	LogUtils.logerror("微信退款异常", e);
	    	return null;
	    }
	}
	
	@Override
	public String refundQuery(String wxOrderNo,String out_trade_no,String out_refund_no,String refund_id) {
		SortedMap<Object, Object> parameters = new TreeMap();
		if(wxOrderNo!=null && !wxOrderNo.equals("")){
	    	parameters.put("transaction_id", wxOrderNo);
	    }else if(out_trade_no!=null && !out_trade_no.equals("")){
	    	parameters.put("out_trade_no", out_trade_no);
	    }else if(out_refund_no!=null && !out_refund_no.equals("")){
	    	parameters.put("out_refund_no", out_refund_no);
	    }else{
	    	if(out_refund_no==null || out_refund_no.equals("")) return null;
	    	parameters.put("refund_id", refund_id);
	    }
		parameters.put("appid", this.wxPayConf.getAppid());
	    parameters.put("mch_id", this.wxPayConf.getMch_id());
	    parameters.put("nonce_str", CommonUtil.createNoncestr());
	    parameters.put("sign", CommonUtil.getSign("UTF-8", parameters, this.wxPayConf.getKey()));
	    
	    long call_before = System.currentTimeMillis();
	    String result = CommonUtil.httpsRequest("https://api.mch.weixin.qq.com/pay/refundquery", "POST", CommonUtil.arrayToXml(parameters));
	    long call_after = System.currentTimeMillis();
	    long time = call_after-call_before;
	    //report(result,"https://api.mch.weixin.qq.com/pay/refundquery",time);
	    LogUtils.loginfo("查询退款响应时间: "+time+"毫秒,结果数据: "+result);
		return result;
	}

	@Override
	public String downloadBill(Calendar bill_date, String bill_type) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String bill_dateStr = sdf.format(bill_date.getTime());
		
		SortedMap<Object, Object> parameters = new TreeMap();
		parameters.put("appid", this.wxPayConf.getAppid());
	    parameters.put("mch_id", this.wxPayConf.getMch_id());
	    parameters.put("bill_date", bill_dateStr);
	    parameters.put("bill_type", bill_type);
	    parameters.put("nonce_str", CommonUtil.createNoncestr());
	    parameters.put("sign", CommonUtil.getSign("UTF-8", parameters, this.wxPayConf.getKey()));
	    
	    long call_before = System.currentTimeMillis();
	    String result = CommonUtil.httpsRequest("https://api.mch.weixin.qq.com/pay/downloadbill", "POST", CommonUtil.arrayToXml(parameters));
	    long call_after = System.currentTimeMillis();
	    long time = call_after-call_before;
	    //report(result,"https://api.mch.weixin.qq.com/pay/downloadbill",time);
	    LogUtils.loginfo("下载对账单响应时间: "+time+"毫秒,结果数据: "+result);
		return result;
	}

	@Override
	public String report(String resultXml,String url,long time) {
		SortedMap<Object, Object> parameters = new TreeMap();
		parameters.put("appid", this.wxPayConf.getAppid());
	    parameters.put("mch_id", this.wxPayConf.getMch_id());
	    parameters.put("nonce_str", CommonUtil.createNoncestr());
	    parameters.put("interface_url", url);
	    parameters.put("execute_time_", String.valueOf(time));
	    try {
			Map map = CommonUtil.xmlToArray(resultXml);
			parameters.put("return_code", map.get("return_code"));
		    parameters.put("return_msg", map.get("return_msg"));
		    parameters.put("result_code", map.get("result_code"));
		    //parameters.put("out_trade_no", map.get("out_trade_no"));
		} catch (JDOMException e) {
			LogUtils.logerror("XML解析JDOM异常", e);
		} catch (IOException e) {
			LogUtils.logerror("XML解析IO异常", e);
		}
	    parameters.put("sign", CommonUtil.getSign("UTF-8", parameters, this.wxPayConf.getKey()));
	    
	    long call_before = System.currentTimeMillis();
	    String result = CommonUtil.httpsRequest("https://api.mch.weixin.qq.com/payitil/report", "POST", CommonUtil.arrayToXml(parameters));
	    long call_after = System.currentTimeMillis();
		long time2 = call_after-call_before;
	    LogUtils.loginfo("测速上报响应时间: "+time2+"毫秒,结果数据: "+result);
		return null;
	}

	@Override
	public String shorturl(String long_url) {
		//请求参数
		SortedMap<Object, Object> parameters = new TreeMap();
		parameters.put("appid", this.wxPayConf.getAppid());
	    parameters.put("mch_id", this.wxPayConf.getMch_id());
	    parameters.put("nonce_str", CommonUtil.createNoncestr());
	    parameters.put("long_url", long_url);
	    parameters.put("sign", CommonUtil.getSign("UTF-8", parameters, this.wxPayConf.getKey()));
	    
	    long call_before = System.currentTimeMillis();
	    String result = CommonUtil.httpsRequest("https://api.mch.weixin.qq.com/tools/shorturl", "POST", CommonUtil.arrayToXml(parameters));
	    long call_after = System.currentTimeMillis();
		long time = call_after-call_before;
		//report(result,"https://api.mch.weixin.qq.com/tools/shorturl",time);
	    LogUtils.loginfo("转换短链接响应时间: "+time+"毫秒,结果数据: "+result);
	    
	    LogUtils.loginfo("长url:"+long_url);
	    try {
			Map map = CommonUtil.xmlToArray(result);
			if(map.get("return_code").toString().equals("SUCCESS")){
				String shortUrl =  map.get("short_url").toString();
				LogUtils.loginfo("短url:"+shortUrl);
				return shortUrl;
			}
			String return_msg =  map.get("return_msg").toString();
			LogUtils.logdebug("转换短链接失败，失败原因："+return_msg, null);
		} catch (JDOMException e) {
			LogUtils.logerror("XML解析JDOM异常", e);
		} catch (IOException e) {
			LogUtils.logerror("XML解析IO异常", e);
		}
		return null;
	}
	
	/**
	 * 获取prepay_id
	 * @param parameters
	 * @return
	 */
	public String getPrepayid(SortedMap<Object, Object> parameters){
		String prepay_id = "";
		
		parameters.put("appid", this.wxPayConf.getAppid());
	    parameters.put("mch_id", this.wxPayConf.getMch_id());

	    parameters.put("nonce_str", CommonUtil.createNoncestr());
	    parameters.put("spbill_create_ip", this.wxPayConf.getSpbill_create_ip());
	    parameters.put("notify_url", this.wxPayConf.getNotify_url());
	    parameters.put("trade_type", "JSAPI");
	    parameters.put("sign", CommonUtil.getSign("UTF-8", parameters, this.wxPayConf.getKey()));
	    
	    Map map = null;
		try {
			map = CommonUtil.xmlToArray(CommonUtil.httpsRequest(this.wxPayConf.getUNIFIED_ORDER_URL(), "POST", CommonUtil.arrayToXml(parameters)));
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    if(map.get("return_code").equals("SUCCESS")&&map.get("result_code").equals("SUCCESS")){  
	    	prepay_id = map.get("prepay_id").toString();//这就是预支付id  
	    	LogUtils.loginfo("微信发起预支付订单成功");
        }else{
        	LogUtils.loginfo("微信发起预支付订单失败："+map.get("return_msg") +"，错误代码："+map.get("err_code")+"，错误信息描述："+map.get("err_code_des"));
        }
	    return prepay_id;
	}
}

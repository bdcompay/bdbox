package com.bdbox.wx.impl;

import com.bdbox.wx.api.WxAccessTokenService;
import com.bdbox.wx.api.WxFilterIPService;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class WxFilterIPServiceImpl
  implements WxFilterIPService
{
  private WxAccessTokenService wxAccessTokenService;

  public void setWxAccessTokenService(WxAccessTokenService wxAccessTokenService)
  {
    this.wxAccessTokenService = wxAccessTokenService;
  }

  public boolean isFormWx(String IP)
  {
    CloseableHttpClient httpClient = HttpClients.createDefault();
    try {
      URI uri = new URIBuilder().setScheme("https").setHost("api.weixin.qq.com").setPath("/cgi-bin/getcallbackip")
        .setParameter("access_token", this.wxAccessTokenService.getToken()).build();
      HttpGet httpGet = new HttpGet(uri);
      CloseableHttpResponse execute = httpClient.execute(httpGet);
      String pipei = "";
      if (execute != null) {
        pipei = EntityUtils.toString(execute.getEntity());
      }
      if (pipei.contains(IP))
        return true;
    }
    catch (URISyntaxException|IOException e)
    {
      System.out.println("An exception " + e + "has been throw in getcallbackip" + "(" + e + ")");
    } finally {
      try {
        if (httpClient != null)
          httpClient.close();
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
    try
    {
      if (httpClient != null)
        httpClient.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }

    return false;
  }
}
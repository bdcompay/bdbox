package com.bdbox.wx.api;

/**
 * Created by ling on 15/6/5.
 */
public interface WxAccessTokenService {
    /**
     * access_token是公众号的全局唯一票据，公众号调用各接口时都需使用access_token。正常情况下access_token有效期为7200秒，
     * 重复获取将导致上次获取的access_token失效。由于获取access_token的api调用次数非常有限，建议开发者全局存储与更新access_token，
     * 频繁刷新access_token会导致api调用受限，影响自身业务。
     * <p/>
     * 默认需要实现缓存
     *
     * @return
     */
    String getToken();

    /**
     * @param isCache 是否从缓存中直接取数据。传入 false 一旦成功获取TOKEN，将覆盖原有缓存。
     * @return
     */
    String getToken(boolean isCache);

}
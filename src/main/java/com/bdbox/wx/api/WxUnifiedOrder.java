package com.bdbox.wx.api;

import java.util.Calendar;
import java.util.SortedMap;

/**
*
* 统一支付接口
* Created by ling on 15/7/3.
*/
public interface WxUnifiedOrder {

   /**
    * h5页面支付需要的package
    * @return
    */
   String getJsPayPackage(SortedMap<Object, Object> parameters);


   /**
    * 扫码支付模式二，动态二维码
    * @param parameters
    * @return
    */
   String getDynamicCodeUrl(SortedMap<Object, Object> parameters);
   
   /**
    * 查询微信订单
    * 	微信订单号与商户订单号使用时二选一，微信订单号优先使用
    * 	该接口提供所有微信支付订单的查询，商户可以通过该接口主动查询订单状态，完成下一步的业务逻辑。
    * 需要调用查询接口的情况：
    * ◆ 当商户后台、网络、服务器等出现异常，商户系统最终未接收到支付通知；
    * ◆ 调用支付接口后，返回系统错误或未知交易状态情况；
    * ◆ 调用被扫支付API，返回USERPAYING的状态；
    * ◆ 调用关单或撤销接口API之前，需确认支付状态；
    * @param wxOrderNo
    * 		微信订单号
    * @param trade_no
    * 		商户订单号
    * @return
    * 		xml
    */
   String queyOrder(String wxOrderNo,String trade_no);
   
   /**
    * 关闭微信订单
    * 	以下情况需要调用关单接口：商户订单支付失败需要生成新单号重新发起支付，
    * 要对原订单号调用关单，避免重复支付；系统下单后，用户支付超时，系统退出不再受理，
    * 避免用户继续，请调用关单接口。
    * 	注意：订单生成后不能马上调用关单接口，最短调用时间间隔为5分钟。
    * @param trade_no
    * 		商户订单号
    * @return
    * 		xml
    */
   String closeOrder(String trade_no);
   
   /**
    * 申请退款
    * 	当交易发生之后一段时间内，由于买家或者卖家的原因需要退款时，
    * 卖家可以通过退款接口将支付款退还给买家，微信支付将在收到退款请
    * 求并且验证成功之后，按照退款规则将支付款按原路退到买家帐号上。
    * 注意：
    * 	1、交易时间超过一年的订单无法提交退款；
    * 	2、微信支付退款支持单笔交易分多次退款，多次退款需要提交原支付
    * 订单的商户订单号和设置不同的退款单号。一笔退款失败后重新提交，要采
    * 用原来的退款单号。总退款金额不能超过用户实际支付金额。
    * 	微信订单号与商户订单号使用时二选一，微信订单号优先使用
    * @param wxOrderNo
    * 		微信订单号
    * @param out_trade_no
    * 		商户订单号
    * @param refund_no
    * 		商户退款单号
    * @param total_fee
    * 		总金额
    * @param refund_fee
    * 		退款金额
    * @return
    */
   String refund(String wxOrderNo,String out_trade_no,String refund_no,String total_fee,String refund_fee);
   
   /**
    * 微信退款查询
    * 	提交退款申请后，通过调用该接口查询退款状态。退款有一定延时，用零钱支付的退款20分钟内到账，银行卡支付的退款3个工作日后重新查询退款状态
    * 	微信订单号、商户订单号、商户退款单号、微信退款单号四选一，
    * 	优先顺序 1.微信订单号、2.商户订单号、3.商户退款单号、4.微信退款单
    * @param wxOrderNo
    * 		微信订单号
    * @param out_trade_no
    * 		商户订单号
    * @param out_refund_no
    * 		商户退款单号
    * @param refund_id
    * 		微信退款单号
    * @return
    * 		xml
    */
   String refundQuery(String wxOrderNo,String out_trade_no,String out_refund_no,String refund_id);
   
   /**
    * 下载对账单
    * @param bill_date
    * 		对账单日期
    * @param bill_type
    * 		账单类型
    * @return
    */
   String downloadBill(Calendar bill_date,String bill_type);
   
   /**
    * 测速上报
    * @param resultXml
    * 		调用接口的回复数据
    * @param time
    * 		调用接口所需的时间
    * @return
    */
   String report(String resultXml,String url,long time);
   
   /**
    * 转换短链接
    * 	该接口主要用于扫码原生支付模式一中的二维码链接转成短链接(weixin://wxpay/s/XXXXXX)，
    * 	减小二维码数据量，提升扫描速度和精确度。
    * @param long_url
    * 		长链接
    * @return
    */
   String shorturl(String long_url);
   
   /**
    * 获取预支付id
    * @param parameters
    * @return
    */
   String getPrepayid(SortedMap<Object, Object> parameters);

}

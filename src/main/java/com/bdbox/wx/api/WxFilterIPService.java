package com.bdbox.wx.api;

/**
 * IP地址过滤
 * Created by ling on 15/7/15.
 */
public interface WxFilterIPService {

    /**
     * 判断请求是否来自微信服务器
     * @param IP
     * @return
     */
    public boolean isFormWx(String IP);

}
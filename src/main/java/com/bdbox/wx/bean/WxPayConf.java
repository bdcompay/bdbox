package com.bdbox.wx.bean;

public class WxPayConf
{
  private String appid;
  private String mch_id;
  private String key;
  private String spbill_create_ip;
  private String notify_url;
  private String UNIFIED_ORDER_URL;
  private String CHECK_ORDER_URL;
  /**
   * 证书存放路径
   */
  private String certLocalPath;

  public String getAppid()
  {
    return this.appid;
  }

  public void setAppid(String appid) {
    this.appid = appid;
  }

  public String getMch_id() {
    return this.mch_id;
  }

  public void setMch_id(String mch_id) {
    this.mch_id = mch_id;
  }

  public String getKey() {
    return this.key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getSpbill_create_ip() {
    return this.spbill_create_ip;
  }

  public void setSpbill_create_ip(String spbill_create_ip) {
    this.spbill_create_ip = spbill_create_ip;
  }

  public String getNotify_url() {
    return this.notify_url;
  }

  public void setNotify_url(String notify_url) {
    this.notify_url = notify_url;
  }

  public String getUNIFIED_ORDER_URL()
  {
    return this.UNIFIED_ORDER_URL;
  }

  public void setUNIFIED_ORDER_URL(String UNIFIED_ORDER_URL) {
    this.UNIFIED_ORDER_URL = UNIFIED_ORDER_URL;
  }

  public String getCHECK_ORDER_URL() {
    return this.CHECK_ORDER_URL;
  }

  public void setCHECK_ORDER_URL(String CHECK_ORDER_URL) {
    this.CHECK_ORDER_URL = CHECK_ORDER_URL;
  }

	public String getCertLocalPath() {
		return certLocalPath;
	}
	
	public void setCertLocalPath(String certLocalPath) {
		this.certLocalPath = certLocalPath;
	}
  
  
}
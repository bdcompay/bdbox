package com.bdbox.wx.bean;

public class WxApiConfig
{
  private String appid;
  private String secret;
  private String token;

  public String getAppid()
  {
    return this.appid;
  }

  public void setAppid(String appid) {
    this.appid = appid;
  }

  public String getSecret() {
    return this.secret;
  }

  public void setSecret(String secret) {
    this.secret = secret;
  }

  public String getToken() {
    return this.token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}
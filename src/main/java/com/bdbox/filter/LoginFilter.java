package com.bdbox.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 访问拦截
 * 
 * 该过滤器用于拦截访问，访问任意页面时判断是否已经登录。
 * 如果登录则继续往下执行，反之返回登录页面。
 * 只过滤请求中包含back_属于后台管理系统页面的uri，其它则不执行过滤
 * 
 * @author WF
 *
 */
public class LoginFilter implements Filter{

	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		HttpSession nowSession = ((HttpServletRequest) arg0).getSession();
		//获取请求Uri，判断地址是否包含back_	，若包含执行，反之不执行。
		String requestUri = ((HttpServletRequest) arg0).getRequestURI();
		if(!requestUri.contains("login")){
	        // 从session 里面获取当前用户名的信息  
	        Object obj = nowSession.getAttribute("username");  
	        // 判断如果没有取到用户信息，就跳转到登陆页面，提示用户进行登陆  
	            if (obj == null || "".equals(obj.toString())) {
	            	//判断用户如果不是访问登录页面，则重定向到登录页面
	            	if (!((HttpServletRequest) arg0).getServletPath().equals("/index.html")) {
	            			((HttpServletResponse) arg1).sendRedirect("index.html");
	            	}else{
	            		arg2.doFilter(arg0, arg1);
	            	}
	            }else{
	            	arg2.doFilter(arg0, arg1);
	            }
		}else{
			arg2.doFilter(arg0, arg1);
		}
	}

	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}

package com.bdbox.control;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bdsdk.util.CommonMethod;
import com.qizhi.dsi.DsiClient;
import com.qizhi.dto.CardLocationDto;
import com.qizhi.dto.CardMessageDto;
import com.qizhi.dto.CardReceiptDto;
import com.qizhi.dto.SmsMessageDto;
import com.qizhi.listener.BdDataListener;

/**
 * 北斗数据打包成数据库实体并持久化到数据库，本类作为被观察者，数据打包成功后，通知其观察者进行下一步处理
 * 
 * @author will.yan
 * 
 */
@Component
public class ControlDsi extends DsiClient implements BdDataListener {
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger fordebug = Logger.getLogger("fordebug");
	private static Logger forinfo = Logger.getLogger("forinfo");
	@Value("${metaq.topic}")
	private String metaqTopic;
	@Value("${metaq.isGetBefore}")
	private Boolean isGetBefore;

	@Autowired
	private ControlBd controlBd;
	@Autowired
	private ControlSms controlSms;

	@PostConstruct
	public void init() {
		try {
			if (!super.createConnect(metaqTopic, isGetBefore,null,"http://123.57.173.91/bdcbus/",this)) {
				forerror.error("连接北斗平台失败");
			} else {
				forinfo.info("成功连接北斗平台");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			forerror.error("连接北斗平台失败，详情：" + CommonMethod.getTrace(e));
		}
	}

	public void processCardMessage(CardMessageDto cardMessageDto) {
		// TODO Auto-generated method stub
		controlBd.processCardMessage(cardMessageDto);
	}

	public void processCardLocation(CardLocationDto cardLocationDto) {
		// TODO Auto-generated method stub
		controlBd.processCardLocation(cardLocationDto);
	}

	public void processCardReceipt(CardReceiptDto cardReceiptDto) {
		// TODO Auto-generated method stub
		controlBd.processCardReceipt(cardReceiptDto);
	}

	public void processSmsMessage(SmsMessageDto smsMessageDto) {
		// TODO Auto-generated method stub
		controlSms.processSmsMessage(smsMessageDto);
	}

}

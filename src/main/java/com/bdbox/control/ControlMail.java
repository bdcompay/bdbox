package com.bdbox.control;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bdbox.biz.MessageControl;
import com.bdbox.constant.MsgType;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxMessage;
import com.bdbox.service.FamilyStatusService;
import com.bdbox.service.UserService;
import com.bdbox.util.MailUtil;
import com.bdsdk.util.CommonMethod;

/**
 * 邮件转发
 * 
 * @author will.yan
 */
@Component
public class ControlMail {

	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");

	@Value("${email.emailImap}")
	private String emailImap;
	@Value("${email.emailSmtp}")
	private String emailSmtp;
	@Value("${email.emailUsername}")
	private String emailUsername;
	@Value("${email.emailPassword}")
	private String emailPassword;
	@Autowired
	private FamilyStatusService familyStatusService;
	@Autowired
	private UserService userService;
	private static Integer mailToBdmsgId = 1;
	@Autowired
	private MessageControl messageControl;

	public void sendMailErrorMessage(String toAddress, String subject,
			String content) {
		MailUtil.sendMail(emailSmtp, emailUsername, emailUsername,
				emailPassword, toAddress, subject, content);
	}

	public void sendMailBoxMessage(BoxMessage boxMessage) throws Exception {

		// 获取发送者姓名
		Box fromBox = boxMessage.getFromBox();
		String name = fromBox.getName() + "(" + fromBox.getBoxSerialNumber()
				+ ")";
		// 根据消息的内容类型，生成发送给SMS的短信内容
		String content = name + boxMessage.getContent();
		String messgeTypeStr;
		if (boxMessage.getMsgType() == MsgType.MSG_SOS) {
			messgeTypeStr = "报警信息";

		} else {
			messgeTypeStr = "报平安信息";
		}
		String subject = "北斗盒子-收到来自" + fromBox.getName() + "的" + messgeTypeStr;

		String tinyUrl = null;
		try {
			if (boxMessage.getLongitude() != null) {
				tinyUrl = CommonMethod
						.LongUrlToShortUrl("http://api.map.baidu.com/marker?location="
								+ boxMessage.getLatitude()
								+ ","
								+ boxMessage.getLongitude()
								+ "&title=我的位置&content="
								+ "&output=html&src=beidou&coord_type=wgs84");
				if (tinyUrl == null) {
					forerror.error("发送消息到家人邮箱时，位置转换为短网址失败");
					return;
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			forerror.error("发送位置到家人邮箱时，位置转换为短网址失败：" + CommonMethod.getTrace(e));
			tinyUrl = null;
		}
		content = "<style class=\"fox_global_style\"> "
				+ "div.fox_html_content { line-height: 1.5;} "
				+ "blockquote { margin-Top: 0px; margin-Bottom: 0px; margin-Left: 0.5em } ol { margin-Top: 0px; margin-Bottom: 0px } ul { margin-Top: 0px; margin-Bottom: 0px } p { margin-Top: 0px; margin-Bottom: 0px } </style> "
				+ "<div><span></span></div><blockquote style=\"margin-top: 0px; margin-bottom: 0px; margin-left: 0.5em;\">"
				+ "<div><div style=\"background-color:white\" class=\"FoxDiv20150601164356724861\" id=\"FMOriginalContent\">"
				+ "<div><div> &nbsp; &nbsp; &nbsp; &nbsp;<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"640\" style=\"margin:0 auto;color:#555; font:16px/26px '微软雅黑','宋体',Arail; \">"
				+ "<tbody><tr><td style=\"height:62px; background-color:#125890; padding:10px 0 10px 152px;\">"
				+ "<a href=\"http://www.beidouhezi.com\" target=\"_blank\"><img src=\"http://www.beidouhezi.com/beidouhezi/img/01/1651516131313.png\" width=\"350\" height=\"60\" style=\"border:none; margin:0 auto;\"></a> </td></tr>"
				+ "<tr style=\"background-color:#fff;\"><td style=\"padding:30px 38px;\">"
				+ "<div style=\"font-size:14px;\"> 亲爱的 <span style=\"color:#019875;\">"
				+ boxMessage.getFamilyMail()
				+ "</span>，您好! "
				+ "<br>您收到来自北斗盒子的"
				+ messgeTypeStr
				+ "。</div>&nbsp;"
				+ "<div style=\"border-radius: 10px;box-shadow: 2px 2px 1px #ececec;margin:15px 0; background-color:#f8f8f8; border:#dddddd 1px solid; padding:10px 15px; position:relative;\"> "
				+ "<font color=\"#205081\" style=\"font-size: 15px;\">"
				+ name
				+ ":</font> "
				+ "<font style=\"font-size:12px; color:#9c9c9c; position:absolute; right:20px;\">"
				+ CommonMethod.CalendarToString(boxMessage.getCreatedTime(),
						null) + "</font>&nbsp;"
				+ "<div style=\"margin-top:10px;\">" + boxMessage.getContent()
				+ "</div>&nbsp;<br>";
		if (tinyUrl != null) {
			content = content
					+ "<a href=\""
					+ tinyUrl
					+ "\" target=\"_blank\" class=\"mobile-button\" ;=\"\" style=\"font-family: 'Helvetica neue', Helvetica, Arial, Verdana, sans-serif; color: rgb(255, 255, 255); text-decoration: none; background-color: rgb(103, 171, 73); border-style: solid; border-color: rgb(101, 160, 75); border-top-left-radius: 5px; border-top-right-radius: 5px; border-bottom-right-radius: 5px; border-bottom-left-radius: 5px; display: inline-block; font-size:12px; padding:2px 10px;border-width: 1px;\">查看我的位置 &gt;&gt;</a> ";

		}
		content = content
				+ " </div>&nbsp;"
				+ "<div style=\"font-size:14px\">此邮件请勿回复</div>&nbsp;"
				+ "<div style=\"height:119px;background:url(http://www.beidouhezi.com/beidouhezi/img/01/weixin_bg.png) 9px 0 no-repeat;padding-top:30px;padding-left:10px;margin:20px auto 20px auto;width:554px;\">&nbsp;"
				+ "<div style=\"width:377px;height:80px;float:left;margin-left:10px;\"> "
				+ "<img src=\"http://www.beidouhezi.com/beidouhezi/img/01/weixin_text_one.png\" width=\"377\" height=\"80\"> </div>&nbsp;<div style=\"width:122px;height:125px;background:url(http://www.beidouhezi.com/beidouhezi/img/01/weixin_border.png) left top no-repeat;float:left;margin-left:25px;margin-top:-20px;\"> "
				+ "<img src=\"http://www.beidouhezi.com/beidouhezi/img/02/%E5%8C%97%E6%96%97%E7%9B%92%E5%AD%90%E5%BE%AE%E4%BF%A1%E4%BA%8C%E7%BB%B4%E7%A0%81.jpg\" width=\"103\" height=\"103\" style=\"margin:10px;\"> </div>&nbsp;</div>&nbsp;"
				+ "<div style=\"color:#c5c5c5; font-size:12px; border-top:1px solid #e6e6e6; padding:7px 0; line-height:20px;\"> 如果你错误地收到了此电子邮件，你可以放心忽略此封邮件，无需进行任何操作 </div>&nbsp;"
				+ "<div style=\"font-size:12px; color:#999;line-height:20px;border-top:1px solid #e6e6e6;padding:10px 0;\"> 如有任何问题，可以与我们联系，我们将尽快为你解答。 "
				+ "<br>官网：www.beidouhezi.com ，电话：400-1636-110，QQ:4001636110 </div></td></tr></tbody></table>&nbsp;"
				+ "</div></div> "
				+ "</div></div></blockquote>";

		MailUtil.sendMail(emailSmtp, emailUsername, emailUsername,
				emailPassword, boxMessage.getFamilyMail(), subject, content);

	}
	// @Scheduled(cron = "0/10 * * * * ?")
	// private void receiveMail() {
	// try {
	//
	// Properties prop = System.getProperties();
	// prop.put("mail.store.protocol", "imap");
	// prop.put("mail.imap.host", emailImap);
	//
	// Session session = Session.getInstance(prop);
	//
	// int total = 0;
	// IMAPStore store = (IMAPStore) session.getStore("imap"); //
	// 使用imap会话机制，连接服务器
	// store.connect(emailUsername, emailPassword);
	//
	// IMAPFolder folder = (IMAPFolder) store.getFolder("INBOX"); // 收件箱
	// folder.open(Folder.READ_WRITE);
	// // 获取总邮件数
	// total = folder.getMessageCount();
	// System.out.println("-----------------共有未处理邮件：" + total
	// + " 封--------------");
	// Message[] messages = folder.getMessages();
	// // folder.get
	// for (Message message : messages) {
	// Flags flags = message.getFlags();
	// if (!flags.contains(Flags.Flag.SEEN)) {
	// message.setFlag(Flags.Flag.DELETED, true);
	// String content = parseMailContent(message.getContent());
	// if (content==null) {
	// break;
	// }
	// InternetAddress address[] = (InternetAddress[]) message
	// .getFrom();
	// String from = address[0].getAddress();
	// BoxMessage boxMessage = packMailMessageDtoToBoxMessage(
	// from, content);
	// if (boxMessage != null) {
	// messageControl.parseRevBoxMessage(boxMessage);
	// }
	// } else {
	// message.setFlag(Flags.Flag.DELETED, true);
	// }
	// }
	// if (folder != null)
	// folder.close(true);
	// if (store != null)
	// store.close();
	// } catch (Exception e) {
	// // TODO: handle exception
	// forerror.error(CommonMethod.getTrace(e));
	// }
	// }
	//
	// /**
	// * @param smsMessageDto
	// * @return
	// */
	// private BoxMessage packMailMessageDtoToBoxMessage(String fromAddress,
	// String content) {
	// User fromUser = userService.queryUser(null, null, null, null,
	// fromAddress);
	// if (fromUser == null) {
	// sendMailErrorMessage(fromAddress, "北斗盒子-未指定联系人", "你发送的消息："
	// + content + " 由于未指定北斗盒子用户，发送失败。");
	// return null;
	// }
	// // 解析手机到盒子的邮件
	// FamilyStatus familyStatus = familyStatusService.getFamilyStatus(null,
	// fromUser.getId());
	// if (familyStatus == null) {
	// // 不存映射关系
	// sendMailErrorMessage(fromAddress, "北斗盒子-未指定联系人", "你发送的消息："
	// + content + " 由于未指定北斗盒子用户，发送失败。");
	// return null;
	// } else {
	// BoxMessage boxMessage = new BoxMessage();
	// boxMessage.setMsgIoType(MsgIoType.IO_FAMILYTOBOX);
	// boxMessage.setMsgType(familyStatus.getMsgType());
	// boxMessage.setDataStatusType(DataStatusType.QUEUE);
	// boxMessage.setFromUser(fromUser);
	// boxMessage.setCreatedTime(Calendar.getInstance());
	// mailToBdmsgId++;
	// boxMessage.setMsgId(mailToBdmsgId);
	// boxMessage.setContent(content);
	// boxMessage.setToBox(familyStatus.getBox());
	// return boxMessage;
	// }
	//
	// }
	//
	// private String parseMailContent(Object content) {
	// try {
	// if (content instanceof Multipart) {
	// Multipart mPart = (MimeMultipart) content;
	// StringBuffer contentBuffer = new StringBuffer();
	// String partContentString = extractPart((MimeBodyPart) mPart
	// .getBodyPart(0));
	// if (partContentString != null) {
	// contentBuffer.append(partContentString);
	// return contentBuffer.toString();
	// }
	// }
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return null;
	// }
	//
	// /**
	// * 抽取内容
	// *
	// * @param part
	// */
	// private String extractPart(MimeBodyPart part) {
	// try {
	// StringBuffer contentBuffer = new StringBuffer();
	// String disposition = part.getDisposition();
	//
	// if (disposition != null
	// && (disposition.equalsIgnoreCase(Part.ATTACHMENT) || disposition
	// .equalsIgnoreCase(Part.INLINE))) {// 附件
	// } else {// 正文
	// if (part.getContent() instanceof String) {// 接收到的纯文本
	// contentBuffer.append(part.getContent());
	// }
	// if (part.getContent() instanceof MimeMultipart) {// 接收的邮件有附件时
	// BodyPart bodyPart = ((MimeMultipart) part.getContent())
	// .getBodyPart(0);
	// contentBuffer.append(bodyPart.getContent());
	// }
	// }
	// return contentBuffer.toString();
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return null;
	// }

}

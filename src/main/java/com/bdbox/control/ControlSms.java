package com.bdbox.control;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdbox.biz.MessageControl;
import com.bdbox.constant.BoxStatusType;
import com.bdbox.constant.MsgIoType;
import com.bdbox.constant.MsgType;
import com.bdbox.constant.UserStatusType;
import com.bdbox.constant.UserType;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxMessage;
import com.bdbox.entity.Dialrecord;
import com.bdbox.entity.FamilyStatus;
import com.bdbox.entity.User;
import com.bdbox.service.BoxService;
import com.bdbox.service.DialrecordService;
import com.bdbox.service.FamilyStatusService;
import com.bdbox.service.UserService;
import com.bdbox.util.BdboxUtil;
import com.bdsdk.constant.DataStatusType;
import com.bdsdk.util.CommonMethod;
import com.qizhi.dto.SendResultDto;
import com.qizhi.dto.SmsMessageDto;

import net.sf.json.JSONObject;

@Component
public class ControlSms {
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");
	private static Logger fordebug = Logger.getLogger("fordebug");
	@Autowired
	private UserService userService;
	@Autowired
	private FamilyStatusService familyStatusService;
	@Autowired
	private MessageControl messageControl;
	@Autowired
	private ControlDsi controlDsi;
	@Autowired
	private BoxService boxService;
	@Autowired
	private DialrecordService dialrecordService;
	
	@Autowired
	private ControlAlidayu controlAlidayu;
	/**
	 * 手机到北斗的短信的消息Id
	 */
	private static Integer smsToBdmsgId = 1;

	public void processSmsMessage(SmsMessageDto smsMessageDto) {
		// TODO Auto-generated method stub
		fordebug.debug(Thread.currentThread().getName() + " 收到上行手机短信：发送者："
				+ smsMessageDto.getFromSmsNumber() + "\n通道号码："
				+ smsMessageDto.getChannelNumber() + "\n内容："
				+ smsMessageDto.getContent());
		BoxMessage boxMessage = packSmsMessageDtoToBoxMessage(smsMessageDto);
		if (boxMessage != null) {
			messageControl.parseRevBoxMessage(boxMessage);
		}
	}

	/**
	 * @param smsMessageDto
	 * @return
	 */
	private BoxMessage packSmsMessageDtoToBoxMessage(SmsMessageDto smsMessageDto) {
		// 判断发送者手机号码是否为盒子用户，如果不是则新增为未激活用户
		String fromMob = smsMessageDto.getFromSmsNumber();
		String content = smsMessageDto.getContent();
		User fromUser = userService.queryUser(null, fromMob, null, null, null);
		if (fromUser == null) {
			fromUser = new User();
			fromUser.setCreatedTime(Calendar.getInstance());
			fromUser.setUserStatusType(UserStatusType.NOTACTIVATED);
			fromUser.setUpdateTime(Calendar.getInstance());
			fromUser.setUsername(fromMob);
			fromUser.setUserType(UserType.ROLE_COMMON);
			fromUser = userService.saveUser(fromUser, null);
		}
		try {
			// 判断　　#盒子ID#回复内容的发送方式
			String[] ss = content.split("#");
			if (ss.length >= 3 && ss[0].equals("") && !ss[1].equals("")
					&& !ss[2].equals("")) {
				String boxId = ss[1];
				Box toBox = boxService.getBox(BoxStatusType.NORMAL, boxId,
						null, null, null);
				if (toBox != null) {
					BoxMessage boxMessage = new BoxMessage();
					boxMessage.setMsgIoType(MsgIoType.IO_FREEMOBTOBD);
					boxMessage.setMsgType(MsgType.MSG_SAFE);
					boxMessage.setDataStatusType(DataStatusType.QUEUE);
					boxMessage.setFamilyMob(fromMob);
					boxMessage.setCreatedTime(Calendar.getInstance());
					smsToBdmsgId++;
					boxMessage.setMsgId(smsToBdmsgId);
					boxMessage.setContent(ss[2]);
					boxMessage.setToBox(toBox);
					return boxMessage;
				} else {
					sendSmsOhterMessage(fromMob, "未找到相关的北斗盒子" + boxId);
					return null;
				}
			}

			// 解析手机到盒子的SMS
			FamilyStatus familyStatus = familyStatusService.getFamilyStatus(
					null, fromMob);
			if (familyStatus == null) {
				// 不存映射关系
				sendSmsOhterMessage(fromMob,
						"手机号码未指向相关的北斗盒子，请联系盒子主人添加你的手机号码为家人号码吧【北斗盒子】");
				return null;
			} else if (content.getBytes("GB18030").length > 52) {
				sendSmsOhterMessage(fromMob,
						"发送失败，手机回复信息不能超过52个字节（26个汉字）【北斗盒子】");
				sendSmsOhterMessage(fromMob,
						"发送失败，手机回复信息不能超过52个字节（26个汉字）【北斗盒子】");

				return null;
			} else {
				BoxMessage boxMessage = new BoxMessage();
				boxMessage.setMsgIoType(MsgIoType.IO_FAMILYTOBOX);
				boxMessage.setMsgType(familyStatus.getMsgType());
				boxMessage.setDataStatusType(DataStatusType.QUEUE);
				boxMessage.setFamilyMob(fromMob);
				boxMessage.setCreatedTime(Calendar.getInstance());
				smsToBdmsgId++;
				boxMessage.setMsgId(smsToBdmsgId);
				boxMessage.setContent(content);
				boxMessage.setToBox(familyStatus.getBox());
				return boxMessage;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			forerror.error(CommonMethod.getTrace(e));
		}
		return null;
	}

	/**
	 * 发送消息给家人手机
	 * 
	 * @param boxMessage
	 * @return
	 */
	public void sendSmsBoxMessage(BoxMessage boxMessage) {
		// 更新短信通道状态映射表
		FamilyStatus familyStatus = familyStatusService.getFamilyStatus(null,
				boxMessage.getFamilyMob());
		if (familyStatus == null) {
			familyStatus = new FamilyStatus();
			familyStatus.setBox(boxMessage.getFromBox());
			familyStatus.setCreatedTime(Calendar.getInstance());
			familyStatus.setFamilyMob(boxMessage.getFamilyMob());
			familyStatus.setLastDataUpdateTime(Calendar.getInstance());
			familyStatus.setMsgType(boxMessage.getMsgType());
			familyStatusService.saveFamilyStatus(familyStatus);
		} else {
			familyStatus.setFamilyMob(boxMessage.getFamilyMob());
			familyStatus.setLastDataUpdateTime(Calendar.getInstance());
			familyStatus.setMsgType(boxMessage.getMsgType());
			String lastBoxSerialNumber=familyStatus.getBox().getBoxSerialNumber();
			if(lastBoxSerialNumber!=boxMessage.getFromBox().getBoxSerialNumber()){
				familyStatus.setSosLastCallTime(null);
			}
			familyStatus.setBox(boxMessage.getFromBox());
			
			familyStatusService.updateFamilyStatus(familyStatus);
		}

		// 获取发送者姓名
		Box fromBox = boxMessage.getFromBox();
		String name = fromBox.getName() + "(" + fromBox.getBoxSerialNumber()
				+ "):";
		String content = "";
		try {
			if (boxMessage.getLongitude() != null) {
				content = name
						+ boxMessage.getContent()
						+ "【N "
						+ BdboxUtil.convertToSexagesimal(boxMessage
								.getLatitude())
						+ ",E "
						+ BdboxUtil.convertToSexagesimal(boxMessage
								.getLongitude()) + "】" + "【北斗盒子】";

				// String tinyUrl = CommonMethod
				// .LongUrlToShortUrl("http://api.map.baidu.com/marker?location="
				// + boxMessage.getLatitude()
				// + ","
				// + boxMessage.getLongitude()
				// + "&title=我的位置&content="
				// + "&output=html&src=beidou&coord_type=gcj02");
				// if (tinyUrl == null) {
				// forerror.error("发送消息到家人手机时，位置转换为短网址失败");
				// return;
				// } else {
				// String locationContent = name + "【位置:"
				// + boxMessage.getLongitude() + ","
				// + boxMessage.getLatitude() + "】"+ "【北斗盒子】";
				// // 发送SMS消息
				// SendResultDto sendResultDto = controlDsi.sendSmsMessage(
				// boxMessage.getFamilyMob(), locationContent);
				// if (!sendResultDto.isSuccess()) {
				// forerror.error("发送消息到家人手机失败，错误消息："
				// + sendResultDto.getErrorMessage());
				// }
				// }
			} else {
				// 根据消息的内容类型，生成发送给SMS的短信内容
				content = name + boxMessage.getContent() + "【北斗盒子】";
			}
			// 发送SMS消息
			SendResultDto sendResultDto = controlDsi.sendSmsMessage(
					boxMessage.getFamilyMob(), content);
			if (!sendResultDto.isSuccess()) {
				forerror.error("发送消息到家人手机失败，错误消息："
						+ sendResultDto.getErrorMessage());
			}

			// sos语音通知
			saveSOScallmessage(boxMessage,familyStatus);
			
		} catch (Exception e) {
			// TODO: handle exception
			forerror.error("发送位置到SMS手机时，位置转换为短网址失败：" + CommonMethod.getTrace(e));
			return;
		}
	}
	
	
	
	public void sendSmsOhterMessage(String toMob, String content) {
		// 发送SMS消息
		SendResultDto sendResultDto = controlDsi.sendSmsMessage(toMob, content);
		if (!sendResultDto.isSuccess()) {
			forerror.error("发送Other消息给手机SMS失败，详细信息：tomob:" + toMob
					+ " content:" + content + " 错误消息："
					+ sendResultDto.getErrorMessage());
		}
	}

	public boolean sendSmsOhterMessage_wl(String toMob, String content) {
		// 发送SMS消息
		boolean flag = true;
		SendResultDto sendResultDto = controlDsi.sendSmsMessage(toMob, content);
		if (!sendResultDto.isSuccess()) {
			forerror.error("发送Other消息给手机SMS失败，详细信息：tomob:" + toMob
					+ " content:" + content + " 错误消息："
					+ sendResultDto.getErrorMessage());
			flag = false;
		}
		return flag;
	}

	/**
	 * 发送消息给自由手机号码
	 * 
	 * @param boxMessage
	 * @return
	 */
	public void sendFreePeopleSmsBoxMessage(BoxMessage boxMessage) {

		// 获取发送者姓名
		Box fromBox = boxMessage.getFromBox();
		String name = fromBox.getName() + "(" + fromBox.getBoxSerialNumber()
				+ "):";
		String content = "";
		try {
			if (boxMessage.getLongitude() != null) {
				String tinyUrl = CommonMethod
						.LongUrlToShortUrl("http://api.map.baidu.com/marker?location="
								+ boxMessage.getLatitude()
								+ ","
								+ boxMessage.getLongitude()
								+ "&title=我的位置&content="
								+ "&output=html&src=beidou&coord_type=gcj02");
				if (tinyUrl == null) {
					forerror.error("发送消息到家人手机时，位置转换为短网址失败");
					content = name
							+ boxMessage.getContent()
							+ "【N "
							+ BdboxUtil.convertToSexagesimal(boxMessage
									.getLatitude())
							+ ",E "
							+ BdboxUtil.convertToSexagesimal(boxMessage
									.getLongitude()) + "】" + "【回复格式:#"
							+ fromBox.getBoxSerialNumber() + "#+内容】";
				} else {
					content = name + boxMessage.getContent() + tinyUrl
							+ "【回复格式:#" + fromBox.getBoxSerialNumber()
							+ "#+内容】";
				}
			} else {
				// 根据消息的内容类型，生成发送给SMS的短信内容
				content = name + boxMessage.getContent() + "【回复格式:#"
						+ fromBox.getBoxSerialNumber() + "#+内容】";
			}
			// 发送SMS消息
			SendResultDto sendResultDto = controlDsi.sendSmsMessage(
					boxMessage.getFamilyMob(), content);
			if (!sendResultDto.isSuccess()) {
				forerror.error("发送消息到家人手机失败，错误消息："
						+ sendResultDto.getErrorMessage());
			}

		} catch (Exception e) {
			// TODO: handle exception
			forerror.error("发送位置到SMS手机时，位置转换为短网址失败：" + CommonMethod.getTrace(e));
			return;
		}
	}
	/**
	 * SOS语音通知功能
	 * @param boxMessage
	 */
	public void saveSOScallmessage(BoxMessage boxMessage,FamilyStatus familyStatus){
		if (boxMessage.getMsgType() == MsgType.MSG_SOS) {
			Box box = boxService.getBox(boxMessage.getFromBox().getId());
			if(box.isOpenSosCall()==false){
				//判断该盒子是否开放SOS语音呼叫功能
				return;
			}
			int SosCallfreq = familyStatus.getSosCallFreq() == null ? 0 : familyStatus
					.getSosCallFreq();
			Calendar time = familyStatus.getSosLastCallTime();
			if (time == null) {
				Dialrecord dialrecord = new Dialrecord();
				dialrecord.setBoxSerialNumber(box.getBoxSerialNumber());
				dialrecord.setHasSend(false);
				dialrecord.setDialNum(boxMessage.getFamilyMob());
				dialrecordService.saveDialrecord(dialrecord);
				familyStatus.setSosLastCallTime(Calendar.getInstance());
				SosCallfreq++;
				familyStatus.setSosCallFreq(SosCallfreq);
				familyStatusService.updateFamilyStatus(familyStatus);

			} else {
				long a = Calendar.getInstance().getTime().getTime();// 当前时间化成毫秒
				long b = time.getTime().getTime();// 最后一次soscall换成毫秒
				long c = a - b;
				// 时间间隔1个小时
				if (c > (60 * 60 * 1000)) {
					
					if (SosCallfreq > 2) {
						SosCallfreq = 0;
					}
					Dialrecord dialrecord = new Dialrecord();
					dialrecord.setBoxSerialNumber(box.getBoxSerialNumber());
					dialrecord.setHasSend(false);
					dialrecord.setDialNum(boxMessage.getFamilyMob());
					dialrecordService.saveDialrecord(dialrecord);
					familyStatus.setSosLastCallTime(Calendar.getInstance());
					SosCallfreq++;
					familyStatus.setSosCallFreq(SosCallfreq);
					familyStatusService.updateFamilyStatus(familyStatus);
				} else {
					if (SosCallfreq < 2) {
						Dialrecord dialrecord = new Dialrecord();
						dialrecord.setBoxSerialNumber(box
								.getBoxSerialNumber());
						dialrecord.setHasSend(false);
						dialrecord.setDialNum(boxMessage.getFamilyMob());
						dialrecordService.saveDialrecord(dialrecord);
						familyStatus.setSosLastCallTime(Calendar.getInstance());
						SosCallfreq++;
						familyStatus.setSosCallFreq(SosCallfreq);
						familyStatusService.updateFamilyStatus(familyStatus);
					}
				}
			}
		}
	}
	
	/**
	 * 调用阿里大鱼发送短信
	 * @param toMob
	 * @param content
	 * @param templateCode
	 * @return
	 */
	public Boolean sendMessage(String toMob, String content, String templateCode){
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", content);
		map.put("product", "  北斗盒子官网  ");
		String param = JSONObject.fromObject(map).toString();
		String recNum = toMob;
		
		Map<String, String> resultMap = controlAlidayu.sendSms( param, recNum, templateCode);

		if(!resultMap.get("status").equals("OK")){
			return false;
		}
		
		return true;
	}
	
	
	public static void main(String[] args) {
		String s = "#1212#你好";
		String s1 = "1212#你好";
		String s2 = "#1212你好";
		String[] ss = new String[3];
		String[] ss1 = new String[3];
		String[] ss2 = new String[3];
		ss = s.split("#");
		ss = s1.split("#");
		ss2 = s2.split("#");
		if (ss[0].equals("") && ss.length >= 3 && !ss[1].equals("")
				&& !ss[2].equals("")) {
			System.out.println("true");
		} else {
			System.out.println(false);
		}
	}
}

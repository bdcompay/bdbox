package com.bdbox.control;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bdbox.alidayu.AlidayuService;

/**
 * 啊里大于 api 管理
 * @author jlj
 *
 */
@Component
public class ControlAlidayu extends AlidayuService {
	
	@Value("${alidayu.appKey}")
	private String appkey;
	
	@Value("${alidayu.secret}")
	private String secret;
	
	@Value("${alidayu.sms.sign}")
	private String smsSign;
	
	@PostConstruct
	public void init() {
		super.init(appkey, secret,smsSign);
	}
}

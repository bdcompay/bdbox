package com.bdbox.control;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdbox.biz.MessageControl;
import com.bdbox.constant.BoxStatusType;
import com.bdbox.constant.MsgIoType;
import com.bdbox.constant.MsgType;
import com.bdbox.control.ControlDsi;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxFamily;
import com.bdbox.entity.BoxLocation;
import com.bdbox.entity.BoxMessage;
import com.bdbox.entity.BoxPartner;
import com.bdbox.entity.MsgReceipt;
import com.bdbox.entity.UserSendBoxMessage;
import com.bdbox.service.BoxFamilyService;
import com.bdbox.service.BoxMessageService;
import com.bdbox.service.BoxPartnerService;
import com.bdbox.service.BoxService;
import com.bdbox.service.EntStatDailyService;
import com.bdbox.service.UserService;
import com.bdsdk.constant.DataStatusType;
import com.bdsdk.util.BdCommonMethod;
import com.bdsdk.util.CommonMethod;
import com.qizhi.dto.CardLocationDto;
import com.qizhi.dto.CardMessageDto;
import com.qizhi.dto.CardReceiptDto;
import com.qizhi.dto.SendResultDto;

/**
 * 北斗数据打包成数据库实体并持久化到数据库，本类作为被观察者，数据打包成功后，通知其观察者进行下一步处理
 * 
 * @author will.yan
 * 
 */
@Component
public class ControlBd {
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");
	private static Logger fordebug = Logger.getLogger("fordebug");
	@Autowired
	private UserService userService;
	@Autowired
	private BoxFamilyService boxFamilyService;
	@Autowired
	private BoxPartnerService boxPartnerService;
	@Autowired
	private BoxService boxService;
	@Autowired
	private BoxMessageService boxMessageService;
	@Autowired
	private ControlDsi controlDsi;
	@Autowired
	private MessageControl messageControl;
	@Autowired
	private EntStatDailyService entStatDailyService;

	// 处理北斗卡信息
	public void processCardMessage(CardMessageDto cardMessageDto) {
		// TODO Auto-generated method stub
		fordebug.debug(Thread.currentThread().getName() + " 收到北斗短报文：发送者：" + cardMessageDto.getFromCardNumber()
				+ "\n接收者：" + cardMessageDto.getToCardNumber() + "\n数据：" + cardMessageDto.getContent());
		// 消息序号 用户回执回复
		String msgId = cardMessageDto.getContent().substring(0, 2);
		try {
			// 统计企业用户下属盒子发送数量
			entStatDailyService.addSendNumber(cardMessageDto.getFromCardNumber());
			// 用户获取短信
			if (processBoxGetMessage(cardMessageDto)) {
				return;
			} else if (packCardMessageDtoToZqbBoxLocations(cardMessageDto) != null) {
				// 发送收到位置消息回执
				sendBdLocaionReceipt(msgId, cardMessageDto.getFromCardNumber());
				List<BoxLocation> lt = packCardMessageDtoToZqbBoxLocations(cardMessageDto);
				forinfo.info("收到增强版盒子：" + lt.get(0).getBox().getBoxSerialNumber() + "上传" + lt.size() + "个位置");
				if (lt != null && lt.size() > 0) {
					messageControl.parseRevBoxLocation(lt);
				}
				return;
			} else if (packCardMessageDtoToLocationBoxMessage(cardMessageDto) != null) {
				BoxMessage boxMessage = packCardMessageDtoToLocationBoxMessage(cardMessageDto);
				BoxLocation boxLocation = packBoxMessageToBoxLocation(boxMessage);

				if (boxMessage.getMsgType() == MsgType.MSG_SOS) {
					// 发送sos回执
					sendBdSOSReceipt(msgId, boxMessage.getFromBox().getCardNumber());
				} else if (boxMessage.getMsgType() == MsgType.MSG_OK) {
					// 发送OK回执 有位置才发送OK回执
					if (boxLocation != null && boxLocation.getLatitude() != null && boxLocation.getLongitude() != null
							&& !(boxLocation.getLatitude() == 0 & boxLocation.getLongitude() == 0)) {
						sendBdOKReceipt(msgId, boxMessage.getFromBox().getCardNumber());
					}

				}

				if (boxLocation != null && boxLocation.getLatitude() != null && boxLocation.getLongitude() != null
						&& !(boxLocation.getLatitude() == 0 & boxLocation.getLongitude() == 0)) {
					messageControl.parseRevBoxLocation(boxLocation);
				}
				messageControl.parseRevBoxMessage(boxMessage);
				return;
			} else {
				BoxMessage boxMessage = packCardMessageDtoToBoxMessage(cardMessageDto);
				if (boxMessage != null) {
					if (boxMessage.getLongitude() != null) {
						BoxLocation boxLocation = packBoxMessageToBoxLocation(boxMessage);
						if (boxLocation != null && boxLocation.getLatitude() != null
								&& boxLocation.getLongitude() != null
								&& !(boxLocation.getLatitude() == 0 & boxLocation.getLongitude() == 0)) {
							messageControl.parseRevBoxLocation(boxLocation);
						}
					}
					messageControl.parseRevBoxMessage(boxMessage);

					// 成功回执
					MsgReceipt msgReceipt = new MsgReceipt();
					msgReceipt.setCreatedTime(Calendar.getInstance());
					msgReceipt.setDataStatusType(DataStatusType.SUCCESS);
					msgReceipt.setMsgId(boxMessage.getMsgId());
					msgReceipt.setStatusDescription("发送成功");
					msgReceipt.setToBox(boxMessage.getFromBox());
					sendBdMsgReceipt(msgReceipt);
					// 两条回执
					sendBdMsgReceipt(msgReceipt);
					return;
				} else {
					forerror.error("转换为chatmessage时失败，详细信息如下\n" + "发送者：" + cardMessageDto.getFromCardNumber() + "\n接收者："
							+ cardMessageDto.getToCardNumber() + "\n数据：" + cardMessageDto.getContent());
					return;
				}
			}

		} catch (Exception e) {
			forerror.error("转换为chatmessage时失败，详细信息如下\n" + "发送者：" + cardMessageDto.getFromCardNumber() + "\n接收者："
					+ cardMessageDto.getToCardNumber() + "\n数据：" + cardMessageDto.getContent() + "\n错误详情：\n"
					+ CommonMethod.getTrace(e));
		}
	}

	public void processCardReceipt(CardReceiptDto cardReceiptDto) {
		// TODO Auto-generated method stub
		fordebug.debug(
				"收到北斗短报文回执：发送者：" + cardReceiptDto.getFromCardNumber() + " 接收者：" + cardReceiptDto.getToCardNumber()
						+ "\n发送时间：" + CommonMethod.CalendarToString(cardReceiptDto.getSendTime(), null));
	
	
	}

	public void processCardLocation(CardLocationDto cardLocationDto) {
		BoxLocation boxLocation = packCardLocationDtoToBoxLocation(cardLocationDto);
		if (boxLocation != null) {
			messageControl.parseRevBoxLocation(boxLocation);

			// 紧急定位处理
			if (boxLocation.getEmergencyLocation() != null && boxLocation.getEmergencyLocation()) {
				try {
					// 紧急定位转成SOS信息
					BoxMessage boxMessage = packEnBoxLocationToSosBoxMessage(boxLocation);
					messageControl.parseRevBoxMessage(boxMessage);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 用户获取短信
	 * 
	 * @param cardMessageDto
	 * @return
	 * @throws Exception
	 */
	public Boolean processBoxGetMessage(CardMessageDto cardMessageDto) {
		try {
			String strHexData = cardMessageDto.getContent();
			String msgIdHexStr = strHexData.substring(0, 2);// 消息ID
			String typeHexStr = strHexData.substring(2, 4);// 内容类型
			String boxIdHexStr = strHexData.substring(4, 14);// 北斗盒子ID
			String timeStr = strHexData.substring(14, 26);// 拉取时间点
			int msgId = Integer.valueOf(BdCommonMethod.castHexStringToDcmString(msgIdHexStr));
			int type = Integer.valueOf(BdCommonMethod.castHexStringToDcmString(typeHexStr));
			String boxId = BdCommonMethod.castHexStringToDcmString(boxIdHexStr);

			if (type == 4) {
				Box fromBox = boxService.getBox(BoxStatusType.NORMAL, boxId, cardMessageDto.getFromCardNumber(), null,
						null);
				if (fromBox == null) {
					return false;
				}
				// 判断时间的有效性
				// String timeMillSecondStr = BdCommonMethod
				// .castHexStringToDcmString(timeStr);

				SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
				Date strTime = sdf.parse(timeStr);

				long nowMillis = Calendar.getInstance().getTimeInMillis();
				long lastMills = strTime.getTime();
				long diffTime = nowMillis - lastMills;
				// 如果时间差大于半年或者小于0，则废弃
				/*
				 * if (diffTime > 14515200000L || diffTime < 0) { return false;
				 * }
				 */

				// 找出该时间之后的短报文信息
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(lastMills);
				// List<BoxMessage> boxMessages =
				// boxMessageService.getUnReadBoxMessage(fromBox.getId(),
				// calendar);

				// 获取最后的六条
				List<BoxMessage> boxMessages = boxMessageService.getLastRevieveBoxMessages(
						Long.parseLong(fromBox.getBoxSerialNumber()), MsgType.MSG_LOCATION, 1, 6);

				// 把获取到的消息加入队列
				for (BoxMessage boxMessage : boxMessages) {
					sendBdBoxMessage(boxMessage);
				}
				return true;
			}

		} catch (Exception e) {
			// TODO: handle exception
			forerror.error("处理盒子用户获取消息时发生错误：" + CommonMethod.getTrace(e));
		}
		return false;
	}

	/**
	 * 打包收到的消息打包成消息实体
	 * 
	 * @param cardMessageDto
	 * @throws ParseException
	 */
	public BoxMessage packCardMessageDtoToBoxMessage(CardMessageDto cardMessageDto) throws Exception {

		String strHexData = cardMessageDto.getContent();
		String msgIdHexStr = strHexData.substring(0, 2);// 消息ID
		String msgTypeHexStr = strHexData.substring(2, 4);// 消息类型

		if (msgTypeHexStr.startsWith("F7")) {// 判断消息类型
			return packCardMessageDtoToFreePeopleBoxMessage(cardMessageDto);
		}

		String iotypeHexStr = strHexData.substring(4, 6);// 数据方向类型
		String tWOiotypeHexStr = BdCommonMethod.castHexStringToBinaryString(iotypeHexStr);// 二进制形式的数据方向
		String fromBoxIdHexStr = strHexData.substring(18, 28);// 北斗盒子ID
		// 判断字节是否带有位置信息
		String strHexLocation = strHexData.substring(28, 30);// 是否有位置标志位
		String strHexGps = "0000000000000000000000000000";// 位置置零
		String contentHexStr = strHexData.substring(30);// 消息内容长度变长
		if (!strHexLocation.equals("FF")) {// 位置信息开始位置是FF表示没有位置信息
			strHexGps = strHexData.substring(28, 56);// 位置信息
			contentHexStr = strHexData.substring(56);// 消息内容
		}

		int msgId = Integer.valueOf(BdCommonMethod.castHexStringToDcmString(msgIdHexStr));

		MsgType msgType = null;
		MsgIoType msgIoType = MsgIoType.IO_BOXTOALL;
		if (msgTypeHexStr.equals("F1")) {
			msgType = MsgType.MSG_SAFE;
		}
		if (msgTypeHexStr.equals("F2")) {
			msgType = MsgType.MSG_SOS;
		}
		if (msgTypeHexStr.equals("F3")) {
			msgType = MsgType.MSG_LOCATION;
		}
		if (msgTypeHexStr.equals("B2")) {
			msgType = MsgType.MSG_RECEIVED;
		}
		if (msgTypeHexStr.equals("B3")) {
			msgType = MsgType.MSG_READED;
		}

		if (iotypeHexStr.equals("01")) {
			msgIoType = MsgIoType.IO_BOXTOFAMILY;
		}
		if (iotypeHexStr.equals("02")) {
			msgIoType = MsgIoType.IO_BOXTOBOX;
		}
		if (iotypeHexStr.equals("03")) {
			msgIoType = MsgIoType.IO_BOXTOALL;
		}
		if (iotypeHexStr.equals("04")) {
			msgIoType = MsgIoType.IO_BOXTOSYSTEM;
		}
		if (iotypeHexStr.equals("06")) {
			msgIoType = MsgIoType.IO_BOXGETPARTNERLOCATION;
		}
		if (iotypeHexStr.equals("09")) {
			msgIoType = MsgIoType.IO_PINGTAITOBOX;
		}
		if (tWOiotypeHexStr.startsWith("01")) {
			msgIoType = MsgIoType.IO_BOXSLECTPEOPLE;
		}
		String fromBoxSerialNumber = BdCommonMethod.castHexStringToDcmString(fromBoxIdHexStr);
		String content = BdCommonMethod.castHexStringToHanziString(contentHexStr);

		// 判断发送者是否有效
		// Box fromBox = boxService.getBox(BoxStatusType.NORMAL,
		// fromBoxSerialNumber, cardMessageDto.getFromCardNumber(), null,
		// null);

		// 判断发送者是否有效 只利用卡号
		Box fromBox = boxService.getBox(BoxStatusType.NORMAL, null, cardMessageDto.getFromCardNumber(), null, null);
		// 测试用的
		// Box fromBox = boxService.getBox(BoxStatusType.NORMAL,
		// fromBoxSerialNumber, null, null,
		// null);

		if (fromBox == null) {
			forerror.error("收到未知盒子的消息，即发送者的北斗卡号以及北斗盒子ID在数据库中没有找到对应的数据，详细信息：发送者北斗卡号："
					+ cardMessageDto.getFromCardNumber() + " 发送者北斗盒子ID：" + fromBoxSerialNumber);
			return null;
		}
		// 位置数据
		int intSign = 0;
		int intLgtLen = 8;
		int intLttLen = 8;
		int intHgtLen = 4;
		int intRateLen = 4;
		int intDirLen = 4;
		String strHexLgt = strHexGps.substring(intSign, intSign = intSign + intLgtLen);
		String strHexLtt = strHexGps.substring(intSign, intSign = intSign + intLttLen);
		String strHexHgt = strHexGps.substring(intSign, intSign = intSign + intHgtLen);
		String strHgtSym = "+";
		if (strHexHgt.substring(0, 1).equals("F")) {
			if (strHexHgt.equals("FFFF")) {
				strHexHgt = "0000";
			} else {
				strHexHgt = strHexHgt.substring(1);
				strHgtSym = "-";
			}
		}
		String strHexRate = strHexGps.substring(intSign, intSign = intSign + intRateLen);
		strHexRate = strHexRate.equals("FFFF") ? "0000" : strHexRate;
		String strHexDirect = strHexGps.substring(intSign, intSign = intSign + intDirLen);
		strHexDirect = strHexDirect.equals("FFFF") ? "0000" : strHexDirect;
		String strFloatLgt = BdCommonMethod.castHexStringToDcmStringGalaxyTude(strHexLgt, 7);
		String strFloatLtt = BdCommonMethod.castHexStringToDcmStringGalaxyTude(strHexLtt, 7);
		String strDcmHgt = strHgtSym + BdCommonMethod.castHexStringToDcmString(strHexHgt);
		String strDcmRate = String.valueOf(Double.valueOf(BdCommonMethod.castHexStringToDcmString(strHexRate)) / 100);
		String strDcmDirect = String
				.valueOf(Double.valueOf(BdCommonMethod.castHexStringToDcmString(strHexDirect)) / 100);

		Double longitude = Double.valueOf(strFloatLgt);
		Double latitude = Double.valueOf(strFloatLtt);
		Double altitude = Double.valueOf(strDcmHgt);
		Double direction = Double.valueOf(strDcmDirect);
		Double speed = Double.valueOf(strDcmRate);

		// 打包数据
		BoxMessage boxMessage = new BoxMessage();
		if (strHexLocation.equals("FF")) {
			boxMessage.setIslocate(false);
		}
		boxMessage.setContent(content);
		// 如果是送达回执先保存十六进制代码
		if (msgTypeHexStr.equals("B2")) {
			boxMessage.setContent(contentHexStr);
		}
		// 如果是也阅读回执先保存十六进制代码
		if (msgTypeHexStr.equals("B3")) {
			boxMessage.setContent(contentHexStr);
		}
		boxMessage.setCreatedTime(Calendar.getInstance());
		boxMessage.setDataStatusType(DataStatusType.SUCCESS);
		boxMessage.setFromBox(fromBox);
		boxMessage.setMsgId(msgId);
		boxMessage.setMsgIoType(msgIoType);
		// 判断是否是可选择联系人
		if (msgIoType == MsgIoType.IO_BOXSLECTPEOPLE) {
			boxMessage.setSelectPeople(tWOiotypeHexStr);
		}
		boxMessage.setMsgType(msgType);
		boxMessage.setLatitude(latitude == 0 ? null : latitude);
		boxMessage.setLongitude(longitude == 0 ? null : longitude);
		boxMessage.setAltitude(altitude == 0 ? null : altitude);
		boxMessage.setSpeed(speed == 0 ? null : speed);
		boxMessage.setDirection(direction == 0 ? null : direction);
		boxMessage.setStatusDescription(DataStatusType.SUCCESS.str());

		// 判断boxMessage 参数的合法性
		if (isBoxMessageParamsRight(boxMessage)) {
			return boxMessage;
		}
		return null;

	}

	/**
	 * ，判断打包好的CHATMESSAGE的参数的合法性
	 * 
	 * @param boxMessage
	 * @return
	 */
	private boolean isBoxMessageParamsRight(BoxMessage boxMessage) {
		// 收到的数据类型不应该是由非北斗发出的
		if (boxMessage.getMsgType() == null || boxMessage.getMsgIoType() == null
				|| boxMessage.getMsgIoType() == MsgIoType.IO_FAMILYTOBOX) {
			return false;
		}
		if ((boxMessage.getLongitude() != null && boxMessage.getLongitude() > 360)
				|| (boxMessage.getLatitude() != null && boxMessage.getLatitude() > 180)
				|| (boxMessage.getAltitude() != null && boxMessage.getAltitude() > 100000)
				|| (boxMessage.getSpeed() != null && boxMessage.getSpeed() > 1000)
				|| (boxMessage.getDirection() != null && boxMessage.getDirection() > 360)) {
			return false;
		}

		return true;
	}

	/**
	 * 将收到的北斗RD定位打包成用户位置信息实体
	 * 
	 * @param cardLocationDto
	 */
	private BoxLocation packCardLocationDtoToBoxLocation(CardLocationDto cardLocationDto) {
		// 判断发送者是否有效
		Box fromBox = boxService.getBox(BoxStatusType.NORMAL, null, cardLocationDto.getFromCardNumber(), null, null);
		if (fromBox == null) {
			return null;
		}
		BoxLocation boxLocation = new BoxLocation();
		boxLocation.setAltitude(cardLocationDto.getAltitude());
		boxLocation.setCreatedTime(cardLocationDto.getCreatedTime());
		boxLocation.setDirection(cardLocationDto.getDirection());
		boxLocation.setLatitude(cardLocationDto.getLatitude());
		boxLocation.setLongitude(cardLocationDto.getLongitude());
		boxLocation.setSpeed(cardLocationDto.getSpeed());
		boxLocation.setLocationSource(MsgType.MSG_LOCATION);
		boxLocation.setBox(fromBox);
		boxLocation.setEmergencyLocation(cardLocationDto.getEmergencyLocation());
		return boxLocation;
	}

	/**
	 * 将位置类型的boxMessage转换为BoxLocation位置实体
	 * 
	 * @param boxMessage
	 * @return
	 */
	private BoxLocation packBoxMessageToBoxLocation(BoxMessage boxMessage) {
		BoxLocation boxLocation = new BoxLocation();
		boxLocation.setAltitude(boxMessage.getAltitude());
		boxLocation.setCreatedTime(boxMessage.getCreatedTime());
		boxLocation.setDirection(boxMessage.getDirection());
		boxLocation.setLatitude(boxMessage.getLatitude());
		boxLocation.setLongitude(boxMessage.getLongitude());
		boxLocation.setSpeed(boxMessage.getSpeed());
		boxLocation.setBox(boxMessage.getFromBox());
		boxLocation.setLocationSource(boxMessage.getMsgType());
		return boxLocation;
	}

	/**
	 * 发送消息回执给北斗盒子
	 * 
	 * @param msgReceipt
	 * @return
	 */
	public void sendBdMsgReceipt(MsgReceipt msgReceipt) {
		String msgIdHex = BdCommonMethod.castDcmStringToHexString(String.valueOf(msgReceipt.getMsgId()), 1);
		String statusHex = BdCommonMethod
				.castDcmStringToHexString(String.valueOf(msgReceipt.getDataStatusType().value()), 1);
		String descritionHex = BdCommonMethod.castHanziStringToHexString(msgReceipt.getStatusDescription());
		String strHexContentData = msgIdHex + "03" + statusHex + descritionHex;
		SendResultDto sendResultDto = controlDsi.sendCardMessage(msgReceipt.getToBox().getCardNumber(),
				strHexContentData);
		if (!sendResultDto.isSuccess()) {
			forerror.error("发送消息回执给北斗终端失败，错误消息：" + sendResultDto.getErrorMessage() + " 电文内容：" + strHexContentData);
		} else {
			fordebug.debug("发送消息回执给北斗盒子成功，接收者：盒子ID" + msgReceipt.getToBox().getBoxSerialNumber() + " 北斗卡号："
					+ msgReceipt.getToBox().getCardNumber() + " 电文内容：" + strHexContentData);
		}
	}

	/**
	 * 发送极限追踪回执给北斗盒子
	 * 
	 * @param msgReceipt
	 * @return
	 */
	public void sendBdLocaionReceipt(String msgId, String cardNumber) {

		String strHexContentData = msgId + "220330333036333239";
		SendResultDto sendResultDto = controlDsi.sendCardMessage(cardNumber, strHexContentData);
		if (!sendResultDto.isSuccess()) {
			forerror.error("发送极限追踪回执给北斗终端失败，错误消息：" + sendResultDto.getErrorMessage() + " 电文内容：" + strHexContentData);
		} else {
			fordebug.debug(
					"发送极限追踪位置回执给北斗盒子成功，接收者：盒子ID" + cardNumber + " 北斗卡号：" + cardNumber + " 电文内容：" + strHexContentData);
		}
	}

	/**
	 * 发送SOS回执
	 * 
	 * @param cardNumber
	 */
	public void sendBdSOSReceipt(String msgId, String cardNumber) {

		String strHexContentData = msgId + "EE0330333036333239";
		SendResultDto sendResultDto = controlDsi.sendCardMessage(cardNumber, strHexContentData);
		if (!sendResultDto.isSuccess()) {
			forerror.error("发送SOS回执给北斗终端失败，错误消息：" + sendResultDto.getErrorMessage() + " 电文内容：" + strHexContentData);
		} else {
			fordebug.debug(
					"发送SOS回执给北斗盒子成功，接收者：盒子ID" + cardNumber + " 北斗卡号：" + cardNumber + " 电文内容：" + strHexContentData);
		}
	}

	/**
	 * 发送OK模式回执
	 * 
	 * @param cardNumber
	 */
	public void sendBdOKReceipt(String msgId, String cardNumber) {

		String strHexContentData = msgId + "1E0330333036333239";
		SendResultDto sendResultDto = controlDsi.sendCardMessage(cardNumber, strHexContentData);
		if (!sendResultDto.isSuccess()) {
			forerror.error("发送OK回执给北斗终端失败，错误消息：" + sendResultDto.getErrorMessage() + " 电文内容：" + strHexContentData);
		} else {
			fordebug.debug(
					"发送OK回执给北斗盒子成功，接收者：盒子ID" + cardNumber + " 北斗卡号：" + cardNumber + " 电文内容：" + strHexContentData);
		}
	}

	/**
	 * 发送消息给北斗盒子
	 * 
	 * @param boxMessage
	 * @return
	 */
	public boolean sendBdBoxMessage(BoxMessage boxMessage) {
		boolean flag = true;
		// 信息内容大于mLength字符分条发送
		/*
		 * int mLength = 24;// 信息内容判断，多条发送 if(!boxMessage.getIslocate()){
		 * mLength=30;//不勾选位置30字符 } // String
		 * hexMessage=BdCommonMethod.castHanziStringToHexString
		 * (boxMessage.getContent()); int messageLength =
		 * boxMessage.getContent().length(); String content =
		 * boxMessage.getContent(); int le = messageLength / mLength; int ly =
		 * messageLength % mLength; if (le > 0 && !(le == 1 && ly == 0)) { for
		 * (int i = 0; i < le + 1; i++) { String content1 = content.substring(le
		 * * mLength); if ((i + 1) * mLength <= messageLength) { content1 =
		 * content .substring(i * mLength, (i + 1) * mLength); }
		 * boxMessage.setContent(content1); sendBdBoxMessage(boxMessage); } }
		 * else {
		 */
		try {
			String msgIdHex = BdCommonMethod.castDcmStringToHexString(String.valueOf(boxMessage.getMsgId()), 1);
			if (msgIdHex.length() > 2) {
				msgIdHex = msgIdHex.substring(msgIdHex.length() - 2);
			}

			String msgTypeHex = null;
			String fromIdHex = null;
			if (boxMessage.getMsgType() == MsgType.MSG_LOCATION) {
				msgTypeHex = "F6";
			} else if (boxMessage.getMsgType() == MsgType.MSG_SOS) {
				msgTypeHex = "F5";
			} else if (boxMessage.getMsgType() == MsgType.MSG_SAFE) {
				msgTypeHex = "F4";
			} else {
				return false;
			}
			String sendTime = CommonMethod.CalendarToString(boxMessage.getCreatedTime(), "yyMMddHHmmss");
			String fromNameHex = null;
			if (boxMessage.getMsgIoType() == MsgIoType.IO_FAMILYTOBOX) {
				fromIdHex = BdCommonMethod.castDcmStringToHexString(boxMessage.getFamilyMob(), 5);
				BoxFamily boxFamily = boxFamilyService.getBoxFamily(boxMessage.getToBox().getId(),
						boxMessage.getFamilyMob());

				if (boxFamily != null) {
					fromNameHex = BdCommonMethod.castHanziStringToHexString(boxFamily.getFamilyName());
				} else {
					fromNameHex = BdCommonMethod.castHanziStringToHexString(fromIdHex);
				}

			} else if (boxMessage.getMsgIoType() == MsgIoType.IO_BOXGETPARTNERLOCATION) {
				fromIdHex = BdCommonMethod
						.castDcmStringToHexString(String.valueOf(boxMessage.getFromBox().getBoxSerialNumber()), 5);
				BoxPartner boxPartner = boxPartnerService.getBoxPartner(boxMessage.getToBox().getId(),
						boxMessage.getFromBox().getBoxSerialNumber());
				fromNameHex = BdCommonMethod.castHanziStringToHexString(boxPartner.getPartnerName());
			} else if (boxMessage.getMsgIoType() == MsgIoType.IO_FREEMOBTOBD) {
				fromIdHex = BdCommonMethod.castDcmStringToHexString(boxMessage.getFamilyMob(), 5);
				BoxFamily boxFamily = boxFamilyService.getBoxFamily(boxMessage.getToBox().getId(),
						boxMessage.getFamilyMob());

				if (boxFamily != null) {
					fromNameHex = BdCommonMethod.castHanziStringToHexString(boxFamily.getFamilyName());
				} else {
					/*
					 * fromNameHex = boxMessage.getFromBox()
					 * .getBoxSerialNumber();
					 */
					fromNameHex = BdCommonMethod.castHanziStringToHexString("手机");
				}

			} else if (boxMessage.getMsgIoType() == MsgIoType.IO_EntUserTOBOX) {
				fromIdHex = BdCommonMethod.castDcmStringToHexString(boxMessage.getFamilyMob(), 5);
				fromNameHex = BdCommonMethod.castHanziStringToHexString("平台用户");
			} else {
				fromIdHex = BdCommonMethod
						.castDcmStringToHexString(String.valueOf(boxMessage.getFromBox().getBoxSerialNumber()), 5);
				BoxPartner boxPartner = boxPartnerService.getBoxPartner(boxMessage.getToBox().getId(),
						boxMessage.getFromBox().getBoxSerialNumber());
				if (boxPartner != null) {
					fromNameHex = BdCommonMethod.castHanziStringToHexString(boxPartner.getPartnerName());
				} else {
					fromNameHex = BdCommonMethod
							.castHanziStringToHexString(boxMessage.getFromBox().getBoxSerialNumber());
				}
			}

			double longitude = boxMessage.getLongitude() == null ? 0 : boxMessage.getLongitude();
			double latitude = boxMessage.getLatitude() == null ? 0 : boxMessage.getLatitude();
			double altitude = boxMessage.getAltitude() == null ? 0 : boxMessage.getAltitude();
			float speed = (float) (boxMessage.getSpeed() == null ? 0 : boxMessage.getSpeed());
			float direction = (float) (boxMessage.getDirection() == null ? 0 : boxMessage.getDirection());
			byte[] bLocation = BdCommonMethod.castLocationToByte(longitude, latitude, altitude, speed, direction);
			// 没有位置FF标志
			String locationHex = "FF";
			if (longitude != 0 && latitude != 0) {
				locationHex = BdCommonMethod.castBytesToString(bLocation);
			}

			String contentHex = BdCommonMethod.castHanziStringToHexString(boxMessage.getContent());
			// 发送者名字不能超过10个字节
			if (fromNameHex.length() > 20) {
				fromNameHex = fromNameHex.substring(0, 20);
			}
			// 有发送者名称
			String strHexContentData = msgIdHex + msgTypeHex + sendTime + fromIdHex + fromNameHex + "00" + locationHex
					+ contentHex;
			// 去掉发送者名称
			/*
			 * String strHexContentData = msgIdHex + msgTypeHex + sendTime +
			 * fromIdHex + "3100" + locationHex + contentHex;
			 */

			SendResultDto sendResultDto = controlDsi.sendCardMessage(boxMessage.getToBox().getCardNumber(),
					strHexContentData);
			if (!sendResultDto.isSuccess()) {
				forerror.error("发送消息给北斗盒子失败，错误消息：" + sendResultDto.getErrorMessage() + "发送者："
						+ boxMessage.getToBox().getCardNumber() + " 电文内容：" + strHexContentData);
				flag = false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			forerror.error(CommonMethod.getTrace(e));
			flag = false;
		}
		// }
		return flag;
	}

	/**
	 * 求救，位置上报信息，OK
	 * 
	 * @param cardMessageDto
	 * @return
	 * @throws Exception
	 */
	private BoxMessage packCardMessageDtoToLocationBoxMessage(CardMessageDto cardMessageDto) throws Exception {
		String strData = cardMessageDto.getContent();
		String msgTypeHex = strData.substring(0, 4);
		// 用于判断OK模式
		String msgTypeHex2 = msgTypeHex.substring(2);
		BoxMessage boxMessage = new BoxMessage();
		// 一代盒子SOS指令为00EE 二代盒子为01EE
		if (msgTypeHex.equals("00EE") || msgTypeHex.equals("01EE")||msgTypeHex2.contains("EE")) {
			boxMessage.setContent("SOS，求救，HELP!");
			boxMessage.setMsgType(MsgType.MSG_SOS);

		} else if (msgTypeHex.equals("00EF")) {
			boxMessage.setContent("位置上报");
			boxMessage.setMsgType(MsgType.MSG_LOCATION);
		} else if (msgTypeHex2.equals("E1")) {
			boxMessage.setContent("OK，I'm OK");
			boxMessage.setMsgType(MsgType.MSG_OK);
		} else {
			return null;
		}
		Box fromBox = boxService.getBox(BoxStatusType.NORMAL, null, cardMessageDto.getFromCardNumber(), null, null);
		if (fromBox == null) {
			return null;
		}

		// 无位置SOS
		String strData1 = strData.substring(2);
		if (strData1.startsWith("EE534F53")) {
			boxMessage.setMsgId(0);
			boxMessage.setCreatedTime(Calendar.getInstance());
			boxMessage.setFromBox(fromBox);
			boxMessage.setIslocate(false);
			boxMessage.setDataStatusType(DataStatusType.SUCCESS);
			boxMessage.setMsgIoType(MsgIoType.IO_BOXTOALL);
			// 判断是否有sos内容
			if (strData.length() >= 12) {
				String str = strData.substring(10);
				byte[] b = BdCommonMethod.castHexStringToByte(str);
				try {
					String content = new String(b, "GBK");
					boxMessage.setContent(content);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return boxMessage;
			// 无位置OK
		} else if (strData1.startsWith("E14F4B")) {
			// 判断是否有OK内容
			if (strData.length() >= 10) {
				String str = strData.substring(8);
				byte[] b = BdCommonMethod.castHexStringToByte(str);
				try {
					String content = new String(b, "GBK");
					boxMessage.setContent(content);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			boxMessage.setMsgId(0);
			boxMessage.setCreatedTime(Calendar.getInstance());
			boxMessage.setFromBox(fromBox);
			boxMessage.setIslocate(false);
			boxMessage.setDataStatusType(DataStatusType.SUCCESS);
			boxMessage.setMsgIoType(MsgIoType.IO_BOXTOALL);
			return boxMessage;
		}

		int intSign = 4;
		int intTimeLen = 8;
		int intLgtLen = 8;
		int intLttLen = 8;
		int intHgtSymLen = 2;
		int intHgtLen = 2;
//		int intErrorSymLen = 2;
//		int intErrorHgtLen = 2;
		String strHexTime = strData.substring(intSign, intSign = intSign + intTimeLen);
		String strHexLgt = strData.substring(intSign, intSign = intSign + intLgtLen);
		String strHexLtt = strData.substring(intSign, intSign = intSign + intLttLen);
		String strHexHgtSym = strData.substring(intSign, intSign = intSign + intHgtSymLen);
		String strHexHgt = strData.substring(intSign, intSign = intSign + intHgtLen);
//		String strHexErrorSym = strData.substring(intSign, intSign = intSign + intErrorSymLen);
//		String strHexError = strData.substring(intSign, intSign = intSign + intErrorHgtLen);
		String strHexRate = "0000";
		String strHexDirect = "0000";
		String hexContent=strData.substring(intSign);
		byte[] b = BdCommonMethod.castHexStringToByte(hexContent);
		try {
			String content = new String(b, "GBK");
			boxMessage.setContent(content);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 不取协议中的时间，某些机器上来的信息时间字段不准确或者为0
		// String strDcmTime = BdCommonMethod
		// .castHexHmsToDcmYmdhmsTime(strHexTime);
		String strDcmLgt = BdCommonMethod.castHexStringToDcmStringNormTude(strHexLgt);
		String strDcmLtt = BdCommonMethod.castHexStringToDcmStringNormTude(strHexLtt);
		String strHgtTmp = "0";
		String strErrorHgt = "0";
		if (strHexHgtSym.equals("01")) {
			strHgtTmp = "-" + BdCommonMethod.castHexStringToDcmString(strHexHgt);
		} else {
			strHgtTmp = BdCommonMethod.castHexStringToDcmString(strHexHgt);
		}
//		if (strHexErrorSym.equals("01")) {
//			strErrorHgt = "-" + BdCommonMethod.castHexStringToDcmString(strHexError);
//		} else {
//			strErrorHgt = BdCommonMethod.castHexStringToDcmString(strHexError);
//		}

		int intDcmHgt = Integer.parseInt(strHgtTmp) + Integer.parseInt(strErrorHgt);
		String strDcmHgt = String.valueOf(intDcmHgt);
		String strDcmRate = BdCommonMethod.castHexStringToDcmString(strHexRate);
		String strDcmDirect = BdCommonMethod.castHexStringToDcmString(strHexDirect);

		boxMessage.setMsgId(0);
		boxMessage.setAltitude(Double.valueOf(strDcmHgt));
		boxMessage.setCreatedTime(Calendar.getInstance());
		boxMessage.setFromBox(fromBox);
		boxMessage.setLatitude(Double.valueOf(strDcmLtt));
		boxMessage.setLongitude(Double.valueOf(strDcmLgt));
		boxMessage.setSpeed(Double.valueOf(strDcmRate));
		boxMessage.setDirection(Double.valueOf(strDcmDirect));
		boxMessage.setDataStatusType(DataStatusType.SUCCESS);
		boxMessage.setMsgIoType(MsgIoType.IO_BOXTOALL);
		return boxMessage;
	}

	/**
	 * 紧急定位转成盒子的SOS信息
	 * 
	 * @param cardMessageDto
	 * @return
	 * @throws Exception
	 */
	private BoxMessage packEnBoxLocationToSosBoxMessage(BoxLocation boxLocation) throws Exception {
		BoxMessage boxMessage = new BoxMessage();
		boxMessage.setContent("SOS，求救，HELP!");
		boxMessage.setMsgType(MsgType.MSG_SOS);
		boxMessage.setMsgId(0);
		boxMessage.setAltitude(boxLocation.getAltitude());
		boxMessage.setCreatedTime(boxLocation.getCreatedTime());
		boxMessage.setFromBox(boxLocation.getBox());
		boxMessage.setLatitude(boxLocation.getLatitude());
		boxMessage.setLongitude(boxLocation.getLongitude());
		boxMessage.setSpeed(boxLocation.getSpeed());
		boxMessage.setDirection(boxLocation.getDirection());
		boxMessage.setDataStatusType(DataStatusType.SUCCESS);
		boxMessage.setMsgIoType(MsgIoType.IO_BOXTOALL);
		return boxMessage;
	}

	/**
	 * 发送消息给北斗盒子 sdk接口下发,支持大于62字节的数据分条下发
	 * 
	 * @param boxMessage
	 * @return
	 */
	public boolean sendBdBoxMessage(UserSendBoxMessage userSendBoxMessage) {
		boolean flag = true;
		int mLength = 62;// 信息内容判断，多条发送
		String sss = userSendBoxMessage.getContent();
		String MessageHex = BdCommonMethod.castHanziStringToHexString(sss);
		if (MessageHex.length() / 2 <= mLength) {
			try {
				String msgIdHex = userSendBoxMessage.getMsgId().toString().substring(12);
				if (msgIdHex.length() > 2) {
					msgIdHex = msgIdHex.substring(msgIdHex.length() - 2);
				}
				String msgTypeHex = "B1";
				String fromIdHex = null;
				fromIdHex = BdCommonMethod.castDcmStringToHexString(userSendBoxMessage.getFromUser().getUsername(), 5);
				String hzHex = userSendBoxMessage.getMsgId().toString();
				String isRecievedHex = "00";
				if (userSendBoxMessage.getIsReceived()) {
					isRecievedHex = "01";
				}
				String isReadHex = "00";
				if (userSendBoxMessage.getIsRead()) {
					isReadHex = "01";
				}
				String contentHex = BdCommonMethod.castHanziStringToHexString(userSendBoxMessage.getContent());
				String strHexContentData = msgIdHex + msgTypeHex + fromIdHex + hzHex + isRecievedHex + isReadHex
						+ contentHex;
				Box toBox = boxService.getBox(null, userSendBoxMessage.getBoxSerialNumber(), null, null, null);
				SendResultDto sendResultDto = controlDsi.sendCardMessage(toBox.getCardNumber(), strHexContentData);
				if (!sendResultDto.isSuccess()) {
					forerror.error("发送消息给北斗盒子失败，错误消息：" + sendResultDto.getErrorMessage() + "发送者："
							+ toBox.getCardNumber() + " 电文内容：" + strHexContentData);
					flag = false;
				}
			} catch (Exception e) {
				// TODO: handle exception
				forerror.error(CommonMethod.getTrace(e));
				flag = false;
			}

		} else {
			for (int i = 0; i < sss.length(); i++) {
				String s1 = sss.substring(0, i);
				String hexMessage = BdCommonMethod.castHanziStringToHexString(s1);
				int MessageLenght = hexMessage.length() / 2;
				if (MessageLenght > mLength) {
					// 处理发送消息
					String s2 = sss.substring(0, i - 1);
					String s3 = sss.substring(i - 1);
					try {
						String msgIdHex = BdCommonMethod
								.castDcmStringToHexString(String.valueOf(userSendBoxMessage.getId()), 1);
						String msgTypeHex = "B1";
						String fromIdHex = null;
						fromIdHex = BdCommonMethod
								.castDcmStringToHexString(userSendBoxMessage.getFromUser().getUsername(), 5);
						String hzHex = BdCommonMethod.castDcmStringToHexString(userSendBoxMessage.getMsgId().toString(),
								7);
						String isRecievedHex = "00";
						if (userSendBoxMessage.getIsReceived()) {
							isRecievedHex = "01";
						}
						String isReadHex = "00";
						if (userSendBoxMessage.getIsRead()) {
							isReadHex = "01";
						}
						String contentHex = BdCommonMethod.castHanziStringToHexString(userSendBoxMessage.getContent());
						String strHexContentData = msgIdHex + msgTypeHex + fromIdHex + hzHex + isRecievedHex + isReadHex
								+ contentHex;
						Box toBox = boxService.getBox(null, userSendBoxMessage.getBoxSerialNumber(), null, null, null);
						SendResultDto sendResultDto = controlDsi.sendCardMessage(toBox.getCardNumber(),
								strHexContentData);
						if (!sendResultDto.isSuccess()) {
							forerror.error("发送消息给北斗盒子失败，错误消息：" + sendResultDto.getErrorMessage() + "发送者："
									+ toBox.getCardNumber() + " 电文内容：" + strHexContentData);
							flag = false;
						}
					} catch (Exception e) {
						// TODO: handle exception
						forerror.error(CommonMethod.getTrace(e));
						flag = false;
					}

					userSendBoxMessage.setContent(s3);
					sendBdBoxMessage(userSendBoxMessage);
					break;
				}
			}
		}
		return flag;
	}

	/**
	 * 打包收到的消息打包成自由联系人消息实体
	 * 
	 * @param cardMessageDto
	 * @throws ParseException
	 */
	public BoxMessage packCardMessageDtoToFreePeopleBoxMessage(CardMessageDto cardMessageDto) throws Exception {
		String strHexData = cardMessageDto.getContent();
		String msgIdHexStr = strHexData.substring(0, 2);// 消息ID
		String msgTypeHexStr = strHexData.substring(2, 4);// 消息类型
		String toNumber = strHexData.substring(4, 14);// 北斗盒子ID或者手机号码
		String fromBoxIdHexStr = strHexData.substring(14, 24);// 北斗盒子ID或者手机号码
		// 判断字节是否带有位置信息
		String strHexLocation = strHexData.substring(24, 26);// 是否有位置标志位
		String strHexGps = "0000000000000000000000000000";// 位置置零
		String contentHexStr = strHexData.substring(26);// 消息内容长度变长
		if (!strHexLocation.equals("FF")) {// 位置信息开始位置是FF表示没有位置信息
			strHexGps = strHexData.substring(24, 52);// 位置信息
			contentHexStr = strHexData.substring(52);// 消息内容
		}

		int msgId = Integer.valueOf(BdCommonMethod.castHexStringToDcmString(msgIdHexStr));

		MsgType msgType = MsgType.MSG_SAFE;

		MsgIoType msgIoType = MsgIoType.IO_FREEPEOPLE;

		String fromBoxSerialNumber = BdCommonMethod.castHexStringToDcmString(fromBoxIdHexStr);
		String content = BdCommonMethod.castHexStringToHanziString(contentHexStr);

		// 判断发送者是否有效
		// Box fromBox = boxService.getBox(BoxStatusType.NORMAL,
		// fromBoxSerialNumber, cardMessageDto.getFromCardNumber(), null,
		// null);

		// 判断发送者是否有效 只利用卡号
		Box fromBox = boxService.getBox(BoxStatusType.NORMAL, null, cardMessageDto.getFromCardNumber(), null, null);
		// 测试用的
		// Box fromBox = boxService.getBox(BoxStatusType.NORMAL,
		// fromBoxSerialNumber, null, null,
		// null);

		if (fromBox == null) {
			forerror.error("收到未知盒子的消息，即发送者的北斗卡号以及北斗盒子ID在数据库中没有找到对应的数据，详细信息：发送者北斗卡号："
					+ cardMessageDto.getFromCardNumber() + " 发送者北斗盒子ID：" + fromBoxSerialNumber);
			return null;
		}
		// 位置数据
		int intSign = 0;
		int intLgtLen = 8;
		int intLttLen = 8;
		int intHgtLen = 4;
		int intRateLen = 4;
		int intDirLen = 4;
		String strHexLgt = strHexGps.substring(intSign, intSign = intSign + intLgtLen);
		String strHexLtt = strHexGps.substring(intSign, intSign = intSign + intLttLen);
		String strHexHgt = strHexGps.substring(intSign, intSign = intSign + intHgtLen);
		String strHgtSym = "+";
		if (strHexHgt.substring(0, 1).equals("F")) {
			if (strHexHgt.equals("FFFF")) {
				strHexHgt = "0000";
			} else {
				strHexHgt = strHexHgt.substring(1);
				strHgtSym = "-";
			}
		}
		String strHexRate = strHexGps.substring(intSign, intSign = intSign + intRateLen);
		strHexRate = strHexRate.equals("FFFF") ? "0000" : strHexRate;
		String strHexDirect = strHexGps.substring(intSign, intSign = intSign + intDirLen);
		strHexDirect = strHexDirect.equals("FFFF") ? "0000" : strHexDirect;
		String strFloatLgt = BdCommonMethod.castHexStringToDcmStringGalaxyTude(strHexLgt, 7);
		String strFloatLtt = BdCommonMethod.castHexStringToDcmStringGalaxyTude(strHexLtt, 7);
		String strDcmHgt = strHgtSym + BdCommonMethod.castHexStringToDcmString(strHexHgt);
		String strDcmRate = String.valueOf(Double.valueOf(BdCommonMethod.castHexStringToDcmString(strHexRate)) / 100);
		String strDcmDirect = String
				.valueOf(Double.valueOf(BdCommonMethod.castHexStringToDcmString(strHexDirect)) / 100);

		Double longitude = Double.valueOf(strFloatLgt);
		Double latitude = Double.valueOf(strFloatLtt);
		Double altitude = Double.valueOf(strDcmHgt);
		Double direction = Double.valueOf(strDcmDirect);
		Double speed = Double.valueOf(strDcmRate);

		// 打包数据
		BoxMessage boxMessage = new BoxMessage();
		if (strHexLocation.equals("FF")) {
			boxMessage.setIslocate(false);
		}
		boxMessage.setContent(content);
		// 如果是送达回执先保存十六进制代码
		if (msgTypeHexStr.equals("B2")) {
			boxMessage.setContent(contentHexStr);
		}
		// 如果是也阅读回执先保存十六进制代码
		if (msgTypeHexStr.equals("B3")) {
			boxMessage.setContent(contentHexStr);
		}

		String toCard = BdCommonMethod.castHexStringToDcmString(toNumber);
		boxMessage.setFreePeopleNumber(toCard);
		boxMessage.setCreatedTime(Calendar.getInstance());
		boxMessage.setDataStatusType(DataStatusType.SUCCESS);
		boxMessage.setFromBox(fromBox);
		boxMessage.setMsgId(msgId);
		boxMessage.setMsgIoType(msgIoType);
		boxMessage.setMsgType(msgType);
		boxMessage.setLatitude(latitude == 0 ? null : latitude);
		boxMessage.setLongitude(longitude == 0 ? null : longitude);
		boxMessage.setAltitude(altitude == 0 ? null : altitude);
		boxMessage.setSpeed(speed == 0 ? null : speed);
		boxMessage.setDirection(direction == 0 ? null : direction);
		boxMessage.setStatusDescription(DataStatusType.SUCCESS.str());

		// 判断boxMessage 参数的合法性
		if (isBoxMessageParamsRight(boxMessage)) {
			return boxMessage;
		}
		return null;

	}

	/**
	 * 监控平台企业用户下发消息
	 * 
	 * @param userSendboxMessage
	 */
	public boolean sendEntUserBoxMessage(UserSendBoxMessage userSendboxMessage) {
		// TODO Auto-generated method stub
		boolean flag = true;
		try {
			// 消息ID
			String msgIdHex = BdCommonMethod.castDcmStringToHexString(String.valueOf(userSendboxMessage.getMsgId()), 1);
			if (msgIdHex.length() > 2) {
				msgIdHex = msgIdHex.substring(msgIdHex.length() - 2);
			}

			String msgTypeHex = null;
			// 发送者标志
			String fromIdHex = null;
			msgTypeHex = "F4";
			String sendTime = CommonMethod.CalendarToString(userSendboxMessage.getCreatedTime(), "yyMMddHHmmss");
			String fromNameHex = null;

			fromIdHex = BdCommonMethod.castDcmStringToHexString(userSendboxMessage.getEntUser().getPhone(), 5);

			fromNameHex = BdCommonMethod.castHanziStringToHexString(userSendboxMessage.getEntUser().getName());

			// 没有位置FF标志
			String locationHex = "FF";

			String contentHex = BdCommonMethod.castHanziStringToHexString(userSendboxMessage.getContent());
			// 发送者名字不能超过10个字节
			if (fromNameHex.length() > 20) {
				fromNameHex = fromNameHex.substring(0, 20);
			}
			// 有发送者名称
			String strHexContentData = msgIdHex + msgTypeHex + sendTime + fromIdHex + fromNameHex + "00" + locationHex
					+ contentHex;
			// 去掉发送者名称
			/*
			 * String strHexContentData = msgIdHex + msgTypeHex + sendTime +
			 * fromIdHex + "3100" + locationHex + contentHex;
			 */
			Box toBox = boxService.getBox(null, userSendboxMessage.getBoxSerialNumber(), null, null, null);
			SendResultDto sendResultDto = controlDsi.sendCardMessage(toBox.getCardNumber(), strHexContentData);
			if (!sendResultDto.isSuccess()) {
				forerror.error("发送消息给北斗盒子失败，错误消息：" + sendResultDto.getErrorMessage() + "发送者："
						+ userSendboxMessage.getEntUser().getName() + " 电文内容：" + strHexContentData);
				flag = false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			forerror.error(CommonMethod.getTrace(e));
			flag = false;
		}
		// }
		return flag;
	}

	public void pp(String sss) {
		int mLength = 6;// 信息内容判断，多条发送
		int beginIndex = mLength / 2;
		String hexMessage1 = BdCommonMethod.castHanziStringToHexString(sss);
		if (hexMessage1.length() / 2 <= mLength) {
			System.out.println(sss);
		}
		for (int i = beginIndex; i < sss.length(); i++) {
			String s1 = sss.substring(0, i);
			String hexMessage = BdCommonMethod.castHanziStringToHexString(s1);
			int MessageLenght = hexMessage.length() / 2;
			if (MessageLenght > mLength) {

				String s3 = sss.substring(i - 1);
				pp(s3);
				break;
			}
		}

	}

	/**
	 * 增强版极限最追踪功能 四个位置
	 * 
	 * @param cardMessageDto
	 * @return
	 * @throws Exception
	 */
	private List<BoxLocation> packCardMessageDtoToZqbBoxLocations(CardMessageDto cardMessageDto) throws Exception {
		String strData = cardMessageDto.getContent();
		String msgTypeHex = strData.substring(0, 4);
		String msgTypeHex2 = msgTypeHex.substring(2);
		List<BoxLocation> lt = new ArrayList<BoxLocation>();
		if (msgTypeHex.equals("01E4")||msgTypeHex.equals("E4")) {
			Box fromBox = boxService.getBox(BoxStatusType.NORMAL, null, cardMessageDto.getFromCardNumber(), null, null);
			if (fromBox == null) {
				return null;
			}
			int datalength = 2;
			int intSign = 6;
			int intTimeLen = 12;
			int intLgtLen = 8;
			int intLttLen = 8;
			int intHgtLen = 4;
			// 轨迹序号的长度
			int intXhlen = 4;

			String datal = strData.substring(4, 6);
			// 位置个数
			String locations = BdCommonMethod.castHexStringToDcmString(datal);
			int locationlength = Integer.parseInt(locations);
			// 判断是否有轨迹序号，当数据长度大于或者等于有轨迹序号的长度时，则判断为有轨迹序号，否则没有序号长度
			boolean tag = false;
			String ddata = strData.substring(6);
			if (ddata.length() >= locationlength * 36) {
				tag = true;
			}

			for (int i = 0; i < locationlength; i++) {

				String strHexTime = strData.substring(intSign, intSign = intSign + intTimeLen);
				String strHexLgt = strData.substring(intSign, intSign = intSign + intLgtLen);
				String strHexLtt = strData.substring(intSign, intSign = intSign + intLttLen);
				String strHexHgt = strData.substring(intSign, intSign = intSign + intHgtLen);
				if (tag) {
					String strXh = strData.substring(intSign, intSign = intSign + intXhlen);
				}
				String timeYear = "20" + BdCommonMethod.castHexStringToDcmString(strHexTime.substring(0, 2));
				String timeMonth = BdCommonMethod.castHexStringToDcmString(strHexTime.substring(2, 4));
				String timeDay = BdCommonMethod.castHexStringToDcmString(strHexTime.substring(4, 6));
				String timeHH = BdCommonMethod.castHexStringToDcmString(strHexTime.substring(6, 8));
				String timeMM = BdCommonMethod.castHexStringToDcmString(strHexTime.substring(8, 10));
				String timess = BdCommonMethod.castHexStringToDcmString(strHexTime.substring(10, 12));
				String timeT = timeYear + "-" + timeMonth + "-" + timeDay + " " + timeHH + ":" + timeMM + ":" + timess;
				SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = sd.parse(timeT);
				Calendar ctime = Calendar.getInstance();
				ctime.setTime(date);
				ctime.add(Calendar.HOUR_OF_DAY, 8);

				String strDcmLgt = BdCommonMethod.castHexStringToDcmStringNormTude(strHexLgt);
				String strDcmLtt = BdCommonMethod.castHexStringToDcmStringNormTude(strHexLtt);
				String strHgtTmp = null;
				// String strErrorHgt = null;

				String strBinaryHgtTmp = BdCommonMethod.castHexStringToBinaryString(strHexHgt);
				String strBinaryHgtTmp1 = strBinaryHgtTmp.substring(0, 2);
				String strBinaryHgtTmp2 = strBinaryHgtTmp.substring(2);

				// 高度
				double hg = 0;
				if (strBinaryHgtTmp1.equals("00")) {
					hg = Integer.parseInt(strBinaryHgtTmp2, 2);
				}
				if (strBinaryHgtTmp1.equals("01")) {
					hg = 0 - Integer.parseInt(strBinaryHgtTmp2, 2);
				}

				BoxLocation boxLocation = new BoxLocation();
				boxLocation.setAltitude(hg);
				boxLocation.setBox(fromBox);
				// 设置用户位置
				if (boxLocation.getBox() != null) {
					boxLocation.setUser(boxLocation.getBox().getUser());
				}
				boxLocation.setCreatedTime(ctime);
				boxLocation.setLatitude(Double.parseDouble(strDcmLtt));
				boxLocation.setLongitude(Double.parseDouble(strDcmLgt));
				boxLocation.setLocationSource(MsgType.MSG_LOCATION);
				boxLocation.setPositioningTime(ctime);
				lt.add(boxLocation);
			}
			return lt;
		} else {
			return null;
		}
	}

	/**
	 * 第三方平台下发自由消息 16进制格式
	 * 
	 * @param userSendboxMessage
	 */
	public void sendEntUserFreeBoxMessage(UserSendBoxMessage userSendboxMessage) {
		// TODO Auto-generated method stub
		Box toBox = boxService.getBox(null, userSendboxMessage.getBoxSerialNumber(), null, null, null);
		SendResultDto sendResultDto = controlDsi.sendCardMessage(toBox.getCardNumber(),
				userSendboxMessage.getContent());
		if (!sendResultDto.isSuccess()) {
			forerror.error("发送消息给北斗盒子失败，错误消息：" + sendResultDto.getErrorMessage() + "发送者："
					+ userSendboxMessage.getEntUser().getName() + " 电文内容：" + userSendboxMessage.getContent());
		}

	}

	public static void main(String[] args) {
//		String str="01E102290000711A2C0717091C05002100004F6BC2F0A3BFB2BB6F6BBECDCBB5";
		
		String strData = "01E102290000711A2C0717091C05002100004F6BC2F0A3BFB2BB6F6BBECDCBB5";
		String msgTypeHex = strData.substring(0, 4);
		// 用于判断OK模式
		String msgTypeHex2 = msgTypeHex.substring(2);
		BoxMessage boxMessage = new BoxMessage();
		// 一代盒子SOS指令为00EE 二代盒子为01EE
		if (msgTypeHex.equals("00EE") || msgTypeHex.equals("01EE")||msgTypeHex2.contains("EE")) {
			boxMessage.setContent("SOS，求救，HELP!");
			boxMessage.setMsgType(MsgType.MSG_SOS);

		} else if (msgTypeHex.equals("00EF")) {
			boxMessage.setContent("位置上报");
			boxMessage.setMsgType(MsgType.MSG_LOCATION);
		} else if (msgTypeHex2.equals("E1")) {
			boxMessage.setContent("OK，I'm OK");
			boxMessage.setMsgType(MsgType.MSG_OK);
		} else {
		}
		Box fromBox = new Box();

		// 无位置SOS
		String strData1 = strData.substring(2);
		if (strData1.startsWith("EE534F53")) {
			boxMessage.setMsgId(0);
			boxMessage.setCreatedTime(Calendar.getInstance());
			boxMessage.setFromBox(fromBox);
			boxMessage.setIslocate(false);
			boxMessage.setDataStatusType(DataStatusType.SUCCESS);
			boxMessage.setMsgIoType(MsgIoType.IO_BOXTOALL);
			// 判断是否有sos内容
			if (strData.length() >= 12) {
				String str = strData.substring(10);
				byte[] b = BdCommonMethod.castHexStringToByte(str);
				try {
					String content = new String(b, "GBK");
					boxMessage.setContent(content);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			// 无位置OK
		} else if (strData1.startsWith("E14F4B")) {
			// 判断是否有OK内容
			if (strData.length() >= 10) {
				String str = strData.substring(8);
				byte[] b = BdCommonMethod.castHexStringToByte(str);
				try {
					String content = new String(b, "GBK");
					boxMessage.setContent(content);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			boxMessage.setMsgId(0);
			boxMessage.setCreatedTime(Calendar.getInstance());
			boxMessage.setFromBox(fromBox);
			boxMessage.setIslocate(false);
			boxMessage.setDataStatusType(DataStatusType.SUCCESS);
			boxMessage.setMsgIoType(MsgIoType.IO_BOXTOALL);
		}

		int intSign = 4;
		int intTimeLen = 8;
		int intLgtLen = 8;
		int intLttLen = 8;
		int intHgtSymLen = 2;
		int intHgtLen = 2;
//		int intErrorSymLen = 2;
//		int intErrorHgtLen = 2;
		String strHexTime = strData.substring(intSign, intSign = intSign + intTimeLen);
		String strHexLgt = strData.substring(intSign, intSign = intSign + intLgtLen);
		String strHexLtt = strData.substring(intSign, intSign = intSign + intLttLen);
		String strHexHgtSym = strData.substring(intSign, intSign = intSign + intHgtSymLen);
		String strHexHgt = strData.substring(intSign, intSign = intSign + intHgtLen);
//		String strHexErrorSym = strData.substring(intSign, intSign = intSign + intErrorSymLen);
//		String strHexError = strData.substring(intSign, intSign = intSign + intErrorHgtLen);
		String strHexRate = "0000";
		String strHexDirect = "0000";
		String hexContent=strData.substring(intSign);
		byte[] b = BdCommonMethod.castHexStringToByte(hexContent);
		try {
			String content = new String(b, "GBK");
			boxMessage.setContent(content);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 不取协议中的时间，某些机器上来的信息时间字段不准确或者为0
		// String strDcmTime = BdCommonMethod
		// .castHexHmsToDcmYmdhmsTime(strHexTime);
		String strDcmLgt = BdCommonMethod.castHexStringToDcmStringNormTude(strHexLgt);
		String strDcmLtt = BdCommonMethod.castHexStringToDcmStringNormTude(strHexLtt);
		String strHgtTmp = "0";
		String strErrorHgt = "0";
		if (strHexHgtSym.equals("01")) {
			strHgtTmp = "-" + BdCommonMethod.castHexStringToDcmString(strHexHgt);
		} else {
			strHgtTmp = BdCommonMethod.castHexStringToDcmString(strHexHgt);
		}
//		if (strHexErrorSym.equals("01")) {
//			strErrorHgt = "-" + BdCommonMethod.castHexStringToDcmString(strHexError);
//		} else {
//			strErrorHgt = BdCommonMethod.castHexStringToDcmString(strHexError);
//		}

		int intDcmHgt = Integer.parseInt(strHgtTmp) + Integer.parseInt(strErrorHgt);
		String strDcmHgt = String.valueOf(intDcmHgt);
		String strDcmRate = BdCommonMethod.castHexStringToDcmString(strHexRate);
		String strDcmDirect = BdCommonMethod.castHexStringToDcmString(strHexDirect);

		boxMessage.setMsgId(0);
		boxMessage.setAltitude(Double.valueOf(strDcmHgt));
		boxMessage.setCreatedTime(Calendar.getInstance());
		boxMessage.setFromBox(fromBox);
		boxMessage.setLatitude(Double.valueOf(strDcmLtt));
		boxMessage.setLongitude(Double.valueOf(strDcmLgt));
		boxMessage.setSpeed(Double.valueOf(strDcmRate));
		boxMessage.setDirection(Double.valueOf(strDcmDirect));
		boxMessage.setDataStatusType(DataStatusType.SUCCESS);
		boxMessage.setMsgIoType(MsgIoType.IO_BOXTOALL);
	}

}

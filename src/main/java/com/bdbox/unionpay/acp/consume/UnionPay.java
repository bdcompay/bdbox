package com.bdbox.unionpay.acp.consume;

import java.util.HashMap;
import java.util.Map;

import com.bdbox.unionpay.acp.sdk.AcpService;
import com.bdbox.unionpay.acp.sdk.LogUtil;
import com.bdbox.unionpay.acp.sdk.SDKConfig;
import com.bdbox.util.UtilDate;

/**
 * 银联支付接口api
 * @author hax
 *
 */
public class UnionPay {
	
	/**
	 * 支付
	 * @param orderId 订单号
	 * @param txnAmt  交易金额
	 * @param transactionType 订单类型
	 * @return
	 */
	public static String pay(String orderId, String txnAmt, String transactionType){
		Map<String, String> requestData = new HashMap<String, String>();
		
		/***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
		requestData.put("version", SDKConfig.getConfig().getVersion());   		  //版本号，全渠道默认值
		requestData.put("encoding", SDKConfig.getConfig().getEncoding_UTF8()); 	  //字符集编码，可以使用UTF-8,GBK两种方式
		requestData.put("signMethod", "01");            			  //签名方法，只支持 01：RSA方式证书加密
		requestData.put("txnType", "01");               			  //交易类型 ，01：消费
		requestData.put("txnSubType", "01");            			  //交易子类型， 01：自助消费
		requestData.put("bizType", "000201");           			  //业务类型，B2C网关支付，手机wap支付
		requestData.put("channelType", "07");           			  //渠道类型，这个字段区分B2C网关支付和手机wap支付；07：PC,平板  08：手机
		
		/***商户接入参数***/
		requestData.put("merId", SDKConfig.getConfig().getMerId());   //商户号码，请改成自己申请的正式商户号或者open上注册得来的777测试商户号
		requestData.put("accessType", "0");             			  //接入类型，0：直连商户 
		requestData.put("orderId",orderId);             			  //商户订单号，8-40位数字字母，不能含“-”或“_”，可以自行定制规则		
		requestData.put("txnTime", UtilDate.getOrderNum());           //订单发送时间，取系统时间，格式为YYYYMMDDhhmmss，必须取当前时间，否则会报txnTime无效
		requestData.put("currencyCode", "156");         			  //交易币种（境内商户一般是156 人民币）		
		requestData.put("txnAmt", txnAmt);             			      //交易金额，单位分，不要带小数点
		
		//前台通知地址 （需设置为外网能访问 http https均可），支付成功后的页面 点击“返回商户”按钮的时候将异步通知报文post到该地址
		requestData.put("frontUrl",getFrontUrl(transactionType, SDKConfig.getConfig().getFrontUrl()));
		
		//后台通知地址
		requestData.put("backUrl", SDKConfig.getConfig().getBackUrl());
		
		/**请求参数设置完毕，以下对请求参数进行签名并生成html表单，将表单写入浏览器跳转打开银联页面**/
		Map<String, String> submitFromData = AcpService.sign(requestData,SDKConfig.getConfig().getEncoding_UTF8()); 
		
		String html = AcpService.createAutoFormHtml(SDKConfig.getConfig().getFrontRequestUrl(), 
				submitFromData,SDKConfig.getConfig().getEncoding_UTF8());
		
		LogUtil.writeLog("打印请求HTML，此为请求报文，为联调排查问题的依据："+html);
		return html;
	}
	
	/**
	 * 预授权
	 * @param orderId
	 * @param txnAmt
	 * @param transactionType
	 * @return
	 */
	public static String authDeal(String orderId, String txnAmt, String transactionType){
		
		Map<String, String> requestData = new HashMap<String, String>();
		
		/***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
		requestData.put("version", SDKConfig.getConfig().getVersion());   			  	  //版本号，全渠道默认值
		requestData.put("encoding", SDKConfig.getConfig().getEncoding_UTF8()); 			  //字符集编码，可以使用UTF-8,GBK两种方式
		requestData.put("signMethod", "01");            			  //签名方法，只支持 01：RSA方式证书加密
		requestData.put("txnType", "02");               			  //交易类型 ，02：预授权
		requestData.put("txnSubType", "01");            			  //交易子类型， 01：预授权
		requestData.put("bizType", "000201");           			  //业务类型，B2C网关支付，手机wap支付
		requestData.put("channelType", "07");           			  //渠道类型，这个字段区分B2C网关支付和手机wap支付；07：PC,平板  08：手机
		
		/***商户接入参数***/
		requestData.put("merId", SDKConfig.getConfig().getMerId());    	          			  //商户号码，请改成自己申请的正式商户号或者open上注册得来的777测试商户号
		requestData.put("accessType", "0");             			  //接入类型，0：直连商户 
		requestData.put("orderId",orderId);             			  //商户订单号，8-40位数字字母，不能含“-”或“_”，可以自行定制规则		
		requestData.put("txnTime", UtilDate.getOrderNum());           //订单发送时间，取系统时间，格式为YYYYMMDDhhmmss，必须取当前时间，否则会报txnTime无效
		requestData.put("currencyCode", "156");         			  //交易币种（境内商户一般是156 人民币）		
		requestData.put("txnAmt", txnAmt);             			      //交易金额，单位分，不要带小数点		
		
		requestData.put("frontUrl", getFrontUrl(transactionType, SDKConfig.getConfig().getFrontUrl()));
		
		requestData.put("backUrl",SDKConfig.getConfig().getBackUrl());
		
		/**请求参数设置完毕，以下对请求参数进行签名并生成html表单，将表单写入浏览器跳转打开银联页面**/
		Map<String, String> submitFromData = AcpService.sign(requestData,SDKConfig.getConfig().getEncoding_UTF8());  //报文中certId,signature的值是在signData方法中获取并自动赋值的，只要证书配置正确即可。

		String requestFrontUrl = SDKConfig.getConfig().getFrontRequestUrl();  //获取请求银联的前台地址：对应属性文件acp_sdk.properties文件中的acpsdk.frontTransUrl
		String html = AcpService.createAutoFormHtml(requestFrontUrl, submitFromData,SDKConfig.getConfig().getEncoding_UTF8());   //生成自动跳转的Html表单
		
		LogUtil.writeLog("打印请求HTML，此为请求报文，为联调排查问题的依据："+html);
		return html;
	}
	
	/**
	 * 预授权完成
	 * @param txnAmt
	 * @param origQryId
	 * @return
	 */
	public static Map<String, String> authFinish(String txnAmt, String origQryId){
		Map<String, String> data = new HashMap<String, String>();
		
		/***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
		data.put("version", SDKConfig.getConfig().getVersion());            //版本号
		data.put("encoding", SDKConfig.getConfig().getEncoding_UTF8());          //字符集编码 可以使用UTF-8,GBK两种方式
		data.put("signMethod", "01");                     //签名方法 目前只支持01-RSA方式证书加密
		data.put("txnType", "03");                        //交易类型 03-预授权完成
		data.put("txnSubType", "00");                     //交易子类型  默认00
		data.put("bizType", "000201");                    //业务类型 B2C网关支付，手机wap支付
		data.put("channelType", "07");                    //渠道类型，07-PC，08-手机
		
		/***商户接入参数***/
		data.put("merId", SDKConfig.getConfig().getMerId());  
		data.put("accessType", "0");                      	
		data.put("orderId", UtilDate.getOrderNum());      		
		data.put("txnTime", UtilDate.getOrderNum());      
		data.put("txnAmt", txnAmt);                       //【完成金额】，金额范围为预授权金额的0-115%	
		data.put("currencyCode", "156");                  		
		data.put("backUrl", SDKConfig.getConfig().getRefundBackUrl());   
		
		//【原始交易流水号】，原消费交易返回的的queryId，可以从消费交易后台通知接口中或者交易状态查询接口中获取
		data.put("origQryId", origQryId);   			  
		
		Map<String, String> reqData  = AcpService.sign(data,SDKConfig.getConfig().getEncoding_UTF8());//报文中certId,signature的值是在signData方法中获取并自动赋值的，只要证书配置正确即可。
		String url = SDKConfig.getConfig().getBackRequestUrl();//交易请求url从配置文件读取对应属性文件acp_sdk.properties中的 acpsdk.backTransUrl
		
		Map<String,String> rspData = AcpService.post(reqData,url,SDKConfig.getConfig().getEncoding_UTF8());
		return rspData;
	}
	
	/**
	 * 预授权撤销
	 * @param txnAmt
	 * @param origQryId
	 * @return
	 */
	public static Map<String, String> authUndo(String txnAmt, String origQryId){
		Map<String, String> data = new HashMap<String, String>();
		
		/***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
		data.put("version", SDKConfig.getConfig().getVersion());            
		data.put("encoding", SDKConfig.getConfig().getEncoding_UTF8());          
		data.put("signMethod", "01");                     
		data.put("txnType", "32");                        //交易类型 31-预授权撤销
		data.put("txnSubType", "00");                     //交易子类型  默认00
		data.put("bizType", "000201");                    
		data.put("channelType", "07");                    
		
		/***商户接入参数***/
		data.put("merId", SDKConfig.getConfig().getMerId());            
		data.put("accessType", "0");                     
		data.put("orderId", UtilDate.getOrderNum());       	
		data.put("txnTime", UtilDate.getOrderNum());  
		data.put("txnAmt", txnAmt);                       
		data.put("currencyCode", "156");                  
		//data.put("reqReserved", "透传信息");                
		data.put("backUrl", SDKConfig.getConfig().getBackUrl());            
		
		/***要调通交易以下字段必须修改***/
		//【原始交易流水号】，原消费交易返回的的queryId，可以从消费交易后台通知接口中或者交易状态查询接口中获取
		data.put("origQryId", origQryId);   			
		
		Map<String, String> reqData  = AcpService.sign(data,SDKConfig.getConfig().getEncoding_UTF8());
		String url = SDKConfig.getConfig().getBackRequestUrl();
		
		Map<String,String> rspData = AcpService.post(reqData,url,SDKConfig.getConfig().getEncoding_UTF8());
		
		return rspData;
	}
	
	/**
	 * 退货退款
	 * @param txnAmt	交易金额
	 * @param origQryId	交易流水号
	 * @return
	 * 交易说明： 1）退货金额不超过总金额，可以进行多次退货
	 * 2）退货能对11个月内的消费做（包括当清算日），支持部分退货或全额退货，
	 * 到账时间较长，一般1-10个清算日（多数发卡行5天内，但工行可能会10天），所有银行都支持
	 */
	public static Map<String, String> refund(String txnAmt, String origQryId){
		Map<String, String> data = new HashMap<String, String>();
		
		/***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
		data.put("version", SDKConfig.getConfig().getVersion());              
		data.put("encoding", SDKConfig.getConfig().getEncoding_UTF8());             
		data.put("signMethod", "01");                        
		data.put("txnType", "04");                           //交易类型 04-退货		
		data.put("txnSubType", "00");                       	
		data.put("bizType", "000201");                       
		data.put("channelType", "07");                     	
		
		/***商户接入参数***/
		data.put("merId", SDKConfig.getConfig().getMerId());             
		data.put("accessType", "0");                         		
		data.put("orderId", UtilDate.getOrderNum());         	
		data.put("txnTime", UtilDate.getOrderNum());         		
		data.put("currencyCode", "156");                    	
		data.put("txnAmt", txnAmt);                          		
		data.put("backUrl", SDKConfig.getConfig().getRefundBackUrl());         
		
		/***要调通交易以下字段必须修改***/
		data.put("origQryId", origQryId);      //****原消费交易返回的的queryId，可以从消费交易后台通知接口中或者交易状态查询接口中获取
		
		/**请求参数设置完毕，以下对请求参数进行签名并发送http post请求，接收同步应答报文------------->**/
		Map<String, String> reqData  = AcpService.sign(data,SDKConfig.getConfig().getEncoding_UTF8());
		
		Map<String, String> rspData = AcpService.post(reqData,
				SDKConfig.getConfig().getBackRequestUrl(),SDKConfig.getConfig().getEncoding_UTF8());
		return rspData;
	}
	
	/**
	 * 撤销、退款
	 * @param txnAmt	交易金额
	 * @param origQryId	交易流水号
	 * @return
	 * 交易说明:消费撤销仅能对当清算日的消费做，必须为全额，一般当日或第二日到账。
	 */
	public static Map<String,String> revocation(String txnAmt, String origQryId){
		Map<String, String> data = new HashMap<String, String>();
		
		/***银联全渠道系统，产品参数，除了encoding自行选择外其他不需修改***/
		data.put("version", SDKConfig.getConfig().getVersion());            
		data.put("encoding", SDKConfig.getConfig().getEncoding_UTF8());          
		data.put("signMethod", "01");                     
		data.put("txnType", "31");                        //交易类型 31-消费撤销
		data.put("txnSubType", "00");                     
		data.put("bizType", "000201");                    
		data.put("channelType", "07");                    
		
		/***商户接入参数***/
		data.put("merId", SDKConfig.getConfig().getMerId());         
		data.put("accessType", "0");                      
		data.put("orderId", UtilDate.getOrderNum());       			 	
		data.put("txnTime", UtilDate.getOrderNum());   	  
		data.put("txnAmt", txnAmt);                       
		data.put("currencyCode", "156");                  
		data.put("backUrl", SDKConfig.getConfig().getRefundBackUrl());      
		
		/***要调通交易以下字段必须修改***/
		data.put("origQryId", origQryId);   			  //【原始交易流水号】，原消费交易返回的的queryId，可以从消费交易后台通知接口中或者交易状态查询接口中获取
		
		/**请求参数设置完毕，以下对请求参数进行签名并发送http post请求，接收同步应答报文**/
		Map<String, String> reqData  = AcpService.sign(data,SDKConfig.getConfig().getEncoding_UTF8());
		
		Map<String,String> rspData = AcpService.post(reqData,
				SDKConfig.getConfig().getBackRequestUrl(),SDKConfig.getConfig().getEncoding_UTF8());
		
		return rspData;
	}
	
	/**
	 * 交易查询
	 * @param orderId 订单ID
	 * @param txnTime 交易时间
	 * @return
	 */
	public static Map<String,String> query(String orderId, String txnTime){
		Map<String, String> data = new HashMap<String, String>();
		
		data.put("version", SDKConfig.getConfig().getVersion());                 
		data.put("encoding", SDKConfig.getConfig().getEncoding_UTF8());               
		data.put("signMethod", "01");                          
		data.put("txnType", "00");                            
		data.put("txnSubType", "00");                          
		data.put("bizType", "000201");                         
		
		/***商户接入参数***/
		data.put("merId", SDKConfig.getConfig().getMerId());               
		data.put("accessType", "0");                           
		
		/***要调通交易以下字段必须修改***/
		data.put("orderId", orderId);                 
		data.put("txnTime", txnTime);                 //****订单发送时间，每次发交易测试需修改为被查询的交易的订单发送时间

		/**请求参数设置完毕，以下对请求参数进行签名并发送http post请求，接收同步应答报文------------->**/
		
		Map<String, String> reqData = AcpService.sign(data,SDKConfig.getConfig().getEncoding_UTF8());
		
		Map<String, String> rspData = AcpService.post(reqData,
				SDKConfig.getConfig().getSingleQueryUrl(),SDKConfig.getConfig().getEncoding_UTF8());
		return rspData;
	}
	
	//判断前台返回的商户地址
	protected static String getFrontUrl(String transactionType, String url){
		if(url.contains(",")){
			String[] urls = url.split("\\,");
			if("GENERAL".equals(transactionType)){
				return urls[0];
			}else{
				return urls[1];
			}
		}else{
			return url;
		}
		
	}
}

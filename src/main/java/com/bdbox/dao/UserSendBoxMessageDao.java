package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.UserSendBoxMessage;

public interface UserSendBoxMessageDao extends IDao<UserSendBoxMessage, Long> {

	public List<UserSendBoxMessage> sdkUserSend(Long serialNumber,Calendar startTime,Calendar endTime,Long userid,String order, Integer page,Integer pageSize);

	public Integer count(Long serialNumber,Calendar startTime,Calendar endTime,Long userid);

}

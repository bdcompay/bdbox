package com.bdbox.dao;

import java.util.List;

import com.bdbox.constant.UserStatusType;
import com.bdbox.constant.UserType;
import com.bdbox.entity.RegisterInformation;
import com.bdbox.entity.User;

public interface UserDao extends IDao<User, Long> {
	public User queryUser(UserType userType, String username, String password,
			UserStatusType userStatusType, String mobKey, String mail);

	/**
	 * 查询
	 * @param userType
	 * @param username
	 * @param password
	 * @param userStatusType
	 * @param mobKey
	 * @param mail
	 * @param order
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<User> query(UserType userType, String username,
			String password, UserStatusType userStatusType, String mobKey,
			String mail, String order, int page, int pageSize);
	
	/**
	 * 数量
	 * @param userType	用户类型
	 * @param username	手机号
	 * @param password	密码
	 * @param userStatusType	用户状态
	 * @param mobKey	验证码
	 * @param mail	邮箱
	 * @return
	 */
	public Integer queryCount(UserType userType, String username,
			String password, UserStatusType userStatusType, String mobKey,
			String mail);
	/**
	 * 通过userPowerKey获取用户
	 * @param userPowerKey
	 * @return
	 */
	public User queryUser(String userPowerKey);
	  
	public User getUserByusername(String user);
	
	public List<User> getRealUser(int page, int pageSize);
	
	public Integer getRealUserCount();
	
	public List<User> queryuser_twl(int page, int pageSize);
	
	public List<User> getRefeesUser(String usernomber,String refereespeoplename,int page, int pageSize);

	public Integer getRefeesUserCount(String usernomber,String refereespeoplename);

	public User getUserByuser(String user);
	
	/**
	 * 获取与驴讯通绑定的盒子帐号
	 * @param lxtAccount
	 * @return
	 */
	public User getUserForLxtAccount(String lxtAccount);

	/**
	 * 绑定
	 * @param account
	 * @param lxtAccount
	 */
	public void bindLxtAccount(String account, String lxtAccount);
	
	/**
	 * 解绑
	 * @param account
	 */
	public void unbindLxtAccount(String account);
	
}

package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.UserRegLog;

/**
 * 用户注册记录DAO接口
 * 
 * @ClassName: UserRegLogDao 
 * @author lijun.jiang@bdwise.com 
 * @date 2016-2-2 上午11:23:48 
 * @version V5.0 
 */
public interface UserRegLogDao extends IDao<UserRegLog, Long> {
	/**
	 * 条件分页查询
	 * 
	 * @param username
	 * 		用户名
	 * @param ip
	 * 		注册ip
	 * @param ipArea
	 * 		ip地区
	 * @param startTime
	 * 		起始时间
	 * @param endTime
	 * 		截止时间
	 * @param userid
	 * 		创建人
	 * @param page
	 * 		查询页数
	 * @param pageSize
	 * 		查询记录数
	 * @return
	 */
	public List<UserRegLog> listUserRegLog(String username,String ip,
			String ipArea,Calendar startTime,Calendar endTime,Long userid,int page,int pageSize);
	
	/**
	 * 
	 * @param username
	 * 		用户名
	 * @param ip
	 * 		注册ip
	 * @param ipArea
	 * 		ip地区
	 * @param startTime
	 * 		起始时间
	 * @param endTime
	 * 		截止时间
	 * @param userid
	 * 		创建人
	 * @return
	 */
	public int count(String username,String ip,
			String ipArea,Calendar startTime,Calendar endTime,Long userid);
}

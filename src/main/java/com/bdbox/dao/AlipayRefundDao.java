package com.bdbox.dao;

import com.bdbox.entity.AlipayOrder;
import com.bdbox.entity.AlipayRefund;
import com.bdbox.entity.Order;
import com.bdbox.entity.Returns;

public interface AlipayRefundDao extends IDao<AlipayRefund, Long>{
	
	public void saveOrUpdate(AlipayRefund alipayRefund);
	
	public AlipayOrder getAlipayOrder(String outTradeNo);
	
	public Order getOrder(String tradeNo);
	
	public AlipayRefund getAlipayRefund(String tradeNo);
	
	public Returns getReturns(String tradeNo);
}

package com.bdbox.dao;

import com.bdbox.entity.Apply;
import java.util.Calendar;
import java.util.List;

public abstract interface ApplyDao extends IDao<Apply, Long>
{
	/**
	 * 查询
	 * @param code	户外代码
	 * @param phone	手机号码
	 * @param createTime	申请时间
	 * @param sex	性别
	 * @param join	试用
	 * @param page	页号
	 * @param pageSize	页大小
	 * @param order	排序
	 * @return	List
	 */
  public abstract List<Apply> listApply(String paramString1, String paramString2, Calendar paramCalendar, String paramString3, Boolean paramBoolean, Integer paramInteger1, Integer paramInteger2, String paramString4);

  /**
	 * 数量
	 * @param code	户外代码
	 * @param phone	手机号码
	 * @param createTime	申请时间
	 * @param sex	性别
	 * @param join	试用
	 * @return
	 */
  public abstract int applyCount(String paramString1, String paramString2, Calendar paramCalendar, String paramString3, Boolean paramBoolean);

  /**
	 * 根据手机号码获取订单
	 * @param phone
	 * @return
	 */
  public abstract Apply getApply(String paramString);

  /**
	 * 根据用户id查询订单
	 * @param uid
	 * @return
	 */
  public abstract Apply getApplyUid(Long paramLong);
}
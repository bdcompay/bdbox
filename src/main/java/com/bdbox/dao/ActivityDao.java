package com.bdbox.dao;

import com.bdbox.entity.Activity;
 
public interface ActivityDao extends IDao<Activity, Long> {

	public int counts(String sql);
}

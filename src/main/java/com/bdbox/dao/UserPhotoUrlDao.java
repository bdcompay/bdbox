package com.bdbox.dao;

import java.util.List;

import com.bdbox.entity.UserPhotoUrl;
import com.bdbox.entity.UserSendBoxMessage;

public interface UserPhotoUrlDao extends IDao< UserPhotoUrl, Long>  {

	public UserPhotoUrl getUserPhotoUrl(Long userid);
	
	public  UserPhotoUrl  getRealNameUrl(long userid);

}

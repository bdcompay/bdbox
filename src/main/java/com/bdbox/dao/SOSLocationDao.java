package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.SOSLocation;

public interface SOSLocationDao extends IDao<SOSLocation, Long>{
	 
	 /**
	  * 根据盒子ID获取位置信息
	  * @param boxId
	  * @return
	  */
	 public SOSLocation getLocationForBoxId(Long boxId);
	 
	 /**
	  * 根据盒子ID获取该盒子的所有位置信息
	  * @param boxId
	  * @return
	  */
	 public List<SOSLocation> getLocationsForBoxId(Long boxId);
	 
	 /**
	  * 获取半小时内的求救信号
	  * @return
	  */
	 public List<SOSLocation> getLocation();
	 
	 /**
	  * 获取半小时内的求救信号
	  * @return
	  */
	 public SOSLocation getLocation(Long boxId);
	 
	 /**
	  * 获取企业用户下属卡所有报警信息
	  * @param json
	  * @return
	  */
	 public List<SOSLocation> getSosLocation(List<Long> ids);
	 
	 /**
	  * 企业用户分页获取sos位置信息
	  * @param ids
	  * @param startTime
	  * @param endTime
	  * @param page
	  * @param pageSize
	  * @return
	  */
	 public List<SOSLocation> getSosLocations(List<Long> ids, Calendar startTime, 
			 Calendar endTime);
	 
	 /**
	  * 统计数量
	  * @param ids
	  * @param startTime
	  * @param endTime
	  * @return
	  */
	 public int queryCount(List<Long> ids, Calendar startTime, Calendar endTime);
	 /**
	  * 根据盒子的删除
	  * @param boxId
	  */
	 public void deleByBoxId(long boxId);
}

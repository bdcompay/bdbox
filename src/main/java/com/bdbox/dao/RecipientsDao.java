package com.bdbox.dao;

import com.bdbox.entity.Recipients;

public interface RecipientsDao extends IDao<Recipients, Long>{

	public void updateDefault(Long id);
	
	public void setZero();
	
	public Long count();
	
	public Recipients getRecipients();
}

package com.bdbox.dao;

import com.bdbox.entity.UserSuggestion;
import java.util.List;

public abstract interface UserSuggestionDao extends IDao<UserSuggestion, Long>
{
  public abstract List<UserSuggestion> listUserSuggestions(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2);

  public abstract int listUserSuggestionsCount(String paramString);
}
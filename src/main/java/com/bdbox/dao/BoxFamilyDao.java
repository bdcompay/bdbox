package com.bdbox.dao;

import java.util.List;

import com.bdbox.entity.BoxFamily;

public interface BoxFamilyDao extends IDao<BoxFamily, Long> {

	public List<BoxFamily> query(Long boxId, String familyMob, int page,
			int pageSize);

	public int queryAmount(Long boxId, String familyMob);

}

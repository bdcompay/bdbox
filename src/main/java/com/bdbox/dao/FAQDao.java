package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.constant.ReturnsStatusType;
import com.bdbox.entity.FAQ;
 
public interface FAQDao extends IDao<FAQ, Long> {

	public List<FAQ> getlistFAQtable(String questiontype,int page, int pageSize);

	public Integer getlistFAQtable(String questiontype);
	
	public List<FAQ> getFAQlist(String questiontype);
	
	public List<FAQ> getfaqlist(String questiontype,String smallTypeid);
	
	public List<FAQ> getFAQquestionlist(String lefttype,String keyword, String toptype,int page,int pageSize);
	
	public int counts(String lefttype,String keyword, String toptype);


}

package com.bdbox.dao.impl;

import java.util.List; 

import org.springframework.stereotype.Repository; 

import com.bdbox.dao.SmallTypeDao;
import com.bdbox.entity.Express;
import com.bdbox.entity.Notification;
import com.bdbox.entity.SmallType;

@Repository
public class SmallTypeDaoImpl extends IDaoImpl<SmallType, Long> implements SmallTypeDao {

	public List<SmallType> gettypelist(String questiontype){
 		String hql = "from SmallType t where t.questionid='" + questiontype+"'";
		return getList(hql.toString(), 0, 0, new Object[0]);
	}
	
	public SmallType gettype(String addtype,String Englishtypename){  
		StringBuffer hql = new StringBuffer("from SmallType t where 1=1");
		if (addtype != null) {
			hql.append(" and t.questionid='").append(addtype)
					.append("'");
		}
		if (Englishtypename != null) {
			hql.append(" and t.smallType='").append(Englishtypename).append("'");
		}
		List<SmallType> smallType = getList(hql.toString(),1,9000);
		if(smallType.size()>0){
			return smallType.get(0);
		}else{
			return null;
		}
		
	}
}

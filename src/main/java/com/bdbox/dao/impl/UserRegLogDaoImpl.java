package com.bdbox.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.UserRegLogDao;
import com.bdbox.entity.UserRegLog;
import com.bdsdk.util.CommonMethod;

/**
 * UserRegLogDao实现类
 * 
 * @ClassName: UserRegLogDaoImpl 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-2-2 下午1:54:26 
 * @version V5.0 
 */
@Repository
public class UserRegLogDaoImpl extends IDaoImpl<UserRegLog, Long> implements UserRegLogDao {

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.dao.UserRegLogDao#listUserRegLog
	 */
	@Override
	public List<UserRegLog> listUserRegLog(String username, String ip,
			String ipArea, Calendar startTime, Calendar endTime,Long userid, int page,
			int pageSize) {
		StringBuffer hql = new StringBuffer("from UserRegLog where 1=1");
		if(username!=null){
			hql.append(" and username like '%").append(username).append("%'");
		}
		if(ip!=null){
			hql.append(" and ip='").append(ip).append("'");
		}
		if(ipArea!=null){
			hql.append(" and ipArea like '%").append(ipArea).append("%'");
		}
		if(userid!=null){
			hql.append(" and userid=").append(userid);
		}
		if (startTime != null) {
			hql.append(" and createdtime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd ")).append("'");
		}
		if (endTime != null) {
			hql.append(" and createdtime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd")).append("'");
		}
		hql.append(" order by id desc");
		return getList(hql.toString(), page, pageSize);
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.dao.UserRegLogDao#count
	 */
	@Override
	public int count(String username, String ip, String ipArea,
			Calendar startTime, Calendar endTime,Long userid) {
		StringBuffer hql = new StringBuffer("select count(*) from UserRegLog where 1=1");
		if(username!=null){
			hql.append(" and username like '%").append(username).append("%'");
		}
		if(ip!=null){
			hql.append(" and ip='").append(ip).append("'");
		}
		if(ipArea!=null){
			hql.append(" and ipArea like '%").append(ipArea).append("%'");
		}
		if(userid!=null){
			hql.append(" and userid=").append(userid);
		}
		if (startTime != null) {
			hql.append(" and createdtime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd ")).append("'");
		}
		if (endTime != null) {
			hql.append(" and createdtime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd")).append("'");
		}
		return count(hql.toString());
	}

}

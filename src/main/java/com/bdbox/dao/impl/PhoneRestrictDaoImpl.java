package com.bdbox.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.PhoneRestrictDao;
import com.bdbox.entity.PhoneRestrict;

@Repository
public class PhoneRestrictDaoImpl extends IDaoImpl<PhoneRestrict, Long>
		implements PhoneRestrictDao{

	@Override
	public PhoneRestrict getPhone(String phoneNum) {
		String hql = "from PhoneRestrict p where p.phoneNum = ?";
		return unique(hql, new Object[]{phoneNum});
	}

	@Override
	public int count() {
		String hql = "select count(*) from PhoneRestrict p";
		return count(hql);
	}

}

package com.bdbox.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.constant.DealStatus;
import com.bdbox.constant.ProductType;
import com.bdbox.dao.OrderDao;
import com.bdbox.entity.Order;
import com.bdsdk.util.CommonMethod;

@Repository
public class OrderDaoImpl extends IDaoImpl<Order, Long> implements OrderDao {
	public List<Order> listOrder(String user, ProductType productType,
			String productName, Calendar buyTime, String orderNo,
			DealStatus dealStatus, Integer page, Integer pageSize, String order,String transactionType,String dealStatusStrbd,String name) {
		StringBuffer hql = new StringBuffer("from Order t where 1=1");
		if (buyTime != null)
			hql.append(" and t.buyTime like '%")
					.append(CommonMethod
							.CalendarToString(buyTime, "yyyy-MM-dd"))
					.append("%'");
		if (productName != null)
			hql.append(" and t.productName like '%").append(productName)
					.append("%'");
		if (orderNo != null)
			hql.append(" and t.orderNo = '").append(orderNo).append("'");
		if (user != null)
			hql.append(" and t.user = '").append(user).append("'");
		if (productType != null)
			hql.append(" and t.productType = '").append(productType)
					.append("'");
		if (dealStatus != null)
			hql.append(" and t.dealStatus = '").append(dealStatus).append("'");
		if(transactionType!=null)
			hql.append(" and t.transactionType!='").append(transactionType).append("'");  
		if(dealStatusStrbd!=null){
			hql.append(" and t.orderType = '").append(dealStatusStrbd).append("'"); 
		}
		if(name!=null && !"".equals(name)){
			hql.append(" and t.name like '%").append(name).append("%'");
		}
		if (order != null)
			hql.append(" order by ").append(order);
		return getList(hql.toString(), page.intValue(), pageSize.intValue(),
				new Object[0]);
	}

	public int listOrderCount(String user, ProductType productType,
			String productName, Calendar buyTime, String orderNo,
			DealStatus dealStatus,String transactionType,String dealStatusStrbd, String name) {
		StringBuffer hql = new StringBuffer(
				"select count(*) from Order t where 1=1");
		if (buyTime != null)
			hql.append(" and t.createdTime like '%")
					.append(CommonMethod
							.CalendarToString(buyTime, "yyyy-MM-dd"))
					.append("%'");
		if (productName != null)
			hql.append(" and t.productName like '%").append(productName)
					.append("%'");
		if (orderNo != null)
			hql.append(" and t.orderNo = '").append(orderNo).append("'");
		if (user != null)
			hql.append(" and t.user = '").append(user).append("'");
		if (productType != null)
			hql.append(" and t.productType = '").append(productType)
					.append("'");
		if (dealStatus != null){
			hql.append(" and t.dealStatus = '").append(dealStatus).append("'"); 
		}
		if(transactionType!=null) 
			hql.append(" and t.transactionType!='").append(transactionType).append("'"); 
		if(dealStatusStrbd!=null){
			hql.append(" and t.orderType = '").append(dealStatusStrbd).append("'");  
		}
		if(name!=null && !"".equals(name)){
			hql.append(" and t.name like '%").append(name).append("%'");
		}
		return count(hql.toString());
	}

	public List<Order> getUserOrder(String user) {
		String hql = "from Order t where t.user='" + user
				+ "' and t.cancelstate='no' and t.transactionType='GENERAL' order by t.orderNo desc ";
		return getList(hql.toString(), 0, 0, new Object[0]);
	}

	public List<Order> listAllSendOrder() {
		return getList("from Order t where 1=1 t.dealStatus='SENT'", 1, 1,
				new Object[0]);
	}

	public void updateOrderByTradeNo(String tradeNo) {
		DateFormat dateTimeformat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String strBeginDate = dateTimeformat.format(new Date());
		StringBuffer hql = new StringBuffer(
				"update Order o set o.dealStatus ='PAID',o.buyTime='"
						+ strBeginDate + "' where o.trade_no ='").append(
				tradeNo).append("'");
		excuteHQL(hql.toString());
	}

	public List<Order> getOder(String orderNo) {
		String hql = "from Order t where t.orderNo='" + orderNo + "'";
		return getList(hql.toString(), 0, 0, new Object[0]);
	}

	@Override
	public List<Order> queryOrderByPidUser(long pid, String username) {
		String hql = "from Order t where t.productId=" + pid + " and  t.user='"+username+"'";
		return getList(hql.toString(), 1, 10);
	}
	
	  public Order getOrderstatus(String orderNo){
		  String hql = "from Order t where t.orderNo = '"+orderNo+"'";
			return unique(hql);
	  }
	  
	 //租用订单
	  public List<Order> listOrder2(String user, ProductType productType, String productName, Calendar buyTime, 
			  String orderNo, DealStatus dealStatus, Integer page, Integer pageSize, String order,
			  String transactionType,String dealStatusStrbd,String name) {
			StringBuffer hql = new StringBuffer("from Order t where 1=1");
			if (buyTime != null)
				hql.append(" and t.buyTime like '%")
						.append(CommonMethod
								.CalendarToString(buyTime, "yyyy-MM-dd"))
						.append("%'");
			if (productName != null)
				hql.append(" and t.productName like '%").append(productName)
						.append("%'");
			if (orderNo != null)
				hql.append(" and t.orderNo = '").append(orderNo).append("'");
			if (user != null)
				hql.append(" and t.user = '").append(user).append("'");
			if (productType != null)
				hql.append(" and t.productType = '").append(productType)
						.append("'");
			if (dealStatus != null) 
				hql.append(" and t.dealStatus = '").append(dealStatus).append("'"); 
			if(transactionType!=null)
				hql.append(" and t.transactionType like '%").append(transactionType).append("%'"); 
			if(dealStatusStrbd!=null){
				hql.append(" and t.orderType = '").append(dealStatusStrbd).append("'");  
			}
			if(name!=null && !"".equals(name)){
				hql.append(" and t.name like '%").append(name).append("%'");
			}
			if (order != null)
				hql.append(" order by ").append(order);
			return getList(hql.toString(), page.intValue(), pageSize.intValue(),
					new Object[0]);
		}
	  
	  
	  public int listOrderCount2(String user, ProductType productType,
				String productName, Calendar buyTime, String orderNo,
				DealStatus dealStatus,String transactionType,String dealStatusStrbd,String name) {
			StringBuffer hql = new StringBuffer(
					"select count(*) from Order t where 1=1");
			if (buyTime != null)
				hql.append(" and t.createdTime like '%")
						.append(CommonMethod
								.CalendarToString(buyTime, "yyyy-MM-dd"))
						.append("%'");
			if (productName != null)
				hql.append(" and t.productName like '%").append(productName)
						.append("%'");
			if (orderNo != null)
				hql.append(" and t.orderNo = '").append(orderNo).append("'");
			if (user != null)
				hql.append(" and t.user = '").append(user).append("'");
			if (productType != null)
				hql.append(" and t.productType = '").append(productType)
						.append("'");
			if (dealStatus != null){
				hql.append(" and t.dealStatus = '").append(dealStatus).append("'");
			}
			if(transactionType!=null)
				hql.append(" and t.transactionType like '%").append(transactionType).append("%'");
			if(dealStatusStrbd!=null){
				hql.append(" and t.orderType = '").append(dealStatusStrbd).append("'"); 
			}
			if(name!=null && !"".equals(name)){
				hql.append(" and t.name like '%").append(name).append("%'");
			}
			return count(hql.toString());
		}
	  
	  
	  public List<Order> getUserRentOrder(String user) {
			String hql = "from Order t where t.user='" + user
					+ "' and t.cancelstate='no' and t.transactionType='RENT' order by t.orderNo desc ";
			return getList(hql.toString(), 0, 0, new Object[0]);
		}
	  
	  public List<Order> queryBdcood(){
		  String hql = "from Order t where t.orderType ='BDORDER' and t.dealStatus ='CANCELORDER' ";
			return getList(hql.toString(), 0, 0, new Object[0]);
	  }
	  
	  public List<Order> getOrderList(String deal,String trandtype){
		  String hql = "from Order t where t.dealStatus='" + deal
					+ "' and t.transactionType='"+trandtype+"' order by t.orderNo desc ";
			return getList(hql.toString(), 0, 0, new Object[0]);
	  }
	  
	  public Order getOrd(String tranOrder){
		  String hql = "from Order t where t.trade_no = '"+tranOrder+"'";
			return unique(hql);
	  }

	@Override
	public List<Order> getUnionPayRentOrders() {
		String hql = "from Order o where o.transactionType = 'RENT' "
				+ "and o.beginTime is not null and o.endTime is null and o.payType = 'UNIONPAY'";
		return getList(hql, 0, 0, new Object[0]);
	}

	@Override
	public int statisticOrder(Calendar startTime, Calendar endTime, String transactionType) {
		String hql = "select count(*) from Order o where o.transactionType = '"+transactionType+"' and o.dealStatus ='DEALCLOSE'"
				+ " and o.createdTime between '"+CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")+"'"
				+ " and '"+CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")+"' order by o.createdTime desc";
		return count(hql);
	}
}

package com.bdbox.dao.impl;

import com.bdbox.dao.UserSuggestionDao;
import com.bdbox.entity.UserSuggestion;
import java.util.List;
import org.springframework.stereotype.Repository;

@Repository
public class UserSuggestionDaoImpl extends IDaoImpl<UserSuggestion, Long>
  implements UserSuggestionDao
{
  public List<UserSuggestion> listUserSuggestions(String fromStr, String order, Integer page, Integer pageSize)
  {
    StringBuffer hql = new StringBuffer("from UserSuggestion t where 1=1");
    if (fromStr != null) hql.append(" and t.fromStr like'%").append(fromStr).append("%'");
    if (order != null) hql.append(" order by t.").append(order).append(" desc");
    return getList(hql.toString(), page.intValue(), pageSize.intValue(), new Object[0]);
  }

  public int listUserSuggestionsCount(String fromStr)
  {
    StringBuffer hql = new StringBuffer("select count(*) from UserSuggestion t where 1=1");
    if (fromStr != null) hql.append(" and t.fromStr like'%").append(fromStr).append("%'");
    return count(hql.toString());
  }
}
package com.bdbox.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.QualificationCheckDao;
import com.bdbox.entity.QualificationCheck;

@Repository
public class QualificationCheckDaoImpl extends IDaoImpl<QualificationCheck, Long>
		implements QualificationCheckDao{

	@Override
	public List<QualificationCheck> query(int page, int pageSize) {
		String hql = "from QualificationCheck q order by q.createTime desc ";
		return getList(hql, page, pageSize);
	}
	
	@Override
	public int queryAcount() {
		String hql = "select count(*) from QualificationCheck";
		return count(hql);
	}
	
	public QualificationCheck get(Long id){
		String hql = "from QualificationCheck where id = " + id;
		return (QualificationCheck) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public QualificationCheck getQualificationCheck(Long invoiceId) {
		String hql = "from QualificationCheck where invoice = " + invoiceId;
		return (QualificationCheck) getSession().createQuery(hql).uniqueResult();
	}
	
	

}

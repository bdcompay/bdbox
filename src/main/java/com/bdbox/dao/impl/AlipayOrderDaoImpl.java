package com.bdbox.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.AlipayOrderDao;
import com.bdbox.entity.AlipayOrder;

@Repository
public class AlipayOrderDaoImpl extends IDaoImpl<AlipayOrder, Long>
		implements AlipayOrderDao{

	@Override
	public void saveOrUpdate(AlipayOrder alipayOrder) {
		getSession().saveOrUpdate(alipayOrder);
	}

	@Override
	public AlipayOrder getOrderForTradeNo(String tradeNo) {
		String hql = "from AlipayOrder where tradeNo = '"+tradeNo+"'";
		return (AlipayOrder) getSession().createQuery(hql).uniqueResult();
	}

}

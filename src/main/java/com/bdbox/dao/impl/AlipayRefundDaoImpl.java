package com.bdbox.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.AlipayRefundDao;
import com.bdbox.entity.AlipayOrder;
import com.bdbox.entity.AlipayRefund;
import com.bdbox.entity.Order;
import com.bdbox.entity.Returns;

@Repository
public class AlipayRefundDaoImpl extends IDaoImpl<AlipayRefund, Long> 
		implements AlipayRefundDao{

	@Override
	public AlipayOrder getAlipayOrder(String outTradeNo) {
		String hql = "from AlipayOrder a where a.outTradeNo = '"+outTradeNo+"'";
		return (AlipayOrder) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public AlipayRefund getAlipayRefund(String tradeNo) {
		String hql = "from AlipayRefund a where a.tradeNo = '"+tradeNo+"'";
		return (AlipayRefund) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public void saveOrUpdate(AlipayRefund alipayRefund) {
		getSession().saveOrUpdate(alipayRefund);
	}

	@Override
	public Order getOrder(String tradeNo) {
		String hql = "from Order o where o.trade_no = '"+tradeNo+"'";
		return (Order) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public Returns getReturns(String orderNo) {
		String hql = "from Returns r where r.orderNum = '"+orderNo+"'";
		return (Returns) getSession().createQuery(hql).uniqueResult();
	}

	
}

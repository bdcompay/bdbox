package com.bdbox.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository; 

import com.bdbox.dao.FAQDao;
import com.bdbox.entity.FAQ;
import com.bdsdk.util.CommonMethod;
 
@Repository
public class FAQDaoImpl extends IDaoImpl<FAQ, Long> implements FAQDao {

	public List<FAQ> getlistFAQtable(String questiontype,int page, int pageSize){
		StringBuffer hql = new StringBuffer("from FAQ where 1=1 "); 
		if(questiontype!=null){
			hql.append(" and questionType ='"+questiontype+"'");
 		}
		hql.append(" order by creatTime desc");
		List<FAQ> results = getList(hql.toString(), page, pageSize);
		return results;
 	} 

	public Integer getlistFAQtable(String questiontype){
		StringBuffer hql = new StringBuffer("select count(*) from FAQ where 1=1 "); 
		if(questiontype!=null){
			hql.append(" and questionType ='"+questiontype+"'");
 		}
		hql.append(" order by creatTime desc");
		return  count(hql.toString());
	}
	
	public List<FAQ> getFAQlist(String questiontype){
 		String hql = "from FAQ t where t.smallTypeid='" + questiontype;
		return getList(hql.toString(), 0, 0, new Object[0]);
	}
	
	public List<FAQ> getfaqlist(String questiontype,String smallTypeid){
		StringBuffer hql = new StringBuffer("from FAQ where 1=1 "); 
		if(questiontype!=null){
			hql.append(" and questionType ='"+questiontype+"'");
 		}
		if(smallTypeid!=null){
			hql.append(" and smallTypeid ='"+smallTypeid+"'");
 		}
 		List<FAQ> results = getList(hql.toString(), 1, 9999999);
		return results;
	}
	
	public List<FAQ> getFAQquestionlist(String lefttype,String keyword, String toptype,int page,int pageSize){
		StringBuffer hql = new StringBuffer("from FAQ where 1=1 "); 
		if(lefttype!=null){
			hql.append(" and questionType ='"+lefttype+"'");
 		}
		if(keyword!=null){
			hql.append(" and question like '%"+keyword+"%'");
 		}
		if(toptype!=null){
			hql.append(" and smallTypeid ='"+toptype+"'");
 		}
 		List<FAQ> results = getList(hql.toString(), page, pageSize);
		return results;
	}
	
	public int counts(String lefttype,String keyword, String toptype){
		StringBuffer hql = new StringBuffer("select count(*) from FAQ where 1=1 ");
		if(lefttype!=null){
			hql.append(" and questionType ='"+lefttype+"'");
 		}
		if(keyword!=null){
			hql.append(" and question like '%"+keyword+"%'");
 		}
		if(toptype!=null){
			hql.append(" and smallTypeid ='"+toptype+"'");
 		}
		return count(hql.toString());
	}




}

package com.bdbox.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.BoxLocationDao;
import com.bdbox.entity.BoxLocation;
import com.bdsdk.util.CommonMethod;

@Repository
public class BoxLocationDaoImpl extends IDaoImpl<BoxLocation, Long> implements
		BoxLocationDao {

	public List<BoxLocation> listBoxLocations(Long boxId, String cardNumber,
			String boxName, String userName, String boxSerialNumber,
			Calendar createdTime, String order, Integer page, Integer pageSize) {
		StringBuffer hql = new StringBuffer("from BoxLocation t where 1=1");
		if(boxId!=null){
			hql.append(" and t.box.id='").append(boxId).append("'");
		}
		if(cardNumber!=null){
			hql.append(" and t.box.cardNumber='").append(cardNumber).append("'");
		}
		if(boxName!=null){
			hql.append(" and t.box.name like '%").append(boxName).append("%'");
		}
		if(userName!=null){
			hql.append(" and t.box.user.username like '%").append(userName).append("%'");
		}
		if(boxSerialNumber!=null){
			hql.append(" and t.box.boxSerialNumber='").append(boxSerialNumber).append("'");
		}
		if(createdTime!=null){
			hql.append(" and t.createdTime like '%").append(CommonMethod.CalendarToString(createdTime, "yyyy-MM-dd")).append("%'");
		}
		if(order!=null){
			hql.append(" order by t.").append(order);
		}
		return getList(hql.toString(), page, pageSize);
	}

	public Integer listBoxLocationsCount(Long boxId, String cardNumber,
			String boxName, String userName, String boxSerialNumber,
			Calendar createdTime) {
		StringBuffer hql = new StringBuffer("select count(*) from BoxLocation t where 1=1");
		if(boxId!=null){
			hql.append(" and t.box.id='").append(boxId).append("'");
		}
		if(cardNumber!=null){
			hql.append(" and t.box.cardNumber='").append(cardNumber).append("'");
		}
		if(boxName!=null){
			hql.append(" and t.box.name like '%").append(boxName).append("%'");
		}
		if(userName!=null){
			hql.append(" and t.box.user.username like '%").append(userName).append("%'");
		}
		if(boxSerialNumber!=null){
			hql.append(" and t.box.boxSerialNumber='").append(boxSerialNumber).append("'");
		}
		if(createdTime!=null){
			hql.append(" and t.createdTime like '%").append(CommonMethod.CalendarToString(createdTime, "yyyy-MM-dd")).append("%'");
		}
		return count(hql.toString());
	}

	/*
	 * 条件分页查询盒子位置记录
	 * (non-Javadoc)
	 * @see com.bdbox.dao.BoxLocationDao#queryBoxLocations(java.lang.Long, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar, java.lang.String, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<BoxLocation> queryBoxLocations(Long boxId, String cardNumber,
			String boxName, String userName, String boxSerialNumber,
			Calendar startTime, Calendar endTime,Long userId, String order, Integer page,
			Integer pageSize) {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer("from BoxLocation t where 1=1");
		if(boxId!=null){
			hql.append(" and t.box.id='").append(boxId).append("'");
		}
		if(cardNumber!=null){
			hql.append(" and t.box.cardNumber='").append(cardNumber).append("'");
		}
		if(boxName!=null){
			hql.append(" and t.box.name like '%").append(boxName).append("%'");
		}
		if(userName!=null){
			hql.append(" and t.box.user.username like '%").append(userName).append("%'");
		}
		if(boxSerialNumber!=null){
			hql.append(" and t.box.boxSerialNumber='").append(boxSerialNumber).append("'");
		}
		if(startTime!=null){
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(startTime!=null){
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(userId != null){
			hql.append(" and t.user.id<='").append(userId).append("'");
		}
		if(order!=null){
			hql.append(" order by t.").append(order);
		}
		return getList(hql.toString(), page, pageSize);
	}

	/*
	 * 条件统计盒子位置记录数量
	 * (non-Javadoc)
	 * @see com.bdbox.dao.BoxLocationDao#amount(java.lang.Long, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public int amount(Long boxId, String cardNumber, String boxName,
			String userName, String boxSerialNumber, Calendar startTime,
			Calendar endTime,Long userId) {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer("select count(*) from BoxLocation t where 1=1");
		if(boxId!=null){
			hql.append(" and t.box.id='").append(boxId).append("'");
		}
		if(cardNumber!=null){
			hql.append(" and t.box.cardNumber='").append(cardNumber).append("'");
		}
		if(boxName!=null){
			hql.append(" and t.box.name like '%").append(boxName).append("%'");
		}
		if(userName!=null){
			hql.append(" and t.box.user.username like '%").append(userName).append("%'");
		}
		if(boxSerialNumber!=null){
			hql.append(" and t.box.boxSerialNumber='").append(boxSerialNumber).append("'");
		}
		if(startTime!=null){
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(startTime!=null){
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(userId != null){
			hql.append(" and t.user.id<='").append(userId).append("'");
		}
		return count(hql.toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BoxLocation> getBoxLocation(Long boxId, Calendar startTime,
			Calendar endTime,Long userId) {
		StringBuilder hql = new StringBuilder("from BoxLocation b where 1=1");
		if(boxId!=null){
			hql.append(" and b.box.id=").append(boxId);
		}
		if(startTime!=null){
			hql.append(" and b.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(endTime!=null){
			hql.append(" and b.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(userId != null){
			hql.append(" and b.user.id<='").append(userId).append("'");
		}
		
		hql.append(" order by b.createdTime asc ");
		
		return getSession().createQuery(hql.toString()).list();
	}

	@Override
	public List<BoxLocation> getAllLocation(Long id) {
		String hql = "from BoxLocation b where 1=1 and b.box.id = ? order by b.createdTime desc";
		return getList(hql.toString(), 1, 3, new Object[]{id});
	}

	@Override
	public List<BoxLocation> getEntUserLocation(Long boxId, Calendar startTime,
			Calendar endTime, Integer page, Integer pageSize) {
		StringBuilder hql = new StringBuilder("from BoxLocation b where 1=1");
		if(boxId!=null){
			hql.append(" and b.box.id=").append(boxId);
		}
		if(startTime!=null){
			hql.append(" and b.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(endTime!=null){
			hql.append(" and b.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		hql.append(" order by b.createdTime desc");
		
		return getList(hql.toString(), page, pageSize);
	}

	@Override
	public List<BoxLocation> getEntUserLocations(Calendar startTime, Calendar endTime, List<Long> ids) {
		if(ids.size()>0){
			StringBuffer hql = new StringBuffer("from BoxLocation b where 1=1");
			for (Long id : ids) {
				if(ids.indexOf(id)==0){
					hql.append(" and b.box.id = "+id);
				}else{
					hql.append(" or b.box.id = "+id);
				}
			}
			if(startTime!=null){
				hql.append(" and b.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
			}
			if(endTime!=null){
				hql.append(" and b.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
			}
			hql.append(" order by b.createdTime desc");
			return getList(hql.toString(), 1, 9999);
		}else{
			return new ArrayList<>();
		}
	}

	@Override
	public int loccount(List<Long> ids) {
		if(ids.size()>0){
			StringBuffer hql = new StringBuffer("select count(*) from BoxLocation b where 1=1");
			for (Long id : ids) {
				if(ids.indexOf(id)==0){
					hql.append(" and b.box.id = "+id);
				}else{
					hql.append(" or b.box.id = "+id);
				}
			}
			return count(hql.toString());
		}else{
			return 0;
		}
	}
}

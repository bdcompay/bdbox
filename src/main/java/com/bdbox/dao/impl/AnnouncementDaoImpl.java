package com.bdbox.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.AnnouncementDao;
import com.bdbox.entity.Announcement;

@Repository
public class AnnouncementDaoImpl extends IDaoImpl<Announcement, Long> implements AnnouncementDao {

	public List<Announcement> getlistAnnouncementtable(int page, int pageSize){
		StringBuffer hql = new StringBuffer("from Announcement t where 1=1 order by t.creatTime desc");
		return getList(hql.toString(), page, pageSize, new Object[0]);
	}
	
	public Integer getlistAnnouncementtableCount(){
		StringBuffer hql = new StringBuffer("select count(*) from Announcement t where 1=1 order by t.creatTime desc");
		return count(hql.toString());
	}
	
	@SuppressWarnings("unchecked")
	public List<Announcement> getfindAnnouncement(){
		StringBuffer hql = new StringBuffer("from Announcement t where 1=1 order by t.creatTime desc");
		return getSession().createQuery(hql.toString()).list();
	}

	@Override
	public List<Announcement> getList(String content, int page, int pageSize) {
		StringBuffer hql = new StringBuffer("from Announcement a where 1=1");
		if(content != null && content != ""){
			hql.append(" and a.center like '%"+content+"%'");
		}
		hql.append(" order by a.creatTime desc");
		return getList(hql.toString(), page, pageSize, new Object[0]);
	} 
	
	@Override
	public int announcementCount(String content){
		StringBuffer hql = new StringBuffer("select count(*) from Announcement a where 1=1");
		if(content != null && content != ""){
			hql.append(" and a.center like '%"+content+"%'");
		}
		return count(hql.toString());
	}

}

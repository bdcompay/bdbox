package com.bdbox.dao.impl;

import java.util.Calendar;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.EntUserPushLocationDao;
import com.bdbox.entity.EntUserPushLocation;
import com.bdsdk.util.CommonMethod;

@Repository
public class EntUserPushLocationDaoImpl extends IDaoImpl<EntUserPushLocation, Long> implements EntUserPushLocationDao {

	@Override
	public List<EntUserPushLocation> query(Long entUserId, Long boxLocationId, Integer count, Boolean isSuccess,
			Calendar startTime, Calendar endTime, int page, int pageSize) {
		StringBuffer hql=new StringBuffer("from EntUserPushLocation el where 1=1 ");
		if(entUserId!=null){
			hql.append(" and el.enterpriseUser.id=").append(entUserId);
		}
		if(boxLocationId!=null){
			hql.append(" and el.boxLocation.id=").append(boxLocationId);
		}
		if(count!=null){
			hql.append(" and el.count<").append(count);
		}
		if(isSuccess!=null){
			hql.append(" and el.isSuccess=").append(isSuccess);
		}
		if(startTime!=null){
			hql.append(" and el.createTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(endTime!=null){
			hql.append(" and el.createTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		hql.append("order by el.id desc");
		return getList(hql.toString(), page, pageSize);
	}

	@Override
	public int amount(Long entUserId, Long boxLocationId, Integer count, Boolean isSuccess, Calendar startTime,
			Calendar endTime) {
		StringBuffer hql=new StringBuffer("select count(*) from EntUserPushLocation el where 1=1 ");
		if(entUserId!=null){
			hql.append(" and el.enterpriseUser.id=").append(entUserId);
		}
		if(boxLocationId!=null){
			hql.append(" and el.boxLocation.id=").append(boxLocationId);
		}
		if(count!=null){
			hql.append(" and el.count=").append(count);
		}
		if(isSuccess!=null){
			hql.append(" and el.isSuccess=").append(isSuccess);
		}
		if(startTime!=null){
			hql.append(" and el.createTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(endTime!=null){
			hql.append(" and el.createTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		return count(hql.toString());
	}

}

package com.bdbox.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.ExpressCompanyDao;
import com.bdbox.entity.ExpressCompany;

@Repository
public class ExpressCompanyDaoImpl extends IDaoImpl<ExpressCompany, Long>
		implements ExpressCompanyDao {

}

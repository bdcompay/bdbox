package com.bdbox.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.EnterpriseUserDao;
import com.bdbox.entity.EnterpriseUser;
import com.bdbox.entity.User;
import com.bdsdk.util.CommonMethod;

/**
 * 企业用户DAO实现类
 * 
 * @ClassName: EnterpriseUserDaoImpl 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-3-11 上午11:20:49 
 * @version V5.0 
 */
@Repository
public class EnterpriseUserDaoImpl  
				extends IDaoImpl<EnterpriseUser, Long> 
				implements EnterpriseUserDao {

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.dao.EnterpriseUserDao#queryEnterpriseUsers(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar, int, int)
	 */
	@Override
	public List<EnterpriseUser> queryEnterpriseUsers(String name,
			String orgCode, String linkman, String phone,String mail, String address,
			Calendar startTime, Calendar endTime, int page, int pageSize) {
		StringBuffer hql = new StringBuffer("from EnterpriseUser where 1=1 ");
		if(name!=null){
			hql.append(" and name like '%").append(name).append("%'");
		}
		if(orgCode!=null){
			hql.append(" and orgCode='").append(orgCode).append("'");
		}
		if(linkman!=null){
			hql.append(" and linkman like '%").append(linkman).append("%'");
		}
		if(phone!=null){
			hql.append(" and phone like '%").append(phone).append("%'");
		}
		if(mail!=null){
			hql.append(" and mail like '%").append(mail).append("%'");
		}
		if(address!=null){
			hql.append(" and address like '%").append(address).append("%'");
		}
		if(startTime!=null){
			hql.append(" and createdTime>='")
				.append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss"))
				.append("'");
		}
		if(endTime!=null){
			hql.append(" and createdTime<='")
				.append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss"))
				.append("'");
		}
		hql.append(" order by id desc ");
		return getList(hql.toString(), page, pageSize);
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.dao.EnterpriseUserDao#amount(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public int amount(String name, String orgCode, String linkman,
			String phone,String mail, String address, Calendar startTime, Calendar endTime) {
		StringBuffer hql = new StringBuffer("select count(*) from EnterpriseUser where 1=1 ");
		if(name!=null){
			hql.append(" and name like '%").append(name).append("%'");
		}
		if(orgCode!=null){
			hql.append(" and orgCode='").append(orgCode).append("'");
		}
		if(linkman!=null){
			hql.append(" and linkman like '%").append(linkman).append("%'");
		}
		if(phone!=null){
			hql.append(" and phone like '%").append(phone).append("%'");
		}
		if(mail!=null){
			hql.append(" and mail like '%").append(mail).append("%'");
		}
		if(address!=null){
			hql.append(" and address like '%").append(address).append("%'");
		}
		if(startTime!=null){
			hql.append(" and createdTime>='")
				.append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss"))
				.append("'");
		}
		if(endTime!=null){
			hql.append(" and createdTime<='")
				.append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss"))
				.append("'");
		}
		return count(hql.toString());
	}

	@Override
	public EnterpriseUser query(String username) {
		String hql = "from EnterpriseUser e where e.phone = ?";
		return unique(hql, new Object[]{username});
	}

	public EnterpriseUser queryEnterpriseUserByPowerKey(String userPowerKey) {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer("from EnterpriseUser where 1=1 ");
		if(userPowerKey!=null){
			hql.append(" and userPowerKey = '").append(userPowerKey).append("'");
		}
		List<EnterpriseUser> enterpriseUser = getList(hql.toString(),1,1);
		if(enterpriseUser.size()>0){
			return enterpriseUser.get(0);
		}else{
			return null;
		}
	}

}

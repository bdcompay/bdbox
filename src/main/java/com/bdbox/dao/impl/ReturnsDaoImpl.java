package com.bdbox.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.constant.ReturnsStatusType;
import com.bdbox.dao.ReturnsDao;
import com.bdbox.entity.Returns;
import com.bdbox.entity.User;
import com.bdsdk.util.CommonMethod;

@Repository
public class ReturnsDaoImpl extends IDaoImpl<Returns, Long> implements
		ReturnsDao {

	@Override
	public List<Returns> listReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum,
			ReturnsStatusType returnsStatusType, Calendar startTime,
			Calendar endTime, int page, int pageSize,String orderstutas) {
		StringBuffer hql = new StringBuffer("from Returns where 1=1 ");
		if(orderId!=null){
			hql.append(" and ordreId="+orderId);
		}
		if(returnsNum!=null){
			hql.append(" and returnsNum="+returnsNum);
		}
		if(userId!=null){
			hql.append(" and userId="+userId);
		}
		if(orderNum!=null && orderNum!=""){
			hql.append(" and orderNum='"+orderNum+"'");
		}
		if(expressNum!=null && !expressNum.equals("")){
			hql.append(" and expressNum='"+expressNum+"'");
		}
		if(returnsStatusType!=null){
			hql.append(" and returnsStatusType='"+returnsStatusType+"'");
		}
		if(orderstutas!=null){
			hql.append(" and returnType like '%").append(orderstutas).append("%'"); 
 		} 
		 
		if(startTime!=null && endTime!=null){
			hql.append(" and applySubmitTime<='"+CommonMethod.CalendarToString(endTime, null)
					+"' and applySubmitTime>='"+CommonMethod.CalendarToString(startTime, null)+"'");
		}
		hql.append(" order by id desc");
		return getList(hql.toString(), page, pageSize);
	}

	@Override
	public int countReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum,
			ReturnsStatusType returnsStatusType, Calendar startTime,
			Calendar endTime,String orderstutas) {
		StringBuffer hql = new StringBuffer("select count(*) from Returns where 1=1 ");
		if(orderId!=null){
			hql.append(" and ordreId="+orderId);
		}
		if(returnsNum!=null){
			hql.append(" and returnsNum="+returnsNum);
		}
		if(userId!=null){
			hql.append(" and userId="+userId);
		}
		if(orderNum!=null && orderNum!=""){
			hql.append(" and orderNum='"+orderNum+"'");
		}
		if(expressNum!=null && !expressNum.equals("")){
			hql.append(" and expressNum='"+expressNum+"'");
		}
		if(returnsStatusType!=null){
			hql.append(" and returnsStatusType='"+returnsStatusType+"'");
		}
		if(orderstutas!=null){  
			hql.append(" and returnType like '%").append(orderstutas).append("%'"); 
 		} 
		if(startTime!=null && endTime!=null){
			hql.append(" and applySubmitTime<='"+CommonMethod.CalendarToString(endTime, null)
					+"' and applySubmitTime>='"+CommonMethod.CalendarToString(startTime, null)+"'");
		}
		return count(hql.toString());
	}
	
	public Returns queryReturns(long expressid) {
		StringBuffer hql = new StringBuffer("from Returns t where 1=1");
		if (expressid != 0) {
			hql.append(" and t.expressId='").append(expressid) .append("'");
		}
		List<Returns> returns = getList(hql.toString(),1,1);
		if(returns.size()>0){
			return returns.get(0);
		}else{
			return null;
		}

	}
	
	public Returns queryReturn(long orderid){
		StringBuffer hql = new StringBuffer("from Returns t where 1=1");
		if (orderid != 0) {
			hql.append(" and t.ordreId='").append(orderid) .append("'");
		}
		List<Returns> returns = getList(hql.toString(),1,1);
		if(returns.size()>0){
			return returns.get(0);
		}else{
			return null;
		}
	}
	
	//租用归还页面查询
	@Override
	public List<Returns> queryRentReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum,
			ReturnsStatusType returnsStatusType, Calendar startTime,
			Calendar endTime, int page, int pageSize,String orderstutas) {
		StringBuffer hql = new StringBuffer("from Returns where 1=1 ");
		if(orderId!=null){
			hql.append(" and ordreId="+orderId);
		}
		if(returnsNum!=null){
			hql.append(" and returnsNum="+returnsNum);
		}
		if(userId!=null){
			hql.append(" and userId="+userId);
		}
		if(orderNum!=null && orderNum!=""){
			hql.append(" and orderNum='"+orderNum+"'");
		}
		if(expressNum!=null && !expressNum.equals("")){
			hql.append(" and expressNum='"+expressNum+"'");
		}
		if(returnsStatusType!=null){
			hql.append(" and returnsStatusType='"+returnsStatusType+"'");
		}
		if(orderstutas!=null){
			hql.append(" and returnType like '%").append(orderstutas).append("%'"); 
 		} 
		 
		if(startTime!=null && endTime!=null){
			hql.append(" and applySubmitTime<='"+CommonMethod.CalendarToString(endTime, null)
					+"' and applySubmitTime>='"+CommonMethod.CalendarToString(startTime, null)+"'");
		}
		hql.append(" order by id desc");
		return getList(hql.toString(), page, pageSize);
	}
	
	
	@Override
	public int countRentReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum,
			ReturnsStatusType returnsStatusType, Calendar startTime,
			Calendar endTime,String orderstutas) {
		StringBuffer hql = new StringBuffer("select count(*) from Returns where 1=1 ");
		if(orderId!=null){
			hql.append(" and ordreId="+orderId);
		}
		if(returnsNum!=null){
			hql.append(" and returnsNum="+returnsNum);
		}
		if(userId!=null){
			hql.append(" and userId="+userId);
		}
		if(orderNum!=null && orderNum!=""){
			hql.append(" and orderNum='"+orderNum+"'");
		}
		if(expressNum!=null && !expressNum.equals("")){
			hql.append(" and expressNum='"+expressNum+"'");
		}
		if(returnsStatusType!=null){
			hql.append(" and returnsStatusType='"+returnsStatusType+"'");
		}
		if(orderstutas!=null){  
			hql.append(" and returnType like '%").append(orderstutas).append("%'"); 
 		} 
		if(startTime!=null && endTime!=null){
			hql.append(" and applySubmitTime<='"+CommonMethod.CalendarToString(endTime, null)
					+"' and applySubmitTime>='"+CommonMethod.CalendarToString(startTime, null)+"'");
		}
		return count(hql.toString());
	}
	
	
	@Override
	public List<Returns> listRentReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum,
			ReturnsStatusType returnsStatusType, Calendar startTime,
			Calendar endTime, int page, int pageSize,String orderstutas) {
		StringBuffer hql = new StringBuffer("from Returns where 1=1 ");
		if(orderId!=null){
			hql.append(" and ordreId="+orderId);
		}
		if(returnsNum!=null){
			hql.append(" and returnsNum="+returnsNum);
		}
		if(userId!=null){
			hql.append(" and userId="+userId);
		}
		if(orderNum!=null && orderNum!=""){
			hql.append(" and orderNum='"+orderNum+"'");
		}
		if(expressNum!=null && !expressNum.equals("")){
			hql.append(" and expressNum='"+expressNum+"'");
		}
		if(returnsStatusType!=null){
			hql.append(" and returnsStatusType='"+returnsStatusType+"'");
		}
		if(orderstutas!=null){
			hql.append(" and returnType like '%").append(orderstutas).append("%'"); 
 		} 
		 
		if(startTime!=null && endTime!=null){
			hql.append(" and applySubmitTime<='"+CommonMethod.CalendarToString(endTime, null)
					+"' and applySubmitTime>='"+CommonMethod.CalendarToString(startTime, null)+"'");
		}
		hql.append(" order by id desc");
		return getList(hql.toString(), page, pageSize);
	}
	
	public Returns getReturnsenty(long expressid){
		StringBuffer hql = new StringBuffer("from Returns t where 1=1");
		if (expressid != 0) {
			hql.append(" and t.expressId='").append(expressid) .append("'");
		}
		List<Returns> returns = getList(hql.toString(),1,1);
		if(returns.size()>0){
			return returns.get(0);
		}else{
			return null;
		}
	}


}

package com.bdbox.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.UnionpayRefundDao;
import com.bdbox.entity.Returns;
import com.bdbox.entity.UnionpayRefund;

@Repository
public class UnionpayRefundDaoImpl extends IDaoImpl<UnionpayRefund, Long>
		implements UnionpayRefundDao{

	@Override
	public UnionpayRefund getUnionpayRefund(String orderId) {
		String hql = "from UnionpayRefund u where u.orderId = '"+orderId+"'";
		return (UnionpayRefund) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public UnionpayRefund getRefundByQueryId(String queryId) {
		String hql = "from UnionpayRefund u where u.origQryId = '"+queryId+"'";
		return (UnionpayRefund) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public Returns getReturns(String orderId) {
		String hql = "from Returns r where r.orderNum = '"+orderId+"'";
		return (Returns) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public UnionpayRefund getRefund(String queryId) {
		String hql = "from UnionpayRefund u where u.queryId = '"+queryId+"'";
		return (UnionpayRefund) getSession().createQuery(hql).uniqueResult();
	}

}

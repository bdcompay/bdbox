package com.bdbox.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.constant.AppType;
import com.bdbox.dao.AppconfigDao;
import com.bdbox.entity.AppConfig;

@Repository
public class AppconfigDaoImpl extends IDaoImpl<AppConfig, Long> implements
		AppconfigDao {

	public AppConfig query(AppType appType) {
		StringBuffer hql = new StringBuffer("from AppConfig t where 1=1");
		if (appType != null) {
			hql.append(" and t.appType='").append(appType.toString())
					.append("'");
		}
		hql.append("order by versionCode desc");
		List<AppConfig> appConfigs = getList(hql.toString(), 1, 1);
		if (appConfigs.size() != 0) {
			return appConfigs.get(0);
		}
		return null;
	}
}

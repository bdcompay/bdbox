package com.bdbox.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.BoxFamilyDao;
import com.bdbox.entity.BoxFamily;

@Repository
public class BoxFamilyDaoImpl extends IDaoImpl<BoxFamily, Long> implements
		BoxFamilyDao {

	public int queryAmount(Long boxId, String familyMob) {
		StringBuffer hql = new StringBuffer(
				"select count(*) from BoxFamily t where 1=1");
		if (boxId != null) {
			hql.append(" and t.box.id=").append(boxId);
		}
		if (familyMob != null) {
			hql.append(" and t.familyMob='").append(familyMob).append("'");
		}
		return count(hql.toString());
	}

	public List<BoxFamily> query(Long boxId, String familyMob, int page,
			int pageSize) {
		StringBuffer hql = new StringBuffer("from BoxFamily t where 1=1");
		if (boxId != null) {
			hql.append(" and t.box.id=").append(boxId);
		}
		if (familyMob != null) {
			hql.append(" and t.familyMob='").append(familyMob).append("'");
		}
		List<BoxFamily> boxFamilies = getList(hql.toString(), page, pageSize);
		return boxFamilies;
	}

}

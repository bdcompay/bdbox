package com.bdbox.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.constant.UserStatusType;
import com.bdbox.constant.UserType;
import com.bdbox.dao.UserDao;
import com.bdbox.entity.User;

@Repository
public class UserDaoImpl extends IDaoImpl<User, Long> implements UserDao {

	public User queryUser(UserType userType, String username, String password,
			UserStatusType userStatusType, String mobKey, String mail) {
		StringBuffer hql = new StringBuffer("from User t where 1=1");
		if (userType != null) {
			hql.append(" and t.userType='").append(userType.toString())
					.append("'");
		}
		if (username != null) {
			hql.append(" and t.username='").append(username).append("'");
		}
		if (password != null) {
			hql.append(" and t.password='").append(password).append("'");
		}
		if (userStatusType != null) {
			hql.append(" and t.userStatusType='").append(userStatusType)
					.append("'");
		}
		if (mobKey != null) {
			hql.append(" and t.mobileKey='").append(mobKey).append("'");
		}
		if (mail != null) {
			hql.append(" and t.mail='").append(mail).append("'");
		}
		List<User> user = getList(hql.toString(),1,1);
		if(user.size()>0){
			return user.get(0);
		}else{
			return null;
		}
	}

	public List<User> query(UserType userType, String username,
			String password, UserStatusType userStatusType, String mobKey,
			String mail, String order, int page, int pageSize) {
		StringBuffer hql = new StringBuffer("from User t where 1=1");
		if (userType != null) {
			hql.append(" and t.userType='").append(userType.toString())
					.append("'");
		}
		if (username != null) {
			hql.append(" and t.username='").append(username).append("'");
		}
		if (password != null) {
			hql.append(" and t.password='").append(password).append("'");
		}
		if (userStatusType != null) {
			hql.append(" and t.userStatusType='").append(userStatusType)
					.append("'");
		}
		if (mobKey != null) {
			hql.append(" and t.mobileKey='").append(mobKey).append("'");
		}
		if (mail != null) {
			hql.append(" and t.mail='").append(mail).append("'");
		}
		if (order != null) {
			hql.append(" order by t.").append(order);
		}
		return getList(hql.toString(), page, pageSize);
	}

	public Integer queryCount(UserType userType, String username,
			String password, UserStatusType userStatusType, String mobKey,
			String mail) {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer("select count(*) from User t where 1=1");
 		if (userType != null) {
			hql.append(" and t.userType='").append(userType.toString())
					.append("'");
		}
		if (username != null) {
			hql.append(" and t.username='").append(username).append("'");
		}
		if (password != null) {
			hql.append(" and t.password='").append(password).append("'");
		}
		if (userStatusType != null) {
			hql.append(" and t.userStatusType='").append(userStatusType)
					.append("'");
		}
		if (mobKey != null) {
			hql.append(" and t.mobileKey='").append(mobKey).append("'");
		}
		if (mail != null) {
			hql.append(" and t.mail='").append(mail).append("'");
		}
		return count(hql.toString());
	}

	
	public User queryUser(String userPowerKey) {
		StringBuffer hql = new StringBuffer("from User t where 1=1");
		if (userPowerKey != null) {
			hql.append(" and t.userPowerKey='").append(userPowerKey.toString())
					.append("'");
		}
		List<User> user = getList(hql.toString(),1,1);
		if(user.size()>0){
			return user.get(0);
		}else{
			return null;
		}

	}

	@Override
	public User getUserByusername(String user) {
		StringBuffer hql = new StringBuffer("from User t where 1=1");
		if (user != null) {
			hql.append(" and t.username='").append(user)
					.append("'");
		}
		List<User> user1 = getList(hql.toString(),1,1);
		if(user1.size()>0){
			return user1.get(0);
		}else{
			return null;
		}
		 
	}
	
	public List<User> getRealUser(int page, int pageSize){
		StringBuffer hql = new StringBuffer("from User t where 1=1 and t.identification  is not null order by t.isAutonymTime desc ");
		return getList(hql.toString(), page, pageSize);
	}
	
	public Integer getRealUserCount(){
		StringBuffer hql = new StringBuffer("select count(*) from User t where 1=1 and t.identification  is not null order by t.isAutonymTime desc");
		return count(hql.toString());
	}
	
	public List<User> queryuser_twl(int page, int pageSize){
		StringBuffer hql = new StringBuffer("from User t order by t.id desc ");
		return getList(hql.toString(), page, pageSize);
	}

	public List<User> getRefeesUser(String usernomber,String refereespeoplename,int page, int pageSize){
		StringBuffer hql = new StringBuffer(" from User where 1=1 "); 
		if(usernomber != null&&!usernomber.equals("")){ 
		  hql.append(" and username = '"+usernomber+"'"); 
		}
		if(refereespeoplename != null&&!refereespeoplename.equals("")){ 
			  hql.append(" and recommend = '"+refereespeoplename+"'"); 
		}
		hql.append(" and isRefes = '1'");  
		hql.append(" order by id desc");
		
		List<User> results = getList(hql.toString(), page, pageSize);
		return results;
	}

	public Integer getRefeesUserCount(String usernomber,String refereespeoplename){
		StringBuffer hql = new StringBuffer("select count(*) from User t where 1=1");
		if(usernomber != null && !"".equals(usernomber)){ 
			  hql.append(" and username = '"+usernomber+"'"); 
			}
		if(refereespeoplename != null&&!refereespeoplename.equals("")){ 
			  hql.append(" and recommend = '"+refereespeoplename+"'"); 
		}
		hql.append(" and isRefes = 1");  
		hql.append(" order by id desc");
		return count(hql.toString());
	}

	public User getUserByuser(String user){
		StringBuffer hql = new StringBuffer("from User t where 1=1");
		if (user != null) {
			hql.append(" and t.username='").append(user)
					.append("'");
		}
		List<User> user1 = getList(hql.toString(),1,1);
		if(user1.size()>0){
			return user1.get(0);
		}else{
			return null;
		}
	}

	@Override
	public void bindLxtAccount(String account, String lxtAccount) {
		String hql = "update User u set u.lxtAccount = '"+lxtAccount+"' where u.username = '"+account+"'";
		excuteHQL(hql);
	}

	@Override
	public void unbindLxtAccount(String account) {
		String hql = "update User u set u.lxtAccount = null where u.username = '"+account+"'";
		excuteHQL(hql);
	}

	@Override
	public User getUserForLxtAccount(String lxtAccount) {
		String hql = "from User u where u.lxtAccount = ?";
		return unique(hql, new Object[]{lxtAccount});
	}

}

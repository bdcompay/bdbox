package com.bdbox.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.EntSendMsgDao;
import com.bdbox.entity.EntSendMsg;
import com.bdsdk.util.CommonMethod;

/**
 * 企业用户发送消息DAO接口实现类
 * 
 * @ClassName: EntSendMsgDaoImpl 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-3-22 下午3:30:55 
 * @version V5.0 
 */
@Repository
public class EntSendMsgDaoImpl extends IDaoImpl<EntSendMsg, Long> implements EntSendMsgDao {

	/*
	 * 多条件查询
	 * (non-Javadoc)
	 * @see com.bdbox.dao.EntSendMsgDao#query(java.lang.String, java.util.Calendar, java.util.Calendar, java.lang.String, int, int)
	 */
	@Override
	public List<EntSendMsg> query(String toBoxSerialNumber, Calendar startTime,
			Calendar endTime, String entUserName, int page, int pageSize) {
		StringBuffer hql = new StringBuffer(" from EntSendMsg t where 1=1 ");
		if(toBoxSerialNumber!=null){
			hql.append(" and t.toBoxSerialNumber='").append(toBoxSerialNumber).append("'");
		}
		if(startTime!=null){
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(endTime!=null){
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(entUserName!=null){
			hql.append(" and t.entUser.name like '%").append(entUserName).append("%'");
		}
		hql.append(" order by t.createdTime desc");
		return getList(hql.toString(), page, pageSize);
	}

	/*
	 * 多条件查询统计
	 * (non-Javadoc)
	 * @see com.bdbox.dao.EntSendMsgDao#count(java.lang.String, java.util.Calendar, java.util.Calendar, java.lang.String)
	 */
	@Override
	public int count(String toBoxSerialNumber, Calendar startTime,
			Calendar endTime, String entUserName) {
		StringBuffer hql = new StringBuffer("select count(*) from EntSendMsg t where 1=1 ");
		if(toBoxSerialNumber!=null){
			hql.append(" and t.toBoxSerialNumber='").append(toBoxSerialNumber).append("'");
		}
		if(startTime!=null){
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(endTime!=null){
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(entUserName!=null){
			hql.append(" and t.entUser.name like '%").append(entUserName).append("%'");
		}
		return count(hql.toString());
	}

}

package com.bdbox.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.FamilyStatusDao;
import com.bdbox.entity.FamilyStatus;

@Repository
public class FamilyStatusDaoImpl extends IDaoImpl<FamilyStatus, Long> implements
		FamilyStatusDao {

	public int queryAmount(Long boxId, String familyMob) {
		StringBuffer hql = new StringBuffer(
				"select count(*) from FamilyStatus t where 1=1");
		if (boxId != null) {
			hql.append(" and t.box.boxSerialNumber=").append(boxId);
		}
		if (familyMob != null) {
			hql.append(" and t.familyMob='").append(familyMob).append("'");
		}
		return count(hql.toString());
	}

	public List<FamilyStatus> query(Long boxId, String familyMob, int page,
			int pageSize) {
		StringBuffer hql = new StringBuffer("from FamilyStatus t where 1=1");
		if (boxId != null) {
			hql.append(" and t.box.boxSerialNumber=").append(boxId);
		}
		if (familyMob != null) {
			hql.append(" and t.familyMob='").append(familyMob).append("'");
		}
		List<FamilyStatus> familyStatus = getList(hql.toString(), 1, 1);
		return familyStatus;
	}
}

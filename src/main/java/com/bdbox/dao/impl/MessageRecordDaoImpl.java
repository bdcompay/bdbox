package com.bdbox.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.MessageRecordDao;
import com.bdbox.entity.MessageRecord;
import com.bdsdk.util.CommonMethod;

@Repository
public class MessageRecordDaoImpl extends IDaoImpl<MessageRecord, Long>
		implements MessageRecordDao{

	@Override
	public List<MessageRecord> getMsgAll(String mob, String content,
			Calendar startTime, Calendar endTime, int page, int pageSize) {
		StringBuilder sb = new StringBuilder("from MessageRecord m where 1=1");
		if(mob!=null && !"".equals(mob)){
			sb.append(" and m.toMob = '"+mob+"'");
		}
		if(content!=null && !"".equals(content)){
			sb.append(" and m.msgContent like '%"+content+"%'");
		}
		if(startTime!=null){
			sb.append(" and m.createdTime >= '"+CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")+"'");
		}
		if(endTime!=null){
			sb.append(" and m.createdTime <= '"+CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")+"'");
		}
		sb.append(" order by m.createdTime desc");
		return getList(sb.toString(), page, pageSize);
	}

	@Override
	public int queryCount(String mob, String content, Calendar startTime,
			Calendar endTime) {
		StringBuilder sb = new StringBuilder("select count(*) from MessageRecord m where 1=1");
		if(mob!=null && !"".equals(mob)){
			sb.append(" and m.toMob = '"+mob+"'");
		}
		if(content!=null && !"".equals(content)){
			sb.append(" and m.msgContent like '%"+content+"%'");
		}
		if(startTime!=null){
			sb.append(" and m.createdTime >= '"+CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")+"'");
		}
		if(endTime!=null){
			sb.append(" and m.createdTime <= '"+CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")+"'");
		}
		return count(sb.toString());
	}

}

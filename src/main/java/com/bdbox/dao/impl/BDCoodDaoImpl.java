package com.bdbox.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.BDCoodDao;
import com.bdbox.entity.BDCood;
import com.bdsdk.util.CommonMethod;

@Repository   
public class BDCoodDaoImpl extends IDaoImpl<BDCood, Long>	implements BDCoodDao {

	public List<BDCood> querybdcood(String bdnumber,String ordernumber,String userpeople,String refereespeople,String isuser,  Calendar startTime,Calendar endTime,Calendar creantTime, String refereespeoplename,String bdcoodtype,int page, int pageSize){
		StringBuffer hql = new StringBuffer("from BDCood where 1=1 "); 
		if(bdnumber!=null){
			hql.append(" and bdcood ='"+bdnumber+"'");
 		}
		if(ordernumber!=null){
 			hql.append(" and orderNo ='"+ordernumber+"'"); 
		}
		if(userpeople!=null){
  			hql.append(" and username ='"+userpeople+"'"); 
		}
		if(refereespeople!=null){
  			hql.append(" and referees ='"+refereespeople+"'"); 
 		}
		if(isuser!=null){
  			hql.append(" and bdcoodStatus ='"+isuser+"'"); 
 		} 
		if(refereespeoplename!=null){
  			hql.append(" and remakname ='"+refereespeoplename+"'"); 
  			
 		}
		if(bdcoodtype!=null){
  			hql.append(" and type ='"+bdcoodtype+"'"); 
  			
 		} 
		if(startTime!=null && endTime!=null){
			hql.append(" and startTime<='"+CommonMethod.CalendarToString(endTime, null)
					+"' and endTime>='"+CommonMethod.CalendarToString(startTime, null)+"'");
		}
		 
		if(creantTime!=null){
			System.out.println(creantTime);
			long cutime = creantTime.getTimeInMillis();
			Calendar time = Calendar.getInstance();
			time.setTimeInMillis((cutime+1000*60*60*24));
 			hql.append(" and creatTime>='"+CommonMethod.CalendarToString(creantTime, null)+"'")
 			.append(" and creatTime<='"+CommonMethod.CalendarToString(time, null)+"'"); 
		}
		hql.append(" order by creatTime desc");
		
		List<BDCood> results = getList(hql.toString(), page, pageSize);
		return results;
	}
	
	@Override
	public int queryAmount(String bdnumber,String ordernumber,String userpeople,String refereespeople,String isuser,  Calendar startTime,Calendar endTime,Calendar creantTime,String refereespeoplename,String bdcoodtype) {
		
		StringBuffer hql = new StringBuffer("select count(*) from BDCood where 1=1 "); 
		
		if(bdnumber!=null){
			hql.append(" and bdcood ='"+bdnumber+"'");
 		}
		if(ordernumber!=null){
 			hql.append(" and orderNo ='"+ordernumber+"'"); 
		}
		if(userpeople!=null){
  			hql.append(" and username ='"+userpeople+"'"); 
		}
		if(refereespeople!=null){
  			hql.append(" and referees ='"+refereespeople+"'"); 
 		}
		if(isuser!=null){
  			hql.append(" and bdcoodStatus ='"+isuser+"'"); 
 		}
		if(refereespeoplename!=null){
  			hql.append(" and remakname ='"+refereespeoplename+"'"); 
 		} 
		if(bdcoodtype!=null){
  			hql.append(" and type ='"+bdcoodtype+"'"); 
  			
 		} 
		if(startTime!=null && endTime!=null){
			hql.append(" and startTime<='"+CommonMethod.CalendarToString(startTime, null)
					+"' and endTime>='"+CommonMethod.CalendarToString(endTime, null)+"'");
		}
		
		if(creantTime!=null){
			System.out.println(creantTime);
			long cutime = creantTime.getTimeInMillis();
			Calendar time = Calendar.getInstance();
			time.setTimeInMillis((cutime+1000*60*60*24));
 			hql.append(" and creatTime>='"+CommonMethod.CalendarToString(creantTime, null)+"'")
 			.append(" and creatTime<='"+CommonMethod.CalendarToString(time, null)+"'"); 
		}
		hql.append(" order by creatTime desc");
		
		return  count(hql.toString());
	}
	
	public BDCood queryBdcood(String bdnumber){ 
		return unique("from BDCood t where t.bdcood = ?", new Object[]{bdnumber}); 
 		 
	}
	
	public int  getRefeesUserCount_wl(String bdnumber){
		StringBuffer hql = new StringBuffer("select count(*) from BDCood where 1=1 and referees='"+bdnumber+"'"); 
		return count(hql.toString());
	}
	
	public int  getRefeesUserCount_wl1(String bdnumber){
		StringBuffer hql = new StringBuffer("select count(*) from BDCood where 1=1 and referees='"+bdnumber+"' and bdcoodStatus='USER'"); 
		return count(hql.toString());
	}
	
	public int  getRefeesUserCount_wl2(String bdnumber){
		StringBuffer hql = new StringBuffer("select count(*) from BDCood where 1=1 and referees='"+bdnumber+"' and bdcoodStatus='NOTUSER'"); 
		return count(hql.toString());
	}
	
	public int  getRefeesUserCount_wl3(String bdnumber){
		StringBuffer hql = new StringBuffer("select count(*) from BDCood where 1=1 and referees='"+bdnumber+"' and bdcoodStatus='EXCEED'"); 
		return count(hql.toString());
	}
	
	public int  getRefeesUserCount_wl4(String bdnumber){
		StringBuffer hql = new StringBuffer("select count(*) from BDCood where 1=1 and referees='"+bdnumber+"' and bdcoodStatus='NOTEXCEED'"); 
		return count(hql.toString());
	}
	
	public BDCood  getRefeesUser_wl5(String bdnumber){
		return unique("from BDCood t where t.referees = ?", new Object[]{bdnumber}); 
	}

	public BDCood getbdcoods_wl(String bdcood){
		return unique("from BDCood t where t.bdcood = ?", new Object[]{bdcood}); 
	}
	
	public List<BDCood> queryBdcood(){
		 String hql = "from BDCood where 1=1 and bdcoodStatus='NOTUSER'";
			return getList(hql.toString(), 0, 0, new Object[0]);
	}
	
	public List<BDCood> getUserByuser_twl(String user){
		 String hql = "from BDCood where 1=1 and referees='"+user+"'";
			return getList(hql.toString(), 0, 0, new Object[0]);
	}




}

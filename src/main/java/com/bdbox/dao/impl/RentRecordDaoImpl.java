package com.bdbox.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.RentRecordDao;
import com.bdbox.entity.RentRecord;

@Repository
public class RentRecordDaoImpl extends IDaoImpl<RentRecord, Long> implements
		RentRecordDao {

	public List<RentRecord> query(String user, Calendar startTime, Calendar endTime,
			int page, int pageSize){
		StringBuffer hql = new StringBuffer("from RentRecord r where 1=1");
		if(user != null && !"".equals(user)){
			hql.append(" and r.user = '"+user+"'");
		}
		if(startTime != null && !"".equals(startTime) && endTime != null && !"".equals(endTime)){
			hql.append(" and r.recordTime >='"+startTime+"' and r.recordTime <='"+endTime+"'");
		}
		hql.append(" order by r.id desc");
		return getList(hql.toString(), page, pageSize);
	}
	
	public int queryAmount(String user, Calendar startTime, Calendar endTime){
		StringBuffer hql = new StringBuffer("select count(*) from RentRecord where 1=1");
		if(user != null && !"".equals(user)){
			hql.append(" and r.user = '"+user+"'");
		}
		if(startTime != null && !"".equals(startTime) && endTime != null && !"".equals(endTime)){
			hql.append(" and r.recordTime >='"+startTime+"' and r.recordTime <='"+endTime+"'");
		}
		return count(hql.toString());
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<RentRecord> getUserRentRecord(String user) {
		String hql = "from RentRecord r where r.user = ?1";
		return getSession().createQuery(hql).setParameter(1, user).list();
	}
}

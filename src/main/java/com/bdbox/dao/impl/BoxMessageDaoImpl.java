package com.bdbox.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.constant.MsgIoType;
import com.bdbox.constant.MsgType;
import com.bdbox.dao.BoxMessageDao;
import com.bdbox.entity.BoxMessage;
import com.bdsdk.constant.DataStatusType;
import com.bdsdk.util.CommonMethod;

@Repository
public class BoxMessageDaoImpl extends IDaoImpl<BoxMessage, Long> implements
		BoxMessageDao {

	public List<BoxMessage> query(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxSerialNumber, Long toBoxSerialNumber, String familyMail,
			Calendar startTime, Calendar endTime,String boxName,long userId,String order, int page,
			int pageSize) {
		
		StringBuffer hql = new StringBuffer("from BoxMessage t where 1=1");
		if (msgId != null) {
			hql.append(" and t.msgId=").append(msgId);
		}
		if (msgType != null) {
			hql.append(" and t.msgType='").append(msgType.toString())
					.append("'");
		}
		if (msgIoType != null) {
			hql.append(" and t.msgIoType='").append(msgIoType).append("'");
		}
		if (dataStatusType != null) {
			hql.append(" and t.dataStatusType='").append(dataStatusType)
					.append("'");
		}
		if (familyMob != null) {
			hql.append(" and t.familyMob=").append(familyMob);
		}
		if (familyMail != null) {
			hql.append(" and t.familyMail='").append(familyMail).append("'");
		}
		if (toBoxSerialNumber != null) {
			hql.append(" and t.toBox.boxSerialNumber=").append(toBoxSerialNumber);
		}
		if (fromBoxSerialNumber != null) {
			hql.append(" and t.fromBox.boxSerialNumber=").append(fromBoxSerialNumber);
		}
		if (startTime != null) {
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if (endTime != null) {
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd  HH:mm:ss")).append("'");
		}
		if (boxName != null) {
			hql.append(" and (t.toBox.name like '%").append(boxName).append("%'").append(" or t.fromBox.name like '%").append(boxName).append("%')");
		}
		if (userId != 0) {
			hql.append(" and (t.fromUserId='").append(userId).append("'").append(" or t.toUserId= '").append(userId).append("')");
		}
		if (order != null) {
			hql.append(" order by t.").append(order);
		}
		System.out.println(hql.toString());
		List<BoxMessage> boxMessages = getList(hql.toString(), page, pageSize);
		return boxMessages;
	}

	public Integer queryCount(Integer msgId, MsgType msgType, MsgIoType msgIoType,
			DataStatusType dataStatusType,String familyMob, Long fromBoxSerialNumber,
			Long toBoxSerialNumber, String familyMail, Calendar startTime, Calendar endTime,String boxName,long userId) {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer("select count(*) from BoxMessage t where 1=1");
		if (msgId != null) {
			hql.append(" and t.msgId=").append(msgId);
		}
		if (msgType != null) {
			hql.append(" and t.msgType='").append(msgType.toString())
					.append("'");
		}
		if (msgIoType != null) {
			hql.append(" and t.msgIoType='").append(msgIoType).append("'");
		}
		if (dataStatusType != null) {
			hql.append(" and t.dataStatusType='").append(dataStatusType)
					.append("'");
		}
		if (familyMob != null) {
			hql.append(" and t.familyMob=").append(familyMob);
		}
		if (fromBoxSerialNumber != null) {
			hql.append(" and t.fromBox.boxSerialNumber=").append(fromBoxSerialNumber);
		}
		if (toBoxSerialNumber != null) {
			hql.append(" and t.toBox.boxSerialNumber=").append(toBoxSerialNumber);
		}
		if (familyMail != null) {
			hql.append(" and t.familyMail='").append(familyMail).append("'");
		}
		if (startTime != null) {
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd")).append("'");
		}
		if (endTime != null) {
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd")).append("'");
		}
		if (boxName != null) {
			hql.append(" and (t.toBox.name like '%").append(boxName).append("%'").append(" or t.fromBox.name like '%").append(boxName).append("%')");
		}
		if (userId != 0) {
			hql.append(" and (t.fromUserId='").append(userId).append("'").append(" or t.toUserId= '").append(userId).append("')");
			//hql.append(" and (t.toBox.user='").append(userId).append("'").append(" or t.fromBox.user= '").append(userId).append("')");
		}
		return count(hql.toString());
	}
	
	public int queryAmount(Integer msgId, MsgType msgType, MsgIoType msgIoType,
			DataStatusType dataStatusType,String familyMob, Long fromBoxSerialNumber,
			Long toBoxSerialNumber, String familyMail, Calendar startTime, Calendar endTime,String boxName,long userId) {
		StringBuffer hql = new StringBuffer(
				"select count(*) from BoxMessage t where 1=1");
		if (msgId != null) {
			hql.append(" and t.msgId=").append(msgId);
		}
		if (msgType != null) {
			hql.append(" and t.msgType='").append(msgType.toString())
					.append("'");
		}
		if (msgIoType != null) {
			hql.append(" and t.msgIoType='").append(msgIoType).append("'");
		}
		if (dataStatusType != null) {
			hql.append(" and t.dataStatusType='").append(dataStatusType)
					.append("'");
		}
		if (familyMob != null) {
			hql.append(" and t.familyMob=").append(familyMob);
		}
		if (fromBoxSerialNumber != null) {
			hql.append(" and t.fromBox.boxSerialNumber=").append(fromBoxSerialNumber);
		}
		if (toBoxSerialNumber != null) {
			hql.append(" and t.toBox.boxSerialNumber=").append(toBoxSerialNumber);
		}
		if (familyMail != null) {
			hql.append(" and t.familyMail=").append(familyMail);
		}
		if (startTime != null) {
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd")).append("'");
		}
		if (endTime != null) {
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd")).append("'");
		}
		if (boxName != null) {
			hql.append(" and (t.toBox.name like '%").append(boxName).append("%'").append(" or t.fromBox.name like '%").append(boxName).append("%')");
		}
		if (userId != 0) {
			hql.append(" and (t.fromUserId='").append(userId).append("'").append(" or t.toUserId= '").append(userId).append("')");
			//hql.append(" and (t.toBox.user='").append(userId).append("'").append(" or t.fromBox.user= '").append(userId).append("')");
		}
		return count(hql.toString());
	}
	
	public List<BoxMessage> querys(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxSerialNumber, Long toBoxSerialNumber, String familyMail,
			Calendar startTime, Calendar endTime,String boxName,long userId,String order, int page,
			int pageSize) {
		
		StringBuffer hql = new StringBuffer("from BoxMessage t where 1=1");
		if (msgId != null) {
			hql.append(" and t.msgId=").append(msgId);
		}
		if (msgType != null) {
			hql.append(" and t.msgType='").append(msgType.toString())
					.append("'");
		}
		if (msgIoType != null) {
			hql.append(" and t.msgIoType='").append(msgIoType).append("'");
		}
		if (dataStatusType != null) {
			hql.append(" and t.dataStatusType='").append(dataStatusType)
					.append("'");
		}
		if (familyMob != null) {
			hql.append(" and t.familyMob=").append(familyMob);
		}
		if (familyMail != null) {
			hql.append(" and t.familyMail='").append(familyMail).append("'");
		}
		if (toBoxSerialNumber != null) {
			hql.append(" and t.toBox.boxSerialNumber=").append(toBoxSerialNumber);
		}
		if (fromBoxSerialNumber != null) {
			hql.append(" and t.fromBox.boxSerialNumber=").append(fromBoxSerialNumber);
		}
		if (startTime != null) {
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd ")).append("'");
		}
		if (endTime != null) {
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd")).append("'");
		}
		if (boxName != null) {
			hql.append(" and (t.toBox.name like '%").append(boxName).append("%'").append(" or t.fromBox.name like '%").append(boxName).append("%')");
		}
		if (userId != 0) {
			hql.append(" and (t.fromUserId='").append(userId).append("'").append(" or t.toUserId= '").append(userId).append("')");
			//hql.append(" and (t.toBox.user='").append(userId).append("'").append(" or t.fromBox.user= '").append(userId).append("')");
		}
		if (order != null) {
			hql.append(" order by t.").append(order);
		}
		System.out.println(hql.toString());
		List<BoxMessage> boxMessages = getList(hql.toString(), page, pageSize);
		return boxMessages;
	}

	@Override
	public List<BoxMessage> querylastRevieved(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxSerialNumber, Long toBoxSerialNumber,
			String familyMail, Calendar startTime, Calendar endTime,
			String boxName, long userId, String order, int page, int pageSize) {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer("from BoxMessage t where 1=1");
		if (msgId != null) {
			hql.append(" and t.msgId=").append(msgId);
		}
		if (msgType != null) {
			hql.append(" and t.msgType!='").append(msgType.toString())
					.append("'");
		}
		if (msgIoType != null) {
			hql.append(" and t.msgIoType='").append(msgIoType).append("'");
		}
		if (dataStatusType != null) {
			hql.append(" and t.dataStatusType='").append(dataStatusType)
					.append("'");
		}
		if (familyMob != null) {
			hql.append(" and t.familyMob=").append(familyMob);
		}
		if (familyMail != null) {
			hql.append(" and t.familyMail='").append(familyMail).append("'");
		}
		if (toBoxSerialNumber != null) {
			hql.append(" and t.toBox.boxSerialNumber=").append(toBoxSerialNumber);
		}
		if (fromBoxSerialNumber != null) {
			hql.append(" and t.fromBox.boxSerialNumber=").append(fromBoxSerialNumber);
		}
		if (startTime != null) {
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd ")).append("'");
		}
		if (endTime != null) {
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd")).append("'");
		}
		if (boxName != null) {
			hql.append(" and (t.toBox.name like '%").append(boxName).append("%'").append(" or t.fromBox.name like '%").append(boxName).append("%')");
		}
		if (userId != 0) {
			hql.append(" and (t.fromUserId='").append(userId).append("'").append(" or t.toUserId= '").append(userId).append("')");
			//hql.append(" and (t.toBox.user='").append(userId).append("'").append(" or t.fromBox.user= '").append(userId).append("')");
		}
		if (order != null) {
			hql.append(" order by t.").append(order);
		}
		System.out.println(hql.toString());
		List<BoxMessage> boxMessages = getList(hql.toString(), page, pageSize);
		return boxMessages;
	}
	/*
	 * (non-Javadoc)
	 * @see com.bdbox.dao.BoxMessageDao#countUserSendBdmsg(long, java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public int countUserSendBdmsg(long userid, Calendar startTime,
			Calendar endTime) {
		StringBuffer hql = new StringBuffer("select count(*) from BoxMessage t " +
				"where 1=1  and t.fromBox<>null ");
		if (startTime != null) {
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if (endTime != null) {
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if (userid != 0) {
			hql.append(" and t.fromUserId= '").append(userid).append("'");
			//hql.append(" and t.fromBox.user='").append(userid).append("'");
		}
		hql.append(" group by t.createdTime,t.fromBox.id");
		
		//HQL估计不适合解决统计之类的问题。
		int count = 0;
		int flag = 1;
		while(flag>0){
			List<Object> os = getList(hql.toString(), flag,100000);
			count+=os.size();
			if(os.size()==0){
				flag=0;
			}else{
				++flag;
			}
		}
		return count;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.dao.BoxMessageDao#countUserToFamilyMsg(long, java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public int countUserToFamilyMsg(long userid, Calendar startTime,
			Calendar endTime) {
		StringBuffer hql = new StringBuffer("select count(*) from BoxMessage t " +
				"where 1=1 and msgIoType='IO_BOXTOFAMILY' and familyMob<>null and t.fromBox<>null ");
		if (startTime != null) {
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if (endTime != null) {
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if (userid != 0) {
			hql.append(" and t.fromUserId= '").append(userid).append("'");
			//hql.append(" and t.fromBox.user= '").append(userid).append("'");
		}
		return count(hql.toString());
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.dao.BoxMessageDao#countUserRevBdmsg(long, java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public int countUserRevBdmsg(long userid, Calendar startTime,
			Calendar endTime) {
		StringBuffer hql = new StringBuffer("select count(*) from BoxMessage t where 1=1  and t.toBox<>null  ");
		if (startTime != null) {
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if (endTime != null) {
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if (userid != 0) {
			hql.append(" and t.toUserId= '").append(userid).append("'");
			//hql.append(" and t.toBox.user='").append(userid).append("'");
		}
		return count(hql.toString());
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.dao.BoxMessageDao#countFamilyToUser(long, java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public int countFamilyToUser(long userid, Calendar startTime,
			Calendar endTime) {
		StringBuffer hql = new StringBuffer("select count(*) from BoxMessage t " +
				"where 1=1 and msgIoType='IO_FAMILYTOBOX' and t.toBox<>null ");
		if (startTime != null) {
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if (endTime != null) {
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if (userid != 0) {
			hql.append(" and t.toUserId= '").append(userid).append("'");
			//hql.append(" and t.toBox.user='").append(userid).append("'");
		}
		return count(hql.toString());
	}

	/*
	 * 企业用户查询
	 * (non-Javadoc)
	 * @see com.bdbox.dao.BoxMessageDao#querys(java.lang.Integer, com.bdbox.constant.MsgType, com.bdbox.constant.MsgIoType, com.bdsdk.constant.DataStatusType, java.lang.String, java.lang.Long, java.lang.Long, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar, java.lang.String, long, java.lang.String, int, int)
	 */
	@Override
	public List<BoxMessage> querys(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxSerialNumber, Long toBoxSerialNumber,
			String familyMail, String entUserName, Calendar startTime,
			Calendar endTime, String boxName, long userId, String order,
			int page, int pageSize) {
		StringBuffer hql = new StringBuffer("select t from BoxMessage t,Box b,EnterpriseUser e where 1=1");
		if (msgId != null) {
			hql.append(" and t.msgId=").append(msgId);
		}
		if (msgType != null) {
			hql.append(" and t.msgType='").append(msgType.toString())
					.append("'");
		}
		if (msgIoType != null) {
			hql.append(" and t.msgIoType='").append(msgIoType).append("'");
		}
		if (dataStatusType != null) {
			hql.append(" and t.dataStatusType='").append(dataStatusType)
					.append("'");
		}
		if (familyMob != null) {
			hql.append(" and t.familyMob=").append(familyMob);
		}
		if (familyMail != null) {
			hql.append(" and t.familyMail='").append(familyMail).append("'");
		}
		if (toBoxSerialNumber != null) {
			hql.append(" and t.toBox.boxSerialNumber=").append(toBoxSerialNumber);
		}
		if (fromBoxSerialNumber != null) {
			hql.append(" and t.fromBox.boxSerialNumber=").append(fromBoxSerialNumber);
		}
		if (startTime != null) {
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd ")).append("'");
		}
		if (endTime != null) {
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd")).append("'");
		}
		if (boxName != null) {
			hql.append(" and (t.toBox.name like '%").append(boxName).append("%'").append(" or t.fromBox.name like '%").append(boxName).append("%')");
		}
		if (userId != 0) {
			hql.append(" and (t.fromUserId='").append(userId).append("'").append(" or t.toUserId= '").append(userId).append("')");
			//hql.append(" and (t.toBox.user='").append(userId).append("'").append(" or t.fromBox.user= '").append(userId).append("')");
		}
		if(entUserName!=null){
			hql.append(" and e.id=b.entUser and (b.id=t.fromBox or b.id=t.toBox) and e.name like '%")
				.append(entUserName).append("%'");
		}
		
		if (order != null) {
			hql.append(" order by t.").append(order);
		}
		System.out.println(hql.toString());
		List<BoxMessage> boxMessages = getList(hql.toString(), page, pageSize);
		return boxMessages;
	}

	/*
	 * 企业用户查询统计
	 * (non-Javadoc)
	 * @see com.bdbox.dao.BoxMessageDao#querysAmount(java.lang.Integer, com.bdbox.constant.MsgType, com.bdbox.constant.MsgIoType, com.bdsdk.constant.DataStatusType, java.lang.String, java.lang.Long, java.lang.Long, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar, java.lang.String, long)
	 */
	@Override
	public int querysAmount(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxSerialNumber, Long toBoxSerialNumber,
			String familyMail, String entUserName, Calendar startTime,
			Calendar endTime, String boxName, long userId) {
		StringBuffer hql = new StringBuffer("select count(*) from BoxMessage t,Box b,EnterpriseUser e  where 1=1");
		if (msgId != null) {
			hql.append(" and t.msgId=").append(msgId);
		}
		if (msgType != null) {
			hql.append(" and t.msgType='").append(msgType.toString())
					.append("'");
		}
		if (msgIoType != null) {
			hql.append(" and t.msgIoType='").append(msgIoType).append("'");
		}
		if (dataStatusType != null) {
			hql.append(" and t.dataStatusType='").append(dataStatusType)
					.append("'");
		}
		if (familyMob != null) {
			hql.append(" and t.familyMob=").append(familyMob);
		}
		if (familyMail != null) {
			hql.append(" and t.familyMail='").append(familyMail).append("'");
		}
		if (toBoxSerialNumber != null) {
			hql.append(" and t.toBox.boxSerialNumber=").append(toBoxSerialNumber);
		}
		if (fromBoxSerialNumber != null) {
			hql.append(" and t.fromBox.boxSerialNumber=").append(fromBoxSerialNumber);
		}
		if (startTime != null) {
			hql.append(" and t.createdTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd ")).append("'");
		}
		if (endTime != null) {
			hql.append(" and t.createdTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd")).append("'");
		}
		if (boxName != null) {
			hql.append(" and (t.toBox.name like '%").append(boxName).append("%'").append(" or t.fromBox.name like '%").append(boxName).append("%')");
		}
		if (userId != 0) {
			hql.append(" and (t.fromUserId='").append(userId).append("'").append(" or t.toUserId= '").append(userId).append("')");
			//hql.append(" and (t.toBox.user='").append(userId).append("'").append(" or t.fromBox.user= '").append(userId).append("')");
		}
		if(entUserName!=null){
			hql.append(" and e.id=b.entUser and (b.id=t.fromBox or b.id=t.toBox) and e.name like '%")
				.append(entUserName).append("%'");
		}
		return count(hql.toString());
	}

	@Override
	public List<BoxMessage> getEntUserMessage(String phoneNum) {
		String hql = "from BoxMessage m where m.familyMob = '"+phoneNum+"' and m.msgIoType = 'IO_EntUserTOBOX' order by m.createdTime desc LIMIT 20";
		return getList(hql, 1, 10);
	}

	@Override
	public List<BoxMessage> getReceiveMessage(Long boxId) {
		String hql = "from BoxMessage m where m.msgIoType != 'IO_EntUserTOBOX' and "
				+ "(m.fromBox.id = "+boxId+" or m.toBox.id = "+boxId+") order by m.createdTime desc LIMIT 20";
		return getList(hql, 1, 10);
	}

	@Override
	public List<BoxMessage> getEntUserMessage(String phoneNum, String keyWord) {
		StringBuffer hql = new StringBuffer("from BoxMessage m where m.familyMob = '"+phoneNum+"' and m.msgIoType = 'IO_EntUserTOBOX'");
		if(!"".equals(keyWord) && keyWord!=null){
			hql.append(" and m.content like '%"+keyWord+"%'");
		}
		hql.append(" order by m.createdTime");
		return getList(hql.toString(), 1, 9999);
	}

	@Override
	public int querycount(String phoneNum, String keyWord) {
		StringBuffer hql = new StringBuffer("select count(*) from BoxMessage m where m.familyMob = '"+phoneNum+"' and m.msgIoType = 'IO_EntUserTOBOX'");
		if(!"".equals(keyWord) && keyWord!=null){
			hql.append(" and m.content like '%"+keyWord+"%'");
		}
		return count(hql.toString());
	}

	@Override
	public List<BoxMessage> getBoxChatLog(String phoneNum, Long boxId) {
		String hql = "from BoxMessage m where m.familyMob = '"+phoneNum+"' and m.fromBox.id = "+boxId+" or m.familyMob = '"+phoneNum+"' and m.toBox.id = "+boxId+" ORDER BY m.createdTime DESC";
		return getList(hql, 1, 10, new Object[]{});
	}

}


package com.bdbox.dao.impl;

import com.bdbox.constant.RefundStatus;
import com.bdbox.dao.RefundDao;
import com.bdbox.entity.Order;
import com.bdbox.entity.Refund;
import com.bdsdk.util.CommonMethod;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class RefundDaoImpl extends IDaoImpl<Refund, Long>
  implements RefundDao
{
  public Refund queryRefundUid(Long oid)
  {
    return (Refund)unique("from Refund t where t.orderId=?", new Object[] { oid });
  }

  public List<Refund> listRefund(String orderNo, Calendar applyTime, Calendar refundTime, String userName, RefundStatus refundStatus, Integer page, Integer pageSize, String order)
  {
    StringBuffer hql = new StringBuffer("from Refund t where 1=1 ");
    if (applyTime != null) hql.append("and t.applyTime like '%").append(CommonMethod.CalendarToString(applyTime, "yyy-MM-dd")).append("%'");
    if (refundTime != null) hql.append("and  t.refundTime like '%").append(CommonMethod.CalendarToString(refundTime, "yyyy-MM-dd")).append("%'");
    if (orderNo != null) hql.append("and  t.orderNo='").append(orderNo).append("'");
    if (userName != null) hql.append("and  t.userName='").append(userName).append("'");
    if (refundStatus != null) hql.append("and  t.refundStatus='").append(refundStatus).append("'");
    if (order != null) hql.append(" order by ").append(order);
    return getList(hql.toString(), page.intValue(), pageSize.intValue(), new Object[0]);
  }

  public int listRefundCount(String orderNo, Calendar applyTime, Calendar refundTime, String userName, RefundStatus refundStatus)
  {
    StringBuffer hql = new StringBuffer("select count(*) from Refund t where 1=1 ");
    if (applyTime != null) hql.append("and  t.applyTime like '%").append(CommonMethod.CalendarToString(applyTime, "yyy-MM-dd")).append("%'");
    if (refundTime != null) hql.append("and  t.refundTime like '%").append(CommonMethod.CalendarToString(refundTime, "yyyy-MM-dd")).append("%'");
    if (orderNo != null) hql.append("and  t.orderNo='").append(orderNo).append("'");
    if (userName != null) hql.append("and  t.userName='").append(userName).append("'");
    if (refundStatus != null) hql.append("and  t.refundStatus='").append(refundStatus).append("'");
    return count(hql.toString());
  }

  public List<Refund> getUserRefund(String user)
  {
    return listAll();
  }
  
  public List<Refund> getrefundNo(String orderNo){
	  String hql = "from Refund t where t.orderNo='" + orderNo + "'"; 
	  return getList(hql.toString(), 0, 0, new Object[0]); 
  }

}
package com.bdbox.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.ApplyDao;
import com.bdbox.entity.Apply;
import com.bdsdk.util.CommonMethod;
@Repository
public class ApplyDaoImpl extends IDaoImpl<Apply, Long> implements ApplyDao{

	public List<Apply> listApply(String code, String phone,
			Calendar createTime, String sex, Boolean join, Integer page,
			Integer pageSize, String apply) {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer("from Apply t where 1=1");
		if(createTime!=null)hql.append(" and t.createTime like '%").append(CommonMethod.CalendarToString(createTime, "yyyy-MM-dd")).append("%'");
		if(code!=null)hql.append(" and t.code='").append(code).append("'");
		if(phone!=null)hql.append(" and t.phone='").append(phone).append("'");
		if(sex!=null)hql.append(" and t.sex='").append(sex).append("'");
		if(join!=null){
			if(join){
				hql.append(" and t.join='").append(1).append("'");
			}else{
				hql.append(" and t.join='").append(0).append("'");
			}
		}
		if(apply!=null)hql.append(" order by ").append(apply);
		return getList(hql.toString(), page, pageSize);
	}

	public int applyCount(String code, String phone, Calendar createTime,
			String sex, Boolean join) {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer("select count(*) from Apply t where 1=1");
		if(createTime!=null)hql.append(" and t.createTime like '%").append(CommonMethod.CalendarToString(createTime, "yyyy-MM-dd")).append("%'");
		if(code!=null)hql.append(" and t.code='").append(code).append("'");
		if(phone!=null)hql.append(" and t.phone='").append(phone).append("'");
		if(sex!=null)hql.append(" and t.sex='").append(sex).append("'");
		if(join!=null){
			if(join){
				hql.append(" and t.join='").append(1).append("'");
			}else{
				hql.append(" and t.join='").append(0).append("'");
			}
		}
		return count(hql.toString());
	}

	public Apply getApply(String phone) {
		// TODO Auto-generated method stub
		return unique("from Apply t where t.phone = ?", new Object[]{phone});
	}

	public Apply getApplyUid(Long uid) {
		// TODO Auto-generated method stub
		return unique("from Apply t where t.user.id = ?", new Object[]{uid});
	}
}

package com.bdbox.dao.impl;

import java.util.List; 

import org.springframework.stereotype.Repository;  

import com.bdbox.dao.NotificationDao;
import com.bdbox.entity.Notification;
import com.bdbox.entity.User;
@Repository
public class NotificationDaoImpl extends IDaoImpl<Notification, Long> implements NotificationDao {
	public List<Notification> getlistNotification(String tongzhifanshi,String tongzhileixing,String type_twl,int page, int pageSize){
		StringBuffer hql = new StringBuffer("from Notification where 1=1 "); 
		if(tongzhifanshi!=null){
			hql.append(" and notificationType ='"+tongzhifanshi+"'");
 		}
		if(tongzhileixing!=null){
			hql.append(" and mailType ='"+tongzhileixing+"'");
 		}
		if(type_twl!=null){
			hql.append(" and typetwl ='"+type_twl+"'");
 		}
		hql.append(" order by createTime desc");
		List<Notification> results = getList(hql.toString(), page, pageSize);
		return results;
 	} 

	public Integer getlistNotification(String tongzhifanshi,String tongzhileixing,String type_twl){
		StringBuffer hql = new StringBuffer("select count(*) from Notification where 1=1 "); 
		if(tongzhifanshi!=null){
			hql.append(" and notificationType ='"+tongzhifanshi+"'");
 		}
		if(tongzhileixing!=null){
			hql.append(" and mailType ='"+tongzhileixing+"'");
 		}
		if(type_twl!=null){
			hql.append(" and typetwl ='"+type_twl+"'");
 		}
		hql.append(" order by createTime desc");
		return  count(hql.toString());
	}
	
	public Notification getnotification(String mailtype,String type_twl){ 
		StringBuffer hql = new StringBuffer("from Notification t where 1=1");
		if (mailtype != null) {
			hql.append(" and t.mailType='").append(mailtype)
					.append("'");
		}
		if (type_twl != null) {
			hql.append(" and t.typetwl='").append(type_twl)
					.append("'");
		}
		List<Notification> notification = getList(hql.toString(),1,1);
		if(notification.size()>0){
			return notification.get(0);
		}else{
			return null;
		}
	}
}

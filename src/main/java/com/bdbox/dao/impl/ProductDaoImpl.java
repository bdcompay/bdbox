package com.bdbox.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.ProductDao;
import com.bdbox.entity.Product;

@Repository
public class ProductDaoImpl extends IDaoImpl<Product, Long>	implements ProductDao  {
	@Override
	public List<Product> query(Product product, Calendar startTime,
			Calendar endTime, int page, int pageSize) {
		StringBuffer hql = new StringBuffer(" from Product where 1=1 ");
		if(product != null){
			if(product.getName()!=null){
				hql.append(" and name like '%"+product.getName()+"%' ");
			}
			if(product.getProductType()!=null){
				hql.append(" and productType='"+product.getProductType()+"' ");
			}
			if(product.getProductBuyType()!=null){
				hql.append(" and productBuyType='"+product.getProductBuyType()+"' ");
			}
		}
		if(startTime!=null && endTime!=null){
			hql.append(" and createdTime>=").append(startTime);
			hql.append(" and createdTime<=").append(endTime);
		}
		hql.append(" order by id desc");
		
		List<Product> results = getList(hql.toString(), page, pageSize);
		return results;
	}

	@Override
	public int queryAmount(Product product, Calendar startTime, Calendar endTime) {
		StringBuffer hql = new StringBuffer("select count(*) from Product where 1=1 ");
		if(product != null){
			if(product.getName()!=null){
				hql.append(" and name like '%"+product.getName()+"%' ");
			}
			if(product.getProductType()!=null){
				hql.append(" and productType='"+product.getProductType()+"' ");
			}
			if(product.getProductBuyType()!=null){
				hql.append(" and productBuyType='"+product.getProductBuyType()+"' ");
			}
		}
		if(startTime!=null && endTime!=null){
			hql.append(" and createdTime>=").append(startTime);
			hql.append(" and createdTime<=").append(endTime);
		}
		
		return  count(hql.toString());
	}

	@Override
	public Product getStockNumber() {
		String hql = "from Product p where p.productBuyType = 'SHOPPINGRUSH'";
		return unique(hql);
	}
}

package com.bdbox.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.SOSLocationDao;
import com.bdbox.entity.SOSLocation;
import com.bdsdk.util.CommonMethod;

@Repository
public class SOSLocationDaoImpl extends IDaoImpl<SOSLocation, Long> implements SOSLocationDao {

	@Override
	public SOSLocation getLocationForBoxId(Long boxId) {
		String hql = "from SOSLocation s where s.box.id = ?";
		return unique(hql, new Object[] { boxId });
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SOSLocation> getLocation() {
		StringBuffer sb = new StringBuffer("from SOSLocation s where 1=1");
		Calendar now = Calendar.getInstance();
		Calendar halfHour = Calendar.getInstance();
		halfHour.setTimeInMillis(halfHour.getTimeInMillis() - 30 * 60 * 1000);
		sb.append(" and s.updateTime >='").append(CommonMethod.CalendarToString(halfHour, "yyyy-MM-dd HH:mm:ss"))
				.append("'");
		sb.append(" and s.updateTime <='").append(CommonMethod.CalendarToString(now, "yyyy-MM-dd HH:mm:ss"))
				.append("'");
		sb.append(" order by s.createdTime desc");
		return getSession().createQuery(sb.toString()).list();
	}

	@Override
	public SOSLocation getLocation(Long boxId) {
		StringBuffer sb = new StringBuffer("from SOSLocation s where 1=1");
		if (boxId != null) {
			sb.append(" and s.box.id = " + boxId);
		}
		Calendar now = Calendar.getInstance();
		Calendar halfHour = Calendar.getInstance();
		halfHour.setTimeInMillis(halfHour.getTimeInMillis() - 30 * 60 * 1000);
		sb.append(" and s.updateTime >='").append(CommonMethod.CalendarToString(halfHour, "yyyy-MM-dd HH:mm:ss"))
				.append("'");
		sb.append(" and s.updateTime <='").append(CommonMethod.CalendarToString(now, "yyyy-MM-dd HH:mm:ss"))
				.append("'");
		sb.append(" order by s.createdTime desc");
		return (SOSLocation) getSession().createQuery(sb.toString()).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SOSLocation> getSosLocation(List<Long> ids) {
		StringBuffer hql = new StringBuffer("from SOSLocation s where 1=1");
		for (Long id : ids) {
			if (ids.indexOf(id) == 0) {
				hql.append(" and s.box.id = " + id);
			} else {
				hql.append(" or s.box.id = " + id);
			}
		}
		hql.append(" order by s.updateTime desc");
		return getSession().createQuery(hql.toString()).list();
	}

	@Override
	public List<SOSLocation> getSosLocations(List<Long> ids, Calendar startTime, Calendar endTime) {
		StringBuffer hql = new StringBuffer("from SOSLocation s where 1=1");
		if (ids.size() > 0) {
			for (Long id : ids) {
				if (ids.indexOf(id) == 0) {
					hql.append(" and (s.box.id = " + id);
				} else {
					hql.append(" or s.box.id = " + id);
				}
			}
			hql.append(")");
			if (startTime != null) {
				hql.append(" and s.updateTime >='" + CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")
						+ "'");
			}
			if (endTime != null) {
				hql.append(
						" and s.updateTime <='" + CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss") + "'");
			}
			hql.append(" order by s.updateTime desc");
			return getList(hql.toString(), 1, 9999);
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public int queryCount(List<Long> ids, Calendar startTime, Calendar endTime) {
		StringBuffer hql = new StringBuffer("select count(*) from SOSLocation s where 1=1");
		if (ids.size() > 0) {
			for (Long id : ids) {
				if (ids.indexOf(id) == 0) {
					hql.append(" and (s.box.id = " + id);
				} else {
					hql.append(" or s.box.id = " + id);
				}
			}
			hql.append(")");
			if (startTime != null) {
				hql.append(" and s.updateTime >='" + CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")
						+ "'");
			}
			if (endTime != null) {
				hql.append(
						" and s.updateTime <='" + CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss") + "'");
			}
			return count(hql.toString());
		} else {
			return 0;
		}

	}

	@Override
	public List<SOSLocation> getLocationsForBoxId(Long boxId) {
		String hql = "from SOSLocation s where s.box.id = ?";
		return getList(hql, 1, 99999, new Object[] { boxId });
	}

	public void deleByBoxId(long boxId) {
		String sql = "DELETE FROM SOSLocation WHERE box.id=:boxId";
		getSession().createSQLQuery(sql).setLong("boxId", boxId).executeUpdate();
	}

}

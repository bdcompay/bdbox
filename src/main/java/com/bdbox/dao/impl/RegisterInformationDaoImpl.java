package com.bdbox.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository; 

import com.bdbox.dao.RegisterInformationDao;
import com.bdbox.entity.RegisterInformation;
 
@Repository 
public class RegisterInformationDaoImpl extends IDaoImpl<RegisterInformation, Long>	implements RegisterInformationDao{
	
	public RegisterInformation getRealNameUrl(long userid,String type){
		 return unique("from RegisterInformation t where t.userid='"+userid+"' and t.type='"+type+"'");

	}
	
	public List<RegisterInformation> getRegisterInformation(int page, int pageSize){
		StringBuffer hql = new StringBuffer("from RegisterInformation t order by t.birthDate desc ");
		return getList(hql.toString(), page, pageSize);
	}

	public Integer getRegisterInformationCount(){
		StringBuffer hql = new StringBuffer("select count(*) from RegisterInformation t order by t.birthDate desc");
		return count(hql.toString());
	}
	
	public List<RegisterInformation> getRegisterInformation(){
		StringBuffer hql = new StringBuffer("from RegisterInformation t order by t.birthDate desc ");
		return getList(hql.toString(), 0, 0, new Object[0]);
	}

}

package com.bdbox.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.IDao;
import com.bdbox.dao.UserPhotoUrlDao;
import com.bdbox.entity.UserPhotoUrl;
import com.bdbox.service.UserPhotoUrlService;

@Repository
public class UserPhotoUrlDaoImpl extends IDaoImpl< UserPhotoUrl, Long> implements UserPhotoUrlDao {

	public UserPhotoUrl getUserPhotoUrl(Long userid){
		return unique("from UserPhotoUrl t where t.userId = ?", new Object[]{userid});
	}
	
	public UserPhotoUrl getRealNameUrl(long userid){
 		 return unique("from UserPhotoUrl t where t.userId='"+userid+"'");

	}

 		 
}

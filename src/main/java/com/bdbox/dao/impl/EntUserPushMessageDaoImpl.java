package com.bdbox.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.EntUserPushMessageDao;
import com.bdbox.entity.EntUserPushMessage;
import com.bdsdk.util.CommonMethod;

@Repository
public class EntUserPushMessageDaoImpl extends IDaoImpl<EntUserPushMessage, Long> implements EntUserPushMessageDao {

	@Override
	public List<EntUserPushMessage> query(Long entUserId, Long boxMessageId, Integer count, Boolean isSuccess,
			Calendar startTime, Calendar endTime, int page, int pageSize) {
		StringBuffer hql=new StringBuffer("from EntUserPushMessage em where 1=1 ");
		if(entUserId!=null){
			hql.append(" and em.enterpriseUser.id=").append(entUserId);
		}
		if(boxMessageId!=null){
			hql.append(" and em.boxMessage.id=").append(boxMessageId);
		}
		if(count!=null){
			hql.append(" and em.count<").append(count);
		}
		if(isSuccess!=null){
			hql.append(" and em.isSuccess=").append(isSuccess);
		}
		if(startTime!=null){
			hql.append(" and em.createTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(endTime!=null){
			hql.append(" and em.createTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		hql.append("order by em.id desc");
		return getList(hql.toString(), page, pageSize);
	}

	@Override
	public int amount(Long entUserId, Long boxMessageId, Integer count, Boolean isSuccess, Calendar startTime,
			Calendar endTime) {
		StringBuffer hql=new StringBuffer("select count(*) from EntUserPushMessage em where 1=1 ");
		if(entUserId!=null){
			hql.append(" and em.enterpriseUser.id=").append(entUserId);
		}
		if(boxMessageId!=null){
			hql.append(" and em.boxMessage.id=").append(boxMessageId);
		}
		if(count!=null){
			hql.append(" and em.count<").append(count);
		}
		if(isSuccess!=null){
			hql.append(" and em.isSuccess=").append(isSuccess);
		}
		if(startTime!=null){
			hql.append(" and em.createTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		if(endTime!=null){
			hql.append(" and em.createTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		return count(hql.toString());
	}

}

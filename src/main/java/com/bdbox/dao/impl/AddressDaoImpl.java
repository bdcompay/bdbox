package com.bdbox.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.AddressDao;
import com.bdbox.entity.Address;

@Repository
public class AddressDaoImpl extends IDaoImpl<Address, Long> 
				implements AddressDao {
	@Override
	public List<Address> listByUserid(Long userId) {
		if(userId == null) return null;
		String hql = "from Address where 1=1 and userId="+userId;
		return getList(hql, 1, 10);
	}

	@Override
	public Address getAddress(Long userId) {
		String hql = "from Address where 1=1 and userId="+userId;
		return (Address) getSession().createQuery(hql).uniqueResult();
	}
}

package com.bdbox.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.constant.BoxStatusType;
import com.bdbox.constant.CardType;
import com.bdbox.dao.BoxDao;
import com.bdbox.entity.Box;

@Repository
public class BoxDaoImpl extends IDaoImpl<Box, Long> implements BoxDao {
	
	@Override
	public List<Box> query(Long userId, CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType,
			String snNumber, String boxName, String userName, String order,
			int page, int pageSize) {
		return query(userId, cardType, cardNumber, boxSerialNumber, boxStatusType, snNumber, boxName, userName, null,order, page, pageSize);
	}

	@Override
	public int queryAmount(Long userId, CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType,
			String snNumber, String boxName, String userName) {
		return queryAmount(userId, cardType, cardNumber, boxSerialNumber, boxStatusType, snNumber, boxName, userName,null);
	}
	
	@Override
	public List<Box> query(Long userId,CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType,
			String snNumber, String boxName, String userName,Long entUserId,String order, int page, int pageSize) {
		StringBuffer hql = new StringBuffer("from Box t where 1=1");
		if (userId!=null) {
			hql.append(" and t.user.id='").append(userId).append("'");
		}
		if (cardType != null) {
			hql.append(" and t.cardType='").append(cardType).append("'");
		}
		if (cardNumber != null) {
			hql.append(" and t.cardNumber='").append(cardNumber).append("'");
		}
		if (boxSerialNumber != null) {
			hql.append(" and t.boxSerialNumber='").append(boxSerialNumber).append("'");
		}
		if (boxStatusType != null) {
			hql.append(" and t.boxStatusType='").append(boxStatusType).append("'");
		}
		if (snNumber != null) {
			hql.append(" and t.snNumber='").append(snNumber).append("'");
		}
		if (boxName != null) {
			hql.append(" and t.name like '%").append(boxName).append("%'");
		}
		if (userName != null) {
			hql.append(" and t.user.username='").append(userName).append("'");
		}
		if(entUserId!=null){
			hql.append(" and t.entUser.id=").append(entUserId);
		}
		if (order != null) {
			hql.append(" order by t.").append(order);
		}
		List<Box> boxs = getList(hql.toString(), page, pageSize);
		return boxs;
	}
	
	@Override
	public int queryAmount(Long userId,CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType, String snNumber,String boxName, String userName,Long entUserId) {
		StringBuffer hql = new StringBuffer(
				"select count(*) from Box t where 1=1");
		if (userId!=null) {
			hql.append(" and t.user.id='").append(userId).append("'");
		}
		if (cardType != null) {
			hql.append(" and t.cardType='").append(cardType).append("'");
		}
		if (cardNumber != null) {
			hql.append(" and t.cardNumber='").append(cardNumber).append("'");
		}
		if (boxSerialNumber != null) {
			hql.append(" and t.boxSerialNumber='").append(boxSerialNumber).append("'");
		}
		if (boxStatusType != null) {
			hql.append(" and t.boxStatusType='").append(boxStatusType).append("'");
		}
		if (snNumber != null) {
			hql.append(" and t.snNumber='").append(snNumber).append("'");
		}
		if (boxName != null) {
			hql.append(" and t.name like '%").append(boxName).append("%'");
		}
		if (userName != null) {
			hql.append(" and t.user.username='").append(userName).append("'");
		}
		if(entUserId!=null){
			hql.append(" and t.entUser.id=").append(entUserId);
		}
		return count(hql.toString());
	}
	
	public List<Box> queryBoxId(Long boxNumber){
		String hql = "from Box t where t.boxSerialNumber='" + boxNumber + "'"; 
		  return getList(hql.toString(), 0, 0, new Object[0]); 
	}
	
	@Override
	public int countByEntUserId(long entUserId) {
		StringBuffer hql = new StringBuffer("select count(*) from Box t where 1=1 and entUser.id="+entUserId);
		return count(hql.toString());
	}

	@Override
	public List<Box> queryByEntUserId(long entUserId, int page, int pageSize) {
		StringBuffer hql = new StringBuffer("from Box t where 1=1 and entUser.id="+entUserId);
		List<Box> boxs = getList(hql.toString(), page, pageSize);
		return boxs;
	}

	@Override
	public Box getBox(String cardNumber) {
		String hql = "from Box b where b.cardNumber = '"+cardNumber+"' or b.boxSerialNumber = '"+cardNumber+"'";
		return (Box) getSession().createQuery(hql).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Box> getBoxInfo(String boxSerialNumber) {
		String hql = "from Box b where b.boxSerialNumber like '%"+boxSerialNumber+"%'";
		return getSession().createQuery(hql).list();
	}

	@Override
	public Box getBoxId(String boxSerialNumber) {
		String hql = "from Box b where b.boxSerialNumber = ?";
		return unique(hql, new Object[]{boxSerialNumber});
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Box> getEntUserBoxs(Long entUserId) {
		String hql = "from Box b where b.entUser.id ="+entUserId;
		return getSession().createQuery(hql).list();
	}

	@Override
	public Box getEntUserBox(Long entUserId, String cardId) {
		String hql = "from Box b where b.entUser.id ="+entUserId+" "
				+ "and b.boxSerialNumber = '"+cardId+"' or b.cardNumber = '"+cardId+"'";
		return (Box) getSession().createQuery(hql).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Box> searchBox(Long entUserId, String cardId) {
		String hql = "from Box b where b.entUser.id = "+ entUserId +" "
				+ "and b.boxSerialNumber like '%"+cardId+"%'";
		return getSession().createQuery(hql).list();
	}
	
}

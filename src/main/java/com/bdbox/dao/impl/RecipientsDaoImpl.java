package com.bdbox.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.RecipientsDao;
import com.bdbox.entity.Recipients;

@Repository
public class RecipientsDaoImpl extends IDaoImpl<Recipients, Long>
		implements RecipientsDao{

	@Override
	public void updateDefault(Long id) {
		String hql = "update Recipients r set r.isDefault = 1 where r.id = "+id;
		getSession().createQuery(hql).executeUpdate();
	}

	@Override
	public void setZero() {
		String hql = "update Recipients r set r.isDefault = 0";
		getSession().createQuery(hql).executeUpdate();
	}

	@Override
	public Long count() {
		String hql = "select count(*) from Recipients";
		return (Long) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public Recipients getRecipients() {
		String hql = "from Recipients r where r.isDefault = 1";
		return (Recipients) getSession().createQuery(hql).uniqueResult();
	}

}

package com.bdbox.dao.impl;

import java.util.Calendar;
import java.util.List;  

import org.springframework.stereotype.Repository; 

import com.bdbox.dao.UserSendBoxMessageDao;
import com.bdbox.entity.UserSendBoxMessage;
import com.bdsdk.util.CommonMethod;
@Repository
public class UserSendBoxMessageDaoImpl extends  IDaoImpl<UserSendBoxMessage, Long> implements UserSendBoxMessageDao {

public List<UserSendBoxMessage> sdkUserSend(Long serialNumber,Calendar startTime,Calendar endTime,Long userid,String order, Integer page,Integer pageSize){
		
		StringBuffer hql = new StringBuffer("from UserSendBoxMessage t where 1=1");
		 
		if(serialNumber!=null){
			hql.append(" and t.boxSerialNumber='").append(serialNumber).append("'");
		}
		if (startTime != null) {
			hql.append(" and t.sendTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd ")).append("'");
		}
		if (endTime != null) {
			hql.append(" and t.sendTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd")).append("'");
		}
		if(userid!=null){
			hql.append(" and t.fromUser.id='").append(userid).append("'");
		} 
		if(order!=null){
			hql.append("order by t.").append(order);
		}
		List<UserSendBoxMessage> userSendBoxMessage=getList(hql.toString(), page, pageSize);
		return userSendBoxMessage;
	}
	
	public Integer count(Long serialNumber,Calendar startTime,Calendar endTime,Long userid){
		StringBuffer hql = new StringBuffer("select count(*) from UserSendBoxMessage t where 1=1");
		if(serialNumber!=null){
			hql.append(" and t.boxSerialNumber='").append(serialNumber).append("'");
		}
		if (startTime != null) {
			hql.append(" and t.sendTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd ")).append("'");
		}
		if (endTime != null) {
			hql.append(" and t.sendTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd")).append("'");
		}
		if(userid!=null){
			hql.append(" and t.fromUser.id='").append(userid).append("'");
		}
		return count(hql.toString());
	}


}

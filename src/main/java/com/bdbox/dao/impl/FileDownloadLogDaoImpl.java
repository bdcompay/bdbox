package com.bdbox.dao.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.FileDownloadLogDao;
import com.bdbox.entity.FileDownloadLog;
import com.bdsdk.util.CommonMethod;

/**
 * 文件下载记录DAO实现类
 * 
 * @ClassName: FileDownloadLogDaoImpl 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-2-26 下午2:07:57 
 * @version V5.0 
 */
@Repository
public class FileDownloadLogDaoImpl 
		extends IDaoImpl<FileDownloadLog,Long> implements FileDownloadLogDao {

	/*
	 * (non-Javadoc)
	 * @see com.bdhezi.dao.FileDownloadLogDao#query(java.lang.String, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar, int, int)
	 */
	@Override
	public List<FileDownloadLog> query(String filename, Boolean status,
			String ip, String ipArea, String username, Calendar startTime,
			Calendar endTime, int page, int pageSize) {
		StringBuffer hql = new StringBuffer(" from FileDownloadLog where 1=1 ");
		if(filename!=null && !"".equals(filename)){
			hql.append(" and filename like '%").append(filename).append("%'");
		}
		if(status!=null){
			hql.append(" and status=").append(status);
		}
		if(ip!=null && !"".equals(ip)){
			hql.append(" and ip like '%").append(ip).append("%'");
		}
		if(ipArea!=null && !"".equals(ipArea)){
			hql.append(" and ipArea like '%").append(ipArea).append("%'");
		}
		if(username!=null && !"".equals(username)){
			hql.append(" and username like '%").append(username).append("%'");
		}
		if(startTime!=null && endTime!=null){
			hql.append(" and downloadTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
			hql.append(" and downloadTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		hql.append(" order by id desc ");
		
		return getList(hql.toString(), page, pageSize);
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdhezi.dao.FileDownloadLogDao#count(java.lang.String, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public int count(String filename, Boolean status, String ip, String ipArea,
			String username, Calendar startTime, Calendar endTime) {
		StringBuffer hql = new StringBuffer("select count(*) from FileDownloadLog where 1=1 ");
		if(filename!=null && !"".equals(filename)){
			hql.append(" and filename like '%").append(filename).append("%'");
		}
		if(status!=null){
			hql.append(" and status=").append(status);
		}
		if(ip!=null && !"".equals(ip)){
			hql.append(" and ip like '%").append(ip).append("%'");
		}
		if(ipArea!=null && !"".equals(ipArea)){
			hql.append(" and ipArea like '%").append(ipArea).append("%'");
		}
		if(username!=null && !"".equals(username)){
			hql.append(" and username like '%").append(username).append("%'");
		}
		if(startTime!=null && endTime!=null){
			hql.append(" and downloadTime>='").append(CommonMethod.CalendarToString(startTime, "yyyy-MM-dd HH:mm:ss")).append("'");
			hql.append(" and downloadTime<='").append(CommonMethod.CalendarToString(endTime, "yyyy-MM-dd HH:mm:ss")).append("'");
		}
		return count(hql.toString());
	}

}

package com.bdbox.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.BoxPartnerDao;
import com.bdbox.entity.BoxPartner;

@Repository
public class BoxPartnerDaoImpl extends IDaoImpl<BoxPartner, Long> implements
		BoxPartnerDao {

	public List<BoxPartner> query(Long boxId, String partnerBoxSerialNumber,
			int page, int pageSize) {
		StringBuffer hql = new StringBuffer("from BoxPartner t where 1=1");
		if (boxId != null) {
			hql.append(" and t.box.id=").append(boxId);
		}
		if (partnerBoxSerialNumber != null) {
			hql.append(" and t.partnerBoxSerialNumber='")
					.append(partnerBoxSerialNumber).append("'");
		}
		List<BoxPartner> boxPartners = getList(hql.toString(), page, pageSize);
		return boxPartners;
	}

	public int queryAmount(Long boxId, String partnerBoxSerialNumber) {
		StringBuffer hql = new StringBuffer(
				"select count(*) from BoxPartner t where 1=1");
		if (boxId != null) {
			hql.append(" and t.box.id=").append(boxId);
		}
		if (partnerBoxSerialNumber != null) {
			hql.append(" and t.partnerBoxSerialNumber='")
					.append(partnerBoxSerialNumber).append("'");
		}
		return count(hql.toString());
	}

}

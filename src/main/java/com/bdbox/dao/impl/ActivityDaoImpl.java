package com.bdbox.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.ActivityDao;
import com.bdbox.entity.Activity;

@Repository
public class ActivityDaoImpl extends IDaoImpl<Activity, Long> implements
		ActivityDao {
	@Override
	public int counts(String sql) {
		int count = count(sql);
		return count;
	}

}

package com.bdbox.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.InvoiceDao;
import com.bdbox.entity.Invoice;
import com.bdbox.entity.Order;

@Repository
public class InvoiceDaoImpl extends IDaoImpl<Invoice, Long>
		implements InvoiceDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<Invoice> getAllInvoice(Long user) {
		String hql = "from Invoice where 1=1 and user = " + user +"and checkStatus = 'PASS'";
		return getSession().createQuery(hql).list();
	}

	@Override
	public Invoice getInvoice(Long id) {
		String hql = "from Invoice where 1=1 and id = " + id;
		return (Invoice) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public Long saveInvoice(Invoice invoice) {
		return (Long) getSession().save(invoice);
	}

	@Override
	public Invoice getCheckStatus(Long userId) {
		String hql = "from Invoice i where i.user = "+ userId +" and i.invoiceType = 'DEDICATED'";
		return (Invoice) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public Order getOrder(Long invoice) {
		String hql = "from Order o where o.invoice = "+invoice;
		return (Order) getSession().createQuery(hql).uniqueResult();
	}
	
	@Override
	public Invoice getAllInvoices(Long user){
		String hql = "from Invoice i where i.user = "+ user +" and i.moren = '1'";
		return (Invoice) getSession().createQuery(hql).uniqueResult();
	}

}

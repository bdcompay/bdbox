package com.bdbox.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bdbox.dao.EntStatDailyDao;
import com.bdbox.entity.EntStatDaily;
import com.bdbox.entity.EnterpriseUser;
import com.bdbox.util.BdCommonMethod;
import com.bdbox.web.dto.EntStatDailyDto;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

/**
 * 统计每日企业用户数据DAO实现类
 * @author jlj
 *
 */
@Repository
public class EntStatDailyDaoImpl extends IDaoImpl<EntStatDaily, Long> implements EntStatDailyDao {

	@Override
	public List<EntStatDaily> listEnStatDailies(Calendar startDate,
			Calendar endDate, Long entId, int page, int pageSize) {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer(" from EenStatDaily where 1=1 ");
		if(startDate!=null && endDate != null){
			hql.append(" and date>='").append(CommonMethod.CalendarToString(startDate, "yyyy-MM-dd")).append("'");
			hql.append(" and date<'").append(CommonMethod.CalendarToString(endDate, "yyyy-MM-dd")).append("'");
		}
		if(entId != null){
			hql.append(" and enterpriseUser.id='").append(entId).append("'");
		}
		return getList(hql.toString(), page, pageSize);
	}

	@Override
	public int amount(Calendar startDate, Calendar endDate, Long entId) {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer(" select count(*) from EenStatDaily where 1=1 ");
		if(startDate!=null && endDate != null){
			hql.append(" and date>='").append(CommonMethod.CalendarToString(startDate, "yyyy-MM-dd")).append("'");
			hql.append(" and date<='").append(CommonMethod.CalendarToString(endDate, "yyyy-MM-dd")).append("'");
		}
		if(entId != null){
			hql.append(" and enterpriseUser.id='").append(entId).append("'");
		}
		return count(hql.toString());
	}

	@Override
	public EntStatDaily get(Calendar date, long entId) {
		// TODO Auto-generated method stub
		if(date==null) return null;
		StringBuffer hql = new StringBuffer(" from EntStatDaily where 1=1 ");
		hql.append(" and date='").append(CommonMethod.CalendarToString(date, "yyyy-MM-dd")).append("'")
			.append(" and enterpriseUser.id=").append(entId).append("");
		return unique(hql.toString());
	}

	@Override
	public List<EntStatDailyDto> statistic(Calendar startDate, Calendar endDate,
			Long entId,int page,int pageSize) {
		// TODO Auto-generated method stub
		StringBuffer sql = new StringBuffer(" select ent_id,sum(boxSendNumber) as boxSendNumber "
				+ "from r_ent_stat_daily where 1=1 ");
		if(startDate!=null && endDate != null){
			sql.append(" and date>='").append(CommonMethod.CalendarToString(startDate, "yyyy-MM-dd")).append("'");
			sql.append(" and date<='").append(CommonMethod.CalendarToString(endDate, "yyyy-MM-dd")).append("'");
		}
		if(entId != null){
			sql.append(" and ent_id='").append(entId).append("'");
		}
		sql.append(" group by ent_id");
		//分页
		Query queryObject =getSession().createSQLQuery(sql.toString());   
        queryObject.setFirstResult((page - 1) * pageSize);  
        queryObject.setMaxResults(pageSize);  
    
	 	List list = queryObject.list();
	 	List<EntStatDailyDto> entStatDailyDtos = new ArrayList<EntStatDailyDto>();
		
		for(Iterator iterator = list.iterator();iterator.hasNext();){ 
			EntStatDailyDto entStatDailyDto = new EntStatDailyDto();
            //每个集合元素都是一个数组
            Object[] objects = (Object[]) iterator.next(); 
            //
            EnterpriseUser ent = new EnterpriseUser();
            ent.setId(Long.valueOf(objects[0].toString()));
            entStatDailyDto.setEnterpriseUser(ent);
            
            entStatDailyDto.setBoxSendNumber(Integer.valueOf(objects[1].toString()));
            
            entStatDailyDto.setStartDateStr(CommonMethod.CalendarToString(startDate, "yyyy-MM-dd"));
    		entStatDailyDto.setEndDateStr(CommonMethod.CalendarToString(endDate, "yyyy-MM-dd"));
    		
    		entStatDailyDtos.add(entStatDailyDto);
		}
		return entStatDailyDtos;
	}

	@Override
	public int statisticCount(Calendar startDate, Calendar endDate, Long entId) {
		// TODO Auto-generated method stub
		StringBuffer sql = new StringBuffer(" select ent_id,sum(boxSendNumber) as boxSendNumber "
				+ "from r_ent_stat_daily where 1=1 ");
		if(startDate!=null && endDate != null){
			sql.append(" and date>='").append(CommonMethod.CalendarToString(startDate, "yyyy-MM-dd")).append("'");
			sql.append(" and date<='").append(CommonMethod.CalendarToString(endDate, "yyyy-MM-dd")).append("'");
		}
		if(entId != null){
			sql.append(" and ent_id='").append(entId).append("'");
		}
		sql.append(" group by ent_id");
		List list = getSession().createSQLQuery(sql.toString()).list();
		return list.size();
	}

}

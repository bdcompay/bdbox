package com.bdbox.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.MobCodeDao;
import com.bdbox.entity.MobCode;

@Repository
public class MobCodeDaoImpl extends IDaoImpl<MobCode, Long> implements MobCodeDao{

	//根据手机号码查询单条记录
	public MobCode findCodeByMob(String phone) {
		String hql = "from MobCode m where m.phone = '"+phone+"'";
		return (MobCode) getSession().createQuery(hql).uniqueResult();
	}

}

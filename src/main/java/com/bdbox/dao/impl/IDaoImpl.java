package com.bdbox.dao.impl;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;

import javax.persistence.Id;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.bdbox.dao.IDao;


/**
 * 对实体通用的CRUD操作的实现
 * 
 * @author will.yan
 * 
 * @param <E>
 *            实体类型
 * @param <ID>
 *            实体的主键
 */
public abstract class IDaoImpl<E, ID extends Serializable> implements
		IDao<E, ID> {

	@Autowired
	@Qualifier("sessionFactory")
	private SessionFactory sessionFactory;

	/**
	 * 实体的class类别
	 */
	private final Class<E> entityClass;

	/**
	 * 实体的主键名称
	 */
	private String idName = null;

	private final String HQL_LIST_ALL;
	private final String HQL_COUNT_ALL;
	private final String HQL_MAXID;
	private final String HQL_MINID;

	/**
	 * 在构造器中初始化成员变量,包括当前实体的class类型,实体的主键名称等
	 */
	@SuppressWarnings("unchecked")
	public IDaoImpl() {
		this.entityClass = (Class<E>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
		Field[] fields = this.entityClass.getDeclaredFields();
		for (Field f : fields) {
			if (f.isAnnotationPresent(Id.class)) {
				this.idName = f.getName();
			}
		}

		HQL_LIST_ALL = "from " + this.entityClass.getSimpleName()
				+ " order by " + idName + " desc";
		HQL_COUNT_ALL = " select count(*) from "
				+ this.entityClass.getSimpleName();
		HQL_MAXID = " select max(id) from " + this.entityClass.getSimpleName();
		HQL_MINID = " select min(id) from " + this.entityClass.getSimpleName();
	}

	@SuppressWarnings("unchecked")
	public E save(E entity) {
		ID id = (ID) getSession().save(entity);
		return this.get(id);
	}

	public void delete(ID id) {
		getSession().delete(this.get(id));
	}

	public void deleteObject(E entity) {
		getSession().delete(entity);

	}

	public void update(E entity) {
		getSession().update(entity);
	}

	@SuppressWarnings("unchecked")
	public E get(ID id) {
		return (E) getSession().get(entityClass, id);
	}

	public ID maxId() {
		return this.aggregate(HQL_MAXID);
	}

	public ID minId() {
		return this.aggregate(HQL_MINID);

	}

	public int countAll() {
		Long count = this.aggregate(HQL_COUNT_ALL);
		return count.intValue();
	}

	public int count(String hql) {
		Long count = this.aggregate(hql);
		return count.intValue();
	}

	public List<E> listAll() {
		List<E> list = getList(HQL_LIST_ALL, 0, 9999999);
		return list;
	}

	public List<E> list(int page, int pageSize) {
		List<E> list = getList(HQL_LIST_ALL, page, pageSize);
		return list;
	}

	public boolean exists(ID id) {
		E entity = this.get(id);
		boolean exist = (entity != null);
		return exist;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getSession() {
		// 事务必须是开启的(Required)，否则获取不到
		return sessionFactory.getCurrentSession();
	}

	protected void excuteHQL(String hql) {
		getSession().createQuery(hql).executeUpdate();
	}

	public <T> List<T> getList(final String hql, final int page,
			final int pageSize, final Object... paramlist) {
		Query query = getSession().createQuery(hql);
		setParameters(query, paramlist);
		if (page > -1 && pageSize > -1) {
			query.setMaxResults(pageSize);

			int start = (page - 1) * pageSize;
			if (start != 0) {
				query.setFirstResult(start);
			}
		}
		if (page < 0) {
			query.setFirstResult(0);
		}
		@SuppressWarnings("unchecked")
		List<T> results = query.list();

		return results;
	}

	/**
	 * 根据查询条件返回唯一一条记录
	 */
	@SuppressWarnings("unchecked")
	protected <T> T unique(final String hql, final Object... paramlist) {
		Query query = getSession().createQuery(hql);
		setParameters(query, paramlist);
		return (T) query.setMaxResults(1).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	protected <T> T aggregate(final String hql, final Object... paramlist) {
		Query query = getSession().createQuery(hql);
		setParameters(query, paramlist);

		return (T) query.uniqueResult();
	}

	protected void setParameters(Query query, Object[] paramlist) {
		if (paramlist != null) {
			for (int i = 0; i < paramlist.length; i++) {
				if (paramlist[i] instanceof Date) {
					// TODO 难道这是bug 使用setParameter不行？？
					query.setTimestamp(i, (Date) paramlist[i]);
				} else {
					query.setParameter(i, paramlist[i]);
				}
			}
		}
	}

	public void saveOrUpdate(E entity){
		getSession().saveOrUpdate(entity);
	}
}

package com.bdbox.dao.impl;

import com.bdbox.constant.ExpressStatus;
import com.bdbox.dao.ExpressDao;
import com.bdbox.entity.Express;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class ExpressDaoImpl extends IDaoImpl<Express, Long>
  implements ExpressDao
{
  public List<Express> listExpress(String expressName, String expressNumber, Calendar sendTime, Calendar completionTime, ExpressStatus expressStatus, Integer page, Integer pageSize, String order)
  {
    return null;
  }

  public int listExpressCount(String expressName, String expressNumber, Calendar sendTime, Calendar completionTime, ExpressStatus expressStatus)
  {
    return 0;
  }

  public Express queryExpressDto(Long oid)
  {
    return (Express)unique("from Express t where t.orderId=?", new Object[] { oid });
  }

  public List<Express> listAllSendExpress()
  {
    return getList("from Express t where 1=1 and t.expressStatus!='SIGN' and t.waybillUrl = null", 0, 0, new Object[0]);
  }
  
  public Express queryExpressByexpressNo(String expressNo){
	    return (Express)unique("from Express t where t.expressNumber=?", new Object[] { expressNo });

  }

  @Override
  public List<Express> getAllSelExpress() {
	  return getList("from Express t where 1=1 and t.expressStatus!='SIGN' and t.waybillUrl != null", 0, 0, new Object[0]);
  }

  @Override
  public Express queryExpressByOrderId(Long orderId) {
	  return unique("from Express e where 1=1 and e.orderId=? and e.waybillUrl != null", new Object[] { orderId });
  }

}
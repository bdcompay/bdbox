package com.bdbox.dao.impl;

import java.util.List; 

import org.springframework.stereotype.Repository; 

import com.bdbox.api.dto.UserQuestionDto;
import com.bdbox.dao.UserQuestionDao;
import com.bdbox.entity.UserQuestion;
@Repository
public class UserQuestionDaoImpl extends IDaoImpl<UserQuestion, Long> implements UserQuestionDao {

	public List<UserQuestion> getuserquestion(String userid){
		StringBuffer hql = new StringBuffer("from UserQuestion where 1=1 "); 
		if(userid!=null){
			hql.append(" and userid ='"+userid+"'");
 		}
		hql.append("order by submitTime ");
 		List<UserQuestion> results = getList(hql.toString(), 1, 9999999);
		return results;
	}
	
	public List<UserQuestion> getuserquestionlist(String username,int page,int pageSize){
		StringBuffer hql = new StringBuffer("from UserQuestion where 1=1 "); 
		if(username!=null){
			hql.append(" and userid ='"+username+"'");
 		}
		hql.append("order by submitTime ");
 		List<UserQuestion> results = getList(hql.toString(), page, pageSize);
		return results;
	}
 	
	public Integer getuserquestionlistcount(String username){
		StringBuffer hql = new StringBuffer("select count(*) from UserQuestion where 1=1 "); 
		if(username!=null){
			hql.append(" and userid ='"+username+"'");
 		}
		hql.append("order by submitTime ");
		return  count(hql.toString());
	}
}

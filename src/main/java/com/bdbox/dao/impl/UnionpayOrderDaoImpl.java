package com.bdbox.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.UnionpayOrderDao;
import com.bdbox.entity.UnionpayOrder;

@Repository
public class UnionpayOrderDaoImpl extends IDaoImpl<UnionpayOrder, Long> 
		implements UnionpayOrderDao{

	@Override
	public UnionpayOrder getUnionpayOrder(String orderId) {
		String hql = "from UnionpayOrder u where u.orderId = '"+orderId+"'";
		return (UnionpayOrder) getSession().createQuery(hql).uniqueResult();
	}

	@Override
	public UnionpayOrder getOrderByQueryId(String queryId) {
		String hql = "from UnionpayOrder u where u.queryId = '"+queryId+"'";
		return (UnionpayOrder) getSession().createQuery(hql).uniqueResult();
	}
}

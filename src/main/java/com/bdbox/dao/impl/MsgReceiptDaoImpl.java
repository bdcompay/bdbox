package com.bdbox.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdbox.dao.MsgReceiptDao;
import com.bdbox.entity.MsgReceipt;

@Repository
public class MsgReceiptDaoImpl extends IDaoImpl<MsgReceipt, Long> implements
		MsgReceiptDao {

}

package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.EntStatDaily;
import com.bdbox.entity.EnterpriseUser;
import com.bdbox.web.dto.EntStatDailyDto;

public interface EntStatDailyDao extends IDao<EntStatDaily, Long> {
	
	/**
	 * 分页查询企业用户每日统计数量
	 * @param startDate
	 * 		开始日期，包含开始时间
	 * @param endDate
	 * 		结束日期，不包含结束时间
	 * @param entId
	 * 		企业用户id
	 * @param page
	 * 		页数
	 * @param pageSize
	 * 		每页数量
	 * @return
	 */
	public List<EntStatDaily> listEnStatDailies(Calendar startDate,Calendar endDate,Long entId,int page,int pageSize);
	/**
	 * 统计数量
	 * @param startDate
	 * 		开始日期，包含开始时间
	 * @param endDate
	 * 		结束日期，不包含结束时间
	 * @param entId
	 * 		企业用户id
	 * @return
	 */
	public int amount(Calendar startDate,Calendar endDate,Long entId);
	
	/**
	 * 通过日期和企业查询统计记录
	 * @param Date
	 * 		日期(非空)
	 * @param entId
	 * 		企业id
	 * @return
	 */
	public EntStatDaily get(Calendar date,long entId);
	
	/**
	 * 统计
	 * @param startDate
	 * 		开始时间
	 * @param endDate
	 * 		结束时间
	 * @param enterpriseUser
	 * 		企业
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<EntStatDailyDto> statistic(Calendar startDate, Calendar endDate,
			Long entId,int page,int pageSize);
	
	public int statisticCount(Calendar startDate, Calendar endDate,Long entId);
	
	
}

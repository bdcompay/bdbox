package com.bdbox.dao;

import com.bdbox.constant.AppType;
import com.bdbox.entity.AppConfig;

public interface AppconfigDao extends IDao<AppConfig, Long> {
	public AppConfig query(AppType appType);

}

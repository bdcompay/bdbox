package com.bdbox.dao;

import com.bdbox.entity.PhoneRestrict;

public interface PhoneRestrictDao extends IDao<PhoneRestrict, Long>{

	/**
	 * 根据手机号码获取数据
	 * @param phoneNum
	 * @return
	 */
	public PhoneRestrict getPhone(String phoneNum);
	
	/**
	 * 统计
	 * @return
	 */
	public int count();
	
}

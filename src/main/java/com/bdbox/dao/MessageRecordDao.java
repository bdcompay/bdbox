package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.MessageRecord;

public interface MessageRecordDao extends IDao<MessageRecord, Long>{

	/**
	 * 分页获取短信数据
	 * @param mob
	 * @param content
	 * @param startTime
	 * @param endTime
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<MessageRecord> getMsgAll(String mob, String content, 
			Calendar startTime, Calendar endTime, int page, int pageSize);
	
	/**
	 * 统计数量
	 * @param mob
	 * @param content
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public int queryCount(String mob, String content, 
			Calendar startTime, Calendar endTime);
}

package com.bdbox.dao;

 import java.util.List;

import com.bdbox.api.dto.UserQuestionDto;
import com.bdbox.entity.UserQuestion;

public interface UserQuestionDao extends IDao<UserQuestion, Long> {

	public List<UserQuestion> getuserquestion(String userid);
	
	public List<UserQuestion> getuserquestionlist(String username,int page,int pageSize);
 	
	public Integer getuserquestionlistcount(String username);
}

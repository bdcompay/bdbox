package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.constant.DealStatus;
import com.bdbox.constant.ProductType;
import com.bdbox.entity.Order;

public abstract interface OrderDao extends IDao<Order, Long>
{
  public abstract List<Order> listOrder(String paramString1, ProductType paramProductType, String paramString2, Calendar paramCalendar, String paramString3, DealStatus paramDealStatus, Integer paramInteger1, Integer paramInteger2, String paramString4,String transactionType,String dealStatusStrbd,String name);

  public abstract int listOrderCount(String paramString1, ProductType paramProductType, String paramString2, Calendar paramCalendar, String paramString3, DealStatus paramDealStatus,String transactionType,String dealStatusStrbd,String name);

  public abstract List<Order> getUserOrder(String paramString);

  public abstract List<Order> listAllSendOrder();

  public abstract void updateOrderByTradeNo(String paramString);
  
  public abstract List<Order> getOder(String orderNo); 
  
  /**
   * 通过用户名和产品id查询订单
   * 
   * @param pid
   * 		产品id
   * @param username
   * 		用户名
   * @return
   * 	最多10条最新数据
   */
  public List<Order> queryOrderByPidUser(long pid, String username);
  
  public Order getOrderstatus(String orderNo);
  
  //租用订单
  public abstract List<Order> listOrder2(String paramString1, ProductType paramProductType, String paramString2, Calendar paramCalendar, String paramString3, DealStatus paramDealStatus, Integer paramInteger1, Integer paramInteger2, String paramString4,String transactionType,String dealStatusStrbd,String name);

  public abstract int listOrderCount2(String paramString1, ProductType paramProductType, String paramString2, Calendar paramCalendar, String paramString3, DealStatus paramDealStatus,String transactionType,String dealStatusStrbd,String name);

  //盒子官网租用订单页面查询
  public abstract List<Order> getUserRentOrder(String paramString);
  
  public List<Order> queryBdcood();
  
  public List<Order> getOrderList(String deal,String trandtype);
  
  public abstract Order getOrd(String tranOrder);
  
  /**
   * 获取使用银联预授权租用还未结束的订单
   * @return
   */
  public List<Order> getUnionPayRentOrders();
  
  /**
   * 统计订单某段时间内的销售数据
   * @param startTime
   * @param endTime
   * @return
   */
  public int statisticOrder(Calendar startTime, Calendar endTime, String transactionType);

}
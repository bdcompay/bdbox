package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.Product;


public interface ProductDao extends IDao<Product, Long>{
	/**
	 * 多条件联合查询产品
	 * 
	 * @param product
	 * 			产品对象，通过对象的属性为查询条件，生成不同的查询语句
	 * @param startTime
	 * 			起始查询时间
	 * @param endTime
	 * 			终止查询时间
	 * @param page
	 * 			页数
	 * @param pagesize
	 * 			每页的记录数
	 * @return
	 * 		List
	 */
	public List<Product> query(Product product,Calendar startTime,Calendar endTime,int page,int pagesize);
	
	/**
	 * 查询数量
	 * 
	 * @param product
	 * 			产品对象，通过对象的属性为查询条件，生成不同的查询语句
	 * @param startTime
	 * 			起始查询时间
	 * @param endTime
	 * 			终止查询时间
	 * @return
	 */
	public int queryAmount(Product product,Calendar startTime,Calendar endTime);
	
	/**
	 * 获取商品状态为抢购的商品
	 * @return
	 */
	public Product getStockNumber();
}

/**
 * 
 */
package com.bdbox.dao;

import com.bdbox.entity.Dialrecord;

/**
 * @author zouzhiwen
 *
 */
public interface DialrecordDao extends IDao<Dialrecord, Long> {

}

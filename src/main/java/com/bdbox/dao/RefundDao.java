package com.bdbox.dao;

import com.bdbox.constant.RefundStatus;
import com.bdbox.entity.Order;
import com.bdbox.entity.Refund;

import java.util.Calendar;
import java.util.List;

public abstract interface RefundDao extends IDao<Refund, Long>
{
  public abstract Refund queryRefundUid(Long paramLong);

  public abstract List<Refund> listRefund(String paramString1, Calendar paramCalendar1, Calendar paramCalendar2, String paramString2, RefundStatus paramRefundStatus, Integer paramInteger1, Integer paramInteger2, String paramString3);

  public abstract int listRefundCount(String paramString1, Calendar paramCalendar1, Calendar paramCalendar2, String paramString2, RefundStatus paramRefundStatus);

  public abstract List<Refund> getUserRefund(String paramString);
  
  public abstract List<Refund> getrefundNo(String orderNo); 

}
package com.bdbox.dao;

import java.util.List;

import com.bdbox.entity.Address;

public interface AddressDao extends IDao<Address, Long> {
	/**
	 * 根据用户id查询收货人地址
	 * 
	 * @param userId
	 * 			用户id
	 * @return
	 */
	public List<Address> listByUserid(Long userId);
	
	/**
	 * 根据用户id查询收货人地址
	 * @param userId
	 * @return
	 */
	public Address getAddress(Long userId);
}

package com.bdbox.dao;

import com.bdbox.entity.MobCode;

public interface MobCodeDao extends IDao<MobCode, Long>{

	/**
	 * 根据手机号码返回单条记录
	 * @param phone
	 * @return
	 */
	public MobCode findCodeByMob(String phone);
	
}

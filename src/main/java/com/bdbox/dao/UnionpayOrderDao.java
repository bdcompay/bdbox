package com.bdbox.dao;

import com.bdbox.entity.UnionpayOrder;

public interface UnionpayOrderDao extends IDao<UnionpayOrder, Long>{

	public UnionpayOrder getUnionpayOrder(String orderId);
	
	public UnionpayOrder getOrderByQueryId(String queryId);
}

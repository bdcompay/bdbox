package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.constant.MsgIoType;
import com.bdbox.constant.MsgType;
import com.bdbox.entity.BoxMessage;
import com.bdsdk.constant.DataStatusType;

public interface BoxMessageDao extends IDao<BoxMessage, Long> {
	
	public List<BoxMessage> query(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxSerialNumber, Long toBoxSerialNumber, String familyMail,
			Calendar startTime, Calendar endTime,String boxName,long userId,String order, int page,
			int pageSize);
	
	public Integer queryCount(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxSerialNumber, Long toBoxSerialNumber,String familyMail,
			Calendar startTime, Calendar endTime,String boxName,long userId);

	public int queryAmount(Integer msgId, MsgType msgType, MsgIoType msgIoType,
			DataStatusType dataStatusType, String familyMob, Long fromBoxSerialNumber,
			Long toBoxSerialNumber, String familyMail,Calendar startTime, Calendar endTime,
			String boxName,long userId);
	
	public List<BoxMessage> querys(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxSerialNumber, Long toBoxSerialNumber, String familyMail,
			Calendar startTime, Calendar endTime,String boxName,long userId,String order, int page,
			int pageSize);
	
	public List<BoxMessage> querylastRevieved(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxSerialNumber, Long toBoxSerialNumber, String familyMail,
			Calendar startTime, Calendar endTime,String boxName,long userId,String order, int page,
			int pageSize);
	/**
	 * 统计用户（所有下属盒子）发送的北斗短报文数量
	 * 统计用户发送的北斗短报文数量，去除时间与发送盒子id都相同的数据
	 * 
	 * @param userid
	 * 		用户id
	 * @param startTime
	 * 		开始时间
	 * @param endTime
	 * 		结束时间
	 * @return
	 */
	public int countUserSendBdmsg(long userid,Calendar startTime,Calendar endTime);
	/**
	 * 统计用户接收的短报文数量
	 * 根据接收盒子id计算
	 * 
	 * @param userid
	 * 		用户id
	 * @param startTime
	 * 		开始时间
	 * @param endTime
	 * 		结束时间
	 * @return
	 */
	public int countUserRevBdmsg(long userid,Calendar startTime,Calendar endTime);
	/**
	 * 统计用户发送给家人的短信数量
	 * 通过MsgIoType值为IO_BOXTOFAMILY(北斗到家人)，并且家人手机非null的用户消息数量
	 *  
	 * @param userid
	 * 		用户id
	 * @param startTime
	 * 		开始时间
	 * @param endTime
	 * 		结束时间
	 * @return
	 */
	public int countUserToFamilyMsg(long userid,Calendar startTime,Calendar endTime);
	/**
	 * 统计家人回复给用户的短信数量
	 * 通过MsgIoType值为IO_FAMILYTOBOX(家人到北斗)
	 * 
	 * @param userid
	 * 		用户id
	 * @param startTime
	 * 		开始时间
	 * @param endTime
	 * 		结束时间
	 * @return
	 */
	public int countFamilyToUser(long userid,Calendar startTime,Calendar endTime);
	
	/**
	 * 可以通过企业用户名称模糊查询
	 * 
	 * @param msgId
	 * 		消息id
	 * @param msgType
	 * 		消息类型
	 * @param msgIoType
	 * 		消息方向
	 * @param dataStatusType
	 * 		数据状态类型
	 * @param familyMob
	 * 		家人手机
	 * @param fromBoxSerialNumber
	 * 		发送盒子序列号
	 * @param toBoxSerialNumber
	 * 		接收盒子序列号
	 * @param familyMail
	 * 		家人邮箱
	 * @param entUserName
	 * 		企业用户名称
	 * @param startTime
	 * 		起始查询时间
	 * @param endTime
	 * 		结束查询时间
	 * @param boxName
	 * 		盒子名称
	 * @param userId
	 * 		用户id
	 * @param order
	 * 		排序条件
	 * @param page
	 * 		起始查询位置
	 * @param pageSize
	 * 		查询数量
	 * @return
	 */
	public List<BoxMessage> querys(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxSerialNumber, Long toBoxSerialNumber, String familyMail,
			String entUserName,Calendar startTime, Calendar endTime,String boxName,long userId,
			String order, int page,int pageSize);
	
	/**
	 * 查询数量
	 * 
	 * @param msgId
	 * @param msgType
	 * @param msgIoType
	 * @param dataStatusType
	 * @param familyMob
	 * @param fromBoxSerialNumber
	 * @param toBoxSerialNumber
	 * @param familyMail
	 * @param entUserName
	 * @param startTime
	 * @param endTime
	 * @param boxName
	 * @param userId
	 * @return
	 */
	public int querysAmount(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxSerialNumber, Long toBoxSerialNumber, String familyMail,
			String entUserName,Calendar startTime, Calendar endTime,String boxName,long userId);
	
	/**
	 * 获取企业用户下发信息
	 * @param phoneNum
	 * @return
	 */
	public List<BoxMessage> getEntUserMessage(String phoneNum);
	
	/**
	 * 获取企业用户接收的信息
	 * @param phoneNum
	 * @return
	 */
	public List<BoxMessage> getReceiveMessage(Long boxId);
	
	/**
	 * 企业用户统计短信信息(分页)
	 * @param phoneNum
	 * @param keyWord
	 * @return
	 */
	public List<BoxMessage> getEntUserMessage(String phoneNum, String keyWord);

	/**
	 * 统计数量
	 * @param phoneNum
	 * @param keyWord
	 * @return
	 */
	public int querycount(String phoneNum, String keyWord);
	
	/**
	 * 查找聊天记录
	 * @param phoneNum
	 * @param boxId
	 * @return
	 */
	public List<BoxMessage> getBoxChatLog(String phoneNum, Long boxId);
}


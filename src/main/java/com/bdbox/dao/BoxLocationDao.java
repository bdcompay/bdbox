package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.BoxLocation;

public interface BoxLocationDao extends IDao<BoxLocation, Long> {

	/**
	 * 
	 * 获取盒子位置记录
	 * @param boxId	盒子id
	 * @param cardNumber 北斗卡号
	 * @param boxName	盒子名称
	 * @param userName	用户名称
	 * @param boxSerialNumber	盒子ID
	 * @param createdTime	创建时间
	 * @param order	排序
	 * @param page	页号
	 * @param pageSize	页大小
	 * @return
	 */
	public List<BoxLocation> listBoxLocations(Long boxId,String cardNumber,String boxName,String userName,String boxSerialNumber,Calendar createdTime,String order,Integer page,Integer pageSize);
	
	/**
	 * 
	 * 获取盒子位置记录数量
	 * @param boxId	盒子id
	 * @param cardNumber 北斗卡号
	 * @param boxName	盒子名称
	 * @param userName	用户名称
	 * @param boxSerialNumber	盒子ID
	 * @param createdTime	创建时间
	 * @param order	排序
	 * @param page	页号
	 * @param pageSize	页大小
	 * @return
	 */
	public Integer listBoxLocationsCount(Long boxId,String  cardNumber,String boxName,String userName,String boxSerialNumber,Calendar createdTime);
	
	/**
	 * 获取盒子位置记录（可通过时间区间）
	 * 
	 * @param boxId	盒子id
	 * @param cardNumber 北斗卡号
	 * @param boxName	盒子名称
	 * @param userName	用户名称
	 * @param boxSerialNumber	盒子ID
	 * @param startTime	开始查询时间
	 * @param endTime	结束查询时间
	 * @param order	排序
	 * @param page	页号
	 * @param pageSize	页大小
	 * @return
	 */
	public List<BoxLocation> queryBoxLocations(Long boxId,String cardNumber,String boxName,String userName,String boxSerialNumber,Calendar startTime,Calendar endTime,Long userId,String order,Integer page,Integer pageSize);
	/**
	 * 条件统计盒子位置记录数量（可通过时间区间）
	 * 
	 * @param boxId	盒子id
	 * @param cardNumber 北斗卡号
	 * @param boxName	盒子名称
	 * @param userName	用户名称
	 * @param boxSerialNumber	盒子ID
	 * @param startTime	开始查询时间
	 * @param endTime	结束查询时间
	 * @return
	 */
	public int amount(Long boxId,String cardNumber,String boxName,String userName,String boxSerialNumber,Calendar startTime,Calendar endTime,Long userId);

	/**
	 * 获取所有轨迹信息
	 * @param boxId 盒子ID
	 * @param startTime 开始查询时间
	 * @param endTime 结束查询时间
	 * @return
	 */
	public List<BoxLocation> getBoxLocation(Long boxId, Calendar startTime, Calendar endTime,Long userId);
	
	/**
	 * 获取企业用户下所有盒子的定位信息
	 * @param ids
	 * @return
	 */
	public List<BoxLocation> getAllLocation(Long id);
	
	/**
	 * 获取定位信息(调度系统)
	 * @param boxId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<BoxLocation> getEntUserLocation(Long boxId, Calendar startTime, Calendar endTime, Integer page, Integer pageSize);

	/**
	 * 分页获取数据
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<BoxLocation> getEntUserLocations(Calendar startTime, Calendar endTime, List<Long> ids);
	
	/**
	 * 统计总数
	 * @param ids
	 * @return
	 */
	public int loccount(List<Long> ids);
}

package com.bdbox.dao;

import java.util.List;

import com.bdbox.entity.QualificationCheck;

public interface QualificationCheckDao extends IDao<QualificationCheck, Long>{

	public List<QualificationCheck> query(int page, int pageSize);
	
	public int queryAcount();
	
	public QualificationCheck get(Long id);
	
	public QualificationCheck getQualificationCheck(Long invoiceId);
}

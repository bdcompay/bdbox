package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.FileDownloadLog;


/**
 * 文件下载记录DAO接口
 * 
 * @ClassName: FileDownloadLogDao 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-2-26 下午1:50:00 
 * @version V5.0 
 */
public interface FileDownloadLogDao  extends IDao<FileDownloadLog,Long> {
	/**
	 * 按条件分页查询
	 * 
	 * @param filename
	 * 		下载文件名
	 * @param status
	 * 		状态
	 * @param ip
	 * 		下载ip
	 * @param ipArea
	 * 		ip归属地
	 * @param username
	 * 		用户名称
	 * @param startTime
	 * 		开始查询下载时间
	 * @param endTime
	 * 		结束查询下载时间
	 * @param page
	 * 		页数
	 * @param pageSize
	 * 		每页显示记录数
	 * @return
	 */
	public List<FileDownloadLog> query(String filename,Boolean status,String ip,String ipArea,
			String username,Calendar startTime,Calendar endTime,int page,int pageSize);
	
	/**
	 * 按条件统计
	 * 
	 * @param filename
	 * 		下载文件名
	 * @param status
	 * 		状态
	 * @param ip
	 * 		下载ip
	 * @param ipArea
	 * 		ip归属地
	 * @param username
	 * 		用户名称
	 * @param startTime
	 * 		开始查询下载时间
	 * @param endTime
	 * 		结束查询下载时间
	 * @return
	 * 		统计数量
	 */
	public int count(String filename,Boolean status,String ip,String ipArea,
			String username,Calendar startTime,Calendar endTime);
}

package com.bdbox.dao;

import java.util.List;

import com.bdbox.constant.BoxStatusType;
import com.bdbox.constant.CardType;
import com.bdbox.entity.Box;

public interface BoxDao extends IDao<Box, Long> {
	public List<Box> query(Long userId,CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType, String snNumber,
			String boxName, String userName,String order, int page, int pageSize);

	public int queryAmount(Long userId,CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType, String snNumber,
			String boxName, String userName);
	
	public List<Box> query(Long userId,CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType, String snNumber,
			String boxName, String userName,Long entUserId,String order, int page, int pageSize);

	public int queryAmount(Long userId,CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType, String snNumber,
			String boxName, String userName,Long entUserId);
	
	public List<Box> queryBoxId(Long boxNumber);
	/**
	 * 通过企业用户id统计盒子数量
	 * @param entUserId
	 * 		企业用户ID
	 * @return
	 */
	public int countByEntUserId(long entUserId);
	/**
	 * 通过企业用户id查询北斗盒子
	 * @param entUserId
	 * 		企业用户ID
	 * @param page
	 * 		页码
	 * @param pageSize
	 * 		每页记录数
	 * @return
	 */
	public List<Box> queryByEntUserId(long entUserId,int page,int pageSize);
	
	/**
	 * 获取盒子ID
	 * @param cardNumber
	 * @return
	 */
	public Box getBox(String cardNumber);
	
	/**
	 * 获取盒子ID
	 * @param boxSerialNumber
	 * @return
	 */
	public List<Box> getBoxInfo(String boxSerialNumber);
	
	/**
	 * 获取盒子ID
	 * @param boxSerialNumber
	 * @return
	 */
	public Box getBoxId(String boxSerialNumber);
	
	/**
	 * 获取企业用户下属卡
	 * @param entUserId
	 * @return
	 */
	public List<Box> getEntUserBoxs(Long entUserId);
	
	/**
	 * 根据用户id和盒子id获取数据
	 * @param entUserId
	 * @param cardId
	 * @return
	 */
	public Box getEntUserBox(Long entUserId, String cardId);
	
	/**
	 * 搜索盒子(模糊)
	 * @param entUserId
	 * @param cardId
	 * @return
	 */
	public List<Box> searchBox(Long entUserId, String cardId);
}

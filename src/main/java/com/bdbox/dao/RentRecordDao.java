package com.bdbox.dao;

import java.util.List;

import com.bdbox.entity.RentRecord;

public interface RentRecordDao extends IDao<RentRecord, Long>{

	public List<RentRecord> getUserRentRecord(String user);
}

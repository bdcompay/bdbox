package com.bdbox.dao;

import java.util.List;

import com.bdbox.entity.Invoice;
import com.bdbox.entity.Order;

public interface InvoiceDao extends IDao<Invoice, Long>{

	/**
	 * 获取用户所有的发票信息
	 * @param user
	 * @return
	 */
	public List<Invoice> getAllInvoice(Long user);
	
	/**
	 * 根据发票id获取信息
	 * @param id
	 * @return
	 */
	public Invoice getInvoice(Long id);
	
	/**
	 * 保存发票信息
	 * @param invoice
	 * @return
	 */
	public Long saveInvoice(Invoice invoice);
	
	/**
	 * 根据用户id获取发票审核状态
	 * @param userId
	 * @return
	 */
	public Invoice getCheckStatus(Long userId);
	
	/**
	 * 根据发票id获取订单信息
	 * @param orderId
	 * @return
	 */
	public Order getOrder(Long invoice);
	
	public Invoice getAllInvoices(Long user);

}

package com.bdbox.dao;

import java.util.List;

import com.bdbox.entity.BoxPartner;

public interface BoxPartnerDao extends IDao<BoxPartner, Long> {

	public List<BoxPartner> query(Long boxId, String partnerBoxSerialNumber,
			int page, int pageSize);

	public int queryAmount(Long boxId, String partnerBoxSerialNumber);
}

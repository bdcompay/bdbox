package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.Apply;

public interface OrderDao1 extends IDao<Apply, Long>{

	/**
	 * 查询
	 * @param code	户外代码
	 * @param phone	手机号码
	 * @param createTime	申请时间
	 * @param sex	性别
	 * @param join	试用
	 * @param page	页号
	 * @param pageSize	页大小
	 * @param order	排序
	 * @return	List
	 */
	public List<Apply> listOrder(String code,String phone,Calendar createTime,String sex,Boolean join,
			Integer page,Integer pageSize,String order);
	
	/**
	 * 数量
	 * @param code	户外代码
	 * @param phone	手机号码
	 * @param createTime	申请时间
	 * @param sex	性别
	 * @param join	试用
	 * @return
	 */
	public int orderCount(String code,String phone,Calendar createTime,String sex,Boolean join);
	
	/**
	 * 根据手机号码获取订单
	 * @param phone
	 * @return
	 */
	public Apply getOrder(String phone);
	
	/**
	 * 根据用户id查询订单
	 * @param uid
	 * @return
	 */
	public Apply getOrderUid(Long uid);
	
}

package com.bdbox.dao;

import java.util.List; 

import com.bdbox.entity.Notification;

public interface NotificationDao extends IDao<Notification, Long> {
	public List<Notification> getlistNotification(String tongzhifanshi,String tongzhileixing,String type_twl,int page, int pageSize);

	public Integer getlistNotification(String tongzhifanshi,String tongzhileixing,String type_twl);
	
	public Notification getnotification(String mailtype,String type_twl);
}

package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.EntUserPushMessage;

public interface EntUserPushMessageDao extends IDao<EntUserPushMessage, Long> {
	/**
	 * 查询企业用户通讯消息推送情况
	 * @param entUserId
	 * @param boxMessageId
	 * @param count
	 * @param isSuccess
	 * @param startTime
	 * @param endTime
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<EntUserPushMessage> query(Long entUserId,Long boxMessageId,Integer count,Boolean isSuccess,
			Calendar startTime,Calendar endTime,int page,int pageSize);
	
	/**
	 * 统计企业用户通讯记录
	 * @param entUserId
	 * @param boxMessageId
	 * @param count
	 * @param isSuccess
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public int amount(Long entUserId,Long boxMessageId,Integer count,Boolean isSuccess,
			Calendar startTime,Calendar endTime);
}

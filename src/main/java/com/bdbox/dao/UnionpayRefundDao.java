package com.bdbox.dao;

import com.bdbox.entity.Returns;
import com.bdbox.entity.UnionpayRefund;

public interface UnionpayRefundDao extends IDao<UnionpayRefund, Long>{

	public UnionpayRefund getUnionpayRefund(String orderId);
	
	public UnionpayRefund getRefundByQueryId(String queryId);
	
	public UnionpayRefund getRefund(String queryId);
	
	public Returns getReturns(String orderId);
}

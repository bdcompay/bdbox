package com.bdbox.dao;

import java.util.List; 
import com.bdbox.entity.SmallType;

public interface SmallTypeDao extends IDao<SmallType, Long> {

	public List<SmallType> gettypelist(String questiontype); 
	
	public SmallType gettype(String addtype,String Englishtypename);
}

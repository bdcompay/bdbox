package com.bdbox.dao;

import java.util.Calendar;
import java.util.List; 

import com.bdbox.entity.BDCood;
import com.bdbox.entity.User;
      
public interface BDCoodDao extends IDao<BDCood, Long> {

	public List<BDCood> querybdcood(String bdnumber,String ordernumber,String userpeople,String refereespeople,String isuser,  Calendar startTime,Calendar endTime,Calendar creantTime,String refereespeoplename,String bdcoodtype, int page, int pageSize);
	
	public int queryAmount(String bdnumber,String ordernumber,String userpeople,String refereespeople,String isuser,  Calendar startTime,Calendar endTime,Calendar creantTime,String refereespeoplename,String bdcoodtype);

	public BDCood queryBdcood(String bdnumber);
	
	public int  getRefeesUserCount_wl(String bdnumber);
	
	public int  getRefeesUserCount_wl1(String bdnumber);

	public int  getRefeesUserCount_wl2(String bdnumber);

	public int  getRefeesUserCount_wl3(String bdnumber);

	public int  getRefeesUserCount_wl4(String bdnumber);
	
	public BDCood  getRefeesUser_wl5(String bdnumber);
	
	public BDCood getbdcoods_wl(String bdcood);
	
	public List<BDCood> queryBdcood();
	
	public List<BDCood> getUserByuser_twl(String user);




}

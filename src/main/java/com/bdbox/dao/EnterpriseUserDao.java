package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.EnterpriseUser;

/**
 * 企业用户DAO接口
 * 
 * @ClassName: EnterpriseUserDao 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-3-10 上午11:13:32 
 * @version V5.0 
 */
public interface EnterpriseUserDao extends IDao<EnterpriseUser, Long> {
	/**
	 * 多条件分页查询
	 * 
	 * @param name
	 * 		企业名称
	 * @param orgCode
	 * 		组织代码
	 * @param linkman
	 * 		联系人
	 * @param phone
	 * 		联系电话
	 * @param address
	 * 		地址
	 * @param startTime
	 * 		开始查询时间
	 * @param endTime
	 * 		结束查询时间
	 * @param page
	 * 		页数
	 * @param pageSize
	 * 		每页记录数
	 * @return
	 */
	public List<EnterpriseUser> queryEnterpriseUsers(String name,String orgCode,String linkman,String phone,
			String mial,String address,Calendar startTime,Calendar endTime,int page,int pageSize);
	
	/**
	 * 多条件统计数量
	 * 
	 * @param name
	 * 		企业名称
	 * @param orgCode
	 * 		组织代码
	 * @param linkman
	 * 		联系人
	 * @param phone
	 * 		联系电话
	 * @param address
	 * 		地址
	 * @param startTime
	 * 		开始时间
	 * @param endTime
	 * 		结束时间
	 * @return
	 * 		统计的数量
	 */
	public int amount(String name,String orgCode,String linkman,String phone,
			String mial,String address,Calendar startTime,Calendar endTime);
	
	/**
	 * 登录
	 * @param username
	 * @return
	 */
	public EnterpriseUser query(String username);

	public EnterpriseUser queryEnterpriseUserByPowerKey(String userPowerKey);
}

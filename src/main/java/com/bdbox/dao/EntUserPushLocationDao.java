package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.EntUserPushLocation;

public interface EntUserPushLocationDao extends IDao<EntUserPushLocation, Long>{
	/**
	 * 查询企业用户盒子位置信息推送情况
	 * @param entUserId
	 * @param boxLocationId
	 * @param count
	 * @param isSuccess
	 * @param startTime
	 * @param endTime
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<EntUserPushLocation> query(Long entUserId,Long boxLocationId,Integer count,Boolean isSuccess,
			Calendar startTime,Calendar endTime,int page,int pageSize);
	/**
	 * 统计企业用户盒子位置推送数据量
	 * @param entUserId
	 * @param boxLocationId
	 * @param count
	 * @param isSuccess
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public int amount(Long entUserId,Long boxLocationId,Integer count,Boolean isSuccess,
			Calendar startTime,Calendar endTime);
}

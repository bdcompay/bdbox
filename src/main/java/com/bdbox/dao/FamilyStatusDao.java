package com.bdbox.dao;

import java.util.List;

import com.bdbox.entity.FamilyStatus;

public interface FamilyStatusDao extends IDao<FamilyStatus, Long> {

	public List<FamilyStatus> query(Long boxId, String familyMob, int page,
			int pageSize);

	public int queryAmount(Long boxId, String familyMob);
}

package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.EntSendMsg;

/**
 * 企业用户发送消息DAO接口
 * 
 * @ClassName: EntSendMsgDao 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-3-22 下午3:22:25 
 * @version V5.0 
 */
public interface EntSendMsgDao extends IDao<EntSendMsg, Long>{
	
	/**
	 * 多条件查询
	 * 
	 * @param toBoxSerialNumber
	 * 		接收盒子序列号
	 * @param startTime
	 * 		开始查询时间
	 * @param entTime
	 * 		结束查询时间
	 * @param entUserName
	 * 		企业用户名称
	 * @return
	 */
	public List<EntSendMsg> query(String toBoxSerialNumber,Calendar startTime,Calendar endTime,String entUserName,
			int page,int pageSize);
	
	/**
	 * 多条件查询统计
	 * 
	 * @param toBoxSerialNumber
	 * @param startTime
	 * @param entTime
	 * @param entUserName
	 * @return
	 */
	public int count(String toBoxSerialNumber,Calendar startTime,Calendar endTime,String entUserName);
}

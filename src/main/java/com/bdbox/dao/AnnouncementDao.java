package com.bdbox.dao;

import java.util.List;

import com.bdbox.entity.Announcement;
 
public interface AnnouncementDao extends IDao<Announcement, Long> {
	
	public List<Announcement> getlistAnnouncementtable(int page, int pageSize);
	
	public Integer getlistAnnouncementtableCount();
	
	public List<Announcement> getfindAnnouncement();
	
	public List<Announcement> getList(String content, int page, int pageSize);
	
	public int announcementCount(String content);
}

package com.bdbox.dao;

 
 import java.util.List;

import com.bdbox.entity.RegisterInformation;
import com.bdbox.entity.User;
 
public interface RegisterInformationDao extends IDao<RegisterInformation, Long>	{
	
	public  RegisterInformation  getRealNameUrl(long userid,String type);
	
	public List<RegisterInformation> getRegisterInformation(int page, int pageSize);

	public Integer getRegisterInformationCount();
	
	public List<RegisterInformation> getRegisterInformation();


}

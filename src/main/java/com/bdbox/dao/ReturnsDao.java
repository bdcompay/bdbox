package com.bdbox.dao;

import java.util.Calendar;
import java.util.List;

import com.bdbox.constant.ReturnsStatusType;
import com.bdbox.entity.Returns;
import com.bdbox.entity.User;

public interface ReturnsDao extends IDao<Returns, Long> {
	/**
	 * 根据条件查询退货信息
	 * 
	 * @param orderId
	 * 			订单id
	 * @param returnsNum
	 * 			退款单号
	 * @param UserId
	 * 			用户id
	 * @param expressNum
	 * 			快递号
	 * @param returnsStatusType
	 * 			退货状态
	 * @param startTime
	 * 			起始查询时间(根据记录创建时间查询)
	 * @param endTime
	 * 			截止查询时间(根据记录创建时间查询)
	 * @return
	 */
	public List<Returns> listReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum, ReturnsStatusType returnsStatusType,
			Calendar startTime,Calendar endTime,int page,int pageSize,String orderstutas);
	
	/**
	 * 根据条件统计记录数量
	 * 
	 * @param orderId
	 * 			订单id
	 * @param returnsNum
	 * 			退款单号
	 * @param UserId
	 * 			用户id
	 * @param expressNum
	 * 			快递号
	 * @param returnsStatusType
	 * 			退货状态
	 * @param startTime
	 * 			起始查询时间(根据记录创建时间查询)
	 * @param endTime
	 * 			截止查询时间(根据记录创建时间查询)
	 * @return
	 * 		统计数量
	 */
	public int countReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum, ReturnsStatusType returnsStatusType,
			Calendar startTime,Calendar endTime,String orderstutas);
	
	//根据物流id查询退货信息 
	public Returns queryReturns(long expressid);
	//根据订单id查询退货信息 
	public Returns queryReturn(long orderid);
	
	//租用归还页面查询
	public List<Returns> queryRentReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum, ReturnsStatusType returnsStatusType,
			Calendar startTime,Calendar endTime,int page,int pageSize,String orderstutas);
	//租用归还页面查询
	public int countRentReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum, ReturnsStatusType returnsStatusType,
			Calendar startTime,Calendar endTime,String orderstutas);

	public List<Returns> listRentReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum, ReturnsStatusType returnsStatusType,
			Calendar startTime,Calendar endTime,int page,int pageSize,String orderstutas);
	
	public Returns getReturnsenty(long expressid);

}

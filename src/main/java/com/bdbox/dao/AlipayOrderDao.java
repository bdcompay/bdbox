package com.bdbox.dao;

import com.bdbox.entity.AlipayOrder;

public interface AlipayOrderDao extends IDao<AlipayOrder, Long>{

	public void saveOrUpdate(AlipayOrder alipayOrder);
	
	public AlipayOrder getOrderForTradeNo(String tradeNo);
}

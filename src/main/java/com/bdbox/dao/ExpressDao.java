package com.bdbox.dao;

import com.bdbox.constant.ExpressStatus;
import com.bdbox.entity.Express;
import java.util.Calendar;
import java.util.List;

public abstract interface ExpressDao extends IDao<Express, Long>
{
  public abstract List<Express> listExpress(String paramString1, String paramString2, Calendar paramCalendar1, Calendar paramCalendar2, ExpressStatus paramExpressStatus, Integer paramInteger1, Integer paramInteger2, String paramString3);

  public abstract int listExpressCount(String paramString1, String paramString2, Calendar paramCalendar1, Calendar paramCalendar2, ExpressStatus paramExpressStatus);

  public abstract Express queryExpressDto(Long paramLong);

  public abstract List<Express> listAllSendExpress();
  
  public abstract List<Express> getAllSelExpress();
  
  public abstract Express queryExpressByexpressNo(String expressNo);
  
  public abstract Express queryExpressByOrderId(Long orderId);
}
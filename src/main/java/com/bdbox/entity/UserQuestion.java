package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 官网faq页面用户提交的问题
 * @author will.yan
 * 
 */
@Entity
@Table(name = "b_userquestion")
public class UserQuestion {
	@Id
	@GeneratedValue
	private long id;
	
	/**
	 * 我的问题
	 */
	private String myquestion;
	
	/**
	 * 用户id
	 */
	private String userid;
	
	/**
	 * 提交时间
	 */
	private Calendar submitTime; 
	
	/**
	 * 是否已解决
	 */
	private Boolean issolve;
	
	/**
	 * 备注
	 */
	private String remask;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMyquestion() {
		return myquestion;
	}

	public void setMyquestion(String myquestion) {
		this.myquestion = myquestion;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public Calendar getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(Calendar submitTime) {
		this.submitTime = submitTime;
	} 

	public String getRemask() {
		return remask;
	}

	public void setRemask(String remask) {
		this.remask = remask;
	}

	public Boolean getIssolve() {
		return issolve;
	}

	public void setIssolve(Boolean issolve) {
		this.issolve = issolve;
	}
	
	
	
	
	
	
	
}

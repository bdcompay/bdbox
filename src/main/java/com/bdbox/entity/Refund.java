package com.bdbox.entity;

import com.bdbox.constant.RefundStatus;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "h_refund")
public class Refund {

	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 订单id
	 */
	@Column(name = "r_orderId")
	private Long orderId;

	/**
	 * 订单号
	 */
	@Column(name = "r_orderNo")
	private String orderNo;

	/**
	 * 退款单号
	 */
	@Column(name = "r_refundNo")
	private String refundNo;

	/**
	 * 退款金额
	 */
	@Column(name = "r_price",columnDefinition="double(20,7) default '0.00'")
	private Double price;

	/**
	 * ?? 不明白 难道是产品数量，上面price 是指产品单价？
	 */
	@Column(name = "r_count")
	private Integer count;

	/**
	 * 支付时间
	 */
	@Column(name = "r_applyTime")
	private Calendar applyTime;
	
	/**
	 * 创建时间
	 */
	@Column(name = "r_createdTime")
	private Calendar createdTime = Calendar.getInstance();

	/**
	 * 退款时间
	 */
	@Column(name = "r_refundTime")
	private Calendar refundTime;

	/**
	 * 备注说明
	 */
	@Type(type = "text")
	@Column(name = "r_explain")
	private String explain;

	/**
	 * 退款状态
	 */
	@Column(name = "r_refundStatus")
	@Enumerated(EnumType.STRING)
	private RefundStatus refundStatus;

	public Calendar getApplyTime() {
		return this.applyTime;
	}

	public Integer getCount() {
		return this.count;
	}

	public String getExplain() {
		return this.explain;
	}

	public Long getId() {
		return this.id;
	}

	public Long getOrderId() {
		return this.orderId;
	}

	public String getOrderNo() {
		return this.orderNo;
	}

	public Double getPrice() {
		return this.price;
	}

	public RefundStatus getRefundStatus() {
		return this.refundStatus;
	}

	public Calendar getRefundTime() {
		return this.refundTime;
	}

	public void setApplyTime(Calendar applyTime) {
		this.applyTime = applyTime;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public void setRefundStatus(RefundStatus refundStatus) {
		this.refundStatus = refundStatus;
	}

	public void setRefundTime(Calendar refundTime) {
		this.refundTime = refundTime;
	}

	public String getRefundNo() {
		return refundNo;
	}

	public void setRefundNo(String refundNo) {
		this.refundNo = refundNo;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}
}
package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 增票资质审核实体
 * @author hax
 *
 */
@Entity
@Table(name = "b_qualification_check")
public class QualificationCheck {

	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 营业执照
	 */
	@Column(name = "business_license")
	private String businessLicense;
	
	/**
	 * 税务登记证
	 */
	@Column(name = "tax_reg")
	private String taxReg;
	
	/**
	 * 组织机构代码证
	 */
	@Column(name = "org_code")
	private String orgCode;
	
	/**
	 * 一般纳税人资格证书
	 */
	@Column(name = "general_tax")
	private String generalTax;
	
	/**
	 * 开票资料
	 */
	@Column(name = "invoice_datum")
	private String invoiceDatum;
	
	/**
	 * 营业执照审核状态 true:通过/false:不通过，默认为false
	 */
	@Column(name = "bus_lic_status")
	private Boolean busLicStatus;
	
	/**
	 * 税务登记证审核状态
	 */
	@Column(name = "tax_reg_status")
	private Boolean taxRegStatus;
	
	/**
	 * 组织机构代码证审核状态
	 */
	@Column(name = "org_code_stauts")
	private Boolean orgCodeStauts;
	
	/**
	 * 一般纳税人资格证书审核状态
	 */
	@Column(name = "general_tax_status")
	private Boolean generalTaxStatus;
	
	/**
	 * 开票资料审核状态
	 */
	@Column(name = "invoice_datum_status")
	private Boolean invoiceDatumStatus;
	
	/**
	 * 营业执照备注，标记通过或不通过的理由
	 */
	@Column(name = "bus_lic_remark")
	private String busLicRemark;
	
	/**
	 * 税务登记证审核备注
	 */
	@Column(name = "tax_reg_remark")
	private String taxRegRemark;
	
	/**
	 * 组织机构代码证审核备注
	 */
	@Column(name = "org_code_remark")
	private String orgCodeRemark;
	
	/**
	 * 一般纳税人资格证书审核备注
	 */
	@Column(name = "general_tax_remark")
	private String generalTaxRemark;
	
	/**
	 * 开票资料审核备注
	 */
	@Column(name = "invoice_datum_remark")
	private String invoiceDatumRemark;
	
	/**
	 * 所属发票
	 */
	@Column(name = "invoice_id")
	private Long invoice;
	
	/**
	 * 生成时间
	 */
	@Column(name = "create_time")
	private Calendar createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBusinessLicense() {
		return businessLicense;
	}

	public void setBusinessLicense(String businessLicense) {
		this.businessLicense = businessLicense;
	}

	public String getTaxReg() {
		return taxReg;
	}

	public void setTaxReg(String taxReg) {
		this.taxReg = taxReg;
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	public String getGeneralTax() {
		return generalTax;
	}

	public void setGeneralTax(String generalTax) {
		this.generalTax = generalTax;
	}

	public Boolean getBusLicStatus() {
		return busLicStatus;
	}

	public void setBusLicStatus(Boolean busLicStatus) {
		this.busLicStatus = busLicStatus;
	}

	public Boolean getTaxRegStatus() {
		return taxRegStatus;
	}

	public void setTaxRegStatus(Boolean taxRegStatus) {
		this.taxRegStatus = taxRegStatus;
	}

	public Boolean getOrgCodeStauts() {
		return orgCodeStauts;
	}

	public void setOrgCodeStauts(Boolean orgCodeStauts) {
		this.orgCodeStauts = orgCodeStauts;
	}

	public Boolean getGeneralTaxStatus() {
		return generalTaxStatus;
	}

	public void setGeneralTaxStatus(Boolean generalTaxStatus) {
		this.generalTaxStatus = generalTaxStatus;
	}
	
	public String getBusLicRemark() {
		return busLicRemark;
	}

	public void setBusLicRemark(String busLicRemark) {
		this.busLicRemark = busLicRemark;
	}

	public String getTaxRegRemark() {
		return taxRegRemark;
	}

	public void setTaxRegRemark(String taxRegRemark) {
		this.taxRegRemark = taxRegRemark;
	}

	public String getOrgCodeRemark() {
		return orgCodeRemark;
	}

	public void setOrgCodeRemark(String orgCodeRemark) {
		this.orgCodeRemark = orgCodeRemark;
	}

	public String getGeneralTaxRemark() {
		return generalTaxRemark;
	}

	public void setGeneralTaxRemark(String generalTaxRemark) {
		this.generalTaxRemark = generalTaxRemark;
	}
	
	public Long getInvoice() {
		return invoice;
	}

	public void setInvoice(Long invoice) {
		this.invoice = invoice;
	}

	public Calendar getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Calendar createTime) {
		this.createTime = createTime;
	}

	public String getInvoiceDatum() {
		return invoiceDatum;
	}

	public void setInvoiceDatum(String invoiceDatum) {
		this.invoiceDatum = invoiceDatum;
	}

	public Boolean getInvoiceDatumStatus() {
		return invoiceDatumStatus;
	}

	public void setInvoiceDatumStatus(Boolean invoiceDatumStatus) {
		this.invoiceDatumStatus = invoiceDatumStatus;
	}

	public String getInvoiceDatumRemark() {
		return invoiceDatumRemark;
	}

	public void setInvoiceDatumRemark(String invoiceDatumRemark) {
		this.invoiceDatumRemark = invoiceDatumRemark;
	}
}

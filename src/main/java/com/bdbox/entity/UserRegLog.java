package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

/**
 *  用户注册记录实体
 * 
 * @ClassName: UserRegLog 
 * @author lijun.jiang@bdwise.com 
 * @date 2016-2-2 上午11:05:31 
 * @version V5.0 
 */
@Entity
@Table(name="b_log_register_user")
public class UserRegLog {
	/**
	 * id
	 */
	@Id
	@GeneratedValue
	private long id;
	/**
	 * 用户名称
	 */
	private String username;
	/**
	 * ip
	 */
	private String ip;
	/**
	 * ip所属地区
	 */
	private String ipArea;
	/**
	 * 创建时间
	 */
	@Index(name = "Index_createdTime")
	private Calendar createdtime = Calendar.getInstance();
	/**
	 * 创建用户
	 */
	@Column(name="createdUserId")
	private long userid;
	
	
	/*********************************** 分割线   ****************************************/
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getIpArea() {
		return ipArea;
	}
	public void setIpArea(String ipArea) {
		this.ipArea = ipArea;
	}
	public Calendar getCreatedtime() {
		return createdtime;
	}
	public void setCreatedtime(Calendar createdtime) {
		this.createdtime = createdtime;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	@Override
	public String toString() {
		return "UserRegLog [id=" + id + ", username=" + username + ", ip=" + ip
				+ ", ipArea=" + ipArea + ", createdtime=" + createdtime
				+ ", userid=" + userid + "]";
	}
	
}

package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 企业用户每日统计数量
 * @author jlj
 *
 */
@Entity
@Table(name="r_ent_stat_daily")
public class EntStatDaily {
	
	/**
	 * Id
	 */
	@Id
	@GeneratedValue
	private long id;
	/**
	 * 日期
	 */
	@Temporal(TemporalType.DATE)
	private Calendar date;
	/**
	 * 发送数量
	 */
	private int boxSendNumber;
	/**
	 * 企业用户
	 */
	@ManyToOne
	@JoinColumn(name="ent_id")
	private EnterpriseUser enterpriseUser;
	/**
	 * 创建时间
	 */
	private Calendar createdTime = Calendar.getInstance();
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Calendar getDate() {
		return date;
	}
	public void setDate(Calendar date) {
		this.date = date;
	}
	public int getBoxSendNumber() {
		return boxSendNumber;
	}
	public void setBoxSendNumber(int boxSendNumber) {
		this.boxSendNumber = boxSendNumber;
	}
	public EnterpriseUser getEnterpriseUser() {
		return enterpriseUser;
	}
	public void setEnterpriseUser(EnterpriseUser enterpriseUser) {
		this.enterpriseUser = enterpriseUser;
	}
	public Calendar getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}
	
	@Override
	public String toString() {
		return "EntStatDaily [id=" + id + ", date=" + date + ", boxSendNumber="
				+ boxSendNumber + ", enterpriseUser=" + enterpriseUser
				+ ", createdTime=" + createdTime + "]";
	}
}

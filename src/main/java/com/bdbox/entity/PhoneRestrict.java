package com.bdbox.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 家人号码限制记录表
 * @author hax
 *
 */
@Entity
@Table(name = "b_phoneRestrict")
public class PhoneRestrict {
	
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 手机号码
	 */
	@Column(name = "phoneNum")
	private String phoneNum;
	
	/**
	 * 备注说明
	 */
	@Column(name = "remark")
	private String remark;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}

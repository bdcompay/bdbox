package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import com.bdsdk.constant.DataStatusType;

/**
 * 消息回执数据表
 * 
 * @author will.yan
 */
@Entity
@Table(name = "b_receipt_msg")
public class MsgReceipt {
	@Id
	@GeneratedValue
	private Long id;
	/**
	 * 消息ID
	 */
	private Integer msgId;
	/**
	 * 数据状态类型
	 */
	@Enumerated(EnumType.STRING)
	private DataStatusType dataStatusType;

	/**
	 * 状态描述
	 */
	private String statusDescription;

	/**
	 * 接受者
	 */
	@ManyToOne
	@JoinColumn(name = "toBoxId")
	private Box toBox;

	/**
	 * 创建时间
	 */
	@Index(name = "Index_createdTime")
	private Calendar createdTime = Calendar.getInstance();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return 数据状态类型
	 */
	public DataStatusType getDataStatusType() {
		return dataStatusType;
	}

	public void setDataStatusType(DataStatusType dataStatusType) {
		this.dataStatusType = dataStatusType;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}

	/**
	 * @return 消息ID
	 */
	public Integer getMsgId() {
		return msgId;
	}

	public void setMsgId(Integer msgId) {
		this.msgId = msgId;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public Box getToBox() {
		return toBox;
	}

	public void setToBox(Box toBox) {
		this.toBox = toBox;
	}

}

package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bdbox.constant.UnionpayRespCode;

/**
 * 银联撤销或退货交易退款订单记录
 * @author hax
 *
 */
@Entity
@Table(name="b_unionpay_refund")
public class UnionpayRefund {

	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 退款订单号（非商品订单号）
	 */
	@Column(name="r_order_id")
	private String orderId;
	
	/**
	 * 交易金额
	 */
	@Column(name="r_txn_amt")
	private Double txnAmt;
	
	/**
	 * 订单发送时间
	 */
	@Column(name="r_txn_time")
	private Calendar txnTime;
	
	/**
	 * 流水号
	 */
	@Column(name="r_query_id")
	private String queryId;
	
	/**
	 * 原流水号
	 */
	@Column(name="r_orig_qry_id")
	private String origQryId;
	
	/**
	 * 系统跟踪号
	 */
	@Column(name="r_trace_no")
	private String traceNo;
	
	/**
	 * 交易传输时间
	 */
	@Column(name="r_trace_time")
	private String traceTime;
	
	/**
	 * 交易应答码(交易状态)
	 */
	@Column(name="u_unionpayRespCode")
	@Enumerated(EnumType.STRING)
	private UnionpayRespCode respCode;
	
	/**
	 * 应答信息
	 */
	@Column(name="u_respMsg")
	private String respMsg;
	
	/**
	 * 清算日期
	 **/
	@Column(name="u_settleDate")
	private String settleDate;
	
	/**
	 * 清算金额
	 **/
	@Column(name="u_settleAmt")
	private String settleAmt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Double getTxnAmt() {
		return txnAmt;
	}

	public void setTxnAmt(Double txnAmt) {
		this.txnAmt = txnAmt;
	}

	public Calendar getTxnTime() {
		return txnTime;
	}

	public void setTxnTime(Calendar txnTime) {
		this.txnTime = txnTime;
	}

	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}

	public String getOrigQryId() {
		return origQryId;
	}

	public void setOrigQryId(String origQryId) {
		this.origQryId = origQryId;
	}

	public String getTraceNo() {
		return traceNo;
	}

	public void setTraceNo(String traceNo) {
		this.traceNo = traceNo;
	}

	public String getTraceTime() {
		return traceTime;
	}

	public void setTraceTime(String traceTime) {
		this.traceTime = traceTime;
	}

	public UnionpayRespCode getRespCode() {
		return respCode;
	}

	public void setRespCode(UnionpayRespCode respCode) {
		this.respCode = respCode;
	}

	public String getRespMsg() {
		return respMsg;
	}

	public void setRespMsg(String respMsg) {
		this.respMsg = respMsg;
	}

	public String getSettleDate() {
		return settleDate;
	}

	public void setSettleDate(String settleDate) {
		this.settleDate = settleDate;
	}

	public String getSettleAmt() {
		return settleAmt;
	}

	public void setSettleAmt(String settleAmt) {
		this.settleAmt = settleAmt;
	}
}

package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bdbox.constant.AilpayTradeStatus;

/**
 * 支付宝订单
 * @author hax
 *
 */
@Entity
@Table(name = "b_alipay_order")
public class AlipayOrder {

	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 订单号
	 */
	@Column(name="out_trade_no")
	private String outTradeNo;
	
	/**
	 * 商品名称
	 */
	@Column(name="subject")
	private String subject;
	
	/**
	 * 交易金额
	 */
	@Column(name="total_fee")
	private Double totalFee;
	
	/**
	 * 支付宝交易号
	 */
	@Column(name="trade_no")
	private String tradeNo;
	
	/**
	 * 卖家支付宝用户号：2088开头
	 */
	@Column(name="seller_id")
	private String sellerId;
	
	/**
	 * 卖家支付宝帐号：手机或邮箱
	 */
	@Column(name="seller_email")
	private String sellerEmail;
	
	/**
	 * 买家支付宝用户号
	 */
	@Column(name="buyer_id")
	private String buyerId;
	
	/**
	 * 买家支付宝帐号
	 */
	@Column(name="buyer_email")
	private String buyerEmail;
	
	/**
	 * 交易创建时间
	 */
	@Column(name="gmt_create")
	private Calendar gmtCreate;
	
	/**
	 * 交易状态
	 */
	@Column(name="trade_status")
	@Enumerated(value=EnumType.STRING)
	private AilpayTradeStatus ailpayTradeStatus;
	
	/**
	 * 交易付款时间
	 */
	@Column(name="gmt_payment")
	private Calendar gmtPayment;
	
	/**
	 * 交易关闭时间
	 */
	@Column(name="gmt_close")
	private Calendar gmtClose;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOutTradeNo() {
		return outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Double getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(Double totalFee) {
		this.totalFee = totalFee;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getSellerId() {
		return sellerId;
	}

	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}

	public String getSellerEmail() {
		return sellerEmail;
	}

	public void setSellerEmail(String sellerEmail) {
		this.sellerEmail = sellerEmail;
	}

	public String getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

	public String getBuyerEmail() {
		return buyerEmail;
	}

	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}

	public Calendar getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Calendar gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public AilpayTradeStatus getAilpayTradeStatus() {
		return ailpayTradeStatus;
	}

	public void setAilpayTradeStatus(AilpayTradeStatus ailpayTradeStatus) {
		this.ailpayTradeStatus = ailpayTradeStatus;
	}

	public Calendar getGmtPayment() {
		return gmtPayment;
	}

	public void setGmtPayment(Calendar gmtPayment) {
		this.gmtPayment = gmtPayment;
	}

	public Calendar getGmtClose() {
		return gmtClose;
	}

	public void setGmtClose(Calendar gmtClose) {
		this.gmtClose = gmtClose;
	}
}

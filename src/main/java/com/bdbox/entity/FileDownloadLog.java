package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

/**
 * 文件下载记录实体
 * 
 * @ClassName: FileDownloadLog 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-2-26 上午11:26:09 
 * @version V5.0 
 */
@Entity
@Table(name="r_log_download")
public class FileDownloadLog {
	/**
	 * id
	 */
	@Id
	@GeneratedValue
	private long id;
	/**
	 * 文件名
	 */
	private String filename;
	/**
	 * 下载时间
	 */
	@Index(name = "Index_downloadtime")
	private Calendar downloadTime = Calendar.getInstance();
	/**
	 * 状态
	 */
	private boolean status;
	/**
	 * 耗时
	 */
	private long timeConsumig;
	/**
	 * 下载ip
	 */
	private String ip;
	/**
	 * ip归属地
	 */
	private String ipArea;
	/**
	 * 创建时间
	 */
	@Index(name = "Index_createdTime")
	private Calendar createdtime = Calendar.getInstance();
	/**
	 * 用户
	 */
	private String username;
	
	/******************************************************************************/
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public Calendar getDownloadTime() {
		return downloadTime;
	}
	public void setDownloadTime(Calendar downloadTime) {
		this.downloadTime = downloadTime;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public long getTimeConsumig() {
		return timeConsumig;
	}
	public void setTimeConsumig(long timeConsumig) {
		this.timeConsumig = timeConsumig;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getIpArea() {
		return ipArea;
	}
	public void setIpArea(String ipArea) {
		this.ipArea = ipArea;
	}
	public Calendar getCreatedtime() {
		return createdtime;
	}
	public void setCreatedtime(Calendar createdtime) {
		this.createdtime = createdtime;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}

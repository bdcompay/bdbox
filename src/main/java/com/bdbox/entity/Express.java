package com.bdbox.entity;


import javax.persistence.*;

import com.bdbox.constant.ExpressStatus;

import java.util.Calendar;

/**
 * 物流
 * 
 * @author WF
 *
 */
@Entity
@Table(name="h_express")
public class Express {
	@Id
	@GeneratedValue
	private Long id;
	/**
	 * 订单id
	 */
	@Column(name="e_orderId")
	private Long orderId;
	/**
	 * 快递公司
	 */
	@Column(name="e_expressName")
	private String expressName;
	/**
	 * 快递号码
	 */
	@Column(name="e_expressNumber")
	private String expressNumber;
	/**
	 * 物流状态
	 */
	@Column(name="e_expressStatus")
	@Enumerated(value=EnumType.STRING)
	private ExpressStatus expressStatus;
	/**
	 * 物流信息
	 */
	@Column(name="e_expressMessage",length=8000)
	private String expressMessage;
	/**
	 * 发送时间
	 */
	@Column(name="e_sendTime")
	private Calendar sendTime;
	/**
	 * 完成时间
	 */
	@Column(name="e_completionTime")
	private Calendar completionTime;
	
	/**
	 * 电子运单url
	 */
	@Column(name="e_waybillUrl")
	private String waybillUrl;

	public Calendar getCompletionTime() {
		return completionTime;
	}

	public String getExpressMessage() {
		return expressMessage;
	}

	public String getExpressName() {
		return expressName;
	}

	public String getExpressNumber() {
		return expressNumber;
	}

	public ExpressStatus getExpressStatus() {
		return expressStatus;
	}

	public Long getId() {
		return id;
	}

	public Long getOrderId() {
		return orderId;
	}

	public Calendar getSendTime() {
		return sendTime;
	}

	public void setCompletionTime(Calendar completionTime) {
		this.completionTime = completionTime;
	}

	public void setExpressMessage(String expressMessage) {
		this.expressMessage = expressMessage;
	}

	public void setExpressName(String expressName) {
		this.expressName = expressName;
	}

	public void setExpressNumber(String expressNumber) {
		this.expressNumber = expressNumber;
	}

	public void setExpressStatus(ExpressStatus expressStatus) {
		this.expressStatus = expressStatus;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public void setSendTime(Calendar sendTime) {
		this.sendTime = sendTime;
	}

	public String getWaybillUrl() {
		return waybillUrl;
	}

	public void setWaybillUrl(String waybillUrl) {
		this.waybillUrl = waybillUrl;
	}
}

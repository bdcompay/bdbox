package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bdbox.constant.MailType;

/**
 * 客户通知短信记录
 * @author hax
 *
 */
@Entity
@Table(name = "b_message_record")
public class MessageRecord {

	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 短信内容
	 */
	private String msgContent;
	
	/**
	 * 接收手机号码
	 */
	private String toMob;
	
	/**
	 * 通知类型
	 */
 	@Enumerated(value=EnumType.STRING)
	private MailType mailType;
 	
 	/**
 	 * 创建时间
 	 */
 	private Calendar createdTime = Calendar.getInstance();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMsgContent() {
		return msgContent;
	}

	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}

	public String getToMob() {
		return toMob;
	}

	public void setToMob(String toMob) {
		this.toMob = toMob;
	}

	public MailType getMailType() {
		return mailType;
	}

	public void setMailType(MailType mailType) {
		this.mailType = mailType;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}
}

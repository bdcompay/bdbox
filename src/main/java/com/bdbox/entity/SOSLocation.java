package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.bdbox.constant.MsgType;

/**
 * 记录北斗卡SOS最新位置信息
 * @author hax
 *
 */
@Entity
@Table(name = "b_sos_location")
public class SOSLocation {

	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 对应的盒子id
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "boxId")
	private Box box;
	
	/**
	 * 经度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double longitude;
	
	/**
	 * 纬度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double latitude;
	
	/**
	 * 高程
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double altitude;
	
	/**
	 * 速度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double speed;
	
	/**
	 * 方向
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double direction;
	
	/**
	 * 位置信息来源
	 */
	@Enumerated(EnumType.STRING)
	private MsgType locationSource;
	
	/**
	 * 定位时间
	 */
	private Calendar positioningTime;
	
	/**
	 * 创建时间
	 */
	private Calendar createdTime = Calendar.getInstance();
	
	/**
	 * 更新时间
	 */
	private Calendar updateTime;
	
	/**
	 * 是否紧急定位信息
	 */
	private Boolean isSOSLocation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Box getBox() {
		return box;
	}

	public void setBox(Box box) {
		this.box = box;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Double getDirection() {
		return direction;
	}

	public void setDirection(Double direction) {
		this.direction = direction;
	}

	public MsgType getLocationSource() {
		return locationSource;
	}

	public void setLocationSource(MsgType locationSource) {
		this.locationSource = locationSource;
	}

	public Calendar getPositioningTime() {
		return positioningTime;
	}

	public void setPositioningTime(Calendar positioningTime) {
		this.positioningTime = positioningTime;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}

	public Calendar getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Calendar updateTime) {
		this.updateTime = updateTime;
	}

	public Boolean getIsSOSLocation() {
		return isSOSLocation;
	}

	public void setIsSOSLocation(Boolean isSOSLocation) {
		this.isSOSLocation = isSOSLocation;
	}
}

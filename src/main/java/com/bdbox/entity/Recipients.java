package com.bdbox.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 收件人信息
 * @author hax
 *
 */
@Entity
@Table(name = "b_recipients")
public class Recipients {

	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 收件公司
	 */
	@Column(name = "r_company")
	private String company;
	
	/**
	 * 联系人
	 */
	@Column(name = "r_contact")
	private String contact;
	
	/**
	 * 联系电话-固话
	 */
	@Column(name = "r_tel")
	private String tel;
	
	/**
	 * 省份
	 */
	@Column(name = "r_province")
	private String province;
	
	/**
	 * 城市
	 */
	@Column(name = "r_city")
	private String city;
	
	/**
	 * 所在县区
	 */
	@Column(name = "r_county")
	private String county;
	
	/**
	 * 详细地址
	 */
	@Column(name = "r_address")
	private String address;
	
	/**
	 * 邮编
	 */
	@Column(name = "r_shipperCode")
	private String shipperCode;
	
	/**
	 * 手机号码
	 */
	@Column(name = "r_moblie")
	private String mobile;
	
	/**
	 * 是否默认信息：1：是；0：否。
	 */
	@Column(name = "r_isDefault")
	private int isDefault = 0;
	
	/**
	 * 区号
	 */
	@Column(name = "r_areaCode")
	private String areaCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getShipperCode() {
		return shipperCode;
	}

	public void setShipperCode(String shipperCode) {
		this.shipperCode = shipperCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(int isDefault) {
		this.isDefault = isDefault;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
}

package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.bdbox.constant.MsgType;

/**
 * 盒子的位置信息数据表
 * 
 * @author will.yan
 */
@Entity
@Table(name = "b_location_box")
public class BoxLocation {
	@Id
	@GeneratedValue
	private long id;
	/**
	 * 对应的盒子
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "boxId")
	private Box box;
	/**
	 * 对应的用户
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="userId")
	private User user;
	/**
	 * 经度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double longitude;
	/**
	 * 纬度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double latitude;
	/**
	 * 高程
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double altitude;
	/**
	 * 速度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double speed;
	/**
	 * 方向
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double direction;
	/**
	 * 位置信息来源
	 */
	@Enumerated(EnumType.STRING)
	private MsgType locationSource;
	/**
	 * 定位时间
	 */
	private Calendar positioningTime;
	/**
	 * 创建时间
	 */
	private Calendar createdTime = Calendar.getInstance();
	/**
	 * 紧急定位
	 */
	private Boolean emergencyLocation;
	

	/**
	 * @return 高度
	 */
	public Double getAltitude() {
		return altitude;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	/**
	 * @return 方向
	 */
	public Double getDirection() {
		return direction;
	}

	public long getId() {
		return id;
	}

	/**
	 * @return 纬度
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * @return 经度
	 */
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * @return 速度
	 */
	public Double getSpeed() {
		return speed;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public Box getBox() {
		return box;
	}

	public void setBox(Box box) {
		this.box = box;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}

	public void setDirection(Double direction) {
		this.direction = direction;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public MsgType getLocationSource() {
		return locationSource;
	}

	public void setLocationSource(MsgType locationSource) {
		this.locationSource = locationSource;
	}

	public Calendar getPositioningTime() {
		return positioningTime;
	}

	public void setPositioningTime(Calendar positioningTime) {
		this.positioningTime = positioningTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getEmergencyLocation() {
		return emergencyLocation;
	}

	public void setEmergencyLocation(Boolean emergencyLocation) {
		this.emergencyLocation = emergencyLocation;
	}
	
	
	
	
}

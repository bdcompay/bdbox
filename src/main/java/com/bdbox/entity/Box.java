package com.bdbox.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import com.bdbox.constant.BoxStatusType;
import com.bdbox.constant.BoxType;
import com.bdbox.constant.CardType;

/**
 * 盒子实体
 * 
 */
@Entity
@Table(name = "b_box")
public class Box {
	@Id
	@GeneratedValue
	private long id;

	/**
	 * 对应的用户
	 */
	@ManyToOne
	@JoinColumn(name = "userId")
	private User user;
	/**
	 * 对应的企业用户
	 */
	@ManyToOne
	@JoinColumn(name = "entUserId")
	private EnterpriseUser entUser;
	/**
	 * 北斗卡号码
	 * 
	 */
	@Index(name = "Index_cardNumber")
	private String cardNumber;
	/**
	 * 北斗卡的类型
	 * 
	 */
	@Enumerated(EnumType.STRING)
	private CardType cardType;
	/**
	 * 频度
	 * 
	 */
	private Integer freq = 60;
	/**
	 * 盒子SN码
	 */
	private String snNumber;
	/**
	 * 盒子序列号
	 */
	private String boxSerialNumber;

	/**
	 * 盒子名称
	 */
	private String name;
	/**
	 * 真实姓名
	 */
	private String realName;

	/**
	 * 身份证号码
	 */
	private String idNumber;
	
	/**
	 * 蓝牙密码
	 */
	private String bluetoothCode;
	/**
	 * 最后一次SOS时间 
	 */
	private Calendar lastSosCallTime;
	/**
	 * SOS电话通知应答时间
	 */
	private Calendar lastSosCallResponseTime;
	/**
	 * sos语音最新时间
	 */
	private Calendar sosLastCallTime;
	/**
	 * SOS频度 一个小时只能两次
	 */
	private Integer sosCallFreq;
	/**
	 * 是否可以SOS语音通知
	 */
	private boolean isOpenSosCall=true;
	/**
	 * 账户余额
	 */
	private Double balance = (double) 0;
	/**
	 * 最新的经度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double longitude;
	/**
	 * 最新的纬度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double latitude;
	/**
	 * 最新的高度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double altitude;
	/**
	 * 最新的速度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double speed;
	/**
	 * 最新的方向
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double direction;

	/**
	 * 最后一次定位的时间
	 */
	private Calendar lastLocationTime = Calendar.getInstance();

	/**
	 * 创建时间
	 */
	private Calendar createdTime = Calendar.getInstance();

	/**
	 * 更新时间
	 */
	private Calendar updateTime = Calendar.getInstance();

	/**
	 * 盒子状态类型，默认为未激活
	 */
	@Enumerated(EnumType.STRING)
	private BoxStatusType boxStatusType = BoxStatusType.NOTACTIVATED;
	/**
	 * 盒子类型
	 */
	@Enumerated(EnumType.STRING)
	private BoxType boxType;
	/**
	 * 蓝牙硬件地址
	 */
	private String BlueMacAdress;
	
	
	/*********************************************************************************/
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="box",fetch=FetchType.LAZY)
	private List<BoxPartner> boxPartners;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="box",fetch=FetchType.LAZY)
	private List<BoxFamily> boxFamilys;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="box",fetch=FetchType.LAZY)
	private List<BoxLocation> boxLocations;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="box",fetch=FetchType.LAZY)
	private List<FamilyStatus> familyStatus;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="toBox",fetch=FetchType.LAZY)
	private List<MsgReceipt> msgReceipts;
	
	
	public List<BoxPartner> getBoxPartners() {
		return boxPartners;
	}

	public void setBoxPartners(List<BoxPartner> boxPartners) {
		this.boxPartners = boxPartners;
	}

	public List<BoxFamily> getBoxFamilys() {
		return boxFamilys;
	}

	public void setBoxFamilys(List<BoxFamily> boxFamilys) {
		this.boxFamilys = boxFamilys;
	}

	public List<BoxLocation> getBoxLocations() {
		return boxLocations;
	}

	public void setBoxLocations(List<BoxLocation> boxLocations) {
		this.boxLocations = boxLocations;
	}

	public List<FamilyStatus> getFamilyStatus() {
		return familyStatus;
	}

	public void setFamilyStatus(List<FamilyStatus> familyStatus) {
		this.familyStatus = familyStatus;
	}

	public List<MsgReceipt> getMsgReceipts() {
		return msgReceipts;
	}

	public void setMsgReceipts(List<MsgReceipt> msgReceipts) {
		this.msgReceipts = msgReceipts;
	}

	/*********************************************************************************/
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return 用户真实姓名
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return 用户余额
	 */
	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	/**
	 * 用户创建时间
	 * 
	 * @return
	 */
	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}

	public Calendar getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Calendar updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return 用户当前登录的北斗卡号
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return 用户的身份证号码
	 */
	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public CardType getCardType() {
		return cardType;
	}

	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	public Integer getFreq() {
		return freq;
	}

	public void setFreq(Integer freq) {
		this.freq = freq;
	}

	public String getSnNumber() {
		return snNumber;
	}

	public void setSnNumber(String snNumber) {
		this.snNumber = snNumber;
	}

	public String getBoxSerialNumber() {
		return boxSerialNumber;
	}

	public void setBoxSerialNumber(String boxSerialNumber) {
		this.boxSerialNumber = boxSerialNumber;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Double getDirection() {
		return direction;
	}

	public void setDirection(Double direction) {
		this.direction = direction;
	}

	public Calendar getLastLocationTime() {
		return lastLocationTime;
	}

	public void setLastLocationTime(Calendar lastLocationTime) {
		this.lastLocationTime = lastLocationTime;
	}

	public BoxStatusType getBoxStatusType() {
		return boxStatusType;
	}

	public void setBoxStatusType(BoxStatusType boxStatusType) {
		this.boxStatusType = boxStatusType;
	}

	public String getBluetoothCode() {
		return bluetoothCode;
	}

	public void setBluetoothCode(String bluetoothCode) {
		this.bluetoothCode = bluetoothCode;
	}

	public EnterpriseUser getEntUser() {
		return entUser;
	}

	public void setEntUser(EnterpriseUser entUser) {
		this.entUser = entUser;
	}

	public boolean isOpenSosCall() {
		return isOpenSosCall;
	}

	public void setOpenSosCall(boolean isOpenSosCall) {
		this.isOpenSosCall = isOpenSosCall;
	}

	public Calendar getLastSosCallTime() {
		return lastSosCallTime;
	}

	public void setLastSosCallTime(Calendar lastSosCallTime) {
		this.lastSosCallTime = lastSosCallTime;
	}

	public Calendar getLastSosCallResponseTime() {
		return lastSosCallResponseTime;
	}

	public void setLastSosCallResponseTime(Calendar lastSosCallResponseTime) {
		this.lastSosCallResponseTime = lastSosCallResponseTime;
	}

	public Calendar getSosLastCallTime() {
		return sosLastCallTime;
	}

	public void setSosLastCallTime(Calendar sosLastCallTime) {
		this.sosLastCallTime = sosLastCallTime;
	}

	public Integer getSosCallFreq() {
		return sosCallFreq;
	}

	public void setSosCallFreq(Integer sosCallFreq) {
		this.sosCallFreq = sosCallFreq;
	}

	public BoxType getBoxType() {
		return boxType;
	}

	public void setBoxType(BoxType boxType) {
		this.boxType = boxType;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getBlueMacAdress() {
		return BlueMacAdress;
	}

	public void setBlueMacAdress(String blueMacAdress) {
		BlueMacAdress = blueMacAdress;
	}
	
	
}
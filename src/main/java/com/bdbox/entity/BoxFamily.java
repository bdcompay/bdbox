package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 盒子对应的家人列表
 * 
 * @author will.yan
 */
@Entity
@Table(name = "b_box_family")
public class BoxFamily {
	@Id
	@GeneratedValue
	private long id;
	/**
	 * 盒子ID
	 */
	@ManyToOne
	@JoinColumn(name = "boxId")
	private Box box;

	/**
	 * 家人备注名
	 */
	private String familyName;

	/**
	 * 家人手机
	 */
	private String familyMob;

	/**
	 * 家人邮箱
	 */
	private String familyMail;

	/**
	 * 创建时间
	 */
	private Calendar createdTime = Calendar.getInstance();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}

	public Box getBox() {
		return box;
	}

	public void setBox(Box box) {
		this.box = box;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getFamilyMob() {
		return familyMob;
	}

	public void setFamilyMob(String familyMob) {
		this.familyMob = familyMob;
	}

	public String getFamilyMail() {
		return familyMail;
	}

	public void setFamilyMail(String familyMail) {
		this.familyMail = familyMail;
	}

}

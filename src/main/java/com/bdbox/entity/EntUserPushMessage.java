package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 给企业用户推送消息记录
 * @author dezhi.zhang
 * @createTime 2017/01/19
 *
 */
@Entity
@Table(name="b_entUser_pushMessage")
public class EntUserPushMessage {
	/**
	 * 主键ID
	 */
	@Id@GeneratedValue
	private Long id;
	/**
	 * 需要接受消息的企业用户
	 */
	@ManyToOne
	@JoinColumn(name="entUserId")
	private EnterpriseUser enterpriseUser;
	/**
	 * 盒子通讯消息
	 */
	@ManyToOne
	@JoinColumn(name="boxMessageId")
	private BoxMessage boxMessage;
	/**
	 * 推送的次数
	 */
	private Integer count=0;
	/**
	 * 推送结果
	 */
	private Boolean isSuccess=false;
	/**
	 * 错误记录
	 */
	@Column(length=4096)
	private String error;
	/**
	 * 消息推送的时间
	 */
	private Calendar createTime=Calendar.getInstance();
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public EnterpriseUser getEnterpriseUser() {
		return enterpriseUser;
	}
	public void setEnterpriseUser(EnterpriseUser enterpriseUser) {
		this.enterpriseUser = enterpriseUser;
	}
	public BoxMessage getBoxMessage() {
		return boxMessage;
	}
	public void setBoxMessage(BoxMessage boxMessage) {
		this.boxMessage = boxMessage;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Boolean getIsSuccess() {
		return isSuccess;
	}
	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public Calendar getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Calendar createTime) {
		this.createTime = createTime;
	}
	
}

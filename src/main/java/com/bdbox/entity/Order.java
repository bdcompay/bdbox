package com.bdbox.entity;

import com.bdbox.constant.DealStatus;
import com.bdbox.constant.OrderType;
import com.bdbox.constant.PayType;
import com.bdbox.constant.ProductType;
import com.bdsdk.util.CommonMethod;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "h_order")
public class Order {

	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 归属用户
	 */
	@Column(name = "o_user")
	private String user;

	/**
	 * 购买商品id
	 */
	@Column(name = "o_productid")
	private long productId;

	/**
	 * 姓名
	 */
	@Column(name = "o_name")
	private String name;

	/**
	 * 地址
	 */
	@Column(name = "o_site")
	private String site;

	/**
	 * 版本
	 */
	@Column(name = "o_versions")
	private String versions;

	/**
	 * 说明
	 */
	@Column(name = "o_explain")
	private String explain;

	/**
	 * 订单生成时间
	 */
	@Column(name = "o_createdTime")
	private Calendar createdTime;
	
	/**
	 * 订单有效起始时间
	 */
	@Column(name = "o_validBeginTime")
	private Calendar validBeginTime;

	/**
	 * 付款时间
	 */
	@Column(name = "o_buyTime")
	private Calendar buyTime;

	/**
	 * 交易完成时间
	 */
	@Column(name = "o_completeTime")
	private Calendar completeTime;

	/**
	 * 联系方式
	 */
	@Column(name = "o_phone")
	private Long phone;

	/**
	 * 颜色
	 */
	@Column(name = "o_color")
	private String color;

	/**
	 * 支付宝交易号
	 */
	@Column(name = "o_trade_no")
	private String trade_no;

	/**
	 * 订单编号
	 */
	@Column(name = "o_orderNo")
	private String orderNo;

	/**
	 * 邮箱
	 */
	@Column(name = "o_email")
	private String email;

	/**
	 * 产品名称
	 */
	@Column(name = "o_productName")
	private String productName;

	/**
	 * 产品url
	 */
	@Column(name = "o_productUrl")
	private String productUrl;

	/**
	 * 商品单价
	 */
	@Column(name = "o_price", columnDefinition="double(20,7) default '0.00'")
	private Double price;

	/**
	 * 商品数量
	 */
	@Column(name = "o_count")
	private Integer count;
	
	/**
	 * 支付金额
	 */
	@Column(name = "o_payAmount", columnDefinition="double(20,7) default '0.00'")
	private Double payAmount;

	/**
	 * 交易状态
	 */
	@Column(name = "o_dealStatus")
	@Enumerated(EnumType.STRING)
	private DealStatus dealStatus;

	/**
	 * 产品类型
	 */
	@Column(name = "o_productType")
	@Enumerated(EnumType.STRING)
	private ProductType productType;

	/**
	 * 小图片路径
	 */
	private String minImg;

	/**
	 * 二维码有效时间
	 */
	private String qrcodeTime;

	/**
	 * 二维码
	 */
	@Type(type = "text")
	private String QRCode;

	/**
	 * 取消状态
	 */
	@Column(name = "o_cancelstate")
	private String cancelstate;
	
	/**
	 * 交易类型(租用和购买)
	 */
	@Column(name = "o_transactionType")
	private String transactionType;
	
	/**
	 * 押金
	 */
	@Column(name = "o_antecedent")
	String antecedent;
	
	/**
	 * 租金
	 */
	@Column(name = "o_rental")
	String rental;
	
	/**
	 * 总押金
	 */
	@Column(name = "o_sumantecedent")
	String sumantecedent;
	
	/**
	 * 总租金
	 */
	@Column(name = "o_sumrental")
	String sumrental; 
	
	/**
	 * 身份证号码
	 */
	@Column(name = "o_idcardnumber")
	String idcardnumber;
	
	/**
	 * 买家留言
	 */
	@Column(name = "o_leavemessage")
	String leavemessage; 
	
	/**
	 * 租用天数
	 */
	@Column(name = "o_numberdate")
	String numberdate; 
	
	/**
	 * 租用开始时间
	 */
	@Column(name = "o_beginTime")
	private Calendar beginTime;
	
	/**
	 * 租用结束时间
	 */
	@Column(name = "o_endTime")
	private Calendar endTime;
	
	/**
	 * 对应发票id
	 * 值为0时表示不需要开具发票
	 */
	@Column(name = "invoice_id")
	private Long invoice;
	
	/**
	 * 北斗码
	 * 
	 */
	@Column(name = "o_bdcood")
	private String bdcood;
	
	/**
	 * 是否是实名制订单
	 * 
	 */
	@Column(name = "o_renlnameorder")
	private String renlnameorder;//(1为是，2为不是)
	
	/**
	 * 每日租用单价
	 * 
	 */
	@Column(name = "o_unitcost")
	private String unitcost;//每天租用单价（实名制30，非实名制50）
	
	/**
	 * 订单类型
	 */
	@Column(name = "o_orderType")
	@Enumerated(EnumType.STRING)
	private OrderType orderType;
	
	/**
	 * 支付方式：微信，支付宝
	 */
	@Column(name = "o_payType")
	@Enumerated(EnumType.STRING)
	private PayType payType;
	
	/**
	 * 区号
	 */
	@Column(name = "a_areaCode")
	private String areaCode;
	
	/**
	 * 保单id
	 */
	@Column(name = "insOrdInfoId")
	private Long insOrdInfoId;

	public Calendar getBuyTime() {
		return this.buyTime;
	}

	public String getColor() {
		return this.color;
	}

	public Calendar getCompleteTime() {
		return this.completeTime;
	}

	public Integer getCount() {
		return this.count;
	}

	public Calendar getCreatedTime() {
		return this.createdTime;
	}

	public DealStatus getDealStatus() {
		return this.dealStatus;
	}

	public String getEmail() {
		return this.email;
	}

	public String getExplain() {
		return this.explain;
	}

	public Long getId() {
		return this.id;
	}

	public String getMinImg() {
		return this.minImg;
	}

	public String getName() {
		return this.name;
	}

	public String getOrderNo() {
		return this.orderNo;
	}

	public Long getPhone() {
		return this.phone;
	}

	public Double getPrice() {
		return this.price;
	}

	public String getProductName() {
		return this.productName;
	}

	public ProductType getProductType() {
		return this.productType;
	}

	public String getProductUrl() {
		return this.productUrl;
	}

	public String getSite() {
		return this.site;
	}

	public String getTrade_no() {
		return this.trade_no;
	}

	public String getUser() {
		return this.user;
	}

	public String getVersions() {
		return this.versions;
	}

	public void setBuyTime(Calendar buyTime) {
		this.buyTime = buyTime;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setCompleteTime(Calendar completeTime) {
		this.completeTime = completeTime;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}

	public void setDealStatus(DealStatus dealStatus) {
		this.dealStatus = dealStatus;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setMinImg(String minImg) {
		this.minImg = minImg;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public void setPhone(Long phone) {
		this.phone = phone;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public void setTrade_no(String trade_no) {
		this.trade_no = trade_no;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setVersions(String versions) {
		this.versions = versions;
	}

	public String succed() {
		return "==============================================================\n\t\t用户 ["
				+ this.user
				+ "] 订单交易成功\n"
				+ "==============================================================\n"
				+ "订单编号："
				+ this.orderNo
				+ "\n"
				+ "商品类型："
				+ this.productType
				+ "\n"
				+ "购买商品："
				+ this.productName
				+ "\n"
				+ "版本："
				+ this.versions
				+ "\n"
				+ "颜色："
				+ this.color
				+ "\n"
				+ "价格："
				+ this.price
				+ "\n"
				+ "购买时间："
				+ CommonMethod.CalendarToString(this.createdTime,
						"yyyy/MM/dd HH:mm") + "\n";
	}

	public String getQrcodeTime() {
		return this.qrcodeTime;
	}

	public void setQrcodeTime(String qrcodeTime) {
		this.qrcodeTime = qrcodeTime;
	}

	public String getQRCode() {
		return this.QRCode;
	}

	public void setQRCode(String QRCode) {
		this.QRCode = QRCode;
	}

	public String getCancelstate() {
		return cancelstate;
	}

	public void setCancelstate(String cancelstate) {
		this.cancelstate = cancelstate;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getAntecedent() {
		return antecedent;
	}

	public void setAntecedent(String antecedent) {
		this.antecedent = antecedent;
	}

	public String getRental() {
		return rental;
	}

	public void setRental(String rental) {
		this.rental = rental;
	}

	public String getSumantecedent() {
		return sumantecedent;
	}

	public void setSumantecedent(String sumantecedent) {
		this.sumantecedent = sumantecedent;
	}

	public String getSumrental() {
		return sumrental;
	}

	public void setSumrental(String sumrental) {
		this.sumrental = sumrental;
	}

	public String getIdcardnumber() {
		return idcardnumber;
	}

	public void setIdcardnumber(String idcardnumber) {
		this.idcardnumber = idcardnumber;
	}

	public String getLeavemessage() {
		return leavemessage;
	}

	public void setLeavemessage(String leavemessage) {
		this.leavemessage = leavemessage;
	}

	public String getNumberdate() {
		return numberdate;
	}

	public void setNumberdate(String numberdate) {
		this.numberdate = numberdate;
	}

	public Calendar getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Calendar beginTime) {
		this.beginTime = beginTime;
	}

	public Calendar getEndTime() {
		return endTime;
	}

	public void setEndTime(Calendar endTime) {
		this.endTime = endTime;
	}

	public Calendar getValidBeginTime() {
		return validBeginTime;
	}

	public void setValidBeginTime(Calendar validBeginTime) {
		this.validBeginTime = validBeginTime;
	}

	public Double getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(Double payAmount) {
		this.payAmount = payAmount;
	}

	public Long getInvoice() {
		return invoice;
	}

	public void setInvoice(Long invoice) {
		this.invoice = invoice;
	}

	public String getBdcood() {
		return bdcood;
	}

	public void setBdcood(String bdcood) {
		this.bdcood = bdcood;
	}

	public String getRenlnameorder() {
		return renlnameorder;
	}

	public void setRenlnameorder(String renlnameorder) {
		this.renlnameorder = renlnameorder;
	}

	public String getUnitcost() {
		return unitcost;
	}

	public void setUnitcost(String unitcost) {
		this.unitcost = unitcost;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public PayType getPayType() {
		return payType;
	}

	public void setPayType(PayType payType) {
		this.payType = payType;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public Long getInsOrdInfoId() {
		return insOrdInfoId;
	}

	public void setInsOrdInfoId(Long insOrdInfoId) {
		this.insOrdInfoId = insOrdInfoId;
	}
	
}
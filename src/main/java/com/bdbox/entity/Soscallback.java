package com.bdbox.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 *  外呼通知状态通知 外呼通知挂机后给应用侧的通知消息。
 * @author zouzhiwen
 *
 */
@Entity
@Table(name="b_soscallback")
public class Soscallback {
	/**
	 * id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	/**
	 * 表示外呼通知 SellingCall
	 */
	private String action;
	/**
	 * 对应接口返回callSid参数，一路呼叫的唯一标示  32位字符串
	 */
	private String callSid;
	/**
	 * 外呼号码
	 */
	private String number;
	/**
	 * 通话状态  0正常通话 1被叫通话未应答  2外呼失败
	 */
	private String state;
	/**
	 * 通话时长 单位秒
	 */
	private String duration	;
	/**
	 * 用户私有数据。外呼通知接口参数	
	 */
	private String userData;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getCallSid() {
		return callSid;
	}
	public void setCallSid(String callSid) {
		this.callSid = callSid;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getUserData() {
		return userData;
	}
	public void setUserData(String userData) {
		this.userData = userData;
	}
	
}

package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bdbox.constant.CheckStatus;
import com.bdbox.constant.InvoiceType;
/**
 * 发票实体
 * @author hax
 *
 */
@Entity
@Table(name = "b_invoice")
public class Invoice {

	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 单位名称(发票抬头)
	 */
	@Column(name = "company_name")
	private String companyName;
	
	/**
	 * 纳税人识别码
	 */
	@Column(name = "taxpayer_id_num")
	private String taxpayerIdNum;
	
	/**
	 * 注册地址
	 */
	@Column(name = "register_addr")
	private String registerAddr;
	
	/**
	 * 注册电话
	 */
	@Column(name = "register_phone")
	private String registerPhone;
	
	/**
	 * 开户银行
	 */
	@Column(name = "bank_name")
	private String bankName;
	
	/**
	 * 银行账号
	 */
	@Column(name = "bank_account")
	private String bankAccount;
	
	/**
	 * 收票人信息
	 */
	@Column(name = "tickets_info")
	private String ticketsInfo;
	
	/**
	 * 收票人电话
	 */
	@Column(name = "tickets_phone")
	private String ticketsPhone;
	
	/**
	 * 收票人区域
	 */
	@Column(name = "tickets_addr")
	private String ticketsAddr;
	
	/**
	 * 详细地址
	 */
	@Column(name = "detail_addr")
	private String detailAddr;
	
	/**
	 * 发票类型
	 */
	@Column(name = "invoice_type")
	@Enumerated(value = EnumType.STRING)
	private InvoiceType invoiceType;
	
	/**
	 * 所属用户
	 */
	@Column(name = "user_id")
	private Long user;
	
	/**
	 * 审核状态
	 */
	@Column(name = "checkStatus")
	@Enumerated(value = EnumType.STRING)
	private CheckStatus checkStatus;
	
	/**
	 * 生成时间
	 */
	@Column(name = "create_time")
	private Calendar createTime = Calendar.getInstance();
	
	/**
	 * 是否为默认发票
	 */
	@Column(name = "moren")
	private Boolean moren=false;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getTaxpayerIdNum() {
		return taxpayerIdNum;
	}

	public void setTaxpayerIdNum(String taxpayerIdNum) {
		this.taxpayerIdNum = taxpayerIdNum;
	}

	public String getRegisterAddr() {
		return registerAddr;
	}

	public void setRegisterAddr(String registerAddr) {
		this.registerAddr = registerAddr;
	}

	public String getRegisterPhone() {
		return registerPhone;
	}

	public void setRegisterPhone(String registerPhone) {
		this.registerPhone = registerPhone;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getTicketsInfo() {
		return ticketsInfo;
	}

	public void setTicketsInfo(String ticketsInfo) {
		this.ticketsInfo = ticketsInfo;
	}

	public String getTicketsPhone() {
		return ticketsPhone;
	}

	public void setTicketsPhone(String ticketsPhone) {
		this.ticketsPhone = ticketsPhone;
	}

	public String getTicketsAddr() {
		return ticketsAddr;
	}

	public void setTicketsAddr(String ticketsAddr) {
		this.ticketsAddr = ticketsAddr;
	}

	public String getDetailAddr() {
		return detailAddr;
	}

	public void setDetailAddr(String detailAddr) {
		this.detailAddr = detailAddr;
	}

	public InvoiceType getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(InvoiceType invoiceType) {
		this.invoiceType = invoiceType;
	}

	public Long getUser() {
		return user;
	}

	public void setUser(Long user) {
		this.user = user;
	}

	public CheckStatus getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(CheckStatus checkStatus) {
		this.checkStatus = checkStatus;
	}

	public Calendar getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Calendar createTime) {
		this.createTime = createTime;
	}

	public Boolean getMoren() {
		return moren;
	}

	public void setMoren(Boolean moren) {
		this.moren = moren;
	}
	
	
}

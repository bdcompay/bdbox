package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import com.bdbox.constant.MsgIoType;
import com.bdsdk.constant.DataStatusType;

/**
 * 本张表保存所有用户发送的盒子信息
 * 
 * @author zouzhiwen
 */
@Entity
@Table(name = "b_usersend_boxmessage")
public class UserSendBoxMessage {
	@Id
	@GeneratedValue
	private long id;
	/**
	 * 消息ID
	 */
	private Long msgId;
	/**
	 * 发送用户
	 */
	@ManyToOne
	@JoinColumn(name = "fromUserId")
	private User fromUser;
	/**
	 * 接收盒子
	 */
	@Index(name = "Index_boxSerialNumber")
	private String boxSerialNumber;

	/**
	 * 数据内容
	 */
	private String content;

	/**
	 * 创建时间
	 */
	@Index(name = "Index_createdTime")
	private Calendar createdTime = Calendar.getInstance();
	
	/**
	 * 发送时间
	 */
	@Index(name = "Index_sendTime")
	private Calendar sendTime ;
	/**
	 * 是否需要收到回执
	 */
	private Boolean isReceived;
	/**
	 * 是否需要也阅读回执
	 */
	private Boolean isRead;
	/**
	 * 数据状态类型
	 */
	@Enumerated(EnumType.STRING)
	private DataStatusType dataStatusType;
	/**
	 * 企业发送用户
	 */
	@ManyToOne
	@JoinColumn(name = "fromEntUserId")
	private EnterpriseUser entUser;
	/**
	 * 消息方向
	 */
	@Enumerated(EnumType.STRING)
	private MsgIoType msgIoType;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public Long getMsgId() {
		return msgId;
	}

	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}


	public User getFromUser() {
		return fromUser;
	}

	public void setFromUser(User fromUser) {
		this.fromUser = fromUser;
	}


	public Calendar getSendTime() {
		return sendTime;
	}

	public void setSendTime(Calendar sendTime) {
		this.sendTime = sendTime;
	}

	public String getBoxSerialNumber() {
		return boxSerialNumber;
	}

	public void setBoxSerialNumber(String boxSerialNumber) {
		this.boxSerialNumber = boxSerialNumber;
	}

	public Boolean getIsReceived() {
		return isReceived;
	}

	public void setIsReceived(Boolean isReceived) {
		this.isReceived = isReceived;
	}

	public Boolean getIsRead() {
		return isRead;
	}

	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}

	public DataStatusType getDataStatusType() {
		return dataStatusType;
	}

	public void setDataStatusType(DataStatusType dataStatusType) {
		this.dataStatusType = dataStatusType;
	}

	public EnterpriseUser getEntUser() {
		return entUser;
	}

	public void setEntUser(EnterpriseUser entUser) {
		this.entUser = entUser;
	}

	public MsgIoType getMsgIoType() {
		return msgIoType;
	}

	public void setMsgIoType(MsgIoType msgIoType) {
		this.msgIoType = msgIoType;
	}
	
	

}

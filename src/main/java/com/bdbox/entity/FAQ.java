package com.bdbox.entity;

import java.util.Calendar; 

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table; 

import org.hibernate.annotations.Columns;

import com.bdbox.constant.QuestionType;

/**
 * FAQ实体
 * @author will.yan
 * 
 */
@Entity
@Table(name = "b_faq")
public class FAQ {
	@Id
	@GeneratedValue
	private long id;
	
	/**
	 * 问题
	 */
	private String question;
	
	/**
	 * 答案
	 */
	@Column(length=2000)
	private String answer;
	
	/**
	 * 创建时间
	 */
	private Calendar creatTime=Calendar.getInstance();
	
	/**
	 * 问题大类
	 */
	@Enumerated(EnumType.STRING)
	private QuestionType questionType;
	
	/**
	 * 问题小类
	 */
	private String smallTypeid;
	
	/**
	 * 问题小类名
	 */
	private String smallTypename;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Calendar getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Calendar creatTime) {
		this.creatTime = creatTime;
	}

	public QuestionType getQuestionType() {
		return questionType;
	}

	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}

	public String getSmallTypeid() {
		return smallTypeid;
	}

	public void setSmallTypeid(String smallTypeid) {
		this.smallTypeid = smallTypeid;
	}

	public String getSmallTypename() {
		return smallTypename;
	}

	public void setSmallTypename(String smallTypename) {
		this.smallTypename = smallTypename;
	}

	 

	 
	 
	 
}

package com.bdbox.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 问题小类
 * @author will.yan
 * 
 */
@Entity
@Table(name = "b_small")
public class SmallType {

	@Id
	@GeneratedValue
	private long id;
	
	/**
	 * 类型名称
	 */
	private String name;
	
	/**
	 * 小类型
	 */
	private String smallType;
	
	/**
	 * questionid
	 */
	private String questionid;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getQuestionid() {
		return questionid;
	}

	public void setQuestionid(String questionid) {
		this.questionid = questionid;
	}

	public String getSmallType() {
		return smallType;
	}

	public void setSmallType(String smallType) {
		this.smallType = smallType;
	}
	
	
	
}

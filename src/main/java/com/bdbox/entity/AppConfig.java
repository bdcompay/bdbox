package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bdbox.constant.AppType;

/**
 * app参数配置
 * 
 * @author will.yan
 */
@Entity
@Table(name = "b_appconfig")
public class AppConfig {
	@Id
	@GeneratedValue
	private long id;

	/**
	 * App类型
	 */
	@Enumerated(EnumType.STRING)
	private AppType appType;
	/**
	 * 版本号
	 */
	private Float versionCode;
	/**
	 * 版本名称
	 */
	private String versionName;
	/**
	 * 更新说明
	 */
	private String versionDetail;
	/**
	 * 下载路径
	 */
	private String downloadUrl;
	/**
	 * APP大小
	 */
	private String versionSize;
	/**
	 * 创建时间
	 */
	private Calendar createdTime = Calendar.getInstance();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public Float getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(Float versionCode) {
		this.versionCode = versionCode;
	}

	public AppType getAppType() {
		return appType;
	}

	public void setAppType(AppType appType) {
		this.appType = appType;
	}

	public String getVersionDetail() {
		return versionDetail;
	}

	public void setVersionDetail(String versionDetail) {
		this.versionDetail = versionDetail;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getVersionSize() {
		return versionSize;
	}

	public void setVersionSize(String versionSize) {
		this.versionSize = versionSize;
	}

}

package com.bdbox.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 物流公司
 * @author hax
 *
 */
@Entity
@Table(name="h_expressCompany")
public class ExpressCompany {

	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 物流公司名称
	 */
	@Column(name="e_companyName")
	private String companyName;
	
	/**
	 * 物流公司代码
	 */
	@Column(name="e_companyCode")
	private String companyCode;
	
	/**
	 * 备注
	 */
	@Column(name="e_remark")
	private String remark;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}

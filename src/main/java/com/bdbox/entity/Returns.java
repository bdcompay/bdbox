package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bdbox.constant.PayType;
import com.bdbox.constant.ReturnsStatusType;

/**
 * 退货
 * 
 * @author jlj
 *
 */
@Entity
@Table(name="h_returns")
public class Returns {
	/**
	 * id
	 */
	@Id
	@GeneratedValue
	private long id;
	/**
	 * 退货号
	 */
	@Column(name = "r_returnsNum")
	private String returnsNum;
	
	/**
	 * 订单号
	 */
	@Column(name = "r_orderNum")
	private String orderNum;
	
	/**
	 * 订单id
	 */
	@Column(name = "r_ordreId")
	private Long ordreId;
	/**
	 * 用户id
	 */
	@Column(name = "r_userId")
	private Long userId;
	/**
	 * 产品id
	 */
	@Column(name = "r_productId")
	private Long productId;
	/**
	 * 产品名称
	 */
	@Column(name = "r_productName")
	private String productName;
	/**
	 * 购买时间
	 */
	@Column(name = "r_buyTime")
	private Calendar buyTime;
	/**
	 * 数量
	 */
	@Column(name = "r_count")
	private int count;
	/**
	 * 商品单价
	 */
	@Column(name = "r_price",columnDefinition="double(20,7) default '0.00'")
	private double price;
	/**
	 * 实际退款金额
	 */
	@Column(name = "r_returnMoney",columnDefinition="double(20,7) default '0.00'")
	private Double returnMoney;
	
	/**
	 * 退货原因
	 */
	@Column(name = "r_returnsCause")
	private String returnsCause;
	/**
	 * 退货状态
	 */
	@Column(name = "r_returnsStatusType")
	@Enumerated(EnumType.STRING)
	private ReturnsStatusType returnsStatusType;
	/**
	 * 申请提交时间
	 */
	@Column(name = "r_applySubmitTime")
	private Calendar applySubmitTime;
	/**
	 * 申请审核时间
	 */
	@Column(name = "r_applyAuditTime")
	private Calendar applyAuditTime;
	/**
	 * 物流单号
	 */
	@Column(name = "r_expressNum")
	private String expressNum;
	/**
	 * 物流单填写时间
	 */
	@Column(name = "r_expressNumWriteTime")
	private Calendar expressNumWriteTime;
	/**
	 * 物流单id
	 */
	@Column(name = "r_expressId")
	private long expressId;
	
	/**
	 * 物流名称
	 */
	@Column(name = "r_expressName")
	private String expressName;
	
	/**
	 * 购买金额
	 */
	@Column(name = "r_buyMoney",columnDefinition="double(20,7) default '0.00'")
	private double buyMoney;
	/**
	 * 退货扣减金额
	 */
	@Column(name = "r_deductionMoney",columnDefinition="double(20,7) default '0.00'")
	private double deductionMoney;
	/**
	 * 扣减原因
	 */
	@Column(name = "r_deductionCause")
	private String deductionCause;
	
	/**
	 * 退款单id
	 */
	@Column(name = "r_refundId")
	private long refundId;
	/**
	 * 退货完成时间
	 */
	@Column(name = "r_finishTime")
	private Calendar finishTime;
	/**
	 * 备注说明
	 */
	@Column(name = "r_explain")
	private String explain;
	/**
	 * 退款单类型(RENT:租用,GENERAL购买)
	 */
	@Column(name = "r_returnType")
	private String returnType;
	/**
	 * 租用开始时间
	 */
	@Column(name = "r_beginTime")
	private Calendar beginTime;
	
	/**
	 * 租用结束时间
	 */
	@Column(name = "r_endTime")
	private Calendar endTime;
	
	/**
	 * 租金合计
	 */
	@Column(name = "r_rentprice",columnDefinition="double(20,7) default '0.00'")
	private Double rentprice;
	
	/**
	 * 支付方式：微信，支付宝
	 */
	@Column(name = "o_payType")
	@Enumerated(EnumType.STRING)
	private PayType payType;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getReturnsNum() {
		return returnsNum;
	}
	public void setReturnsNum(String returnsNum) {
		this.returnsNum = returnsNum;
	}
	public Long getOrdreId() {
		return ordreId;
	}
	public void setOrdreId(Long ordreId) {
		this.ordreId = ordreId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Calendar getBuyTime() {
		return buyTime;
	}
	public void setBuyTime(Calendar buyTime) {
		this.buyTime = buyTime;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getReturnsCause() {
		return returnsCause;
	}
	public void setReturnsCause(String returnsCause) {
		this.returnsCause = returnsCause;
	}
	public ReturnsStatusType getReturnsStatusType() {
		return returnsStatusType;
	}
	public void setReturnsStatusType(ReturnsStatusType returnsStatusType) {
		this.returnsStatusType = returnsStatusType;
	}
	public Calendar getApplySubmitTime() {
		return applySubmitTime;
	}
	public void setApplySubmitTime(Calendar applySubmitTime) {
		this.applySubmitTime = applySubmitTime;
	}
	public Calendar getApplyAuditTime() {
		return applyAuditTime;
	}
	public void setApplyAuditTime(Calendar applyAuditTime) {
		this.applyAuditTime = applyAuditTime;
	}
	public String getExpressNum() {
		return expressNum;
	}
	public void setExpressNum(String expressNum) {
		this.expressNum = expressNum;
	}
	public Calendar getExpressNumWriteTime() {
		return expressNumWriteTime;
	}
	public void setExpressNumWriteTime(Calendar expressNumWriteTime) {
		this.expressNumWriteTime = expressNumWriteTime;
	}
	public double getBuyMoney() {
		return buyMoney;
	}
	public void setBuyMoney(double buyMoney) {
		this.buyMoney = buyMoney;
	}
	public double getDeductionMoney() {
		return deductionMoney;
	}
	public void setDeductionMoney(double deductionMoney) {
		this.deductionMoney = deductionMoney;
	}
	public String getDeductionCause() {
		return deductionCause;
	}
	public void setDeductionCause(String deductionCause) {
		this.deductionCause = deductionCause;
	}
	public Calendar getFinishTime() {
		return finishTime;
	}
	public void setFinishTime(Calendar finishTime) {
		this.finishTime = finishTime;
	}
	public String getExplain() {
		return explain;
	}
	public void setExplain(String explain) {
		this.explain = explain;
	}
	public long getExpressId() {
		return expressId;
	}
	public void setExpressId(long expressId) {
		this.expressId = expressId;
	}
	public String getExpressName() {
		return expressName;
	}
	public void setExpressName(String expressName) {
		this.expressName = expressName;
	}
	public long getRefundId() {
		return refundId;
	}
	public void setRefundId(long refundId) {
		this.refundId = refundId;
	}
	public String getOrderNum() {
		return orderNum;
	}
	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Double getReturnMoney() {
		return returnMoney;
	}
	public void setReturnMoney(Double returnMoney) {
		this.returnMoney = returnMoney;
	}
	public String getReturnType() {
		return returnType;
	}
	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}
	public Calendar getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Calendar beginTime) {
		this.beginTime = beginTime;
	}
	public Calendar getEndTime() {
		return endTime;
	}
	public void setEndTime(Calendar endTime) {
		this.endTime = endTime;
	}
	public Double getRentprice() {
		return rentprice;
	}
	public void setRentprice(Double rentprice) {
		this.rentprice = rentprice;
	}
	public PayType getPayType() {
		return payType;
	}
	public void setPayType(PayType payType) {
		this.payType = payType;
	}
}

package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 库存不足信息登记表
 * 
 * @author will.yan
 * 
 */
@Entity
@Table(name = "b_RegisterInformation")
public class RegisterInformation {
	@Id
	@GeneratedValue
	private long id;
	/**
	 * 手机号码
	 */
	private String telephone;

	/**
	 * 邮箱
	 */
	private String mail; 
	 
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 登记时间
	 */
	private Calendar birthDate= Calendar.getInstance();
	
	/**
	 * 用户id
	 */
	private String userid;
	
	/**
	 * 登记类型(购买和租用)
	 */
	private String type;
	

	/**
	 * 是否已通知客户
	 */
	private Boolean record=false;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Calendar getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Calendar birthDate) {
		this.birthDate = birthDate;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getRecord() {
		return record;
	}

	public void setRecord(Boolean record) {
		this.record = record;
	}

	 

	 
	
	
	
}

package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="h_apply")
public class Apply {
	@Id
	@GeneratedValue()
	private long id;
	/**
	 * 用户
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	private User user;
	/**
	 * 姓名
	 */
	@Column(name="o_name")
	private String name;
	/**
	 * 手机号码
	 */
	@Column(name="o_phone")
	private String phone;
	/**
	 * 邮箱
	 */
	@Column(name="o_email")
	private String email;
	/**
	 * 是否参与面试试用：1=参加，0=不参加
	 */
	@Column(name="o_join")
	private boolean join;
	/**
	 * 性别：1=男，0=女
	 */
	@Column(name="o_sex")
	private String sex;
	/**
	 * 省份
	 */
	@Column(name="o_province")
	private String province;
	/**
	 * 市区
	 */
	@Column(name="o_city")
	private String city;
	/**
	 * 户外代号
	 */
	@Column(name="o_code")
	private String code;
	/**
	 * 简介
	 */
	@Column(name="o_intro")
	private String intro;
	/**
	 * 其它说明
	 */
	@Column(name="o_explain")
	private String explain;
	/**
	 * 申请时间
	 */
	@Column(name="o_createTime")
	private Calendar createTime;
	public String getCity() {
		return city;
	}
	public String getCode() {
		return code;
	}
	public Calendar getCreateTime() {
		return createTime;
	}
	public String getEmail() {
		return email;
	}
	public String getExplain() {
		return explain;
	}
	public long getId() {
		return id;
	}
	public String getIntro() {
		return intro;
	}
	public String getName() {
		return name;
	}
	public String getPhone() {
		return phone;
	}
	public String getProvince() {
		return province;
	}
	public String getSex() {
		return sex;
	}
	public User getUser() {
		return user;
	}
	public boolean isJoin() {
		return join;
	}
	public String isSex() {
		return sex;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setCreateTime(Calendar createTime) {
		this.createTime = createTime;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setExplain(String explain) {
		this.explain = explain;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public void setJoin(boolean join) {
		this.join = join;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public void setUser(User user) {
		this.user = user;
	}
}

package com.bdbox.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 收货人地址
 * @author jlj
 *
 */
@Entity
@Table(name = "b_address")
public class Address {
	/**
	 * ID
	 */
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 用户id
	 */
	@Column(name = "a_user_id")
	private Long userId;
	
	/**
	 * 收货人名称
	 */
	@Column(name="a_name")
	private String name;
	
	/**
	 * 收货人手机号码
	 */
	@Column(name = "a_mob")
	private String mob;
	
	/**
	 * 邮箱
	 */
	@Column(name = "a_email")
	private String email;
	
	/**
	 * 省份/自治区/直辖市
	 */
	@Column(name = "a_province")
	private String province;
	
	/**
	 * 城市/地区/自治州
	 */
	@Column(name = "a_city")
	private String city;
	
	/**
	 * 区/县
	 */
	@Column(name = "a_county")
	private String county;
	
	/**
	 * 详细地址
	 */
	@Column(name = "a_detailedAddress")
	private String detailedAddress;
	
	/**
	 * 邮政编码
	 */
	@Column(name = "a_postCode")
	private String postCode;
	
	/**
	 * 区号
	 */
	@Column(name = "a_areaCode")
	private String areaCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMob() {
		return mob;
	}

	public void setMob(String mob) {
		this.mob = mob;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getDetailedAddress() {
		return detailedAddress;
	}

	public void setDetailedAddress(String detailedAddress) {
		this.detailedAddress = detailedAddress;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
}

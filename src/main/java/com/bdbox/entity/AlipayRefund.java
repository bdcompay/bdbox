package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bdbox.constant.AlipayErrorCode;
import com.bdbox.constant.RefundStatus;

@Entity
@Table(name="b_alipay_refund")
public class AlipayRefund {

	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 订单号
	 */
	@Column(name="a_order_no")
	private String orderNo;
	
	/**
	 * 交易号
	 */
	@Column(name="a_trade_no")
	private String tradeNo;
	
	/**
	 * 退款金额
	 */
	@Column(name="a_total_fee")
	private Double total_fee;
	
	/**
	 * 退款状态
	 */
	@Column(name="a_refund_status")
	@Enumerated(value=EnumType.STRING)
	private RefundStatus refundStatus;
	
	/**
	 * 退款错误代码
	 */
	@Column(name="a_alipayErrorCode")
	@Enumerated(value=EnumType.STRING)
	private AlipayErrorCode alipayErrorCode;
	
	/**
	 * 退款原因
	 */
	@Column(name="a_refund_cause")
	private String refundCause;
	
	/**
	 * 创建时间
	 */
	@Column(name="a_create_time")
	private Calendar createTime;
	
	/**
	 * 退款完成时间
	 */
	@Column(name="a_finish_time")
	private Calendar finishTime;
	
	/**
	 * 退款账户
	 */
	@Column(name="a_refund_email")
	private String refundEmail;
	
	/**
	 * 结果明细
	 */
	@Column(name="a_result_detail")
	private String resultDetail;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public Double getTotal_fee() {
		return total_fee;
	}

	public void setTotal_fee(Double total_fee) {
		this.total_fee = total_fee;
	}

	public RefundStatus getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(RefundStatus refundStatus) {
		this.refundStatus = refundStatus;
	}

	public String getRefundCause() {
		return refundCause;
	}

	public void setRefundCause(String refundCause) {
		this.refundCause = refundCause;
	}

	public Calendar getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Calendar createTime) {
		this.createTime = createTime;
	}

	public Calendar getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(Calendar finishTime) {
		this.finishTime = finishTime;
	}

	public String getRefundEmail() {
		return refundEmail;
	}

	public void setRefundEmail(String refundEmail) {
		this.refundEmail = refundEmail;
	}

	public String getResultDetail() {
		return resultDetail;
	}

	public void setResultDetail(String resultDetail) {
		this.resultDetail = resultDetail;
	}

	public AlipayErrorCode getAlipayErrorCode() {
		return alipayErrorCode;
	}

	public void setAlipayErrorCode(AlipayErrorCode alipayErrorCode) {
		this.alipayErrorCode = alipayErrorCode;
	}
}

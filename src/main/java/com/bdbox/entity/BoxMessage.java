package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

import com.bdbox.constant.MsgIoType;
import com.bdbox.constant.MsgType;
import com.bdsdk.constant.DataStatusType;

/**
 * 消息数据表
 * 
 * @author will.yan
 */
@Entity
@Table(name = "b_message_box")
public class BoxMessage {
	@Id
	@GeneratedValue
	private Long id;
	/**
	 * 消息ID
	 */
	private Integer msgId;
	/**
	 * 消息类型
	 */
	@Enumerated(EnumType.STRING)
	private MsgType msgType;

	/**
	 * 消息方向
	 */
	@Enumerated(EnumType.STRING)
	private MsgIoType msgIoType;

	/**
	 * 数据状态类型
	 */
	@Enumerated(EnumType.STRING)
	private DataStatusType dataStatusType;

	/**
	 * 状态描述
	 */
	private String statusDescription;

	/**
	 * 发送者盒子
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "fromBoxId")
	private Box fromBox;

	/**
	 * 接受者盒子ID
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "toBoxId")
	private Box toBox;

	/**
	 * 家人手机号码
	 */
	private String familyMob;

	/**
	 * 家人邮箱
	 */
	private String familyMail;

	/**
	 * 短信内容
	 */
	private String content;
	/**
	 * 经度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double longitude;
	/**
	 * 纬度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double latitude;
	/**
	 * 高度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double altitude;
	/**
	 * 速度
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double speed;
	/**
	 * 方向
	 */
	@Column(columnDefinition="double(20,7) default '0.00'")
	private Double direction;

	/**
	 * 创建时间
	 */
	@Index(name = "Index_createdTime")
	private Calendar createdTime = Calendar.getInstance();
	/**
	 * 可选择联系人
	 */
	private String selectPeople;
	/**
	 * 是否有定位信息
	 */
	private Boolean islocate=true;
	/**
	 * 自由联系人
	 */
	private String freePeopleNumber;
	/**
	 * 发送用户id
	 */
	private Long fromUserId;
	/**
	 * 接收用户id
	 */
	private Long toUserId;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return 数据状态类型
	 */
	public DataStatusType getDataStatusType() {
		return dataStatusType;
	}

	public void setDataStatusType(DataStatusType dataStatusType) {
		this.dataStatusType = dataStatusType;
	}

	public Integer getMsgId() {
		return msgId;
	}

	public void setMsgId(Integer msgId) {
		this.msgId = msgId;
	}

	public MsgType getMsgType() {
		return msgType;
	}

	public void setMsgType(MsgType msgType) {
		this.msgType = msgType;
	}

	public MsgIoType getMsgIoType() {
		return msgIoType;
	}

	public void setMsgIoType(MsgIoType msgIoType) {
		this.msgIoType = msgIoType;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public Box getFromBox() {
		return fromBox;
	}

	public void setFromBox(Box fromBox) {
		this.fromBox = fromBox;
	}

	public Box getToBox() {
		return toBox;
	}

	public void setToBox(Box toBox) {
		this.toBox = toBox;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getAltitude() {
		return altitude;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public Double getDirection() {
		return direction;
	}

	public void setDirection(Double direction) {
		this.direction = direction;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}

	public String getFamilyMob() {
		return familyMob;
	}

	public void setFamilyMob(String familyMob) {
		this.familyMob = familyMob;
	}

	public String getFamilyMail() {
		return familyMail;
	}

	public void setFamilyMail(String familyMail) {
		this.familyMail = familyMail;
	}

	public String getSelectPeople() {
		return selectPeople;
	}

	public void setSelectPeople(String selectPeople) {
		this.selectPeople = selectPeople;
	}

	public Boolean getIslocate() {
		return islocate;
	}

	public void setIslocate(Boolean islocate) {
		this.islocate = islocate;
	}

	public String getFreePeopleNumber() {
		return freePeopleNumber;
	}

	public void setFreePeopleNumber(String freePeopleNumber) {
		this.freePeopleNumber = freePeopleNumber;
	}

	public Long getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(Long fromUserId) {
		this.fromUserId = fromUserId;
	}

	public Long getToUserId() {
		return toUserId;
	}

	public void setToUserId(Long toUserId) {
		this.toUserId = toUserId;
	}
	
}

package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 公告实体
 * @author will.yan
 * 
 */
@Entity
@Table(name = "b_announcement")
public class Announcement {
	@Id
	@GeneratedValue
	private long id;
	
	/**
	 * 公告内容
	 */
	private String center;
	
	/**
	 * 公告超链接
	 */
	private String hyperlink;
	
	/**
	 * 创建时间
	 */
	private Calendar creatTime=Calendar.getInstance();
	
	/**
	 * 备注
	 */
	private String remark;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCenter() {
		return center;
	}

	public void setCenter(String center) {
		this.center = center;
	}

	public String getHyperlink() {
		return hyperlink;
	}

	public void setHyperlink(String hyperlink) {
		this.hyperlink = hyperlink;
	}

	public Calendar getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Calendar creatTime) {
		this.creatTime = creatTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
}

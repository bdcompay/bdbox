package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bdbox.constant.ProductBuyType;
import com.bdbox.constant.ProductType;

/**
 * 产品
 * 
 * @author jlj
 * 
 */
@Entity
@Table(name = "b_product")
public class Product {
	/**
	 * ID
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 产品名称
	 */
	@Column(name = "p_name")
	private String name;

	/**
	 * 产品类型
	 */
	@Column(name = "p_productType")
	@Enumerated(EnumType.STRING)
	private ProductType productType;

	/**
	 * 价格
	 */
	@Column(name = "p_price" ,columnDefinition="double(20,7) default '0.00'")
	private Double price;
	
	/**
	 * 租用普通价格
	 */
	@Column(name = "p_rentPrice",columnDefinition="double(20,7) default '0.00'")
	private Double rentPrice;
	
	/**
	 * 租用实名制价格
	 */
	@Column(name = "p_rentAutonymPrice",columnDefinition="double(20,7) default '0.00'")
	private Double rentAutonymPrice;

	/**
	 * 入库总数量
	 */
	/*@Column(name = "p_totalNumber")
	private int totalNumber;*/
	
	
	/**
	 * 是否开放购买
	 */
	@Column(name="p_isopenbuy")
	private Boolean isOpenBuy;
	
	/**
	 * 购买开始时间
	 */
	@Column(name="p_buystarttime")
	private Calendar buyStartTime;
	
	/**
	 * 购买结束时间
	 */
	@Column(name="p_buyendtime")
	private Calendar buyEndTime;
	
	/**
	 * 卖出数量
	 */
	@Column(name = "p_saleNumber")
	private Integer saleNumber;

	/**
	 * 库存数量
	 */
	@Column(name = "p_stockNumber")
	private int stockNumber;
	
	/**
	 * 颜色
	 */
	@Column(name = "p_color")
	private String color;

	/**
	 * 购买类型
	 */
	@Column(name = "p_productBuyType")
	@Enumerated(EnumType.STRING)
	private ProductBuyType productBuyType;

	/**
	 * 抢购价格
	 *//*
	@Column(name = "p_shoppingRush_price",,columnDefinition="double(20,7) default '0.00'")
	private Double shoppingRush_price;

	*//**
	 * 抢购数量
	 *//*
	@Column(name = "p_shoppingRushNumber")
	private int shoppingRushNumber;*/

	/**
	 * 用户限购数量
	 */
/*	@Column(name = "p_quotaNumber")
	private int quotaNumber;*/

	/**
	 * 创建时间
	 */
	@Column(name = "p_createdTime")
	private Calendar createdTime;

	/**
	 * 产品url
	 */
	@Column(name = "p_productUrl")
	private String productUrl;

	/**
	 * 说明 解释
	 */
	@Column(name = "p_explain")
	private String explain;
	
	/**
	 * 优惠后价格
	 */
	@Column(name = "coupon_price" ,columnDefinition="double(20,7) default '0.00'")
	private Double couponPrice;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

/*	public int getTotalNumber() {
		return totalNumber;
	}

	public void setTotalNumber(int totalNumber) {
		this.totalNumber = totalNumber;
	}*/

	public int getStockNumber() {
		return stockNumber;
	}

	public void setStockNumber(int stockNumber) {
		this.stockNumber = stockNumber;
	}



/*	public int getQuotaNumber() {
		return quotaNumber;
	}

	public void setQuotaNumber(int quotaNumber) {
		this.quotaNumber = quotaNumber;
	}*/

	public String getExplain() {
		return explain;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}

	public String getProductUrl() {
		return productUrl;
	}

	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Calendar getBuyStartTime() {
		return buyStartTime;
	}

	public void setBuyStartTime(Calendar buyStartTime) {
		this.buyStartTime = buyStartTime;
	}

	public Calendar getBuyEndTime() {
		return buyEndTime;
	}

	public void setBuyEndTime(Calendar buyEndTime) {
		this.buyEndTime = buyEndTime;
	}

	public Integer getSaleNumber() {
		return saleNumber;
	}

	public void setSaleNumber(Integer saleNumber) {
		this.saleNumber = saleNumber;
	}

	public Boolean getIsOpenBuy() {
		return isOpenBuy;
	}

	public void setIsOpenBuy(Boolean isOpenBuy) {
		this.isOpenBuy = isOpenBuy;
	}

	public ProductBuyType getProductBuyType() {
		return productBuyType;
	}

	public void setProductBuyType(ProductBuyType productBuyType) {
		this.productBuyType = productBuyType;
	}

	public Double getRentPrice() {
		return rentPrice;
	}

	public void setRentPrice(Double rentPrice) {
		this.rentPrice = rentPrice;
	}

	public Double getRentAutonymPrice() {
		return rentAutonymPrice;
	}

	public void setRentAutonymPrice(Double rentAutonymPrice) {
		this.rentAutonymPrice = rentAutonymPrice;
	}

	public Double getCouponPrice() {
		return couponPrice;
	}

	public void setCouponPrice(Double couponPrice) {
		this.couponPrice = couponPrice;
	}
	
}

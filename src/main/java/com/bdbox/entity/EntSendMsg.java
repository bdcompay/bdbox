package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 企业用户发送的消息记录
 * 
 * @ClassName: EntSendMsg 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-3-22 下午3:12:45 
 * @version V5.0 
 */
@Entity
@Table(name="b_message_send_ent")
public class EntSendMsg {
	/**
	 * ID
	 */
	@Id
	@GeneratedValue
	private long id;
	/**
	 * 接收盒子序列号
	 */
	private String toBoxSerialNumber;
	/**
	 * 消息内容
	 */
	private String content;
	/**
	 * 创建时间
	 */
	private Calendar createdTime = Calendar.getInstance();
	/**
	 * 所属企业用户
	 */
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "entUserId")
	private EnterpriseUser entUser;
	
	/*********************************** 分割线    *************************************/
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getToBoxSerialNumber() {
		return toBoxSerialNumber;
	}
	public void setToBoxSerialNumber(String toBoxSerialNumber) {
		this.toBoxSerialNumber = toBoxSerialNumber;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Calendar getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}
	public EnterpriseUser getEntUser() {
		return entUser;
	}
	public void setEntUser(EnterpriseUser entUser) {
		this.entUser = entUser;
	}
}

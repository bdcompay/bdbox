package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 用户提出的建议表
 * 
 * @author will.yan
 */
@Entity
@Table(name = "b_suggestion")
public class UserSuggestion {
	@Id
	@GeneratedValue
	private long id;

	/**
	 * 发送者
	 */
	private String fromStr;

	/**
	 * 建议内容
	 */
	private String content;

	/**
	 * 创建时间
	 */
	private Calendar createdTime = Calendar.getInstance();
	/**
	 * 用户姓名
	 */
	private String username;
	/**
	 * 手机号码
	 */
	private String phone;
	/**
	 * QQ号码
	 */
	private String qq;
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFromStr() {
		return fromStr;
	}

	public void setFromStr(String fromStr) {
		this.fromStr = fromStr;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}
	
}

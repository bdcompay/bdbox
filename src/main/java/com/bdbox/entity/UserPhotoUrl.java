package com.bdbox.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
 
@Entity
@Table(name = "b_user_photo")
public class UserPhotoUrl {

	@Id
	@GeneratedValue
	private long id;
	
	/**
	 * 头像存放路径
	 */
	private String headPhotoUrl;
	
	/**
	 * 身份证正面照存放路径
	 */
	private String facadePhotoUrl;
	
	/**
	 * 身份证反面照存放路径
	 */
	private String revePhotoUrl;
	
	/**
	 * 用户id
	 */
	@Column(name = "a_user_id")
	private Long userId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getHeadPhotoUrl() {
		return headPhotoUrl;
	}

	public void setHeadPhotoUrl(String headPhotoUrl) {
		this.headPhotoUrl = headPhotoUrl;
	}

	public String getFacadePhotoUrl() {
		return facadePhotoUrl;
	}

	public void setFacadePhotoUrl(String facadePhotoUrl) {
		this.facadePhotoUrl = facadePhotoUrl;
	}

	public String getRevePhotoUrl() {
		return revePhotoUrl;
	}

	public void setRevePhotoUrl(String revePhotoUrl) {
		this.revePhotoUrl = revePhotoUrl;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	
	
}

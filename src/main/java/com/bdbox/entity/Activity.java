package com.bdbox.entity;

import com.bdbox.constant.Status; 
import java.util.Calendar; 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

 
@Entity
@Table(name="h_activity")
public class Activity { 
 
  /**
   * id
   */
  @Id
  @GeneratedValue
  private Long id;
  
  /**
   * 活动类型
   */
  @Column(name="o_status")
  @Enumerated(EnumType.STRING)
  private Status status;
  
  /**
   * 抢购开始时间
   */
  @Column(name="o_startTime")
  private Calendar startTime;
  
  /**
   * 抢购结束时间
   */
  @Column(name="o_endTime")
  private Calendar endTime;

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Status getStatus() {
		return status;
	}
	
	public void setStatus(Status status) {
		this.status = status;
	}
	
	public Calendar getStartTime() {
		return startTime;
	}
	
	public void setStartTime(Calendar startTime) {
		this.startTime = startTime;
	}
	
	public Calendar getEndTime() {
		return endTime;
	}
	
	public void setEndTime(Calendar endTime) {
		this.endTime = endTime;
	}
  
  

}

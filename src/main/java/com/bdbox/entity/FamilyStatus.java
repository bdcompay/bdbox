package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.bdbox.constant.MsgType;

/**
 * 家人用户最近通讯的盒子对象
 * 
 * @author will.yan
 */
@Entity
@Table(name = "r_family_status")
public class FamilyStatus {
	@Id
	@GeneratedValue
	private long id;

	@Enumerated(EnumType.STRING)
	private MsgType msgType;

	@ManyToOne
	@JoinColumn(name = "boxId")
	private Box box;

	private String familyMob;
	/**
	 * 通道号码
	 */
	private String channelNumber;

	/**
	 * 创建时间
	 */
	private Calendar createdTime = Calendar.getInstance();
	/**
	 * 最后一次通讯数据时间
	 */
	private Calendar lastDataUpdateTime = Calendar.getInstance();
	/**
	 * sos语音最新时间
	 */
	private Calendar sosLastCallTime;
	/**
	 * SOS频度 一个小时只能两次
	 */
	private Integer sosCallFreq;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}

	/**
	 * @return 最后更新时间
	 */
	public Calendar getLastDataUpdateTime() {
		return lastDataUpdateTime;
	}

	public void setLastDataUpdateTime(Calendar lastDataUpdateTime) {
		this.lastDataUpdateTime = lastDataUpdateTime;
	}

	public String getChannelNumber() {
		return channelNumber;
	}

	public void setChannelNumber(String channelNumber) {
		this.channelNumber = channelNumber;
	}

	public Box getBox() {
		return box;
	}

	public void setBox(Box box) {
		this.box = box;
	}

	public MsgType getMsgType() {
		return msgType;
	}

	public void setMsgType(MsgType msgType) {
		this.msgType = msgType;
	}

	public String getFamilyMob() {
		return familyMob;
	}

	public void setFamilyMob(String familyMob) {
		this.familyMob = familyMob;
	}

	public Calendar getSosLastCallTime() {
		return sosLastCallTime;
	}

	public void setSosLastCallTime(Calendar sosLastCallTime) {
		this.sosLastCallTime = sosLastCallTime;
	}

	public Integer getSosCallFreq() {
		return sosCallFreq;
	}

	public void setSosCallFreq(Integer sosCallFreq) {
		this.sosCallFreq = sosCallFreq;
	}
	
}

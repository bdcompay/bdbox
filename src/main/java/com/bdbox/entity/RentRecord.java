package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "b_rent_record")
public class RentRecord {
	
	@Id
	@GeneratedValue
	private Long id;
	
	/**
	 * 所属用户
	 */
	@Column(name = "r_user")
	private String user;
	
	/**
	 * 所属订单ID
	 */
	@Column(name = "r_orderId")
	private Long orderId;
	
	/**
	 * 押金
	 */
	@Column(name = "r_antecedent")
	private String antecedent;
	
	/**
	 * 租金
	 */
	@Column(name = "r_rental")
	private String rental;
	
	/**
	 * 总押金
	 */
	@Column(name = "r_sumAntecedent")
	private String sumAntecedent;
	
	/**
	 * 总租金
	 */
	@Column(name = "r_sumRental")
	private String sumRental;
	
	/**
	 * 买家留言
	 */
	@Column(name = "r_leaveMessage")
	private String leaveMessage;
	
	/**
	 * 租用天数
	 */
	@Column(name = "r_rentDays")
	private String rentDays;
	
	/**
	 * 租用记录生成时间
	 */
	@Column(name = "r_recordTime")
	private Calendar recordTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getAntecedent() {
		return antecedent;
	}

	public void setAntecedent(String antecedent) {
		this.antecedent = antecedent;
	}

	public String getRental() {
		return rental;
	}

	public void setRental(String rental) {
		this.rental = rental;
	}

	public String getSumAntecedent() {
		return sumAntecedent;
	}

	public void setSumAntecedent(String sumAntecedent) {
		this.sumAntecedent = sumAntecedent;
	}

	public String getSumRental() {
		return sumRental;
	}

	public void setSumRental(String sumRental) {
		this.sumRental = sumRental;
	}

	public String getLeaveMessage() {
		return leaveMessage;
	}

	public void setLeaveMessage(String leaveMessage) {
		this.leaveMessage = leaveMessage;
	}

	public String getRentDays() {
		return rentDays;
	}

	public void setRentDays(String rentDays) {
		this.rentDays = rentDays;
	}

	public Calendar getRecordTime() {
		return recordTime;
	}

	public void setRecordTime(Calendar recordTime) {
		this.recordTime = recordTime;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	
}

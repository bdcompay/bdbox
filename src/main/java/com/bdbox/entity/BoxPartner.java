package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 盒子队友表
 * 
 * @author will.yan
 */
@Entity
@Table(name = "b_box_partner")
public class BoxPartner {
	@Id
	@GeneratedValue
	private long id;
	/**
	 * 盒子用户
	 */
	@ManyToOne
	@JoinColumn(name = "boxId")
	private Box box;

	/**
	 * 队友的盒子ID
	 */
	private String partnerBoxSerialNumber;

	/**
	 * 队友的盒子备注名
	 */
	private String partnerName;

	/**
	 * 创建时间
	 */
	private Calendar createdTime = Calendar.getInstance();

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}

	public Box getBox() {
		return box;
	}

	public void setBox(Box box) {
		this.box = box;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerBoxSerialNumber() {
		return partnerBoxSerialNumber;
	}

	public void setPartnerBoxSerialNumber(String partnerBoxSerialNumber) {
		this.partnerBoxSerialNumber = partnerBoxSerialNumber;
	}

}

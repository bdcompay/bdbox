package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 手机验证码实体
 * @author admin
 *
 */
@Entity
@Table(name = "h_code")
public class MobCode {
	
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 手机号码
	 */
	@Column(name = "o_phone")
	private String phone;
	
	/**
	 * 验证码
	 */
	@Column(name = "o_code")
	private String code;
	
	/**
	 * 验证码创建时间
	 */
	@Column(name = "o_createTime")
	private Calendar createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Calendar getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Calendar createTime) {
		this.createTime = createTime;
	}
}

package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bdbox.constant.UserStatusType;
import com.bdbox.constant.UserType;

/**
 * 用户表
 * 
 * @author will.yan
 * 
 */
@Entity
@Table(name = "b_user")
public class User {
	@Id
	@GeneratedValue
	private long id;

	/**
	 * 手机号码
	 */
	private String username;

	/**
	 * 邮箱
	 */
	private String mail;

	/**
	 * 密码
	 */
	private String password;
	/**
	 * 真实姓名
	 */
	private String name;
	/**
	 * 出生日期
	 */
	private Calendar birthDate;
	/**
	 * 性别
	 */
	private String sex = "保密";
	/**
	 * 居住地
	 */
	private String residence;
	/**
	 * 职业
	 */
	private String job;
	/**
	 * 爱好
	 */
	private String hobby;
	/**
	 * 手机验证码
	 */
	private String mobileKey;
	/**
	 * 创建日期
	 */
	private Calendar createdTime = Calendar.getInstance();
	/**
	 * 更新时间
	 */
	private Calendar updateTime = Calendar.getInstance();
	/**
	 * 用户状态类型
	 */
	@Enumerated(EnumType.STRING)
	private UserStatusType userStatusType;
	
	/**
	 * 用户类型
	 */
	@Enumerated(EnumType.STRING)
	private UserType userType = UserType.ROLE_COMMON;

	/**
	 * 是否可用
	 */
	private Boolean isEnable = true;

	/**
	 * 是否启用网络数据转发服务
	 */
	private Boolean isDsiEnable = false;

	/**
	 * 是否是实名制用户
	 */
	private Boolean isAutonym = false;

	/**
	 * 用户的权限Key，用户网络数据转发的验证
	 */
	private String userPowerKey;
	/**
	 * 发送频度
	 */
	private int freq;
	/**
	 * 最新请求发送盒子消息时间
	 */
	private Calendar lastSendBoxMessageTime;

	/**
	 * 默认收货地址
	 */
	private long addressId;

	/**
	 * 实名制姓名
	 */
	private String realName;

	/**
	 * 身份证号码
	 */
	private String identification;

	/**
	 * 实名制用户手机号码
	 */
	private String mobilePhone;

	/**
	 * 实名制不通过原因
	 */
	private String reason;
	/**
	 * 实名制时间
	 */
	private Calendar isAutonymTime;
	/**
	 * 是否是北斗码推荐人
	 */
	private Boolean isRefes;
	/**
	 * 推荐人备注名
	 */
	private String recommend;

	/**
	 * 绑定驴讯通帐号
	 */
	private String lxtAccount;

	public Calendar getCreatedTime() {
		return createdTime;
	}

	public String getHobby() {
		return hobby;
	}

	public long getId() {
		return id;
	}

	public String getJob() {
		return job;
	}

	public String getMail() {
		return mail;
	}

	/**
	 * @return 手机验证码
	 */
	public String getMobileKey() {
		return mobileKey;
	}

	public String getName() {
		return name;
	}

	/**
	 * @return 用户密码，已经过MD5加密
	 */
	public String getPassword() {
		return password;
	}

	public String getResidence() {
		return residence;
	}

	public Calendar getUpdateTime() {
		return updateTime;
	}

	/**
	 * @return 用户名
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @return 用户状态
	 */

	public UserStatusType getUserStatusType() {
		return userStatusType;
	}

	public UserType getUserType() {
		return userType;
	}

	public Calendar getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Calendar birthDate) {
		this.birthDate = birthDate;
	}

	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setMobileKey(String mobileKey) {
		this.mobileKey = mobileKey;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setResidence(String residence) {
		this.residence = residence;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public void setUpdateTime(Calendar updateTime) {
		this.updateTime = updateTime;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setUserStatusType(UserStatusType userStatusType) {
		this.userStatusType = userStatusType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public Boolean getIsEnable() {
		return isEnable;
	}

	public void setIsEnable(Boolean isEnable) {
		this.isEnable = isEnable;
	}

	public Boolean getIsDsiEnable() {
		return isDsiEnable;
	}

	public void setIsDsiEnable(Boolean isDsiEnable) {
		this.isDsiEnable = isDsiEnable;
	}

	public String getUserPowerKey() {
		return userPowerKey;
	}

	public void setUserPowerKey(String userPowerKey) {
		this.userPowerKey = userPowerKey;
	}

	public int getFreq() {
		return freq;
	}

	public void setFreq(int freq) {
		this.freq = freq;
	}

	public Calendar getLastSendBoxMessageTime() {
		return lastSendBoxMessageTime;
	}

	public void setLastSendBoxMessageTime(Calendar lastSendBoxMessageTime) {
		this.lastSendBoxMessageTime = lastSendBoxMessageTime;
	}

	public long getAddressId() {
		return addressId;
	}

	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public Boolean getIsAutonym() {
		return isAutonym;
	}

	public void setIsAutonym(Boolean isAutonym) {
		this.isAutonym = isAutonym;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Calendar getIsAutonymTime() {
		return isAutonymTime;
	}

	public void setIsAutonymTime(Calendar isAutonymTime) {
		this.isAutonymTime = isAutonymTime;
	}

	public Boolean getIsRefes() {
		return isRefes;
	}

	public void setIsRefes(Boolean isRefes) {
		this.isRefes = isRefes;
	}

	public String getRecommend() {
		return recommend;
	}

	public void setRecommend(String recommend) {
		this.recommend = recommend;
	}

	public String getLxtAccount() {
		return lxtAccount;
	}

	public void setLxtAccount(String lxtAccount) {
		this.lxtAccount = lxtAccount;
	}

}

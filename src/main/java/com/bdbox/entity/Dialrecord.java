package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * SOS拨号记录
 * @author zouzhiwen
 *
 */
@Entity
@Table(name = "b_dialrecord")
public class Dialrecord {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	/**
	 * 盒子ID
	 */
	private String boxSerialNumber;
	/**
	 * 拨号时间
	 */
	private Calendar dialtime;
	/**
	 * 消息ID，api回调的消息ID
	 */
	private long msgID;
	/**
	 * 呼叫号码
	 */
	private String dialNum;
	/**
	 * 本机号码
	 */
	private String terminalNum;
	/**
	 * 是否接听
	 */
	private boolean isanswer;
	/**
	 * 接听时间
	 */
	private Calendar answertime;
	/**
	 * 挂机时间
	 */
	private Calendar hanguptime;
	/**
	 * 接听时长 单位秒
	 */
	private Long talktime;
	/**
	 * 回调地址
	 */
	private String url;
	/**
	 * 是否客户先挂机
	 */
    private boolean ishongup;
    /**
     * 音乐url
     */
    private String  musicUrl;
    /**
	 * 创建时间
	 */
	private Calendar createdtime=Calendar.getInstance();
	/**
	 * 是否已经推送
	 */
	private boolean hasSend=false;
    
	public String getMusicUrl() {
		return musicUrl;
	}

	public void setMusicUrl(String musicUrl) {
		this.musicUrl = musicUrl;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getBoxSerialNumber() {
		return boxSerialNumber;
	}

	public void setBoxSerialNumber(String boxSerialNumber) {
		this.boxSerialNumber = boxSerialNumber;
	}

	public Calendar getDialtime() {
		return dialtime;
	}

	public void setDialtime(Calendar dialtime) {
		this.dialtime = dialtime;
	}

	public long getMsgID() {
		return msgID;
	}

	public void setMsgID(long msgID) {
		this.msgID = msgID;
	}

	public String getDialNum() {
		return dialNum;
	}

	public void setDialNum(String dialNum) {
		this.dialNum = dialNum;
	}

	public String getTerminalNum() {
		return terminalNum;
	}

	public void setTerminalNum(String terminalNum) {
		this.terminalNum = terminalNum;
	}

	public boolean isIsanswer() {
		return isanswer;
	}

	public void setIsanswer(boolean isanswer) {
		this.isanswer = isanswer;
	}

	public Calendar getAnswertime() {
		return answertime;
	}

	public void setAnswertime(Calendar answertime) {
		this.answertime = answertime;
	}

	public Calendar getHanguptime() {
		return hanguptime;
	}

	public void setHanguptime(Calendar hanguptime) {
		this.hanguptime = hanguptime;
	}

	public Long getTalktime() {
		return talktime;
	}

	public void setTalktime(Long talktime) {
		this.talktime = talktime;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	public boolean isIshongup() {
		return ishongup;
	}

	public void setIshongup(boolean ishongup) {
		this.ishongup = ishongup;
	}

	public Calendar getCreatedtime() {
		return createdtime;
	}

	public void setCreatedtime(Calendar createdtime) {
		this.createdtime = createdtime;
	}

	public boolean isHasSend() {
		return hasSend;
	}

	public void setHasSend(boolean hasSend) {
		this.hasSend = hasSend;
	}
    
	
}

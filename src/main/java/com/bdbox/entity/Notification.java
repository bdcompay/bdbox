package com.bdbox.entity;

import java.util.Calendar; 
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table; 
import com.bdbox.constant.MailType;
import com.bdbox.constant.NotificationType;

/**
 * 通知
 * 
 * @author WF
 *
 */
@Entity
@Table(name="h_notification")
public class Notification {
	@Id
	@GeneratedValue
	private Long id; 
	/**
	 * 邮件主题
	 */
 	private String subject;
	/**
	 * 邮件内容
	 */
 	private String center;
	/**
	 * 通知类型
	 */
 	@Enumerated(value=EnumType.STRING)
	private MailType mailType;
	/**
	 * 通知方式
	 */
    @Enumerated(value=EnumType.STRING) 
 	private NotificationType notificationType;
    /**
	 * 发件人邮箱
	 */
  	private String frommail;
  	/**
	 * 收件人邮箱
	 */
  	private String tomail;
  	 /**
	 * 发送手机号码
	 */
  	private String sendTeliPhone;
  	/**
	 * 接收手机号码
	 */
  	private String receiveTeliPhone;
  	/**
	 * 姓名
	 */
  	private String name;
	/**
	 * 是否发送邮件
	 */
  	private Boolean isemail=false;
	/**
	 * 是否发送短信
	 */
  	private Boolean isnote=false;
  	/**
	 * 短信内容
	 */
 	private String notecenter;
 	/**
	 * 类别(客户通知，管理员通知)
	 */
 	private String typetwl;
	/**
	 * 添加时间
	 */
 	private Calendar createTime=Calendar.getInstance();
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCenter() {
		return center;
	}
	public void setCenter(String center) {
		this.center = center;
	}
	public MailType getMailType() {
		return mailType;
	}
	public void setMailType(MailType mailType) {
		this.mailType = mailType;
	}
	public NotificationType getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}
	public String getFrommail() {
		return frommail;
	}
	public void setFrommail(String frommail) {
		this.frommail = frommail;
	}
	public String getTomail() {
		return tomail;
	}
	public void setTomail(String tomail) {
		this.tomail = tomail;
	}
	public Calendar getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Calendar createTime) {
		this.createTime = createTime;
	}
	public String getSendTeliPhone() {
		return sendTeliPhone;
	}
	public void setSendTeliPhone(String sendTeliPhone) {
		this.sendTeliPhone = sendTeliPhone;
	}
	public String getReceiveTeliPhone() {
		return receiveTeliPhone;
	}
	public void setReceiveTeliPhone(String receiveTeliPhone) {
		this.receiveTeliPhone = receiveTeliPhone;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	} 
	public String getNotecenter() {
		return notecenter;
	}
	public void setNotecenter(String notecenter) {
		this.notecenter = notecenter;
	}
	public Boolean getIsemail() {
		return isemail;
	}
	public void setIsemail(Boolean isemail) {
		this.isemail = isemail;
	}
	public Boolean getIsnote() {
		return isnote;
	}
	public void setIsnote(Boolean isnote) {
		this.isnote = isnote;
	}
	public String getTypetwl() {
		return typetwl;
	}
	public void setTypetwl(String typetwl) {
		this.typetwl = typetwl;
	}
 	
 	
	 
}

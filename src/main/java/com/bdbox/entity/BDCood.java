package com.bdbox.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bdbox.constant.BdcoodStatus;

/**
 * 北斗码实体表
 * 
 */
@Entity
@Table(name = "b_bdcood")
public class BDCood {
	/**
	 * id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * 代码
	 */
	private String bdcood;

	/**
	 * 优惠金额
	 */
	private String money;

	/**
	 * 推荐人
	 */
	private String referees;

	/**
	 * 使用人
	 */
	private String username;

	/**
	 * 有效期开始时间
	 */
	private Calendar startTime;

	/**
	 * 有效期结束时间
	 */
	private Calendar endTime;

	/**
	 * 生成时间
	 */
	private Calendar creatTime = Calendar.getInstance();

	/**
	 * 是否已经使用
	 */
	@Enumerated(EnumType.STRING)
	private BdcoodStatus bdcoodStatus;

	/**
	 * 订单号
	 */
	private String orderNo;

	/**
	 * 类别(北斗券和北斗码)
	 */
	private String type;

	/**
	 * 推荐人备注姓名
	 */
	private String remakname;
	
	/**
	 * 抵用天数
	 */
	private int dayNum;
	
	/**
	 * 折扣
	 */
	@Column(name = "discount", columnDefinition="double(10,1)")
	private Double discount;
	
	/**
	 * 生成北斗码/券环境：如盒子后台、驴讯通后台
	 */
	@Column(name = "createCondition")
	private String createCondition;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBdcood() {
		return bdcood;
	}

	public void setBdcood(String bdcood) {
		this.bdcood = bdcood;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getReferees() {
		return referees;
	}

	public void setReferees(String referees) {
		this.referees = referees;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Calendar getStartTime() {
		return startTime;
	}

	public void setStartTime(Calendar startTime) {
		this.startTime = startTime;
	}

	public Calendar getEndTime() {
		return endTime;
	}

	public void setEndTime(Calendar endTime) {
		this.endTime = endTime;
	}

	public Calendar getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Calendar creatTime) {
		this.creatTime = creatTime;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRemakname() {
		return remakname;
	}

	public void setRemakname(String remakname) {
		this.remakname = remakname;
	}

	public BdcoodStatus getBdcoodStatus() {
		return bdcoodStatus;
	}

	public void setBdcoodStatus(BdcoodStatus bdcoodStatus) {
		this.bdcoodStatus = bdcoodStatus;
	}

	public int getDayNum() {
		return dayNum;
	}

	public void setDayNum(int dayNum) {
		this.dayNum = dayNum;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public String getCreateCondition() {
		return createCondition;
	}

	public void setCreateCondition(String createCondition) {
		this.createCondition = createCondition;
	}
}

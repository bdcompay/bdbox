package com.bdbox.util;

import java.math.BigDecimal;

public class BdboxUtil {

	/**
	 * 将整数型经纬度转换为度分秒形式的经纬度
	 *
	 * @param num
	 * @return
	 */
	// 将小数转换为度分秒
	public static String convertToSexagesimal(double num) {

		int du = (int) Math.floor(Math.abs(num)); // 获取整数部分
		double temp = getdPoint(Math.abs(num)) * 60;
		int fen = (int) Math.floor(temp); // 获取整数部分
		double miao = getdPoint(temp) * 60;
		miao = myRound(miao, 2);
		if (num < 0) {
			return "-" + du + "°" + fen + "′" + miao + "″";
		}
		return du + "°" + fen + "′" + miao + "″";
	}

	// 获取小数部分
	private static double getdPoint(double num) {
		double d = num;
		int fInt = (int) d;
		BigDecimal b1 = new BigDecimal(Double.toString(d));
		BigDecimal b2 = new BigDecimal(Integer.toString(fInt));
		double dPoint = b1.subtract(b2).floatValue();
		return dPoint;
	}

	/**
	 * 四舍五入
	 *
	 * @param d
	 * @param n
	 *            保留小数点后几位
	 * @return
	 */
	public static double myRound(double d, int n) {
		// 分别以A:d = 0.514d, n = 1和B:d = 0.514d, n = 2为例进行分析
		d = d * Math.pow(10, n);
		// A: 0.514 * 10 -> 5.14
		// B: 0.514 * 100 -> 51.4
		d += 0.5d;
		// A: 5.14 + 0.5 -> 5.64
		// B: 51.4 + 0.5 -> 51.9
		d = (long) d;
		// A: (long)5.64 -> 5
		// B: (long)51.9 -> 51
		d = d / Math.pow(10d, n);
		// A: 5 / 10d -> 0.5
		// B: 51 / 100d -> 0.51
		return d;
		// 合起来写
		// return (long)(d * Math.pow(10d, n) + 0.5d) / Math.pow(10d, n);
	}

}
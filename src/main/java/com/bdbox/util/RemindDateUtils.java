package com.bdbox.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class RemindDateUtils {
	/** 
	* 获取  当前年、半年、季度、月、日、小时 开始结束时间 
	*/ 
	private static final SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd"); 
    private static final SimpleDateFormat longHourSdf = new SimpleDateFormat("yyyy-MM-dd HH"); 
    private static final SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
    
/*  public RemindDateUtils(){ 
        this.shortSdf = new SimpleDateFormat("yyyy-MM-dd"); 
        this.longHourSdf = new SimpleDateFormat("yyyy-MM-dd HH"); 
        this.longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
    } */
    
    /** 
     * 获得本周的第一天，周一 
     * 
     * @return 
     */ 
    public static Date getCurrentWeekDayStartTime() { 
        Calendar c = Calendar.getInstance(); 
        try { 
            int weekday = c.get(Calendar.DAY_OF_WEEK) - 2; 
            c.add(Calendar.DATE, -weekday); 
            c.setTime(longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00")); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return c.getTime(); 
    } 

    /** 
     * 获得本周的最后一天，周日 
     * 
     * @return 
     */ 
    public  static Date getCurrentWeekDayEndTime() { 
        Calendar c = Calendar.getInstance(); 
        try { 
            int weekday = c.get(Calendar.DAY_OF_WEEK); 
            c.add(Calendar.DATE, 8 - weekday); 
            c.setTime(longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59")); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return c.getTime(); 
    } 

    /** 
     * 获得本天的开始时间，即2012-01-01 00:00:00 
     * 
     * @return 
     */ 
    public static  Date getCurrentDayStartTime() { 
        Date now = new Date(); 
        try { 
            now = shortSdf.parse(shortSdf.format(now)); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return now; 
    } 

    /** 
     * 获得本天的结束时间，即2012-01-01 23:59:59 
     * 
     * @return 
     */ 
    public static  Date getCurrentDayEndTime() { 
        Date now = new Date(); 
        try { 
            now = longSdf.parse(shortSdf.format(now) + " 23:59:59"); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return now; 
    } 

    /** 
     * 获得本小时的开始时间，即2012-01-01 23:59:59 
     * 
     * @return 
     */ 
    public static  Date getCurrentHourStartTime() { 
        Date now = new Date(); 
        try { 
            now = longHourSdf.parse(longHourSdf.format(now)); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return now; 
    } 

    /** 
     * 获得本小时的结束时间，即2012-01-01 23:59:59 
     * 
     * @return 
     */ 
    public static  Date getCurrentHourEndTime() { 
        Date now = new Date(); 
        try { 
            now = longSdf.parse(longHourSdf.format(now) + ":59:59"); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return now; 
    } 

    /** 
     * 获得本月的开始时间，即2012-01-01 00:00:00 
     * 
     * @return 
     */ 
    public static  Date getCurrentMonthStartTime() { 
        Calendar c = Calendar.getInstance(); 
        Date now = null; 
        try { 
            c.set(Calendar.DATE, 1); 
            now = shortSdf.parse(shortSdf.format(c.getTime())); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return now; 
    } 

    /** 
     * 当前月的结束时间，即2012-01-31 23:59:59 
     * 
     * @return 
     */ 
    public static  Date getCurrentMonthEndTime() { 
        Calendar c = Calendar.getInstance(); 
        Date now = null; 
        try { 
            c.set(Calendar.DATE, 1); 
            c.add(Calendar.MONTH, 1); 
            c.add(Calendar.DATE, -1); 
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59"); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return now; 
    } 

    /** 
     * 当前年的开始时间，即2012-01-01 00:00:00 
     * 
     * @return 
     */ 
    public static  Date getCurrentYearStartTime() { 
        Calendar c = Calendar.getInstance(); 
        Date now = null; 
        try { 
            c.set(Calendar.MONTH, 0); 
            c.set(Calendar.DATE, 1); 
            now = shortSdf.parse(shortSdf.format(c.getTime())); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return now; 
    } 

    /** 
     * 当前年的结束时间，即2012-12-31 23:59:59 
     * 
     * @return 
     */ 
    public static  Date getCurrentYearEndTime() { 
        Calendar c = Calendar.getInstance(); 
        Date now = null; 
        try { 
            c.set(Calendar.MONTH, 11); 
            c.set(Calendar.DATE, 31); 
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59"); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return now; 
    } 

    /** 
     * 当前季度的开始时间，即2012-01-1 00:00:00 
     * 
     * @return 
     */ 
    public static  Date getCurrentQuarterStartTime() { 
        Calendar c = Calendar.getInstance(); 
        int currentMonth = c.get(Calendar.MONTH) + 1; 
        Date now = null; 
        try { 
            if (currentMonth >= 1 && currentMonth <= 3) 
                c.set(Calendar.MONTH, 0); 
            else if (currentMonth >= 4 && currentMonth <= 6) 
                c.set(Calendar.MONTH, 3); 
            else if (currentMonth >= 7 && currentMonth <= 9) 
                c.set(Calendar.MONTH, 4); 
            else if (currentMonth >= 10 && currentMonth <= 12) 
                c.set(Calendar.MONTH, 9); 
            c.set(Calendar.DATE, 1); 
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00"); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return now; 
    } 

    /** 
     * 当前季度的结束时间，即2012-03-31 23:59:59 
     * 
     * @return 
     */ 
    public static  Date getCurrentQuarterEndTime() { 
        Calendar c = Calendar.getInstance(); 
        int currentMonth = c.get(Calendar.MONTH) + 1; 
        Date now = null; 
        try { 
            if (currentMonth >= 1 && currentMonth <= 3) { 
                c.set(Calendar.MONTH, 2); 
                c.set(Calendar.DATE, 31); 
            } else if (currentMonth >= 4 && currentMonth <= 6) { 
                c.set(Calendar.MONTH, 5); 
                c.set(Calendar.DATE, 30); 
            } else if (currentMonth >= 7 && currentMonth <= 9) { 
                c.set(Calendar.MONTH,8); 
                c.set(Calendar.DATE, 30); 
            } else if (currentMonth >= 10 && currentMonth <= 12) { 
                c.set(Calendar.MONTH, 11); 
                c.set(Calendar.DATE, 31); 
            } 
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59"); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return now; 
    } 
    /** 
     * 获取前/后半年的开始时间 
     * @return 
     */ 
    public static  Date getHalfYearStartTime(){ 
        Calendar c = Calendar.getInstance(); 
        int currentMonth = c.get(Calendar.MONTH) + 1; 
        Date now = null; 
        try { 
            if (currentMonth >= 1 && currentMonth <= 6){ 
                c.set(Calendar.MONTH, 0); 
            }else if (currentMonth >= 7 && currentMonth <= 12){ 
                c.set(Calendar.MONTH, 6); 
            } 
            c.set(Calendar.DATE, 1); 
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00"); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return now; 
        
    } 
    /** 
     * 获取前/后半年的结束时间 
     * @return 
     */ 
    public static  Date getHalfYearEndTime(){ 
        Calendar c = Calendar.getInstance(); 
        int currentMonth = c.get(Calendar.MONTH) + 1; 
        Date now = null; 
        try { 
            if (currentMonth >= 1 && currentMonth <= 6){ 
                c.set(Calendar.MONTH, 5); 
                c.set(Calendar.DATE, 30); 
            }else if (currentMonth >= 7 && currentMonth <= 12){ 
                c.set(Calendar.MONTH, 11); 
                c.set(Calendar.DATE, 31); 
            } 
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59"); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return now; 
    } 
    
    /** 
     * 获得指定参数时间的开始时间，即2012-01-01 00:00:00 
     * @param date
     * @return
     */
    public static  Date getParamDayStartTime(Date date) { 
        try { 
        	date = shortSdf.parse(shortSdf.format(date)); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return date; 
    } 
    
    /**
     * 
     */
    public static Calendar changeStringToCalendar(String time){
    	Calendar ct=Calendar.getInstance();
    	SimpleDateFormat sd=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    	Date d=new Date();
    	try {
			d=sd.parse(time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	ct.setTime(d);
    	return ct;
    	
    }
    
    /**
     * 获取指定时间是本年的第几周
	 *		（1）一周的起始日为周日，最后一天是周六
	 *		（2）以1.1所在周为一年的第一周
     * @param ca
     * 		时间
     * @return
     * 		年W周，如 2016年的第一周是：2016W1
     */
    public static String getYearWeekName(Calendar ca){
    	//本年第几周
    	int week = ca.get(Calendar.WEEK_OF_YEAR);   
    	//上一周
    	ca.add(Calendar.DAY_OF_MONTH, -7);  
    	//上一周的年
    	int year = ca.get(Calendar.YEAR);  
    	//上一周比本周大，表示两周不是同一年的周
    	if(week<ca.get(Calendar.WEEK_OF_YEAR)){  
    	    year+=1;  
    	}  
    	return year+"W"+week;
    }
    
    /**
     * 通过年周获取周起始时间
     *		（1）一周的起始日为周日，最后一天是周六
	 *		（2）以1.1所在周为一年的第一周
     * @param yearWeekName
     * 		年周，格式：yyyy'W'w ,如2016W1是 2016年的第一周
     * @return
     * @throws ParseException
     */
    public static Calendar getWeekStartTime(String yearWeekName) throws ParseException{
    	//参数错误
    	if(yearWeekName==null || !yearWeekName.contains("W")) return null;
    	//获取参数年的1月1号
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    	Calendar ca = Calendar.getInstance();
    	String[] yearWeek = yearWeekName.split("W");
    	ca.setTime(sdf.parse(yearWeek[0]+"-1-1"));
    	//参数周中的一天
    	ca.add(ca.DATE, ((Integer.valueOf(yearWeek[1])-1)*7));
    	//设置星期天为一周的第一天
    	ca.setFirstDayOfWeek(Calendar.SUNDAY);
    	//获取当前时间的第一天
    	ca.add(ca.DATE, 1-ca.get(ca.DAY_OF_WEEK));
    	
    	return ca;
    }
    
    /** 
     * 获得某一天的开始时间，即2012-01-01 00:00:00 
     * 
     * @return 
     */ 
    public static  Date getRamdomDayStartTime(Date date) { 
        try { 
        	date = shortSdf.parse(shortSdf.format(date)); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return date; 
    } 

    /** 
     * 获得某一天的结束时间，即2012-01-01 23:59:59 
     * 
     * @return 
     */ 
    public static  Date getRamdomDayEndTime(Date date) { 
        try { 
        	date = longSdf.parse(shortSdf.format(date) + " 23:59:59"); 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return date; 
    } 
    
    public static void main(String[] args) throws ParseException {
    	/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");   
    	Calendar cl = Calendar.getInstance();   
    	cl.setTime(sdf.parse("2015-12-27"));   
    	System.out.println(getYearWeekName(cl));*/
    	getWeekStartTime("2016W1");
	}
    
    
    /*
     * 		首先，需要了解一个国际标准：ISO8601，该标准是国际标准化组织用来定义日期和时间的表示方法，
     * 全称是《数据存储和交换形式·信息交换·日期和时间的表示方法》。其中有关第几周的计算，
     * 在WIKI上有如下描述：日历星期和日表示法
     * 可以用2位数表示本年内第几个日历星期，再加上一位数表示日历星期内第几天，但日历星期前要加上一个大写字母W，
     * 如2004年5月3日可写成2004-W17-3或2004W173。以1月4日所在的那个星期为当年的第一个星期，
     * 如：2005年1月1日是2004-W53-6，2005年1月3日是2005-W01-1。每个日历星期从星期一开始，星期日为第7天。
     */
}
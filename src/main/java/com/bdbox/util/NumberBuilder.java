package com.bdbox.util;

public class NumberBuilder {
	
	/**
	 * 生成退货单号
	 * 
	 * @return
	 * 		退货单号
	 */
	public static String getReturnsNum(){
		String head = "20";    //表示退货单
		long now = System.currentTimeMillis();//一个13位的时间戳
		int r1=(int)(Math.random()*(10));//生成3个0-9的随机数
		int r2=(int)(Math.random()*(10));
		int r3=(int)(Math.random()*(10));
		String returnsNum = head+now+r1+r2+r3;
		return returnsNum;
	}
	
/*	public static void main(String[] args) {
		System.out.println(getReturnsNum());
	}*/
}

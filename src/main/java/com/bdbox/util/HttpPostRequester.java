package com.bdbox.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
/**
 * 
 * @author dezhi.zhang
 * @createTime 2017年2月9日
 * @description 发送http Post请求
 */
public class HttpPostRequester {
	public static String doPost(String url, Map<String,String> headers,Map<String,String> params){
		try {
			//创建http客户端
			CloseableHttpClient httpClient=HttpClients.createDefault();
			//设置连接超时
			//httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 6000);
			//httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 6000);
			
			//创建httpPost
			HttpPost httpPost = new HttpPost(url);
			
			//设置请求头参数
			if(headers != null){
				Set<String> set = headers.keySet();
				for(String key:set){
					httpPost.setHeader(key, headers.get(key));
				}
			}
			
			//处理请求参数
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(params != null){
				Set<String> set = params.keySet();
				for(String key:set){
					nvps.add(new BasicNameValuePair(key, params.get(key)));
				}
			}
			
			//设置httpPost参数
			httpPost.setEntity(new UrlEncodedFormEntity(nvps,"utf8"));
			
			//执行http请求
			CloseableHttpResponse response = httpClient.execute(httpPost);
	
			//处理响应
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent(),"utf8"));
			StringBuffer result = new StringBuffer();
			String inputLine = null;
			while((inputLine = reader.readLine()) != null){
				result.append(inputLine);
			}
			return result.toString();
			
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * HttpPostMime请求
	 * 
	 * @param url
	 * 		请求URL
	 * @param map
	 * 		请求参数
	 * @return
	 */
	public static String httpPostMime(String url, Map<String,String> headers,Map<String,Object> params){
		//创建HttpClient
		CloseableHttpClient httpClient = HttpClients.createDefault();
		//连接超时
		//httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 1000);
		//httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 6000);
		//创建HttpPost
		HttpPost httpPost = new HttpPost(url);
		
		try {
			//设置请求头参数
			if(headers != null){
				Set<String> set = headers.keySet();
				for(String key:set){
					httpPost.setHeader(key, headers.get(key));
				}
			}
			
			//设置参数
			MultipartEntity mulEntity = new MultipartEntity();
			Set<String> set = params.keySet();
			for(String key:set){
				System.out.println(params.get(key).getClass());
				if(params.get(key).getClass().equals(File.class) || params.get(key).getClass().equals(FileBody.class) ){
					mulEntity.addPart(key, new FileBody((File)params.get(key)));
				}else{
					mulEntity.addPart(key,new StringBody((String)params.get(key)));
				}
			}
			
			//设置HttpPost参数
			httpPost.setEntity(mulEntity);
		
			//执行httpPost请求
			CloseableHttpResponse response = httpClient.execute(httpPost);
			
			//处理响应
			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(),"utf8"));
			StringBuffer result = new StringBuffer();
			String inputLine = null;
			if((inputLine = reader.readLine()) !=null ){
				result.append(inputLine);
			}
			return result.toString();
		} catch (UnsupportedEncodingException e) {
			LogUtils.logerror("HttpPost基础请求，不支持编码错误", e);
		} catch (ClientProtocolException e) {
			LogUtils.logerror("HttpPost基础请求，客服端协议错误", e);
		} catch (IOException e) {
			LogUtils.logerror("HttpPost基础请求，IO错误", e);
		} catch(Exception e){
			LogUtils.logerror("HttpPost基础请求，系统错误", e);
		} finally{
			//释放连接
			httpPost.releaseConnection();
		}
		return null;
	}
}	

package com.bdbox.util;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class MailAuthenticator extends Authenticator {
	private String username;
	private String password;

	/**
	 * 
	 * @author geloin
	 * @date 2012-5-8 下午2:48:53
	 * @param username
	 * @param password
	 */
	public MailAuthenticator(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(username, password);
	}
}

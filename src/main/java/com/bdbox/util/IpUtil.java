package com.bdbox.util;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

/**
 * ip地址工具
 * 
 * @ClassName: IpUtil 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-2-2 下午3:54:43 
 * @version V5.0 
 */
public class IpUtil {
	/**
	 * 获取访问者的ip地址
	 * @param request
	 * @return
	 */
	public static String getIp(HttpServletRequest request) {
		if (null == request) {
			return null;
		}

		String proxs[] = { "X-Forwarded-For", "Proxy-Client-IP",
				"WL-Proxy-Client-IP", "HTTP_CLIENT_IP", "HTTP_X_FORWARDED_FOR" };

		String ip = null;

		for (String prox : proxs) {
			ip = request.getHeader(prox);
			if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
				continue;
			} else {
				break;
			}
		}

		if (StringUtils.isBlank(ip)) {
			return request.getRemoteAddr();
		}

		return ip;
	}
	
	/**
	 * 获取ip归属地址
	 * @param ip
	 * @return
	 */
	public static String getIpAddr(String ip){
		String url="http://ip.taobao.com/service/getIpInfo.php?ip="+ip;
        try {
        	HttpGet httpGet = new HttpGet(url);  
            httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");  
            httpGet.addHeader("Connection", "Keep-Alive");  
            httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20100101 Firefox/29.0");  
            httpGet.addHeader("Cookie", "");
        	
            CloseableHttpClient httpclient = HttpClients.createDefault();
            CloseableHttpResponse response = httpclient.execute(httpGet);  
            int status = response.getStatusLine().getStatusCode();  
            if (status >= 200 && status < 300) {  
            	if (response == null)  return null;  
 				String result = EntityUtils.toString(response.getEntity());
 				JSONObject j = JSONObject.fromObject(result);
 				System.out.println(j.get("code"));
 				JSONObject json = j.getJSONObject("data");
 				
 				String country = json.getString("country"); //国家
 				String area = json.getString("area");  		//范围地址
 				String region = json.getString("region");  	//省区
 				String city = json.getString("city");  		//地区城市
 				String isp = json.getString("isp");  		//服务提供商
 				
 				if(area.trim().equals("")){
 					return country;
 				}
 				return region+city+"  "+isp;
            } 
		} catch (Exception e) {
			LogUtils.logerror("ip解析错误", e);
		}  
		return null;
	}
}

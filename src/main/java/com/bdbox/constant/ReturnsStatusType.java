package com.bdbox.constant;

/**
 * 退货状态
 * 1.退货申请审核中
 * 2.申请通过/未通过
 * 3.退货物流配送中
 * 4.退货产品审核中
 * 5.产品申请通过/未通过
 * 6.退款中
 * 7.已完成
 * 8.已取消
 * 
 * @author jlj
 *
 */
public enum ReturnsStatusType {
	APPLYAUDIT("申请审核中"),
	CANCEL("已取消"),
	APPLYPASS("申请通过"),
	APPLYNOTPASS("申请未通过"),
	EXPRESS("物流配送中"),
	AUDIT("审核中"),
	AUDITPASS("审核通过"),
	AUDITNOTPASS("审核未通过"),
	REFUND("退款中"),
	REFUNDFAIL("退款失败"), 
	FINISH("已完成");
	
	private String _str;

	private ReturnsStatusType(String _str) {
		this._str = _str;
	}
	
	public String _str(){
		return this._str;
	}
	
	public static ReturnsStatusType switchStatus(String returnsStatusType){
		if(returnsStatusType!=null && !"".equals(returnsStatusType)){
			if(returnsStatusType.equals("APPLYAUDIT")){
				return APPLYAUDIT;
			}else if(returnsStatusType.equals("APPLYPASS")){
				return APPLYPASS;
			}else if(returnsStatusType.equals("APPLYNOTPASS")){
				return APPLYNOTPASS;
			}else if(returnsStatusType.equals("EXPRESS")){
				return EXPRESS;
			}else if(returnsStatusType.equals("AUDIT")){
				return AUDIT;
			}else if(returnsStatusType.equals("AUDITPASS")){
				return AUDITPASS;
			}else if(returnsStatusType.equals("AUDITNOTPASS")){
				return AUDITNOTPASS;
			}else if(returnsStatusType.equals("REFUND")){
				return REFUND;
			}else if(returnsStatusType.equals("FINISH")){
				return FINISH;
			}else if(returnsStatusType.equals("CANCEL")){
				return CANCEL;
			}else if(returnsStatusType.equals("REFUNDFAIL")){
				return REFUNDFAIL;
			}
		}
		
		return null;
	}
	
	
}

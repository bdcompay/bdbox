package com.bdbox.constant;

/**
 * 银联支付卡类型
 * @author hax
 *
 */
public enum PayCardType {

	TYPE_00("未知"),
	TYPE_01("借记账户"),
	TYPE_02("贷记账户"),
	TYPE_03("准贷记账户"),
	TYPE_04("借贷合一账户"),
	TYPE_05("预付费账户"),
	TYPE_06("半开放预付费账户");
	
	private String _str;
	private PayCardType(String str){
		_str = str;
	}
	public String str(){
		return _str;
	}
	
	/**
	 * 转换类型	：PayCardType
	 * @param	str	
	 * @return
	 */
	public static PayCardType switchStatus(String str){
		switch(str){
			case "01":return TYPE_01;
			case "02":return TYPE_02;
			case "03":return TYPE_03;
			case "04":return TYPE_04;
			case "05":return TYPE_05;
			default:return TYPE_00;
		}
	}
}

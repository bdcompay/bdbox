package com.bdbox.constant;

/**
 * 银联支付方式
 * @author hax
 *
 */
public enum UnionpayType {

	TYPE_0001("认证支付"),
	TYPE_0002("快捷支付"),
	TYPE_0004("储值卡支付"),
	TYPE_0005("IC卡支付"),
	TYPE_0201("网银支付"),
	TYPE_1001("牡丹畅通卡支付"),
	TYPE_1002("中铁银通卡支付"),
	TYPE_0401("信用卡支付——暂定"),
	TYPE_0402("小额临时支付"),
	TYPE_0403("认证支付2.0"),
	TYPE_0404("互联网订单手机支付"),
	TYPE_9000("其他无卡支付"),
	NOTFOUND("未知类型");
	
	private String _str;
	private UnionpayType(String str){
		_str = str;
	}
	public String str(){
		return _str;
	}
	
	/**
	 * 转换类型	：UnionpayType
	 * @param	str	
	 * @return
	 */
	public static UnionpayType switchStatus(String str){
		switch(str){
			case "0001":return TYPE_0001;
			case "0002":return TYPE_0002;
			case "0004":return TYPE_0004;
			case "0005":return TYPE_0005;
			case "0201":return TYPE_0201;
			case "1001":return TYPE_1001;
			case "1002":return TYPE_1002;
			case "0401":return TYPE_0401;
			case "0402":return TYPE_0402;
			case "0403":return TYPE_0403;
			case "0404":return TYPE_0404;
			case "9000":return TYPE_9000;
			default:return NOTFOUND;
		}
	}
}

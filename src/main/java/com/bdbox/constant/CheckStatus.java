package com.bdbox.constant;

public enum CheckStatus {
	
	PASS("通过"),
	CHECKING("审核中"),
	PASSFAIL("审核失败");
	
	private String _str;
	private CheckStatus(String _str){
		this._str = _str;
	}
	
	public String str(){
		return _str;
	}
	
	/**
	 * checkStatus转汉字
	 * @param checkStatus
	 * @return 对应汉字
	 */
	public static String switchStatusForStr(CheckStatus checkStatus){
		
		switch (checkStatus) {
			case PASS: return "通过";
			case CHECKING: return "审核中";
			case PASSFAIL: return "审核失败";
			default: return null;
		}
	}
	
	/**
	 * checkStatus转汉字
	 * @param checkStatus
	 * @return 对应的checkStatus
	 */
	public static CheckStatus switchStatusForEng(String checkStatus){
		
		switch (checkStatus) {
			case "通过": return PASS;
			case "审核中": return CHECKING;
			case "审核失败": return PASSFAIL;
			default: return null;
		}
	}
}

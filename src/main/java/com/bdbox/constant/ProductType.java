package com.bdbox.constant;

public enum ProductType
{
  TERMINAL("终端设备");

  private String _str;

  private ProductType(String str) { this._str = str; }

  public String str() {
    return this._str;
  }

  public static ProductType switchType(String productTypeStr)
  {
    if ((productTypeStr != null) && (productTypeStr != "")) {
      if (productTypeStr.equals("TERMINAL")) {
        return TERMINAL;
      }
      return null;
    }

    return null;
  }
}
package com.bdbox.constant;

/**
 * 消息类型
 * 
 * @author will.yan
 */
public enum MsgType {

	/**
	 * SOS信息
	 */
	MSG_SOS("报警信息", 1), /**
	 * 位置信息
	 */
	MSG_LOCATION("位置信息", 2), /**
	 * 报平安信息
	 */
	MSG_SAFE("报平安信息", 3),
	/**
	 * 平台下发
	 */
	MSG_SDKSEND("平台下发", 4),
	/**
	 * 消息送达回执
	 */
	MSG_RECEIVED("平台消息送达", 5),
	/**
	 * 消息已阅读回执
	 */
	MSG_READED("平台消息已阅", 6),
	/**
	 * 消息已阅读回执
	 */
	MSG_TASK("任务中", 7),
	/**
	 * 消息已阅读回执
	 */
	MSG_OK("OK消息", 8);
	
	private String _str;
	private int _value;

	private MsgType(String str, int value) {
		_str = str;
		_value = value;
	}

	public String str() {
		return _str;
	}

	public int value() {
		return _value;
	}

	public static MsgType getFromString(String str) {
		if (str == null) {
			return null;
		}
		if (str.equals(MSG_SOS.toString())) {
			return MSG_SOS;
		}
		if (str.equals(MSG_LOCATION.toString())) {
			return MSG_LOCATION;
		}
		if (str.equals(MSG_SAFE.toString())) {
			return MSG_SAFE;
		}
		if (str.equals(MSG_SDKSEND.toString())) {
			return MSG_SDKSEND;
		}
		if (str.equals(MSG_RECEIVED.toString())) {
			return MSG_RECEIVED;
		}
		if (str.equals(MSG_READED.toString())) {
			return MSG_READED;
		}
		if (str.equals(MSG_TASK.toString())) {
			return MSG_TASK;
		}
		if (str.equals(MSG_OK.toString())) {
			return MSG_OK;
		}
		return null;

	}

	public static MsgType getFromValue(int value) {
		if (value == 1) {
			return MSG_SOS;
		}
		if (value == 2) {
			return MSG_LOCATION;
		}
		if (value == 3) {
			return MSG_SAFE;
		}
		if (value == 4) {
			return MSG_SDKSEND;
		}
		if (value == 5) {
			return MSG_RECEIVED;
		}
		if (value == 6) {
			return MSG_READED;
		}
		if (value == 7) {
			return MSG_TASK;
		}
		if (value == 8) {
			return MSG_OK;
		}
		return null;

	}
}
package com.bdbox.constant;

/**
 * 银联返回的应答码
 * @author hax
 *
 */
public enum UnionpayRespCode {

	CODE_00("成功"),
	CODE_01("交易失败"),
	CODE_02("系统未开放或暂时关闭，请稍后再试"),
	CODE_03("交易通讯超时，请发起查询交易"),
	CODE_04("交易状态未明，请查询对账结果"),
	CODE_05("交易已受理，请稍后查询交易结果"),
	CODE_10("报文格式错误"),
	CODE_11("验证签名失败"),
	CODE_12("重复交易"),
	CODE_13("报文交易要素缺失"),
	CODE_14("批量文件格式错误"),
	CODE_30("交易未通过，请尝试使用其他银联卡支付或联系95516"),
	CODE_31("商户状态不正确"),
	CODE_32("无此交易权限"),
	CODE_33("交易金额超限"),
	CODE_34("查无此交易"),
	CODE_35("原交易不存在或状态不正确"),
	CODE_36("与原交易信息不符"),
	CODE_37("已超过最大查询次数或操作过于频繁"),
	CODE_38("银联风险受限"),
	CODE_39("交易不在受理时间范围内"),
	CODE_40("绑定关系检查失败"),
	CODE_41("批量状态不正确，无法下载"),
	CODE_42("扣款成功但交易超过规定支付时间"),
	CODE_43("无此业务权限，详情请咨询95516"),
	CODE_44("输入卡号错误或暂未开通此项业务，详情请咨询95516"),
	CODE_60("交易失败，详情请咨询您的发卡行"),
	CODE_61("输入的卡号无效，请确认后输入"),
	CODE_62("交易失败，发卡银行不支持该商户，请更换其他银行卡"),
	CODE_63("卡状态不正确"),
	CODE_64("卡上的余额不足"),
	CODE_65("输入的密码、有效期或CVN2有误，交易失败"),
	CODE_66("持卡人身份信息或手机号输入不正确，验证失败"),
	CODE_67("密码输入次数超限"),
	CODE_68("您的银行卡暂不支持该业务，请向您的银行或95516咨询"),
	CODE_69("您的输入超时，交易失败"),
	CODE_70("交易已跳转，等待持卡人输入"),
	CODE_71("动态口令或短信验证码校验失败"),
	CODE_72("您尚未在{}银行网点柜面或个人网银签约加办银联无卡支付业务，请去柜面或网银开通或拨打{}"),
	CODE_73("支付卡已超过有效期"),
	CODE_74("扣款成功，销账未知"),
	CODE_75("扣款成功，销账失败"),
	CODE_76("需要验密开通"),
	CODE_77("银行卡未开通认证支付"),
	CODE_78("发卡行交易权限受限，详情请咨询您的发卡行"),
	CODE_79("此卡可用，但发卡行暂不支持短信验证"),
	CODE_80("交易失败，Token 已过期"),
	CODE_98("文件不存在"),
	CODE_99("通用错误"),
	CODE_A6("有缺陷的成功"),
	NOTFOUND("未知错误");
	
	private String _str;
	private UnionpayRespCode(String str){
		_str = str;
	}
	public String str(){
		return _str;
	}
	
	public static UnionpayRespCode switchStatus(String str){
		switch (str) {
		case "00": return CODE_00;
		case "01": return CODE_01;
		case "02": return CODE_02;
		case "03": return CODE_03;
		case "04": return CODE_04;
		case "05": return CODE_05;
		case "10": return CODE_10;
		case "11": return CODE_11;
		case "12": return CODE_12;
		case "13": return CODE_13;
		case "14": return CODE_14;
		case "30": return CODE_30;
		case "31": return CODE_31;
		case "32": return CODE_32;
		case "33": return CODE_33;
		case "34": return CODE_34;
		case "35": return CODE_35;
		case "36": return CODE_36;
		case "37": return CODE_37;
		case "38": return CODE_38;
		case "39": return CODE_39;
		case "40": return CODE_40;
		case "41": return CODE_41;
		case "42": return CODE_42;
		case "43": return CODE_43;
		case "44": return CODE_44;
		case "60": return CODE_60;
		case "61": return CODE_61;
		case "62": return CODE_62;
		case "63": return CODE_63;
		case "64": return CODE_64;
		case "65": return CODE_65;
		case "66": return CODE_66;
		case "67": return CODE_67;
		case "68": return CODE_68;
		case "69": return CODE_69;
		case "70": return CODE_70;
		case "71": return CODE_71;
		case "72": return CODE_72;
		case "73": return CODE_73;
		case "74": return CODE_74;
		case "75": return CODE_75;
		case "76": return CODE_76;
		case "77": return CODE_77;
		case "78": return CODE_78;
		case "79": return CODE_79;
		case "80": return CODE_80;
		case "98": return CODE_98;
		case "99": return CODE_99;
		case "A6": return CODE_A6;
		default: return NOTFOUND;
		}
	}
}

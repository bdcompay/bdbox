package com.bdbox.constant;

public enum NotificationType {

	  NOTE("短信"),
	  MAIL("邮件");
	  
	  private String _str; 
	  private NotificationType(String str) { this._str = str; }

	  public String _str() {
	    return this._str;
	  }

	  public static NotificationType switchStatus(String notificationType)
	  {
	    if ((notificationType != null) && (notificationType != "")) {
	      if (notificationType.equals("NOTE"))
	        return NOTE;
	      
	      if (notificationType.equals("MAIL")) {
	        return MAIL;
	      }
	      return null;
	    }

	    return null;
	  }
}

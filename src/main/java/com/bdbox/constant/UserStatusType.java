package com.bdbox.constant;

/**
 * @author will.yan 用户状态
 */
public enum UserStatusType {

	/**
	 * 正常状态
	 */
	NORMAL("正常"),
	/**
	 * 未激活状态
	 */
	NOTACTIVATED("未激活"),
	/**
	 * 禁止状态
	 */
	STOP("禁止");
	private String _str;

	private UserStatusType(String str) {
		_str = str;

	}

	public String str() {
		return _str;
	}

	public static UserStatusType switchUserStatusType(String userStatusTypeStr){
		
		if(userStatusTypeStr==null||userStatusTypeStr.equals(""))return null;
		
		if(userStatusTypeStr.equals(NORMAL.toString())){
			return NORMAL;
		}else if (userStatusTypeStr.equals(NOTACTIVATED.toString())) {
			return NOTACTIVATED;
		}else if (userStatusTypeStr.equals(STOP.toString())) {
			return STOP;
		}else {
			return null;
		}
	}
	
}

package com.bdbox.constant;

public enum MailType {

	BUYORDERNOTPAY("购买下单通知"), 
	RENTORDERNOTPAY("租赁下单通知"), 
	BUYPAY("购买付款通知"), 
	RENTPAY("租赁付款通知"), 
	BUTREFUND("购买申请退款通知"), 
	RENTREFUND("租赁申请退款通知"), 
	buySEND("购买发货通知"), 
	RENTSEND("租赁发货通知"), 
	BUYSIGN("购买签收通知"), 
	RENTSIGN("租赁签收通知"), 
	APPLYRETURN("申请退货通知"), 
	APPLYRENTRETURN("申请归还通知"), 
	BUYAPPLYRENTRETURN("退货审核申请通过通知"), 
	RENTAPPLYRENTRETURN("归还审核申请通过通知"), 
	WRITERETURN("填写退货单通知"), 
	RETURNSIGN("填写归还单通知"), 
	BUYRENT("退货退款"), 
	RENTRETURN("归还退款"), 
	XUQITIXIN("续期通知"), 
	DAOQITIXIN("到期通知"), 
	SHIMINGRENZHENGPASS("实名认证审核通过通知"), 
	SHIMINGRENZHENG("实名认证审核未通过通知"), 
	FAPIAOSHENHEPASS("发票审核通过通知"), 
	FAPIAOSHENHE("发票审核不通过通知"), 
	BUYSTOCK("库存不足通知（购买）"), 
	RENTSTOCK("库存不足通知（租赁）"),
	SHIMZHI("实名制审核通知"), 
	FPIAO("发票审核通知"), 
	BUYDAOHUO("到货通知（购买）"), 
	RENTDAOHUO("到货通知（租赁）"), 
	ACTIVTI("活动到期时间通知"), 
	ONTHOUR("下单1小时未付款通知"), 
	TENHOUR("下单10小时未付款通知"), 
	tuihuoshenqingtongguo("退货审核通过退款"), 
	guihuanshenqingtongguo("归还审核通过退款"), 
	submitquestion("用户提交问题"), 
	solvequestion("解答用户问题");

	private String _str;

	private MailType(String str) {
		this._str = str;
	}

	public String _str() {
		return this._str;
	}

	public static String switchStatusForString(MailType mailType) {
		String type = "";
		if ((mailType != null)) {
			switch (mailType) {
				case BUYORDERNOTPAY: type = "购买下单通知"; break;
				case RENTORDERNOTPAY: type = "租赁下单通知"; break;
				case BUYPAY: type = "购买付款通知"; break;
				case RENTPAY: type = "租赁付款通知"; break;
				case BUTREFUND: type = "购买申请退款通知"; break;
				case RENTREFUND: type = "租赁申请退款通知"; break;
				case buySEND: type = "购买发货通知"; break;
				case RENTSEND: type = "租赁发货通知"; break;
				case BUYSIGN: type = "购买签收通知"; break;
				case RENTSIGN: type = "租赁签收通知"; break;
				case APPLYRETURN: type = "申请退货通知"; break;
				case APPLYRENTRETURN: type = "申请归还通知"; break;
				case BUYAPPLYRENTRETURN: type = "退货审核申请通过通知"; break;
				case RENTAPPLYRENTRETURN: type = "归还审核申请通过通知"; break;
				case WRITERETURN: type = "填写退货单通知"; break;
				case RETURNSIGN: type = "填写归还单通知"; break;
				case BUYRENT: type = "退货退款"; break;
				case RENTRETURN: type = "归还退款"; break;
				case XUQITIXIN: type = "续期通知"; break;
				case DAOQITIXIN: type = "到期通知"; break;
				case SHIMINGRENZHENGPASS: type = "实名认证审核通过通知"; break;
				case SHIMINGRENZHENG: type = "实名认证审核未通过通知"; break;
				case FAPIAOSHENHEPASS: type = "发票审核通过通知"; break;
				case FAPIAOSHENHE: type = "发票审核不通过通知"; break;
				case BUYSTOCK: type = "库存不足通知（购买）"; break;
				case RENTDAOHUO: type = "到货通知（租赁）"; break;
				case ACTIVTI: type = "活动到期时间通知"; break;
				case ONTHOUR: type = "下单1小时未付款通知"; break;
				case TENHOUR: type = "下单10小时未付款通知"; break;
				case tuihuoshenqingtongguo: type = "退货审核通过退款"; break;
				case guihuanshenqingtongguo: type = "归还审核通过退款"; break;
				case submitquestion: type = "用户提交问题"; break;
				default: type = ""; break;
			}
		}
		return type;
	}
	
	public static MailType switchStatus(String mailType) {
		if ((mailType != null) && (mailType != "")) {
			if (mailType.equals("BUYORDERNOTPAY"))
				return BUYORDERNOTPAY;
			if (mailType.equals("RENTORDERNOTPAY"))
				return RENTORDERNOTPAY;
			if (mailType.equals("BUYPAY"))
				return BUYPAY;
			if (mailType.equals("RENTPAY"))
				return RENTPAY;
			if (mailType.equals("BUTREFUND"))
				return BUTREFUND;
			if (mailType.equals("RENTREFUND"))
				return RENTREFUND;
			if (mailType.equals("RENTSEND"))
				return RENTSEND;
			if (mailType.equals("BUYSIGN"))
				return BUYSIGN;
			if (mailType.equals("RENTSIGN"))
				return RENTSIGN;
			if (mailType.equals("APPLYRETURN"))
				return APPLYRETURN;
			if (mailType.equals("APPLYRENTRETURN"))
				return APPLYRENTRETURN;
			if (mailType.equals("BUYAPPLYRENTRETURN"))
				return BUYAPPLYRENTRETURN;
			if (mailType.equals("RENTAPPLYRENTRETURN"))
				return RENTAPPLYRENTRETURN;
			if (mailType.equals("WRITERETURN"))
				return WRITERETURN;
			if (mailType.equals("RETURNSIGN"))
				return RETURNSIGN;
			if (mailType.equals("BUYRENT"))
				return BUYRENT;
			if (mailType.equals("RENTRETURN"))
				return RENTRETURN;
			if (mailType.equals("XUQITIXIN"))
				return XUQITIXIN;
			if (mailType.equals("DAOQITIXIN"))
				return DAOQITIXIN;
			if (mailType.equals("SHIMINGRENZHENGPASS"))
				return SHIMINGRENZHENGPASS;
			if (mailType.equals("SHIMINGRENZHENG"))
				return SHIMINGRENZHENG;
			if (mailType.equals("FAPIAOSHENHEPASS"))
				return FAPIAOSHENHEPASS;
			if (mailType.equals("buySEND"))
				return buySEND;
			if (mailType.equals("BUYSTOCK"))
				return BUYSTOCK;
			if (mailType.equals("RENTSTOCK"))
				return RENTSTOCK;
			if (mailType.equals("SHIMZHI"))
				return SHIMZHI;
			if (mailType.equals("FPIAO"))
				return FPIAO;
			if (mailType.equals("ACTIVTI"))
				return ACTIVTI;
			if (mailType.equals("RENTDAOHUO"))
				return RENTDAOHUO;
			if (mailType.equals("BUYDAOHUO"))
				return BUYDAOHUO;
			if (mailType.equals("TENHOUR"))
				return TENHOUR;
			if (mailType.equals("ONTHOUR"))
				return ONTHOUR;
			if (mailType.equals("tuihuoshenqingtongguo"))
				return tuihuoshenqingtongguo;
			if (mailType.equals("guihuanshenqingtongguo"))
				return guihuanshenqingtongguo;
			if (mailType.equals("submitquestion"))
				return submitquestion;
			if (mailType.equals("solvequestion"))
				return solvequestion;
			if (mailType.equals("FAPIAOSHENHE")) {
				return FAPIAOSHENHE;
			}
			return null;
		}

		return null;
	}
}

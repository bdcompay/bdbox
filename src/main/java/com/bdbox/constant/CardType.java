package com.bdbox.constant;

/**
 * 北斗卡的类型
 * 
 * @author will.yan
 */
public enum CardType {

	LEVEL_1(1, "一级卡"), LEVEL_2(2, "二级卡"), LEVEL_3(3, "三级卡"), LEVEL_4(4, "四级卡");

	private int _value;
	private String _str;

	private CardType(int value, String str) {
		_value = value;
		_str = str;
	}

	public int value() {
		return _value;
	}

	public String str() {
		return _str;
	}
	public static CardType getFromString(String str) {
		if (str == null) {
			return null;
		}
		if (str.equals(LEVEL_1.toString())) {
			return LEVEL_1;
		}
		if (str.equals(LEVEL_2.toString())) {
			return LEVEL_2;
		}
		if (str.equals(LEVEL_3.toString())) {
			return LEVEL_3;
		}
		if (str.equals(LEVEL_4.toString())) {
			return LEVEL_4;
		}
		return null;
	}
}

package com.bdbox.constant;

/**
 * 用户类型
 * 
 * @author will.yan
 */
public enum UserType {

	ROLE_COMMON("普通"), 
	ROLE_MANAGER("管理"),
	ROLE_SERVICE("服务号");
	
	private String _str;

	private UserType(String str) {
		_str = str;

	}

	public String str() {
		return _str;
	}
	
	public static UserType switchUserType(String userTypeStr){
		if(userTypeStr==null||userTypeStr.equals(""))return null;
		
		if(userTypeStr.equals(ROLE_COMMON.toString())){
			return ROLE_COMMON;
		}else if(userTypeStr.equals(ROLE_MANAGER.toString())){
			return ROLE_MANAGER;
		}else if(userTypeStr.equals(ROLE_SERVICE.toString())){
			return ROLE_SERVICE;}
		else{
			return null;
		}
	}

}

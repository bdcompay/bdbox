package com.bdbox.constant;

/**
 * 盒子版本
 * 
 * @author beidouhezi
 *
 */
public enum BoxType {
	GRAZE("放牧终端"),

	ONE("一代盒子"),

	TWO("二代盒子");

	private String name;

	private BoxType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static BoxType getFromString(String str) {

		if (str == null || "".equals(str))
			return null;

		if (str.equals(GRAZE.toString())) {
			return GRAZE;
		} else if (str.equals(ONE.toString())) {
			return ONE;
		} else if (str.equals(TWO.toString())) {
			return TWO;
		} else {
			return null;
		}
	}
}

package com.bdbox.constant;

public enum RefundStatus
{
  APPLY("退款中"), 
  FAIL("退款失败"),
  CANCEL("关闭退款"), 
  ACCOMPLISH("退款成功");

  private String _str;

  private RefundStatus(String str) { this._str = str; }

  public String _str(String str) {
    return this._str;
  }

  public static RefundStatus switchStatus(String refundStatusStr)
  {
    if ((refundStatusStr != null) && (refundStatusStr != "")) {
      if (refundStatusStr.equals("APPLY"))
        return APPLY;
      if (refundStatusStr.equals("CANCEL"))
        return CANCEL;
      if (refundStatusStr.equals("FAIL"))
          return FAIL;
      if (refundStatusStr.equals("ACCOMPLISH")) {
        return ACCOMPLISH;
      }
      return null;
    }

    return null;
  }
}
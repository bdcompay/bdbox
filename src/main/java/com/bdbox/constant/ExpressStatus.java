package com.bdbox.constant;

/**
 * 物流状态
 * @author WF
 *
 */
public enum ExpressStatus {

	SENT("发出"),
	PASSAGE("途中"),
	TOOK("揽件"),
	KNOTTY("疑难"),
	SIGN("签收"),
	OUT("退签"),
	DELIVERY("派件"),
	BACK("返回");
	
	
	private String _str;
	private ExpressStatus(String str){
		_str = str;
	}
	public String str(){
		return _str;
	}
	
	/**
	 * 转换类型	：expressStatus转换汉字
	 * @param	expressStatus	汉字
	 * @return
	 */
	public static String switchStatus3(ExpressStatus expressStatus){
		switch(expressStatus){
			case SENT:return "发出";
			case PASSAGE:return "途中";
			case TOOK:return "揽件";
			case KNOTTY:return "疑难";
			case SIGN:return "签收";
			case OUT:return "退签";
			case DELIVERY:return "派件";
			case BACK:return "返回";
			default:return "未知类型";
		}
	}
	
	/**
	 * 转换类型	：汉字转换expressStatus
	 * @param	expressStatus	汉字
	 * @return
	 */
	public ExpressStatus switchStatus4(String expressStatus){
		switch(expressStatus){
			case "途中":return PASSAGE;
			case "揽件":return TOOK;
			case "疑难":return KNOTTY;
			case "签收":return SIGN;
			case "退签":return OUT;
			case "派件":return DELIVERY;
			case "返回":return BACK;
			default:return null;
		}
	}
	
	/**
	 * 转换类型	：编码转换汉字
	 * @param expressStatus	编码
	 * @return
	 */
	public String switchStatus1(String expressStatus){
		switch(expressStatus){
			case "0":return "途中";
			case "1":return "揽件";
			case "2":return "疑难";
			case "3":return "签收";
			case "4":return "退签";
			case "5":return "派件";
			case "6":return "返回";
			default:return "未知类型";
		}
	}
	
	/**
	 * 转换类型	：汉字转换编码
	 * @param	expressStatus	汉字
	 * @return
	 */
	public String switchStatus2(String expressStatus){
		switch(expressStatus){
			case "途中":return "0";
			case "揽件":return "1";
			case "疑难":return "2";
			case "签收":return "3";
			case "退签":return "4";
			case "派件":return "5";
			case "返回":return "6";
			default:return "未知类型";
		}
	}
	
}
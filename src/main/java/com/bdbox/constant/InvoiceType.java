package com.bdbox.constant;

public enum InvoiceType {

	COMMON("增值税普通发票"),
	DEDICATED("增值税专用发票");
	
	private String _str;
	
	private InvoiceType(String _str){
		this._str = _str;
	}
	
	public String str(){
		return _str;
	}
	
	/**
	 * invoiceType转汉字
	 * @param invoiceType
	 * @return 对应汉字
	 */
	public static String switchStatusForStr(InvoiceType invoiceType){
		
		switch (invoiceType) {
			case COMMON: return "增值税普通发票";
			case DEDICATED: return "增值税专用发票";
			default: return null;
		}
	}
	
	/**
	 * 汉字转invoiceType
	 * @param invoiceType
	 * @return 对应的invoiceType
	 */
	public static InvoiceType switchStatusForEng(String invoiceType){
		
		switch (invoiceType) {
			case "增值税普通发票": return COMMON;
			case "增值税专用发票": return DEDICATED;
			default:return null;
		}
	}
}

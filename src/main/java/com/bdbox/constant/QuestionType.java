package com.bdbox.constant;

public enum QuestionType {

	  GENERALQUESTION("常见问题"), 
	  KPBD("科普北斗"), 
	  NEWGUIDE("新手指南"), 
	  OUTDOORS("户外知识"),
	  NOUNEXPLAIN("名词解释"),
	  USERQUESTION("用户提问");
	  
	  private String _str;

	  private QuestionType(String str) { 
		  this._str = str; 
		  }

	  public String _str() {
	    return this._str;
	  }
	  
	  public static QuestionType switchStatus(String questionType)
	  {
	    if ((questionType != null) && (questionType != "")) {
	      if (questionType.equals("GENERALQUESTION"))
	        return GENERALQUESTION;
	      if (questionType.equals("KPBD"))
	        return KPBD;
	      if (questionType.equals("NEWGUIDE"))
	        return NEWGUIDE;
	      if (questionType.equals("NOUNEXPLAIN"))
		        return NOUNEXPLAIN;
	      if (questionType.equals("OUTDOORS")){
	        return OUTDOORS;
	      }
	      if (questionType.equals("USERQUESTION")){
	    	  return USERQUESTION;
	      }
	      return null;
	    } 
	    return null;
	  }
}

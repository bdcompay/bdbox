package com.bdbox.constant;

/**
 * 支付宝交易状态
 * @author hax
 *
 */
public enum AilpayTradeStatus {

	WAIT_BUYER_PAY("交易创建"), 	//交易创建，等待买家付款。
	TRADE_CLOSED("交易关闭"),		//在指定时间段内未支付时关闭的交易；在交易完成全额退款成功时关闭的交易。
	TRADE_SUCCESS("交易成功"),	//交易成功，且可对该交易做操作，如：多级分润、退款等。
	TRADE_PENDING("等待买家收款"),//等待卖家收款（买家付款后，如果卖家账号被冻结）。
	TRADE_FINISHED("交易成功且结束"),//交易成功且结束，即不可再做任何操作。
	NOTFOUND("未知类型");
	
	private String _str;
	private AilpayTradeStatus(String str){
		_str = str;
	}
	public String str(){
		return _str;
	}
	
	/**
	 * 转换类型	：ailpayTradeStatus
	 * @param	str	
	 * @return
	 */
	public static AilpayTradeStatus switchStatus(String str){
		switch(str){
			case "WAIT_BUYER_PAY":return WAIT_BUYER_PAY;
			case "TRADE_CLOSED":return TRADE_CLOSED;
			case "TRADE_SUCCESS":return TRADE_SUCCESS;
			case "TRADE_PENDING":return TRADE_PENDING;
			case "TRADE_FINISHED":return TRADE_FINISHED;
			default:return NOTFOUND;
		}
	}
}

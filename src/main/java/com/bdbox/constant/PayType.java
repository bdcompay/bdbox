package com.bdbox.constant;

public enum PayType {

	ALIPAY("支付宝支付"),
	WECHAT("微信支付"),
	UNIONPAY("银联支付"),
	NOTFOUND("未知类型");
	
	private String _str;
	private PayType(String str){
		_str = str;
	}
	public String str(){
		return _str;
	}
	
	/**
	 * 转换类型	：PayType
	 * @param	str	
	 * @return
	 */
	public static PayType switchStatus(String str){
		switch(str){
			case "alipay":return ALIPAY;
			case "wechat":return WECHAT;
			case "unionpay": return UNIONPAY;
			case "phone_wechat" : return WECHAT;
			default:return NOTFOUND;
		}
	}
	
	/**
	 * 转换类型	：String
	 * @param	PayType	
	 * @return
	 */
	public static String switchStatusToString(PayType payType){
		switch(payType){
			case ALIPAY:return "alipay";
			case WECHAT:return "wechat";
			case UNIONPAY:return "unionpay";
			default:return "未知类型";
		}
	}
}

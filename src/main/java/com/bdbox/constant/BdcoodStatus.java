package com.bdbox.constant;

public enum BdcoodStatus {

	  USER("已使用"), 
	  NOTUSER("未使用"), 
	  EXCEED("已过期"), 
	  NOTEXCEED("未过期");
	  
	  private String _str;

	  private BdcoodStatus(String str) { 
		  this._str = str; 
		  }

	  public String _str() {
	    return this._str;
	  }
	  
	  public static BdcoodStatus switchStatus(String dealStatusStr)
	  {
	    if ((dealStatusStr != null) && (dealStatusStr != "")) {
	      if (dealStatusStr.equals("USER"))
	        return USER;
	      if (dealStatusStr.equals("NOTUSER"))
	        return NOTUSER;
	      if (dealStatusStr.equals("EXCEED"))
	        return EXCEED;
	      if (dealStatusStr.equals("NOTEXCEED")){
	        return NOTEXCEED;
	      }
	      return null;
	    } 
	    return null;
	  }
 }

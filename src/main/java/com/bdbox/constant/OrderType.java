package com.bdbox.constant;

public enum OrderType {
	  COMMONORDER("普通订单"),
	  BDORDER("北斗通道订单");
	 
	  private String _str;

	  private OrderType(String str) { this._str = str; }

	  public String _str() {
	    return this._str;
	  }
	  
	  public static OrderType switchStatus(String ordertype)
	  {
	    if ((ordertype != null) && (ordertype != "")) {
	      if (ordertype.equals("COMMONORDER"))
	        return COMMONORDER;
	      if (ordertype.equals("BDORDER")){
	        return BDORDER; 
	      }
	      return null;
	    }

	    return null;
	  }
}

package com.bdbox.constant;

public enum Status {
	  RUSH("抢购"), 
	  ORDER("预约"), 
	  COMMONBUY("普通购买");
	  
	  private String _str;

	  private Status(String str) { this._str = str; }

	  public String _str() {
	    return this._str;
	  }
	  
	  public static Status switchType(String status) {
	    if ((status != null) && (status != "")) {
	      if (status.equals("RUSH")) 
	        return RUSH; 
	      if (status.equals("ORDER"))  
		    return ORDER; 
	      if (status.equals("COMMONBUY")){ 
	        return COMMONBUY;
	      }
	      return null; 
	    } 
	    return null;
	  }
}

package com.bdbox.constant;

/**
 * 表示产品的购买类型，区分产品是否参与抢购
 * @author jlj
 *
 */
public enum ProductBuyType {
	/**
	 * 抢购
	 */
	SHOPPINGRUSH("抢购",1),
	/**
	 * 普通
	 */
	GENERAL("普通",2),
	/**
	 * 租用
	 */
	RENT("租用",3);
	
	private String _str;
	private int _value;

	private ProductBuyType(String str, int value) {
		_str = str;
		_value = value;
	}

	public String str() {
		return _str;
	}

	public int value() {
		return _value;
	}
	
	public static ProductBuyType getFromString(String str) {
		if(str == null){
			return null;
		}
		if(str.equals(SHOPPINGRUSH.toString())){
			return SHOPPINGRUSH;
		}
		if(str.equals(GENERAL.toString())){
			return GENERAL;
		}
		if(str.equals(RENT.toString())){
			return RENT;
		}
		return null;
	}
	
	public static ProductBuyType getFromValue(int value){
		if(value==1){
			return SHOPPINGRUSH;
		}
		if(value == 2){
			return GENERAL;
		}
		if(value == 3){
			return RENT;
		}
		return null;
	}
}

package com.bdbox.constant;

public enum DealStatus
{
  DEALCLOSE("交易完成"), 
  PAID("付款成功"), 
  WAITPAID("未付款"), 
  CANCELORDER("取消订单"), 
  SENT("已发货"), 
  REBATES("退款中"), 
  REFUNDFAIL("退款失败"), 
  REFUNDED("退款成功"), 
  ABNORMAL("物流异常"), 
  CANCELREFUND("关闭退款"),
  TOAUDIT("待审核"),
  WAITREFUND("待退款"); 

  private String _str;

  private DealStatus(String str) { this._str = str; }

  public String _str() {
    return this._str;
  }

  public static DealStatus switchStatus(String dealStatusStr)
  {
    if ((dealStatusStr != null) && (dealStatusStr != "")) {
      if (dealStatusStr.equals("DEALCLOSE"))
        return DEALCLOSE;
      if (dealStatusStr.equals("PAID"))
        return PAID;
      if (dealStatusStr.equals("WAITPAID"))
        return WAITPAID;
      if (dealStatusStr.equals("SENT"))
        return SENT;
      if (dealStatusStr.equals("REBATES"))
        return REBATES;
      if (dealStatusStr.equals("REFUNDED"))
        return REFUNDED;
      if (dealStatusStr.equals("ABNORMAL"))
        return ABNORMAL;
      if (dealStatusStr.equals("CANCELREFUND"))
        return CANCELREFUND;
      if (dealStatusStr.equals("REFUNDFAIL"))
          return REFUNDFAIL;
      if (dealStatusStr.equals("TOAUDIT"))
          return TOAUDIT;
      if (dealStatusStr.equals("WAITREFUND"))
          return WAITREFUND;
      if (dealStatusStr.equals("CANCELORDER")) {
        return CANCELORDER;
      }
      return null;
    }

    return null;
  }
}
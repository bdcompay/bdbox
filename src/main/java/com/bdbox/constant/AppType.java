package com.bdbox.constant;

/**
 * APP的类型
 * 
 * @author will.yan
 * 
 */
public enum AppType {

	BOX_ANDROID("手机安卓版本"),
	/**
	 * IOS手机类型，即使用公网的手机APP
	 */
	BOX_IOS("手机IOS版本");

	private String _str;

	private AppType(String str) {
		_str = str;
	}

	public String str() {
		return _str;
	}
}

package com.bdbox.constant;

/**
 * 手机验证码类型
 * 
 * @author will.yan
 */
public enum MobKeyType {

	/**
	 * 用户注册验证码
	 */
	MOBKEY_REGISTER("注册验证码"),
	/**
	 * 用户找回密码验证码
	 */
	MOBKEY_FORGETPWD("忘记密码"),
	/**
	 * 用户重置密码验证码
	 */
	MOBKEY_RESETPWD("修改密码验证码"), 
	/**
	 * 用户修改资料验证码
	 */
	MOBKEY_CHANGEINFO("修改资料验证码"),
	/**
	 * 用户预约验证码
	 */
	MOBKEY_APPLYPWD("预约验证码");

	private String _str;

	private MobKeyType(String str) {
		_str = str;
	}

	public String str() {
		return _str;
	}

	public static MobKeyType getFromString(String str) {
		if (str == null) {
			return null;
		}
		if (str.equals(MOBKEY_REGISTER.toString())) {
			return MOBKEY_REGISTER;
		}
		if (str.equals(MOBKEY_RESETPWD.toString())) {
			return MOBKEY_RESETPWD;
		}
		if (str.equals(MOBKEY_FORGETPWD.toString())) {
			return MOBKEY_FORGETPWD;
		}
		if (str.equals(MOBKEY_CHANGEINFO.toString())) {
			return MOBKEY_CHANGEINFO;
		}
		if (str.equals(MOBKEY_APPLYPWD.toString())) {
			return MOBKEY_APPLYPWD;
		}
		return null;

	}
}

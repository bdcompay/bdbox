package com.bdbox.constant;

/**
 * 盒子状态
 * 
 * @author will.yan
 */
public enum BoxStatusType {

	/**
	 * 正常状态
	 */
	NORMAL("已激活"),
	/**
	 * 未激活状态
	 */
	NOTACTIVATED("未激活"),
	/**
	 * 禁止状态
	 */
	STOP("禁止");
	private String _str;

	private BoxStatusType(String str) {
		_str = str;

	}

	public String str() {
		return _str;
	}
	
	public static BoxStatusType getFromString(String str){
		
		if (str==null||"".equals(str)) return null;
		
		if (str.equals(NORMAL.toString())) {
			return NORMAL;
		}else if (str.equals(NOTACTIVATED.toString())) {
			return NOTACTIVATED;
		}else if (str.equals(STOP.toString())) {
			return STOP;
		}else {
			return null;
		}
	}
	
	

}

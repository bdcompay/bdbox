package com.bdbox.constant;

/**
 * 消息方向类型
 * 
 * @author will.yan
 */
public enum MsgIoType {

	/**
	 * 北斗到家人手机
	 */
	IO_BOXTOFAMILY("北斗到家人", 1), /**
	 * 家人到北斗
	 */
	IO_FAMILYTOBOX("家人到北斗", 2), /**
	 * 北斗到队友北斗
	 */
	IO_BOXTOBOX("北斗到队友", 3), /**
	 * 北斗到平台
	 */
	IO_BOXTOSYSTEM("北斗到平台", 4), 
	IO_BOXTOALL("北斗到全部", 5), 
	IO_BOXGETPARTNERLOCATION("北斗获取队友位置", 6),
	IO_BOXSLECTPEOPLE("可以选择联系人", 7),
	IO_SDKTOBOX("SDK用户到盒子", 8),// 华云科技的下发
	IO_PINGTAITOBOX("平台到盒子", 9),
	IO_FREEPEOPLE("自由联系人", 10),
	IO_FREEMOBTOBD("自由手机到盒子", 11),
	IO_EntUserTOBOX("监控平台企业用户到盒子", 12),
	IO_EntUserToBoxFree("监控平台企业用户到盒子", 13);

	private String _str;
	private int _value;

	private MsgIoType(String str, int value) {
		_str = str;
		_value = value;
	}

	public String str() {
		return _str;
	}

	public int value() {
		return _value;
	}

	public static MsgIoType getFromString(String str) {
		if (str == null) {
			return null;
		}
		if (str.equals(IO_BOXTOFAMILY.toString())) {
			return IO_BOXTOFAMILY;
		}
		if (str.equals(IO_FAMILYTOBOX.toString())) {
			return IO_FAMILYTOBOX;
		}
		if (str.equals(IO_BOXTOBOX.toString())) {
			return IO_BOXTOBOX;
		}
		if (str.equals(IO_BOXTOSYSTEM.toString())) {
			return IO_BOXTOSYSTEM;
		}
		if (str.equals(IO_BOXTOALL.toString())) {
			return IO_BOXTOALL;
		}
		if (str.equals(IO_BOXGETPARTNERLOCATION.toString())) {
			return IO_BOXGETPARTNERLOCATION;
		}
		if (str.equals(IO_BOXSLECTPEOPLE.toString())) {
			return IO_BOXSLECTPEOPLE;
		}
		if (str.equals(IO_SDKTOBOX.toString())) {
			return IO_SDKTOBOX;
		}
		if (str.equals(IO_PINGTAITOBOX.toString())) {
			return IO_PINGTAITOBOX;
		}
		if (str.equals(IO_FREEPEOPLE.toString())) {
			return IO_FREEPEOPLE;
		}
		if (str.equals(IO_FREEMOBTOBD.toString())) {
			return IO_FREEMOBTOBD;
		}
		if (str.equals(IO_EntUserTOBOX.toString())) {
			return IO_EntUserTOBOX;
		}
		if (str.equals(IO_EntUserToBoxFree.toString())) {
			return IO_EntUserToBoxFree;
		}
		return null;

	}

	public static MsgIoType getFromValue(int value) {
		if (value == 1) {
			return IO_BOXTOFAMILY;
		}
		if (value == 2) {
			return IO_FAMILYTOBOX;
		}
		if (value == 3) {
			return IO_BOXTOBOX;
		}
		if (value == 4) {
			return IO_BOXTOSYSTEM;
		}

		if (value == 5) {
			return IO_BOXTOALL;
		}
		if (value == 6) {
			return IO_BOXGETPARTNERLOCATION;
		}
		if (value == 7) {
			return IO_BOXSLECTPEOPLE;
		}
		if (value == 8) {
			return IO_SDKTOBOX;
		}
		if (value == 9) {
			return IO_PINGTAITOBOX;
		}
		if (value == 10) {
			return IO_FREEPEOPLE;
		}
		if (value == 11) {
			return IO_FREEMOBTOBD;
		}
		if (value == 12) {
			return IO_EntUserTOBOX;
		}
		if (value == 13) {
			return IO_EntUserToBoxFree;
		}
		return null;

	}
}
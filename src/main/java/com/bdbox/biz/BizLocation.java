package com.bdbox.biz;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Observable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.bdbox.api.dto.BoxLocationDto;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxLocation;
import com.bdbox.entity.SOSLocation;
import com.bdbox.service.BoxLocationService;
import com.bdbox.service.BoxService;
import com.bdbox.service.SOSLocationService;
import com.bdbox.util.LogUtils;
import com.bdsdk.util.CommonMethod;

/**
 * 
 * @author will.yan
 */
@Component
public class BizLocation extends Observable {
	private static Logger forinfo = Logger.getLogger("forinfo");

	@Autowired
	private BoxLocationService boxLocationService;
	@Autowired
	private BoxService boxService;
	@Autowired
	private SOSLocationService sOSLocationService;

	@Autowired
	public BizLocation(@Qualifier("observerDsi") ObserverBase observerDsi,
			@Qualifier("observerBizOthers") ObserverBase observerBizOthers) {
		addObserver(observerDsi);
		addObserver(observerBizOthers);
	}

	public void update(Object arg) {

		if (arg.getClass() == BoxLocation.class) {
			BoxLocation boxLocation = (BoxLocation) arg;
			if (boxLocation.getBox() != null) {
				boxLocation.setUser(boxLocation.getBox().getUser());
			}
			boxLocationService.saveBoxLocation(boxLocation);
			updateSosLastlcation(boxLocation);
			Box box = boxLocation.getBox();
			box.setAltitude(boxLocation.getAltitude());
			box.setLongitude(boxLocation.getLongitude());
			box.setLatitude(boxLocation.getLatitude());
			box.setSpeed(boxLocation.getSpeed());
			box.setDirection(boxLocation.getDirection());
			box.setUpdateTime(Calendar.getInstance());
			box.setLastLocationTime(boxLocation.getCreatedTime());
			boxService.updateBox(box);
			sendNotify(boxLocation);
		}

	}

	/**
	 * 保存盒子上传的位置数据
	 * 
	 * @param boxLocations
	 * @param box
	 * @return
	 */
	public boolean saveBoxLocationData(List<BoxLocationDto> boxLocationdtos, Box box) {
		try {
			List<BoxLocation> boxLocations = new ArrayList<BoxLocation>();
			for (BoxLocationDto dto : boxLocationdtos) {
				BoxLocation boxLocation = FromBoxLocationDto(dto, box);
				if (boxLocation != null) {
					boxLocations.add(boxLocation);
				} else {
					LogUtils.loginfo("BoxLocationDto参数异常");
					return false;
				}
			}
			for (BoxLocation boxLocation : boxLocations) {
				if (boxLocation.getBox() != null) {
					boxLocation.setUser(boxLocation.getBox().getUser());
				}
				boxLocationService.saveBoxLocation(boxLocation);
				// 更新盒子的经纬度
				if (boxLocation.getAltitude() != box.getAltitude() || boxLocation.getLatitude() != box.getLatitude()
						|| boxLocation.getLongitude() != box.getLongitude()) {
					box.setAltitude(boxLocation.getAltitude());
					box.setLatitude(boxLocation.getLatitude());
					box.setLongitude(boxLocation.getLongitude());
					boxService.updateBox(box);
				}
				sendNotify(boxLocation);
			}
			return true;
		} catch (Exception e) {
			LogUtils.logerror("保存盒子定位数据错误，", e);
		}
		return false;
	}

	private BoxLocation FromBoxLocationDto(BoxLocationDto dto, Box box) {
		BoxLocation boxLocation = null;
		try {
			boxLocation = new BoxLocation();
			// 创建时间取定位时间
			boxLocation
					.setCreatedTime(CommonMethod.StringToCalendar(dto.getPositioningTimeStr(), "yyyy/MM/dd HH:mm:ss"));
			boxLocation.setPositioningTime(
					CommonMethod.StringToCalendar(dto.getPositioningTimeStr(), "yyyy/MM/dd HH:mm:ss"));
			boxLocation.setAltitude(dto.getAltitude());
			boxLocation.setBox(box);
			boxLocation.setDirection(dto.getDirection());
			boxLocation.setLocationSource(dto.getLocationSource());
			boxLocation.setSpeed(dto.getSpeed());
			boxLocation.setLatitude(Double.parseDouble(dto.getLatitude()));
			boxLocation.setLongitude(Double.parseDouble(dto.getLongitude()));
		} catch (Exception e) {
			LogUtils.logerror("BoxLocationDto转BoxLocation错误，", e);
		}
		return boxLocation;
	}

	/**
	 * 保存SOS最新位置
	 * 
	 * @param boxMessage
	 */
	private void updateSosLastlcation(BoxLocation boxLocation) {
		if (boxLocation != null) {
			SOSLocation sOSLocation = sOSLocationService.getLocationByBoxId(boxLocation.getBox().getId());

			if (sOSLocation == null) {
				sOSLocation = new SOSLocation();
				sOSLocation.setBox(boxLocation.getBox());
			}

			if (boxLocation.getLatitude() != null && boxLocation.getLatitude() != 0) {
				sOSLocation.setLatitude(boxLocation.getLatitude());
			}
			if (boxLocation.getLongitude() != null && boxLocation.getLongitude() != 0) {
				sOSLocation.setLongitude(boxLocation.getLongitude());
			}
			if (boxLocation.getAltitude() != null && boxLocation.getAltitude() != 0) {
				sOSLocation.setAltitude(boxLocation.getAltitude());
			}
			if (boxLocation.getSpeed() != null) {
				sOSLocation.setSpeed(boxLocation.getSpeed());
			}
			if (boxLocation.getDirection() != null) {
				sOSLocation.setDirection(boxLocation.getDirection());
			}
			sOSLocation.setPositioningTime(boxLocation.getPositioningTime());
			sOSLocationService.saveOrUpdate(sOSLocation);
		}
	}

	/**
	 * 保存增强版盒子多个位置
	 * 
	 * @param lt
	 */
	public void updateBoxLocatiosns(List<BoxLocation> lt) {
		// TODO Auto-generated method stub
		// 将定位时间排序
//        MySortList<BoxLocation> msList = new MySortList<BoxLocation>();
//        msList.sortByMethod(lt, "getCreatedTime", true);
		//取四个位置的最新位置更新盒子位置
		BoxLocation MboxLocation=lt.get(0);
		for (int i = 0; i < lt.size(); i++) {
			
			BoxLocation boxLocation = lt.get(i);
			
			if(i>0){
				if(MboxLocation.getCreatedTime().getTimeInMillis()<boxLocation.getCreatedTime().getTimeInMillis()){
					MboxLocation=boxLocation;
				}
			}
			try{
//				SimpleDateFormat sd=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String stime =sd.format(boxLocation.getCreatedTime().getTime());
				System.out.println(">>>>>>>>时间："+stime);
				boxLocationService.saveBoxLocation(boxLocation);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if (i==lt.size()-1) {
				if (MboxLocation.getBox() != null) {
					MboxLocation.setUser(MboxLocation.getBox().getUser());
				}
				Box box = MboxLocation.getBox();
				box.setAltitude(MboxLocation.getAltitude());
				box.setLongitude(MboxLocation.getLongitude());
				box.setLatitude(MboxLocation.getLatitude());
				box.setSpeed(MboxLocation.getSpeed());
				box.setDirection(MboxLocation.getDirection());
				box.setUpdateTime(Calendar.getInstance());
				box.setLastLocationTime(MboxLocation.getCreatedTime());
				boxService.updateBox(box);
				updateSosLastlcation(MboxLocation);
				sendNotify(MboxLocation);
			}
		}
	}

	private void sendNotify(Object object) {
		// 确保线程安全，此处如果不加锁，并发高时会导致消息丢失
		synchronized (this) {
			setChanged();
			notifyObservers(object);
		}
	}

}

package com.bdbox.biz;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.bdbox.consts.GlobalVar;
import com.bdbox.dto.BoxLocationDto;
import com.bdbox.dto.BoxMessageDto;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxLocation;
import com.bdbox.entity.BoxMessage;
import com.bdbox.entity.EntUserPushLocation;
import com.bdbox.entity.EntUserPushMessage;
import com.bdbox.entity.EnterpriseUser;
import com.bdbox.service.EntUserPushLocationService;
import com.bdbox.service.EntUserPushMessageService;
import com.bdbox.util.HttpPostRequester;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BeansUtil;

/**
 * 其他业务数据处理
 * 
 * @author will.yan
 */
@Component
public class ObserverBizOthers extends ObserverBaseImpl implements ObserverBase {
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");
	/**
	 * 新建线程池
	 */
	private ExecutorService taskThreadPool=Executors.newFixedThreadPool(5*Runtime.getRuntime().availableProcessors());
	
	@Autowired
	private EntUserPushLocationService localService;
	@Autowired
	private EntUserPushMessageService messageService;
	
	@PostConstruct
	public void init() {
		this.start();
	}

	@Override
	public void update(Observable o, Object arg) {
		super.update(o, arg);
	}

	@Override
	public void updateObserver(Observable o, Object arg) {
		if (arg.getClass() == BoxLocation.class) {
			BoxLocation boxLocation = (BoxLocation) arg;
			//推送用户的位置信息
			doSendBoxLocation(boxLocation);
		} else if (arg.getClass() == BoxMessage.class) {
			BoxMessage boxMessage = (BoxMessage) arg;
			//推送盒子通讯信息
			doSendBoxMessage(boxMessage);
		}	
	}

	/**
	 * 转发北斗RD短报文给企业用户
	 * 
	 * @param cardMessage
	 */
	private void doSendBoxMessage(BoxMessage boxMessage) {
		try {
			Box frombox =boxMessage.getFromBox();
			Box tobox =boxMessage.getToBox();
			
			Long fromUserId = frombox.getEntUser() == null ? null : frombox
					.getEntUser().getId();
			Long toUserId =tobox==null?null: tobox.getEntUser() == null ? null : tobox.getEntUser()
					.getId();
			if (fromUserId == null && toUserId == null) {
				return;
			}
			
			// 当该回执发送卡和接收卡属同一用户时，只转发一次即可
			if (fromUserId == toUserId) {
				EnterpriseUser fromEntUser = frombox.getEntUser();
				if (fromEntUser != null &&fromEntUser.getIsDsiEnable()!=null&& fromEntUser.getIsDsiEnable()
						&& checkURL(fromEntUser.getPushURL())) {
					//生成一条信息推送记录
					EntUserPushMessage entUserPushMessage=new EntUserPushMessage();
					entUserPushMessage.setEnterpriseUser(fromEntUser);
					entUserPushMessage.setBoxMessage(boxMessage);
					messageService.save(entUserPushMessage);
					//将子线程执行的结果放入推送结果数组里
					GlobalVar.PushDataList.add(taskThreadPool.submit(new PushDataRunnable(entUserPushMessage)));
//					taskThreadPool.submit(new PushDataRunnable(entUserPushMessage));
				}
			} else {
				// 当回执发送卡和接受卡所属用户不一致时，分开转发。
				if (fromUserId != null) {
					EnterpriseUser fromEntUser = frombox.getEntUser();
					if (fromEntUser != null &&fromEntUser.getIsDsiEnable()!=null&&fromEntUser.getIsDsiEnable()
							&& checkURL(fromEntUser.getPushURL())) {
						//生成一条信息推送记录
						EntUserPushMessage entUserPushMessage=new EntUserPushMessage();
						entUserPushMessage.setEnterpriseUser(fromEntUser);
						entUserPushMessage.setBoxMessage(boxMessage);
						messageService.save(entUserPushMessage);
						//将子线程执行的结果放入推送结果数组里
						GlobalVar.PushDataList.add(taskThreadPool.submit(new PushDataRunnable(entUserPushMessage)));
//						taskThreadPool.submit(new PushDataRunnable(entUserPushMessage));
					}
				}
				if (toUserId != null) {
					EnterpriseUser toEntUser = tobox.getEntUser();
					if (toEntUser != null && toEntUser.getIsDsiEnable()!=null&&toEntUser.getIsDsiEnable()
							&& checkURL(toEntUser.getPushURL())) {
						//生成一条信息推送记录
						EntUserPushMessage entUserPushMessage=new EntUserPushMessage();
						entUserPushMessage.setEnterpriseUser(toEntUser);
						entUserPushMessage.setBoxMessage(boxMessage);
						messageService.save(entUserPushMessage);
						//将子线程执行的结果放入推送结果数组里
						GlobalVar.PushDataList.add(taskThreadPool.submit(new PushDataRunnable(entUserPushMessage)));
//						taskThreadPool.submit(new PushDataRunnable(entUserPushMessage));
					}
				}
			}
			
		} catch (Exception e) {
			forerror.info("推送短报文信息给企业用户时出错", e);
		}
	}

	/**
	 * 推送北斗定位给企业用户
	 * 
	 * @param cardLocation
	 */
	private void doSendBoxLocation(BoxLocation boxLocation) {
		try {
			Box frombox = boxLocation.getBox();
			
			if (frombox.getEntUser() != null) {
				 EnterpriseUser entUser = frombox.getEntUser();
//				if (user != null && user.getIsDsiEnable() && user.getIsEnable()) {
				if (entUser != null && entUser.getIsDsiEnable() && checkURL(entUser.getPushURL())) {
					//推送定位信息给企业用户,并生成记录
					EntUserPushLocation entUserPushLocation=new EntUserPushLocation();
					entUserPushLocation.setBoxLocation(boxLocation);
					entUserPushLocation.setEnterpriseUser(entUser);
					localService.save(entUserPushLocation);
					//将子线程执行的结果放入推送结果数组里
					GlobalVar.PushDataList.add(taskThreadPool.submit(new PushDataRunnable(entUserPushLocation)));
//					taskThreadPool.submit(new PushDataRunnable(entUserPushLocation));
				}
			}
		} catch (Exception e) {
			forerror.error(e.getMessage());
		}
	}
	
	/**
	 * 多线程执行类，该类会将执行结果返回
	 * @author dezhi.zhang
	 * @createTime 2017/01/23
	 *
	 */
	private class PushDataRunnable implements Callable<Object>{
		
		private Object data;
		
		public PushDataRunnable(Object data) {
			this.data=data;
		}

		/**
		 * 多线程执行类，执行完将结果返回
		 */
		@Override
		public Object call() throws Exception {
			//推送位置信息
			String url="";
			if(EntUserPushLocation.class==data.getClass()){
				EntUserPushLocation location=(EntUserPushLocation)data;
				
				url=location.getEnterpriseUser().getPushURL();
				if(!url.endsWith("/")){
					url+="/";
				}
				url+="location.do";
				
				Map<String, String> headers = new HashMap<String, String>();
				Map<String, String> params = new HashMap<String, String>();;
				
				params.put("boxLocation", JSON.toJSONString(case2BoxLocationDto(location.getBoxLocation())));
				forinfo.info("给用户推送位置信息：\n"+params.get("boxLocation"));
				
				do{
					//获取http请求结果
					String result = HttpPostRequester.doPost(url, headers, params);
					forinfo.info("推送位置消息返回结果：\n"+result);
					result=result.trim();
					//推送次数+1
					location.setCount(location.getCount()+1);
					ObjectResult objectResult=new ObjectResult();
					try {
						//将返回结果转为对象
						objectResult = JSON.parseObject(result, ObjectResult.class);
					} catch (Exception e) {
						objectResult.setStatus(ResultStatus.BAD_REQUEST);
					}
					if(objectResult.getStatus().name().equals(ResultStatus.OK.name())){
						//执行成功
						location.setIsSuccess(true);
						location.setError(null);
						break;
					}else{
						location.setError(result);
					}
					//休眠1s
					Thread.sleep(1000);
				}while(location.getCount()<GlobalVar.ENTERPRISE_USER_PUSH_LOCATION_COUNT && !location.getIsSuccess());
				
				return location;
				//推送通讯消息
			}else if(EntUserPushMessage.class==data.getClass()){
				EntUserPushMessage message=(EntUserPushMessage)data;
				url=message.getEnterpriseUser().getPushURL();
				if(!url.endsWith("/")){
					url+="/";
				}
				url+="message.do";
				
				Map<String, String> headers = new HashMap<String, String>();
				Map<String, String> params = new HashMap<String, String>();;
				
				params.put("boxMessage", JSON.toJSONString(case2BoxMessageDto(message.getBoxMessage())));
				forinfo.info("给用户推送通讯信息：\n"+params.get("boxMessage"));
				do{
					//http请求结果
					String result = HttpPostRequester.doPost(url, headers, params);
					forinfo.info("推送通讯消息返回结果：\n"+result);
					
					result=result.trim();
					//推送次数+1
					message.setCount(message.getCount()+1);
					ObjectResult objectResult=new ObjectResult();
					try {
						//将返回结果转为对象
						objectResult = JSON.parseObject(result, ObjectResult.class);
					} catch (Exception e) {
						objectResult.setStatus(ResultStatus.BAD_REQUEST);
					}
					if(objectResult.getStatus().name().equals(ResultStatus.OK.name())){
						//执行成功
						message.setIsSuccess(true);
						message.setError(null);
						break;
					}else{
						message.setError(result);
					}
					//休眠1s
					Thread.sleep(1000);
				}while(message.getCount()<GlobalVar.ENTERPRISE_USER_PUSH_MESSAGE_COUNT && !message.getIsSuccess());
				
				return message;
			}
			
			return "Parameter Error!";
		}

	}
	
	private BoxLocationDto case2BoxLocationDto(BoxLocation boxLocation){
		if(boxLocation==null) return null;
		BoxLocationDto boxLocationDto = new BoxLocationDto();
		BeansUtil.copyBean(boxLocationDto, boxLocation, true);
		boxLocationDto.setBoxSerialNumber(boxLocation.getBox().getBoxSerialNumber());
		boxLocationDto.setSourceType(boxLocation.getLocationSource()==null?"":boxLocation.getLocationSource().str());
		return boxLocationDto;
	}
	
	private BoxMessageDto case2BoxMessageDto(BoxMessage boxMessage){
		if(boxMessage==null) return null;
		BoxMessageDto boxMessageDto = new BoxMessageDto();
		BeansUtil.copyBean(boxMessageDto, boxMessage, true);
		
		Box frombox =boxMessage.getFromBox();
		Box tobox =boxMessage.getToBox();
		boxMessageDto.setFromBoxSerialNumber(frombox.getBoxSerialNumber());
		if(tobox!=null){
			boxMessageDto.setToBoxSerialNumber(tobox.getBoxSerialNumber());
		}
		boxMessageDto.setDataStatusType(boxMessage.getDataStatusType().str());
		boxMessageDto.setMsgIoType(boxMessage.getMsgIoType().str());
		boxMessageDto.setMsgType(boxMessage.getMsgType().str());
		return boxMessageDto;
	}
	
	private boolean checkURL(String url){
		if(url==null || "".equals(url))
		{
			return false;
		}
		if(!url.startsWith("http://") && !url.startsWith("https://")){
			return false;
		}
		return true;
	}
}

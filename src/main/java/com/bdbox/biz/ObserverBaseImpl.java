package com.bdbox.biz;

import java.util.Observable;

import com.bdsdk.util.CommonMethod;

public class ObserverBaseImpl extends Thread implements ObserverBase {

	private Queue argQ = new Queue();

	public void init() {

	}

	public void run() {
		while (true) {
			ArgEntry a = (ArgEntry) argQ.dequeue();
			Observable o = a.o;
			Object arg = a.arg;
			try {
				updateObserver(o, arg);
			} catch (Exception e) {
				System.out.println(CommonMethod.getTrace(e));
			}
		}

	}

	public void update(Observable o, Object arg) {
		ArgEntry a = new ArgEntry(o, arg);
		argQ.queue(a);

	}

	public void updateObserver(Observable o, Object arg) {

	}

}

package com.bdbox.biz;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ConcurrentLinkedQueue;

public interface ObserverBase extends Observer {
	public class Queue extends ConcurrentLinkedQueue<Object> {
		private static final long serialVersionUID = 1L;

		public synchronized void queue(Object o) {
			add(o);
			notify();
		}

		public synchronized Object dequeue() {
			while (this.isEmpty()) {
				try {
					wait();
				} catch (InterruptedException e) {
				}
			}
			Object o = this.poll();
			return o;
		}
	}

	public class ArgEntry {
		public Observable o;
		public Object arg;

		public ArgEntry(Observable o, Object arg) {
			this.o = o;
			this.arg = arg;
		}
	}

	public void init();

}

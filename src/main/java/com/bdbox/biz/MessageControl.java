package com.bdbox.biz;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bdbox.entity.BoxLocation;
import com.bdbox.entity.BoxMessage;
import com.bdbox.entity.UserSendBoxMessage;

/**
 * 
 * 消息统一入口
 * 
 * @author William
 *
 */
@Component
public class MessageControl {
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");
	@Autowired
	private BizLocation bizLocation;
	@Autowired
	private BizMsgTrans bizMsgTrans;

	/**
	 * 处理收到的聊天消息
	 * 
	 * @param chatMessage
	 * @return
	 */
	public void parseRevBoxMessage(BoxMessage boxMessage) {
		bizMsgTrans.update(boxMessage);

	}

	/**
	 * 处理收到用户位置信息
	 * 
	 * @param boxLocation
	 */
	public void parseRevBoxLocation(BoxLocation boxLocation) {
		bizLocation.update(boxLocation);
	}
	/**
	 * 处理用户下发消息
	 * @param userSendBoxMessage
	 */
	public void parseUserSendBoxMessage(UserSendBoxMessage userSendBoxMessage) {
		// TODO Auto-generated method stub
		bizMsgTrans.update(userSendBoxMessage);
	}
	
	/**
	 * 处理多个位置
	 * @param lt
	 */
	public void parseRevBoxLocation(List<BoxLocation> lt) {
		// TODO Auto-generated method stub
		bizLocation.updateBoxLocatiosns(lt);
	}

}

package com.bdbox.biz;

import java.util.Calendar;
import java.util.List;
import java.util.Observable;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.bdbox.api.dto.SOSLocationDto;
import com.bdbox.constant.BoxStatusType;
import com.bdbox.constant.MsgIoType;
import com.bdbox.constant.MsgType;
import com.bdbox.control.ControlBd;
import com.bdbox.control.ControlMail;
import com.bdbox.control.ControlSms;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxFamily;
import com.bdbox.entity.BoxMessage;
import com.bdbox.entity.BoxPartner;
import com.bdbox.entity.EnterpriseUser;
import com.bdbox.entity.SOSLocation;
import com.bdbox.entity.UserSendBoxMessage;
import com.bdbox.service.BoxFamilyService;
import com.bdbox.service.BoxMessageService;
import com.bdbox.service.BoxPartnerService;
import com.bdbox.service.BoxService;
import com.bdbox.service.DialrecordService;
import com.bdbox.service.EnterpriseUserService;
import com.bdbox.service.MsgReceiptService;
import com.bdbox.service.SOSLocationService;
import com.bdbox.service.UserSendBoxMessageService;
import com.bdbox.service.UserService;
import com.bdbox.web.dto.EnterpriseUserDto;
import com.bdsdk.constant.DataStatusType;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

/**
 * 消息转发，存储，回执处理，业务处理
 * 
 * @author will.yan
 */
@Component
public class BizMsgTrans extends Observable {
	private static Logger forinfo = Logger.getLogger("forinfo");
	private static Logger fordebug = Logger.getLogger("fordebug");
	private static Logger forerror = Logger.getLogger("forerror");
	@Autowired
	private UserService userService;
	@Autowired
	private BoxMessageService boxMessageService;
	@Autowired
	private BoxPartnerService boxPartnerService;
	@Autowired
	private BoxFamilyService boxFamilyService;
	@Autowired
	private MsgReceiptService msgReceiptService;
	@Autowired
	private ControlBd controlBd;
	@Autowired
	private ControlSms controlSms;
	@Autowired
	private ControlMail controlMail;
	@Autowired
	private BoxService boxService;
	@Autowired
	private UserSendBoxMessageService userSendBoxMessageService;
	@Autowired
	private DialrecordService dialrecordService;
	@Autowired
	private SOSLocationService sOSLocationService;
	@Autowired
	private EnterpriseUserService enterpriseUserService;
	// 平台下发的message id不能大于255
	public static int messageId = 1;

	@Autowired
	public BizMsgTrans(@Qualifier("observerDsi") ObserverBase observerDsi,
			@Qualifier("observerBizOthers") ObserverBase observerBizOthers) {
		addObserver(observerDsi);
		addObserver(observerBizOthers);
	}

	public void update(Object arg) {
		if (arg.getClass() == BoxMessage.class) {
			BoxMessage boxMessage = (BoxMessage) arg;
			if (boxMessage.getMsgIoType() == MsgIoType.IO_BOXTOBOX) {
				boxTransToPartnerBoxMessage(boxMessage);
				return;
			}
			if (boxMessage.getMsgIoType() == MsgIoType.IO_BOXTOALL) {
				boxTransToAllBoxMessage(boxMessage);
				return;
			}
			if (boxMessage.getMsgIoType() == MsgIoType.IO_BOXTOFAMILY) {
				boxTransToFamilyBoxMessage(boxMessage);
				return;
			}
			if (boxMessage.getMsgIoType() == MsgIoType.IO_FAMILYTOBOX) {
				familyTransToBoxBoxMessage(boxMessage);
				return;
			}
			if (boxMessage.getMsgIoType() == MsgIoType.IO_BOXGETPARTNERLOCATION) {
				boxGetPartnerLocation(boxMessage);
				return;
			}
			if (boxMessage.getMsgIoType() == MsgIoType.IO_BOXSLECTPEOPLE) {
				boxTransToSelectPeopleBoxMessage(boxMessage);
				return;
			}
			if (boxMessage.getMsgIoType() == MsgIoType.IO_SDKTOBOX) {
				boxTransToSdkBoxMessage(boxMessage);
				return;
			}
			if (boxMessage.getMsgIoType() == MsgIoType.IO_BOXTOSYSTEM) {
				boxTransToSystemMessage(boxMessage);
				return;
			}
			if (boxMessage.getMsgIoType() == MsgIoType.IO_FREEPEOPLE) {
				boxTransToFreePeopleMessage(boxMessage);
				return;
			}
			if (boxMessage.getMsgIoType() == MsgIoType.IO_EntUserTOBOX) {
				boxTransToEntUserBoxMessage(boxMessage);
				return;
			}
		}
		if (arg.getClass() == UserSendBoxMessage.class) {
			UserSendBoxMessage userSendBoxMessage = (UserSendBoxMessage) arg;
			if (userSendBoxMessage.getMsgIoType() == MsgIoType.IO_SDKTOBOX) {
				boxTransToSdkBoxMessage(userSendBoxMessage);
			}
			if (userSendBoxMessage.getMsgIoType() == MsgIoType.IO_EntUserTOBOX) {
				boxTransToEntUserBoxMessage(userSendBoxMessage);
			}
			if (userSendBoxMessage.getMsgIoType() == MsgIoType.IO_EntUserToBoxFree) {
				boxTransToEntUserFreeBoxMessage(userSendBoxMessage);
			}

		}
	}

	/**
	 * 盒子到系统的消息 （没测试）
	 * 
	 * @param boxMessage
	 */

	private void boxTransToSystemMessage(BoxMessage boxMessage) {
		// TODO Auto-generated method stub
		String HexContents = boxMessage.getContent();
		int size = Integer.valueOf(HexContents.substring(0, 2));
		String contents = HexContents.substring(2);
		for (int i = 0; i < size; i++) {
			boxMessage.setId(null);
			String content = contents.substring(i * 14, i * 14 + 14);
			boxMessage.setContent(content);
			boxMessageService.saveBoxMessage(boxMessage);
			BoxMessage boxMessage1 = new BoxMessage();
			BeansUtil.copyBean(boxMessage1, boxMessage, true);
			sendNotify(boxMessage1);
		}

	}

	/**
	 * 盒子获取队友位置
	 * 
	 * @param boxMessage
	 */
	private void boxGetPartnerLocation(BoxMessage boxMessage) {
		fordebug.debug("收到盒子获取队友位置的消息，详情：发送者 盒子ID："
				+ boxMessage.getFromBox().getBoxSerialNumber() + " 北斗卡号："
				+ boxMessage.getFromBox().getCardNumber());

		// 获取队友最新位置
		List<BoxPartner> boxPartners = boxPartnerService
				.getBoxPartners(boxMessage.getFromBox().getId());
		for (BoxPartner boxPartner : boxPartners) {
			BoxMessage boxMessageLocation = new BoxMessage();
			Box partnerBox = boxService.getBox(BoxStatusType.NORMAL,
					boxPartner.getPartnerBoxSerialNumber(), null, null, null);

			System.out.println(partnerBox.getBoxSerialNumber());
			if (partnerBox != null) {
				if ((partnerBox.getLongitude() == null
						|| partnerBox.getLatitude() == null || partnerBox
						.getLatitude() == null)
						|| (partnerBox.getLatitude() == 0
								&& partnerBox.getLongitude() == 0 && partnerBox
								.getLatitude() == 0)) {
					boxMessageLocation.setContent("位置未知");
					continue;
				} else {
					boxMessageLocation.setAltitude(partnerBox.getAltitude());
					boxMessageLocation.setLatitude(partnerBox.getLatitude());
					boxMessageLocation.setLongitude(partnerBox.getLongitude());
					boxMessageLocation.setSpeed(partnerBox.getSpeed());
					boxMessageLocation.setDirection(partnerBox.getDirection());

				}
				boxMessageLocation.setCreatedTime(partnerBox
						.getLastLocationTime() == null ? Calendar.getInstance()
						: partnerBox.getLastLocationTime());
				boxMessageLocation.setFromBox(partnerBox);
				boxMessageLocation.setMsgId(0);
				boxMessageLocation
						.setMsgIoType(MsgIoType.IO_BOXGETPARTNERLOCATION);
				boxMessageLocation.setMsgType(MsgType.MSG_LOCATION);
				boxMessageLocation.setToBox(boxMessage.getFromBox());
				boxMessageService.saveBoxMessage(boxMessageLocation);
				if (boxMessageLocation.getLatitude() != null
						&& boxMessageLocation.getLongitude() != null
						&& !(boxMessageLocation.getLatitude() == 0 & boxMessageLocation
								.getLongitude() == 0)) {
					controlBd.sendBdBoxMessage(boxMessageLocation);
				}

				BoxMessage boxMessage1 = new BoxMessage();
				BeansUtil.copyBean(boxMessage1, boxMessageLocation, true);
				sendNotify(boxMessage1);
			}
		}

	}

	/**
	 * 盒子到队友的消息
	 * 
	 */
	private void boxTransToPartnerBoxMessage(BoxMessage boxMessage) {
		// 判断接收者是否为盒子用户
		fordebug.debug("收到盒子到队友的消息，详情：发送者 盒子ID："
				+ boxMessage.getFromBox().getBoxSerialNumber() + " 北斗卡号："
				+ boxMessage.getFromBox().getCardNumber());
		// 转发消息给队友
		List<BoxPartner> boxPartners = boxPartnerService
				.getBoxPartners(boxMessage.getFromBox().getId());
		if (boxPartners.size() > 0) {
			boxMessage.setMsgIoType(MsgIoType.IO_BOXTOBOX);
			boxMessage.setFamilyMob(null);
			boxMessage.setFamilyMail(null);
			for (BoxPartner boxPartner : boxPartners) {
				Box partnerBox = boxService.getBox(BoxStatusType.NORMAL,
						boxPartner.getPartnerBoxSerialNumber(), null, null,
						null);
				if (partnerBox != null) {
					boxMessage.setToBox(partnerBox);
					// 保存消息
					boxMessageService.saveBoxMessage(boxMessage);
					controlBd.sendBdBoxMessage(boxMessage);

					BoxMessage boxMessage1 = new BoxMessage();
					BeansUtil.copyBean(boxMessage1, boxMessage, true);
					sendNotify(boxMessage1);
				}
			}
		}

	}

	/**
	 * 盒子到家人的消息
	 * 
	 * @param boxMessage
	 */
	private void boxTransToFamilyBoxMessage(BoxMessage boxMessage) {
		fordebug.debug("收到盒子到家人的消息，详情：发送者 盒子ID："
				+ boxMessage.getFromBox().getBoxSerialNumber() + " 北斗卡号："
				+ boxMessage.getFromBox().getCardNumber());

		// 转发消息给家人
		List<BoxFamily> boxFamilies = boxFamilyService.getBoxFamilys(boxMessage
				.getFromBox().getId());
		for (BoxFamily boxFamily : boxFamilies) {
			boxMessage.setMsgIoType(MsgIoType.IO_BOXTOFAMILY);
			boxMessage.setToBox(null);
			// 转发手机消息
			if (!StringUtils.isEmpty(boxFamily.getFamilyMob())) {
				boxMessage.setFamilyMail(null);
				boxMessage.setFamilyMob(boxFamily.getFamilyMob());
				// 保存消息
				boxMessageService.saveBoxMessage(boxMessage);
				// 转发短信
				controlSms.sendSmsBoxMessage(boxMessage);

				BoxMessage boxMessage1 = new BoxMessage();
				BeansUtil.copyBean(boxMessage1, boxMessage, true);
				sendNotify(boxMessage1);
			}
			// 转发邮件
			if (!StringUtils.isEmpty(boxFamily.getFamilyMail())) {
				boxMessage.setFamilyMob(null);
				boxMessage.setFamilyMail(boxFamily.getFamilyMail());
				// 保存消息
				boxMessageService.saveBoxMessage(boxMessage);
				try {
					controlMail.sendMailBoxMessage(boxMessage);
				} catch (Exception e) {
					// TODO: handle exception
					forerror.error("发送消息到家人邮箱时，发生错误，错误详情：盒子ID："
							+ boxMessage.getFromBox().getId() + " 家人邮箱："
							+ boxFamily.getFamilyMail() + " 错误详情："
							+ CommonMethod.getTrace(e));
				}

			}
		}

	}

	/**
	 * 盒子到全部的消息
	 * 
	 * @param boxMessage
	 */
	private void boxTransToAllBoxMessage(BoxMessage boxMessage) {
		fordebug.debug("收到盒子到家人及队友的消息，详情：发送者 盒子ID："
				+ boxMessage.getFromBox().getBoxSerialNumber() + " 北斗卡号："
				+ boxMessage.getFromBox().getCardNumber());
		//是否已经推送标志
        boolean sendflag=false;
		// 保存SOS最新位置
		saveSosLastlcation(boxMessage);
		// 注意：不独立保存IO_BOXTOALL类型的数据，均拆分为IO_BOXTOFAMILY或者IO_BOXTOBOX数据进行保存
		// 转发消息给家人
		List<BoxFamily> boxFamilies = boxFamilyService.getBoxFamilys(boxMessage
				.getFromBox().getId());
		for (BoxFamily boxFamily : boxFamilies) {
			boxMessage.setMsgIoType(MsgIoType.IO_BOXTOFAMILY);
			boxMessage.setToBox(null);
			// 转发手机消息
			if (!StringUtils.isEmpty(boxFamily.getFamilyMob())) {
				boxMessage.setFamilyMail(null);
				boxMessage.setFamilyMob(boxFamily.getFamilyMob());
				// 保存消息
				boxMessageService.saveBoxMessage(boxMessage);
				// 转发短信
				controlSms.sendSmsBoxMessage(boxMessage);
				BoxMessage boxMessage1 = new BoxMessage();
				BeansUtil.copyBean(boxMessage1, boxMessage, true);
				sendNotify(boxMessage1);
				sendflag=true;
			}
			// 转发邮件
			if (!StringUtils.isEmpty(boxFamily.getFamilyMail())) {
				boxMessage.setFamilyMob(null);
				boxMessage.setFamilyMail(boxFamily.getFamilyMail());
				// 保存消息
				boxMessageService.saveBoxMessage(boxMessage);
				try {
					controlMail.sendMailBoxMessage(boxMessage);
				} catch (Exception e) {
					// TODO: handle exception
					forerror.error("发送消息到家人邮箱时，发生错误，错误详情：盒子ID："
							+ boxMessage.getFromBox().getId() + " 家人邮箱："
							+ boxFamily.getFamilyMail() + " 错误详情："
							+ CommonMethod.getTrace(e));
				}

			}
		}
		// 转发消息给队友
		List<BoxPartner> boxPartners = boxPartnerService
				.getBoxPartners(boxMessage.getFromBox().getId());
		if (boxPartners.size() > 0) {
			boxMessage.setMsgIoType(MsgIoType.IO_BOXTOBOX);
			boxMessage.setFamilyMob(null);
			boxMessage.setFamilyMail(null);
			for (BoxPartner boxPartner : boxPartners) {
				Box partnerBox = boxService.getBox(BoxStatusType.NORMAL,
						boxPartner.getPartnerBoxSerialNumber(), null, null,
						null);
				if (partnerBox != null) {
					boxMessage.setToBox(partnerBox);
					// 保存消息
					boxMessageService.saveBoxMessage(boxMessage);
					controlBd.sendBdBoxMessage(boxMessage);
					BoxMessage boxMessage1 = new BoxMessage();
					BeansUtil.copyBean(boxMessage1, boxMessage, true);
					sendNotify(boxMessage1);
					sendflag=true;
				}
			}
		}
		//判断是否曾经推送 
		if(!sendflag){
			//sos是必须推送的
			if(boxMessage.getMsgType()==MsgType.MSG_SOS||boxMessage.getMsgType()==MsgType.MSG_OK){
				System.out.println("推送SOS");
				BoxMessage boxMessage1 = new BoxMessage();
				BeansUtil.copyBean(boxMessage1, boxMessage, true);
				sendNotify(boxMessage1);
			}
		}

	}

	/**
	 * 家人手机到盒子的消息
	 * 
	 * @param boxMessage
	 */
	private void familyTransToBoxBoxMessage(BoxMessage boxMessage) {
		fordebug.debug("收到手机到盒子的消息，详情：发送者 手机：" + boxMessage.getFamilyMob()
				+ " 接收者 盒子ID：" + boxMessage.getToBox().getBoxSerialNumber()
				+ " 北斗卡号：" + boxMessage.getToBox().getCardNumber());
		// 添加到消息列表
		boxMessage.setDataStatusType(DataStatusType.SUCCESS);
		boxMessageService.saveBoxMessage(boxMessage);
		// 加入盒子是否活跃状态
		controlBd.sendBdBoxMessage(boxMessage);

		BoxMessage boxMessage1 = new BoxMessage();
		BeansUtil.copyBean(boxMessage1, boxMessage, true);
		sendNotify(boxMessage1);
	}

	/**
	 * 盒子到可选择联系人的消息
	 * 
	 * @param boxMessage
	 */
	private void boxTransToSelectPeopleBoxMessage(BoxMessage boxMessage) {
		fordebug.debug("收到盒子选择联系人的消息，详情：发送者 盒子ID："
				+ boxMessage.getFromBox().getBoxSerialNumber() + " 北斗卡号："
				+ boxMessage.getFromBox().getCardNumber());
		String IOtoParners = boxMessage.getSelectPeople().substring(2, 5);// 队友选择标志
		String IOtoFamilys = boxMessage.getSelectPeople().substring(5);// 家人选择标志

		if (IOtoParners.equals("000") && IOtoFamilys.equals("000")) {
			// 保存消息
			boxMessageService.saveBoxMessage(boxMessage);
			BoxMessage boxMessage1 = new BoxMessage();
			BeansUtil.copyBean(boxMessage1, boxMessage, true);
			sendNotify(boxMessage1);
		}

		// 注意：不独立保存IO_BOXTOALL类型的数据，均拆分为IO_BOXTOFAMILY或者IO_BOXTOBOX数据进行保存
		// 转发消息给家人
		List<BoxFamily> boxFamilies = boxFamilyService.getBoxFamilys(boxMessage
				.getFromBox().getId());
		for (int i = 0; i < IOtoFamilys.length(); i++) {
			if (IOtoFamilys.charAt(i) == "1".charAt(0)) {
				BoxFamily boxFamily = boxFamilies.get(i);
				boxMessage.setMsgIoType(MsgIoType.IO_BOXTOFAMILY);
				boxMessage.setToBox(null);
				// 转发手机消息
				if (!StringUtils.isEmpty(boxFamily.getFamilyMob())) {
					boxMessage.setFamilyMail(null);
					boxMessage.setFamilyMob(boxFamily.getFamilyMob());
					// 保存消息
					boxMessageService.saveBoxMessage(boxMessage);
					// 转发短信
					controlSms.sendSmsBoxMessage(boxMessage);

					BoxMessage boxMessage1 = new BoxMessage();
					BeansUtil.copyBean(boxMessage1, boxMessage, true);
					sendNotify(boxMessage1);
				}
				// 转发邮件
				if (!StringUtils.isEmpty(boxFamily.getFamilyMail())) {
					boxMessage.setFamilyMob(null);
					boxMessage.setFamilyMail(boxFamily.getFamilyMail());
					// 保存消息
					boxMessageService.saveBoxMessage(boxMessage);
					try {
						controlMail.sendMailBoxMessage(boxMessage);
					} catch (Exception e) {
						// TODO: handle exception
						forerror.error("发送消息到家人邮箱时，发生错误，错误详情：盒子ID："
								+ boxMessage.getFromBox().getId() + " 家人邮箱："
								+ boxFamily.getFamilyMail() + " 错误详情："
								+ CommonMethod.getTrace(e));
					}
				}
			}
		}

		// 转发消息给队友
		List<BoxPartner> boxPartners = boxPartnerService
				.getBoxPartners(boxMessage.getFromBox().getId());
		if (boxPartners.size() > 0) {
			boxMessage.setMsgIoType(MsgIoType.IO_BOXTOBOX);
			boxMessage.setFamilyMob(null);
			boxMessage.setFamilyMail(null);
			for (int i = 0; i < IOtoParners.length(); i++) {
				if (IOtoParners.charAt(i) == "1".charAt(0)) {
					BoxPartner boxPartner = boxPartners.get(i);
					Box partnerBox = boxService.getBox(BoxStatusType.NORMAL,
							boxPartner.getPartnerBoxSerialNumber(), null, null,
							null);
					if (partnerBox != null) {
						boxMessage.setToBox(partnerBox);
						// 保存消息
						boxMessageService.saveBoxMessage(boxMessage);
						controlBd.sendBdBoxMessage(boxMessage);
						BoxMessage boxMessage1 = new BoxMessage();
						BeansUtil.copyBean(boxMessage1, boxMessage, true);
						sendNotify(boxMessage1);
					}

				}
			}
		}
	}

	/**
	 * sdk用户下发盒子信息
	 * 
	 * @param boxMessage
	 */
	private void boxTransToSdkBoxMessage(BoxMessage boxMessage) {
		String username = "";
		if (boxMessage.getFromBox().getUser().getUsername() != null
				&& !boxMessage.getFromBox().getUser().getUsername().equals("")) {
			username = boxMessage.getFromBox().getUser().getUsername();
		}
		// TODO Auto-generated method stub
		fordebug.debug("收到sdk用户下发给盒子的消息，详情：发送者：" + username + " 目标盒子："
				+ boxMessage.getToBox().getBoxSerialNumber());
		boxMessage.setDataStatusType(DataStatusType.SUCCESS);
		// 保存消息
		boxMessageService.saveBoxMessage(boxMessage);
		controlBd.sendBdBoxMessage(boxMessage);
	}

	/**
	 * sdk用户下发平台消息
	 * 
	 * @param boxMessage
	 */
	private void boxTransToSdkBoxMessage(UserSendBoxMessage userSendboxMessage) {
		String username = "";
		if (userSendboxMessage.getFromUser() != null
				&& userSendboxMessage.getFromUser().getUsername() != null
				&& !userSendboxMessage.getFromUser().getUsername().equals("")) {
			username = userSendboxMessage.getFromUser().getUsername();
		}
		// TODO Auto-generated method stub
		fordebug.debug("收到sdk用户下发给盒子的消息，详情：发送者：" + username + " 目标盒子："
				+ userSendboxMessage.getBoxSerialNumber());
		userSendboxMessage.setDataStatusType(DataStatusType.SUCCESS);
		userSendboxMessage.setSendTime(Calendar.getInstance());
		// 保存消息
		userSendBoxMessageService.saveUserSendboxMessage(userSendboxMessage);
		controlBd.sendBdBoxMessage(userSendboxMessage);
	}

	/**
	 * 发消息给自由联系人
	 * 
	 * @param boxMessage
	 */
	private void boxTransToFreePeopleMessage(BoxMessage boxMessage) {
		// TODO Auto-generated method stub
		boxMessage.setMsgIoType(MsgIoType.IO_FREEPEOPLE);
		String freeNumber = boxMessage.getFreePeopleNumber();

		if (freeNumber.length() < 10) {
			//
			Box partnerBox = boxService.getBox(BoxStatusType.NORMAL,
					freeNumber, null, null, null);
			if (partnerBox != null) {
				boxMessage.setToBox(partnerBox);
				// 保存消息
				boxMessageService.saveBoxMessage(boxMessage);
				controlBd.sendBdBoxMessage(boxMessage);

				BoxMessage boxMessage1 = new BoxMessage();
				BeansUtil.copyBean(boxMessage1, boxMessage, true);
				sendNotify(boxMessage1);
			} else {
				Box toBox = boxMessage.getFromBox();
				Box fromBox = new Box();
				fromBox.setBoxSerialNumber(boxMessage.getToBox()
						.getBoxSerialNumber());
				boxMessage.setToBox(toBox);
				boxMessage.setFromBox(fromBox);
				boxMessage.setContent("盒子" + fromBox + "不存在!");
				controlBd.sendBdBoxMessage(boxMessage);
			}
		}
		if (freeNumber.length() > 10) {
			// 转发手机消息
			if (!StringUtils.isEmpty(freeNumber)) {
				boxMessage.setFamilyMail(null);
				boxMessage.setFamilyMob(freeNumber);
				// 保存消息
				boxMessageService.saveBoxMessage(boxMessage);
				// 转发短信
				controlSms.sendSmsBoxMessage(boxMessage);

				BoxMessage boxMessage1 = new BoxMessage();
				BeansUtil.copyBean(boxMessage1, boxMessage, true);
				sendNotify(boxMessage1);
			}
		}
	}

	/**
	 * 保存SOS最新位置
	 * 
	 * @param boxMessage
	 */
	private void saveSosLastlcation(BoxMessage boxMessage) {
		if (boxMessage != null && boxMessage.getMsgType() == MsgType.MSG_SOS) {
			SOSLocation sOSLocation = sOSLocationService
					.getLocationByBoxId(boxMessage.getFromBox().getId());
			if (sOSLocation == null) {
				sOSLocation = new SOSLocation();
				sOSLocation.setBox(boxMessage.getFromBox());
			}
			sOSLocation.setCreatedTime(Calendar.getInstance());

			if (boxMessage.getLatitude() != null
					&& boxMessage.getLatitude() != 0) {
				sOSLocation.setLatitude(boxMessage.getLatitude());
			}
			if (boxMessage.getLongitude() != null
					&& boxMessage.getLongitude() != 0) {
				sOSLocation.setLongitude(boxMessage.getLongitude());
			}
			if (boxMessage.getAltitude() != null
					&& boxMessage.getAltitude() != 0) {
				sOSLocation.setAltitude(boxMessage.getAltitude());
			}
			if (boxMessage.getSpeed() != null) {
				sOSLocation.setSpeed(boxMessage.getSpeed());
			}
			if (boxMessage.getDirection() != null) {
				sOSLocation.setDirection(boxMessage.getDirection());
			}
			if (boxMessage.getMsgType() != null) {
				sOSLocation.setLocationSource(boxMessage.getMsgType());
			}
			sOSLocation.setUpdateTime(Calendar.getInstance());
			sOSLocationService.saveOrUpdate(sOSLocation);
		}
	}

	// 监控平台企业用户下发用户消息
	private void boxTransToEntUserBoxMessage(
			UserSendBoxMessage userSendboxMessage) {
		// TODO Auto-generated method stub
		String username = "";
		if (userSendboxMessage.getFromUser() != null
				&& userSendboxMessage.getFromUser().getUsername() != null
				&& !userSendboxMessage.getFromUser().getUsername().equals("")) {
			username = userSendboxMessage.getFromUser().getUsername();
		}
		// TODO Auto-generated method stub
		fordebug.debug("收到监控平台企业用户下发给盒子的消息，详情：发送者：" + username + " 目标盒子："
				+ userSendboxMessage.getBoxSerialNumber());
		userSendboxMessage.setDataStatusType(DataStatusType.SUCCESS);
		userSendboxMessage.setSendTime(Calendar.getInstance());
		// 保存消息
		userSendBoxMessageService.saveUserSendboxMessage(userSendboxMessage);
		controlBd.sendEntUserBoxMessage(userSendboxMessage);

	}

	// 监控平台企业用户下发盒子消息
	private void boxTransToEntUserBoxMessage(BoxMessage boxMessage) {
		// TODO Auto-generated method stub
		fordebug.debug("收到手机到盒子的消息，详情：发送者 手机：" + boxMessage.getFamilyMob()
				+ " 接收者 盒子ID：" + boxMessage.getToBox().getBoxSerialNumber()
				+ " 北斗卡号：" + boxMessage.getToBox().getCardNumber());
		// 添加到消息列表
		boxMessage.setDataStatusType(DataStatusType.SUCCESS);
		boxMessageService.saveBoxMessage(boxMessage);
		UserSendBoxMessage userSendboxMessage=new UserSendBoxMessage();
		userSendboxMessage.setBoxSerialNumber(boxMessage.getToBox().getBoxSerialNumber());
		userSendboxMessage.setContent(boxMessage.getContent());
		userSendboxMessage.setCreatedTime(Calendar.getInstance());
		userSendboxMessage.setDataStatusType(boxMessage.getDataStatusType());
		List<EnterpriseUserDto> lt = enterpriseUserService.listEnts(null, null, null, boxMessage.getFamilyMob(), null, null, null, null, 1, 1);
		if(lt.size()>0){
			EnterpriseUserDto enterpriseUserDto=lt.get(0);
			EnterpriseUser entUser=new EnterpriseUser();
			entUser.setId(enterpriseUserDto.getId());
			userSendboxMessage.setEntUser(entUser);
			userSendboxMessage.setMsgId(userSendboxMessage.getId());
			userSendboxMessage.setIsRead(false);
			userSendboxMessage.setIsReceived(false);
			userSendboxMessage.setMsgIoType(MsgIoType.IO_EntUserTOBOX);
			userSendboxMessage.setSendTime(Calendar.getInstance());
			userSendBoxMessageService.saveUserSendboxMessage(userSendboxMessage);
		}
		// 加入盒子是否活跃状态
		controlBd.sendBdBoxMessage(boxMessage);
	}
	
	// 监控平台企业用户下发自定义16进制用户消息
	private void boxTransToEntUserFreeBoxMessage(
			UserSendBoxMessage userSendboxMessage) {
		// TODO Auto-generated method stub
		String username = "";
		if (userSendboxMessage.getFromUser() != null
				&& userSendboxMessage.getFromUser().getUsername() != null
				&& !userSendboxMessage.getFromUser().getUsername().equals("")) {
			username = userSendboxMessage.getFromUser().getUsername();
		}
		// TODO Auto-generated method stub
		fordebug.debug("收到监控平台企业用户下发给盒子的消息，详情：发送者：" + username + " 目标盒子："
				+ userSendboxMessage.getBoxSerialNumber());
		userSendboxMessage.setDataStatusType(DataStatusType.SUCCESS);
		userSendboxMessage.setSendTime(Calendar.getInstance());
		// 保存消息
		userSendBoxMessageService.saveUserSendboxMessage(userSendboxMessage);
		controlBd.sendEntUserFreeBoxMessage(userSendboxMessage);

	}
	

	private void sendNotify(Object object) {
		// 确保线程安全，此处如果不加锁，并发高时会导致消息丢失
		synchronized (this) {
			setChanged();
			notifyObservers(object);
		}
	}

}
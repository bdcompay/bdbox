package com.bdbox.biz;

import java.util.Observable;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bdbox.dto.BoxLocationDto;
import com.bdbox.dto.BoxMessageDto;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxLocation;
import com.bdbox.entity.BoxMessage;
import com.bdbox.entity.EnterpriseUser;
import com.bdsdk.util.BeansUtil;
import com.taobao.metamorphosis.client.MetaMessageSessionFactory;
import com.taobao.metamorphosis.client.extension.spring.JavaSerializationMessageBodyConverter;
import com.taobao.metamorphosis.client.extension.spring.MessageBuilder;
import com.taobao.metamorphosis.client.extension.spring.MetaqTemplate;
import com.taobao.metamorphosis.client.producer.SendResult;

/**
 * HeziDSI网络数据转发业务
 * 
 * @author zhiwen.zou
 */
@Component
public class ObserverDsi extends ObserverBaseImpl implements ObserverBase {
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");
	private static Logger fordebug = Logger.getLogger("fordebug");
	@Autowired
	@Qualifier("metaqTemplate")
	private MetaqTemplate metaqTemplate;
	@Qualifier("zkSessionFactory")
	private MetaMessageSessionFactory zkSessionFactory;
	@Qualifier("messageBodyConverter")
	private JavaSerializationMessageBodyConverter messageBodyConverter;

	@Value("${metaq.sendtopics}")
	private String sendtopics;

	@PostConstruct
	public void init() {
		// initMessageListener();
		forinfo.info("成功启动DSI服务");
		this.start();// 启动观察者模式
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		super.update(o, arg);
	}

	@Override
	public void updateObserver(Observable o, Object arg) {
		// TODO Auto-generated method stub
		super.updateObserver(o, arg);
		if (arg.getClass() == BoxLocation.class) {
			BoxLocation boxLocation = (BoxLocation) arg;

			sendBoxLocation(boxLocation);
		} else if (arg.getClass() == BoxMessage.class) {
			BoxMessage boxMessage = (BoxMessage) arg;

			sendBoxMessage(boxMessage);
		}
	}

	/**
	 * 转发北斗RD短报文给企业用户
	 * 
	 * @param cardMessage
	 */
	private void sendBoxMessage(BoxMessage boxMessage) {
		BoxMessageDto boxMessageDto = new BoxMessageDto();
		BeansUtil.copyBean(boxMessageDto, boxMessage, true);
		try {

			Box frombox = boxMessage.getFromBox();
			Box tobox = boxMessage.getToBox();
			if (frombox != null) {
				boxMessageDto.setFromBoxSerialNumber(frombox.getBoxSerialNumber());
				boxMessageDto.setFromBoxCardNumber(frombox.getCardNumber() == null ? "" : frombox.getCardNumber());
			}
			if (tobox != null) {
				boxMessageDto.setToBoxSerialNumber(tobox.getBoxSerialNumber());
				boxMessageDto.setToBoxCardNumber(tobox.getCardNumber() == null ? "" : tobox.getCardNumber());
			}
			boxMessageDto.setDataStatusType(boxMessage.getDataStatusType().str());
			boxMessageDto.setMsgIoType(boxMessage.getMsgIoType().str());
			boxMessageDto.setMsgType(boxMessage.getMsgType().str());

//			// 发送给指挥调度系统
//			sendMessage(boxMessageDto, "CdoIgxO484");
//			sendMessage(boxMessageDto, "uT0q7qb0137");
//			// 发往驴讯通
//			sendMessage(boxMessageDto, "b41YbvIn138");
//			// 发往驴讯通测试
//			sendMessage(boxMessageDto, "kSb08RUQ139");
			////转发其他业务系统
			sendtoOthersystem(boxMessageDto,sendtopics);

			Long fromUserId = frombox == null ? null
					: frombox.getEntUser() == null ? null : frombox.getEntUser().getId();
			Long toUserId = tobox == null ? null : tobox.getEntUser() == null ? null : tobox.getEntUser().getId();
			if (fromUserId == null && toUserId == null) {
				return;
			}
			// 当该回执发送卡和接收卡属同一用户时，只转发一次即可
			if (fromUserId == toUserId) {
				EnterpriseUser fromEntUser = frombox.getEntUser();
				if (fromEntUser != null && fromEntUser.getIsDsiEnable() != null && fromEntUser.getIsDsiEnable()) {
					sendMessage(boxMessageDto, fromEntUser.getUserPowerKey());
				}
			} else {
				// 当回执发送卡和接受卡所属用户不一致时，分开转发。
				if (fromUserId != null) {
					EnterpriseUser fromEntUser = frombox.getEntUser();
					if (fromEntUser != null && fromEntUser.getIsDsiEnable() != null && fromEntUser.getIsDsiEnable()) {
						sendMessage(boxMessageDto, fromEntUser.getUserPowerKey());
					}
				}
				if (toUserId != null) {
					EnterpriseUser toEntUser = tobox.getEntUser();
					if (toEntUser != null && toEntUser.getIsDsiEnable() != null && toEntUser.getIsDsiEnable()) {
						sendMessage(boxMessageDto, toEntUser.getUserPowerKey());
					}
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 转发北斗定位给企业用户
	 * 
	 * @param cardLocation
	 */
	private void sendBoxLocation(BoxLocation boxLocation) {
		BoxLocationDto boxLocationDto = new BoxLocationDto();
		BeansUtil.copyBean(boxLocationDto, boxLocation, true);
		boxLocationDto.setBoxSerialNumber(boxLocation.getBox().getBoxSerialNumber());
		boxLocationDto
				.setSourceType(boxLocation.getLocationSource() == null ? "" : boxLocation.getLocationSource().str());
		boxLocationDto.setCardNumber(boxLocation.getBox().getCardNumber());
		try {
			//转发其他业务系统
			sendtoOthersystem(boxLocationDto,sendtopics);
			
			
			Box frombox = boxLocation.getBox();

			if (frombox.getEntUser() != null) {
				EnterpriseUser entUser = frombox.getEntUser();
				// if (user != null && user.getIsDsiEnable() &&
				// user.getIsEnable()) {
				if (entUser != null && entUser.getIsDsiEnable()) {
					sendMessage(boxLocationDto, entUser.getUserPowerKey());
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			forerror.error(e.getMessage());
		}
	}

	// 发给其他系统
	private <T> void sendtoOthersystem(T t,String sendtopics) {
		sendtopics = sendtopics.replace("，", ",");
		String[] topics = sendtopics.split(",");
		if (topics != null && topics.length > 0) {
			for (int i = 0; i < topics.length; i++) {
				String s = topics[i];
				try {
					sendMessage(t,s);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private <T> void sendMessage(T t, String userPowerKey) throws InterruptedException {
		final SendResult sendResult = metaqTemplate.send(MessageBuilder.withTopic(userPowerKey).withBody(t));
		if (!sendResult.isSuccess()) {
			forerror.error("转发消息失败...目标用户：" + userPowerKey);
		} else {

		}
	}
}

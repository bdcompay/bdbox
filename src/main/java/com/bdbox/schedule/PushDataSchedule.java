package com.bdbox.schedule;
/**
 * 定时任务，用于检查推送结果数组GlobalVar.PushDataList是否有结果，有结果就更新数据
 * @author dezhi.zhang
 * @createTime 2017/01/23
 * 
 */
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bdbox.consts.GlobalVar;
import com.bdbox.entity.EntUserPushLocation;
import com.bdbox.entity.EntUserPushMessage;
import com.bdbox.service.EntUserPushLocationService;
import com.bdbox.service.EntUserPushMessageService;
import com.bdbox.util.LogUtils;

@Component
public class PushDataSchedule {
	@Autowired
	private EntUserPushLocationService localService;
	@Autowired
	private EntUserPushMessageService messageService;
	
	//每1秒执行一次
	@Scheduled(cron="0/1 * * * * ? ")
	public void doTask(){
		//更新线程返回的执行结果
		if(GlobalVar.PushDataList!=null){
			for(Future<Object> obj:GlobalVar.PushDataList){
				//判断是否返回了结果
				try {
					//判断是否执行完成
					if(obj.isDone()){
						//位置信息的推送结果
						if(EntUserPushLocation.class==obj.get().getClass()){
							EntUserPushLocation entUserPushLocation=(EntUserPushLocation)obj.get();
							localService.update(entUserPushLocation);
						}
						//通讯信息的推送结果
						if(EntUserPushMessage.class==obj.get().getClass()){
							EntUserPushMessage entUserPushMessage=(EntUserPushMessage)obj.get();
							messageService.update(entUserPushMessage);
						}
						//记录错误信息
						if(String.class==obj.get().getClass()){
							LogUtils.loginfo("params error:"+(String)obj.get());
						}
						//更新完数据，将结果从数组中移除
						GlobalVar.PushDataList.remove(obj);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}

package com.bdbox.notification;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value; 
import org.springframework.stereotype.Component;

import com.bdbox.biz.MessageControl;
import com.bdbox.control.ControlSms;
import com.bdbox.service.FamilyStatusService;
import com.bdbox.service.UserService;
import com.bdbox.util.MailUtil;
@Component
public class MailNotification { 
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo"); 
	@Value("${email.emailImap}")
	private String emailImap;
	@Value("${email.emailSmtp}")
	private String emailSmtp;
	@Value("${email.emailUsername}")
	private String emailUsername;
	@Value("${email.emailPassword}")
	private String emailPassword;
	@Autowired
	private FamilyStatusService familyStatusService;
	@Autowired
	private UserService userService;
	private static Integer mailToBdmsgId = 1;
	@Autowired
	private ControlSms controlSms;

	@Autowired
	private MessageControl messageControl;
    
	//发送邮件
	public void sendMailErrorMessage(String toAddress, String subject,
			String content) {
		MailUtil.sendMail(emailSmtp, emailUsername, emailUsername,
				emailPassword, toAddress, subject, content);
	}  
	//发送短信 
	public void sendNote(String telephone,String content){
		boolean flag=controlSms.sendMessage(telephone, content, "SMS_60465021");
	}
	
	
	
	
}

package com.bdbox.express.shunfeng.api;

import com.bdbox.util.UtilDate;
import com.sf.openapi.common.entity.AppInfo;
import com.sf.openapi.common.entity.HeadMessageReq;
import com.sf.openapi.common.entity.MessageReq;
import com.sf.openapi.common.entity.MessageResp;
import com.sf.openapi.express.sample.security.dto.TokenReqDto;
import com.sf.openapi.express.sample.security.dto.TokenRespDto;
import com.sf.openapi.express.sample.security.tools.SecurityTools;

/**
 * AccessToken工具类
 * @author hax
 *
 */
public class GetAccessToken {

	private static AppInfoConfig appInfoConfig;
	
	public static AppInfoConfig getAppInfoConfig() {
		return appInfoConfig;
	}

	public static void setAppInfoConfig(AppInfoConfig appInfoConfig) {
		GetAccessToken.appInfoConfig = appInfoConfig;
	}

	/**
	 * 获取AccessToken
	 * @return	
	 */
	public static TokenRespDto getAccessToken(){
		//应用编号
		String appId = appInfoConfig.getAppId();
		//应用服务安全key
		String appKey = appInfoConfig.getAppKey();
		//请求url
		String url = "https://open-prod.sf-express.com/public/v1.0/security/access_token/"
				+ "sf_appid/"+appId+"/sf_appkey/"+appKey+"";
		
		AppInfo info = new AppInfo();
		HeadMessageReq hrq = new HeadMessageReq();
		MessageReq<TokenReqDto> req = new MessageReq<>();
		
		info.setAppId(appId);
		info.setAppKey(appKey);
		//交易类型
		hrq.setTransType(301);
		//流水号
		hrq.setTransMessageId(UtilDate.getDate()+UtilDate.getTen());
		//设置头信息
		req.setHead(hrq);
		try {
			MessageResp<TokenRespDto> apply = SecurityTools.applyAccessToken(url, info, req);
			return apply.getBody();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

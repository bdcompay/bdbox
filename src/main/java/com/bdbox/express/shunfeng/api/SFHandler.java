package com.bdbox.express.shunfeng.api;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.bdbox.entity.Address;
import com.bdbox.entity.Invoice;
import com.bdbox.entity.Order;
import com.bdbox.entity.Recipients;
import com.bdbox.util.Base64ImageTools;
import com.bdbox.util.LogUtils;
import com.bdbox.util.UtilDate;
import com.sf.openapi.common.entity.AppInfo;
import com.sf.openapi.common.entity.HeadMessageReq;
import com.sf.openapi.common.entity.MessageReq;
import com.sf.openapi.common.entity.MessageResp;
import com.sf.openapi.express.sample.order.dto.CargoInfoDto;
import com.sf.openapi.express.sample.order.dto.DeliverConsigneeInfoDto;
import com.sf.openapi.express.sample.order.dto.OrderFilterReqDto;
import com.sf.openapi.express.sample.order.dto.OrderFilterRespDto;
import com.sf.openapi.express.sample.order.dto.OrderQueryReqDto;
import com.sf.openapi.express.sample.order.dto.OrderQueryRespDto;
import com.sf.openapi.express.sample.order.dto.OrderReqDto;
import com.sf.openapi.express.sample.order.dto.OrderRespDto;
import com.sf.openapi.express.sample.order.tools.OrderTools;
import com.sf.openapi.express.sample.route.dto.RouteReqDto;
import com.sf.openapi.express.sample.route.dto.RouteRespDto;
import com.sf.openapi.express.sample.route.tools.RouteTools;
import com.sf.openapi.express.sample.security.dto.TokenRespDto;
import com.sf.openapi.express.sample.waybill.dto.WaybillReqDto;
import com.sf.openapi.express.sample.waybill.dto.WaybillRespDto;
import com.sf.openapi.express.sample.waybill.tools.WaybillDownloadTools;

/**
 * 顺丰一系列操作:比如下单、查询
 * @author hax
 *
 */
public class SFHandler {
	
	private static AppInfoConfig appInfoConfig;
	
	public static AppInfoConfig getAppInfoConfig() {
		return appInfoConfig;
	}

	public static void setAppInfoConfig(AppInfoConfig appInfoConfig) {
		SFHandler.appInfoConfig = appInfoConfig;
	}
	
	// 授权类API的访问令牌
	public static TokenRespDto trd = GetAccessToken.getAccessToken();

	/**
	 * 顺丰快速下单测试
	 * @return 订单结果
	 * @throws Exception
	 */
	public static OrderQueryRespDto downOrder(Order order, Address address, Recipients recipients,Invoice invoice) throws Exception{
		String url = "https://open-prod.sf-express.com/rest/v1.0/order/access_token/"+trd.getAccessToken()+"" + 
				"/sf_appid/"+appInfoConfig.getAppId()+"/sf_appkey/"+appInfoConfig.getAppKey()+"";
		//基本信息配置
		AppInfo appInfo = new AppInfo();
		appInfo.setAppId(appInfoConfig.getAppId());
		appInfo.setAppKey(appInfoConfig.getAppKey());
		appInfo.setAccessToken(trd.getAccessToken());
		
		MessageReq<OrderReqDto> req = new MessageReq<OrderReqDto>();
		HeadMessageReq head = new HeadMessageReq();
		//交易类型
		head.setTransType(200);
		//流水号
		head.setTransMessageId(UtilDate.getDate()+UtilDate.getTen());
		req.setHead(head);
		
		//快递订单信息配置
		OrderReqDto orderReqDto = new OrderReqDto();
		//订单号
		orderReqDto.setOrderId(UtilDate.getOrderNum());
		//快件产品类别
		orderReqDto.setExpressType(new Short("2"));
		//付款方式
		orderReqDto.setPayMethod(new Short("1"));
		//是否下call（通知收派员上门取件）
		orderReqDto.setIsDoCall(new Short("0"));
		//是否需要签回单号
		orderReqDto.setNeedReturnTrackingNo(new Short("0"));
		//是否申请运单号
		orderReqDto.setIsGenBillNo(new Short("1"));
		//顺丰月结卡号 10位数字
		orderReqDto.setCustId("0206045563");
		
		//寄件方信息
		DeliverConsigneeInfoDto deliverInfoDto = new DeliverConsigneeInfoDto();
		//详细地址信息
		deliverInfoDto.setAddress(recipients.getProvince()+recipients.getCity()+recipients.getAddress());
		//寄件方所属城市名称，必须是标准的城市称谓
		deliverInfoDto.setCity(recipients.getCity());
		//寄件方公司名称
		deliverInfoDto.setCompany(recipients.getCompany());
		//寄件方联系人
		deliverInfoDto.setContact(recipients.getContact());
		//所属国家
		deliverInfoDto.setCountry("中国");
		//寄件方所在省份，必须是标准的省名称谓
		deliverInfoDto.setProvince(recipients.getProvince());
		//寄件方邮编代码
		deliverInfoDto.setShipperCode(recipients.getAreaCode()==null?recipients.getShipperCode():recipients.getAreaCode());
		//寄件方联系电话
		deliverInfoDto.setTel(recipients.getTel() == null ? recipients.getMobile() : recipients.getTel());
		//寄件方手机
		//deliverInfoDto.setMobile(recipients.getMobile());
		
		//收件方信息
		DeliverConsigneeInfoDto consigneeInfoDto = new DeliverConsigneeInfoDto();
		//consigneeInfoDto.setAddress(address.getCounty()+address.getDetailedAddress());
		//consigneeInfoDto.setCity(address.getCity());
		//consigneeInfoDto.setCompany(address.getName());
		//consigneeInfoDto.setContact(address.getName());
		//consigneeInfoDto.setProvince(address.getProvince());
		//consigneeInfoDto.setShipperCode(address.getPostCode());
		//consigneeInfoDto.setTel(address.getMob());
		//收件信息从订单中获取
		String site=order.getSite();
		String[] sites=null;
		
		if(site.contains("邮编：")||site.contains("邮编:")){
			site=site.replace(":", "：");
			sites=site.split("邮编：");
			site=sites[0];
			if(sites.length>1){
				consigneeInfoDto.setShipperCode(order.getAreaCode()==null?sites[1]:order.getAreaCode());
			}else{
//				consigneeInfoDto.setAddress(site);
			}
		}
		//取出省市
		String site1=site.replace("省", "省#").replace("区", "区#").replace("市", "市#").replace("洲", "洲#");
		String[] sites1=site1.split("#");
		String siteAddress=sites1[2];
		for(int i=3;i<sites1.length;i++){
			siteAddress=siteAddress+sites1[i];
		}
		
		
		consigneeInfoDto.setContact(order.getName());
		consigneeInfoDto.setTel(order.getPhone().toString());
		consigneeInfoDto.setProvince(sites1[0]);
		consigneeInfoDto.setCity(sites1[1]);
		consigneeInfoDto.setAddress(siteAddress);
		
		if(invoice!=null){
		consigneeInfoDto.setCompany(invoice.getCompanyName());
		}else{
			consigneeInfoDto.setCompany(order.getName());
		}
		
		//商品信息
		CargoInfoDto cargoInfoDto = new CargoInfoDto();
		//货物名称
		cargoInfoDto.setCargo(order.getProductName());
		
		orderReqDto.setDeliverInfo(deliverInfoDto);
		orderReqDto.setConsigneeInfo(consigneeInfoDto);
		orderReqDto.setCargoInfo(cargoInfoDto);
		
		req.setBody(orderReqDto);
		
		MessageResp<OrderRespDto> response = OrderTools.order(url, appInfo, req);
		
		String code = response.getHead().getCode();
		//判断访问令牌是否可用
		if(isUse(code)){
			LogUtils.loginfo(response.getHead().toString());
			//重新获取访问令牌
			trd = GetAccessToken.getAccessToken();
			//回调
			downOrder(order, address, recipients,invoice);
		} else if("EX_CODE_OPENAPI_0200".equals(code)){
			LogUtils.loginfo(response.getHead().toString());
			//调用查询信息方法、并返回查询信息
			return queryOrder(response.getBody().getOrderId());
		}
		LogUtils.loginfo(response.getHead().toString());
		return null;
	}
	
	/**
	 * 订单信息查询
	 * @throws Exception 
	 */
	public static OrderQueryRespDto queryOrder(String orderId) throws Exception{
		String url = "https://open-prod.sf-express.com/rest/v1.0/order/query/access_token/"+trd.getAccessToken()+"/" + 
				"sf_appid/"+appInfoConfig.getAppId()+"/sf_appkey/"+appInfoConfig.getAppKey()+"";
		AppInfo appInfo = new AppInfo();
		appInfo.setAppId(appInfoConfig.getAppId());
		appInfo.setAppKey(appInfoConfig.getAppKey());
		appInfo.setAccessToken(trd.getAccessToken());

		MessageReq<OrderQueryReqDto> req = new MessageReq<OrderQueryReqDto>();
		HeadMessageReq head = new HeadMessageReq();
		//交易类型
		head.setTransType(203);
		//流水号
		head.setTransMessageId(UtilDate.getDate()+UtilDate.getTen());
		req.setHead(head);

		OrderQueryReqDto orderQueryReqDto = new OrderQueryReqDto();
		//将orderId添加body
		orderQueryReqDto.setOrderId(orderId);
		//设置body信息
		req.setBody(orderQueryReqDto);

		MessageResp<OrderQueryRespDto> response = OrderTools.orderQuery(url, appInfo, req);
		//返回查询信息
		return response.getBody();
	}
	
	/**
	 * 路由查询
	 * @param mailNo 物流单号
	 * @throws Exception 
	 */
	public static List<RouteRespDto> selectRoute(String mailNo) throws Exception{
		String url = "https://open-prod.sf-express.com/rest/v1.0/route/query/access_token/"+trd.getAccessToken()+"/" + 
				"sf_appid/"+appInfoConfig.getAppId()+"/sf_appkey/"+appInfoConfig.getAppKey()+"";
		AppInfo appInfo = new AppInfo();
		appInfo.setAppId(appInfoConfig.getAppId());
		appInfo.setAppKey(appInfoConfig.getAppKey());
		appInfo.setAccessToken(trd.getAccessToken());

		MessageReq<RouteReqDto> req = new MessageReq<RouteReqDto>();
		HeadMessageReq head = new HeadMessageReq();
		//交易类型
		head.setTransType(501);
		//流水号
		head.setTransMessageId(UtilDate.getDate()+UtilDate.getTen());
		req.setHead(head);

		RouteReqDto routeReqDto = new RouteReqDto();
		//查询类别
		routeReqDto.setMethodType(1);
		//查询方法选择
		routeReqDto.setTrackingType(1);
		//物流单号
		routeReqDto.setTrackingNumber(mailNo);

		req.setBody(routeReqDto);

		MessageResp<List<RouteRespDto>> response = RouteTools.routeQuery(url, appInfo, req);
		//判断token是否可用
		if(isUse(response.getHead().getCode())){
			LogUtils.loginfo(response.getHead().toString());
			//重新获取token
			trd = GetAccessToken.getAccessToken();
			//回调
			selectRoute(mailNo);
		} else if("EX_CODE_OPENAPI_0200".equals(response.getHead().getCode())){
			LogUtils.loginfo(response.getHead().toString());
			//返回物流信息
			return response.getBody();
		}
		LogUtils.loginfo(response.getHead().toString());
		return null;
	}
	
	/**
	 * 转换路由信息格式
	 * @param list
	 */
	public static String formatStr(List<RouteRespDto> list){

		//定义一个路由信息数组
		RouteRespDto[] rrds = new RouteRespDto[list.size()];
		//获取数组长度
		int rrdLenth = rrds.length;
		//将路由信息顺序调换
		for (RouteRespDto routeRespDto : list) {
			rrds[rrdLenth-1] = routeRespDto;
			rrdLenth--;
		}
		//拼接路由信息
		StringBuilder sb = new StringBuilder("express:[");
		for (RouteRespDto rrd : rrds) {
			sb.append("{\'content\':\'"+rrd.getRemark()+"\',\'time\':\'"+rrd.getAcceptTime()+"\'},");
		}
		//去除最后一个逗号
		sb.substring(0, sb.length()-1);
		sb.append("]");
		return sb.toString();
	}
	
	/**
	 * 订单刷选
	 * @throws Exception
	 */
	public void orderFilter() throws Exception{
		String url = "https://open-prod.sf-express.com/rest/v1.0/filter/access_token/"+trd.getAccessToken()+"" + 
				"/sf_appid/"+appInfoConfig.getAppId()+"/sf_appkey/"+appInfoConfig.getAppKey()+"";
		AppInfo appInfo = new AppInfo();
		appInfo.setAppId(appInfoConfig.getAppId());
		appInfo.setAppKey(appInfoConfig.getAppKey());
		appInfo.setAccessToken(trd.getAccessToken());

		MessageReq<OrderFilterReqDto> req = new MessageReq<OrderFilterReqDto>();
		HeadMessageReq head = new HeadMessageReq();
		head.setTransType(204);
		head.setTransMessageId(UtilDate.getDate()+UtilDate.getTen());
		req.setHead(head);

		OrderFilterReqDto orderFilterReqDto = new OrderFilterReqDto();
		orderFilterReqDto.setFilterType(1);
		orderFilterReqDto.setConsigneeCountry("中国");
		orderFilterReqDto.setConsigneeProvince("广东省");
		orderFilterReqDto.setConsigneeCity("深圳市");
		orderFilterReqDto.setConsigneeCounty("南山区");
		orderFilterReqDto.setConsigneeAddress("南海大道3688号");
		orderFilterReqDto.setConsigneeTel("26536114");

		orderFilterReqDto.setDeliverCountry("中国");
		orderFilterReqDto.setDeliverProvince("湖北省");
		orderFilterReqDto.setDeliverCity("武汉市");
		orderFilterReqDto.setDeliverCounty("武昌区");
		orderFilterReqDto.setDeliverAddress("八一路299号");
		orderFilterReqDto.setDeliverTel("74512587");

		req.setBody(orderFilterReqDto);

		MessageResp<OrderFilterRespDto> response = OrderTools.filterOrder(url, appInfo, req);
		System.out.println("======================");
		System.out.println(response.getHead());
	}
	
	/**
	 * 电子运单下载
	 * @throws Exception
	 */
	public static String waybill(String orderId, HttpServletRequest request) throws Exception{
		String url = "https://open-prod.sf-express.com/rest/v1.0/waybill/image/access_token/"+trd.getAccessToken()+"/" + 
				"sf_appid/"+appInfoConfig.getAppId()+"/sf_appkey/"+appInfoConfig.getAppKey()+"";
		AppInfo appInfo = new AppInfo();
		appInfo.setAppId(appInfoConfig.getAppId());
		appInfo.setAppKey(appInfoConfig.getAppKey());
		appInfo.setAccessToken(trd.getAccessToken());

		MessageReq<WaybillReqDto> req = new MessageReq<WaybillReqDto>();
		WaybillReqDto waybillReqDto = new WaybillReqDto();
		HeadMessageReq head = new HeadMessageReq();
		//交易类型
		head.setTransType(205);
		//流水号
		head.setTransMessageId(UtilDate.getDate()+UtilDate.getTen());
		//将orderId添加进body
		waybillReqDto.setOrderId(orderId);
		//设置头信息
		req.setHead(head);
		//设置body信息
		req.setBody(waybillReqDto);

		//执行
		MessageResp<WaybillRespDto> response = WaybillDownloadTools.waybillDownload(url, appInfo, req);
		//判断token是否可用
		if(isUse(response.getHead().getCode())){
			LogUtils.loginfo(response.getHead().toString());
			//重新获取token
			trd = GetAccessToken.getAccessToken();
			//回调
			waybill(orderId, request);
		} else if("EX_CODE_OPENAPI_0200".equals(response.getHead().getCode())){
			LogUtils.loginfo(response.getHead().toString());
			//获取图片的字符数组
			String[] ss = response.getBody().getImages();
			String imageStr = "";
			for (String string : ss) {
				imageStr += string;
			}
			String path = request.getSession().getServletContext().getRealPath("/sfwayBill");
			File file = new File(path);
			//判断是否有该文件夹、否则创建该文件夹
			if(!file.exists()){
				file.mkdir();
			}
			//生成jpg图片
			String imgUrl= path+"\\sf"+orderId+".jpg";
			Base64ImageTools.GenerateImage(imageStr,imgUrl);
			return imgUrl.substring(imgUrl.length()-30);
		}
		LogUtils.loginfo(response.getHead().toString());
		return null;
	}
	
	/**
	 * 判断访问令牌是否还可使用
	 * @param code
	 * @return
	 */
	public static boolean isUse(String code){
		if("EX_CODE_OPENAPI_0103".equals(code) || "EX_CODE_OPENAPI_0104".equals(code)
				|| "EX_CODE_OPENAPI_0105".equals(code) || "EX_CODE_OPENAPI_0106".equals(code)){
			return true;
		}
		return false;
	}
	
}

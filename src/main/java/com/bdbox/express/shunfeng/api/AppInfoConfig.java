package com.bdbox.express.shunfeng.api;

/**
 * 顺丰基本参数配置
 * @author hax
 *
 */
public class AppInfoConfig {
	/**
	 * 应用编号
	 */
	private String appId;
	
	/**
	 * 应用服务安全key
	 */
	private String appKey;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}
}

package com.bdbox.alipay.config;

import java.util.HashMap;
import java.util.Map;

import com.bdbox.alipay.util.AlipaySubmit;

public class Alipay {
	
	private static AlipayConfig alipayConfig;

	public static AlipayConfig getAlipayConfig() {
		return alipayConfig;
	}

	public static void setAlipayConfig(AlipayConfig alipayConfig) {
		Alipay.alipayConfig = alipayConfig;
	}

	/**
	 * 生成支付宝订单
	 * @param out_trade_no 订单号
	 * @param subject 商品名称
	 * @param total_fee 交易金额
	 * @param showUrl 商品展示页面
	 * @return
	 */
	public static String alipaySubmit(String out_trade_no, String subject, String total_fee,
			String showUrl, String transactionType){
		//把请求参数打包成数组
		String[] urls = alipayConfig.getReturnUrl().split(",");
		String returnUrl = "";
		if("GENERAL".equals(transactionType)){
			returnUrl = urls[0];
		}else{
			returnUrl = urls[1];
		}
		total_fee = total_fee.replace(",", "");
		Map<String, String> sParaTemp = new HashMap<String, String>();
		sParaTemp.put("service", "create_direct_pay_by_user");
        sParaTemp.put("partner", alipayConfig.getPartner());
        sParaTemp.put("seller_email", alipayConfig.getSellerEmail());
        sParaTemp.put("_input_charset", alipayConfig.getInputCharset());
		sParaTemp.put("payment_type", alipayConfig.getPaymentType());
		sParaTemp.put("notify_url", alipayConfig.getNotifyUrl());
		sParaTemp.put("return_url", returnUrl);
		sParaTemp.put("out_trade_no", out_trade_no);
		sParaTemp.put("subject", subject);
		sParaTemp.put("total_fee", total_fee);
		sParaTemp.put("show_url", showUrl);
		
		//建立请求
		String sHtmlText = AlipaySubmit.buildRequest(sParaTemp,"get","确认");
		return sHtmlText;
	}
	
	/**
	 * 支付宝退款请求
	 * @param refund_date 退款当天日期
	 * 必填，格式：年[4位]-月[2位]-日[2位] 小时[2位 24小时制]:分[2位]:秒[2位]，如：2007-10-01 13:13:13
	 * @param batch_no 批次号
	 * 必填，格式：当天日期[8位]+序列号[3至24位]，如：201008010000001
	 * @param batch_num 退款笔数
	 * 必填，参数detail_data的值中，“#”字符出现的数量加1，最大支持1000笔（即“#”字符出现的数量999个）
	 * @param detail_data 退款详细数据
	 * 必填，格式：原付款支付宝交易号^退款总金额^退款理由
	 * @return
	 */
	public static String alipayRefundSubmit(String refund_date, String batch_no, String batch_num, String detail_data){
		//把请求参数打包成数组
		Map<String, String> sParaTemp = new HashMap<String, String>();
		sParaTemp.put("service", "refund_fastpay_by_platform_pwd");
        sParaTemp.put("partner", alipayConfig.getPartner());
        sParaTemp.put("_input_charset", alipayConfig.getInputCharset());
		sParaTemp.put("notify_url", alipayConfig.getNotifyRefundUrl());
		sParaTemp.put("seller_email", alipayConfig.getSellerEmail());
		sParaTemp.put("refund_date", refund_date);
		sParaTemp.put("batch_no", batch_no);
		sParaTemp.put("batch_num", batch_num);
		sParaTemp.put("detail_data", detail_data);
		
		//建立请求
		String sHtmlText = AlipaySubmit.buildRequest(sParaTemp,"get","确认");
		return sHtmlText;
	}
	
	/**
	 * 生成支付宝订单-手机网站支付
	 * @param out_trade_no 订单号
	 * @param subject 商品名称
	 * @param total_fee 交易金额
	 * @param showUrl 商品展示页面
	 * @return
	 */
	public static String alipayAppSubmit(String out_trade_no, String subject, String total_fee,
			String showUrl, String transactionType){
		//把请求参数打包成数组
		String returnUrl = "";
		if("GENERAL".equals(transactionType)){
			returnUrl = "http://61.140.233.131:8080/bdhezi/mob/module/my/buy_order.html";
		}else{
			returnUrl = "http://61.140.233.131:8080/bdhezi/mob/module/my/tenancy_order.html";
		}
		total_fee = total_fee.replace(",", "");
		Map<String, String> sParaTemp = new HashMap<String, String>();
		sParaTemp.put("service", "alipay.wap.create.direct.pay.by.user");
        sParaTemp.put("partner", alipayConfig.getPartner());
        sParaTemp.put("seller_id", alipayConfig.getPartner());
        sParaTemp.put("_input_charset", alipayConfig.getInputCharset());
		sParaTemp.put("payment_type", alipayConfig.getPaymentType());
		sParaTemp.put("notify_url", alipayConfig.getNotifyUrl());
		sParaTemp.put("return_url", returnUrl);
		sParaTemp.put("out_trade_no", out_trade_no);
		sParaTemp.put("subject", subject);
		sParaTemp.put("total_fee", total_fee);
		sParaTemp.put("show_url", showUrl);
		//启用此参数可唤起钱包APP支付。
		sParaTemp.put("app_pay","Y");
		
		//建立请求
		String sHtmlText = AlipaySubmit.buildRequest(sParaTemp,"get","确认");
		return sHtmlText;
	}
}

package com.bdbox.alipay.config;
/**
 * 支付宝核心参数配置
 * @author hax
 *
 */
public class AlipayConfig {

	/**
	 * 合作身份者ID，以2088开头由16位纯数字组成的字符串
	 */
	private String partner;
	
	/**
	 * 收款支付宝账号，一般情况下收款账号就是签约账号
	 */
	private String sellerEmail;
	
	/**
	 * 商户的私钥
	 */
	private String key;
	
	/**
	 * 调试用，创建TXT日志文件夹路径
	 */
	private String logPath;
	
	/**
	 * 字符编码格式 目前支持 gbk 或 utf-8
	 */
	private String inputCharset;
	
	/**
	 * 签名方式 不需修改
	 */
	private String signType;
	
	/**
	 * 服务器异步通知页面路径
	 */
	private String notifyUrl;
	
	/**
	 * 服务器退款异步通知页面路径
	 */
	private String notifyRefundUrl;
	
	/**
	 * 页面跳转同步通知页面路径
	 */
	private String returnUrl;
	
	/**
	 * 支付类型
	 */
	private String paymentType;
	
	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public String getSellerEmail() {
		return sellerEmail;
	}

	public void setSellerEmail(String sellerEmail) {
		this.sellerEmail = sellerEmail;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getLogPath() {
		return logPath;
	}

	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}

	public String getInputCharset() {
		return inputCharset;
	}

	public void setInputCharset(String inputCharset) {
		this.inputCharset = inputCharset;
	}

	public String getSignType() {
		return signType;
	}

	public void setSignType(String signType) {
		this.signType = signType;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getNotifyRefundUrl() {
		return notifyRefundUrl;
	}

	public void setNotifyRefundUrl(String notifyRefundUrl) {
		this.notifyRefundUrl = notifyRefundUrl;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
}

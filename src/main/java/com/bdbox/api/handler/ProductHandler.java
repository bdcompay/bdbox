package com.bdbox.api.handler;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.api.dto.ProductDto;
import com.bdbox.constant.ProductBuyType;
import com.bdbox.constant.ProductType;
import com.bdbox.constant.Status;
import com.bdbox.entity.Activity;
import com.bdbox.entity.Product;
import com.bdbox.service.ActivityService;
import com.bdbox.service.ProductService;
import com.bdbox.util.LogUtils;
import com.bdbox.web.dto.ActivityDto;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.CommonMethod;
 
@Controller
public class ProductHandler {
	
	@Autowired
	private ProductService productService;
	@Autowired
	private ActivityService activityService;
	
	//增加
	@RequestMapping(value="addProduct.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult add(
							@RequestParam(required=false) String name,
							@RequestParam(required=false) String productType,
							@RequestParam(required=false) Double price,
							@RequestParam(required=false) int saleNumber,
							@RequestParam(required=false) int stockNumber,
							@RequestParam(required=false) String productBuyType,
							@RequestParam(required=false) boolean isOpenBuy,
							@RequestParam(required=false) String buyStartTimeStr,
							@RequestParam(required=false) String buyEndTimeStr,
							@RequestParam(required=false) String productUrl,
							@RequestParam(required=false) String color,
							@RequestParam(required=false) String explain,
							@RequestParam(required=false) Double rental,
							@RequestParam(required=false) Double antecedent,
							@RequestParam(required=false) Double couponPrice
							){
		ProductType productTypeEnum = ProductType.TERMINAL;
		ProductBuyType pBuyTypeEnum = ProductBuyType.GENERAL;
		if(productBuyType!=null && productBuyType.equals("SHOPPINGRUSH")){
			pBuyTypeEnum = ProductBuyType.SHOPPINGRUSH;
		}
		if(productBuyType!=null && productBuyType.equals("RENT")){
			pBuyTypeEnum = ProductBuyType.RENT;
		}
		Calendar buyStartTime = null;
		Calendar buyEndTime = null;
		try{
			if(buyStartTimeStr!=null && !buyStartTimeStr.equals("")){
				buyStartTime = CommonMethod.StringToCalendar(buyStartTimeStr, "yyyy-MM-dd HH:mm:ss");
			}
			if(buyEndTimeStr!=null && !buyEndTimeStr.equals("")){
				buyEndTime = CommonMethod.StringToCalendar(buyEndTimeStr, "yyyy-MM-dd HH:mm:ss");
			}
		}catch(Exception e){
			LogUtils.logerror("添加产品--字符串格式时间转Calendar时间异常", e);
		}
		
		Product product = productService.save(name, productTypeEnum, price, saleNumber, stockNumber,
				pBuyTypeEnum, isOpenBuy, buyStartTime, buyEndTime,productUrl,color, explain, rental, antecedent, couponPrice);
		
		ObjectResult obj = new ObjectResult();
		if(product==null){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("添加失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("添加成功");
		}
		return obj;
	}
	//删除
	@RequestMapping(value="deleteProduct.do")
	public @ResponseBody ObjectResult delete(Long id){
		ObjectResult obj = new ObjectResult();
		
		boolean b = productService.deleteProduct(id);
		
		if(!b){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("删除失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("删除成功");
		}
		obj.setResult(b);
		return obj;
	}
	//修改
	@RequestMapping(value="updateProduct.do")
	public @ResponseBody ObjectResult update(Long id,
			@RequestParam(required=false) String name,
			@RequestParam(required=false) String productType,
			@RequestParam(required=false) Double price,
			@RequestParam(required=false) int saleNumber,
			@RequestParam(required=false) int stockNumber,
			@RequestParam(required=false) String productBuyType,
			@RequestParam(required=false) boolean isOpenBuy,
			@RequestParam(required=false) String buyStartTimeStr,
			@RequestParam(required=false) String buyEndTimeStr,
			@RequestParam(required=false) String productUrl,
			@RequestParam(required=false) String color,
			@RequestParam(required=false) String explain,
			@RequestParam(required=false) Double updateRental,
			@RequestParam(required=false) Double updateAntecedent,
			@RequestParam(required=false) Double updateCouponPrice
			){
		ObjectResult obj = new ObjectResult();
		
		ProductType productTypeEnum = ProductType.TERMINAL;
		ProductBuyType pBuyTypeEnum = ProductBuyType.GENERAL;
		if(productBuyType!=null && productBuyType.equals("SHOPPINGRUSH")){
			pBuyTypeEnum = ProductBuyType.SHOPPINGRUSH;
		}
		if(productBuyType!=null && productBuyType.equals("RENT")){
			pBuyTypeEnum = ProductBuyType.RENT;
		}
		Calendar buyStartTime = null;
		Calendar buyEndTime = null;
		try{
			if(buyStartTimeStr!=null && !buyStartTimeStr.equals("")){
				buyStartTime = CommonMethod.StringToCalendar(buyStartTimeStr, "yyyy-MM-dd HH:mm:ss");
			}
			if(buyEndTimeStr!=null && !buyEndTimeStr.equals("")){
				buyEndTime = CommonMethod.StringToCalendar(buyEndTimeStr, "yyyy-MM-dd HH:mm:ss");
			}
		}catch(Exception e){
			LogUtils.logerror("修改产品信息--字符串格式时间转Calendar时间异常", e);
		}
		
		
		boolean b = productService.update(id, name, productTypeEnum, price, saleNumber, stockNumber,
				pBuyTypeEnum, isOpenBuy, buyStartTime, buyEndTime,productUrl,color, explain,
				updateRental, updateAntecedent, updateCouponPrice);
		
		if(!b){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("修改失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("修改成功");
		}
		return obj;
	}
	
	//查询单个
	@RequestMapping(value="getProduct.do")
	public @ResponseBody ObjectResult getProduct(Long id){
		ObjectResult objectResult = new ObjectResult();
		Product product = productService.get(id);
		ProductDto dto = productService.castFrom(product);
		if(product!=null){
			objectResult.setMessage("查询成功！");
			objectResult.setResult(dto);
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setMessage("查询失败");
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	@RequestMapping(value="getProduct1.do")
	public @ResponseBody ObjectResult getProducts(Long id){
		ObjectResult objectResult = new ObjectResult();
		Product product = productService.get(id);
		ProductDto dto = productService.castFrom(product);
		if(product!=null){
			objectResult.setMessage("查询成功！");
			objectResult.setResult(dto);
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setMessage("查询失败");
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	//查询多个
	@RequestMapping(value="listProduct.do")
	public @ResponseBody String listProduct(
			@RequestParam(required=false) String name,
			@RequestParam(required=false) String startDate,
			@RequestParam(required=false) String endDate,
			@RequestParam(required=false) String productType,
			@RequestParam(required=false) String productBuyType,
			int page,int pageSize
			){
		return null;
	}
	
	//符合jquery_datatable插件规范
	@RequestMapping(value="listProductDatatable.do")
	public @ResponseBody ListParam listProductDatatable(
			@RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
			@RequestParam int iColumns, @RequestParam String sEcho,
			@RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0,
			@RequestParam(required=false) String name,
			@RequestParam(required=false) String startDate,
			@RequestParam(required=false) String endDate,
			@RequestParam(required=false) String productType,
			@RequestParam(required=false) String productBuyType
			){
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		ProductType productTypeEnum = ProductType.TERMINAL;
		ProductBuyType pBuyTypeEnum = null;
		if(productBuyType!=null && productBuyType.equals("SHOPPINGRUSH")){
			pBuyTypeEnum = ProductBuyType.SHOPPINGRUSH;
		}else if(productBuyType!=null && productBuyType.equals("GENERAL")){
			pBuyTypeEnum = ProductBuyType.GENERAL;
		}else if(productBuyType!=null && productBuyType.equals("RENT")){
			pBuyTypeEnum = ProductBuyType.RENT;
		}
		
		if(name.equals("")){
			name = null;
		}
		
		Calendar startTime = null;
		Calendar endTime = null;
		try{
			if(startDate!=null && !startDate.equals("")){
				startTime = CommonMethod.StringToCalendar(startDate, "yyyy-MM-dd HH:mm:ss");
			}
			if(endDate!=null && !endDate.equals("")){
				endTime = CommonMethod.StringToCalendar(endDate, "yyyy-MM-dd HH:mm:ss");
			}
		}catch(Exception e){
			LogUtils.logwarn("String类型转Calendar异常", e);
		}
		
		List<Product> list = productService.listBy(name, productTypeEnum, pBuyTypeEnum, startTime, endTime, page, pageSize);
		List<ProductDto> dtos = new ArrayList<ProductDto>();
		for(Product p:list){
			ProductDto dto = productService.castFrom(p);
			dtos.add(dto);
		}
		int count = productService.count(name, productTypeEnum, pBuyTypeEnum, startTime, endTime);
		
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(dtos);
		return listParam;
	}
	
	//产品活动查询
		@RequestMapping(value = "activity.do")
		public @ResponseBody ListParam activity(
				@RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
				@RequestParam int iColumns, @RequestParam String sEcho,
				@RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0) throws ParseException {
			int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
			int pageSize = iDisplayLength;  
			List<ActivityDto> list = activityService.queryActivity(page, pageSize);
			int couont=activityService.counts();
	 		ListParam listParam = new ListParam();
			listParam.setiDisplayLength(iDisplayLength);
			listParam.setiDisplayStart(iDisplayStart);
			listParam.setiTotalDisplayRecords(couont);
			listParam.setiTotalRecords(couont);
	  		listParam.setsEcho(sEcho);
			listParam.setAaData(list);
			return listParam;
		}
		
		
		@RequestMapping(value = { "/getActivity.do" }, method = { RequestMethod.POST })
		@ResponseBody
		public ObjectResult getBox(@RequestParam(required = false) Long id ){
			ActivityDto dto=new ActivityDto();
			if ((id != null) && (!"".equals(id))){
				dto=activityService.getActivity(id);
			}
			System.out.println(dto.getId());
			System.out.println(dto.getStatus()); 
			ObjectResult objectResult = new ObjectResult(); 
	 		objectResult.setResult(dto);
			objectResult.setStatus(ResultStatus.OK); 
			return objectResult;
		}
		
		@RequestMapping(value = {"update_activity.do" }, method = { RequestMethod.POST })
		@ResponseBody
		public ObjectResult update_activity(@RequestParam(required = false) Long id,
				@RequestParam(required=false) String startTime, 
				@RequestParam(required=false) String endTime,
				@RequestParam(required=false) String status) throws ParseException{
			Activity activity=activityService.getActivitys(id);
			ObjectResult objectResult = new ObjectResult(); 
			boolean result=false; 
			Calendar strTime=null;
	 		if(startTime!=null&&!"".equals(startTime)){
	 			strTime = CommonMethod.StringToCalendar(startTime,"yyyy-MM-dd HH:mm:ss");
			} 
	 		Calendar enrTime=null;
			if(endTime!=null&&!"".equals(endTime)){
				enrTime = CommonMethod.StringToCalendar(endTime,"yyyy-MM-dd HH:mm:ss");
			} 
			if(activity!=null){
				if(status!=null&&status.equals("RUSH")) 
				   activity.setStatus(Status.RUSH);
				if(status!=null&&status.equals("ORDER"))
				   activity.setStatus(Status.ORDER);
				if(status!=null&&status.equals("COMMONBUY")){
				   activity.setStatus(Status.COMMONBUY);
				}
				System.out.println(strTime);
				activity.setStartTime(strTime);
				activity.setEndTime(enrTime);
				 result=activityService.updateActivity(activity);
			}
			if(result){
				objectResult.setMessage("修改成功");
				objectResult.setStatus(ResultStatus.OK);
			}else{
				objectResult.setMessage("修改失败");
				objectResult.setStatus(ResultStatus.FAILED); 
			}
			return objectResult;
			
		 
		}
		
		@RequestMapping("findActivity.do")
		@ResponseBody
		public ActivityDto findActivity(){
			List<ActivityDto> dtoList = activityService.findActivity();
			ActivityDto dto = null;
			for (ActivityDto activityDto : dtoList) {
				dto = activityDto;
			}
			return dto;
		}
		
		@RequestMapping("getStockNum.do")
		@ResponseBody
		public int getStockNum(){
			return productService.getStockNumber();
		}
		
		
		//修改
		@RequestMapping(value="updateWarehouseProduct.do")
		public @ResponseBody ObjectResult updateWarehouseProduct(Long id, 
				@RequestParam(required=false) String stocktype,
				@RequestParam(required=false) int storageNumberd){
			ObjectResult obj = new ObjectResult(); 
			Product product = productService.get(id);
			if(stocktype.equals("storage") && storageNumberd>0){
				product.setStockNumber(product.getStockNumber()+storageNumberd);
			}else if(stocktype.equals("getout") && storageNumberd>0){
				product.setStockNumber(product.getStockNumber()-storageNumberd);
			}else if(stocktype.equals("getout") && product.getStockNumber()<storageNumberd){
				obj.setStatus(ResultStatus.FAILED);
				obj.setMessage("取出数量不能大于库存数量");
				return obj;
			}else{
				obj.setStatus(ResultStatus.FAILED);
				obj.setMessage("参数异常");
				return obj;
			}
			
			boolean b = productService.updateProduct(product); 
			if(b){
				obj.setStatus(ResultStatus.OK);
				obj.setMessage("修改成功");
			}else{
				obj.setStatus(ResultStatus.FAILED);
				obj.setMessage("修改失败");
			}
			return obj;
		}
}

package com.bdbox.api.handler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.httpclient.methods.PostMethod;
import org.jdom.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.constant.BdcoodStatus;
import com.bdbox.constant.DealStatus;
import com.bdbox.constant.MailType;
import com.bdbox.constant.OrderType;
import com.bdbox.constant.ProductType;
import com.bdbox.constant.RefundStatus;
import com.bdbox.entity.BDCood;
import com.bdbox.entity.Invoice;
import com.bdbox.entity.Notification;
import com.bdbox.entity.Order;
import com.bdbox.entity.Product;
import com.bdbox.entity.Refund;
import com.bdbox.entity.User;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.AddressService;
import com.bdbox.service.BDCoodService;
import com.bdbox.service.InvoiceService;
import com.bdbox.service.MessageRecordService;
import com.bdbox.service.NotificationService;
import com.bdbox.service.OrderService;
import com.bdbox.service.ProductService;
import com.bdbox.service.RefundService;
import com.bdbox.service.UserService;
import com.bdbox.unionpay.acp.sdk.HttpClient;
import com.bdbox.util.LogUtils;
import com.bdbox.util.UtilDate;
import com.bdbox.web.dto.InvoiceDto;
import com.bdbox.web.dto.OrderDto;
import com.bdbox.wx.api.WxFilterIPService;
import com.bdbox.wx.api.WxUnifiedOrder;
import com.bdbox.wx.util.CommonUtil;
import com.bdbox.wx.util.NotifyUtil;
import com.bdbox.wx.util.OrderNumUtil;
import com.bdbox.wx.util.QRCodeUtil;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.CommonMethod;
import com.google.gson.Gson;

@Controller
public class OrderHandler extends HttpLXTHandler{

	@Autowired
	private OrderService orderService;
	@Autowired
	private UserService userService;
	
	@Autowired
	private RefundService refundService;

	@Autowired
	private ProductService productService;

	@Autowired
	private AddressService addressService;

	@Autowired
	private WxUnifiedOrder wxUnifiedOrder;

	@Autowired
	private WxFilterIPService wxFilterIPService;
	
	@Autowired
	private InvoiceService invoiceService; 
	
	@Autowired
	private BDCoodService bdcoodService; 
	
	@Autowired  
	private MailNotification mailNotification; 
	@Autowired 
	private NotificationService notificationService;  
	@Autowired
	private MessageRecordService MessageRecordService;
	
	private static Map<String, String> staticmap = new HashMap<String, String>();
	private static Map<String, String> staticmapdd = new HashMap<String, String>();  
	private static String flag="";//标记发邮件的次数，每个支付订单只发一次

	
	@RequestMapping({ "saveOrder.do" })
	@ResponseBody
	public String saveOrder(String name, String site, String versions,
			String color, Long phone) {
		Order order = new Order();
		order.setName(name);
		order.setVersions(versions);
		order.setSite(site);
		order.setCreatedTime(Calendar.getInstance());
		order.setColor(color);
		order.setPhone(phone);
		this.orderService.saveOrder(order);
		return null;
	}

	@RequestMapping({ "listOrder.do" })
	@ResponseBody
	public ListParam listOrder(@RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength, @RequestParam int iColumns,
			@RequestParam String sEcho, @RequestParam Integer iSortCol_0,
			@RequestParam String sSortDir_0,
			@RequestParam(required = false) String userNumber,
			@RequestParam(required = false) String productNameStr,
			@RequestParam(required = false) String buyTimeStr,
			@RequestParam(required = false) String orderNumbers,
			@RequestParam(required = false) String dealStatusStr,
			@RequestParam(required = false) String productTypeStr,
			@RequestParam(required = false) String transactionType,
			@RequestParam(required = false) String dealStatusStrbd, 
			@RequestParam(required = false) String order,
			@RequestParam(required = false) String name) throws Exception {
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;

		if ((userNumber != null) && (userNumber.equals(""))) {
			userNumber = null;
		}

		if ((productNameStr != null) && (productNameStr.equals(""))) {
			productNameStr = null;
		}

		if ((orderNumbers != null) && (orderNumbers.equals(""))) {
			orderNumbers = null;
		}

		Calendar buyTime = null;
		if ((buyTimeStr != null) && (buyTimeStr != "")) {
			buyTime = CommonMethod.StringToCalendar(buyTimeStr, "yyyy/MM/dd");
		}
		
		if(transactionType==null||transactionType==""){
			transactionType="RENT";
		}
		
		if(dealStatusStrbd.equals("订单类型")||dealStatusStrbd==""){
			dealStatusStrbd=null;
		}

		ProductType productType = ProductType.switchType(productTypeStr);
		DealStatus dealStatus = DealStatus.switchStatus(dealStatusStr);

		List<OrderDto> list = this.orderService.listOrder(userNumber, productType,
				productNameStr, buyTime, orderNumbers, dealStatus,
				Integer.valueOf(page), Integer.valueOf(pageSize),
				" t.createdTime desc ",transactionType,dealStatusStrbd,name);
		
		for (OrderDto orderDto : list) {
			if(orderDto.getInvoice()!=null && orderDto.getInvoice()!=0){
				InvoiceDto iDto = invoiceService.getInvoiceDto(orderDto.getInvoice());
				orderDto.setInvoiceType(iDto.getInvoiceType()==null?"":iDto.getInvoiceType().toString());
				orderDto.setCompanyName(iDto.getCompanyName());
			}
			
		}
		
		
		int count = this.orderService.listOrderCount(userNumber, productType,
				productNameStr, buyTime, orderNumbers, dealStatus,transactionType,dealStatusStrbd,name);
		for (OrderDto orderDto : list) {
			orderDto.setProductName("<a href=" + orderDto.getProductUrl()
					+ " target=\"_blank\">" + orderDto.getProductName()
					+ "</a>");
			orderDto.setManager("<a onclick=query("
					+ orderDto.getId()
					+ ") href='javascript:void(0);' data-toggle='modal' data-target='#myModal'>查看更多</a>");
			if (orderDto.getDealStatus() == "待审核") {
				orderDto.setHandle("<a href=\"javascript:shipmentsOrder(" + orderDto.getOrderNo() +",'"+orderDto.getDealStatus()+ "')" + "\">审核退款</a>");
			} else if (orderDto.getDealStatus() == "待退款" || orderDto.getDealStatus() == "退款失败") {
				orderDto.setHandle("<a href=\"javascript:refund(" + orderDto.getOrderNo() +",'"+orderDto.getDealStatus()+ "','"+orderDto.getZfType()+"')" + "\">退款</a>");
			} else if (orderDto.getDealStatus() == "等待发货") {
				orderDto.setHandle("<a onclick=query1(" + orderDto.getId() +",'"+orderDto.getDealStatus()+ "') href='javascript:void(0);' data-toggle='modal' data-target='#myModal'>发货</a>");
				//orderDto.setHandle("<a onclick=shipments(" + orderDto.getId() +",'"+orderDto.getDealStatus()+ "') href='javascript:void(0);'>发货</a>");
			} else if (orderDto.getDealStatus() == "已发货") {
				orderDto.setHandle("<a onclick=updateexpress(" + orderDto.getId() +",'"+orderDto.getDealStatus()+ "') href='javascript:void(0);' class='updateexpress' data-toggle='modal'  data-target='#myModal'>修改快递</a>");
			} else if (orderDto.getDealStatus() == "退款中") {
				orderDto.setHandle("<a href=\"javascript:queryRefund(" + orderDto.getOrderNo() +",'"+orderDto.getDealStatus()+ "')" + "\">查询退款</a>");
			}
		}

		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(Integer.valueOf(iDisplayLength));
		listParam.setiDisplayStart(Integer.valueOf(iDisplayStart));
		listParam.setiTotalDisplayRecords(Integer.valueOf(count));
		listParam.setiTotalRecords(Integer.valueOf(count));
		listParam.setsEcho(sEcho);
		listParam.setAaData(list);
		return listParam;
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping({ "getUserOrder.do" })
	@ResponseBody
	public ObjectResult getUserOrder(String user)
			throws UnsupportedEncodingException {
		List list = this.orderService.getUserOrder(user);
		Map map = new HashMap();
		map.put("list", list);
		ObjectResult objectResult = new ObjectResult(ResultStatus.OK);
		objectResult.setResult(map);

		return objectResult;
	}

	@RequestMapping({ "deleteOrder.do" })
	@ResponseBody
	public String deleteOrder(Long id) {
		this.orderService.deleteOrder(id.longValue());
		return null;
	}

	@RequestMapping({"getOrder.do"})
	@ResponseBody
	public OrderDto getOrder(long id) {
		OrderDto orderDto = this.orderService.getOrderDto(id);
		if("wechat".equals(orderDto.getZfType())){
			if(orderDto.getQrcodeTime()!=null && !"".equals(orderDto.getQrcodeTime())){
				if ((Long.valueOf(orderDto.getQrcodeTime()).longValue() < new Date()
						.getTime()) && (orderDto.getDealStatus().equals("未付款"))) {
					SortedMap<Object,Object> parameters = new TreeMap<Object,Object>();
					parameters.put("body", orderDto.getProductName());
					parameters.put("out_trade_no", orderDto.getTrade_no());
					parameters.put("total_fee",
							changeY2F(String.valueOf(orderDto.getPrice())));
		
					Map map = null;
					try {
						map = CommonUtil.xmlToArray(this.wxUnifiedOrder
								.getDynamicCodeUrl(parameters));
					} catch (JDOMException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
		
					String code = "";
		
					if (map.get("code_url") != null) {
						code = QRCodeUtil.getCode((String) map.get("code_url"));
					}
		
					orderDto.setQRCode(code);
		
					Order order = this.orderService.getOrder(Long.valueOf(orderDto
							.getId()));
					order.setQRCode(code);
					order.setQrcodeTime(String.valueOf(new Date().getTime() + 3600000L));
		
					this.orderService.updateOrder(order);
				} 
				orderDto.setQRCode("data:image/gif;base64," + orderDto.getQRCode());
			}
	 		
		}
		
	    if("等待发货".equals(orderDto.getDealStatus())|| "付款成功".equals(orderDto.getDealStatus())){ 
	    	if(!orderDto.getOrderNo().equals(flag)){
	    		flag=orderDto.getOrderNo();
	    		StringBuffer content=new StringBuffer(""); 
				String type="BUYPAY";  //通知类型
				String type_twl="admin"; 
				String invoices="";            //是否开发票
				String tomail=null;            //收信箱
				String subject=null;           //主题
				String mailcentent=null;       //内容
		    	if("租用".equals(orderDto.getTransactionType())){ 
		    		type="RENTPAY";
	    			Notification notification=notificationService.getnotification(type,type_twl);
	    			if(notification!=null){ 
	    				if(notification.getIsemail().equals(true)){   
	    						tomail=notification.getTomail();
	    						subject=notification.getSubject();  
	    						content.append(notification.getCenter()==null?"":notification.getCenter()).append("订单号："+orderDto.getOrderNo()+",下单时间："+orderDto.getCreatedTime()+"，下单数量："+orderDto.getCount()+"，收货地址："+orderDto.getSite()+"联系人："+orderDto.getName()+"，联系方式："+orderDto.getPhone()).append(invoices);
	    						if(content!=null){
	    							mailcentent=content.toString(); 
	    						}
	    						String [] email=notification.getTomail().split(",");
	    						for(String str:email){
	    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
	    						}
	    					//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
	    				}
	    			}
	            }else{
	            	Notification notification=notificationService.getnotification(type,type_twl);
	    			if(notification!=null){ 
	    				if(notification.getIsemail().equals(true)){  
	    						tomail=notification.getTomail();
	    						subject=notification.getSubject(); 
	    						if(orderDto.getInvoice()!=0){
	    							Invoice enty=invoiceService.getInvoice(orderDto.getInvoice());
	    							invoices="，发票抬头："+enty.getCompanyName();
	    						}
	    						content.append(notification.getCenter()+":").append("订单号："+orderDto.getOrderNo()+",下单时间："+orderDto.getCreatedTime()+"，下单数量："+orderDto.getCount()+"，收货地址："+orderDto.getSite()+"联系人："+orderDto.getName()+"，联系方式："+orderDto.getPhone()).append(invoices);
	    						if(content!=null){
	    							mailcentent=content.toString(); 
	    						}
	    						String [] email=tomail.split(",");
	    						for(String str:email){
	    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
	    						}	    					//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
	    				}
	    			} 
	            } 
	    }
	    }
		return orderDto;
	}
	
	
	@RequestMapping({"getOrder1.do"})
	@ResponseBody
	public OrderDto getOrder1(long id,String dealStatus) {
		OrderDto orderDto = this.orderService.getOrderDto(id);
		if("wechat".equals(orderDto.getZfType())){
			if(orderDto.getDealStatus().equals(dealStatus)){ 
			if ((Long.valueOf(orderDto.getQrcodeTime()).longValue() < new Date()
					.getTime()) && (orderDto.getDealStatus().equals("未付款"))) {
				SortedMap parameters = new TreeMap();
				parameters.put("body", orderDto.getProductName());
				parameters.put("out_trade_no", orderDto.getTrade_no());
				parameters.put("total_fee",
						changeY2F(String.valueOf(orderDto.getPrice())));
	
				Map map = null;
				try {
					map = CommonUtil.xmlToArray(this.wxUnifiedOrder
							.getDynamicCodeUrl(parameters));
				} catch (JDOMException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
	
				String code = "";
	
				if (map.get("code_url") != null) {
					code = QRCodeUtil.getCode((String) map.get("code_url"));
				}
	
				orderDto.setQRCode(code);
	
				Order order = this.orderService.getOrder(Long.valueOf(orderDto
						.getId()));
				order.setQRCode(code);
				order.setQrcodeTime(String.valueOf(new Date().getTime() + 3600000L));
	
				this.orderService.updateOrder(order);
			} 
			orderDto.setQRCode("data:image/gif;base64," + orderDto.getQRCode());
			}else{
				orderDto=null;
			}
		}
		return orderDto;
	}

	/**
	 * 统一下单接口 生成订单，状态：未付款
	 * 
	 * @param code
	 *            邮编
	 * @param count
	 *            数量
	 * @param url
	 *            产品url
	 * @param proVersion
	 *            产品名称
	 * @param username
	 *            用户名称
	 * @param mobilenum
	 *            联系号码
	 * @param emailnum
	 *            邮箱
	 * @param site
	 *            详细地址
	 * @param province
	 *            省、区、直辖市
	 * @param city
	 *            城市（地区）
	 * @param area
	 *            区县
	 * @param price
	 *            价格
	 * @param color
	 *            颜色
	 * @param versions
	 *            版本
	 * @param img
	 *            小图片路径
	 * @param user
	 *            用户
	 * @return
	 */
	@RequestMapping({ "savePlaceOrder.do" })
	@ResponseBody
	public String placeOrder(String code, Integer count, String url,
			String proVersion, String username, Long mobilenum,
			String emailnum, String site, String province, String city,
			String area, Double price, String color, String versions,
			String img, String user) {
		Order order = new Order();

		String orderNo = UtilDate.getOrderNum();
		String site2 = province + city + area + site + "   \t\t邮编："+ code;

		order.setTrade_no(OrderNumUtil.getNum());
		order.setUser(user);
		order.setColor(color);
		order.setCreatedTime(Calendar.getInstance());
		order.setEmail(emailnum);
		order.setName(username);
		order.setOrderNo(orderNo);
		order.setPhone(mobilenum);
		order.setPrice(Double.valueOf(3499.0D * count.intValue()));
		order.setProductName(proVersion);
		order.setProductType(ProductType.TERMINAL);
		order.setProductUrl(url);
		order.setSite(site2);
		order.setVersions(versions);
		order.setCount(count);
		order.setDealStatus(DealStatus.WAITPAID);
		order.setMinImg(img);
		order.setCancelstate("no");
		order.setTransactionType("GENERAL");
		SortedMap parameters = new TreeMap();
		parameters.put("body", order.getProductName());
		parameters.put("out_trade_no", order.getTrade_no()); 
		parameters.put("total_fee", "1"); 
		Map map = null;
		try {
			map = CommonUtil.xmlToArray(this.wxUnifiedOrder
					.getDynamicCodeUrl(parameters));
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String qrcode = QRCodeUtil.getCode((String) map.get("code_url"));

		order.setQRCode(qrcode);
		order.setQrcodeTime(String.valueOf(new Date().getTime() + 3600000L));

		this.orderService.saveOrder(order);

		Map res = new HashMap();
		res.put("orderNum", order.getOrderNo());
		res.put("qrcode", qrcode); 
		return new Gson().toJson(res);
	}

	/**
	 * 保存用户租用订单 单价
	 * 
	 * @return
	 */
	@RequestMapping({ "saveRentOrder.do" })
	@ResponseBody
	public ObjectResult saveRentOrder(long productId, Double price, int count, Double totalPrice,
			long addressId,String user,String productBuyType,
			String renttal, String rentDays, String leaveMessage, String sumRental,String bdcoodnember, String zfType) {
		ObjectResult objectResult = new ObjectResult();
		String type="RENTSTOCK";  //通知类型 
		String type_twl="admin"; 
		String mailcentent=null;
		String subject=null;  
		String tomail=null;  
		BigDecimal b1 = new BigDecimal(String.valueOf(price));
	    BigDecimal b2 = new BigDecimal(String.valueOf(count));
		// 获得产品信息
		Product product = productService.get(productId);
	    //获取用户判断用户是否是实名制用户
	    User u = userService.getUserByusername(user);
	    if(u.getIsAutonym()){
	    	b1 = new BigDecimal(String.valueOf(product.getCouponPrice()));
	    	if(count<1 || productId<1 || (b1.multiply(b2).doubleValue())!=totalPrice 
					|| addressId<1 || user == null){
				LogUtils.loginfo("参数异常");
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("参数异常");
				return objectResult;
			}
	    }else{
	    	if(count<1 || productId<1 || (b1.multiply(b2).doubleValue())!=totalPrice 
					|| addressId<1 || user == null){
				LogUtils.loginfo("参数异常");
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("参数异常");
				return objectResult;
			}
	    }
		if(product.getStockNumber()<count){
			LogUtils.loginfo("产品"+product.getName()+"无货");
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("库存不足"); 
			Notification notification=notificationService.getnotification(type,type_twl); 
			if(notification!=null){
				if(notification.getIsemail().equals(true)){
					mailcentent=notification.getCenter();
					subject=notification.getSubject();
					tomail=notification.getTomail();
					String [] email=tomail.split(",");
					for(String str:email){
    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
					}
				}
			} 
			return objectResult;
		}
		//判断是否开放购买
		if(!product.getIsOpenBuy()){
			LogUtils.loginfo("产品"+product.getName()+"未开放购买");
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("产品未开放购买");
			return objectResult;
		}
		//判断是否时间是否在开发购买区间中
		if(product.getBuyStartTime()!=null && product.getBuyEndTime()!=null ){
			long currentTime = System.currentTimeMillis();
			long buyStartTime = product.getBuyStartTime().getTimeInMillis();
			long buyEndTime = product.getBuyEndTime().getTimeInMillis();
			if(currentTime>=buyStartTime && currentTime<=buyEndTime){}else{
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("产品未开放购买"); 
				type="ACTIVTI";
				Notification notification2=notificationService.getnotification(type,type_twl); 
					if(notification2!=null){
						if(notification2.getIsemail().equals(true)){	
							mailcentent=notification2.getCenter();
							subject=notification2.getSubject();
							tomail=notification2.getTomail();
							String [] email=tomail.split(",");
    						for(String str:email){
    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
    						}						}
					} 
				return objectResult;
			}
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("产品未开放购买");
			return objectResult;
		}
		//抢购商品每个用户只能购买一个产品
		if(productBuyType!=null && productBuyType.equals("SHOPPINGRUSH")){
			//按照用户与商品id查询
			boolean b = orderService.queryOrderByPidUser(productId, user);
			if(b){
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("该产品您已购买，该产品限购一台。");
				return objectResult;
			}
		} 
		Map<String,String> map = new HashMap<String,String>(); 
		String[] bdcoodnembers=null; 
		if(!"".equals(bdcoodnember)){
			  bdcoodnembers=bdcoodnember.split(","); 
		}else{
			bdcoodnember=null;
		}
		if(bdcoodnembers!=null){
			for(String trs:bdcoodnembers){
				String teb=trs.substring(0, 3);
				if("BDM".equals(teb)){
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("下单失败，北斗券通道不能使用"+trs+"北斗码！！");
					return objectResult;
				}
				if(!map.containsKey(trs)){
					map.put(trs, trs); 
				}else{
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("下单失败，北斗券"+trs+"存在重复！！");
					return objectResult;
				}
			}
			for (String key : map.keySet()) {   
			    System.out.println("Key = " + key);  
				BDCood bcdood=bdcoodService.getbdcood_wl(key);
				if(bcdood!=null){
					 if(bcdood.getBdcoodStatus().toString().equals("USER")){
						 objectResult.setStatus(ResultStatus.FAILED);
						 objectResult.setMessage("下单失败,北斗券"+key+"已使用，<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;不可重复使用！！");
						 return objectResult;
					 }else  if(bcdood.getEndTime().getTime().getTime()<Calendar.getInstance().getTime().getTime()||bcdood.getBdcoodStatus().toString().equals(BdcoodStatus.EXCEED)){
						 objectResult.setStatus(ResultStatus.FAILED);
						 objectResult.setMessage("下单失败,北斗券"+key+"已过期，不可再使用！！");
						 return objectResult;
					 }else{
						 objectResult.setStatus(ResultStatus.OK);
						 objectResult.setMessage("through");
					 }
				}else{
					 objectResult.setStatus(ResultStatus.FAILED);
					 objectResult.setMessage("下单失败，北斗券"+key+"错误，请重新输入！");
					 return objectResult;
				}
			}  
		}   
		staticmap.clear();
		User usenty=userService.getUserByusername(user);
		String renlnameorder="";
		String unitcost="";
		if(usenty!=null&&usenty.getIsAutonym()!=null&&usenty.getIsAutonym().equals(true)){
			renlnameorder="1";
			unitcost=String.valueOf(product.getRentAutonymPrice());
		}else{
			renlnameorder="2";
			unitcost=String.valueOf(product.getRentPrice());
		} 
		
		Order order = orderService.userRentOrder(productId, price, count, totalPrice, addressId, user,
				productBuyType, renttal, rentDays, leaveMessage, sumRental,renlnameorder,unitcost,bdcoodnember, zfType);
		if(order!=null){
			objectResult.setResult(order);
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setMessage("下单成功");  
			
			//判断是否北斗券/码生成来源、如果是驴讯通后台调用接口生成、则调用接口改变状态
			if(bdcoodnembers!=null){
				for (String bdcood : bdcoodnembers) {
					BDCood enty = bdcoodService.getbdcood_wl(bdcood);
					if("lxt".equals(enty.getCreateCondition())){
						PostMethod postMethod = postMethod("/changeLxtCouponStatus.do");
						postMethod.addParameter("bdcood", bdcood);
						postMethod.addParameter("bdcoodStatus", "USER");
						postMethod.addParameter("orderNo", order.getOrderNo());
						postMethod.addParameter("username", order.getUser());
						httpClient(postMethod);
					}
				}
			}
			
			/*//以下是下单成功后给管理员发送邮件通知
 			StringBuffer content=new StringBuffer(); 
			String type="RENTORDERNOTPAY";  //通知类型
			String type_twl="admin"; 
			String invoices="";            //是否开发票
			String tomail=null;            //收信箱
			String subject=null;           //主题
			String mailcentent=null;       //内容
			Notification notification=notificationService.getnotification(type,type_twl);
			if(notification!=null){ 
				if(notification.getIsemail().equals(true)){  
						tomail=notification.getTomail();
						subject=notification.getSubject();  
						content.append(notification.getCenter()+":").append("订单号："+order.getOrderNo()+",下单时间："+CommonMethod.CalendarToString(Calendar.getInstance(), "yyyy/MM/dd HH:mm:ss")+"，下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，联系方式："+order.getPhone()).append(invoices);
						if(content!=null){
							mailcentent=content.toString(); 
						}
					mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
					//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
				}
			}*/
			
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("系统错误，下单失败");
		}
		return objectResult;
	}
	
	/**
	 * 保存用户购买订单 单价
	 * 
	 * @return
	 */
	@RequestMapping({ "saveBuyOrder.do" })
	@ResponseBody
	public ObjectResult saveBuyOrder(long productId, Double price, int count, Double totalPrice,
			long addressId,String user,String productBuyType, Long invoice,String bdcoodnember, String zfType) {
		BDCood bdCood = bdcoodService.getbdcood_wl(bdcoodnember);
		Product product = productService.get(productId);
		//获取商品原价格
		price = product.getPrice();
		//获取优惠价格
		Double ttPrice = 0.0;
		if(bdCood!=null){
			if(bdCood.getMoney()!=null){
				ttPrice = Double.parseDouble(bdCood.getMoney());
			}else{
				//BigDecimal()避免double精度丢失
				ttPrice = new BigDecimal(Double.toString(price))
						.multiply(new BigDecimal(Double.toString(1-(bdCood.getDiscount()/10)))).doubleValue();
			}
		}
		//格式化数字
		NumberFormat nf = NumberFormat.getInstance();
		ttPrice = Double.parseDouble(nf.format(ttPrice));
		
		ObjectResult objectResult = new ObjectResult();
		String type="BUYSTOCK";  //通知类型 
		String type_twl="admin"; 
		String mailcentent=null;
		String subject=null;  
		String tomail=null;  
		
		//BigDecimal()避免double精度丢失
		Double jprice = new BigDecimal(Double.toString(price*count))
					.subtract(new BigDecimal(Double.toString(ttPrice))).doubleValue();
		
		if(count<1 || productId<1 || (price*count)!=totalPrice+ttPrice || !jprice.equals(totalPrice)
				|| addressId<1 || user == null){
			LogUtils.loginfo("参数异常");
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数异常");
			return objectResult;
		}
		// 获得产品信息
		if(product.getStockNumber()<count){
			LogUtils.loginfo("产品"+product.getName()+"无货");
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("库存不足"); 
			Notification notification=notificationService.getnotification(type,type_twl); 
			if(notification!=null){
				if(notification.getIsemail().equals(true)){
					mailcentent=notification.getCenter();
					subject=notification.getSubject();
					tomail=notification.getTomail();
					String [] email=tomail.split(",");
					for(String str:email){
    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
					}				}
			} 
			return objectResult;
		}
		//判断是否开放购买
		if(!product.getIsOpenBuy()){
			LogUtils.loginfo("产品"+product.getName()+"未开放购买");
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("产品未开放购买");
			return objectResult;
		}
		//判断是否时间是否在开发购买区间中
		if(product.getBuyStartTime()!=null && product.getBuyEndTime()!=null ){
			long currentTime = System.currentTimeMillis();
			long buyStartTime = product.getBuyStartTime().getTimeInMillis();
			long buyEndTime = product.getBuyEndTime().getTimeInMillis();
			if(currentTime>=buyStartTime && currentTime<=buyEndTime){}else{
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("产品未开放购买");
				type="ACTIVTI";
				Notification notification2=notificationService.getnotification(type,type_twl); 
					if(notification2!=null){
						if(notification2.getIsemail().equals(true)){	
							mailcentent=notification2.getCenter();
							subject=notification2.getSubject();
							tomail=notification2.getTomail();
							String [] email=tomail.split(",");
    						for(String str:email){
    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
    						}						}
					} 
				
				return objectResult;
			}
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("产品未开放购买");
			return objectResult;
		}
		//抢购商品每个用户只能购买一个产品
		if(productBuyType!=null && productBuyType.equals("SHOPPINGRUSH")){
			//按照用户与商品id查询
			boolean b = orderService.queryOrderByPidUser(productId, user);
			if(b){
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("该产品您已购买，该产品限购一台。");
				return objectResult;
			}
		}
		
		
		Order order = orderService.userBuyOrder(productId, price, count, totalPrice, addressId, user,
				productBuyType, invoice, bdcoodnember, zfType);
		if(order!=null){
			objectResult.setResult(order);
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setMessage("下单成功"); 
			//以下是下单成功后给管理员发送邮件通知
 			/*StringBuffer content=new StringBuffer(); 
			String type="BUYORDERNOTPAY";   //通知类型
			String type_twl="admin";
			String invoices="";            //是否开发票
			String tomail=null;            //收信箱
			String subject=null;           //主题
			String mailcentent=null;       //内容
			Notification notification=notificationService.getnotification(type,type_twl);
			if(notification!=null){ 
				if(notification.getIsemail().equals(true)){  
						tomail=notification.getTomail();
						subject=notification.getSubject(); 
						if(order.getInvoice()!=0){
							Invoice enty=invoiceService.getInvoice(order.getInvoice());
							invoices="，发票抬头："+enty.getCompanyName();
						}
					   content.append(notification.getCenter()+":").append("订单号："+order.getOrderNo()+",下单时间："+CommonMethod.CalendarToString(Calendar.getInstance(), "yyyy/MM/dd HH:mm:ss")+"，下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，联系方式："+order.getPhone()).append(invoices);
						if(content!=null){
							mailcentent=content.toString(); 
						}
					  mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
					//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
				}
			}*/
			
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("系统错误，下单失败");
		}
		return objectResult;
	}

	/**
	 * 关闭订单接口 订单关闭成功，修改订单状态为：取消订单
	 * 
	 * @param trade_no
	 */
	public void closeorder(String trade_no) {
		try {
			Order order = orderService.getOrferByOrderNO(trade_no);
			if (order == null)
				return;
			String XMLData = wxUnifiedOrder.closeOrder(trade_no);
			Map map = CommonUtil.xmlToArray(XMLData);
			if ("SUCCESS".equals(map.get("result_code"))) {
				order.setDealStatus(DealStatus.CANCELORDER);
				orderService.updateOrder(order);
				if(order.getOrderType()==OrderType.BDORDER){
					if(order.getBdcood()!=null&&order.getBdcood()!=""){
						BDCood  bdcood=	bdcoodService.getbdcood_wl(order.getBdcood());
						bdcood.setBdcoodStatus(BdcoodStatus.NOTUSER);
						bdcood.setOrderNo(null);
						bdcoodService.update(bdcood);
					}
					
				}
				
				
				Product product = productService.get(order.getProductId());
				product.setSaleNumber(product.getSaleNumber()-order.getCount());
				product.setStockNumber(product.getStockNumber()+order.getCount());
				productService.updateProduct(product);
				
				LogUtils.loginfo("关闭订单:" + trade_no + "  成功");
			} else {
				LogUtils.loginfo("关闭订单:" + trade_no + "  失败，失败原因："
						+ map.get("err_code_des"));
			}
		} catch (Exception e) {
			LogUtils.logerror("关闭订单： " + trade_no + " 发生异常 ", e);
		}
	}

	/**
	 * 申请退款
	 * 
	 * @param out_trade_no
	 *            商户订单号
	 * @param total_fee
	 *            订单总金额
	 * @param refund_fee
	 *            退款金额
	 * @return
	 */
	public Boolean refund(String out_trade_no, double total_fee,
			double refund_fee) {
		try {
			Order order = orderService.getOrferByOrderNO(out_trade_no);
			if (order == null)
				return false;

			// 生成退款单号
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			String refundNo1 = df.format(date);
			Random r = new Random(8999);
			int refundNo2 = r.nextInt() + 1000;
			String refundNo = refundNo1 + refundNo2;
			int totalfee = (int) total_fee;
			int refundfee = (int) refund_fee;
			String total_feeStr = String.valueOf(totalfee);
			String refund_feeStr = String.valueOf(refundfee);
			String XMLData = wxUnifiedOrder.refund(null, out_trade_no,
					refundNo, total_feeStr, refund_feeStr);
			Map map = CommonUtil.xmlToArray(XMLData);
			if ("SUCCESS".equals(map.get("result_code"))) {
				// 保存退款信息
				Refund refund = new Refund();
				refund.setApplyTime(order.getBuyTime());
				refund.setOrderId(order.getId());
				refund.setOrderNo(order.getOrderNo());
				refund.setPrice(refund_fee);
				refund.setRefundNo(refundNo);
				refund.setRefundStatus(RefundStatus.APPLY);
				refundService.saveRefund(refund);

				order.setDealStatus(DealStatus.REBATES);
				orderService.updateOrder(order);

				LogUtils.loginfo("微信申请退款成功,订单号：" + order.getOrderNo()
						+ ",退款单号:" + refundNo + "  ");
				return true;
			} else {
				LogUtils.logwarn("微信申请退款失败，失败原因：" + map.get("err_code_des"),
						null);
			}
		} catch (Exception e) {
			LogUtils.logerror("微信申请退款异常", e);
		}
		return false;

	}

	/**
	 * 支付结果通用通知 订单支付成功后，状态修改为已支付
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping({ "OrdNotify.do" })
	@ResponseBody
	public String payNotify(HttpServletRequest request) {
		LogUtils.loginfo("处理一次异步请求");

		String notityXml = "";
		try {
			String inputLine;
			while ((inputLine = request.getReader().readLine()) != null) {
				// String inputLine;
				notityXml = notityXml + inputLine;
			}
			request.getReader().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	
		Map map = null;
		try {
			map = CommonUtil.xmlToArray(notityXml);
		} catch (JDOMException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		LogUtils.loginfo("XML:" + notityXml);
		if ("SUCCESS".equals(map.get("result_code"))) {
			//查询微信是否真的支付成功
			boolean paySuccess = false;
			String XMLData = wxUnifiedOrder.queyOrder(null, (String) map.get("out_trade_no"));
			Map querymap = null;
			try {
				querymap = CommonUtil.xmlToArray(XMLData);
				if ("SUCCESS".equals(querymap.get("result_code"))) {
					// SUCCESS—支付成功
					if ("SUCCESS".equals(querymap.get("trade_state"))){
						paySuccess = true;
					}
				}
			} catch (JDOMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(!paySuccess){
				LogUtils.loginfo("微信支付结果通用通知异常,微信支付通道与查询结果不一致！");
				return NotifyUtil.XMLFAIL;
			}
			
			LogUtils.loginfo("成功，交易单改成已付款");

			this.orderService.updateByTradeNo((String) map.get("out_trade_no")); 
			/*Order ord=orderService.getOrd((String) map.get("out_trade_no"));
			StringBuffer content=new StringBuffer(); 
			String type="BUYPAY";  //通知类型
			String type_twl="admin"; 
			String invoices="";            //是否开发票
			String tomail=null;            //收信箱
			String subject=null;           //主题
			String mailcentent=null;       //内容
	    	if("租用".equals(ord.getTransactionType())){ 
	    		type="RENTPAY";
    			Notification notification=notificationService.getnotification(type,type_twl);
    			if(notification!=null){ 
    				if(notification.getIsemail().equals(true)){   
    						tomail=notification.getTomail();
    						subject=notification.getSubject();  
    						content.append(notification.getCenter()).append("订单号："+ord.getOrderNo()+",下单时间："+ord.getCreatedTime()+"，下单数量："+ord.getCount()+"，收货地址："+ord.getSite()+"联系人："+ord.getName()+"，联系方式："+ord.getPhone()).append(invoices);
    						if(content!=null){
    							mailcentent=content.toString(); 
    						}
    						String [] email=notification.getTomail().split(",");
    						for(String str:email){
    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
    						}
    					//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
    				}
    			}
            }else{
            	Notification notification=notificationService.getnotification(type,type_twl);
    			if(notification!=null){ 
    				if(notification.getIsemail().equals(true)){  
    						tomail=notification.getTomail();
    						subject=notification.getSubject(); 
    						if(ord.getInvoice()!=0){
    							Invoice enty=invoiceService.getInvoice(ord.getInvoice());
    							invoices="，发票抬头："+enty.getCompanyName();
    						}
    						content.append(notification.getCenter()+":").append("订单号："+ord.getOrderNo()+",下单时间："+ord.getCreatedTime()+"，下单数量："+ord.getCount()+"，收货地址："+ord.getSite()+"联系人："+ord.getName()+"，联系方式："+ord.getPhone()).append(invoices);
    						if(content!=null){
    							mailcentent=content.toString(); 
    						}
    						String [] email=tomail.split(",");
    						for(String str:email){
    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
    						}	    					//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
    				}
    			} 
            } */

			return NotifyUtil.XMLSUCCEED;
		}
		LogUtils.loginfo("失败，没有付款成功");

		return NotifyUtil.XMLFAIL;
	}

	@RequestMapping({ "wxShortUrl.do" })
	@ResponseBody
	public void shorturl() {
		wxUnifiedOrder.shorturl("test");
	}

	public String changeY2F(String amount) {
		String currency = amount.replaceAll("\\$|\\￥|\\,", "");
		int index = currency.indexOf(".");
		int length = currency.length();
		Long amLong = Long.valueOf(0L);
		if (index == -1)
			amLong = Long.valueOf(currency + "00");
		else if (length - index >= 3)
			amLong = Long.valueOf(currency.substring(0, index + 3).replace(".",
					""));
		else if (length - index == 2)
			amLong = Long.valueOf(currency.substring(0, index + 2).replace(".",
					"") + 0);
		else {
			amLong = Long.valueOf(currency.substring(0, index + 1).replace(".",
					"")
					+ "00");
		}
		return amLong.toString();
	}

	/**
	 * 用户取消订单
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping({ "cancelOrderNo.do" })
	@ResponseBody
	public ObjectResult cancelOrderNo(String orderNo)
			throws UnsupportedEncodingException {
		Boolean result = orderService.updateOrderCancel(orderNo);
		ObjectResult objectResult = new ObjectResult();
		if (result) {
			Order order = orderService.getOrderstatus(orderNo);
			String[] bdcoodnembers = null;
			if(order.getBdcood()!=null){
				bdcoodnembers = order.getBdcood().split(",");
			}
			//判断是否北斗券/码生成来源、如果是驴讯通后台调用接口生成、则调用接口改变状态
			if(bdcoodnembers!=null){
				for (String bdcood : bdcoodnembers) {
					BDCood enty = bdcoodService.getbdcood_wl(bdcood);
					if("lxt".equals(enty.getCreateCondition())){
						PostMethod postMethod = postMethod("/changeLxtCouponStatus.do");
						postMethod.addParameter("bdcood", bdcood);
						postMethod.addParameter("bdcoodStatus", "NOTUSER");
						postMethod.addParameter("orderNo", "");
						postMethod.addParameter("username", "");
						httpClient(postMethod);
					}
				}
			}
			objectResult.setResult(result);
			objectResult.setStatus(ResultStatus.OK);
		} else {
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}

	/**
	 * 用户删除订单
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping({ "deleteOrderNo.do" })
	@ResponseBody
	public ObjectResult deleteOrderNo(String orderNo)
			throws UnsupportedEncodingException {
		Boolean result;
		result = orderService.updateOrderDelete(orderNo);
		ObjectResult objectResult = new ObjectResult();
		if (result) {
			objectResult.setResult(result);
			objectResult.setStatus(ResultStatus.OK);
		} else {
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}

	/**
	 * 用户申请退款
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping({ "refundOrderNo.do" })
	@ResponseBody
	public ObjectResult refundOrderNo(String orderNo)
			throws UnsupportedEncodingException {
		Boolean result;
		ObjectResult objectResult = new ObjectResult();
		result = orderService.updateOrderRefund(orderNo);
		if (result) {
			objectResult.setResult(result);
			objectResult.setStatus(ResultStatus.OK);
			
			Order order=orderService.getOrderstatus(orderNo);
			if(order!=null){  
			//以下是下单成功后给管理员发送邮件通知
 			StringBuffer content=new StringBuffer(); 
			String type="BUTREFUND";   //通知类型
			if(order.getTransactionType().equals("RENT")){
				type="RENTREFUND";
			}
			String type_twl="admin";
			String invoices="";            //是否开发票
			String tomail=null;            //收信箱
			String subject=null;           //主题
			String mailcentent=null;       //内容
			Notification notification=notificationService.getnotification(type,type_twl);
			if(notification!=null){ 
				if(notification.getIsemail().equals(true)){  
						tomail=notification.getTomail();
						subject=notification.getSubject(); 
						if(!order.getTransactionType().equals("RENT")){
							if(order.getInvoice()!=0){
								Invoice enty=invoiceService.getInvoice(order.getInvoice());
								invoices="，发票抬头："+enty.getCompanyName();
							}
						} 
					   content.append(notification.getCenter()).append("订单号："+order.getOrderNo()+",下单时间："+CommonMethod.CalendarToString(Calendar.getInstance(), "yyyy/MM/dd HH:mm:ss")+"，下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，联系方式："+order.getPhone()).append(invoices);
						if(content!=null){
							mailcentent=content.toString(); 
						}
						String [] email=tomail.split(",");
						for(String str:email){
	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
						}
 					//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
				}
			}
			}
			
		} else {
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}

	/**
	 * 用户关闭退款
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping({ "closeRefund.do" })
	@ResponseBody
	public ObjectResult closeRefund(String orderNo)
			throws UnsupportedEncodingException {
		Boolean result;
		ObjectResult objectResult = new ObjectResult();
		result = orderService.updateOrderClose(orderNo);
		if (result) {
			objectResult.setResult(result);
			objectResult.setStatus(ResultStatus.OK);
		} else {
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}

	/**
	 * 管理员审核退款
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping({ "shipmentsOrder.do" })
	@ResponseBody
	public String shipmentsOrder(String orderNo,String dealStatus)
			throws UnsupportedEncodingException {
		Boolean result=false;
		JSONObject jsonObject;
		ObjectResult objectResult = new ObjectResult();
		Order ordre=orderService.getOrderstatus(orderNo); 
		if(ordre.getDealStatus().equals(DealStatus.TOAUDIT)){
			result = orderService.updateOrderShipments(orderNo);  
		if (result) {
			objectResult.setResult(result);
			objectResult.setStatus(ResultStatus.OK);
			jsonObject = JSONObject.fromObject(objectResult);
		} else {
			objectResult.setResult(result);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
			jsonObject = JSONObject.fromObject(objectResult);
		}
		}else{
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
			jsonObject = JSONObject.fromObject(objectResult);
		} 
		return jsonObject.getString("result");
	}

	/**
	 * 管理员退款
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping({ "refund.do" })
	@ResponseBody
	public String refund(String orderNo,String dealStatus) throws UnsupportedEncodingException {
		Boolean result = false;
		JSONObject jsonObject = null;
		ObjectResult objectResult = new ObjectResult();
		if (orderNo != null) {
			Order ordre=orderService.getOrderstatus(orderNo); 
			if(ordre.getDealStatus().equals(DealStatus.WAITREFUND)){
				List<Order> list;
				list = orderService.getOrder(orderNo);
				if (list.size() != 0) {
					for (Order o : list) {
						//获取退款金额插入
						double total_fee=o.getPayAmount()*100*o.getCount();
						double refund_fee=total_fee;
						result = this.refund(o.getTrade_no(), total_fee, refund_fee);
					}
				}
				if (result) {
					objectResult.setResult(result);
					objectResult.setStatus(ResultStatus.OK);
					jsonObject = JSONObject.fromObject(objectResult);
					
					//以下是下单成功后给管理员发送邮件通知
		 			StringBuffer content=new StringBuffer(); 
					String type="BUYRENT";  //通知类型
					if(ordre.getTransactionType().equals("RENT")){
						type="RENTRETURN";
					}
					String type_twl="customer";  
					Notification notification=notificationService.getnotification(type,type_twl);
					if(notification!=null){ 
						if(notification.getIsnote().equals(true)){   
							//mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
							mailNotification.sendNote(String.valueOf(ordre.getPhone()), notification.getNotecenter()+"退款金额："+ordre.getPayAmount());
							//保存通知短信记录
							MessageRecordService.save(String.valueOf(ordre.getPhone()), 
									notification.getNotecenter()+"退款金额："+ordre.getPayAmount(), 
									MailType.switchStatus(type));
						}
					}
					
				} else {
					objectResult.setResult(result);
					objectResult.setStatus(ResultStatus.SYS_ERROR);
					jsonObject = JSONObject.fromObject(objectResult);
				}
			}else{
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.SYS_ERROR);
				jsonObject = JSONObject.fromObject(objectResult);
			}
		}
		return jsonObject.getString("result");
	}

	/**
	 * 查询退款
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping({ "queryRefund.do" })
	@ResponseBody
	public String queryRefund(String orderNo,String dealStatus)throws UnsupportedEncodingException {
		Boolean result = false;
		JSONObject jsonObject = null;
		ObjectResult objectResult = new ObjectResult();
		if (orderNo != null) {
			Order ordre=orderService.getOrderstatus(orderNo); 
			if(ordre.getDealStatus().equals(DealStatus.REBATES)){ 
				List<Refund> list2;
				list2 = orderService.getrefundNo(orderNo);
				if (list2.size() != 0) {
					for (Refund o : list2) {
						result = orderService.doWxQueryRefund(o.getRefundNo());
					}
				}
				if (result) {
					objectResult.setResult(result);
					objectResult.setStatus(ResultStatus.OK);
					jsonObject = JSONObject.fromObject(objectResult);
				} else {
					objectResult.setResult(result);
					objectResult.setStatus(ResultStatus.SYS_ERROR);
					jsonObject = JSONObject.fromObject(objectResult);
				}
			}else{
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.SYS_ERROR);
				jsonObject = JSONObject.fromObject(objectResult);
			}
		}
		return jsonObject.getString("result");
	}
	
	
	@RequestMapping({ "rentlistOrder.do" })
	@ResponseBody
	public ListParam rentlistOrder(@RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength, @RequestParam int iColumns,
			@RequestParam String sEcho, @RequestParam Integer iSortCol_0,
			@RequestParam String sSortDir_0,
			@RequestParam(required = false) String userNumber,
			@RequestParam(required = false) String productNameStr,
			@RequestParam(required = false) String buyTimeStr,
			@RequestParam(required = false) String orderNumbers,
			@RequestParam(required = false) String dealStatusStr,
			@RequestParam(required = false) String productTypeStr,
			@RequestParam(required = false) String transactionType,
			@RequestParam(required = false) String dealStatusStrbd, 
			@RequestParam(required = false) String order,
			@RequestParam(required = false) String name) throws Exception {
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;

		if ((userNumber != null) && (userNumber.equals(""))) {
			userNumber = null;
		}

		if ((productNameStr != null) && (productNameStr.equals(""))) {
			productNameStr = null;
		}

		if ((orderNumbers != null) && (orderNumbers.equals(""))) {
			orderNumbers = null;
		}

		Calendar buyTime = null;
		if ((buyTimeStr != null) && (buyTimeStr != "")) {
			buyTime = CommonMethod.StringToCalendar(buyTimeStr, "yyyy/MM/dd");
		}
		if(transactionType==null||transactionType==""){
			transactionType="RENT";
		}
		if(dealStatusStrbd.equals("订单类型")||dealStatusStrbd==""){
			dealStatusStrbd=null;
		}

		ProductType productType = ProductType.switchType(productTypeStr);
		DealStatus dealStatus = DealStatus.switchStatus(dealStatusStr);

		List<OrderDto> list = this.orderService.listOrder2(userNumber, productType,
				productNameStr, buyTime, orderNumbers, dealStatus,
				Integer.valueOf(page), Integer.valueOf(pageSize),
				" t.createdTime desc ",transactionType,dealStatusStrbd, name);
		int count = this.orderService.listOrderCount2(userNumber, productType,
				productNameStr, buyTime, orderNumbers, dealStatus,transactionType,dealStatusStrbd, name);
		for (OrderDto orderDto : list) {
			orderDto.setProductName("<a href=" + orderDto.getProductUrl()
					+ " target=\"_blank\">" + orderDto.getProductName()
					+ "</a>");
			orderDto.setManager("<a onclick=query("
					+ orderDto.getId()
					+ ") href='javascript:void(0);' data-toggle='modal' data-target='#myModal'>查看更多</a>");
			if (orderDto.getDealStatus() == "待审核") {
				orderDto.setHandle("<a href=\"javascript:shipmentsOrder(" + orderDto.getOrderNo() +",'"+orderDto.getDealStatus()+ "')" + "\">审核退款</a>");
			} else if (orderDto.getDealStatus() == "待退款" || orderDto.getDealStatus() == "退款失败") {
				orderDto.setHandle("<a href=\"javascript:refund(" + orderDto.getOrderNo() +",'"+orderDto.getDealStatus()+ "','"+orderDto.getZfType()+"')" + "\">退款</a>");
			} else if (orderDto.getDealStatus() == "等待发货") {
				orderDto.setHandle("<a onclick=query1(" + orderDto.getId() +",'"+orderDto.getDealStatus()+ "') href='javascript:void(0);' data-toggle='modal' data-target='#myModal'>发货</a>");
				//orderDto.setHandle("<a onclick=shipments(" + orderDto.getId() +",'"+orderDto.getDealStatus()+ "') href='javascript:void(0);'>发货</a>");
			} else if (orderDto.getDealStatus() == "已发货") {
				orderDto.setHandle("<a onclick=updateexpress(" + orderDto.getId() +",'"+orderDto.getDealStatus()+ "') href='javascript:void(0);' class='updateexpress' data-toggle='modal'  data-target='#myModal'>修改快递</a>");
			} else if (orderDto.getDealStatus() == "退款中") {
				orderDto.setHandle("<a href=\"javascript:queryRefund(" + orderDto.getOrderNo() +",'"+orderDto.getDealStatus()+ "')" + "\">查询退款</a>");
			}
		}

		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(Integer.valueOf(iDisplayLength));
		listParam.setiDisplayStart(Integer.valueOf(iDisplayStart));
		listParam.setiTotalDisplayRecords(Integer.valueOf(count));
		listParam.setiTotalRecords(Integer.valueOf(count));
		listParam.setsEcho(sEcho);
		listParam.setAaData(list);
		return listParam;
	} 

	/**
	 * 续租
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping({ "applyRelet.do" })
	@ResponseBody
	public ObjectResult applyRelet(String orderNo, String dates, String date2)
			throws UnsupportedEncodingException {
		Boolean result;
		ObjectResult objectResult = new ObjectResult();
		Order ord = orderService.getOrderstatus(orderNo);
		if (ord != null) {
			long sumdate = Long.valueOf(dates) + Long.valueOf(date2);
			ord.setNumberdate(String.valueOf(sumdate));
			orderService.updateOrder(ord);
			result = true;
		} else {
			result = false;
		}
		if (result) {
			objectResult.setResult(result);
			objectResult.setStatus(ResultStatus.OK);

			// 以下是下单成功后给管理员发送邮件通知
			StringBuffer content = new StringBuffer();
			String type = "XUQITIXIN"; // 通知类型
			String type_twl = "admin";
			String tomail = null; // 收信箱
			String subject = null; // 主题
			String mailcentent = null; // 内容
			Notification notification = notificationService.getnotification(
					type, type_twl);
			if (notification != null) {
				if (notification.getIsemail().equals(true)) {
					tomail = notification.getTomail();
					subject = notification.getSubject();
					content.append(notification.getCenter() + ":").append(
							"订单号："
									+ ord.getOrderNo()
									+ ",下单时间："
									+ CommonMethod.CalendarToString(
											Calendar.getInstance(),
											"yyyy/MM/dd HH:mm:ss") + "，下单数量："
									+ ord.getCount() + "，收货地址：" + ord.getSite()
									+ "联系人：" + ord.getName() + "，联系方式："
									+ ord.getPhone());
					if (content != null) {
						mailcentent = content.toString();
					}
					String[] email = tomail.split(",");
					for (String str : email) {
						mailNotification.sendMailErrorMessage(str, subject,
								mailcentent);
					}
					// mailNotification.sendNote(notification.getReceiveTeliPhone(),
					// notification.getNotecenter());
				}
			}

		} else {
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}

	// 租赁订单查询
	@SuppressWarnings("rawtypes")
	@RequestMapping({ "getUserRentOrder.do" })
	@ResponseBody
	public ObjectResult getUserRentOrder(String user)
			throws UnsupportedEncodingException {
		List list = this.orderService.getUserRentOrder(user);
		Map map = new HashMap();
		map.put("list", list);
		ObjectResult objectResult = new ObjectResult(ResultStatus.OK);
		objectResult.setResult(map);

		return objectResult;
	}

	// 北斗券通道
	@RequestMapping(value = "/subcheckoutTicket.do", method = RequestMethod.POST)
	public @ResponseBody
	ObjectResult checkoutTicket(
			@RequestParam(required = true) String bdcoodnember) {
		ObjectResult objectResult = new ObjectResult();
		String tapes = "";
		if (bdcoodnember != null && !bdcoodnember.equals("")) {
			tapes = bdcoodnember.substring(0, 3);
		}
		if (bdcoodnember == null) {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("北斗券不能为空");
			return objectResult;
		} else if (!tapes.equals("") && !tapes.equals("BDQ")) {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("errors");
			return objectResult;
		} else {
			BDCood bcdood = bdcoodService.getbdcood_wl(bdcoodnember);
			if (bcdood != null) {
				if (bcdood.getBdcoodStatus().toString().equals("USER")) {
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("user");
					staticmap.clear();
				} else if (bcdood.getBdcoodStatus().toString().equals("EXCEED")) {
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("Notthrough");
					staticmap.clear();
				} else {
					if (!staticmap.containsKey(bdcoodnember)) {
						staticmap.put(bdcoodnember, bdcoodnember);
						objectResult.setStatus(ResultStatus.OK);
						objectResult.setMessage("through");
					} else {
						objectResult.setStatus(ResultStatus.FAILED);
						objectResult.setMessage("repetition");
						staticmap.clear();
					}
				}
			} else {
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("error");
				staticmap.clear();
			}
		}
		return objectResult;
	}

	@RequestMapping(value = "/editOrder.do")
	@ResponseBody
	public ObjectResult editOrder(String linkman, String linkmob, Long invoice,
			String address, Long proId, String startTime, String endTime) {
		ObjectResult result = new ObjectResult();
		try {

			Order order = orderService.getOrder(proId);

			Calendar sTime = null;
			Calendar eTime = null;
			if (!"".equals(startTime)) {
				sTime = CommonMethod.StringToCalendar(startTime,
						"yyyy-MM-dd HH:mm:ss");
				order.setBeginTime(sTime);
			}
			if (!"".equals(endTime)) {
				eTime = CommonMethod.StringToCalendar(endTime,
						"yyyy-MM-dd HH:mm:ss");
				order.setEndTime(eTime);
			}

			order.setName(linkman);
			order.setPhone(Long.parseLong(linkmob));
			order.setInvoice(invoice);
			order.setSite(address);
			orderService.updateOrder(order);
			result.setMessage("修改成功");
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("修改失败");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}

	/**
	 * 销售统计
	 * @param startTime	查询开始时间
	 * @param endTime	查询结束时间
	 * @param statisticType	统计类型：日、周、月、季
	 * @return
	 */
	@RequestMapping(value="statisticOrder.do")
	@ResponseBody
	public ObjectResult statisticOrder(String startTime, String endTime, 
			String statisticType){

		Map<String, Map<String, Integer>> map = new TreeMap<>();
		ObjectResult result = new ObjectResult();
		Calendar sTime = null;
		Calendar eTime = null;
		try {
			if(startTime!=null && !"".equals(startTime)){
				sTime = CommonMethod.StringToCalendar(startTime, "yyyy-MM-dd HH:mm:ss");
			}
			if(endTime!=null && !"".equals(endTime)){
				eTime = CommonMethod.StringToCalendar(endTime, "yyyy-MM-dd HH:mm:ss");
			}
			
			map.put("general", orderService.statisticOrder(sTime, eTime, statisticType, "GENERAL"));
			map.put("rent", orderService.statisticOrder(sTime, eTime, statisticType, "RENT"));
			
			result.setStatus(ResultStatus.OK);
			result.setResult(map);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}
}
package com.bdbox.api.handler;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.api.dto.BoxDto;
import com.bdbox.api.dto.BoxLocationDto;
import com.bdbox.api.dto.BoxMessageDto;
import com.bdbox.api.dto.FamilyDto;
import com.bdbox.api.dto.MyCardLocStatusPOJO;
import com.bdbox.api.dto.PartnerDto;
import com.bdbox.constant.BoxStatusType;
import com.bdbox.constant.BoxType;
import com.bdbox.constant.CardType;
import com.bdbox.constant.UserStatusType;
import com.bdbox.entity.Box;
import com.bdbox.entity.User;
import com.bdbox.service.BoxFamilyService;
import com.bdbox.service.BoxLocationService;
import com.bdbox.service.BoxMessageService;
import com.bdbox.service.BoxPartnerService;
import com.bdbox.service.BoxService;
import com.bdbox.service.UserSendBoxMessageService;
import com.bdbox.service.UserService;
import com.bdbox.util.LogUtils;
import com.bdbox.web.dto.SdkUserSendDto;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Controller
public class BoxHandler {
	@Autowired
	private BoxService boxService;

	@Autowired
	private UserService userService;

	@Autowired
	private BoxMessageService boxMessageService;

	@Autowired
	private BoxLocationService boxLocationService;
	
	@Autowired
	private UserSendBoxMessageService userSendBoxMessageService;
	@Autowired
	private BoxPartnerService boxPartnerService;
	@Autowired
	private BoxFamilyService boxFamilyService;
	
	/**
	 * 获取盒子消息记录
	 * 
	 * @param iDisplayStart
	 *            从第几个开始取值
	 * @param iDisplayLength
	 *            每页显示数量
	 * @param iColumns
	 *            该表格列的数量
	 * @param sEcho
	 *            响应次数
	 * @param iSortCol_0
	 * @param sSortDir_0
	 *            排序方式
	 * 
	 * @param boxSerialNumber
	 *            盒子序列号
	 * @param queryTtype 查询类型	参数：1=接收信息，0=发送信息
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(value = "listBoxMessage.do")
	public @ResponseBody ListParam listBoxMessage(
			@RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
			@RequestParam int iColumns, @RequestParam String sEcho,
			@RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0,
			@RequestParam(required=false) String familyMob,@RequestParam(required=false) String familyMail,
			@RequestParam(required=false) String startTimeStr,@RequestParam(required=false) String endTimeStr,
			@RequestParam(required=false) String boxName,@RequestParam(required=false) String userName,
			@RequestParam(required = false) String boxSerialNumber,@RequestParam(required = false) String toboxSerialNumber,
			@RequestParam(required=false) Long queryType,@RequestParam(required=false) String entUserName,
			Long userId) throws ParseException {
		
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;

		Long fromserialNumber = null;
		Long toserialNumber = null; 
		if (boxSerialNumber!=null&&!"".equals(boxSerialNumber)) {
			fromserialNumber = Long.parseLong(boxSerialNumber);
		}else{
			boxSerialNumber = null;
		}
		if(entUserName!=null && entUserName.equals("")){
			entUserName = null;
		}
		if (toboxSerialNumber!=null&&!"".equals(toboxSerialNumber)) {
			toserialNumber = Long.parseLong(toboxSerialNumber);
		}else{
			toboxSerialNumber = null;
		}

		if(familyMob!=null&&familyMob.equals(""))	{
			familyMob = null;
		}
		
		if(familyMail!=null&&familyMail.equals(""))	{
			familyMail = null;
		}
		
		if(boxName!=null&&boxName.equals(""))	{
			boxName = null;
		}
		if(userId == null){
			userId = 0l;
		}
		
	/*	if(userName==null||userName.equals(""))	{
			userName = null;
		}else{
			UserDto userDto = userService.queryUser(userName);
			if(userDto!=null){
				userId = userDto.getId();
			}
		}*/
		
		Calendar startTime = null;
		if(startTimeStr!=null&&!"".equals(startTimeStr)){
			startTime = CommonMethod.StringToCalendar(startTimeStr,"yyyy-MM-dd HH:mm:ss");
		}
		
		Calendar endTime = null;
		if(endTimeStr!=null&&!"".equals(endTimeStr)){
			endTime = CommonMethod.StringToCalendar(endTimeStr,"yyyy-MM-dd HH:mm:ss");
		}
		
		List<BoxMessageDto> list = new ArrayList<BoxMessageDto>();
		Integer count = 0;
		if (queryType==null||queryType.equals("")) {
			 //list = boxMessageService.listBoxMessage(null, null, null, null, familyMob, fromserialNumber, toserialNumber, familyMail, startTime, endTime, boxName, userId,  "createdTime desc", page, pageSize);
			 list = boxMessageService.listBoxMessage(null, null, null, null, familyMob, fromserialNumber, toserialNumber, familyMail, startTime, endTime, boxName, userId, entUserName, "createdTime desc",  page, pageSize);
			count = boxMessageService.queryCount(null, null, null, null, familyMob, fromserialNumber, toserialNumber, familyMail, startTime, endTime, boxName, userId,entUserName);
		}else if(queryType==1){
			 list = boxMessageService.listBoxMessage(fromserialNumber,null, null, userId, "createdTime desc", page, pageSize);
			count = boxMessageService.queryCount(fromserialNumber, null, boxName, userId);
		}else if(queryType==0){
			 list = boxMessageService.listBoxMessage(null,toserialNumber, boxName, userId, "createdTime desc", page, pageSize);
			count = boxMessageService.queryCount( null,toserialNumber, boxName, userId);
		}
		
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(list);
		return listParam;
	}

	@RequestMapping(value="deleteBoxMessage")
	public String deleteBoxMessage(long id){
		boxMessageService.deleteBoxMessage(id);
		return "redirect:/table_message";
	}
	
	/**
	 * 获取盒子位置记录
	 * 
	 * @param iDisplayStart
	 *            从第几个开始取值
	 * @param iDisplayLength
	 *            每页显示数量
	 * @param iColumns
	 *            该表格列的数量
	 * @param sEcho
	 *            响应次数
	 * @param iSortCol_0
	 * @param sSortDir_0
	 *            排序方式
	 * 
	 * @param boxId
	 *            盒子id
	 *            
	 *  @param 盒子卡号
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(value = "listBoxLocation.do")
	public @ResponseBody ListParam listBoxLocation(
			@RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
			@RequestParam int iColumns, @RequestParam String sEcho,
			@RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0,
			@RequestParam(required = false) Long boxId,@RequestParam(required=false) String cardNumber,
			@RequestParam(required=false) String boxName,@RequestParam(required=false) String userName,
			@RequestParam(required=false) String boxSerialNumber,@RequestParam(required=false) String startTimeStr,
			@RequestParam(required=false) String endTimeStr,Long userId) throws ParseException {
		//处理分页参数
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		//处理查询参数
		if(boxId==null||boxId.equals(""))boxId = null;
		if(cardNumber==null||cardNumber.equals(""))cardNumber = null;
		if(boxName==null||boxName.equals(""))boxName = null;
		if(userName==null||userName.equals(""))userName = null;
		if(boxSerialNumber==null||boxSerialNumber.equals(""))boxSerialNumber = null;
		Calendar startTime = null;
		Calendar endTime = null;
		if(startTimeStr!=null && !"".equals(startTimeStr)
				&& endTimeStr!=null && !"".equals(endTimeStr)){
			startTime = CommonMethod.StringToCalendar(startTimeStr, "yyyy-MM-dd HH:mm:ss");
			endTime = CommonMethod.StringToCalendar(endTimeStr, "yyyy-MM-dd HH:mm:ss");
		}
		
		/*Calendar createdTime = null;
		if(createdTimeStr!=null&&!"".equals(createdTimeStr)){
			createdTime = CommonMethod.StringToCalendar(createdTimeStr,"yyyy/MM/dd hh:mm");
		}
		List<BoxLocationDto> list = boxLocationService.listBoxLocations(boxId,cardNumber,boxName,userName,boxSerialNumber,createdTime,"createdTime desc", page, pageSize);
		Integer count = boxLocationService.listBoxLocationsCount(boxId,cardNumber,boxName,userName,boxSerialNumber,createdTime);
		*/
		//查询
		List<BoxLocationDto> list = boxLocationService.queryBoxLocations(boxId, cardNumber, boxName, userName, boxSerialNumber, startTime, endTime,userId, "createdTime desc", page, pageSize);
		int count = boxLocationService.count(boxId, cardNumber, boxName, userName, boxSerialNumber, startTime, endTime,userId);
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(list);
		return listParam;
	}

	@RequestMapping(value="getBoxLocation",method=RequestMethod.POST)
	public @ResponseBody ObjectResult getBoxLocation(Long id){
		ObjectResult objectResult = new ObjectResult();
		objectResult.setMessage("查询位置信息");
		objectResult.setResult(boxLocationService.getBoxLocationDto(id));
		objectResult.setStatus(ResultStatus.OK);
		return objectResult;
	}
	
	@RequestMapping(value="deleteBoxlocation")
	public String deleteBoxlocation(long id){
		boxLocationService.deleteBoxLocation(id);
		return "redirect:/table_location";
	}
	
	/**
	 * 北斗盒子入库
	 * 
	 * @param boxDto
	 * @return
	 */
	@RequestMapping(value = "putinBox.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult putinBox(
			@RequestParam(required = true) String boxSerialNumber,
			@RequestParam(required = true) String snNumber,
			@RequestParam(required = true) String cardNumber,
			@RequestParam(required = true) String cardTypeStr,
			@RequestParam(required = true) int freq,
			@RequestParam(required = true) String boxType,
			@RequestParam(required = true) String boxStatusTypeStr,
			@RequestParam(required = true) String systemCode) {
		ObjectResult objectResult = new ObjectResult();
		if (!userService.checkSystemCode(systemCode)) {
			objectResult.setMessage("系统码出错");
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		}
		CardType cardType = CardType.getFromString(cardTypeStr);
		Box box = boxService.getBox(null,null, null, snNumber, null);
		if (box != null) {
			objectResult.setMessage("入库失败，北斗SN号:" + snNumber + " 已被北斗盒子:"
					+ box.getBoxSerialNumber() + "绑定");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		}
		box = boxService.getBox(null,null, cardNumber, null, null);
		if (box != null) {
			objectResult.setMessage("入库失败，北斗卡号:" + cardNumber + " 已被北斗盒子:"
					+ box.getBoxSerialNumber() + "绑定");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		}
		box = boxService.getBox(null,boxSerialNumber, null, null, null);
		if (box == null) {
			box = new Box();
			box.setSnNumber(snNumber);
			box.setBoxSerialNumber(boxSerialNumber);
			box.setCardNumber(cardNumber);
			if ((boxStatusTypeStr != null) && (!boxStatusTypeStr.equals(""))) {
				box.setBoxStatusType(BoxStatusType.getFromString(boxStatusTypeStr));
			}
			if ((boxType != null) && (!boxType.equals(""))) {
				box.setBoxType(BoxType.getFromString(boxType));
			}
			box.setFreq(freq);
			box.setCardType(cardType);
			boxService.saveBox(box);
			objectResult.setMessage("成功入库");
		} else {
			objectResult.setResult(null);
			objectResult.setMessage("入库失败，盒子ID:" + boxSerialNumber + "已存在");
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		}
		objectResult.setStatus(ResultStatus.OK);
		objectResult.setResult(true);
		return objectResult;
	}

	/**
	 * 激活绑定北斗盒子
	 * 
	 * @param boxSerialNumber
	 * @param systemCode
	 * @return
	 */
	@RequestMapping(value = "/bindBox", method = RequestMethod.POST)
	public @ResponseBody ObjectResult bindBox(
			@RequestParam(required = true) String boxSerialNumber,
			@RequestParam(required = true) String snNumber,
			@RequestParam(required = true) Long userId,
			@RequestParam(required = true) String name,
			@RequestParam(required = true) String idNumber,
			@RequestParam(required=true) String realName,
			@RequestParam(required = true) String systemCode) {

		ObjectResult objectResult = new ObjectResult();
		if (!userService.checkSystemCode(systemCode)) {
			objectResult.setMessage("系统调用码错误");
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		}
		Box box = boxService.getBox(BoxStatusType.NORMAL,boxSerialNumber, null, null, null);
		if (box != null) {
			objectResult.setMessage("激活失败，北斗盒子已被绑定");
			objectResult.setResult(false);
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		}
		Boolean result = boxService.doBindBox(boxSerialNumber, snNumber, name,
				userId,idNumber,realName);
		if (!result) {
			objectResult.setMessage("激活北斗盒子失败");
			objectResult.setResult(result);
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		}
		objectResult.setMessage("成功激活北斗盒子");
		objectResult.setResult(result);
		objectResult.setStatus(ResultStatus.OK);
		objectResult.setResult(true);
		return objectResult;
	}

	/**
	 * 解绑北斗盒子
	 * 
	 * @param boxSerialNumber
	 * @param systemCode
	 * @return
	 */
	@RequestMapping(value = "/unbindBox.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult unbindBox(
			@RequestParam String boxSerialNumber,
			@RequestParam(required = true) Long userId,
			@RequestParam String systemCode) {
		ObjectResult objectResult = new ObjectResult();
		if (!userService.checkSystemCode(systemCode)) {
			objectResult.setMessage("系统调用码错误");
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		}
		Box box = boxService.getBox(BoxStatusType.NORMAL,boxSerialNumber, null, null, userId);
		if (box == null) {
			objectResult.setMessage("用户对应的北斗盒子信息不存在，解绑失败");
			objectResult.setResult(false);
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		}
		Boolean result = boxService.doUnBindBox(boxSerialNumber, userId);
		if (!result) {
			objectResult.setMessage("北斗盒子解绑失败");
			objectResult.setResult(result);
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		}
		objectResult.setMessage("北斗盒子解绑成功");
		objectResult.setResult(result);
		objectResult.setStatus(ResultStatus.OK);
		objectResult.setResult(true);
		return objectResult;
	}

	/**
	 * 获取北斗盒子信息
	 * 
	 * @param boxSerialNumber
	 *            盒子id
	 * @param cardNumber
	 *            盒子卡号
	 * @param snNumber
	 *            盒子sn
	 * @param systemCode
	 *            系统调用码
	 * @param boxId
	 *            盒子实体id
	 * @return
	 */
	@RequestMapping(value = { "/getBox.do" }, method = { RequestMethod.POST })
	@ResponseBody
	public ObjectResult getBox(
			@RequestParam(required = false) String boxSerialNumber,
			@RequestParam(required = false) String cardNumber,
			@RequestParam(required = false) String snNumber,
			@RequestParam(required = false) Long boxId,
			@RequestParam(required = true) String systemCode) {
		ObjectResult objectResult = new ObjectResult();

		if (!this.userService.checkSystemCode(systemCode).booleanValue()) {
			objectResult.setMessage("系统调用码错误");
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		}

		BoxDto boxDto = new BoxDto();

		if ((boxSerialNumber != null) && (!"".equals(boxSerialNumber))) {
			boxDto = this.boxService.getBoxDto(boxSerialNumber, null, null,
					null);
			if (boxDto == null) {
				objectResult.setMessage("北斗盒子ID：" + boxSerialNumber + "不存在");
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				return objectResult;
			}
		}

		if ((cardNumber != null) && (!"".equals(cardNumber))) {
			boxDto = this.boxService.getBoxDto(null, cardNumber, null, null);
			if (boxDto == null) {
				objectResult.setMessage("北斗盒子卡号：" + cardNumber + "不存在");
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				return objectResult;
			}
		}

		if ((snNumber != null) && (!"".equals(snNumber))) {
			boxDto = this.boxService.getBoxDto(null, null, snNumber, null);
			if (boxDto == null) {
				objectResult.setMessage("北斗盒子SN码：" + snNumber + "不存在");
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				return objectResult;
			}
		}

		if ((boxId != null) && (!"".equals(boxId))) {
			boxDto = this.boxService.getBoxDto(boxId);
			if (boxDto == null) {
				objectResult.setMessage("北斗盒子id：" + boxId + "不存在");
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				return objectResult;
			}

		}

		objectResult.setMessage("成功获取北斗盒子信息");
		objectResult.setResult(boxDto);
		objectResult.setStatus(ResultStatus.OK);

		return objectResult;
	}

	/**
	 * 用户更改盒子信息
	 * 
	 * @param boxDto
	 * @return
	 */
	@RequestMapping(value = "/updateBox.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult updateBox(
			@RequestParam(required = false) Long userId,
			@RequestParam(required = true) String boxSerialNumber,
			@RequestParam(required = false) String name,
			@RequestParam(required = false) String idNumber,
			@RequestParam(required = false) String realName,
			@RequestParam(required = true) String systemCode) {

		ObjectResult objectResult = new ObjectResult();
		if (!userService.checkSystemCode(systemCode)) {
			objectResult.setMessage("系统码有误");
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		}

		Box box = boxService.getBox(BoxStatusType.NORMAL,boxSerialNumber, null, null, null);
		if (box == null) {
			objectResult.setMessage("更新盒子信息失败，没有找到盒子ID为：" + boxSerialNumber
					+ " 的盒子信息");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		} else {
			if (userId != null) {
				User user = userService.getUser(userId);
				box.setUser(user);
			}
			if (idNumber != null&&!"".equals(idNumber)) {
				box.setIdNumber(idNumber);
			}
			if (name != null&&!"".equals(name)) {
				box.setName(name);
			}
			if (realName != null&&!"".equals(realName)) {
				box.setRealName(realName);
			}
			box.setUpdateTime(Calendar.getInstance());
			boxService.updateBox(box);
			objectResult.setMessage("成功修改北斗盒子信息");
		}
		objectResult.setStatus(ResultStatus.OK);
		objectResult.setResult(true);

		return objectResult;
	}

	/**
	 * 获取用户所有的盒子
	 * 
	 * @param userId
	 *            用户ID
	 * @return
	 */
	@RequestMapping(value = "/listUserBox.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult listUserBox(Long userId) {
		ObjectResult objectResult = new ObjectResult();
		List<BoxDto> list = boxService.getUserBoxDtos(userId, 0, 0);
		if (list.size() > 0) {
			objectResult.setResult(list);
			objectResult.setMessage("成功获取用户盒子");
			objectResult.setStatus(ResultStatus.OK);
		} else {
			objectResult.setMessage("该用户没有盒子");
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}

	/**
	 * 查询盒子：管理平台
	 * 
	 * @param iDisplayStart
	 *            从第几个开始取值
	 * @param iDisplayLength
	 *            每页显示数量
	 * @param iColumns
	 *            该表格列的数量
	 * @param sEcho
	 *            响应次数
	 * @param iSortCol_0
	 * @param sSortDir_0
	 *            排序方式
	 * 
	 * @param cardTypeStr
	 *            卡类型
	 * @param snNumber
	 *            SN码
	 * @param boxSerialNumber
	 *            序列号
	 * @param boxStatusTypeStr
	 *            盒子状态
	 * @return
	 */
	@RequestMapping(value = { "listBox.do" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET })
	@ResponseBody
	public ListParam listBox(@RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength, @RequestParam int iColumns,
			@RequestParam String sEcho, @RequestParam Integer iSortCol_0,
			@RequestParam String sSortDir_0,
			@RequestParam(required = false) String cardNumber,
			@RequestParam(required = false) String cardTypeStr,
			@RequestParam(required = false) String snNumber,
			@RequestParam(required = false) String boxSerialNumber,
			@RequestParam(required = false) String boxStatusTypeStr,
			@RequestParam(required = false) String boxName,
			@RequestParam(required = false) String userName) {
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;

		if ((cardNumber != null) && ("".equals(cardNumber))) {
			cardNumber = null;
		}
		if ((snNumber != null) && ("".equals(snNumber))) {
			snNumber = null;
		}
		if ((boxSerialNumber != null) && ("".equals(boxSerialNumber))) {
			boxSerialNumber = null;
		}
		if ((boxName != null) && ("".equals(boxName))) {
			boxName = null;
		}
		if ((userName != null) && ("".equals(userName))) {
			userName = null;
		}

		CardType cardType = null;
		if ((cardTypeStr != null) && (!cardTypeStr.equals(""))) {
			cardType = CardType.getFromString(cardTypeStr);
		}
		BoxStatusType boxStatusType = null;
		if ((boxStatusTypeStr != null) && (!boxStatusTypeStr.equals(""))) {
			boxStatusType = BoxStatusType.getFromString(boxStatusTypeStr);
		}

		List list = boxService.query(null, cardType, cardNumber,
				boxSerialNumber, boxStatusType, snNumber,boxName,  userName, "createdTime desc",
				page, pageSize);
		Integer count = Integer.valueOf(boxService.queryAmount(null, cardType,
				cardNumber, boxSerialNumber, boxStatusType, snNumber,boxName,userName));

		ListParam listParam = new ListParam();
		listParam.setAaData(list);
		listParam.setiDisplayLength(Integer.valueOf(iDisplayLength));
		listParam.setiDisplayStart(Integer.valueOf(iDisplayStart));
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		return listParam;
	}

	/**
	 * 修改盒子
	 * 
	 * @param boxStr
	 * @param updateCardTypeStr
	 *            卡类型
	 * @param updateBoxStatusTypeStr
	 *            盒子状态
	 * @param username 用户
	 * @return
	 */
	@RequestMapping(value = { "update_manage_box.do" }, method = { RequestMethod.POST })
	@ResponseBody
	public ObjectResult update_manage_box(Box boxStr, String cardTypeStr,
			String boxStatusTypeStr,@RequestParam(required=false) String username,boolean isOpenSosCall,String boxType) {
		ObjectResult objectResult = new ObjectResult();
		Box box = boxService.getBox(boxStr.getId());
		//判断是否需要修改北斗卡
		if (!box.getCardNumber().equals(boxStr.getCardNumber())) {
			if(boxStr.getCardNumber()!=null && !"".equals(boxStr.getCardNumber())){
				Box box2 = boxService.getBox(null, null, boxStr.getCardNumber(), null, null);
				if (box2 != null) {
					objectResult.setMessage("修改失败，北斗卡号:" + boxStr.getCardNumber() + " 已被北斗盒子:"
							+ box2.getBoxSerialNumber() + "绑定");
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					return objectResult;
				}
			}
		}
		
		BeansUtil.copyBean(box, boxStr, true);
		box.setOpenSosCall(isOpenSosCall);
		if ((cardTypeStr != null) && (!cardTypeStr.equals(""))) {
			box.setCardType(CardType.getFromString(cardTypeStr));
		}
		if ((cardTypeStr != null) && (!cardTypeStr.equals(""))) {
			box.setCardType(CardType.getFromString(cardTypeStr));
		}
		if ((boxStatusTypeStr != null) && (!boxStatusTypeStr.equals(""))) {
			box.setBoxStatusType(BoxStatusType.getFromString(boxStatusTypeStr));
		}
		if ((boxType != null) && (!boxType.equals(""))) {
			box.setBoxType(BoxType.getFromString(boxType));
		}
	
		if(username != null && !username.equals("")){
			User user = userService.queryUser(null, username, null, UserStatusType.NORMAL, null);
			if(user == null){
				objectResult.setMessage("用户不存在。");
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				return objectResult;
			}	
			//盒子绑定用户名未改变，正常修改盒子信息
			if(box.getUser()!=null && username.equals(box.getUser().getUsername())){
				box.setUser(user);
				boxService.updateBox(box);
				objectResult.setMessage("修改成功");
				objectResult.setStatus(ResultStatus.OK);
				return objectResult;
			}
			box.setUser(user);
		}else{
			//清除用户
			box.setUser(null);
		}
		
		//盒子名称、省份证号
		box.setName(null);
		box.setIdNumber(null);
		//清除 家人
		box.setBoxFamilys(null);
		List<FamilyDto> familyDtos = boxFamilyService.getFamilyDtos(box.getCardNumber(),box.getBoxSerialNumber());
		if(familyDtos!=null){
			for(FamilyDto dto:familyDtos){
				boxFamilyService.deleteFamily(dto.getId());
			}
		}
		
		//清除队友
		List<PartnerDto> partnerDtos = boxPartnerService.getPartnerDtos(box.getCardNumber(), box.getBoxSerialNumber());
		if(partnerDtos != null){
			for(PartnerDto dto:partnerDtos){
				boxPartnerService.deletePartner(dto.getId());
			}
		}
		
		//更新盒子
		boxService.updateBox(box);
		
		objectResult.setMessage("修改成功");
		objectResult.setStatus(ResultStatus.OK);
		return objectResult;
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "deleteBox.do")
	public String deleteBox(Long id) {
		boxService.deleteBox(id);
		return "redirect:/table_box";
	}
	
	@RequestMapping(value = "SdkUserSend.do")
	public @ResponseBody ListParam SdkUserSend(
			@RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
			@RequestParam int iColumns, @RequestParam String sEcho,
			@RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0,
			@RequestParam(required = false) String fromboxSerialNumber,@RequestParam(required=false) String startTimeStr,
			@RequestParam(required=false) String endTimeStr) throws ParseException {
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
 		long serialNumber = 0; 
		if (fromboxSerialNumber!=null&&!"".equals(fromboxSerialNumber)) {
			serialNumber = Long.parseLong(fromboxSerialNumber);
		}else{
			fromboxSerialNumber = null;
		}
 		Calendar sendTime = null;
 		Calendar endsendTime = null;

		if(startTimeStr!=null&&!"".equals(startTimeStr)){ 
			sendTime = CommonMethod.StringToCalendar(startTimeStr,"yyyy/MM/dd");
		}
		if(endTimeStr!=null&&!"".equals(endTimeStr)){ 
			endsendTime = CommonMethod.StringToCalendar(endTimeStr,"yyyy/MM/dd");
		}
		List<SdkUserSendDto> list = userSendBoxMessageService.sdkUserSend(serialNumber,sendTime,endsendTime,"createdTime desc", page, pageSize);
		Integer count = userSendBoxMessageService.count(serialNumber,sendTime,endsendTime);
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(list);
		return listParam;
	}
	
	 @RequestMapping({"deleteSdkUserSend.do"})
	  public String deleteSdkUserSend(long id) {
	    this.userSendBoxMessageService.deleteSdkUserSend(id);
	    return "table_SdkUserSend";
	  } 
	 
	 @RequestMapping(value = { "listBox2.do" }, method = { org.springframework.web.bind.annotation.RequestMethod.GET })
		@ResponseBody
		public ListParam listBox2(@RequestParam int iDisplayStart,
				@RequestParam int iDisplayLength, @RequestParam int iColumns,
				@RequestParam String sEcho, @RequestParam Integer iSortCol_0,
				@RequestParam String sSortDir_0,
				@RequestParam(required = false) String cardNumber,
				@RequestParam(required = false) String cardTypeStr,
				@RequestParam(required = false) String snNumber,
				@RequestParam(required = false) String boxSerialNumber,
				@RequestParam(required = false) String boxStatusTypeStr,
				@RequestParam(required = false) String boxName,
				@RequestParam(required = false) String userName) {
			int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
			int pageSize = iDisplayLength;

			if ((cardNumber != null) && ("".equals(cardNumber))) {
				cardNumber = null;
			}
			if ((snNumber != null) && ("".equals(snNumber))) {
				snNumber = null;
			}
			if ((boxSerialNumber != null) && ("".equals(boxSerialNumber))) {
				boxSerialNumber = null;
			}
			if ((boxName != null) && ("".equals(boxName))) {
				boxName = null;
			}
			if ((userName != null) && ("".equals(userName))) {
				userName = null;
			}

			CardType cardType = null;
			if ((cardTypeStr != null) && (!cardTypeStr.equals(""))) {
				cardType = CardType.getFromString(cardTypeStr);
			}
			BoxStatusType boxStatusType = null;
			if ((boxStatusTypeStr != null) && (!boxStatusTypeStr.equals(""))) {
				boxStatusType = BoxStatusType.getFromString(boxStatusTypeStr);
			}

			List list = boxService.query(null, cardType, cardNumber,
					boxSerialNumber, boxStatusType, snNumber,boxName,  userName, "createdTime desc",
					page, pageSize);
			Integer count = Integer.valueOf(boxService.queryAmount(null, cardType,
					cardNumber, boxSerialNumber, boxStatusType, snNumber,boxName,userName));

			ListParam listParam = new ListParam();
			listParam.setAaData(list);
			listParam.setiDisplayLength(Integer.valueOf(iDisplayLength));
			listParam.setiDisplayStart(Integer.valueOf(iDisplayStart));
			listParam.setiTotalDisplayRecords(count);
			listParam.setiTotalRecords(count);
			listParam.setsEcho(sEcho);
			return listParam;
		}
	 
	 /**
	  * 通过盒子id(序列号)与盒子SN获取北斗盒子
	  * @param boxSerialNumber
	  * 	盒子id（序列号）
	  * @param boxSn
	  * 	盒子SN码
	  * @param systemCode
	  * 	系统调用码
	  * @return
	  */
	 @RequestMapping(value="getBoxByIdSn.do",method=RequestMethod.POST)
	 @ResponseBody
	 public ObjectResult getBoxByIdSn(
			@RequestParam(required = true) String boxSerialNumber,
			@RequestParam(required = true) String boxSn,
			@RequestParam(required = true) String systemCode) {
		
		ObjectResult objectResult = new ObjectResult();
		if (!userService.checkSystemCode(systemCode)) {
			objectResult.setMessage("系统码有误");
			objectResult.setStatus(ResultStatus.AUTH_FAILURE);
			return objectResult;
		}
		
		Box box = boxService.getBox(BoxStatusType.NORMAL, boxSerialNumber, null, boxSn, null);
		if(box!=null){
			BoxDto dto = boxService.getBoxDto(box.getId());
			objectResult.setMessage("查询成功");
			objectResult.setResult(dto);
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setMessage("查询失败");
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setResult(null);
		}
		return objectResult;
	}
	 
	 /**
	  * 统计用户收发短报文和短信数量
	  * 
	  * @param startTimeStr
	  * 	开始时间
	  * @param endTimeStr
	  * 	结束时间
	  * @param username
	  * 	用户名
	  * @return
	  */
	 @RequestMapping(value = { "statisticsUserMessage.do" })
	 public @ResponseBody ObjectResult statisticsUserMessage(
			 @RequestParam(required=true) String startTimeStr,
			 @RequestParam(required=true) String endTimeStr,
			 @RequestParam(required=false) long userid){
		 ObjectResult objectResult = new ObjectResult();
		 
		 Calendar startTime = null;
		 Calendar endTime = null;
		 try {
			startTime = CommonMethod.StringToCalendar(startTimeStr, "yyyy-MM-dd HH:mm:ss");
			endTime = CommonMethod.StringToCalendar(endTimeStr, "yyyy-MM-dd HH:mm:ss");
		} catch (ParseException e) {
			LogUtils.logerror("日期转换错误", e);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("String日期转Calendar日期错误");
			return objectResult;
		}
		 
		 Map<String, Integer> map = boxMessageService.statisticsUserMessage(userid, startTime, endTime);
		 objectResult.setStatus(ResultStatus.OK);
		 objectResult.setResult(map);
		 
		 return objectResult;
	 }

	 /**
	  * 前端获取轨迹信息
	  * @param boxId
	  * @param startTime
	  * @param endTime
	  * @return
	  */
	 @RequestMapping(value = "cardlocreplay.do")
	 @ResponseBody
	 public ObjectResult getBoxLocation(Long boxId, String startTime, String endTime,Long userId){
		 ObjectResult result = new ObjectResult();
		 Calendar sTime = null;
		 Calendar eTime = null;
		 try {
			 if(startTime!=null && !"".equals(startTime)){
				 sTime = CommonMethod.StringToCalendar(startTime, "yyyy-MM-dd HH:mm:ss");
			 }
			 if(endTime!=null && !"".equals(endTime)){
				 eTime = CommonMethod.StringToCalendar(endTime, "yyyy-MM-dd HH:mm:ss");
			 }
			 List<MyCardLocStatusPOJO> list = boxLocationService.getBoxLocationDto(boxId, sTime, eTime,userId);
			 result.setResult(list);
			 result.setStatus(ResultStatus.OK);
		 } catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResultStatus.FAILED);
		 }
		 return result;
	 }
	 
	 /**
	  * 后台获取轨迹信息
	  * @param id
	  * @param startTime
	  * @param endTime
	  * @return
	  */
	 @RequestMapping(value = "queryCardlocreplay.do")
	 @ResponseBody
	 public ObjectResult queryBoxLocation(String cardId, String startTime, String endTime){
		 ObjectResult result = new ObjectResult();
		 Calendar sTime = null;
		 Calendar eTime = null;
		 try {
			 if(startTime!=null && !"".equals(startTime)){
				 sTime = CommonMethod.StringToCalendar(startTime, "yyyy-MM-dd HH:mm:ss");
			 }
			 if(endTime!=null && !"".equals(endTime)){
				 eTime = CommonMethod.StringToCalendar(endTime, "yyyy-MM-dd HH:mm:ss");
			 }
			 List<MyCardLocStatusPOJO> list = boxLocationService.queryBoxLocationDto(cardId, sTime, eTime);
			 result.setResult(list);
			 result.setStatus(ResultStatus.OK);
		 } catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResultStatus.FAILED);
		 }
		 return result;
	 }

	/**
	 * 根据企业用户ID获取所有下属盒子
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "getBoxs.do")
	@ResponseBody
	public ObjectResult getUserBoxs(Long entUserId){
		return boxService.getEntUserBoxs(entUserId);
	}
	
	/**
	 * 获取企业用户下所有盒子定位信息
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "getAllLocation.do")
	@ResponseBody
	public ObjectResult getAllLocation(String ids){
		JSONObject json = JSONObject.fromObject(ids);
		return boxLocationService.getAllLocation(json);
	}
	
	/**
	 * 获取企业用户下发的消息
	 * @param phoneNum
	 * @return
	 */
	@RequestMapping(value = "getEntUserMessage.do")
	@ResponseBody
	public ObjectResult getEntUserMessage(String phoneNum){
		return boxMessageService.getEntUserMessage(phoneNum);
	}
	
	/**
	 * 获取企业用户接收的消息
	 * @param phoneNum
	 * @return
	 */
	@RequestMapping(value = "getReceiveMessage.do")
	@ResponseBody
	public ObjectResult getReceiveMessage(String ids){
		JSONObject json = JSONObject.fromObject(ids);
		return boxMessageService.getReceiveMessage(json);
	}
	
	/**
	 * 获取定位信息
	 * @param cardId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	@RequestMapping(value = "getBoxLocations.do")
	@ResponseBody
	public ObjectResult getEntUserLocation(String cardId, String startTime, String endTime, int page, Long uid){
		ObjectResult result = new ObjectResult();
		try {
			Box box = boxService.getEntUserBox(uid, cardId);
			if(box!=null){
				int pageSize = 15;
				
				Calendar sTime = null;
				Calendar eTime = null;
				if(startTime!=null && !"".equals(startTime)){
					sTime = CommonMethod.StringToCalendar(startTime, "yyyy-MM-dd HH:mm:ss");
				}
				if(endTime!=null && !"".equals(endTime)){
					eTime = CommonMethod.StringToCalendar(endTime, "yyyy-MM-dd HH:mm:ss");
				}
				return boxLocationService.getEntUserLocation(cardId, sTime, eTime, page, pageSize);
				
			}else{
				result.setMessage("抱歉，您无法查看该盒子ID的定位信息");
				result.setStatus(ResultStatus.NO_PERMISSION);
				return result;
			}	
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	  * 企业用户获取轨迹信息
	  * @param id
	  * @param startTime
	  * @param endTime
	  * @return
	  */
	 @RequestMapping(value = "queryEntUserCardlocreplay.do")
	 @ResponseBody
	 public ObjectResult queryEntUserCardlocreplay(String cardId, String startTime, String endTime){
		 ObjectResult result = new ObjectResult();
		 try{
			 Calendar sTime = null;
			 Calendar eTime = null;
			
			 if(startTime!=null && !"".equals(startTime)){
				 sTime = CommonMethod.StringToCalendar(startTime, "yyyy-MM-dd HH:mm:ss");
			 }
			 if(endTime!=null && !"".equals(endTime)){
				 eTime = CommonMethod.StringToCalendar(endTime, "yyyy-MM-dd HH:mm:ss");
			 }
			 List<MyCardLocStatusPOJO> list = boxLocationService.queryBoxLocationDto(cardId, sTime, eTime);
			 result.setResult(list);
			 result.setStatus(ResultStatus.OK);
	 	 } catch (Exception e) {
			 e.printStackTrace();
			 result.setMessage("系统异常");
			 result.setStatus(ResultStatus.SYS_ERROR);
		 }
		 return result;
	 }
	 
	 /**
	  * 关键字搜索短信
	  * @param phoneNum
	  * @param keyWord
	  * @param page
	  * @return
	  */
	 @RequestMapping(value = "getEntUserMessages.do")
	 @ResponseBody
	 public ObjectResult getEntUserMessages(String phoneNum, String keyWord){
		 return boxMessageService.getEntUserMessage(phoneNum, keyWord);
	 }
	 
	 /**
	  * 分页获取定位信息
	  * @param ids
	  * @param page
	  * @return
	  */
	 @RequestMapping(value = "getEntUserBoxLocations.do")
	 @ResponseBody
	 public ObjectResult getEntUserBoxLocations(String startTime, String endTime, String ids){
		 try {
			 Calendar sTime = null;
			 Calendar eTime = null;
			
			 if(startTime!=null && !"".equals(startTime)){
				 sTime = CommonMethod.StringToCalendar(startTime, "yyyy-MM-dd HH:mm:ss");
			 }
			 if(endTime!=null && !"".equals(endTime)){
				 eTime = CommonMethod.StringToCalendar(endTime, "yyyy-MM-dd HH:mm:ss");
			 }
			 JSONObject json = JSONObject.fromObject(ids);
			 return boxLocationService.getEntUserLocations(sTime, eTime, json);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	 }
	 
	 /**
	  * 搜索盒子
	  * @param uid
	  * @param cardId
	  * @return
	  */
	 @RequestMapping(value = "searchBox.do")
	 @ResponseBody
	 public ObjectResult searchBox(Long uid, String cardId){
		 return boxService.searchBox(uid, cardId);
	 }
	 
	 /**
	  * 寻找盒子(用户指挥调度系统绑定盒子)
	  * @param boxId
	  * @return
	  */
	 @RequestMapping(value = "findBox.do")
	 @ResponseBody
	 public ObjectResult findBox(String boxId){
		 ObjectResult result = new ObjectResult();
		 BoxDto dto = boxService.getBoxDto(boxId, null, null, null);
		 if(dto!=null){
			 result.setResult(dto);
			 result.setStatus(ResultStatus.OK);
		 }else{
			 result.setStatus(ResultStatus.FAILED);
		 }
		 return result;
	 }
	 
	 /**
	  * 根据盒子ID获取盒子信息
	  * 盒子ID例：110000
	  * @param ids
	  * @return
	  */
	 @RequestMapping(value = "getBoxsForBoxSerialNumber.do")
	 @ResponseBody
	 public ObjectResult getBoxs(String ids){
		 JSONObject json = JSONObject.fromObject(ids);
		 return boxService.getBoxs(json);
	 }
	 
	 /**
	  * 获取企业用户与盒子聊天记录
	  * @param phoneNum
	  * @param boxSerialNumber
	  * @return
	  */
	 @RequestMapping(value = "getBoxChatLog.do")
	 @ResponseBody
	 public ObjectResult getBoxChatLog(String phoneNum, String boxSerialNumber){
		 return boxMessageService.getBoxChatLog(phoneNum, boxSerialNumber);
	 }
	 
	 /**
	  * 修改盒子激活或未激活状态(用于指挥调度系统)
	  * @param boxId
	  * @param status
	  * @return
	  */
	 @RequestMapping(value = "updateBoxStatus.do")
	 @ResponseBody
	 public ObjectResult updateBoxStatus(String boxId, Boolean status){
		 ObjectResult result = new ObjectResult();
		 Box box = boxService.getBoxOne(boxId);
		 if(box!=null){
			 if(status){
				 box.setBoxStatusType(BoxStatusType.NORMAL);
			 }else{
				 box.setBoxStatusType(BoxStatusType.NOTACTIVATED);
			 }
			 boxService.updateBox(box);
			 result.setResult(box.getCardNumber());
			 result.setMessage("修改成功");
			 result.setStatus(ResultStatus.OK);
		 }else{
			 result.setMessage("修改失败，无此盒子ID");
			 result.setStatus(ResultStatus.FAILED);
		 }
		 return result;
	 }
	 
	 
	 /**
		 * 更新盒子MAC地址
		 * 
		 * @param boxSerialNumber
		 *            盒子id
		 * @param cardNumber
		 *            盒子卡号
		 * @param snNumber
		 *            盒子sn
		 * @param systemCode
		 *            系统调用码
		 * @param boxId
		 *            盒子实体id
		 * @return
		 */
		@RequestMapping(value = { "/updateBlueMacAdress.do" }, method = { RequestMethod.POST })
		@ResponseBody
		public ObjectResult updateBlueMacAdress(
				@RequestParam(required = false) String boxSerialNumber,
				@RequestParam(required = false) String cardNumber,
				@RequestParam(required = false) String snNumber,
				@RequestParam(required = false) String  blueMacAdress,
				@RequestParam(required = true) String systemCode) {
			ObjectResult objectResult = new ObjectResult();

			if (!this.userService.checkSystemCode(systemCode).booleanValue()) {
				objectResult.setMessage("系统调用码错误");
				objectResult.setStatus(ResultStatus.FAILED);
				return objectResult;
			}

			BoxDto boxDto = new BoxDto();

			if ((boxSerialNumber != null) && (!"".equals(boxSerialNumber))) {
				boxDto = this.boxService.getBoxDto(boxSerialNumber, null, null,
						null);
				if (boxDto == null) {
					objectResult.setMessage("北斗盒子ID：" + boxSerialNumber + "不存在");
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					return objectResult;
				}
			}

			if ((cardNumber != null) && (!"".equals(cardNumber))) {
				boxDto = this.boxService.getBoxDto(null, cardNumber, null, null);
				if (boxDto == null) {
					objectResult.setMessage("北斗盒子卡号：" + cardNumber + "不存在");
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					return objectResult;
				}
			}

			if ((snNumber != null) && (!"".equals(snNumber))) {
				boxDto = this.boxService.getBoxDto(null, null, snNumber, null);
				if (boxDto == null) {
					objectResult.setMessage("北斗盒子SN码：" + snNumber + "不存在");
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					return objectResult;
				}
			}
			
			Box box = boxService.getBox(BoxStatusType.NORMAL,boxSerialNumber, null, null, null);
			box.setUpdateTime(Calendar.getInstance());
			box.setBlueMacAdress(blueMacAdress);
			boxService.updateBox(box);
			boxDto.setBlueMacAdress(blueMacAdress);
			objectResult.setMessage("成功修改北斗盒子MAC地址");
			objectResult.setResult(boxDto);
			objectResult.setStatus(ResultStatus.OK);

			return objectResult;
		}
}
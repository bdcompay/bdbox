package com.bdbox.api.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bdbox.entity.Recipients;
import com.bdbox.service.RecipientsService;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
/**
 * 收件人信息管理
 * @author hax
 *
 */
@Controller
public class RecipientsHandler {

	@Autowired
	private RecipientsService recipientsService;
	
	/**
	 * 分页获取数据
	 * @param iDisplayStart
	 * @param iDisplayLength
	 * @param iColumns
	 * @param sEcho
	 * @param iSortCol_0
	 * @param sSortDir_0
	 * @return
	 */
	@RequestMapping(value = "getRecipients.do")
	@ResponseBody
	public ListParam getList(@RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
			@RequestParam int iColumns, @RequestParam String sEcho,
			@RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0){
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		
		List<Recipients> list = recipientsService.getList(page, pageSize);
		int count = recipientsService.count();
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(list);
		return listParam;
	}
	
	/**
	 * 保存信息
	 * @param recipients
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "saveRecipients.do")
	public ModelAndView saveRecipients(Recipients recipients, ModelAndView model){
		try {
			recipientsService.save(recipients);
			model.setViewName("redirect:/table_recipients.html");
			return model;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 删除信息
	 * @param id
	 */
	@RequestMapping(value = "deleteRecipients.do")
	@ResponseBody
	public ObjectResult deleteRecipients(Long id){
		ObjectResult result = new ObjectResult();
		try {
			recipientsService.delete(id);
			result.setMessage("删除成功");
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("删除失败");
			result.setStatus(ResultStatus.OK);
		}
		return result;
	}
	
	/**
	 * 根据id获取收件人信息
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "getRecipientsById.do")
	@ResponseBody
	public ObjectResult getRecipientsById(Long id){
		ObjectResult result = new ObjectResult();
		try {
			Recipients recipients = recipientsService.getRecipientsById(id);
			result.setResult(recipients);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 修改数据
	 * @param recipients
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "updateRecipients.do")
	public ModelAndView updateRecipients(Recipients recipients, 
			String u_province, String u_city, String u_county, ModelAndView model){
		try {
			if(u_province!=null && u_province!="")
				recipients.setProvince(u_province);
			if(u_city!=null && u_city!="")
				recipients.setCity(u_city);
			if(u_county!=null && u_county!="")
				recipients.setCounty(u_county);
			recipientsService.update(recipients);
			model.setViewName("redirect:/table_recipients.html");
			return model;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 修改默认收件人
	 * @param id
	 */
	@RequestMapping(value = "updateDefault.do")
	@ResponseBody
	public ObjectResult updateDefault(Long id){
		ObjectResult result = new ObjectResult();
		try {
			//所有默认收件设为0
			recipientsService.doSetZero();
			//根据id去修改默认收件为1
			recipientsService.updateDefault(id);
			result.setMessage("设置成功");
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("设置失败");
			result.setStatus(ResultStatus.OK);
		}
		return result;
	}
}

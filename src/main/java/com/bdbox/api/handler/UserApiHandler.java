package com.bdbox.api.handler;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.entity.Box;
import com.bdbox.entity.EnterpriseUser;
import com.bdbox.service.BoxLocationService;
import com.bdbox.service.BoxMessageService;
import com.bdbox.service.BoxService;
import com.bdbox.service.EnterpriseUserService;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;

@Controller
@RequestMapping(value = "ent/")
public class UserApiHandler {

	@Autowired
	private BoxService boxService;
	
	@Autowired
	private BoxMessageService boxMessageService;

	@Autowired
	private BoxLocationService boxLocationService;
	
	@Autowired
	private EnterpriseUserService enterpriseUserService;
	
	/**
	 * 根据企业用户ID获取所有下属盒子
	 * @param userPowerKey 授权码
	 * @return
	 */
	@RequestMapping(value = "/getBoxs.do")
	@ResponseBody
	public ObjectResult getUserBoxs(String userPowerKey){
		ObjectResult result = new ObjectResult();
		EnterpriseUser euser = enterpriseUserService.getUserByPowerKey(userPowerKey);
		if(euser!=null){
			return boxService.getEntUserBoxs(euser.getId());
		}
		return result;
	}
	
	/**
	 * 获取企业用户下所有盒子定位信息
	 * @param ids 盒子ID数组
	 * @param userPowerKey 授权码
	 * @param entUserId 用户ID
	 * @return
	 */
	@RequestMapping(value = "/getAllLocation.do")
	@ResponseBody
	public ObjectResult getAllLocation(String[] ids, String userPowerKey){
		ObjectResult result = new ObjectResult();
		try {
			if(ids.getClass().isArray()){
				EnterpriseUser euser = enterpriseUserService.getUserByPowerKey(userPowerKey);
				//将数组中的用户盒子ID添加进Json中
				JSONObject jsonObject = new JSONObject();
				for (String cardId : ids) {
					Box box = boxService.getEntUserBox(euser.getId(), cardId);
					if(box!=null){
						jsonObject.put(box.getId(), box.getId());
					}
				}
				
				return boxLocationService.getAllLocation(jsonObject);
			}else{
				result.setMessage("参数有误");
				result.setStatus(ResultStatus.FAILED);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("系统错误");
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		
		return result;
	}
	
	/**
	 * 获取企业用户下盒子消息
	 * @param ids 盒子ID数组
	 * @param userPowerKey 用户授权码
	 * @param entUserId 用户ID
	 * @return
	 */
	@RequestMapping(value = "/getReceiveMessage.do")
	@ResponseBody
	public ObjectResult getReceiveMessage(String[] ids, String userPowerKey){
		ObjectResult result = new ObjectResult();
		try {
			if(ids.getClass().isArray()){
				EnterpriseUser euser = enterpriseUserService.getUserByPowerKey(userPowerKey);
				//将数组中的用户盒子ID添加进Json中
				JSONObject jsonObject = new JSONObject();
				for (String cardId : ids) {
					Box box = boxService.getEntUserBox(euser.getId(), cardId);
					if(box!=null){
						jsonObject.put(box.getId(), box.getId());
					}
				}
				
				return boxMessageService.getReceiveMessage(jsonObject);
			}else{
				result.setMessage("参数有误");
				result.setStatus(ResultStatus.FAILED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("系统错误");
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		
		return result;
	}
	
}

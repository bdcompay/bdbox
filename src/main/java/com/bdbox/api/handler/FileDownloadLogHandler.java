package com.bdbox.api.handler;

import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.zookeeper.jmx.CommonNames;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.entity.FileDownloadLog;
import com.bdbox.service.FileDownloadLogService;
import com.bdbox.util.RemindDateUtils;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.CommonMethod;

/**
 * 文件下载记录控制器
 * 
 * @ClassName: FileDownloadLogHandler 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-2-26 下午3:52:06 
 * @version V5.0 
 */
@Controller
public class FileDownloadLogHandler {
	@Autowired
	private FileDownloadLogService fileDownloadLogService;
	
	/**
	 * 分页查询
	 */
	@RequestMapping(value="fileDownloadLogs.do")
	public @ResponseBody ListParam fileDownloadLogs(
			@RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
			@RequestParam int iColumns, @RequestParam String sEcho,
			@RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0,
			String filename,Integer statusInt,String ip,String ipArea,
			String username,String startTimeStr,String endTimeStr){
		//计算页号
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		
		Calendar startTime = null;
		Calendar endTime = null;
		
		try {
			if(startTimeStr!=null && !startTimeStr.equals("")){
				startTime = CommonMethod.StringToCalendar(startTimeStr, "yyyy-MM-dd HH:mm:ss");
			}
			if(endTimeStr!=null && !endTimeStr.equals("")){
				endTime = CommonMethod.StringToCalendar(endTimeStr, "yyyy-MM-dd HH:mm:ss");
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Boolean status = null;
	    if(statusInt!=null && statusInt==0){
	    	status = false;
	    }else if(statusInt!=null &&  statusInt==1){
	    	status = true;
	    }
		int count = fileDownloadLogService.count(filename, status, ip, ipArea, username, startTime, endTime);
		ListParam listParam = new ListParam();
		listParam = fileDownloadLogService.fileDownloadLogs(filename, status, ip, ipArea, username, startTime, endTime, page, pageSize);
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setsEcho(sEcho);
		return listParam;
	}
	
	/**
	 * 统计下载数量
	 */
	@RequestMapping(value="statisticDownloadNumber.do")
	public @ResponseBody ObjectResult statisticDownloadNumber(
			@RequestParam(required=false) String startTimeStr,
			@RequestParam(required=false) String endTimeStr,
			@RequestParam(required=false) String filename,
			@RequestParam(required=false) Integer statusInt){
		
		Calendar startTime = null;
	    Calendar endTime = null;
	    if(startTimeStr!=null && endTimeStr!=null){
	    	try {
	    		startTime = CommonMethod.StringToCalendar(startTimeStr, "yyyy-MM-dd HH:mm:ss");
	    		endTime = CommonMethod.StringToCalendar(endTimeStr, "yyyy-MM-dd HH:mm:ss");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    Boolean status = null;
	    int statusi = statusInt;
	    if(statusi == 0){
	    	status = false;
	    }else if(statusi == 1){
	    	status = true;
	    }
		
	    //按查询时间统计文件下载数量
	    int count = fileDownloadLogService.count(filename, status, null, null, null, startTime, endTime);
	    //统计今天文件下载数量
	    Calendar todayStartTime = Calendar.getInstance();
	    Calendar todayEndTime = Calendar.getInstance();
	    todayStartTime.setTime(RemindDateUtils.getCurrentDayStartTime());
	    todayEndTime.setTime(RemindDateUtils.getCurrentDayEndTime());
	    int todayCount = fileDownloadLogService.count(filename, status, null, null, null, todayStartTime, todayEndTime);
	    //统计本月文件下载数量
	    Calendar monthStartTime = Calendar.getInstance();
	    Calendar monthEndTime = Calendar.getInstance();
	    monthStartTime.setTime(RemindDateUtils.getCurrentMonthStartTime());
	    monthEndTime.setTime(RemindDateUtils.getCurrentMonthEndTime());
	    int monthCount = fileDownloadLogService.count(filename, status, null, null, null, monthStartTime, monthEndTime);
	    //统计全部文件下载数量
	    int allCount = fileDownloadLogService.count(filename, status, null, null, null, null, null);
	    
	    //保存统计数量
	    Map<String,Integer> map = new HashMap<String,Integer>();
	    map.put("queryCount", count);
	    map.put("todayCount", todayCount);
	    map.put("monthCount", monthCount);
	    map.put("allCount", allCount);
	    
	    
		ObjectResult objectResult = new ObjectResult();
		objectResult.setStatus(ResultStatus.OK);
		objectResult.setResult(map);
		return objectResult;
	}
	
	/**
	 * 保存文件下载记录
	 * 
	 */
	@RequestMapping(value="addFileDownloadLog.do")
	public @ResponseBody ObjectResult saveFileDownloadLog(
			@RequestParam(required=false) String filename,
			@RequestParam(required=false) Boolean status,
			@RequestParam(required=false) Long timeConsumig,
			@RequestParam(required=false) String ip,
			@RequestParam(required=false) String ipArea,
			@RequestParam(required=false) String username,
			@RequestParam(required=false) String downloadTimeStr)
	{
		Calendar downloadTime = null;
		if(downloadTimeStr!=null && !downloadTimeStr.equals("")){
			try {
				downloadTime = CommonMethod.StringToCalendar(downloadTimeStr, "yyyy-MM-dd HH:mm:ss");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		FileDownloadLog downloadLog = fileDownloadLogService.add(filename, status, timeConsumig, ip, ipArea, username, downloadTime);
		
		ObjectResult objectResult = new ObjectResult();
		if(downloadLog!=null){
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setMessage("成功");
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("失败");
		}
		return objectResult;
	}
}

package com.bdbox.api.handler;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.alipay.config.Alipay;
import com.bdbox.alipay.util.AlipayNotify;
import com.bdbox.alipay.util.UtilDate;
import com.bdbox.constant.RefundStatus;
import com.bdbox.entity.AlipayOrder;
import com.bdbox.entity.AlipayRefund;
import com.bdbox.service.AlipayRefundService;
import com.bdbox.util.StringUril;

@Controller
public class AlipayRefundHandler {

	@Autowired
	private AlipayRefundService alipayRefundService;
	
	/**
	 * 退款申请
	 * @param orderNo 订单编号
	 * @param response
	 * @throws ParseException
	 */
	@RequestMapping(value="alipayRefundQuery")
	public void alipayRefundExecute(String orderNo, String totalFee, HttpServletResponse response) throws ParseException{
		String sHtmlText = "";
		if(orderNo != null){
			AlipayOrder order = alipayRefundService.getAlipayOrder(orderNo);
			if(order!=null){
				//退款请求时间
				String refund_date = UtilDate.getDateFormatter();
				//退款批次号
				String batch_no = UtilDate.getOrderNum()+UtilDate.getThree();
				//总笔数
				String batch_num = "1";
				//单笔数据集
				String detail_data = order.getTradeNo()+"^"+(totalFee == null ? order.getTotalFee() : totalFee)+"^";
				//生成退款请求表单
				sHtmlText = Alipay.alipayRefundSubmit(refund_date, batch_no, batch_num, detail_data);
				
				//保存退款信息
				AlipayRefund refund = alipayRefundService.getAlipayRefund(order.getTradeNo());
				if(refund==null){
					refund = new AlipayRefund();
				}
				refund.setOrderNo(orderNo);
				refund.setTradeNo(order.getTradeNo());
				refund.setTotal_fee(order.getTotalFee());
				refund.setRefundStatus(RefundStatus.APPLY);
				refund.setCreateTime(Calendar.getInstance());
				refund.setRefundEmail(order.getBuyerEmail());
				alipayRefundService.saveOrUpdate(refund);
			}
			StringUril.writeToWeb(sHtmlText, response);
		}
	}
	
	/**
	 * 支付宝退款成功后返回来的异步通知
	 * @param request
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="alipayRefundNotify.do")
	@ResponseBody
	public String alipayRefundNotify(HttpServletRequest request) throws UnsupportedEncodingException{
		//获取支付宝POST过来反馈信息
		Map<String,String> params = new HashMap<String,String>();
		Map requestParams = request.getParameterMap();
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
			//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
			params.put(name, valueStr);
		}

		if(AlipayNotify.verify(params)){//验证成功
			alipayRefundService.update(params);
			return "success";
		}else{//验证失败
			return "fail";
		}
	}
}

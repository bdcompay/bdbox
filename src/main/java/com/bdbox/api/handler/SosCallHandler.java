package com.bdbox.api.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.api.dto.DialrecordDto;
import com.bdbox.api.dto.FamilyDto;
import com.bdbox.constant.BoxStatusType;
import com.bdbox.entity.Box;
import com.bdbox.entity.Dialrecord;
import com.bdbox.service.BoxService;
import com.bdbox.service.DialrecordService;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;

@Controller
public class SosCallHandler {
	
	@Autowired
	private DialrecordService dialRecordService;
	
	@Autowired
	private BoxService boxService;
	
	/**
	 * 获取SOS待拨号记录
	 * @return
	 */
	@RequestMapping(value = "/getDialrecords.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult getDialrecords() {
		ObjectResult objectResult = new ObjectResult();
		List<DialrecordDto> lto=dialRecordService.queryDialrecordDto(null, false, null, 1, 10);
		for(int i=0;i<lto.size();i++){
			Dialrecord dialrecord=dialRecordService.getDialrecord(lto.get(i).getMsgID());
			dialrecord.setHasSend(true);
			dialRecordService.updateDialrecord(dialrecord);
		}
		objectResult.setStatus(ResultStatus.OK);
		objectResult.setResult(lto);
		objectResult.setMessage("");
		return objectResult;
	}
	
}

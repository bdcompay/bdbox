package com.bdbox.api.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.api.dto.BoxDto;
import com.bdbox.api.dto.BoxMessageDto;
import com.bdbox.api.dto.MyCardLocStatusPOJO;
import com.bdbox.constant.UserStatusType;
import com.bdbox.constant.UserType;
import com.bdbox.entity.Box;
import com.bdbox.entity.EnterpriseUser;
import com.bdbox.entity.User;
import com.bdbox.service.BoxLocationService;
import com.bdbox.service.BoxMessageService;
import com.bdbox.service.BoxService;
import com.bdbox.service.EnterpriseUserService;
import com.bdbox.service.UserService;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;

/**
 * 该类为用户提供查询定位，获取通讯历史记录的信息
 * @author dezhi.zhang
 * @createTime 2017/01/13
 *
 */

@Controller
public class UserBoxHandler {
	@Autowired
	private BoxService boxService;

	@Autowired
	private BoxMessageService boxMessageService;

	@Autowired
	private BoxLocationService boxLocationService;
	
	@Autowired
	private EnterpriseUserService enterpriseUserService;
	
	@Autowired
	private UserService userService;
	
	/**
	 * 获取企业用户下盒子的定位信息
	 * @param ids 以','隔开的盒子ID数组
	 * @param userPowerKey 用户授权码
	 * @param page 分页页码
	 * @param pageSize 分页大小
	 * @return
	 */
	@RequestMapping(value = "/entUserQueryLocation.do")
	@ResponseBody
	public ObjectResult entUserQueryLocation(String ids, @RequestParam String userPowerKey,Integer page,Integer pageSize){
		ObjectResult result = new ObjectResult();
		try {
			//默认是最新的10条记录
			if(page==null){
				page=1;
			}
			if(pageSize==null){
				pageSize=10;
			}
			//不输入卡号，则默认查询所有卡号
			EnterpriseUser euser = enterpriseUserService.getUserByPowerKey(userPowerKey);
			List<BoxDto> boxs=new ArrayList<>(0);
			if(ids==null){
				boxs=boxService.queryEntBoxs(euser.getId(), page, pageSize);
				StringBuffer sBuffer=new StringBuffer();
				for(BoxDto bd:boxs){
					sBuffer.append(bd.getBoxSerialNumber()).append(",");
				}
				ids=sBuffer.toString();
			}
			
			String[] idList=ids.split(",");
			Map<String, List<MyCardLocStatusPOJO>> map=new HashMap<>();
			if(idList.length>0){
				//将数组中的用户盒子ID添加进Json中
				for (String serialNumber : idList) {
					Box box = boxService.getEntUserBox(euser.getId(), serialNumber);
					if(box!=null){
						map.put(box.getBoxSerialNumber()+"", boxLocationService.listUserBoxLocations(box.getId(), null, "id", page, pageSize));
					}
				}
				
				result.setStatus(ResultStatus.OK);
				result.setResult(map);
				result.setMessage("成功");
			}else{
				result.setMessage("参数错误,ids传入的不是以','分割的数组");
				result.setStatus(ResultStatus.FAILED);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("系统错误");
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		
		return result;
	}
	
	/**
	 * 获取企业用户下盒子消息
	 * @param ids 以','隔开的盒子ID数组
	 * @param userPowerKey 用户授权码
	 * @param page 分页页码
	 * @param pageSize 分页大小
	 * @return
	 */
	@RequestMapping(value = "/entUserQueryReceiveMessage.do")
	@ResponseBody
	public ObjectResult entUserQueryReceiveMessage(String ids, @RequestParam String userPowerKey,Integer page,Integer pageSize){
		ObjectResult result = new ObjectResult();
		try {
			//默认是最新的10条记录
			if(page==null){
				page=1;
			}
			if(pageSize==null){
				pageSize=10;
			}
			//不输入卡号，则默认查询所有卡号
			EnterpriseUser euser = enterpriseUserService.getUserByPowerKey(userPowerKey);
			List<BoxDto> boxs=new ArrayList<>(0);
			if(ids==null){
				boxs=boxService.queryEntBoxs(euser.getId(), page, pageSize);
				StringBuffer sBuffer=new StringBuffer();
				for(BoxDto bd:boxs){
					sBuffer.append(bd.getBoxSerialNumber()).append(",");
				}
				ids=sBuffer.toString();
			}
			
			String[] idList=ids.split(",");
			Map<String, List<BoxMessageDto>> map=new HashMap<>();
			if(idList.length>0){
				//将数组中的用户盒子ID添加进Json中
				for (String serialNumber : idList) {
					Box box = boxService.getEntUserBox(euser.getId(), serialNumber);
					if(box!=null){
						Long fromBoxId=0L;
						Long toBoxId=0L;
						fromBoxId=Long.parseLong(box.getBoxSerialNumber());
						toBoxId=Long.parseLong(box.getBoxSerialNumber());
						map.put("from_"+serialNumber, boxMessageService.listBoxMessage(fromBoxId, null, null, 0, "id", page, pageSize));
						map.put("to_"+serialNumber, boxMessageService.listBoxMessage(null, toBoxId, null, 0, "id", page, pageSize));
					}
				}
				
				result.setStatus(ResultStatus.OK);
				result.setResult(map);
				result.setMessage("成功");
			}else{
				result.setMessage("参数错误,ids传入的不是以','分割的数组");
				result.setStatus(ResultStatus.FAILED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("系统错误");
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		
		return result;
	}
	
	
	/**
	 * 获取用户盒子的定位信息
	 * @param ids 以','隔开的盒子ID数组
	 * @param username 用户名
	 * @param password 密码
	 * @param page 分页页码
	 * @param pageSize 分页大小
	 * @return
	 */
	@RequestMapping(value = "/userQueryLocation.do")
	@ResponseBody
	public ObjectResult userQueryLocation(String ids, @RequestParam String username,@RequestParam String password,
			Integer page,Integer pageSize){
		ObjectResult result = new ObjectResult();
		try {
			//默认是最新的10条记录
			if(page==null){
				page=1;
			}
			if(pageSize==null){
				pageSize=10;
			}
			//不输入盒子ID号，则默认查询所有盒子
			User user=userService.queryUser(null, username, password, UserStatusType.NORMAL, null);
			List<BoxDto> boxs=new ArrayList<>(0);
			if(ids==null){
				boxs=boxService.getUserBoxDtos(user.getId(), page, pageSize);
				StringBuffer sBuffer=new StringBuffer();
				for(BoxDto bd:boxs){
					sBuffer.append(bd.getBoxSerialNumber()).append(",");
				}
				ids=sBuffer.toString();
			}
			
			String[] idList=ids.split(",");
			Map<String, List<MyCardLocStatusPOJO>> map=new HashMap<>();
			if(idList.length>0){
				//将数组中的用户盒子ID添加进Json中
				for (String serialNumber : idList) {
					Box box = boxService.getBox(null, serialNumber, null, null, user.getId());
					if(box!=null){
						map.put(box.getBoxSerialNumber()+"", boxLocationService.listUserBoxLocations(box.getId(), null, "id", page, pageSize));
					}
				}
				
				result.setStatus(ResultStatus.OK);
				result.setResult(map);
				result.setMessage("成功");
			}else{
				result.setMessage("参数错误,ids传入的不是以','分割的数组");
				result.setStatus(ResultStatus.FAILED);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("系统错误");
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		
		return result;
	}
	
	/**
	 * 获取用户盒子消息
	 * @param ids 以','隔开的盒子ID数组
	 * @param username 用户名
	 * @param password 密码
	 * @param page 分页页码
	 * @param pageSize 分页大小
	 * @return
	 */
	@RequestMapping(value = "/userQueryReceiveMessage.do")
	@ResponseBody
	public ObjectResult userQueryReceiveMessage(String ids, @RequestParam String username,@RequestParam String password,
			Integer page,Integer pageSize){
		ObjectResult result = new ObjectResult();
		try {
			//默认是最新的10条记录
			if(page==null){
				page=1;
			}
			if(pageSize==null){
				pageSize=10;
			}
			//不输入卡号，则默认查询所有卡号
			User user=userService.queryUser(null, username, password, UserStatusType.NORMAL, null);
			List<BoxDto> boxs=new ArrayList<>(0);
			if(ids==null){
				boxs=boxService.getUserBoxDtos(user.getId(), page, pageSize);
				StringBuffer sBuffer=new StringBuffer();
				for(BoxDto bd:boxs){
					sBuffer.append(bd.getBoxSerialNumber()).append(",");
				}
				ids=sBuffer.toString();
			}
			
			String[] idList=ids.split(",");
			Map<String, List<BoxMessageDto>> map=new HashMap<>();
			if(idList.length>0){
				//将数组中的用户盒子ID添加进Json中
				for (String serialNumber : idList) {
					Box box = boxService.getBox(null, serialNumber, null, null, user.getId());
					if(box!=null){
						Long fromBoxId=0L;
						Long toBoxId=0L;
						fromBoxId=Long.parseLong(box.getBoxSerialNumber());
						toBoxId=Long.parseLong(box.getBoxSerialNumber());
						map.put("from_"+serialNumber, boxMessageService.listBoxMessage(fromBoxId, null, null, 0, "id", page, pageSize));
						map.put("to_"+serialNumber, boxMessageService.listBoxMessage(null, toBoxId, null, 0, "id", page, pageSize));
					}
				}
				
				result.setStatus(ResultStatus.OK);
				result.setResult(map);
				result.setMessage("成功");
			}else{
				result.setMessage("参数错误,ids传入的不是以','分割的数组");
				result.setStatus(ResultStatus.FAILED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("系统错误");
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		
		return result;
	}
}

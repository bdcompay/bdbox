package com.bdbox.api.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.entity.Address;
import com.bdbox.entity.User;
import com.bdbox.service.AddressService;
import com.bdbox.service.UserService;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.google.gson.Gson;

@Controller
public class AddressHandler {
	@Autowired
	private AddressService addressService;
	@Autowired
	private UserService userService;
	
	/**
	 * 保存收货地址
	 * 
	 * @return
	 */
	@RequestMapping(value = "saveAddress.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult saveAddress(
							@RequestParam(required=false) Long userId,
							@RequestParam(required=false) String name,
							@RequestParam(required=false) String email,
							@RequestParam(required=false) String mob,
							@RequestParam(required=false) String province,
							@RequestParam(required=false) String city,
							@RequestParam(required=false) String county,
							@RequestParam(required=false) String detailedAddress,
							@RequestParam(required=false) String postCode,
							@RequestParam(required=false) String areaCode){
		ObjectResult obj = new ObjectResult();
		//验证参数
		if(userId==null){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("参数 userId 不能为空");
			return obj;
		}else if(name==null || name.equals("")){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("参数 name 不能为空");
			return obj;
		}else if(mob==null || mob.equals("")){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("参数 mob 不能为空");
			return obj;
		}else if(province==null || province.equals("")){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("参数 province 不能为空");
			return obj;
		}else if(city==null || city.equals("")){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("参数 city 不能为空");
			return obj;
		}else if(detailedAddress==null || detailedAddress.equals("")){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("参数 detailedAddress 不能为空");
			return obj;
		}/*else if(postCode==null || postCode.equals("")){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("参数 postCode 不能为空");
			return obj;
		}*/
		
		User user = userService.getUser(userId);
		if(user==null){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("无此用户");
			return obj;
		}
	
		Address addr = addressService.save(userId,name, mob,email, province, city, county, detailedAddress, postCode, areaCode);
		
		if(user.getAddressId()==0){
			user.setAddressId(addr.getId());
			userService.updateUser(user);
		}
		
		if(addr==null){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("未知原因");
			return obj;
		}
		obj.setStatus(ResultStatus.OK);
		obj.setMessage("保存成功");
		return obj;
	}
	
	/**
	 * 根据默认收货地址
	 */
	@RequestMapping(value="updateUserAddress.do")
	public @ResponseBody ObjectResult updateUserAddress(Long userId,Long addressId){
		ObjectResult objectResult = new ObjectResult();
		
		//验证参数
		if(userId==null){
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数 userId 不能为空");
			return objectResult;
		}
		if(addressId==null){
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数 addressId 不能为空");
			return objectResult;
		}
		
		User user = userService.getUser(userId);
		if(user==null){
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("无此用户");
			return objectResult;
		}
		
		user.setAddressId(addressId);
		userService.updateUser(user);
		
		objectResult.setStatus(ResultStatus.OK);
		objectResult.setMessage("修改成功");
		
		return objectResult;
	}
	
	/**
	 * 根据用户id获取收货地址
	 * 
	 * @param userId
	 * @return
	 */
	@RequestMapping(value="getAddressByUid.do")
	public @ResponseBody ObjectResult getAddress(Long userId){
		ObjectResult obj = new ObjectResult();
		
		List<Address> addrs = addressService.getAddressByUserid(userId);
		if(addrs == null){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("获取收货为null");
		}else{
			Gson gson = new Gson(); 
			String json = gson.toJson(addrs);
			
			User user = userService.getUser(userId);
			
			obj.setStatus(ResultStatus.OK);
			obj.setResult(json);
			obj.setMessage(String.valueOf(user.getAddressId()));
		}
		return obj;
	}
	
	/**
	 * 更新收货地址
	 */
	@RequestMapping(value="updateAddress.do")
	public @ResponseBody ObjectResult updateAddress(
							@RequestParam(required=false) Long id,
							@RequestParam(required=false) String name,
							@RequestParam(required=false) String email,
							@RequestParam(required=false) Long userId,
							@RequestParam(required=false) String mob,
							@RequestParam(required=false) String province,
							@RequestParam(required=false) String city,
							@RequestParam(required=false) String county,
							@RequestParam(required=false) String detailedAddress,
							@RequestParam(required=false) String postCode,
							@RequestParam(required=false) String areaCode){
		ObjectResult obj = new ObjectResult();
		boolean b = addressService.update(id, userId,name, mob,email,province, city, county, detailedAddress, postCode, areaCode);
		if(!b){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("修改收货地址失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("修改收货地址成功");
		}
		return obj;
	}
	
	/**
	 * 删除收货地址
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value="deleteAddress.do")
	public @ResponseBody ObjectResult deleteAddress(Long id){
		ObjectResult obj = new ObjectResult();
		boolean b = addressService.deleteAddress(id);
		if(!b){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("删除失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("删除成功");
		}
		return obj;
	}
	
	/**
	 * 根据id查找收货地址
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value="getAddressById.do", produces = "application/json; charset=utf-8")
	public @ResponseBody String getAddressById(Long id){
		Address addr = addressService.get(id);
		Gson gson = new Gson(); 
		return gson.toJson(addr);
	}
	
	
	/**
	 * 修改用户默认收货地址
	 * 
	 * @param userId
	 * 			用户id
	 * @param addressId
	 * 			收货地址id
	 * @return
	 */
	@RequestMapping(value="updateUserDefualtAddr.do")
	public @ResponseBody ObjectResult updateUserDefualtAddr(long userId,long addressId){
		ObjectResult obj = new ObjectResult();
		User user = userService.getUser(userId);
		user.setAddressId(addressId);
		boolean b = userService.updateUser(user);
		if(!b){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("修改失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("修改成功");
		}
		return obj;
	}
	
	
}

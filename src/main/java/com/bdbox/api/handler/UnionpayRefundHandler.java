package com.bdbox.api.handler;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.entity.UnionpayOrder;
import com.bdbox.service.UnionpayOrderService;
import com.bdbox.service.UnionpayRefundService;
import com.bdbox.unionpay.acp.consume.BackRcvResponse;
import com.bdbox.unionpay.acp.consume.UnionPay;
import com.bdbox.unionpay.acp.sdk.AcpService;
import com.bdbox.unionpay.acp.sdk.LogUtil;
import com.bdbox.unionpay.acp.sdk.SDKConstants;
import com.bdbox.util.LogUtils;
import com.bdsdk.json.ObjectResult;

@Controller
@RequestMapping(value="unionpay")
public class UnionpayRefundHandler {

	@Autowired
	private UnionpayRefundService unionpayRefundService;
	
	@Autowired
	private UnionpayOrderService unionpayOrderService;
	
	/**
	 * 撤销退款操作
	 * @param orderId 订单号
	 */
	@RequestMapping(value="/revocation")
	@ResponseBody
	public ObjectResult revocation(String orderId){
		ObjectResult result = null;
		try {
			Map<String, String> rspData = new HashMap<String, String>();
			UnionpayOrder order = unionpayOrderService.getUnionpayOrder(orderId);
			if(order!=null){
				Calendar now =  order.getTxnTime();
				Calendar night23 = getTimesnight();
				if(getYMD(now)==getYMD(night23) && now.getTimeInMillis()>night23.getTimeInMillis()){
					rspData = UnionPay.revocation(removeDian(order.getTxnAmt()), order.getQueryId());
					LogUtils.loginfo("银联消费撤销成功");
				}else{
					rspData = UnionPay.refund(removeDian(order.getTxnAmt()), order.getQueryId());
					LogUtils.loginfo("银联消费退货退款成功");
				}
				//查询交易
				Map<String, String> data = UnionPay.query(orderId, rspData.get("txnTime"));
				result = unionpayRefundService.save(data, orderId);
			}
		} catch (Exception e) {
			LogUtils.logerror("银联消费退款失败", e);
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 退货退款
	 * @param orderId
	 */
	@RequestMapping(value="/refund")
	@ResponseBody
	public ObjectResult refund(String orderId){
		ObjectResult result = null;
		try {
			UnionpayOrder order = unionpayOrderService.getUnionpayOrder(orderId);
			if(order!=null){
				Map<String, String> rspData = UnionPay.refund(removeDian(order.getTxnAmt()), order.getQueryId());
				//查询交易
				Map<String, String> data = UnionPay.query(orderId, rspData.get("txnTime"));
				result = unionpayRefundService.save(data, orderId);
				LogUtils.loginfo("银联消费退货退款成功");
			}
		} catch (Exception e) {
			LogUtils.logerror("银联消费退款失败", e);
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 异步信息接收
	 * @param req
	 * @return
	 */
	@RequestMapping(value="/backRefundResponse.do")
	@ResponseBody
	public String backRcvResponse(HttpServletRequest req){
		
		LogUtil.writeLog("BackRcvResponse接收后台通知开始");
		try {
			String encoding = req.getParameter(SDKConstants.param_encoding);
			//获取银联后台返回来的信息
			Map<String, String> valideData = BackRcvResponse.backRcvResponse(req, encoding);

			//重要！验证签名前不要修改reqParam中的键值对的内容，否则会验签不过
			if (!AcpService.validate(valideData, encoding)) {
				LogUtil.writeLog("验证签名结果[失败].");
				System.out.println("验证签名结果[失败]...");
			} else {
				LogUtil.writeLog("验证签名结果[成功].");
				//保存返回来的银联订单信息
				unionpayRefundService.update(valideData);
			}
			return "ok";
		} catch (Exception e) {
			e.printStackTrace();
		}
		LogUtil.writeLog("BackRcvResponse接收后台通知结束");
		return "fail";
	}
	
	/**
	 * 预授权完成
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value="/authFinish")
	@ResponseBody
	public ObjectResult authFinish(String orderId, Double rentMoney){
		ObjectResult result = null;
		try {
			UnionpayOrder order = unionpayOrderService.getUnionpayOrder(orderId);
			Map<String, String> rspData = null;
			if(order!=null){
				//判断金额是否为0、如果为0则使用预授权撤销方法、否则使用预授权完成方法。
				if(rentMoney==0){
					rspData = UnionPay.authUndo(removeDian(order.getTxnAmt()), order.getQueryId());
				}else{
					rspData = UnionPay.authFinish(removeDian(rentMoney*100), order.getQueryId());
				}
				Map<String, String> data = UnionPay.query(orderId, rspData.get("txnTime"));
				result = unionpayRefundService.save(data, orderId);
				LogUtils.loginfo("预授权交易完成");
			}
		} catch (Exception e) {
			LogUtils.logerror("预授权交易出现异常", e);
		}
		return result;
	}
	
	/**
	 * 获取当天23点的时间
	 * @return 时间
	 */
	public static Calendar getTimesnight(){ 
		Calendar cal = Calendar.getInstance(); 
		cal.set(Calendar.HOUR_OF_DAY, 23); 
		cal.set(Calendar.SECOND, 0); 
		cal.set(Calendar.MINUTE, 0); 
		cal.set(Calendar.MILLISECOND, 0); 
		return cal; 
	} 
	
	/**
	 * 获取年月日
	 * @return
	 */
	public static int getYMD(Calendar cal){
		return cal.get(Calendar.YEAR) + (cal.get(Calendar.MONTH)+1) + cal.get(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * 清除小数点
	 * @param txnAmt
	 * @return
	 */
	public static String removeDian(Double txnAmt){
		String[] arr = txnAmt.toString().split("\\.");
		return arr[0];
	}

}

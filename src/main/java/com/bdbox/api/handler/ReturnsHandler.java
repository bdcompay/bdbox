package com.bdbox.api.handler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.api.dto.ReturnsDto;
import com.bdbox.constant.MailType;
import com.bdbox.constant.ReturnsStatusType;
import com.bdbox.entity.Invoice;
import com.bdbox.entity.Notification;
import com.bdbox.entity.Order;
import com.bdbox.entity.Refund;
import com.bdbox.entity.Returns;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.BDCoodService;
import com.bdbox.service.InvoiceService;
import com.bdbox.service.MessageRecordService;
import com.bdbox.service.NotificationService;
import com.bdbox.service.OrderService;
import com.bdbox.service.RefundService;
import com.bdbox.service.ReturnsService;
import com.bdbox.service.UserService;
import com.bdbox.util.LogUtils;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.CommonMethod;

/**
 * 退货处理
 * 
 * @author jlj
 * 
 */
@Controller
public class ReturnsHandler {
	@Autowired
	private ReturnsService returnsService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private UserService userService;
	@Autowired
	private RefundService refundService;
	@Autowired
	private InvoiceService invoiceService;  
	@Autowired
	private BDCoodService bdcoodService; 
	@Autowired  
	private MailNotification mailNotification; 
	@Autowired 
	private NotificationService notificationService;
	@Autowired
	private MessageRecordService MessageRecordService;
	

	// 根据id查询退货信息
	@RequestMapping(value = "getReturns.do")
	public @ResponseBody
	ObjectResult getReturns(long id) {
		ObjectResult objectResult = new ObjectResult();
		Returns returns = returnsService.getReturnsById(id);
		if (returns != null) {
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setResult(returns);
		} else {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setResult(returns);
		}
		return objectResult;
	}
	
	// 根据id查询退货信息
	@RequestMapping(value = "getReturnsDto.do")
	public @ResponseBody
	ObjectResult getReturnsDto(long id) {
		ObjectResult objectResult = new ObjectResult();
		Returns returns = returnsService.getReturnsDtoById(id);
		if (returns != null) {
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setResult(returns);
		} else {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setResult(returns);
		}
		return objectResult;
	}
	
	// 查询订单已退货数量
	@RequestMapping(value = "getReturnsInfo.do")
	public @ResponseBody ObjectResult getReturnsInfo(long orderId) {
		ObjectResult objectResult = new ObjectResult();
		
		Order order = orderService.getOrder(orderId);
		if(order==null){
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setResult(order);
		}else{
			List<Returns> list = returnsService.listReturns(orderId, null, null, null, null, null, null, 1, 9999,null);
			int count = 0;
			for(Returns r:list){
				int rcount = r.getCount();
				count +=rcount;
			}
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setResult(order);
			objectResult.setMessage(String.valueOf(count));
		}
		return objectResult;
	}

	// 根据条件查询退货信息
	@SuppressWarnings("unused")
	@RequestMapping(value = "listReturens.do")
	public @ResponseBody
	ListParam listReturns(@RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength, @RequestParam int iColumns,
			@RequestParam String sEcho, @RequestParam Integer iSortCol_0,
			@RequestParam String sSortDir_0,
			@RequestParam(required = false) Long orderId,
			@RequestParam(required = false) Long returnsNum,
			@RequestParam(required = false) String orderNum,
			@RequestParam(required = false) Long userId,
			@RequestParam(required = false) String expressNum,
			@RequestParam(required = false) String returnsStatus,
			@RequestParam(required = false) String startTimeStr,
			@RequestParam(required = false) String endTimeStr,
			@RequestParam(required = false) String orderstutas) {
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		ReturnsStatusType returnsStatusType = null;
		if (returnsStatus != null && !returnsStatus.equals("")) {
			returnsStatusType = ReturnsStatusType.switchStatus(returnsStatus);
		} else {
			returnsStatusType = null;
		}
		if (expressNum == null || expressNum.equals("")) {
			expressNum = null;
		}
		if (returnsStatus == null || "".equals(returnsStatus)) {
			returnsStatus = null;
		} 
		orderstutas = "GENERAL"; 
		Calendar startTime = null;
		Calendar endTime = null;
		try {
			if (startTimeStr != null && !"".equals(startTimeStr)) {
				startTime = CommonMethod.StringToCalendar(startTimeStr,
						"yyyy/MM/dd");
			}
			if (endTimeStr != null && !"".equals(endTimeStr)) {
				endTime = CommonMethod.StringToCalendar(endTimeStr,
						"yyyy/MM/dd");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		ListParam listParam = returnsService.queryReturns(orderId, returnsNum, orderNum,
				userId, expressNum, returnsStatusType, startTime, endTime,
				page, pageSize,orderstutas);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setsEcho(sEcho);
		return listParam;
	}
	
	//查询全部returns记录
	@RequestMapping(value = "listReturensDto.do")
	public @ResponseBody Map listReturnsDto(
			@RequestParam(required = false) Long orderId,
			@RequestParam(required = false) Long returnsNum,
			@RequestParam(required = false) String orderNum,
			@RequestParam(required = false) Long userId,
			@RequestParam(required = false) String expressNum,
			@RequestParam(required = false) String returnsStatus,
			@RequestParam(required = false) String startTimeStr,
			@RequestParam(required = false) String endTimeStr,
			@RequestParam(required = false) int page,
			@RequestParam(required = false) String orderstutas
			) {
		/*int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;*/
		//int page = 1;
		int pageSize = 10;
		ReturnsStatusType returnsStatusType = null;
		if (returnsStatus != null && !returnsStatus.equals("")) {
			returnsStatusType = ReturnsStatusType.switchStatus(returnsStatus);
		} else {
			returnsStatusType = null;
		}
		if (expressNum == null || expressNum.equals("")) {
			expressNum = null;
		}
		if (returnsStatus == null || "".equals(returnsStatus)) {
			returnsStatus = null;
		}
		orderstutas="GENERAL";
		Calendar startTime = null;
		Calendar endTime = null;
		try {
			if (startTimeStr != null && !"".equals(startTimeStr)) {
				startTime = CommonMethod.StringToCalendar(startTimeStr,
						"yyyy/MM/dd");
			}
			if (endTimeStr != null && !"".equals(endTimeStr)) {
				endTime = CommonMethod.StringToCalendar(endTimeStr,
						"yyyy/MM/dd");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		int count = returnsService.countReturns(orderId, returnsNum, orderNum, userId, expressNum, returnsStatusType, startTime, endTime, orderstutas);
		List<Returns> list = returnsService.listReturns(orderId, returnsNum, userId, expressNum, returnsStatusType, startTime, endTime, page, pageSize,orderstutas);
		
		List<ReturnsDto> dtos = new ArrayList<ReturnsDto>();
		for(Returns r:list){
			ReturnsDto dto = returnsService.castFromReturns(r);
			dtos.add(dto);
		}
		
		
		Map<String, Object> map = new HashMap<String, Object>();
		//当前页
		//总记录数
		//总页数
		//起始记录
		//截止记录
		//数据
		map.put("page", page);
		map.put("records", count);
		
		int totalpage = count/10;
		if(count%10!=0){
			totalpage+=1;
		}
		map.put("totalpage", totalpage);
		
		int displayStart = (page-1)*10+1;
		map.put("displayStart", displayStart);
		
		int displayEnd = page*10;
		if(count<displayEnd){
			displayEnd = count;
		}
		map.put("displayEnd", displayEnd);
		
		map.put("returns", dtos);
		
		/*ListParam listParam = returnsService.queryReturns(orderId, returnsNum,
				userId, expressNum, returnsStatusType, startTime, endTime,
				page, pageSize);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setsEcho(sEcho);*/
		return map;
	}
	

	// 用户申请退货（创建退货订单）
	@RequestMapping(value = "saveReturns.do")
	public @ResponseBody ObjectResult saveReturns(String orderNo,Long userId,
			/*Long productId,*/ String productName,
			int returnsCount, String returnsCause,String buy) {
		ObjectResult objectResult = new ObjectResult();
		if (userId == null) {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数" + userId + "不能为空");
		}
		if (orderNo == null) {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数" + orderNo + "不能为空");
		}
		/*if (productId == null) {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数" + productId + "不能为空");
		}*/
		if (productName == null) {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数" + productName + "不能为空");
		}
		if (returnsCount < 1) {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数" + returnsCount + "值小于1");
		}
		if (returnsCause == null) {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数" + returnsCause + "不能为空");
		}
		if ("FAILED".equals(objectResult.getStatus().toString())) {
			return objectResult;
		}
		List<Order> list = orderService.getOrder(orderNo);
		if(list==null || list.size()==0 ){
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage(orderNo+"订单号不存在");
			return objectResult;
		}
		Order order = list.get(0);
		if (returnsCount > order.getCount()) {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("退货数量不能大于订单购买数量");
			return objectResult;
		}
		double price = order.getPrice();
		Calendar buyTime = order.getCreatedTime();
		/*String username = order.getUser();
		UserDto userdto = userService.queryUser(username);*/
		
		Returns returns = returnsService.saveReturns(order.getId(), null,order.getOrderNo(),
				productName,userId, price, buyTime, returnsCount, returnsCause,buy, order.getPayType());
		if (returns != null) {
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setResult(returns);
			objectResult.setMessage("申请成功");
			
			//申请退货后给管理员发送邮件通知
 			StringBuffer content=new StringBuffer(""); 
			String type="APPLYRETURN";  //通知类型
			String type_twl="admin"; 
			if(order.getTransactionType().equals("RENT")){
				type="APPLYRENTRETURN";
			}
			String invoices="";            //是否开发票
			String tomail=null;            //收信箱
			String subject=null;           //主题
			String mailcentent=null;       //内容
			Notification notification=notificationService.getnotification(type,type_twl);
			if(notification!=null){ 
				if(notification.getIsemail().equals(true)){  
						tomail=notification.getTomail();
						subject=notification.getSubject(); 
						if(order.getInvoice()!=null&&order.getInvoice()!=0){
							Invoice enty=invoiceService.getInvoice(order.getInvoice());
							invoices="，发票抬头："+enty.getCompanyName();
						}
						content.append(notification.getCenter()).append("订单号："+order.getOrderNo()+",下单时间："+CommonMethod.CalendarToString(order.getCreatedTime(), "yyyy/MM/dd HH:mm:ss")+"，下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，联系方式："+order.getPhone()).append(invoices);
						if(content!=null){
							mailcentent=content.toString(); 
						}
						String [] email=tomail.split(",");
						for(String str:email){
	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
						}
 					//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
				}
			}
			
		} else {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("申请操作失败");
		}
		return objectResult;
	}
	
	//用户取消退货
	@RequestMapping(value="cancelReturn.do")
	public @ResponseBody ObjectResult cancelReturn(long id){
		ObjectResult objectResult = new ObjectResult();
		try{
			Returns returns =  returnsService.getReturnsById(id);
			if(returns!=null){
				//判断当前退货状态是否为：APPLYAUDIT("申请审核中")，否则提示状态错误
				if((returns.getReturnsStatusType().toString()).equals("APPLYAUDIT")){
					returns.setReturnsStatusType(ReturnsStatusType.CANCEL);
					Returns r = returnsService.updateRetuens(returns);
					objectResult.setStatus(ResultStatus.OK);
				}else{
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("statusError");
				}
			}else{
				objectResult.setStatus(ResultStatus.FAILED);
			}
		}catch(Exception e){
			LogUtils.logerror("用户取消退货单id："+id+"异常", e);
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	//用户再次确定退货
	@RequestMapping(value="confirmReturn.do")
	public @ResponseBody ObjectResult confirmReturn(long id){
		ObjectResult objectResult = new ObjectResult();
		try{
			Returns returns =  returnsService.getReturnsById(id);
			if(returns!=null){
				//判断当前退货状态是否为：CANCEL("已取消")，否则提示状态错误
				if((returns.getReturnsStatusType().toString()).equals("CANCEL")){
					returns.setReturnsStatusType(ReturnsStatusType.APPLYAUDIT);
					Returns r = returnsService.updateRetuens(returns);
					objectResult.setStatus(ResultStatus.OK);
				}else{
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("statusError");
				}
			}else{
				objectResult.setStatus(ResultStatus.FAILED);
			}
		}catch(Exception e){
			LogUtils.logerror("用户取消退货单号"+id+"失败", e);
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}

	// 管理员审核退货申请
	@RequestMapping(value = "auditApply.do")
	public @ResponseBody ObjectResult auditApply(long id, String auditResult, String cause) {
		ObjectResult objectResult = new ObjectResult();
		try{
			Returns returns =  returnsService.getReturnsById(id);
			if(returns!=null){
				//判断当前退货状态是否为：APPLYAUDIT("申请审核中")APPLYPASS("申请通过"),APPLYNOTPASS("申请未通过"),，否则提示状态错误
				String rstatus = returns.getReturnsStatusType().toString();
				if(rstatus.equals("APPLYAUDIT") || rstatus.equals("APPLYPASS") || rstatus.equals("APPLYNOTPASS")){
					boolean b = returnsService.doAuditApply(id, auditResult, cause);
					if (b) {
						objectResult.setStatus(ResultStatus.OK);
						objectResult.setMessage("审核操作成功");
						
						//以下管理员审核通过后短信通知客户
			 			StringBuffer content=new StringBuffer(); 
						String type="BUYAPPLYRENTRETURN";  //通知类型
						if(returns.getReturnType().equals("RENT")){
							type="RENTAPPLYRENTRETURN";
						}
						String type_twl="customer"; 
						String invoices="";            //是否开发票
						String tomail=null;            //收信箱
						String subject=null;           //主题
						String mailcentent=null;       //内容
						Notification notification=notificationService.getnotification(type,type_twl);
						Order order = orderService.getOrder(returns.getOrdreId());  
						if(notification!=null&&order!=null){ 
							if(notification.getIsnote().equals(true)){    
									content.append(notification.getNotecenter());
									if(content!=null){
										mailcentent=content.toString(); 
									}
								//mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
								mailNotification.sendNote(String.valueOf(order.getPhone()),mailcentent);
								//保存发送的短信记录
								MessageRecordService.save(String.valueOf(order.getPhone()), 
										mailcentent, MailType.switchStatus(type));
							}
						}
						
					} else {
						objectResult.setStatus(ResultStatus.FAILED);
						objectResult.setMessage("审核操作失败");
					}
				}else{
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("statusError");
				}
			}else{
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("退货单不存在");
			}
		}catch(Exception e){
			LogUtils.logerror("管理员审核退货申请异常", e);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("systemError");
		}
		return objectResult;
	}

	//用户退货，填写快递单号
	@RequestMapping(value = "submitExpressNum.do")
	public @ResponseBody ObjectResult submitExpressNum(long id, String expressName,
			String expressNum) {
		ObjectResult objectResult = new ObjectResult();
		try{
			Returns returns =  returnsService.getReturnsById(id);
			if(returns!=null){
				//判断当前退货状态是否为：APPLYPASS("申请通过")，否则提示状态错误
				if((returns.getReturnsStatusType().toString()).equals("APPLYPASS")){
					boolean b = returnsService.doSubmitExpressNum(id, expressName,expressNum);
					if (b) {
						objectResult.setStatus(ResultStatus.OK);
						objectResult.setMessage("操作成功");
						
						//以下是用户填写了物流后邮件通知管理员审核
			 			StringBuffer content=new StringBuffer(); 
						String type="WRITERETURN";  //通知类型
						String type_twl="admin"; 
						String reson="原因：无";
						Order order = orderService.getOrder(returns.getOrdreId()); 
						if(!"".equals(returns.getReturnsCause())||returns.getReturnsCause()!=null){
							reson="原因："+returns.getReturnsCause();
						} 
						if(returns.getReturnType().equals("RENT")){
							type="RETURNSIGN"; 
						}
						String invoices="";            //是否开发票
						String tomail=null;            //收信箱
						String subject=null;           //主题
						String mailcentent=null;       //内容
						Notification notification=notificationService.getnotification(type,type_twl);
						if(notification!=null&&order!=null){ 
							if(notification.getIsemail().equals(true)){  
									tomail=notification.getTomail();
									subject=notification.getSubject();  
									content.append(notification.getCenter()==null?"":notification.getCenter()+":").append("订单号："+returns.getOrderNum()+",申请提交时间："+CommonMethod.CalendarToString(returns.getExpressNumWriteTime(), "yyyy/MM/dd HH:mm:ss")+"，数量："+returns.getCount()+"，物流单号："+expressNum+",联系方式"+order.getPhone());
									if(content!=null){
										mailcentent=content.toString(); 
									}
									String [] email=tomail.split(",");
		    						for(String str:email){
		    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
		    						}
 								//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
							}
						}
						
					} else {
						objectResult.setStatus(ResultStatus.FAILED);
						objectResult.setMessage("操作失败");
					}
				}else{
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("statusError");
				}
			}else{
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("退货单不存在");
			}
		}catch(Exception e){
			LogUtils.logerror("用户退货，填写快递单号异常", e);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("systemError");
		}
		return objectResult;
	}
	
	// 管理员确定收货
	@RequestMapping(value = "receiveProduct.do")
	public @ResponseBody ObjectResult receiveProduct(long id, 
			@RequestParam(required=false) String endTime, @RequestParam(required=false) Long expressId, String type) {
		ObjectResult objectResult = new ObjectResult();
		try{
			Returns returns =  returnsService.getReturnsById(id);
			if(returns!=null){
				//判断当前退货状态是否为：EXPRESS("物流配送中")，否则提示状态错误
				if((returns.getReturnsStatusType().toString()).equals("EXPRESS")){
					if("RENT".equals(type)){
						returnsService.doConfirmSH(expressId, endTime);
					}
					boolean b = returnsService.doReceiveProduct(id);
					if (b) {
						objectResult.setStatus(ResultStatus.OK);
						objectResult.setMessage("操作成功");
					} else {
						objectResult.setStatus(ResultStatus.FAILED);
						objectResult.setMessage("操作失败");
					}
				}else{
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("statusError");
				}
			}else{
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("退货单不存在");
			}
		}catch(Exception e){
			LogUtils.logerror("管理员确定收货异常", e);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("systemError");
		}
		return objectResult;
	}

	// 管理员填写产品审核结果
	@RequestMapping(value = "auditProduct.do")
	public @ResponseBody ObjectResult auditProduct(long id, String auditResult, String explain) {
		
		ObjectResult objectResult = new ObjectResult();
		try{
			Returns returns =  returnsService.getReturnsById(id);
			if(returns!=null){
				//判断当前退货状态是否为：AUDIT("审核中")，否则提示状态错误
				if((returns.getReturnsStatusType().toString()).equals("AUDIT")){
					boolean b = returnsService.doAuditProduct(id, auditResult, explain.trim());
					if (b) {
						objectResult.setStatus(ResultStatus.OK);
						objectResult.setMessage("操作成功");
					} else {
						objectResult.setStatus(ResultStatus.FAILED);
						objectResult.setMessage("操作失败");
					}
				}else{
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("statusError");
				}
			}else{
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("退货单不存在");
			}
		}catch(Exception e){
			LogUtils.logerror("管理员填写产品审核结果异常", e);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("systemError");
		}
		return objectResult;
	}
	
	//管理员退货通过，填写退货退款数据
	@RequestMapping(value = "returnsRefund.do")
	public @ResponseBody ObjectResult returnsRefund(long id,double buyMoney,
			double deductionMoney,String deductionCause,double returnMoney) {
		ObjectResult objectResult = new ObjectResult();

		boolean b = returnsService.doRefund(id, buyMoney, deductionMoney, deductionCause, returnMoney);
		if (b) {
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setMessage("操作成功");
			Returns returns=returnsService.getReturnsById(id);
			Order order = orderService.getOrder(returns.getOrdreId());
			if(returns!=null&&order!=null){
			//以下是管理员审核通过后给用户发送退款通知
 			StringBuffer content=new StringBuffer(); 
			String type="tuihuoshenqingtongguo";  //通知类型
			String type_twl="customer"; 
			String dismony="";
			String returnmony="退回金额"+String.valueOf(returnMoney);
			String buymony="购买金额："+String.valueOf(buyMoney); 
			if(returns.getDeductionMoney()!=0){
				 dismony="扣减金额："+String.valueOf(deductionMoney)+","; 
			}
			if(returns.getReturnType().equals("RENT")){
				type="guihuanshenqingtongguo";
				buymony="押金："+String.valueOf(buyMoney);
				dismony="租金："+String.valueOf(deductionMoney)+","; 

			} 
			String invoices="";            //是否开发票
			String tomail=null;            //收信箱
			String subject=null;           //主题
			String mailcentent=null;       //内容
			Notification notification=notificationService.getnotification(type,type_twl);
			if(notification!=null){ 
				if(notification.getIsnote().equals(true)){  
						tomail=notification.getTomail();
						subject=notification.getSubject();  
						content.append(notification.getNotecenter()).append("退款单号："+returns.getReturnsNum()).append(buymony+",").append(dismony).append(returnmony);
						if(content!=null){
							mailcentent=content.toString(); 
						}
					//mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
					mailNotification.sendNote(String.valueOf(order.getPhone()), mailcentent);
					//保存发送的短信记录
					MessageRecordService.save(String.valueOf(order.getPhone()), 
							mailcentent, MailType.switchStatus(type));
				}
			}
		}	
			
		} else {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("操作失败");
		}
		return objectResult;
	}
	
	//管理员查询退货退款
	@RequestMapping(value = "queryReturnsRegund.do")
	public @ResponseBody ObjectResult queryReturnsRegund(long id) {
		ObjectResult objectResult = new ObjectResult();
		Returns returns = returnsService.getReturnsById(id);
		Refund refund = refundService.getRefund(returns.getRefundId());
		boolean b = refundService.doWxQueryRefund(refund.getRefundNo());
		if (b) {
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setMessage("操作成功");
		} else {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("操作失败");
		}
		return objectResult;
	}

	// 管理员确定退货成功
	@RequestMapping(value = "returnsSuccess.do")
	public @ResponseBody ObjectResult returnsSuccess(long id) {
		ObjectResult objectResult = new ObjectResult();

		boolean b = returnsService.doReturnsSuccess(id);
		if (b) {
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setMessage("操作成功");
		} else {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("操作失败");
		}
		return objectResult;
	}
	
	
	
   // 租用归还管理页面
	@SuppressWarnings("unused")
	@RequestMapping(value = "listRengReturens.do")
	public @ResponseBody
	ListParam listRengReturens(@RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength, @RequestParam int iColumns,
			@RequestParam String sEcho, @RequestParam Integer iSortCol_0,
			@RequestParam String sSortDir_0,
			@RequestParam(required = false) Long orderId,
			@RequestParam(required = false) Long returnsNum,
			@RequestParam(required = false) String orderNum,
			@RequestParam(required = false) Long userId,
			@RequestParam(required = false) String expressNum,
			@RequestParam(required = false) String returnsStatus,
			@RequestParam(required = false) String startTimeStr,
			@RequestParam(required = false) String endTimeStr,
			@RequestParam(required = false) String orderstutas) {
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		ReturnsStatusType returnsStatusType = null;
		if (returnsStatus != null && !returnsStatus.equals("")) {
			returnsStatusType = ReturnsStatusType.switchStatus(returnsStatus);
		} else {
			returnsStatusType = null;
		}
		if (expressNum == null || expressNum.equals("")) {
			expressNum = null;
		}
		if (returnsStatus == null || "".equals(returnsStatus)) {
			returnsStatus = null;
		} 
		orderstutas ="RENT";
		 
		Calendar startTime = null;
		Calendar endTime = null;
		try {
			if (startTimeStr != null && !"".equals(startTimeStr)) {
				startTime = CommonMethod.StringToCalendar(startTimeStr,
						"yyyy/MM/dd");
			}
			if (endTimeStr != null && !"".equals(endTimeStr)) {
				endTime = CommonMethod.StringToCalendar(endTimeStr,
						"yyyy/MM/dd");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		ListParam listParam = returnsService.doqueryRentReturns(orderId, returnsNum, orderNum,
				userId, expressNum, returnsStatusType, startTime, endTime,
				page, pageSize,orderstutas);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setsEcho(sEcho);
		return listParam;
	}
	
	
	//查询全部returns记录
	@RequestMapping(value = "listRentDto.do")
	public @ResponseBody Map listRentDto(
			@RequestParam(required = false) Long orderId,
			@RequestParam(required = false) Long returnsNum,
			@RequestParam(required = false) String orderNum,
			@RequestParam(required = false) Long userId,
			@RequestParam(required = false) String expressNum,
			@RequestParam(required = false) String returnsStatus,
			@RequestParam(required = false) String startTimeStr,
			@RequestParam(required = false) String endTimeStr,
			@RequestParam(required = false) int page,
			@RequestParam(required = false) String orderstutas
			) {
		/*int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;*/
		//int page = 1;
		int pageSize = 10;
		ReturnsStatusType returnsStatusType = null;
		if (returnsStatus != null && !returnsStatus.equals("")) {
			returnsStatusType = ReturnsStatusType.switchStatus(returnsStatus);
		} else {
			returnsStatusType = null;
		}
		if (expressNum == null || expressNum.equals("")) {
			expressNum = null;
		}
		if (returnsStatus == null || "".equals(returnsStatus)) {
			returnsStatus = null;
		}
		orderstutas = "RENT";
		Calendar startTime = null;
		Calendar endTime = null;
		try {
			if (startTimeStr != null && !"".equals(startTimeStr)) {
				startTime = CommonMethod.StringToCalendar(startTimeStr,
						"yyyy/MM/dd");
			}
			if (endTimeStr != null && !"".equals(endTimeStr)) {
				endTime = CommonMethod.StringToCalendar(endTimeStr,
						"yyyy/MM/dd");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		int count = returnsService.countRentReturns(orderId, returnsNum, orderNum, userId, expressNum, returnsStatusType, startTime, endTime,orderstutas);
		List<Returns> list = returnsService.listRentReturns(orderId, returnsNum, userId, expressNum, returnsStatusType, startTime, endTime, page, pageSize,orderstutas);
		
		List<ReturnsDto> dtos = new ArrayList<ReturnsDto>();
		for(Returns r:list){
			ReturnsDto dto = returnsService.castFromReturns(r);
			dtos.add(dto);
		}
		
		
		Map<String, Object> map = new HashMap<String, Object>();
		//当前页
		//总记录数
		//总页数
		//起始记录
		//截止记录
		//数据
		map.put("page", page);
		map.put("records", count);
		
		int totalpage = count/10;
		if(count%10!=0){
			totalpage+=1;
		}
		map.put("totalpage", totalpage);
		
		int displayStart = (page-1)*10+1;
		map.put("displayStart", displayStart);
		
		int displayEnd = page*10;
		if(count<displayEnd){
			displayEnd = count;
		}
		map.put("displayEnd", displayEnd);
		
		map.put("returns", dtos);
		
		/*ListParam listParam = returnsService.queryReturns(orderId, returnsNum,
				userId, expressNum, returnsStatusType, startTime, endTime,
				page, pageSize);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setsEcho(sEcho);*/
		return map;
	}
	
	/**
	 * 获取用户退货订单(用于手机端官网)
	 * @param userId
	 * @return
	 */
	@RequestMapping(value = "getUserReturnOrder.do")
	@ResponseBody
	public ObjectResult getUserReturnOrder(Long userId, String orderStatus){
		ObjectResult result = new ObjectResult();
		try {
			List<Returns> list = returnsService.listReturns(null, null, userId, null, null, null, null, 1, 9999, orderStatus);
			List<ReturnsDto> dtos = new ArrayList<ReturnsDto>();
			if(list.size()>0){
				for(Returns r:list){
					ReturnsDto dto = returnsService.castFromReturns(r);
					dtos.add(dto);
				}
			}
			result.setResult(dtos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
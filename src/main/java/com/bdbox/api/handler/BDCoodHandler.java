package com.bdbox.api.handler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.api.dto.BDCoodDto;
import com.bdbox.api.dto.RefereesUserDto;
import com.bdbox.constant.BdcoodStatus;
import com.bdbox.entity.BDCood;
import com.bdbox.entity.User;
import com.bdbox.service.BDCoodService;
import com.bdbox.service.UserService;
import com.bdbox.util.LogUtils;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.CommonMethod;

@Controller
public class BDCoodHandler {

	@Autowired
	private BDCoodService bdcoodService;

	@Autowired
	private UserService userService;

	// 北斗码页面查询
	@RequestMapping(value = "listBDCood.do")
	public @ResponseBody
	ListParam listProductDatatable(@RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength, @RequestParam int iColumns,
			@RequestParam String sEcho, @RequestParam Integer iSortCol_0,
			@RequestParam String sSortDir_0,
			@RequestParam(required = false) String bdnumber,
			@RequestParam(required = false) String ordernumber,
			@RequestParam(required = false) String userpeople,
			@RequestParam(required = false) String refereespeople,
			@RequestParam(required = false) String isuser,
			@RequestParam(required = false) String startTimeStrs,
			@RequestParam(required = false) String endTimeStr,
			@RequestParam(required = false) String creantTime,
			@RequestParam(required = false) String refereespeoplename,
			@RequestParam(required = false) String bdcoodtype) {
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		if (isuser.equals("")) {
			isuser = null;
		} else {
			if (isuser.equals("是否已使用")) {
				isuser = null;
			}
		}

		if (bdcoodtype.equals("")) {
			bdcoodtype = null;
		} else {
			if (bdcoodtype.equals("类型")) {
				bdcoodtype = null;
			}
		}

		if (bdnumber.equals("")) {
			bdnumber = null;
		}
		if (ordernumber.equals("")) {
			ordernumber = null;
		}
		if (userpeople.equals("")) {
			userpeople = null;
		}
		if (refereespeople.equals("")) {
			refereespeople = null;
		}

		if (refereespeoplename.equals("")) {
			refereespeoplename = null;
		}

		Calendar startTime = null;
		Calendar endTime = null;
		Calendar creatTime = null;

		try {
			if (startTimeStrs != null && !"".equals(startTimeStrs)) {
				startTime = CommonMethod.StringToCalendar(startTimeStrs,
						"yyyy/MM/dd");
			}
			if (endTimeStr != null && !"".equals(endTimeStr)) {
				endTime = CommonMethod.StringToCalendar(endTimeStr,
						"yyyy/MM/dd");
			}
			if (creantTime != null && !"".equals(creantTime)) {
				creatTime = CommonMethod.StringToCalendar(creantTime,
						"yyyy/MM/dd");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<BDCood> list = bdcoodService.listBDcood(bdnumber, ordernumber,
				userpeople, refereespeople, isuser, startTime, endTime,
				creatTime, refereespeoplename, bdcoodtype, page, pageSize);
		List<BDCoodDto> dtos = new ArrayList<BDCoodDto>();
		for (BDCood p : list) {
			BDCoodDto dto = bdcoodService.castFrom(p);
			dtos.add(dto);
		}
		int count = bdcoodService.count(bdnumber, ordernumber, userpeople,
				refereespeople, isuser, startTime, endTime, creatTime,
				refereespeoplename, bdcoodtype);

		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(dtos);
		return listParam;
	}

	// 用户查询
	@RequestMapping(value = "/queryUserOptions.do", method = RequestMethod.GET)
	public @ResponseBody
	ObjectResult queryUserOptions(
			@RequestParam(required = false) Long selectUserId,
			@RequestParam(required = false) Long parentUserId,
			@RequestParam(required = false) int page,
			@RequestParam(required = false) int pageSize) {
		ObjectResult objectResult = new ObjectResult();
		try {
			String result = userService.getUserSeclectOptions(selectUserId,
					page, pageSize);
			objectResult.setResult(result);
			objectResult.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}

	// 生成北斗码
	@RequestMapping(value = "/createBDcood.do")
	public @ResponseBody
	ObjectResult createBDcood(@RequestParam(required = false) String BDtype,
			@RequestParam(required = false) String saveBuyStartTimeStr,
			@RequestParam(required = false) String saveBuyEndTimeStr,
			@RequestParam(required = false) String count,
			@RequestParam(required = false) String price,
			@RequestParam(required = false) String tuijianren,
			@RequestParam(required = false) String remarkname,
			@RequestParam(required = false) int dayNum,
			@RequestParam(required = false) Double discount,
			@RequestParam(required = false) String createCondition) { 
		ObjectResult objectResult = new ObjectResult();
		try {

			if (BDtype.equals("") || BDtype == null) {
				objectResult.setMessage("北斗码性质不能为空");
				objectResult.setStatus(ResultStatus.FAILED);
				return objectResult;
			}
			if (saveBuyStartTimeStr.equals("") || saveBuyStartTimeStr == null) {
				objectResult.setMessage("有效期的开始时间不能为空");
				objectResult.setStatus(ResultStatus.FAILED);
				return objectResult;
			}
			if (saveBuyEndTimeStr.equals("") || saveBuyEndTimeStr == null) {
				objectResult.setMessage("北斗码结束时间不能为空");
				objectResult.setStatus(ResultStatus.FAILED);
				return objectResult;
			}
			if (count.equals("") || count == null) {
				objectResult.setMessage("生成北斗码的数量不能为空");
				objectResult.setStatus(ResultStatus.FAILED);
				return objectResult;
			}
			if (BDtype.equals("BDM")) {
				if (price.equals("") || price == null) {
					objectResult.setMessage("金额不能为空");
					objectResult.setStatus(ResultStatus.FAILED);
					return objectResult;
				}
			} else {
				price = null;
			}
			if (!"".equals(tuijianren) && tuijianren!=null) {
				if ("".equals(remarkname) || remarkname==null) {
					objectResult.setMessage("推荐人备注名不能为空！");
					objectResult.setStatus(ResultStatus.FAILED);
					return objectResult;
				}
			}

			objectResult = bdcoodService.saveBdCood(BDtype,
					saveBuyStartTimeStr, saveBuyEndTimeStr, count, price,
					tuijianren, remarkname, dayNum, discount, createCondition);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}

	/**
	 * 北斗码推荐人信息统计页面
	 * 
	 * @param 用户id
	 * @return
	 */
	@RequestMapping(value = "listRefereesUser.do")
	public @ResponseBody
	ListParam listRefereesUser(@RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength, @RequestParam int iColumns,
			@RequestParam String sEcho, @RequestParam Integer iSortCol_0,
			@RequestParam String sSortDir_0,
			@RequestParam(required = false) String username,
			@RequestParam(required = false) String refereespeoplename) {
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		List<RefereesUserDto> dtos = new ArrayList<RefereesUserDto>();
		List<User> listuser = userService.getRefeesUser(username,
				refereespeoplename, page, pageSize);
		int count = userService
				.getRefeesUserCount(username, refereespeoplename);
		for (User user1 : listuser) {
			String names = user1.getUsername();
			BDCood bdcood = bdcoodService.getRefeesUser_wl5(names);
			int num = bdcoodService.getRefeesUserCount_wl(names);// 获取手持北斗码的数量
			int num1 = bdcoodService.getRefeesUserCount_wl1(names);// 已经使用的数量
			int num2 = bdcoodService.getRefeesUserCount_wl2(names);// 未使用的数量
			int num3 = bdcoodService.getRefeesUserCount_wl3(names);// 已经过期的数量
			int num4 = bdcoodService.getRefeesUserCount_wl4(names);// 未过期的北斗码数量
			RefereesUserDto refereesUserDto = new RefereesUserDto();
			if (bdcood != null) {
				if (user1.getRecommend() != null) {
					refereesUserDto.setBeizhuname(user1.getRecommend());
				} else {
					refereesUserDto.setBeizhuname("");
				}
				refereesUserDto.setRefereesUser(bdcood.getReferees());
				refereesUserDto.setId(String.valueOf(bdcood.getId()));
				refereesUserDto.setMony(String.valueOf(num1 * 100));
				refereesUserDto.setNums(String.valueOf(num));
				refereesUserDto.setUsernums(String.valueOf(num1));
				refereesUserDto.setNotusenums(String.valueOf(num2));
				refereesUserDto.setDatednums(String.valueOf(num3));
				refereesUserDto.setNotdatednums(String.valueOf(num4));
				dtos.add(refereesUserDto);
			}
		}
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(dtos);
		return listParam;
	}

	@RequestMapping(value = { "/getbdcood.do" }, method = { RequestMethod.POST })
	@ResponseBody
	public ObjectResult getbdcood(@RequestParam(required = false) Long id) {
		BDCoodDto dto = new BDCoodDto();
		if ((id != null) && (!"".equals(id))) {
			BDCood bdcood = bdcoodService.getbdcood(id);
			dto = bdcoodService.castFrom(bdcood);
		}
		ObjectResult objectResult = new ObjectResult();
		objectResult.setResult(dto);
		objectResult.setStatus(ResultStatus.OK);
		return objectResult;
	}

	@RequestMapping(value = "updateBDCood.do")
	@ResponseBody
	public ObjectResult updateBDCood(@RequestParam(required = false) String id,
			@RequestParam(required = false) String bdcood,
			@RequestParam(required = false) String updateBuyStartTimeStr,
			@RequestParam(required = false) String updateBuyEndTimeStr,
			@RequestParam(required = false) String bdtypeid,
			@RequestParam(required = false) int dayNum) {
		Calendar StartTime = null;
		Calendar EndTime = null;
		ObjectResult objectResult = new ObjectResult();
		BdcoodStatus bdcoodStatus = BdcoodStatus.switchStatus(bdtypeid);
		boolean result = false;
		try {
			StartTime = CommonMethod.StringToCalendar(updateBuyStartTimeStr,
					"yyyy-MM-dd HH:mm:ss");
			EndTime = CommonMethod.StringToCalendar(updateBuyEndTimeStr,
					"yyyy-MM-dd HH:mm:ss");
		} catch (Exception e) {
			LogUtils.logerror("字符串格式时间转Calendar时间异常", e);
		}
		if ((id != null) && (!"".equals(id))) {
			BDCood enty = bdcoodService.getbdcood(Long.valueOf(id));
			if (enty != null) {
				enty.setStartTime(StartTime);
				enty.setEndTime(EndTime);
				enty.setBdcoodStatus(bdcoodStatus);
				enty.setDayNum(dayNum);
				result = bdcoodService.update(enty);
			} else {
				result = false;
			}
		}
		if (result) {
			objectResult.setStatus(ResultStatus.OK);
		} else {
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}

	// 北斗码通道
	@RequestMapping(value = "/checkoutBDcood.do", method = RequestMethod.POST)
	public @ResponseBody
	ObjectResult bindBox(@RequestParam(required = true) String bdcoodnember) {
		ObjectResult objectResult = new ObjectResult();
		String tapes = "";
		if (bdcoodnember != null && !bdcoodnember.equals("")) {
			tapes = bdcoodnember.substring(0, 3);
		}
		if (bdcoodnember == null) {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("北斗码不能为空");
			return objectResult;
		} else if (!tapes.equals("") && !tapes.equalsIgnoreCase("BDM")) {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("errors");
			return objectResult;
		} else {
			BDCood bcdood = bdcoodService.getbdcood_wl(bdcoodnember);
			if (bcdood != null) {
				if (bcdood.getBdcoodStatus().toString().equals("USER")) {
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("user");
				} else if (bcdood.getEndTime().getTime().getTime() < Calendar
						.getInstance().getTime().getTime()
						|| bcdood.getBdcoodStatus().toString()
								.equals(BdcoodStatus.EXCEED)) {
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("Notthrough");
				} else if (bcdood.getStartTime().getTimeInMillis() > 
						Calendar.getInstance().getTimeInMillis()){
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("notusetime");
				} else {
					objectResult.setStatus(ResultStatus.OK);
					objectResult.setMessage("through");
				}
			} else {
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("error");
			}
		}
		return objectResult;
	}

	// 北斗券通道
	@RequestMapping(value = "/checkoutTicket.do", method = RequestMethod.POST)
	public @ResponseBody
	ObjectResult checkoutTicket(
			@RequestParam(required = true) String bdcoodnember) {
		ObjectResult objectResult = new ObjectResult();
		String tapes = "";
		if (bdcoodnember != null && !bdcoodnember.equals("")) {
			tapes = bdcoodnember.substring(0, 3);
		}
		if (bdcoodnember == null) {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("北斗券不能为空");
			return objectResult;
		} else if (!tapes.equals("") && !tapes.equalsIgnoreCase("BDQ")) {
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("errors");
			return objectResult;
		} else {
			BDCood bcdood = bdcoodService.getbdcood_wl(bdcoodnember);
			if (bcdood != null) {
				if (bcdood.getBdcoodStatus().toString().equals("USER")) {
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("user");
				} else if (bcdood.getBdcoodStatus().toString().equals("EXCEED")) {
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("Notthrough");
				} else if (bcdood.getStartTime().getTimeInMillis() > 
					Calendar.getInstance().getTimeInMillis()){
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("notusetime");
				} else {
					objectResult.setStatus(ResultStatus.OK);
					objectResult.setMessage("through");
					objectResult.setResult(bcdood);
				}
			} else {
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("error");
			}
		}
		return objectResult;
	}

	// 使用北斗码(北斗券)
	@RequestMapping(value = "/submitBDcood.do", method = RequestMethod.POST)
	public @ResponseBody
	ObjectResult submitBDcood(@RequestParam(required = true) String bdcoodnember) {
		ObjectResult objectResult = new ObjectResult();
		boolean result = false;
		if (bdcoodnember == null) {
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		} else {
			BDCood bcdood = bdcoodService.getbdcood_wl(bdcoodnember);
			if (bcdood != null) {
				result = true;
			} else {
				result = false;
			}
		}
		if (result) {
			objectResult.setStatus(ResultStatus.OK);
		} else {
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}

	@RequestMapping(value = { "/gettuijianren.do" }, method = { RequestMethod.POST })
	@ResponseBody
	public ObjectResult gettuijianren(
			@RequestParam(required = false) String tuijianren) {
		ObjectResult objectResult = new ObjectResult();
		String tjname = "";
		if (!tuijianren.equals("") || tuijianren != null) {
			User user = userService.getUserByusername(tuijianren);
			if (user != null && user.getRecommend() != null) {
				tjname = user.getRecommend();
			} else {
				tjname = "";
			}
		}
		objectResult.setMessage(tjname);
		return objectResult;
	}

	// 北斗码通道
	@RequestMapping(value = "/findBDcood.do", method = RequestMethod.POST)
	public @ResponseBody
	ObjectResult findBDcood(@RequestParam(required = true) String bdcoodnember) {
		ObjectResult objectResult = new ObjectResult();
		BDCood bcdood = bdcoodService.getbdcood_wl(bdcoodnember);
		BDCoodDto dto = bdcoodService.castFrom(bcdood);
		if (bcdood != null) {
			objectResult.setMessage("查询成功！");
			objectResult.setResult(dto);
			objectResult.setStatus(ResultStatus.OK);
		} else {
			objectResult.setMessage("查询失败");
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}

}

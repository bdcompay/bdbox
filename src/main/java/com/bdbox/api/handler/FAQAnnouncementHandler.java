package com.bdbox.api.handler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.api.dto.AnnouncementDto;
import com.bdbox.api.dto.FAQDto;
import com.bdbox.api.dto.NotificationDto;
import com.bdbox.api.dto.UserQuestionDto;
import com.bdbox.constant.MailType;
import com.bdbox.constant.QuestionType;
import com.bdbox.entity.Announcement;
import com.bdbox.entity.FAQ;
import com.bdbox.entity.Notification;
import com.bdbox.entity.SmallType;
import com.bdbox.entity.UserQuestion;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.AnnouncementService;
import com.bdbox.service.FAQService;
import com.bdbox.service.MessageRecordService;
import com.bdbox.service.NotificationService;
import com.bdbox.service.SmallTypeService;
import com.bdbox.service.UserQuestionService;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.CommonMethod;

@Controller
public class FAQAnnouncementHandler {
	
	@Autowired
	private AnnouncementService announcementService;
	@Autowired
	private FAQService fAQService;
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private SmallTypeService smallTypeService;
	@Autowired
	private UserQuestionService userQuestionService;
	@Autowired  
	private MailNotification mailNotification; 
	@Autowired
	private MessageRecordService messageRecordService;
 
	/**
	 * 公告管理页面
	 * @param 用户id 
 	 * @return
	 */
	@RequestMapping(value="listAnnouncementtable.do")
	public @ResponseBody ListParam listAnnouncementtable(
		   @RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
		   @RequestParam int iColumns, @RequestParam String sEcho,
		   @RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0){
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		List<AnnouncementDto> dtos = new ArrayList<AnnouncementDto>();
		List<Announcement> listuser=announcementService.getlistAnnouncementtable(page,pageSize); 
		int count=announcementService.getlistAnnouncementtableCount();
  		for(Announcement announcement:listuser){
  			AnnouncementDto announcementDto=new AnnouncementDto();
  			announcementDto.setId(announcement.getId());
  			announcementDto.setCenter(announcement.getCenter());
  			announcementDto.setHyperlink(announcement.getHyperlink());
  			announcementDto.setCreatTime(CommonMethod.CalendarToString(announcement.getCreatTime(), "yyyy/MM/dd HH:mm:ss")); 
 			dtos.add(announcementDto);
  		   } 
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(dtos);
		return listParam;
	}
	

	//新增公告
	@RequestMapping(value="addAnnouncement.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult add(
		   @RequestParam(required=false) String announcementname,
		   @RequestParam(required=false) String hyperlink ){ 
		Announcement announcement = announcementService.save(announcementname, hyperlink); 
		ObjectResult obj = new ObjectResult();
		if(announcement==null){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("添加失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("添加成功");
		}
		return obj;
	}
	
	//查询单条公告
	@RequestMapping(value="getAnnouncement.do")
	public @ResponseBody ObjectResult getProduct(Long id){
		ObjectResult objectResult = new ObjectResult();
		Announcement announcement = announcementService.get(id);
		AnnouncementDto dto = announcementService.castFrom(announcement);
		if(announcement!=null){
			objectResult.setMessage("查询成功！");
			objectResult.setResult(dto);
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setMessage("查询失败");
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	//修改公告
	@RequestMapping(value="updateAnnouncement.do")
	public @ResponseBody ObjectResult update(Long id,
			@RequestParam(required=false) String updateannouncementname,
			@RequestParam(required=false) String updatehyperlink){
		ObjectResult obj = new ObjectResult();  
		boolean b = announcementService.update(id, updateannouncementname, updatehyperlink); 
		if(!b){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("修改失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("修改成功");
		}
		return obj;
	}
	
	//删除公告
	@RequestMapping(value="deleteAnnouncement.do")
	public @ResponseBody ObjectResult delete(Long id){
		ObjectResult obj = new ObjectResult(); 
		boolean b = announcementService.deleteAnnouncement(id); 
		if(!b){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("删除失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("删除成功");
		}
		obj.setResult(b);
		return obj;
	}
	
	/**
	 * 首页公告查询
	 * @param 用户id 
 	 * @return
	 */
	@RequestMapping(value="findAnnouncement.do")
	public @ResponseBody List findAnnouncement(){ 
		List<AnnouncementDto> dtos = new ArrayList<AnnouncementDto>();
		List<Announcement> listuser=announcementService.getfindAnnouncement(); 
   		for(Announcement announcement:listuser){
  			AnnouncementDto announcementDto=new AnnouncementDto();
  			announcementDto.setId(announcement.getId());
  			announcementDto.setCenter(announcement.getCenter());
  			announcementDto.setHyperlink(announcement.getHyperlink());
  			announcementDto.setCreatTime(CommonMethod.CalendarToString(announcement.getCreatTime(), "yyyy/MM/dd HH:mm:ss")); 
 			dtos.add(announcementDto);
  		   }  
		return dtos;
	}
	
	
	/**
	 * FAQ管理页面
	 * @param 用户id 
 	 * @return
	 */
	@RequestMapping(value="listFAQtable.do")
	public @ResponseBody ListParam listFAQtable(
		   @RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
		   @RequestParam int iColumns, @RequestParam String sEcho,
		   @RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0,
			@RequestParam(required = false) String questiontype){
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		
		if("".equals(questiontype)){
			questiontype=null;
		}
		if(questiontype.equals("问题归类")){
			questiontype=null;
		}
		List<FAQDto> dtos = new ArrayList<FAQDto>();
		List<FAQ> listuser=fAQService.getlistFAQtable(questiontype,page,pageSize); 
		int count=fAQService.getlistFAQtable(questiontype);
  		for(FAQ faq:listuser){
  			FAQDto faqto=new FAQDto();
  			faqto.setId(faq.getId());
  			if(faq.getQuestionType()==QuestionType.GENERALQUESTION){
   	  			faqto.setQuestionType("常见问题");
  			}else if(faq.getQuestionType()==QuestionType.NEWGUIDE){
   	  			faqto.setQuestionType("新手指南"); 
  			}else if(faq.getQuestionType()==QuestionType.OUTDOORS){
   	  			faqto.setQuestionType("户外知识"); 
  			}else if(faq.getQuestionType()==QuestionType.NOUNEXPLAIN){
   	  			faqto.setQuestionType("名词解释"); 
  			}else if(faq.getQuestionType()==QuestionType.USERQUESTION){
  				faqto.setQuestionType("用户提问");
  			}
  			faqto.setSmallTypename(faq.getSmallTypename());
  			faqto.setAnswer(faq.getAnswer());
  			faqto.setQuestion(faq.getQuestion());
  			faqto.setCreatTime(CommonMethod.CalendarToString(faq.getCreatTime(), "yyyy/MM/dd HH:mm:ss")); 
 			dtos.add(faqto);
  		   } 
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(dtos);
		return listParam;
	}
	
	
	//新增问题
	@RequestMapping(value="addFAQ.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult addFAQ(
		   @RequestParam(required=false) String questionname,
		   @RequestParam(required=false) String questionvlaue,
		   @RequestParam(required=false) String questiontype,
		   @RequestParam(required=false) String addsmalltype,
		   @RequestParam(required=false) String num){ 
		FAQ faq = fAQService.saveFaq(questionname, questionvlaue,questiontype,addsmalltype,num); 
		ObjectResult obj = new ObjectResult(); 
		if(faq==null){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("添加失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("添加成功");
			//如果问题是用户提交的，则更新用户问题表，把时状态设为已经解决
			if(!"new".equals(num)){
				UserQuestion enty=userQuestionService.getenty(Long.valueOf(num));
				enty.setIssolve(true);
				userQuestionService.update(enty); 
				String type="solvequestion";  //通知类型 
				String type_twl="customer"; 
			    Notification notification=notificationService.getnotification(type,type_twl); 
				if(notification!=null){
					if(notification.getIsnote().equals(true)){ 
						mailNotification.sendNote(enty.getUserid(), notification.getNotecenter());
						messageRecordService.save(enty.getUserid(), 
								notification.getNotecenter(), MailType.switchStatus(type));
					}
				} 
				
			}
		}
		return obj;
	}
	
	//查询单条问题
	@RequestMapping(value="getFAQ.do")
	public @ResponseBody ObjectResult getFAQ(Long id){
		ObjectResult objectResult = new ObjectResult();
		FAQ faq = fAQService.getfaq(id);
		FAQDto dto = fAQService.castFrom(faq);
		if(faq!=null){
			objectResult.setMessage("查询成功！");
			objectResult.setResult(dto);
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setMessage("查询失败");
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	//更新faq问题
	@RequestMapping(value="updateFAQ.do")
	public @ResponseBody ObjectResult updateFAQ(Long id,
			@RequestParam(required=false) String updatequestionname,
			@RequestParam(required=false) String updatequestionvlaue,
			@RequestParam(required=false) String updatequestiontype,
			@RequestParam(required=false) String updatesmalltype){
		ObjectResult obj = new ObjectResult();  
		boolean b = fAQService.update(id, updatequestionname, updatequestionvlaue,updatequestiontype,updatesmalltype); 
		if(!b){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("修改失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("修改成功");
		}
		return obj;
	}
	
	//删除问题
	@RequestMapping(value="deleteFAQ.do")
	public @ResponseBody ObjectResult deleteFAQ(Long id){
		ObjectResult obj = new ObjectResult(); 
		boolean b = fAQService.deleteFAQ(id); 
		if(!b){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("删除失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("删除成功");
		}
		obj.setResult(b);
		return obj;
	}
	
	/**
	 * 通知管理页面
	 * @param 用户id 
 	 * @return
	 */
	@RequestMapping(value="listNotification.do")
	public @ResponseBody ListParam listNotification(
		   @RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
		   @RequestParam int iColumns, @RequestParam String sEcho,
		   @RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0,
		   @RequestParam(required = false) String type_twl,
		   @RequestParam(required = false) String tongzhileixing){
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		
	/*	if(tongzhifanshi==null){
			tongzhifanshi=null;
		}else{
			if("".equals(tongzhifanshi)){
				tongzhifanshi=null;
			}else if("通知方式".equals(tongzhifanshi)){
				tongzhifanshi=null;
			} 
		} */
		String tongzhifanshi=null;
		if(tongzhileixing==null){
			tongzhileixing=null;
		}else{
			if("".equals(tongzhileixing)){
				tongzhileixing=null;
			}else if("通知类型".equals(tongzhileixing)){
				tongzhileixing=null;
			} 
		} 
		List<NotificationDto> dtos = new ArrayList<NotificationDto>();
		List<Notification> notification=notificationService.getlistNotification(tongzhifanshi, tongzhileixing,type_twl, page, pageSize);
		int count=notificationService.getlistNotificationcount(tongzhifanshi,tongzhileixing,type_twl);
  		for(Notification en:notification){
  			NotificationDto notificationDto=notificationService.castFrom(en);
 			dtos.add(notificationDto);
  		   } 
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(dtos);
		return listParam;
	}
	
	
	//查询单条通知
	@RequestMapping(value="getNotification.do")
	public @ResponseBody ObjectResult getNotification(Long id){
		ObjectResult objectResult = new ObjectResult();
		Notification notification = notificationService.getNotification(id);
		NotificationDto dto = notificationService.castFrom(notification);
		if(notification!=null){
			objectResult.setMessage("查询成功！");
			objectResult.setResult(dto);
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setMessage("查询失败");
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	//新增通知
	@RequestMapping(value="addNotification.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult addNotification(
		   @RequestParam(required=false) String isemail,
		   @RequestParam(required=false) String note, 
		   @RequestParam(required=false) String tongzhileixing,
		   @RequestParam(required=false) String frommail,
		   @RequestParam(required=false) String toemail,
		   @RequestParam(required=false) String subject,
		   @RequestParam(required=false) String center,
		   @RequestParam(required=false) String tomailname,
		   @RequestParam(required=false) String TeliPhone,
		   @RequestParam(required=false) String notecenter,
		   @RequestParam(required=false) String type_twl){ 
		ObjectResult obj = new ObjectResult(); 
		Notification enty=notificationService.getnotification(tongzhileixing,null);
		if(enty!=null){
			if("1".equals(isemail)){
				enty.setIsemail(true);
			}
			if("1".equals(note)){
				enty.setIsnote(true);
			}
			MailType str=MailType.switchStatus(tongzhileixing);
			enty.setMailType(str);
			enty.setFrommail(frommail);
			enty.setTomail(toemail);
			enty.setSubject(subject);
			enty.setCenter(center);
			enty.setName(tomailname);
			enty.setSendTeliPhone(TeliPhone);
			enty.setNotecenter(notecenter); 
			notificationService.updateNotification(enty); 
 		}else{ 
			Notification notification = notificationService.saveNotification(isemail, tongzhileixing,frommail,toemail,subject,center,note,tomailname,TeliPhone,notecenter,type_twl); 
			if(notification==null){
				obj.setStatus(ResultStatus.FAILED);
				obj.setMessage("添加失败");
			}else{
				obj.setStatus(ResultStatus.OK);
				obj.setMessage("添加成功");
			}
		}
		return obj;
	}
	
	//修改通知
	@RequestMapping(value="updateNotification.do")
	public @ResponseBody ObjectResult updateNotification(Long id,
			@RequestParam(required=false) String isemail,
			@RequestParam(required=false) String note,
			@RequestParam(required=false) String tongzhileixing,
			@RequestParam(required=false) String frommail,
			@RequestParam(required=false) String upedatetomailname,
			@RequestParam(required=false) String updateemail,
			@RequestParam(required=false) String updateTeliPhone,
			@RequestParam(required=false) String updatesubject,
			@RequestParam(required=false) String updatecenter,
			@RequestParam(required=false) String updatenotecenter){
		ObjectResult obj = new ObjectResult();  
		boolean b = notificationService.update(id, isemail, note,tongzhileixing,frommail,upedatetomailname,updateemail,updateTeliPhone,updatesubject,updatecenter,updatenotecenter); 
		if(!b){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("修改失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("修改成功");
		}
		return obj;
	}
		
	//删除FAQ问题
	@RequestMapping(value="deleteNotification.do")
	public @ResponseBody ObjectResult deleteNotification(Long id){
		ObjectResult obj = new ObjectResult(); 
		boolean b = notificationService.deleteNotification(id); 
		if(!b){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("删除失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("删除成功");
		}
		obj.setResult(b);
		return obj;
	}
	
	
	 
	@RequestMapping(value="updateFAQ1.do")
	public @ResponseBody ObjectResult updateFAQ(
			@RequestParam(required=false) String updatetype,
			@RequestParam(required=false) String updatetype_twl,
			@RequestParam(required=false) String updatetypename){
		ObjectResult obj = new ObjectResult();  
		boolean b = fAQService.updates(updatetype, updatetype_twl,updatetypename); 
		if(!b){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("修改失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("修改成功");
		}
		return obj;
	}
	
	
	//查询每个大类中的六个小类，此方法针对FAQ查询
	@RequestMapping(value = "/queryTypeOptions.do", method = RequestMethod.GET)
	public @ResponseBody ObjectResult queryUserOptions(
			@RequestParam(required = false) String updatetype,
 			@RequestParam(required = false) int page,
			@RequestParam(required = false) int pageSize) {
		ObjectResult objectResult = new ObjectResult();
		try {
			String result = smallTypeService.gettypeSeclectOptions(updatetype);
			objectResult.setResult(result);
			objectResult.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}
	
	//新增FAQ问题类型
	@RequestMapping(value="addFAQtype.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult addFAQtype(
		   @RequestParam(required=false) String addtype,
		   @RequestParam(required=false) String Englishtypename,
		   @RequestParam(required=false) String chaintypename){ 
		SmallType enty=smallTypeService.gettype(null, Englishtypename);
		boolean result=false;
		if(enty==null){
			SmallType smallType = smallTypeService.saveFaqtype(addtype, Englishtypename,chaintypename); 
			result=true;
		}else{
			enty.setName(chaintypename);
			smallTypeService.updates(enty);
			result=true;
		} 
		ObjectResult obj = new ObjectResult();
		if(!result){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("添加失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("添加成功");
		}
		return obj;
	}
	
	
	//修改faq类型
	@RequestMapping(value="updateFAQtype.do")
	public @ResponseBody ObjectResult updateFAQtype( 
			@RequestParam(required=false) String updatetype,
			@RequestParam(required=false) String type,
			@RequestParam(required=false) String updatetypename){
		ObjectResult obj = new ObjectResult();  
		boolean b=smallTypeService.update(updatetype,type,updatetypename);
 		if(!b){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("修改失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("修改成功");
		}
		return obj;
	}
	
	
	//批量删除FAQ问题
	@RequestMapping(value="deletefaq.do")
	public @ResponseBody ObjectResult deletefaq(String id){
		ObjectResult obj = new ObjectResult();
		boolean result=false;
		if(!"".equals(id)){
			String[] ids=id.split(",");
			for(String faqid:ids){
				result=fAQService.deleteFAQ(Long.valueOf(faqid));
			}
			if(result){
				obj.setStatus(ResultStatus.OK);
				obj.setMessage("操作成功");
				obj.setResult(result);
			}else{
				obj.setStatus(ResultStatus.FAILED);
				obj.setMessage("操作失败");
				obj.setResult(result);
			}
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("删除成功");
			obj.setResult(result);
		} 
		 
		return obj;
	}
	
	
	/* 
	 * 查询faq类型
	 */
	@RequestMapping(value = "/Faqlist.do", method = RequestMethod.POST)
	public @ResponseBody List querytype(
			@RequestParam(required = true) String keyword,
			@RequestParam(required = true) String lefttype,
			@RequestParam(required = true) String toptype) { 
		   if("".equals(lefttype)){
			   lefttype=null;
		   }
 		   List<SmallType> list=smallTypeService.getalltype(lefttype); 
		   return list;
	}
	
	/* 
	 * 查询faq一问一答
	 */
	@RequestMapping(value = "/FAQquestionlist.do", method = RequestMethod.POST)
	public @ResponseBody Map FAQquestionlist(
			@RequestParam(required = true) String keyword,
			@RequestParam(required = true) String lefttype,
			@RequestParam(required = true) String toptype,
			@RequestParam(required = false) int page) { 
		   int pageSize = 10;
		   if("".equals(keyword)){
			   keyword=null;
		   }
		   if("".equals(lefttype)){
			   lefttype=null;
		   }
		   if("".equals(toptype)){
			   toptype=null;
		   } 
		   Map list=fAQService.getFAQquestionlist(lefttype,keyword,toptype,page,pageSize); 
 		   System.out.println("list的长度"+list.size());
		   return list;
	}
	
	/* 
	 * 用户提交问题
	 */
	@RequestMapping(value = "/submitmyquestion.do")
	public @ResponseBody String submitmyquestion(
			@RequestParam(required = true) String myquestion,
			@RequestParam(required = true) String user) { 
		   UserQuestion enty=new UserQuestion();
		   String result="";
 		   enty.setSubmitTime(Calendar.getInstance());
		   enty.setIssolve(false);
		   enty.setMyquestion(myquestion);
		   enty.setUserid(user);
		   enty=userQuestionService.save(enty);
		   String mailcentent="";
		   String subject=""; 
		   String tomail=""; 
		   String type="submitquestion";  //通知类型 
			String type_twl="admin"; 
		   if(enty!=null){
			   //list=userQuestionService.getuserquestion(user);
			   result="OK";
			   Notification notification=notificationService.getnotification(type,type_twl); 
				if(notification!=null){
					if(notification.getIsemail().equals(true)){
						mailcentent=notification.getCenter();
						subject=notification.getSubject();
						tomail=notification.getTomail();
						String [] email=tomail.split(",");
						for(String str:email){
	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
						}
					}
				} 
			   
		   }else{
			   result="FAILD";
		   }
		   return result;
	}
	
	/* 
	 * 查询用户提交的问题
	 */
	@RequestMapping(value = "/queryuserquestion.do", method = RequestMethod.POST)
	public @ResponseBody List queryuserquestion(
 			@RequestParam(required = true) String user) { 
		     List<UserQuestionDto> list=null; 
			 list=userQuestionService.getuserquestion(user); 
		    return list;
	}
	
	/**
	 * 用户提交问题后台管理页面
	 * @param 用户id 
 	 * @return
	 */
	@RequestMapping(value="userquestiontable.do")
	public @ResponseBody ListParam userquestiontable(
		   @RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
		   @RequestParam int iColumns, @RequestParam String sEcho,
		   @RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0,
 		   @RequestParam(required = false) String username){
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength; 
		if("".equals(username)){
			username=null;
		} 
 		List<UserQuestionDto> notification=userQuestionService.getuserquestionlist(username, page, pageSize);
		int count=userQuestionService.getuserquestionlistcount(username); 
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(notification);
		return listParam;
	}
	
	//查询单条问题
	@RequestMapping(value="getquestion.do")
	public @ResponseBody ObjectResult getuserquestion(Long id){
		ObjectResult objectResult = new ObjectResult();
		UserQuestionDto userQuestionDto = userQuestionService.getquestion(id);
 		if(userQuestionDto!=null){
			objectResult.setMessage("查询成功！");
			objectResult.setResult(userQuestionDto);
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setMessage("查询失败");
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	//用户删除我的问题
	@RequestMapping(value="deletemyquestion.do")
	public @ResponseBody String deletemyquestion(Long id){
		ObjectResult objectResult = new ObjectResult();
		String status="";
		boolean result = userQuestionService.deletemyquestion(id);
 		if(result){
			objectResult.setMessage("删除成功");
			status="OK";
 			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setMessage("删除失败");
			status="FAILED"; 
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return status;
	}
	
	@RequestMapping(value="getAnnouncementList.do")
	@ResponseBody
	public Map<String, Object> getList(String content, int page){
		int pageSize = 10;
		List<AnnouncementDto> listDto = announcementService.getList(content, page, pageSize);
		int count = announcementService.count(content);
		Map<String, Object> map = new HashMap<String, Object>();
		
		//当前页
		map.put("page", page);
		//总记录数
		map.put("records", count);
		//总页数
		int totalpage = count/10;
		if(count%10!=0){
			totalpage+=1;
		}
		
		map.put("totalpage", totalpage);
		//起始记录
		int displayStart = (page-1)*10+1;
		map.put("displayStart", displayStart);
		
		//截止记录
		int displayEnd = page*10;
		if(count<displayEnd){
			displayEnd = count;
		}
		map.put("displayEnd", displayEnd);
		
		//数据
		map.put("returns", listDto);
		
		return map;
	}
	
}

package com.bdbox.api.handler;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.util.LogUtils;

/**
 * 云讯通 语音通知回调handler类
 * @author zouzhiwen
 *
 */
@Controller
public class YxtHandler {
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger fordebug = Logger.getLogger("fordebug");
	private static Logger forinfo = Logger.getLogger("forinfo");
	
	@RequestMapping({ "sosYunyinCallback.do" })
	@ResponseBody
	public String sosYunyinCallback(HttpServletRequest request,HttpServletResponse response) {
		String s="";
		try {
			java.io.BufferedInputStream bis=new java.io.BufferedInputStream(request.getInputStream());
			byte read[] = new byte[1024*1024]; 
			
			while( ( bis.read(read, 0, 1024*1024)) != -1 ) 
			{ 
			s+=new String(read,0,1024*1024);
			}
			System.out.println(s); 
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 HashMap map=xmlToMap(s);
		
		String notityXml = "";
		try {
			String inputLine;
			while ((inputLine = request.getReader().readLine()) != null) {
				// String inputLine;
				notityXml = notityXml + inputLine;
			}
			request.getReader().close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(notityXml);
		response.setContentType("application/xml"); 
		byte[] b=null;
		b=("<statuscode>000000</statuscode>").getBytes();
		try {
			response.getOutputStream().write(b);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response.toString();
	}

	private HashMap xmlToMap(String xml)
    {
        HashMap map = new HashMap();
        Document doc = null;
        try
        {
            doc = DocumentHelper.parseText(xml);
            Element rootElt = doc.getRootElement();
            HashMap hashMap2 = new HashMap();
            ArrayList arrayList = new ArrayList();
            for(Iterator i = rootElt.elementIterator(); i.hasNext();)
            {
                Element e = (Element)i.next();
                if("statusCode".equals(e.getName()) || "statusMsg".equals(e.getName()))
                    map.put(e.getName(), e.getText());
                else
                if("SubAccount".equals(e.getName()) || "TemplateSMS".equals(e.getName()) || "totalCount".equals(e.getName()) || "token".equals(e.getName()) || "callSid".equals(e.getName()) || "state".equals(e.getName()) || "downUrl".equals(e.getName()))
                {
                    if(!"SubAccount".equals(e.getName()) && !"TemplateSMS".equals(e.getName()))
                        hashMap2.put(e.getName(), e.getText());
                    else
                    if("SubAccount".equals(e.getName()))
                    {
                        HashMap hashMap3 = new HashMap();
                        Element e2;
                        for(Iterator i2 = e.elementIterator(); i2.hasNext(); hashMap3.put(e2.getName(), e2.getText()))
                            e2 = (Element)i2.next();

                        arrayList.add(hashMap3);
                        hashMap2.put("SubAccount", arrayList);
                    } else
                    if("TemplateSMS".equals(e.getName()))
                    {
                        HashMap hashMap3 = new HashMap();
                        Element e2;
                        for(Iterator i2 = e.elementIterator(); i2.hasNext(); hashMap3.put(e2.getName(), e2.getText()))
                            e2 = (Element)i2.next();

                        arrayList.add(hashMap3);
                        hashMap2.put("TemplateSMS", arrayList);
                    }
                    map.put("data", hashMap2);
                } else
                {
                    HashMap hashMap3 = new HashMap();
                    Element e2;
                    for(Iterator i2 = e.elementIterator(); i2.hasNext(); hashMap3.put(e2.getName(), e2.getText()))
                        e2 = (Element)i2.next();

                    if(hashMap3.size() != 0)
                        hashMap2.put(e.getName(), hashMap3);
                    else
                        hashMap2.put(e.getName(), e.getText());
                    map.put("data", hashMap2);
                }
            }
            
        }
        catch(DocumentException e)
        {
            e.printStackTrace();
            forerror.error(e.getMessage());
        }
        catch(Exception e)
        {
        	forerror.error(e.getMessage());
            e.printStackTrace();
        }
        return map;
    }
	 
	
	
}

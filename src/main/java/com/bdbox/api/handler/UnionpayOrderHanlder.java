package com.bdbox.api.handler;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.service.UnionpayOrderService;
import com.bdbox.unionpay.acp.consume.BackRcvResponse;
import com.bdbox.unionpay.acp.consume.UnionPay;
import com.bdbox.unionpay.acp.sdk.AcpService;
import com.bdbox.unionpay.acp.sdk.LogUtil;
import com.bdbox.unionpay.acp.sdk.SDKConstants;
import com.bdbox.util.LogUtils;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;

@Controller
@RequestMapping(value="unionpay")
public class UnionpayOrderHanlder {

	@Autowired
	private UnionpayOrderService unionpayOrderService;
	
	/**
	 * 银联支付
	 * @param orderId
	 * @param txnAmt
	 * @param transactionType
	 * @return
	 */
	@RequestMapping(value="/pay")
	@ResponseBody
	public ObjectResult pay(String orderId, Double txnAmt, String transactionType){
		ObjectResult result = new ObjectResult();
		try {
			txnAmt = txnAmt * 100;
			String[] arr = txnAmt.toString().split("\\.");
			String html = UnionPay.pay(orderId, arr[0], transactionType);
			result.setResult(html);
			result.setStatus(ResultStatus.OK);
			LogUtils.loginfo("银联支付交易成功");
		} catch (Exception e) {
			LogUtils.logerror("银联支付交易出现异常", e);
			e.printStackTrace();
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}
	
	/**
	 * 预授权交易
	 * @param orderId
	 * @param txnAmt
	 * @param transactionType
	 * @return
	 */
	@RequestMapping(value="/authDeal")
	@ResponseBody
	public ObjectResult authDeal(String orderId, Double txnAmt, String transactionType){
		ObjectResult result = new ObjectResult();
		try {
			txnAmt = txnAmt * 100;
			String[] arr = txnAmt.toString().split("\\.");
			String html = UnionPay.authDeal(orderId, arr[0], transactionType);
			result.setResult(html);
			result.setStatus(ResultStatus.OK);
			LogUtils.loginfo("预授权交易完成");
		} catch (Exception e) {
			LogUtils.logerror("预授权交易出现异常", e);
			e.printStackTrace();
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}
	
	/**
	 * 接收异步通知信息
	 * @param req
	 * @return
	 */
	@RequestMapping(value="backRcvResponse.do")
	@ResponseBody
	public String backRcvResponse(HttpServletRequest req){
		
		LogUtil.writeLog("BackRcvResponse接收后台通知开始");
		try {
			String encoding = req.getParameter(SDKConstants.param_encoding);
			//获取银联后台返回来的信息
			Map<String, String> valideData = BackRcvResponse.backRcvResponse(req, encoding);

			//重要！验证签名前不要修改reqParam中的键值对的内容，否则会验签不过
			if (!AcpService.validate(valideData, encoding)) {
				LogUtil.writeLog("验证签名结果[失败].");
				System.out.println("验证签名结果[失败]...");
			} else {
				LogUtil.writeLog("验证签名结果[成功].");
				//保存返回来的银联订单信息
				unionpayOrderService.save(valideData);
			}
			return "ok";
		} catch (Exception e) {
			e.printStackTrace();
		}
		LogUtil.writeLog("BackRcvResponse接收后台通知结束");
		return "fail";
	}
}

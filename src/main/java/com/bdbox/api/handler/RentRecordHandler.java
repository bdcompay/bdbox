package com.bdbox.api.handler;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.entity.RentRecord;
import com.bdbox.service.RentRecordService;
import com.bdbox.web.dto.RentRecordDto;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.CommonMethod;

@Controller
public class RentRecordHandler {

	@Autowired
	private RentRecordService rentRecordService;
	
	@RequestMapping(value="getRentAll.do")
	@ResponseBody
	public ListParam query(@RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength, @RequestParam String sEcho,
			@RequestParam String user, @RequestParam String startTime,
			@RequestParam String endTime) throws Exception{
		
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		
		Calendar start_time = null;
		Calendar end_time = null;
		if(startTime!=null && !"".equals(startTime) || endTime!=null && !"".equals(endTime)){
			start_time = CommonMethod.StringToCalendar(startTime, "yyyy-MM-dd");
			end_time = CommonMethod.StringToCalendar(endTime, "yyyy-MM-dd");
		}
		
		List<RentRecordDto> dto = rentRecordService.query(user, start_time, end_time, page, pageSize);
		int count = rentRecordService.queryAmount(user, start_time, end_time);
		
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(Integer.valueOf(iDisplayLength));
		listParam.setiDisplayStart(Integer.valueOf(iDisplayStart));
		listParam.setiTotalDisplayRecords(Integer.valueOf(count));
		listParam.setiTotalRecords(Integer.valueOf(count));
		listParam.setsEcho(sEcho);
		listParam.setAaData(dto);
		return listParam;
	}
	
	@RequestMapping("saveRent.do")
	@ResponseBody
	public ObjectResult saveRent(String user, String antecedent, 
			String rental, String sumAntecedent, String sumRental,
			String leaveMessage, String rentDays, Long orderId){
		ObjectResult result = new ObjectResult();
		RentRecord rr = new RentRecord();
		rr.setUser(user);
		rr.setOrderId(orderId);
		rr.setAntecedent(antecedent);
		rr.setRental(rental);
		rr.setSumAntecedent(sumAntecedent);
		rr.setSumRental(sumRental);
		rr.setLeaveMessage(leaveMessage);
		rr.setRentDays(rentDays);
		rr.setRecordTime(Calendar.getInstance());
		
		RentRecord rent = rentRecordService.save(rr);
		
		if(rent!=null){
			result.setResult(rent);
			result.setMessage("记录生成成功！");
			result.setStatus(ResultStatus.OK);
		}else{
			result.setMessage("记录生成失败！");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}
	
	
}

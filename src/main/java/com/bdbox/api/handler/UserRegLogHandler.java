package com.bdbox.api.handler;

import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bdbox.constant.UserStatusType;
import com.bdbox.service.UserRegLogService;
import com.bdbox.service.UserService;
import com.bdbox.util.RemindDateUtils;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.CommonMethod;

/**
 * 用户注册记录控制
 * 
 * @ClassName: UserRegLogHandler 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-2-15 下午4:59:44 
 * @version V5.0 
 */
@Controller
public class UserRegLogHandler {
	@Autowired
	private UserRegLogService userRegLogService;
	@Autowired
	private UserService userService;
	
	/**
	 * 获取用户注册记录信息
	 * @return
	 */
	@RequestMapping(value="listUserRegLog.do")
	public @ResponseBody ListParam listUserRegLog(@RequestParam int iDisplayStart,
			  @RequestParam int iDisplayLength, @RequestParam int iColumns, 
			  @RequestParam String sEcho, @RequestParam Integer iSortCol_0, 
			  @RequestParam String sSortDir_0,@RequestParam(required=false) String startTimeStr,
			  @RequestParam(required=false) String endTimeStr,@RequestParam(required=false) String username){
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
	    int pageSize = iDisplayLength;
	    if(username.equals("")){
	    	username=null;
	    }
	    Calendar startTime = null;
	    Calendar endTime = null;
	    if(startTimeStr!=null && endTimeStr!=null){
	    	try {
	    		startTime = CommonMethod.StringToCalendar(startTimeStr, "yyyy/MM/dd");
	    		endTime = CommonMethod.StringToCalendar(endTimeStr, "yyyy/MM/dd");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
	    
	    ListParam listParam = userRegLogService.listUserRegLogJq(username, null, null, null, startTime, endTime, page, pageSize);
	    listParam.setsEcho(sEcho);
		return listParam;
	}
	
	/**
	 * 统计时间段内用户注册数量
	 * 
	 * @param startTimeStr
	 * 		开始时间
	 * @param endTimeStr
	 * 		结束时间
	 * @return
	 */
	@RequestMapping(value="statisticRegNumber.do")
	public @ResponseBody ObjectResult statisticUserRegNumber(
			@RequestParam(required=false) String startTimeStr,
			@RequestParam(required=false) String endTimeStr){
		
		Calendar startTime = null;
	    Calendar endTime = null;
	    if(startTimeStr!=null && endTimeStr!=null){
	    	try {
	    		startTime = CommonMethod.StringToCalendar(startTimeStr, "yyyy-MM-dd HH:mm:ss");
	    		endTime = CommonMethod.StringToCalendar(endTimeStr, "yyyy-MM-dd HH:mm:ss");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
		
		ObjectResult objectResult = new ObjectResult();
		
		Map<String,Integer> map = new HashMap<String, Integer>();
		//输入条件统计用户注册数量
		int count = userRegLogService.count(null, null, null, null, startTime, endTime);
		//统计今天用户注册数量
		Calendar todayStartTime = Calendar.getInstance(); 
		Calendar todayEndTime = Calendar.getInstance();
		todayStartTime.setTime(RemindDateUtils.getCurrentDayStartTime());
		todayEndTime.setTime(RemindDateUtils.getCurrentDayEndTime());
		int todayCount = userRegLogService.count(null, null, null, null, todayStartTime, todayEndTime);
		//统计本月用户注册数量
		Calendar monthStartTime = Calendar.getInstance();
		Calendar monthEndTime = Calendar.getInstance();
		monthStartTime.setTime(RemindDateUtils.getCurrentMonthStartTime());
		monthEndTime.setTime(RemindDateUtils.getCurrentMonthEndTime());
		int monthCount = userRegLogService.count(null, null, null, null, monthStartTime, monthEndTime);
		//统计全部用户数量（状态为正常的全部用户）
		int allUserCount = userService.queryCount(null, null, null, UserStatusType.NORMAL, null, null);
		
		map.put("queryCount", count);
		map.put("todayCount", todayCount);
		map.put("monthCount", monthCount);
		map.put("allUserCount", allUserCount);
		
		objectResult.setStatus(ResultStatus.OK);
		objectResult.setResult(map);
		return objectResult;
	}
	
}

package com.bdbox.api.handler;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bdbox.constant.DealStatus;
import com.bdbox.constant.ExpressStatus;
import com.bdbox.constant.MailType;
import com.bdbox.entity.Express;
import com.bdbox.entity.Invoice;
import com.bdbox.entity.Notification;
import com.bdbox.entity.Order;
import com.bdbox.entity.Recipients;
import com.bdbox.entity.Returns;
import com.bdbox.entity.User;
import com.bdbox.express.shunfeng.api.SFHandler;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.AddressService;
import com.bdbox.service.ExpressService;
import com.bdbox.service.InvoiceService;
import com.bdbox.service.MessageRecordService;
import com.bdbox.service.NotificationService;
import com.bdbox.service.OrderService;
import com.bdbox.service.RecipientsService;
import com.bdbox.service.ReturnsService;
import com.bdbox.service.UserService;
import com.bdbox.util.LogUtils;
import com.bdbox.web.dto.ExpressDto;
import com.bdbox.web.dto.OrderDto;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.sf.openapi.express.sample.order.dto.OrderQueryRespDto;
import com.sf.openapi.express.sample.route.dto.RouteRespDto;

@Controller
public class ExpressHandler {

	@Autowired
	private ExpressService expressService;
	@Autowired
	private ReturnsService returnsService;	
	@Autowired
	private UserService userService;
	@Autowired
	private AddressService addressService;
	@Autowired
	private RecipientsService recipientsService;
	@Autowired 
	private NotificationService notificationService; 
	@Autowired
	private InvoiceService invoiceService; 
	@Autowired  
	private MailNotification mailNotification; 
	@Autowired
	private MessageRecordService messageRecordService;

	@Autowired
	private OrderService orderService;
	private static final String KEY = "2ce8093d9732f1ee";

	@RequestMapping({ "getExpress.do" })
	@ResponseBody
	public Express getExpress(Long user) {
		return this.expressService.queryExpress(user);
	}
	
	//根据订单id查询物流
	@RequestMapping({ "getExpressDto.do" })
	@ResponseBody
	public ObjectResult getExpressDto(Long oid) {
		ExpressDto dto = this.expressService.queryExpressDto(oid);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("express", dto);
		ObjectResult objectResult = new ObjectResult(ResultStatus.OK);
		objectResult.setResult(map);
		return objectResult;
	}

	//根据退货id查询物流
	@RequestMapping({ "getExpressDtoById.do" })
	@ResponseBody
	public ObjectResult getExpressDtoById(Long id) {
		Returns returns = returnsService.getReturnsById(id);

		ExpressDto dto = this.expressService.queryExpressDtoById(returns
				.getExpressId());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("express", dto);
		ObjectResult objectResult = new ObjectResult(ResultStatus.OK);
		objectResult.setResult(map);
		return objectResult;
	}

	//根据订单id查询物流
	@RequestMapping({ "getExpressInfo.do" })
	@ResponseBody
	public ExpressDto getExpressInfo(Long oid) {
		ExpressDto dto = this.expressService.queryExpressDto(oid);
		if(dto.getExpressMessage()==null&&dto.getExpressNumber()!=null){
			//如果数据库物流信息为空，则调用物流接口查询物流
			Express express=expressService.queryExpressEnty(oid);
 			this.expressService.doExpressApi(express);
			dto = this.expressService.queryExpressDto(oid);
		}
		return dto;
	}
	
	//根据订单id查询物流
	@RequestMapping({ "getExpressInfo1.do" })
	@ResponseBody
	public ExpressDto getExpressInfo1(Long oid,String dealstatus) {
		OrderDto orderDto =orderService.getOrderDto(oid);
		ExpressDto dto;
		if(orderDto.getDealStatus().equals(dealstatus)){ 
		  dto = this.expressService.queryExpressDto(oid);
		if(dto.getExpressMessage()==null&&dto.getExpressNumber()!=null){
			//如果数据库物流信息为空，则调用物流接口查询物流
			Express express=expressService.queryExpressEnty(oid);
 			this.expressService.doExpressApi(express);
			dto = this.expressService.queryExpressDto(oid);
		}
		}else{
			dto=null;
		}
		return dto;
	}

	@RequestMapping({ "saveExpress.do" })
	@ResponseBody
	public String saveExpress(Long oid, String numStr, String expressName) {
		if ((oid != null) && (numStr != null) && (expressName != null)) {
			numStr=numStr.trim();
			expressName = "shunfeng"; 
			Order order2 = this.orderService.getOrder(oid);
			order2.setDealStatus(DealStatus.SENT);
			this.orderService.updateOrder(order2);

			Express express = new Express();

			express.setExpressStatus(ExpressStatus.SENT);
			express.setExpressName(expressName);
			express.setExpressNumber(numStr);
			express.setOrderId(oid);
			express.setSendTime(Calendar.getInstance());
			this.expressService.saveExpress(express);
			
			//以下是下单成功后给管理员发送邮件通知
 			StringBuffer content=new StringBuffer(); 
			String type="buySEND";  //通知类型
			if(order2.getTransactionType().equals("RENT")){
				type="RENTSEND";
			}
			String type_twl="customer"; 
			String invoices="";            //是否开发票
			String tomail=null;            //收信箱
			String subject=null;           //主题
			String mailcentent=null;       //内容
			Notification notification=notificationService.getnotification(type,type_twl);
			if(notification!=null){ 
				if(notification.getIsnote().equals(true)){    
						content.append(notification.getNotecenter()).append("顺丰快递，单号："+numStr).append(invoices);
						if(content!=null){
							mailcentent=content.toString(); 
						}
					//mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
					mailNotification.sendNote(String.valueOf(order2.getPhone()),mailcentent);
					//保存短信通知记录
					messageRecordService.save(String.valueOf(order2.getPhone()), 
							mailcentent, MailType.switchStatus(type));
					
				}
			}
			
			return "ok";
		}
		return null;
	}
	
	/**
	 * 保存物流信息
	 * @param orderId
	 * @return
	 */
	@RequestMapping(value="saveExpressInfo.do")
	@ResponseBody
	public ObjectResult saveExpressInfo(Long orderId, HttpServletRequest request){
		ObjectResult result = new ObjectResult();
		try {
			Order order = orderService.getOrder(orderId);
			//获取用户id
			User user = userService.getUserByusername(order.getUser());
			//获取用户所属地址信息
//			Address address = addressService.getAddress(user.getId());
			//获取默认的收件人信息
			Recipients recipients = recipientsService.getRecipients();
			//获取发票信息
			Invoice invoice=invoiceService.getInvoice(order.getId());
			//下单，并获取物流订单信息
			OrderQueryRespDto oqrd = SFHandler.downOrder(order, null, recipients, invoice);
			//查询路由信息
			List<RouteRespDto> list = SFHandler.selectRoute(oqrd.getMailNo());
			String expressMessage = "";
			if(list!=null){
				expressMessage = SFHandler.formatStr(list);
			}
			
			//生成电子运单图片、并返回图片储存url
			String waybillUrl = SFHandler.waybill(oqrd.getOrderId(), request);
			
			//设置物流信息
			Express express =expressService.queryExpressByOrderId(orderId);
			if(express==null){
				express=new Express();
			}		
			express.setExpressStatus(ExpressStatus.SENT);
			express.setExpressName("shunfeng");
			express.setExpressNumber(oqrd.getMailNo());
			express.setExpressMessage(expressMessage);
			express.setOrderId(orderId);
			express.setSendTime(Calendar.getInstance());
			express.setWaybillUrl(waybillUrl);
			
			//保存物流信息
			this.expressService.saveOrUpdate(express);
			
			//更新订单的物流状态
			order.setDealStatus(DealStatus.SENT);
			orderService.updateOrder(order);
			
			result.setMessage("发货成功");
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("发货失败");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}

	@RequestMapping({ "sendExpress.do" })
	public String sendExpress(Long oid, String numStr, String expressName) {
		try {
			URL url = new URL(
					"http://api.kuaidi100.com/api?id=XXXX&com=tiantian&nu=11111&show=2&muti=1&order=desc");

			URLConnection con = url.openConnection();

			con.setAllowUserInteraction(false);

			InputStream urlStream = url.openStream();

			String type = URLConnection.guessContentTypeFromStream(urlStream);
			String charSet = null;
			if (type == null) {
				type = con.getContentType();
			}
			if ((type == null) || (type.trim().length() == 0)
					|| (type.trim().indexOf("text/html") < 0)) {
				return null;
			}
			if (type.indexOf("charset=") > 0) {
				charSet = type.substring(type.indexOf("charset=") + 8);
			}
			byte[] b = new byte[10000];
			int numRead = urlStream.read(b);
			String content = new String(b, 0, numRead);
			while (numRead != -1) {
				numRead = urlStream.read(b);
				if (numRead != -1) {
					String newContent = new String(b, 0, numRead, charSet);
					content = content + newContent;
				}
			}
			System.out.println("content:" + content);
			urlStream.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		Order order2 = this.orderService.getOrder(oid);
		order2.setDealStatus(DealStatus.SENT);
		this.orderService.updateOrder(order2);

		Express express = new Express();

		express.setOrderId(oid);
		express.setSendTime(Calendar.getInstance());

		return null;
	}
	
	//更新物流单号
	@RequestMapping({ "updateExpress.do" })
	@ResponseBody
	public  ObjectResult updateExpress(Long oid, String expressNumber, String expressName) {
		ObjectResult objectResult = new ObjectResult();
		try{
			ExpressDto expressDto = expressService.queryExpressDto(oid);
			Express express = expressService.queryExpress(expressDto.getId());
			express.setExpressNumber(expressNumber);
			expressService.updateExpress(express);
			objectResult.setStatus(ResultStatus.OK);
		}catch(Exception e){
			objectResult.setStatus(ResultStatus.FAILED);
			LogUtils.logerror("修改物流单异常", e);
		}
		return objectResult;
	}
	
	// 用户退货，修改快递信息
	@RequestMapping(value = "updateReturnExpress.do")
	public @ResponseBody ObjectResult updateReturnExpress(long id, String expressName,
				String expressNum) {
		ObjectResult objectResult = new ObjectResult();
		try{
			Express express = expressService.queryExpress(id);
			express.setExpressName(expressName);
			express.setExpressNumber(expressNum);
			
			expressService.updateExpress(express);
			
			//更新退货单实体
			Returns returns=returnsService.getReturnsenty(id);
			returns.setExpressNum(expressNum);
			returnsService.updateRetuens(returns);
			
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setMessage("操作成功");
		}catch(Exception e){
			LogUtils.logerror("修改物流异常", e);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("操作失败");
		}
		return objectResult;
	}
	
	@RequestMapping(value = "wayBill")
	public ModelAndView wayBill(Long id, ModelAndView model){
		Express express = expressService.queryExpressEnty(id);
		model.addObject("waybillUrl", express.getWaybillUrl());
		model.setViewName("wayBill");
		return model;
	}
	
	@RequestMapping(value = "getExpressByOrderId.do")
	@ResponseBody
	public Express getExpressByOrderId(Long orderId){
		return expressService.queryExpressByOrderId(orderId);
	}
	
}
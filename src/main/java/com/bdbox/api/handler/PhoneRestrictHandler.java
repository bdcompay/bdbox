package com.bdbox.api.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bdbox.api.dto.PhoneRestrictDto;
import com.bdbox.entity.PhoneRestrict;
import com.bdbox.service.PhoneRestrictService;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;

@Controller
public class PhoneRestrictHandler {

	@Autowired
	private PhoneRestrictService phoneRestrictService;
	
	/**
	 * 
	 * @param phoneRestrict
	 */
	@RequestMapping(value="addPhoneRestrict.do")
	public ModelAndView add(String phoneNum, String remark){
		PhoneRestrict phoneRestrict = new PhoneRestrict();
		phoneRestrict.setPhoneNum(phoneNum);
		phoneRestrict.setRemark(remark);
		phoneRestrictService.save(phoneRestrict);
		return new ModelAndView("redirect:/table_phoneRestrict.html");
	}
	
	/**
	 * 获取所有数据
	 * @return
	 */
	@RequestMapping(value="getAll.do")
	@ResponseBody
	public ListParam getAll(@RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
			@RequestParam int iColumns, @RequestParam String sEcho,
			@RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0){
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		List<PhoneRestrictDto> list = phoneRestrictService.getAll(page, pageSize);
		int count = phoneRestrictService.count();
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(list);
		return listParam;
	}
	
	/**
	 * 删除
	 * @param id
	 */
	@RequestMapping(value="deletePhone.do")
	@ResponseBody
	public ObjectResult delete(Long id){
		ObjectResult result = new ObjectResult();
		try {
			phoneRestrictService.delete(id);
			result.setMessage("删除成功");
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("删除失败");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}
}

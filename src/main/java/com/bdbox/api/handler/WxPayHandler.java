package com.bdbox.api.handler;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.wx.api.WxUnifiedOrder;
import com.bdbox.wx.bean.WxPayConf;
import com.bdbox.wx.util.CommonUtil;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;

@Controller
@RequestMapping(value = "wx")
public class WxPayHandler {
	
	@Autowired
	private WxUnifiedOrder wxUnifiedOrder;
	
	@Autowired
	private WxPayConf wxPayConf;

	@RequestMapping(value = "/wxPayForJSAPI.do")
	@ResponseBody
	public ObjectResult wxPay(String productName, String trade_no, String totalPrice, String openid){
		ObjectResult result = new ObjectResult();
		
		SortedMap<Object,Object> parameters = new TreeMap<Object,Object>();
		parameters.put("body", productName);
		parameters.put("out_trade_no", trade_no);
		//把价格转为以分为单位，并转为String类型
		String wxprice = String.valueOf((int)(Double.parseDouble(totalPrice)*100));
		
		parameters.put("total_fee", wxprice);
		parameters.put("openid", openid);
		
		//获取预支付id
		String prepay_id = wxUnifiedOrder.getPrepayid(parameters);
    	//时间戳
        String timeStamp=(System.currentTimeMillis()/1000)+"";  
        //随机字符串
        String nonceStr = CommonUtil.createNoncestr();
          
        //组装map用于生成sign  
        SortedMap<Object,Object> parm = new TreeMap<Object,Object>();
        parm.put("appId", wxPayConf.getAppid());  
        parm.put("timeStamp", timeStamp);  
        parm.put("nonceStr", nonceStr);  
        parm.put("package", "prepay_id="+prepay_id);  
        parm.put("signType", "MD5");
        
        //保存信息
        Map<String, String> map=new HashMap<String, String>();  
        map.put("appId", wxPayConf.getAppid());  
        map.put("timeStamp", timeStamp);  
        map.put("nonceStr", nonceStr);  
        map.put("prepay_id", "prepay_id="+prepay_id);  
        map.put("signType", "MD5");
        map.put("paySign", CommonUtil.getSign("UTF-8", parm, this.wxPayConf.getKey()));
        
        result.setResult(map);
        result.setStatus(ResultStatus.OK);
	        
        return result;
	}
}

package com.bdbox.api.handler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.alipay.config.Alipay;
import com.bdbox.alipay.util.AlipayNotify;
import com.bdbox.service.AlipayOrderService;
import com.bdsdk.json.ObjectResult;

@Controller
public class AlipayHandler {
	
	@Autowired
	private AlipayOrderService alipayOrderService;

	/**
	 * 提交支付申请
	 * @param out_trade_no 产品唯一订单号
	 * @param subject 产品名称
	 * @param total_fee	交易金额
	 * @param show_url 产品展示页面url
	 * @throws IOException 
	 */
	@RequestMapping(value="executeAlipay")
	@ResponseBody
	public ObjectResult executeAlipay(String out_trade_no, String subject, 
			String total_fee, String showUrl, String transactionType) throws IOException{
		String sHtmlText = Alipay.alipaySubmit(out_trade_no, subject, total_fee, showUrl, transactionType);
		ObjectResult result = new ObjectResult();
		result.setResult(sHtmlText);
		return result;
	}
	
	/**
	 * 支付异步通知
	 * @param request
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value="alipayNotify.do")
	@ResponseBody
	public String alipayNotify(HttpServletRequest request){
		Map<String,String> params = new HashMap<String,String>();
		Map requestParams = request.getParameterMap();
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
			//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
			params.put(name, valueStr);
		}
		//验证是否从支付宝发来的消息
		if(AlipayNotify.verify(params)){
			alipayOrderService.saveOrUpdate(params);
			return "success";
		}else{
			return "fail";
		}
	}
	
	/**
	 * 手机网站提交订单支付
	 * @param out_trade_no 产品唯一订单号
	 * @param subject 产品名称
	 * @param total_fee	交易金额
	 * @param show_url 产品展示页面url
	 * @throws IOException 
	 */
	@RequestMapping(value="executeAppAlipay")
	@ResponseBody
	public ObjectResult executeAppAlipay(String out_trade_no, String subject, 
			String total_fee, String showUrl, String transactionType) throws IOException{
		String sHtmlText = Alipay.alipayAppSubmit(out_trade_no, subject, total_fee, showUrl, transactionType);
		ObjectResult result = new ObjectResult();
		result.setResult(sHtmlText);
		return result;
	}
}

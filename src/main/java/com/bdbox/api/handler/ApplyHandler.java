package com.bdbox.api.handler;

import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.api.dto.ApplyDto;
import com.bdbox.entity.Apply;
import com.bdbox.service.ApplyService;
import com.bdbox.service.UserService;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Controller
public class ApplyHandler {

	  @Autowired
	  private ApplyService applyService;

	  @Autowired
	  private UserService userService;
	
	/**
	 * 根据用户id查询订单
	 * @param uid
	 * @return
	 */
	  @RequestMapping(value={"getApplyUid.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
	  @ResponseBody
	  public ObjectResult getApplyUid(Long uid)
	  {
	    ObjectResult objectResult = new ObjectResult();
	    ApplyDto applyDto = this.applyService.getApplyUid(uid);
	    if (applyDto != null) {
	      objectResult.setResult(applyDto);
	      objectResult.setMessage("查询订单成功");
	      objectResult.setStatus(ResultStatus.OK);
	    } else {
	      objectResult.setMessage("查询订单失败,该用户无订单");
	      objectResult.setStatus(ResultStatus.FAILED);
	    }
	    return objectResult;
	  }


	
	/**
	 * 添加
	 * @param orderDto
	 * @return
	 */
	  @RequestMapping(value={"saveApply.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
	  @ResponseBody
	  public ObjectResult saveApply(ApplyDto applyDto, Long uid)
	  {
	    //User user = this.userService.getUser(uid);

	    Apply apply = new Apply();
	    BeansUtil.copyBean(apply, applyDto, true);
	    apply.setCreateTime(Calendar.getInstance());
	    //apply.setUser(user);

	    if ((applyDto.getJoin() != null) && (applyDto.getJoin() != "") && 
	      (applyDto.getJoin().equals("参与"))) {
	      apply.setJoin(true);
	    }

	    this.applyService.saveApply(apply);

	    ObjectResult result = new ObjectResult();
	    result.setStatus(ResultStatus.OK);
	    result.setMessage("提交成功");
	    return result;
	  }
	
	/**
	 * 判断用户是否重复报名
	 * @param phone
	 * @return
	 */
	  @RequestMapping(value={"judgePhone.do"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
	  @ResponseBody
	  public ObjectResult judgePhone(String phone)
	  {
	    ApplyDto applyDto = this.applyService.getApply(phone);
	    ObjectResult objectResult = new ObjectResult();
	    if (applyDto == null) {
	      objectResult.setMessage("无重复报名");
	      objectResult.setStatus(ResultStatus.OK);
	    } else {
	      objectResult.setMessage("重复报名");
	      objectResult.setStatus(ResultStatus.FAILED);
	    }
	    return objectResult;
	  }
	
	/**
	 * 	查询
	 * @param iDisplayStart	从第几个开始取值
	 * @param iDisplayLength	每页显示数量
	 * @param iColumns	该表格列的数量
	 * @param sEcho	响应次数
	 * @param iSortCol_0
	 * @param sSortDir_0	排序方式
	 * 
	 * @param code	户外代号
	 * @param phone	手机号码
	 * @param createTimeStr	申请时间
	 * @param sex	性别
	 * @param joinStr	参与状态
	 * @return	JSon
	 * @throws Exception
	 */
	  @RequestMapping({"listApply.do"})
	  @ResponseBody
	  public ListParam listApply(@RequestParam int iDisplayStart, 
			  @RequestParam int iDisplayLength, 
			  @RequestParam int iColumns, 
			  @RequestParam String sEcho, 
			  @RequestParam Integer iSortCol_0, 
			  @RequestParam String sSortDir_0, 
			  @RequestParam(required=false) String code, 
			  @RequestParam(required=false) String phone, 
			  @RequestParam(required=false) String createTimeStr, 
			  @RequestParam(required=false) String sex, 
			  @RequestParam(required=false) String joinStr)
	    throws Exception
	  {
	    int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
	    int pageSize = iDisplayLength;

	    if ((code != null) && 
	      (code.equals(""))) {
	      code = null;
	    }

	    if ((phone != null) && 
	      (phone.equals(""))) {
	      phone = null;
	    }

	    if ((sex != null) && (
	      (sex.equals("")) || (sex.equals("性别")))) {
	      sex = null;
	    }

	    Calendar createTime = null;
	    if ((!createTimeStr.equals("申请时间")) && (!createTimeStr.equals("")) && (createTimeStr != null) && (createTimeStr != "")) {
	      createTime = CommonMethod.StringToCalendar(createTimeStr, "MM/dd/yyyy");
	    }

	    List list = null;
	    int count = 0;
	    if (joinStr.equals("参与")) {
	      list = this.applyService.listApply(code, phone, createTime, sex, Boolean.valueOf(true), Integer.valueOf(page), Integer.valueOf(pageSize), " t.createTime DESC");
	      count = this.applyService.applyCount(code, phone, createTime, sex, Boolean.valueOf(true));
	    } else if (joinStr.equals("不参与")) {
	      list = this.applyService.listApply(code, phone, createTime, sex, Boolean.valueOf(false), Integer.valueOf(page), Integer.valueOf(pageSize), " t.createTime DESC");
	      count = this.applyService.applyCount(code, phone, createTime, sex, Boolean.valueOf(false));
	    } else {
	      list = this.applyService.listApply(code, phone, createTime, sex, null, Integer.valueOf(page), Integer.valueOf(pageSize), " t.createTime DESC");
	      count = this.applyService.applyCount(code, phone, createTime, sex, null);
	    }

	    ListParam listParam = new ListParam();
	    listParam.setiDisplayLength(Integer.valueOf(iDisplayLength));
	    listParam.setiDisplayStart(Integer.valueOf(iDisplayStart));
	    listParam.setiTotalDisplayRecords(Integer.valueOf(count));
	    listParam.setiTotalRecords(Integer.valueOf(count));
	    listParam.setsEcho(sEcho);
	    listParam.setAaData(list);

	    return listParam;
	  }
	  
	  /**
	   * 根据手机号查询产品预约信息
	   * 
	   * @param phone
	   * 			手机号
	   * @return
	   */
	  @RequestMapping({"getUserApply.do"})
	  @ResponseBody
	  public ObjectResult getUserApply(String phone){
		  ObjectResult ojbectResult = new ObjectResult();
		  ApplyDto applyDto = applyService.getApply(phone);
		  if(applyDto==null){
			  ojbectResult.setStatus(ResultStatus.FAILED);
		  }else{
			  ojbectResult.setStatus(ResultStatus.OK);
			  ojbectResult.setResult(applyDto);
		  }
		  return ojbectResult;
	  }
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	  @RequestMapping({"deleteApply.do"})
	  public String deleteApply(Long id)
	  {
	    this.applyService.deleteApply(id);
	    return "redirect:/table_apply";
	  }


	
	/**
	 * 导出文件
	 * @return
	 */
	  @RequestMapping({"exportApply.do"})
	  @ResponseBody
	  public String exportApply(){
		
		//文件名:时间戳
		String documentName = Long.toString(System.currentTimeMillis())+".xls";
		
		// 第一步，创建一个webbook，对应一个Excel文件
		HSSFWorkbook wb = new HSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		HSSFSheet sheet = wb.createSheet(documentName);
		// 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
		HSSFRow row = sheet.createRow((int) 0);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

		HSSFCell cell = row.createCell((short) 0);
		cell.setCellValue("姓名");
		cell.setCellStyle(style);

		cell = row.createCell((short) 1);
		cell.setCellValue("手机号码");
		cell.setCellStyle(style);

		cell = row.createCell((short) 2);
		cell.setCellValue("联系邮箱");
		cell.setCellStyle(style);

		cell = row.createCell((short) 3);
		cell.setCellValue("参与状态");
		cell.setCellStyle(style);

		cell = row.createCell((short) 4);
		cell.setCellValue("性别");
		cell.setCellStyle(style);
		
		cell = row.createCell((short) 5);
		cell.setCellValue("省份");
		cell.setCellStyle(style);
		
		cell = row.createCell((short) 6);
		cell.setCellValue("市区");
		cell.setCellStyle(style);
		
		cell = row.createCell((short) 7);
		cell.setCellValue("户外代码");
		cell.setCellStyle(style);
		
		cell = row.createCell((short) 8);
		cell.setCellValue("申请时间");
		cell.setCellStyle(style);
		
		cell = row.createCell((short) 9);
		cell.setCellValue("个人简介");
		cell.setCellStyle(style);
		
		cell = row.createCell((short) 10);
		cell.setCellValue("其它说明");
		cell.setCellStyle(style);


		// 第五步，写入实体数据 实际应用中这些数据从数据库得到，
		List<ApplyDto> list = applyService.listApply(null, null, null, null, null, 1, 1, null);
		for (int i = 0; i < list.size(); i++) {
			row = sheet.createRow((int) i + 1);
			ApplyDto stu = (ApplyDto) list.get(i);
			
			// 第四步，创建单元格，并设置值
			row.createCell((short) 0).setCellValue(stu.getName());
			row.createCell((short) 1).setCellValue(stu.getPhone());
			row.createCell((short) 2).setCellValue(stu.getEmail());
			row.createCell((short) 3).setCellValue(stu.getJoin());
			row.createCell((short) 4).setCellValue(stu.getSex());
			row.createCell((short) 5).setCellValue(stu.getProvince());
			row.createCell((short) 6).setCellValue(stu.getCity());
			row.createCell((short) 7).setCellValue(stu.getCode());
			row.createCell((short) 8).setCellValue(stu.getCreateTime());
			row.createCell((short) 9).setCellValue(stu.getIntro());
			row.createCell((short) 10).setCellValue(stu.getExplain());
		}
		// 第六步，将文件存到指定位置
		try {
			String path = "C:/Users/WF/git/bdbox/src/main/webapp/export/"+documentName;
			//String path =  "D:/apache-tomcat-8.0.15-bd/apache-tomcat-8.0.15/webapps/aboutBd/export/产品数据.xls";
			FileOutputStream fout = new FileOutputStream(path);
			wb.write(fout);
			fout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		 return "export/"+documentName;
	}
	
}

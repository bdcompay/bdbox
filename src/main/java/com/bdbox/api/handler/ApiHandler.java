package com.bdbox.api.handler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.api.dto.AppConfigDto;
import com.bdbox.api.dto.BoxDto;
import com.bdbox.api.dto.BoxblueCodeDto;
import com.bdbox.api.dto.FamilyDto;
import com.bdbox.api.dto.ParamBoxLocationDataDto;
import com.bdbox.api.dto.ParamFamilyDataDto;
import com.bdbox.api.dto.ParamPartnerDataDto;
import com.bdbox.api.dto.PartnerDto;
import com.bdbox.api.dto.PostApiParam;
import com.bdbox.api.dto.QueryParamAppApi;
import com.bdbox.biz.BizLocation;
import com.bdbox.biz.BizMsgTrans;
import com.bdbox.biz.MessageControl;
import com.bdbox.constant.AppType;
import com.bdbox.constant.BoxStatusType;
import com.bdbox.constant.MsgIoType;
import com.bdbox.constant.MsgType;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxFamily;
import com.bdbox.entity.BoxMessage;
import com.bdbox.entity.BoxPartner;
import com.bdbox.entity.EnterpriseUser;
import com.bdbox.entity.PhoneRestrict;
import com.bdbox.entity.UserSendBoxMessage;
import com.bdbox.service.AppConfigService;
import com.bdbox.service.BoxFamilyService;
import com.bdbox.service.BoxPartnerService;
import com.bdbox.service.BoxService;
import com.bdbox.service.DialrecordService;
import com.bdbox.service.EnterpriseUserService;
import com.bdbox.service.PhoneRestrictService;
import com.bdbox.service.UserService;
import com.bdsdk.constant.DataStatusType;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BdCommonMethod;
import com.bdsdk.util.CommonMethod;

@Controller
public class ApiHandler {

	@Autowired
	private AppConfigService appConfigService;

	@Autowired
	private BoxFamilyService boxFamilyService;
	@Autowired
	private BoxPartnerService boxPartnerService;
	@Autowired
	private BoxService boxService;
	@Autowired
	private MessageControl messageControl;
	@Autowired
	private BizLocation bizLocation;
	@Autowired
	private EnterpriseUserService enterpriseUserService;
	@Autowired
	private PhoneRestrictService phoneRestrictService;

	/**
	 * 检查APP是否有更新
	 * 
	 * @param q
	 * @return
	 */
	@RequestMapping(value = "/checkAppUpdate", method = RequestMethod.POST)
	public @ResponseBody ObjectResult checkAppUpdate(
			@RequestBody QueryParamAppApi q) {

		ObjectResult objectResult = new ObjectResult();
		AppType appType = AppType.valueOf(q.getAppType());
		AppConfigDto appConfigDto = appConfigService.getAppConfig(appType);
		if (appConfigDto != null) {
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setResult(appConfigDto);
		} else {
			objectResult.setMessage("未找到相关版本信息");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}

	/**
	 * 修改家人参数
	 * 
	 * @param familyDto
	 *            家人
	 * @param boxSerialNumber
	 *            盒子序列号
	 * @param cardNumber
	 *            北斗卡号
	 * @return
	 */
	@RequestMapping(value = "/updateFamilySingle.do")
	public @ResponseBody ObjectResult updateFamily(FamilyDto familyDto,
			String boxSerialNumber, String cardNumber) {
		ObjectResult objectResult = new ObjectResult();
		String phone=familyDto.getPhone();
		PhoneRestrict phoneRestrict=phoneRestrictService.getPhone(phone);
		if(phoneRestrict!=null){
			objectResult.setMessage("更新失败，"+phone+"为限制号码，不可设置为家人号码");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
			return objectResult;
		}
		Boolean result = boxFamilyService.updateFamilySingle(familyDto,
				cardNumber, boxSerialNumber);
		if (result) {
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setResult(true);
		} else {
			objectResult.setMessage("更新失败，未找到相关盒子信息");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}

	/**
	 * 更新家人的参数
	 * 
	 * @param paramFamilyDataDtos
	 * @return
	 */
	@RequestMapping(value = "/updateFamily.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult updateFamily(
			@RequestBody ParamFamilyDataDto paramFamilyDataDto) {
		ObjectResult objectResult = new ObjectResult();
		//判断家人号码限制
		List<FamilyDto> fdtos=paramFamilyDataDto.getFamilyDtos();
		if(fdtos!=null&fdtos.size()>0){
			for(FamilyDto familyDto:fdtos){
				String phone=familyDto.getPhone();
				PhoneRestrict phoneRestrict=phoneRestrictService.getPhone(phone);
				if(phoneRestrict!=null){
					objectResult.setMessage("更新失败，"+phone+"为限制号码，不可设置为家人号码");
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					return objectResult;
				}
			}
		}
		Boolean result = boxFamilyService.updateBoxFamily(
				paramFamilyDataDto.getFamilyDtos(),
				paramFamilyDataDto.getCardNumber(),
				paramFamilyDataDto.getBoxSerialNumber());

		if (result) {
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setResult(true);
		} else {
			objectResult.setMessage("更新失败，未找到相关盒子信息");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}

	/**
	 * 获取家人参数
	 * 
	 * @param paramFamilyDataDto
	 * @return
	 */
	@RequestMapping(value = "/getFamily.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult getFamily(
			@RequestParam String boxSerialNumber,
			@RequestParam String cardNumber) {
		ObjectResult objectResult = new ObjectResult();
		
		Box box = boxService.getBox(null, boxSerialNumber,
				null, null, null);
		if(box==null){
			objectResult.setMessage("获取失败，未找到相关盒子信息");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
		}else{
			if(box.getBoxStatusType()!=BoxStatusType.NORMAL){
				objectResult.setMessage("盒子不是激活状态,请到官网激活");
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
			}else{
				List<FamilyDto> familyDtos = boxFamilyService.getFamilyDtos(cardNumber,
						boxSerialNumber);
				if (familyDtos != null) {
					objectResult.setStatus(ResultStatus.OK);
					objectResult.setResult(familyDtos);
				} else {
					objectResult.setMessage("获取失败，未找到相关盒子信息");
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
				}
			}
		}
		return objectResult;
	}

	/**
	 * 添加家人
	 * 
	 * @param familyDto
	 * @param boxSerialNumber
	 *            ID
	 * @return
	 */
	@RequestMapping(value = "/saveFamily.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult saveFamily(String phone, String name,
			@RequestParam String boxSerialNumber, String familyMail) {

		BoxFamily boxFamily = new BoxFamily();
		Box box = boxService.getBox(BoxStatusType.NORMAL, boxSerialNumber,
				null, null, null);
		boxFamily.setBox(box);
		boxFamily.setFamilyMob(phone);
		boxFamily.setFamilyName(name);
		boxFamily.setFamilyMail(familyMail);
		boxFamilyService.saveBoxFamily(boxFamily);

		// 设置响应
		ObjectResult objectResult = new ObjectResult();
		objectResult.setMessage("添加家人成功");
		objectResult.setStatus(ResultStatus.OK);
		return objectResult;
	}

	/**
	 * 删除
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "deleteFamily.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult deleteFamily(Long id) {
		boxFamilyService.deleteFamily(id);
		ObjectResult objectResult = new ObjectResult();
		objectResult.setMessage("删除家人成功");
		objectResult.setStatus(ResultStatus.OK);
		return objectResult;
	}

	/**
	 * 修改伙伴参数
	 * 
	 * @param partnerDto
	 *            家人
	 * @param boxSerialNumber
	 *            盒子序列号
	 * @param cardNumber
	 *            北斗卡号
	 * @return
	 */
	@RequestMapping(value = "/updatePartnerSingle.do")
	public @ResponseBody ObjectResult updatePartner(PartnerDto partnerDto) {
		ObjectResult objectResult = new ObjectResult();
		Boolean result = boxPartnerService.updatePartnerSingle(partnerDto);
		
		if (result) {
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setResult(true);
		} else {
			objectResult.setMessage("更新失败，未找到相关盒子信息");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}

	/**
	 * 更新小伙伴的参数
	 * 
	 * @param paramFaPartnertaDto
	 * @return
	 */
	@RequestMapping(value = "/updatePartner.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult updatePartner(
			@RequestBody ParamPartnerDataDto paramPartnerDataDto) {

		ObjectResult objectResult = new ObjectResult();
		
		Box box = boxService.getBox(null, paramPartnerDataDto.getBoxSerialNumber(),
				null, null, null);
		if(box==null){
			objectResult.setMessage("获取失败，未找到相关盒子信息");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
		}else{
			if(box.getBoxStatusType()!=BoxStatusType.NORMAL){
				objectResult.setMessage("盒子不是激活状态,请到官网激活");
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
			}else{
				List<PartnerDto> partnerDtos = paramPartnerDataDto.getPartnerDtos();
				for (PartnerDto partnerDto : partnerDtos) {
					if (boxService.getBox(BoxStatusType.NORMAL,
							partnerDto.getBoxSerialNumber(), null, null, null) == null) {
						objectResult.setMessage("盒子ID "
								+ partnerDto.getBoxSerialNumber() + " 不存在，保存失败");
						objectResult.setResult(null);
						objectResult.setStatus(ResultStatus.FAILED);
						return objectResult;
					}
				}
				Boolean result = boxPartnerService.updateBoxPartner(partnerDtos,
						paramPartnerDataDto.getCardNumber(),
						paramPartnerDataDto.getBoxSerialNumber());

				if (result) {
					objectResult.setStatus(ResultStatus.OK);
					objectResult.setResult(true);
				} else {
					objectResult.setMessage("系统异常");
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
				}
				
				}
			}
		return objectResult;
	}

	/**
	 * 获取小伙伴的参数
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/getPartner.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult getPartner(
			@RequestParam String boxSerialNumber,
			@RequestParam String cardNumber) {

		ObjectResult objectResult = new ObjectResult();
		Box box = boxService.getBox(null, boxSerialNumber,
				null, null, null);
		if(box==null){
			objectResult.setMessage("获取失败，未找到相关盒子信息");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
		}else{
			if(box.getBoxStatusType()!=BoxStatusType.NORMAL){
				objectResult.setMessage("盒子不是激活状态,请到官网激活");
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
			}else{
					List<PartnerDto> partnerDtos = boxPartnerService.getPartnerDtos(
							cardNumber, boxSerialNumber);
					if (partnerDtos != null) {
						objectResult.setStatus(ResultStatus.OK);
						objectResult.setResult(partnerDtos);
					} else {
						objectResult.setMessage("获取失败，未找到相关盒子信息");
						objectResult.setResult(null);
						objectResult.setStatus(ResultStatus.FAILED);
					}
				}
			}
		
		return objectResult;
	}

	/**
	 * 添加队友
	 * 
	 * @param partnerDto
	 * @param userBoxSerialNumber
	 *            ID
	 * @return
	 */
	@RequestMapping(value = "/savePartner.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult savePartner(String boxSerialNumber,
			String name, @RequestParam String userBoxSerialNumber) {

		BoxPartner boxPartner = new BoxPartner();
		Box box = boxService.getBox(BoxStatusType.NORMAL, userBoxSerialNumber,
				null, null, null);
		boxPartner.setBox(box);
		boxPartner.setPartnerBoxSerialNumber(boxSerialNumber);
		boxPartner.setPartnerName(name);
		boxPartnerService.saveBoxPartner(boxPartner);

		// 设置响应
		ObjectResult objectResult = new ObjectResult();
		objectResult.setMessage("添加家人成功");
		objectResult.setStatus(ResultStatus.OK);
		return objectResult;
	}

	/**
	 * 删除队友
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "deletePartner.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult deletePartner(Long id) {
		boxPartnerService.deletePartner(id);
		ObjectResult objectResult = new ObjectResult();
		objectResult.setMessage("删除队友成功！");
		objectResult.setStatus(ResultStatus.OK);
		return objectResult;
	}

	/**
	 * 验证用户授权码接口
	 * 
	 * @param postApiParam
	 * @return
	 */
	@RequestMapping(value = "checkUserPowerKey.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult checkUserPowerKey(
			@RequestBody PostApiParam postApiParam) {
		ObjectResult objectResult = new ObjectResult();
		if (postApiParam.getUserPowerKey() != null
				&& !postApiParam.getUserPowerKey().equals("")) {
			EnterpriseUser user = enterpriseUserService.getUserByPowerKey(postApiParam
					.getUserPowerKey());
			
			/*User user = userService.getUserByPowerKey(postApiParam
					.getUserPowerKey());*/
			if (user != null) {
				objectResult.setResult(true);
				objectResult.setStatus(ResultStatus.OK);

			} else {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("授权码无效，发送失败");
			}

		} else {
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数不正确，发送失败");
		}
		return objectResult;

	}

	/**
	 * 发送盒子信息 
	 * 
	 * @param postApiParam
	 * @return
	 */
	@RequestMapping(value = "sendBoxMessage.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult sendBoxMessage(
			@RequestBody PostApiParam postApiParam) {
		ObjectResult objectResult = new ObjectResult();
		if (postApiParam.getToBoxSerialNumber() != null
				&& !postApiParam.getToBoxSerialNumber().equals("")
				&& postApiParam.getContent() != null
				&& !postApiParam.getContent().equals("")
				&& postApiParam.getUserPowerKey() != null
				&& !postApiParam.getUserPowerKey().equals("")) {
			if (!CommonMethod.isValidateNumber(postApiParam
					.getToBoxSerialNumber())) {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("盒子ID错误，发送失败");
				return objectResult;
			}
			
			String contentHex=BdCommonMethod
					.castHanziStringToHexString(postApiParam.getContent());
			
			if (contentHex.length() > 96) {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("内容不可超过24个字符");
				return objectResult;
			}
			try {
				/*User user = userService.getUserByPowerKey(postApiParam
						.getUserPowerKey());*/
				EnterpriseUser entUser = enterpriseUserService.getUserByPowerKey(postApiParam
						.getUserPowerKey());
				if (entUser != null) {
					// 判断频度是否到
					if (entUser.getLastSendBoxMessageTime() != null) {
						Calendar c1 = entUser.getLastSendBoxMessageTime();
						Calendar c2 = Calendar.getInstance();
						long a = (long) (c2.getTimeInMillis() - c1
								.getTimeInMillis());
						long b = a / 1000;
						long c = 5;
						if (entUser.getFreq() != 0) {
							c = entUser.getFreq();
						}
						long d = c - b;
						if (d > 0) {
							objectResult.setResult(null);
							objectResult.setStatus(ResultStatus.FAILED);
							objectResult.setMessage("频度未到，还剩" + d + "秒");
							return objectResult;
						}
					}
					BoxMessage boxMessage = new BoxMessage();
					Box toBox = boxService.getBox(BoxStatusType.NORMAL,
							postApiParam.getToBoxSerialNumber(), null, null,
							null);
					if (toBox == null) {
						objectResult.setResult(null);
						objectResult.setStatus(ResultStatus.FAILED);
						objectResult.setMessage("目标盒子不存在或者未激活");
						return objectResult;
					}
					boxMessage.setMsgId(BizMsgTrans.messageId);
					BizMsgTrans.messageId++;
					if(BizMsgTrans.messageId>255){
						BizMsgTrans.messageId=1;
					}
					boxMessage.setToBox(toBox);
					boxMessage.setCreatedTime(Calendar.getInstance());
					boxMessage.setMsgIoType(MsgIoType.IO_SDKTOBOX);
					boxMessage.setMsgType(MsgType.MSG_SAFE);
					boxMessage.setContent(postApiParam.getContent());
					boxMessage.setCreatedTime(Calendar.getInstance());
					SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
			        String strTime = sdf.format(boxMessage.getCreatedTime().getTime());
			        String msgId=boxMessage.getMsgId().toString();
			        if(msgId.length()>2){
			        	msgId=msgId.substring(1);
			        }
			        if(msgId.length()==1){
			        	msgId="0"+msgId;
			        }
					String msgHzId =strTime+msgId;//消息回执ID
					postApiParam.setMsgId(Long.parseLong(msgHzId));
					Box fromBox = boxService.getBox(BoxStatusType.NORMAL,
							"100001", null, null,
							null);
					if(fromBox==null){
						fromBox=new Box();
						fromBox.setBoxStatusType(BoxStatusType.NORMAL);
						fromBox.setSnNumber("QZ000001");
						fromBox.setBoxSerialNumber("100001");
						fromBox.setName("平台");
						fromBox.setEntUser(entUser);
						fromBox.setBoxStatusType(BoxStatusType.NORMAL);
						boxService.saveBox(fromBox);
					}
					
					boxMessage.setFromBox(fromBox);
					boxMessage.setDataStatusType(DataStatusType.SENT);

					messageControl.parseRevBoxMessage(boxMessage);
					entUser.setLastSendBoxMessageTime(Calendar.getInstance());
					objectResult.setResult(postApiParam);
					objectResult.setStatus(ResultStatus.OK);
					enterpriseUserService.update(entUser);
					return objectResult;
				} else {
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("授权码无效，发送失败");
					return objectResult;
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.SYS_ERROR);
				objectResult.setMessage("系统错误，发送失败");
				return objectResult;
			}
		} else {
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数不正确，发送失败");
			return objectResult;
		}

	}

	/**
	 * 发送盒子信息 带有盒子消息送达回执和消息阅读回执
	 * 
	 * @param postApiParam
	 * @return
	 */
	@RequestMapping(value = "sendBoxMessageRR.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult sendBoxMessageRR(
			@RequestBody PostApiParam postApiParam) {
		ObjectResult objectResult = new ObjectResult();
		
		if (postApiParam.getToBoxSerialNumber() != null
				&& !postApiParam.getToBoxSerialNumber().equals("")
				&& postApiParam.getContent() != null
				&& !postApiParam.getContent().equals("")
				&& postApiParam.getUserPowerKey() != null
				&& !postApiParam.getUserPowerKey().equals("")) {
			if (!CommonMethod.isValidateNumber(postApiParam
					.getToBoxSerialNumber())) {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("盒子ID错误，发送失败");
				return objectResult;
			}
			String contentHex=BdCommonMethod
					.castHanziStringToHexString(postApiParam.getContent());
			if (contentHex.length() > 124) {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("内容不可超过31个字符");
				return objectResult;
			}
			try {
				EnterpriseUser entUser = enterpriseUserService.getUserByPowerKey(postApiParam
						.getUserPowerKey());
				
				if (entUser != null) {
					// 判断频度是否到
					if (entUser.getLastSendBoxMessageTime() != null) {
						Calendar c1 = entUser.getLastSendBoxMessageTime();
						Calendar c2 = Calendar.getInstance();
						long a = (long) (c2.getTimeInMillis() - c1
								.getTimeInMillis());
						long b = a / 1000;
						long c = 5;
						if (entUser.getFreq() != 0) {
							c = entUser.getFreq();
						}
						long d = c - b;
						if (d > 0) {
							objectResult.setResult(null);
							objectResult.setStatus(ResultStatus.FAILED);
							objectResult.setMessage("频度未到，还剩" + d + "秒");
							return objectResult;
						}
					}
					UserSendBoxMessage userSendBoxMessage = new UserSendBoxMessage();
					Box toBox = boxService.getBox(BoxStatusType.NORMAL,
							postApiParam.getToBoxSerialNumber(), null, null,
							null);
					if (toBox == null) {
						objectResult.setResult(null);
						objectResult.setStatus(ResultStatus.FAILED);
						objectResult.setMessage("目标盒子不存在或者未激活");
						return objectResult;
					}
					BizMsgTrans.messageId++;
					if(BizMsgTrans.messageId>255){
						BizMsgTrans.messageId=1;
					}
					userSendBoxMessage.setBoxSerialNumber(postApiParam.getToBoxSerialNumber());
					userSendBoxMessage.setContent(postApiParam.getContent());
					userSendBoxMessage.setCreatedTime(Calendar.getInstance());
					userSendBoxMessage.setEntUser(entUser);
					
					userSendBoxMessage.setIsRead(false);
					if(postApiParam.getIsRead()!=null&&postApiParam.getIsRead()){
					userSendBoxMessage.setIsRead(postApiParam.getIsRead());
					}
					userSendBoxMessage.setIsReceived(false);
					if(postApiParam.getIsReceived()!=null&&postApiParam.getIsReceived()){
						userSendBoxMessage.setIsReceived(postApiParam.getIsReceived());
					}
					SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
			        String strTime = sdf.format(userSendBoxMessage.getCreatedTime().getTime());
			        String msgId=Integer.toString(BizMsgTrans.messageId);
			        if(msgId.length()>2){
			        	msgId=msgId.substring(1);
			        }
			        if(msgId.length()==1){
			        	msgId="0"+msgId;
			        }
					String msgHzId =strTime+msgId;//消息回执ID
					userSendBoxMessage.setMsgId(Long.parseLong(msgHzId.toString()));
					postApiParam.setMsgId(Long.parseLong(msgHzId));

					messageControl.parseUserSendBoxMessage(userSendBoxMessage);
					entUser.setLastSendBoxMessageTime(Calendar.getInstance());
					objectResult.setResult(postApiParam);
					objectResult.setStatus(ResultStatus.OK);
					enterpriseUserService.update(entUser);
					return objectResult;
				} else {
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("授权码无效，发送失败");
					return objectResult;
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.SYS_ERROR);
				objectResult.setMessage("系统错误，发送失败");
				return objectResult;
			}
		} else {
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数不正确，发送失败");
			return objectResult;
		}

	}
	
	/**
	 * 发送盒子信息 带有盒子消息送达回执和消息阅读回执  网页调用测试所用
	 * 
	 * @param postApiParam
	 * @return
	 */
	@RequestMapping(value = "sendBMTest.do", method = RequestMethod.GET)
	public @ResponseBody ObjectResult sendBoxMessageTest(
			@RequestParam String boxId,@RequestParam String content,@RequestParam String powerkey) {
		ObjectResult objectResult = new ObjectResult();
		PostApiParam postApiParam=new PostApiParam();
		postApiParam.setIsReceived(true);
		postApiParam.setContent(content);
		postApiParam.setToBoxSerialNumber(boxId);
		postApiParam.setIsRead(true);
		postApiParam.setUserPowerKey(powerkey);
		if (postApiParam.getToBoxSerialNumber() != null
				&& !postApiParam.getToBoxSerialNumber().equals("")
				&& postApiParam.getContent() != null
				&& !postApiParam.getContent().equals("")
				&& postApiParam.getUserPowerKey() != null
				&& !postApiParam.getUserPowerKey().equals("")) {
			if (!CommonMethod.isValidateNumber(postApiParam
					.getToBoxSerialNumber())) {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("盒子ID错误，发送失败");
				return objectResult;
			}
			String contentHex=BdCommonMethod
					.castHanziStringToHexString(postApiParam.getContent());
			if (contentHex.length() > 124) {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("内容不可超过31个字符");
				return objectResult;
			}
			try {
				EnterpriseUser entUser = enterpriseUserService.getUserByPowerKey(postApiParam
						.getUserPowerKey());
				if (entUser != null) {
					// 判断频度是否到
					if (entUser.getLastSendBoxMessageTime() != null) {
						Calendar c1 = entUser.getLastSendBoxMessageTime();
						Calendar c2 = Calendar.getInstance();
						int a = (int) (c2.getTimeInMillis() - c1
								.getTimeInMillis());
						int b = a / 1000;
						int c = 5;
						if (entUser.getFreq() != 0) {
							c = entUser.getFreq();
						}
						int d = c - b;
						if (d > 0) {
							objectResult.setResult(null);
							objectResult.setStatus(ResultStatus.FAILED);
							objectResult.setMessage("频度未到，还剩" + d + "秒");
							return objectResult;
						}
					}
					UserSendBoxMessage userSendBoxMessage = new UserSendBoxMessage();
					Box toBox = boxService.getBox(BoxStatusType.NORMAL,
							postApiParam.getToBoxSerialNumber(), null, null,
							null);
					if (toBox == null) {
						objectResult.setResult(null);
						objectResult.setStatus(ResultStatus.FAILED);
						objectResult.setMessage("目标盒子不存在或者未激活");
						return objectResult;
					}
					BizMsgTrans.messageId++;
					if(BizMsgTrans.messageId>255){
						BizMsgTrans.messageId=1;
					}
					userSendBoxMessage.setBoxSerialNumber(postApiParam.getToBoxSerialNumber());
					userSendBoxMessage.setContent(postApiParam.getContent());
					userSendBoxMessage.setCreatedTime(Calendar.getInstance());
					//userSendBoxMessage.setFromUser(user);
					userSendBoxMessage.setMsgIoType(MsgIoType.IO_SDKTOBOX);
					userSendBoxMessage.setEntUser(entUser);
					userSendBoxMessage.setIsRead(false);
					if(postApiParam.getIsRead()!=null&&postApiParam.getIsRead()){
					userSendBoxMessage.setIsRead(postApiParam.getIsRead());
					}
					userSendBoxMessage.setIsReceived(false);
					if(postApiParam.getIsReceived()!=null&&postApiParam.getIsReceived()){
						userSendBoxMessage.setIsReceived(postApiParam.getIsReceived());
					}
					SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
			        String strTime = sdf.format(userSendBoxMessage.getCreatedTime().getTime());
			        String msgId=Integer.toString(BizMsgTrans.messageId);
			        if(msgId.length()>2){
			        	msgId=msgId.substring(1);
			        }
			        if(msgId.length()==1){
			        	msgId="0"+msgId;
			        }
					String msgHzId =strTime+msgId;//消息回执ID
					userSendBoxMessage.setMsgId(Long.parseLong(msgHzId.toString()));
					postApiParam.setMsgId(Long.parseLong(msgHzId));

					messageControl.parseUserSendBoxMessage(userSendBoxMessage);
					entUser.setLastSendBoxMessageTime(Calendar.getInstance());
					objectResult.setResult(postApiParam);
					objectResult.setStatus(ResultStatus.OK);
					enterpriseUserService.update(entUser);
					return objectResult;
				} else {
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("授权码无效，发送失败");
					return objectResult;
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.SYS_ERROR);
				objectResult.setMessage("系统错误，发送失败");
				return objectResult;
			}
		} else {
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数不正确，发送失败");
			return objectResult;
		}

	}
	
	
	/**
	 * 获取蓝牙密码
	 * @param boxSerialNumber
	 * @param cardNumber
	 * @return
	 */
	@RequestMapping(value = "/getBluetoothCode.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult getBluetoothCode(
			@RequestParam String boxSerialNumber,
			@RequestParam String machineId) {

		ObjectResult objectResult = new ObjectResult();
		String machineId2 = machineId;
		if(machineId==null&&machineId2.equals("")){
			objectResult.setMessage("手机唯一ID不能为空");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
		}
		BoxDto boxDto = boxService.getBoxDto(boxSerialNumber, null, null, null);
		if (boxDto != null) {
			objectResult.setStatus(ResultStatus.OK);
			BoxblueCodeDto boxblueCodeDto=new BoxblueCodeDto();
			if(boxDto.getBluetoothCode()==null || "".equals(boxDto.getBluetoothCode().trim())){
				boxblueCodeDto.setBluetoothCode("000000");
			}else{
				boxblueCodeDto.setBluetoothCode(boxDto.getBluetoothCode());
			}
			boxblueCodeDto.setBoxSerialNumber(boxDto.getBoxSerialNumber());;
			boxblueCodeDto.setCardNumber(boxDto.getCardNumber());
			objectResult.setResult(boxblueCodeDto);
		} else {
			objectResult.setMessage("获取失败，未找到相关盒子信息");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	/**
	 * 获取蓝牙密码
	 * @param boxSerialNumber
	 * @param cardNumber
	 * @return
	 */
	@RequestMapping(value = "/getUserBluetoothCode.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult getUserBluetoothCode(
			@RequestParam String boxSerialNumber,
			@RequestParam String machineId,@RequestParam String userPowerkey) {

		ObjectResult objectResult = new ObjectResult();
		String machineId2 = machineId;
		if(machineId==null&&machineId2.equals("")){
			objectResult.setMessage("手机唯一ID不能为空");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
		}
		String userPowerkey2 = userPowerkey;
		if(userPowerkey==null&&userPowerkey2.equals("")){
			objectResult.setMessage("用户授权码不能为空");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
		}
		BoxDto boxDto = boxService.getBoxDto(boxSerialNumber, null, null, null);
		if (boxDto != null) {
			objectResult.setStatus(ResultStatus.OK);
			BoxblueCodeDto boxblueCodeDto=new BoxblueCodeDto();
			System.out.println(boxDto.getBluetoothCode());
			if(boxDto.getBluetoothCode()==null || "".equals(boxDto.getBluetoothCode().trim())){
				boxblueCodeDto.setBluetoothCode("000000");
			}else{
				boxblueCodeDto.setBluetoothCode(boxDto.getBluetoothCode());
			}
			
			boxblueCodeDto.setBoxSerialNumber(boxDto.getBoxSerialNumber());;
			boxblueCodeDto.setCardNumber(boxDto.getCardNumber());
			objectResult.setResult(boxblueCodeDto);
		} else {
			objectResult.setMessage("获取失败，未找到相关盒子信息");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	
	/**
	 * 保存盒子缓存位置
	 */
	@RequestMapping(value = "/saveBoxCacheLocations.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult saveBoxCacheLocation(
			@RequestBody ParamBoxLocationDataDto paramBoxLocationDataDto) {
		ObjectResult objectResult = new ObjectResult();

//		List<BoxLocationDto> boxLocationDtos=paramBoxLocationDataDto.getBoxLocationDtos();
		/*for(BoxLocationDto d:boxLocationDtos){
			System.out.println(d.getLocationSource().str());
		}*/
		
		Box box = boxService.getBox(null,paramBoxLocationDataDto.getBoxSerialNumber(), paramBoxLocationDataDto.getCardNumber(), null, null);
		if(box!=null){
			boolean result = bizLocation.saveBoxLocationData(paramBoxLocationDataDto.getBoxLocationDtos(), box);
			if (result) {
				objectResult.setStatus(ResultStatus.OK);
				objectResult.setResult(true);
			} else {
				objectResult.setMessage("BoxLocationDtos参数有误");
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
			}
		}else{
			objectResult.setMessage("未找到相关盒子信息");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	/**
	 * 获取SOS待拨号记录
	 * @return
	 */
	/*
	
	@RequestMapping(value = "/getDialrecords.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult getDialrecords() {
		ObjectResult objectResult = new ObjectResult();
		List<DialrecordDto> lto=dialRecordService.queryDialrecordDto(null, false, null, 1, 10);
		for(int i=0;i<lto.size();i++){
			Dialrecord dialrecord=dialRecordService.getDialrecord(lto.get(i).getMsgID());
			dialrecord.setHasSend(true);
			dialRecordService.updateDialrecord(dialrecord);
		}
		objectResult.setStatus(ResultStatus.OK);
		objectResult.setResult(lto);
		objectResult.setMessage("");
		return objectResult;
		}
		
		*/
	/**
	 * 发送盒子信息 ,自由联系人作为发送者的方式发送
	 * 
	 * @param postApiParam
	 * @return
	 */
	@RequestMapping(value = "sendBoxMessageByEntUser.do")
	public @ResponseBody ObjectResult sendBoxMessageByEntUser(
			@RequestParam String boxSerialNumber,@RequestParam String content,@RequestParam String userPowerKey,String latitude,String longitude,String altitude) {
		ObjectResult objectResult = new ObjectResult();
		PostApiParam postApiParam=new PostApiParam();
		postApiParam.setIsReceived(false);
		postApiParam.setContent(content);
		postApiParam.setToBoxSerialNumber(boxSerialNumber);
		postApiParam.setIsRead(false);
		postApiParam.setUserPowerKey(userPowerKey);
		if (postApiParam.getToBoxSerialNumber() != null
				&& !postApiParam.getToBoxSerialNumber().equals("")
				&& postApiParam.getContent() != null
				&& !postApiParam.getContent().equals("")
				&& postApiParam.getUserPowerKey() != null
				&& !postApiParam.getUserPowerKey().equals("")) {
			if (!CommonMethod.isValidateNumber(postApiParam
					.getToBoxSerialNumber())) {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("盒子ID错误，发送失败");
				return objectResult;
			}
			
			String contentHex=BdCommonMethod
					.castHanziStringToHexString(postApiParam.getContent());
			
			if (contentHex.length() > 104) {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("内容不可超过26个字符");
				return objectResult;
			}
			if((latitude!=null&&!latitude.equals(""))||(longitude!=null&&!longitude.equals(""))||(altitude!=null&&!altitude.equals(""))){
				if (contentHex.length() > 76) {
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("带位置，内容不可超过19个字符");
					return objectResult;
				}
			}
			try {
				/*User user = userService.getUserByPowerKey(postApiParam
						.getUserPowerKey());*/
				EnterpriseUser entUser = enterpriseUserService.getUserByPowerKey(postApiParam
						.getUserPowerKey());
				if (entUser != null) {
					// 判断频度是否到
					if (entUser.getLastSendBoxMessageTime() != null) {
						Calendar c1 = entUser.getLastSendBoxMessageTime();
						Calendar c2 = Calendar.getInstance();
						long a = (long) (c2.getTimeInMillis() - c1
								.getTimeInMillis());
						long b = a / 1000;
						long c = 5;
						if(entUser.getFreq() != 0){
							c = entUser.getFreq();
						}
						long d = c - b;
						if (d > 0) {
							objectResult.setResult(null);
							objectResult.setStatus(ResultStatus.BAD_REQUEST);
							objectResult.setMessage("频度未到，还剩" + d + "秒");
							return objectResult;
						}
					}
					
					
					BoxMessage boxMessage = new BoxMessage();
					Box toBox = boxService.getBox(BoxStatusType.NORMAL,
							postApiParam.getToBoxSerialNumber(), null, null,
							null);
					if (toBox == null) {
						objectResult.setResult(null);
						objectResult.setStatus(ResultStatus.FAILED);
						objectResult.setMessage("目标盒子不存在或者未激活");
						return objectResult;
					}
					boxMessage.setMsgId(BizMsgTrans.messageId);
					BizMsgTrans.messageId++;
					if(BizMsgTrans.messageId>255){
						BizMsgTrans.messageId=1;
					}
				    if(latitude!=null&&!latitude.equals("")){
				    	boxMessage.setLatitude(Double.parseDouble(latitude));
				    }
				    if(longitude!=null&&!longitude.equals("")){
				    	boxMessage.setLongitude(Double.parseDouble(longitude));
				    }	
				    if(altitude!=null&&!altitude.equals("")){
				    	boxMessage.setAltitude(Double.parseDouble(altitude));
				    }
					boxMessage.setToBox(toBox);
					boxMessage.setCreatedTime(Calendar.getInstance());
					boxMessage.setMsgIoType(MsgIoType.IO_EntUserTOBOX);
					boxMessage.setFamilyMob(entUser.getPhone());
					boxMessage.setMsgType(MsgType.MSG_SAFE);
					boxMessage.setContent(postApiParam.getContent());
					boxMessage.setCreatedTime(Calendar.getInstance());
					SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
			        String strTime = sdf.format(boxMessage.getCreatedTime().getTime());
			        String msgId=boxMessage.getMsgId().toString();
			        if(msgId.length()>2){
			        	msgId=msgId.substring(1);
			        }
			        if(msgId.length()==1){
			        	msgId="0"+msgId;
			        }
					String msgHzId =strTime+msgId;//消息回执ID
					postApiParam.setMsgId(Long.parseLong(msgHzId));
					
					boxMessage.setDataStatusType(DataStatusType.SENT);

					messageControl.parseRevBoxMessage(boxMessage);
					entUser.setLastSendBoxMessageTime(Calendar.getInstance());
					objectResult.setResult(postApiParam);
					objectResult.setStatus(ResultStatus.OK);
					enterpriseUserService.update(entUser);
					return objectResult;
				} else {
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("授权码无效，发送失败");
					return objectResult;
				}
					

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.SYS_ERROR);
				objectResult.setMessage("系统错误，发送失败");
				return objectResult;
			}
		} else {
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数不正确，发送失败");
			return objectResult;
		}

	}
	
	/**
	 * 发送盒子信息(用于旧版指挥调度系统)
	 * 
	 * @param postApiParam
	 * @return
	 */
	@RequestMapping(value = "sendMsgByEntUser.do")
	public @ResponseBody ObjectResult sendMsgByEntUser(
			@RequestParam String boxSerialNumber,@RequestParam String content,@RequestParam String username,String latitude,String longitude,String altitude) {
		ObjectResult objectResult = new ObjectResult();
		EnterpriseUser eu = enterpriseUserService.getEntUser(username);
		PostApiParam postApiParam=new PostApiParam();
		postApiParam.setIsReceived(false);
		postApiParam.setContent(content);
		postApiParam.setToBoxSerialNumber(boxSerialNumber);
		postApiParam.setIsRead(false);
		postApiParam.setUserPowerKey(eu.getUserPowerKey());
		if (postApiParam.getToBoxSerialNumber() != null
				&& !postApiParam.getToBoxSerialNumber().equals("")
				&& postApiParam.getContent() != null
				&& !postApiParam.getContent().equals("")
				&& postApiParam.getUserPowerKey() != null
				&& !postApiParam.getUserPowerKey().equals("")) {
			if (!CommonMethod.isValidateNumber(postApiParam
					.getToBoxSerialNumber())) {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("盒子ID错误，发送失败");
				return objectResult;
			}
			
			String contentHex=BdCommonMethod
					.castHanziStringToHexString(postApiParam.getContent());
			
			if (contentHex.length() > 104) {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("内容不可超过26个字符");
				return objectResult;
			}
			if((latitude!=null&&!latitude.equals(""))||(longitude!=null&&!longitude.equals(""))||(altitude!=null&&!altitude.equals(""))){
				if (contentHex.length() > 76) {
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("带位置，内容不可超过19个字符");
					return objectResult;
				}
			}
			try {
				/*User user = userService.getUserByPowerKey(postApiParam
						.getUserPowerKey());*/
				EnterpriseUser entUser = enterpriseUserService.getUserByPowerKey(postApiParam
						.getUserPowerKey());
				if (entUser != null) {
					// 判断频度是否到
					if (entUser.getLastSendBoxMessageTime() != null) {
						Calendar c1 = entUser.getLastSendBoxMessageTime();
						Calendar c2 = Calendar.getInstance();
						long a = (long) (c2.getTimeInMillis() - c1
								.getTimeInMillis());
						long b = a / 1000;
						long c = entUser.getFreq();
						long d = c - b;
						if (d > 0) {
							objectResult.setResult(null);
							objectResult.setStatus(ResultStatus.BAD_REQUEST);
							objectResult.setMessage("频度未到，还剩" + d + "秒");
							return objectResult;
						}
					}
					
					
					BoxMessage boxMessage = new BoxMessage();
					Box toBox = boxService.getBox(BoxStatusType.NORMAL,
							postApiParam.getToBoxSerialNumber(), null, null,
							null);
					if (toBox == null) {
						objectResult.setResult(null);
						objectResult.setStatus(ResultStatus.FAILED);
						objectResult.setMessage("目标盒子不存在或者未激活");
						return objectResult;
					}
					boxMessage.setMsgId(BizMsgTrans.messageId);
					BizMsgTrans.messageId++;
					if(BizMsgTrans.messageId>255){
						BizMsgTrans.messageId=1;
					}
				    if(latitude!=null&&!latitude.equals("")){
				    	boxMessage.setLatitude(Double.parseDouble(latitude));
				    }
				    if(longitude!=null&&!longitude.equals("")){
				    	boxMessage.setLongitude(Double.parseDouble(longitude));
				    }	
				    if(altitude!=null&&!altitude.equals("")){
				    	boxMessage.setAltitude(Double.parseDouble(altitude));
				    }
					boxMessage.setToBox(toBox);
					boxMessage.setCreatedTime(Calendar.getInstance());
					boxMessage.setMsgIoType(MsgIoType.IO_EntUserTOBOX);
					boxMessage.setFamilyMob(entUser.getPhone());
					boxMessage.setMsgType(MsgType.MSG_SAFE);
					boxMessage.setContent(postApiParam.getContent());
					boxMessage.setCreatedTime(Calendar.getInstance());
					SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
			        String strTime = sdf.format(boxMessage.getCreatedTime().getTime());
			        String msgId=boxMessage.getMsgId().toString();
			        if(msgId.length()>2){
			        	msgId=msgId.substring(1);
			        }
			        if(msgId.length()==1){
			        	msgId="0"+msgId;
			        }
					String msgHzId =strTime+msgId;//消息回执ID
					postApiParam.setMsgId(Long.parseLong(msgHzId));
					
					boxMessage.setDataStatusType(DataStatusType.SENT);

					messageControl.parseRevBoxMessage(boxMessage);
					entUser.setLastSendBoxMessageTime(Calendar.getInstance());
					objectResult.setResult(postApiParam);
					objectResult.setStatus(ResultStatus.OK);
					enterpriseUserService.update(entUser);
					return objectResult;
				} else {
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("授权码无效，发送失败");
					return objectResult;
				}
					

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.SYS_ERROR);
				objectResult.setMessage("系统错误，发送失败");
				return objectResult;
			}
		} else {
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数不正确，发送失败");
			return objectResult;
		}

	}
	
	/**
	 * 发送盒子信息(用于新版指挥调度系统)
	 * 
	 * @param postApiParam
	 * @return
	 */
	@RequestMapping(value = "sendMsgByEntUserNew.do")
	public @ResponseBody ObjectResult sendMsgByEntUser(
			@RequestParam String boxSerialNumber,@RequestParam String content,@RequestParam String username,@RequestParam String userPowerKey,String latitude,String longitude,String altitude) {
		ObjectResult objectResult = new ObjectResult();
		PostApiParam postApiParam=new PostApiParam();
		postApiParam.setIsReceived(false);
		postApiParam.setContent(content);
		postApiParam.setToBoxSerialNumber(boxSerialNumber);
		postApiParam.setIsRead(false);
		postApiParam.setUserPowerKey(userPowerKey);
		if (postApiParam.getToBoxSerialNumber() != null
				&& !postApiParam.getToBoxSerialNumber().equals("")
				&& postApiParam.getContent() != null
				&& !postApiParam.getContent().equals("")
				&& postApiParam.getUserPowerKey() != null
				&& !postApiParam.getUserPowerKey().equals("")) {
			if (!CommonMethod.isValidateNumber(postApiParam
					.getToBoxSerialNumber())) {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("盒子ID错误，发送失败");
				return objectResult;
			}
			
			String contentHex=BdCommonMethod
					.castHanziStringToHexString(postApiParam.getContent());
			
			if (contentHex.length() > 104) {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("内容不可超过26个字符");
				return objectResult;
			}
			if((latitude!=null&&!latitude.equals(""))||(longitude!=null&&!longitude.equals(""))||(altitude!=null&&!altitude.equals(""))){
				if (contentHex.length() > 76) {
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("带位置，内容不可超过19个字符");
					return objectResult;
				}
			}
			try {
				/*User user = userService.getUserByPowerKey(postApiParam
						.getUserPowerKey());*/
				EnterpriseUser entUser = enterpriseUserService.getUserByPowerKey(postApiParam
						.getUserPowerKey());
				if (entUser != null) {
					// 判断频度是否到
					if (entUser.getLastSendBoxMessageTime() != null) {
						Calendar c1 = entUser.getLastSendBoxMessageTime();
						Calendar c2 = Calendar.getInstance();
						long a = (long) (c2.getTimeInMillis() - c1
								.getTimeInMillis());
						long b = a / 1000;
						long c = entUser.getFreq();
						long d = c - b;
						if (d > 0) {
							objectResult.setResult(null);
							objectResult.setStatus(ResultStatus.BAD_REQUEST);
							objectResult.setMessage("频度未到，还剩" + d + "秒");
							return objectResult;
						}
					}
					
					
					BoxMessage boxMessage = new BoxMessage();
					Box toBox = boxService.getBox(BoxStatusType.NORMAL,
							postApiParam.getToBoxSerialNumber(), null, null,
							null);
					if (toBox == null) {
						objectResult.setResult(null);
						objectResult.setStatus(ResultStatus.FAILED);
						objectResult.setMessage("目标盒子不存在或者未激活");
						return objectResult;
					}
					boxMessage.setMsgId(BizMsgTrans.messageId);
					BizMsgTrans.messageId++;
					if(BizMsgTrans.messageId>255){
						BizMsgTrans.messageId=1;
					}
				    if(latitude!=null&&!latitude.equals("")){
				    	boxMessage.setLatitude(Double.parseDouble(latitude));
				    }
				    if(longitude!=null&&!longitude.equals("")){
				    	boxMessage.setLongitude(Double.parseDouble(longitude));
				    }	
				    if(altitude!=null&&!altitude.equals("")){
				    	boxMessage.setAltitude(Double.parseDouble(altitude));
				    }
					boxMessage.setToBox(toBox);
					boxMessage.setCreatedTime(Calendar.getInstance());
					boxMessage.setMsgIoType(MsgIoType.IO_EntUserTOBOX);
					boxMessage.setFamilyMob(username);
					boxMessage.setMsgType(MsgType.MSG_SAFE);
					boxMessage.setContent(postApiParam.getContent());
					boxMessage.setCreatedTime(Calendar.getInstance());
					SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
			        String strTime = sdf.format(boxMessage.getCreatedTime().getTime());
			        String msgId=boxMessage.getMsgId().toString();
			        if(msgId.length()>2){
			        	msgId=msgId.substring(1);
			        }
			        if(msgId.length()==1){
			        	msgId="0"+msgId;
			        }
					String msgHzId =strTime+msgId;//消息回执ID
					postApiParam.setMsgId(Long.parseLong(msgHzId));
					
					boxMessage.setDataStatusType(DataStatusType.SENT);

					messageControl.parseRevBoxMessage(boxMessage);
					entUser.setLastSendBoxMessageTime(Calendar.getInstance());
					objectResult.setResult(postApiParam);
					objectResult.setStatus(ResultStatus.OK);
					enterpriseUserService.update(entUser);
					return objectResult;
				} else {
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("授权码无效，发送失败");
					return objectResult;
				}
					

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.SYS_ERROR);
				objectResult.setMessage("系统错误，发送失败");
				return objectResult;
			}
		} else {
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数不正确，发送失败");
			return objectResult;
		}

	}
	
	/**
	 * 第三方用户平台发送16进制自定义盒子信息
	 * 
	 * @param postApiParam
	 * @return
	 */
	@RequestMapping(value = "sendBoxMessageFreeByEntUser.do")
	public @ResponseBody ObjectResult sendBoxMessageFreeByEntUser(
			@RequestParam String boxSerialNumber,@RequestParam String content,@RequestParam String userPowerKey) {
		ObjectResult objectResult = new ObjectResult();
		PostApiParam postApiParam=new PostApiParam();
		postApiParam.setIsReceived(false);
		postApiParam.setContent(content);
		postApiParam.setToBoxSerialNumber(boxSerialNumber);
		postApiParam.setIsRead(false);
		postApiParam.setUserPowerKey(userPowerKey);
		if (postApiParam.getToBoxSerialNumber() != null
				&& !postApiParam.getToBoxSerialNumber().equals("")
				&& postApiParam.getContent() != null
				&& !postApiParam.getContent().equals("")
				&& postApiParam.getUserPowerKey() != null
				&& !postApiParam.getUserPowerKey().equals("")) {
			if (!CommonMethod.isValidateNumber(postApiParam
					.getToBoxSerialNumber())) {
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("盒子ID错误，发送失败");
				return objectResult;
			}
			
			if (!CommonMethod.isHex(postApiParam.getContent())) {
				objectResult.setResult(false);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("电文内容格式有误");
				return objectResult;
			}
			if (postApiParam.getContent().length() > 156) {
				objectResult.setResult(false);
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("电文内容不可超过156个字节");
				return objectResult;
			}
			
			try {
				/*User user = userService.getUserByPowerKey(postApiParam
						.getUserPowerKey());*/
				EnterpriseUser entUser = enterpriseUserService.getUserByPowerKey(postApiParam
						.getUserPowerKey());
				if (entUser != null) {
					// 判断频度是否到
					if (entUser.getLastSendBoxMessageTime() != null) {
						Calendar c1 = entUser.getLastSendBoxMessageTime();
						Calendar c2 = Calendar.getInstance();
						long a = (long) (c2.getTimeInMillis() - c1
								.getTimeInMillis());
						long b = a / 1000;
						long c = 5;
						if (entUser.getFreq() != 0) {
							c = entUser.getFreq();
						}
						long d = c - b;
						if (d > 0) {
							objectResult.setResult(null);
							objectResult.setStatus(ResultStatus.FAILED);
							objectResult.setMessage("频度未到，还剩" + d + "秒");
							return objectResult;
						}
					}
					UserSendBoxMessage userSendBoxMessage = new UserSendBoxMessage();
					Box toBox = boxService.getBox(BoxStatusType.NORMAL,
							postApiParam.getToBoxSerialNumber(), null, null,
							null);
					if (toBox == null) {
						objectResult.setResult(null);
						objectResult.setStatus(ResultStatus.FAILED);
						objectResult.setMessage("目标盒子不存在或者未激活");
						return objectResult;
					}
					BizMsgTrans.messageId++;
					if(BizMsgTrans.messageId>255){
						BizMsgTrans.messageId=1;
					}
					userSendBoxMessage.setBoxSerialNumber(postApiParam.getToBoxSerialNumber());
					userSendBoxMessage.setContent(postApiParam.getContent());
					userSendBoxMessage.setCreatedTime(Calendar.getInstance());
					//userSendBoxMessage.setFromUser(user);
					userSendBoxMessage.setMsgIoType(MsgIoType.IO_EntUserToBoxFree);
					userSendBoxMessage.setEntUser(entUser);
					userSendBoxMessage.setIsRead(false);
					if(postApiParam.getIsRead()!=null&&postApiParam.getIsRead()){
					userSendBoxMessage.setIsRead(postApiParam.getIsRead());
					}
					userSendBoxMessage.setIsReceived(false);
					if(postApiParam.getIsReceived()!=null&&postApiParam.getIsReceived()){
						userSendBoxMessage.setIsReceived(postApiParam.getIsReceived());
					}
					SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
			        String strTime = sdf.format(userSendBoxMessage.getCreatedTime().getTime());
			        String msgId=Integer.toString(BizMsgTrans.messageId);
			        if(msgId.length()>2){
			        	msgId=msgId.substring(1);
			        }
			        if(msgId.length()==1){
			        	msgId="0"+msgId;
			        }
					String msgHzId =strTime+msgId;//消息回执ID
					userSendBoxMessage.setMsgId(Long.parseLong(msgHzId.toString()));
					postApiParam.setMsgId(Long.parseLong(msgHzId));

					messageControl.parseUserSendBoxMessage(userSendBoxMessage);
					entUser.setLastSendBoxMessageTime(Calendar.getInstance());
					objectResult.setResult(postApiParam);
					objectResult.setStatus(ResultStatus.OK);
					enterpriseUserService.update(entUser);
					return objectResult;
				} else {
					objectResult.setResult(null);
					objectResult.setStatus(ResultStatus.FAILED);
					objectResult.setMessage("授权码无效，发送失败");
					return objectResult;
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.SYS_ERROR);
				objectResult.setMessage("系统错误，发送失败");
				return objectResult;
			}
		} else {
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("参数不正确，发送失败");
			return objectResult;
		}

	}
	
	
}

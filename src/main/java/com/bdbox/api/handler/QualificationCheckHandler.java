package com.bdbox.api.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bdbox.constant.CheckStatus;
import com.bdbox.constant.MailType;
import com.bdbox.entity.Invoice;
import com.bdbox.entity.Notification;
import com.bdbox.entity.QualificationCheck;
import com.bdbox.entity.User;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.InvoiceService;
import com.bdbox.service.MessageRecordService;
import com.bdbox.service.NotificationService;
import com.bdbox.service.QualificationCheckService;
import com.bdbox.service.UserService;
import com.bdbox.web.dto.InvoiceDto;
import com.bdbox.web.dto.QualificationCheckDto;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;

@Controller
public class QualificationCheckHandler {

	@Autowired
	private QualificationCheckService qualificationCheckService;
	
	@Autowired
	private InvoiceService invoiceService;
	
	@Autowired
	private UserService userService;
	
	@Autowired 
	private NotificationService notificationService; 
	 
	@Autowired  
	private MailNotification mailNotification; 
	
	@Autowired
	private MessageRecordService MessageRecordService;
	
	@RequestMapping(value = "saveQualificationCheck.do")
	@ResponseBody
	public ObjectResult save(String businessLicense, String taxReg, 
			String orgCode, String generalTax, String invoiceDatum, Long invoice, Long id){
		return qualificationCheckService.save(businessLicense, taxReg, orgCode, generalTax, invoiceDatum, invoice, id);
	}
	
	@RequestMapping(value = "getQualificationCheckAll.do")
	@ResponseBody
	public ListParam query(@RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
			@RequestParam int iColumns, @RequestParam String sEcho,
			@RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0){
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		
		List<QualificationCheckDto> dtos = qualificationCheckService.query(page, pageSize);
		int count = qualificationCheckService.queryAcount();
		
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(dtos);
		return listParam; 
	}
	
	@RequestMapping(value = "table_checkDetail")
	@ResponseBody
	public ModelAndView checkDetail(Long id, @RequestParam(value="invoiceId", required=false) Long invoiceId){
		ModelAndView mav = new ModelAndView();  
		if(invoiceId!=null){
			QualificationCheck q = qualificationCheckService.getQualificationCheck(invoiceId);
			id = q.getId();
		}
		
		//返回审核信息
		QualificationCheckDto dto = qualificationCheckService.getDto(id);
		if(dto!=null){
			mav.addObject("id",id);
			mav.addObject("businessLicense", dto.getBusinessLicense());
			mav.addObject("taxReg",dto.getTaxReg());
			mav.addObject("generalTax", dto.getGeneralTax());
			mav.addObject("orgCode", dto.getOrgCode());
			mav.addObject("invoiceDatum", dto.getInvoiceDatum());
			mav.addObject("busLicStatus", dto.getBusLicStatus()+"");
			mav.addObject("taxRegStatus", dto.getTaxRegStatus()+"");
			mav.addObject("generalTaxStatus", dto.getGeneralTaxStatus()+"");
			mav.addObject("orgCodeStauts", dto.getOrgCodeStauts()+"");
			mav.addObject("invoiceDatumStatus", dto.getInvoiceDatumStatus()+"");
			mav.addObject("busLicRemark", dto.getBusLicRemark()==null?" " : dto.getBusLicRemark());
			mav.addObject("taxRegRemark", dto.getTaxRegRemark()==null?" " : dto.getTaxRegRemark());
			mav.addObject("generalTaxRemark", dto.getGeneralTaxRemark()==null?" " : dto.getGeneralTaxRemark());
			mav.addObject("orgCodeRemark", dto.getOrgCodeRemark()==null?" " : dto.getOrgCodeRemark());
			mav.addObject("invoiceDatumRemark", dto.getInvoiceDatumRemark()==null?" " : dto.getInvoiceDatumRemark());
			mav.addObject("invoice", dto.getInvoice());
			//获取发票信息
			InvoiceDto iDto = invoiceService.getInvoiceDto(dto.getInvoice());
			mav.addObject("companyName",iDto.getCompanyName());
			mav.addObject("taxpayerIdNum", iDto.getTaxpayerIdNum());
			mav.addObject("registerAddr", iDto.getRegisterAddr());
			mav.addObject("registerPhone",iDto.getRegisterPhone());
			mav.addObject("bankName",iDto.getBankName());
			mav.addObject("bankAccount",iDto.getBankAccount());
			mav.addObject("ticketsInfo",iDto.getTicketsInfo());
			mav.addObject("ticketsPhone",iDto.getTicketsPhone());
			mav.addObject("ticketsAddr",iDto.getTicketsAddr());
			mav.addObject("detailAddr",iDto.getDetailAddr());
			mav.addObject("createTime",iDto.getCreatedTime());
			//获取用户
			User user = userService.getUser(iDto.getUser());
			mav.addObject("user",user.getName()==null?user.getUsername():user.getName());
			
			mav.setViewName("table_checkDetail");
		}
		return mav;
	}
	
	@RequestMapping(value = "updateQC.do")
	@ResponseBody
	public ObjectResult update(Long id, Boolean busLicStatus, Boolean taxRegStatus, Boolean orgCodeStauts, 
			Boolean generalTaxStatus, Boolean invoiceDatumStatus, String busLicRemark, String taxRegRemark, 
			String orgCodeRemark, String generalTaxRemark, String invoiceDatumRemark, Long invoice){
		ObjectResult result = new ObjectResult();
		try {
			QualificationCheck qc = qualificationCheckService.get(id);
			qc.setBusLicStatus(busLicStatus);
			qc.setTaxRegStatus(taxRegStatus);
			qc.setOrgCodeStauts(orgCodeStauts);
			qc.setGeneralTaxStatus(generalTaxStatus);
			qc.setInvoiceDatumStatus(invoiceDatumStatus);
			qc.setBusLicRemark(busLicRemark);
			qc.setTaxRegRemark(taxRegRemark);
			qc.setOrgCodeRemark(orgCodeRemark);
			qc.setGeneralTaxRemark(generalTaxRemark);
			qc.setInvoiceDatumRemark(invoiceDatumRemark);
			Invoice i = invoiceService.getInvoice(invoice);
			CheckStatus status = null;
			if(busLicStatus && taxRegStatus && orgCodeStauts && generalTaxStatus && invoiceDatumStatus){
				status = CheckStatus.PASS;
			}else{
				status = CheckStatus.PASSFAIL;
			}
			i.setCheckStatus(status);
			//更新审核表的数据
			qualificationCheckService.update(qc);
			//更新发票表的数据
			invoiceService.updateInvoice(i);
			User user=userService.getUser(i.getUser());
			String phone=null;
			if(user!=null){
				phone=user.getUsername();
			}
			//发送审核结果给用户
			if(busLicStatus!=null&&busLicStatus&&taxRegStatus!=null&&taxRegStatus&&orgCodeStauts!=null&&orgCodeStauts&&generalTaxStatus!=null&&generalTaxStatus){
				//以下是实名认证审核通过后给用户发送短信通知
	 			StringBuffer content=new StringBuffer(); 
				String type="FAPIAOSHENHEPASS";  //通知类型  
				String type_twl="customer"; 
				String invoices="";            //是否开发票
				String tomail=null;            //收信箱
				String subject=null;           //主题
				String mailcentent=null;       //内容
				Notification notification=notificationService.getnotification(type,type_twl);
				if(notification!=null){ 
					if(notification.getIsnote().equals(true)){   
							content.append(notification.getNotecenter());
							if(content!=null){
								mailcentent=content.toString(); 
							}
						//mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
						mailNotification.sendNote(phone,mailcentent);
						//保存发送的短信记录
						MessageRecordService.save(phone, 
								mailcentent, MailType.switchStatus(type));
					}
				} 
			}else{  
				//以下是实名认证审核不通过后给用户发送短信通知
	 			StringBuffer content=new StringBuffer(); 
				String type="FAPIAOSHENHE";  //通知类型  
				String type_twl="customer"; 
				String invoices="";            //是否开发票
				String tomail=null;            //收信箱
				String subject=null;           //主题
				String mailcentent=null;       //内容
				Notification notification=notificationService.getnotification(type,type_twl);
				if(notification!=null){ 
					if(notification.getIsnote().equals(true)){   
							content.append(notification.getNotecenter());
							if(content!=null){
								mailcentent=content.toString(); 
							}
						//mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
						mailNotification.sendNote(phone,mailcentent);
						//保存发送的短信记录
						MessageRecordService.save(phone, 
								mailcentent, MailType.switchStatus(type));
					}
				}
			}
			result.setMessage("更新成功");
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			result.setMessage("更新失败");
			result.setStatus(ResultStatus.FAILED);
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value="getQualificationCheck.do")
	@ResponseBody
	public ObjectResult getQualificationCheck(Long invoiceId){
		ObjectResult result = new ObjectResult();
		QualificationCheckDto dto = qualificationCheckService.getQualificationCheck(invoiceId);
		if(dto!=null){
			result.setMessage("查询成功");
			result.setResult(dto);
			result.setStatus(ResultStatus.OK);
		}else{
			result.setMessage("查无此数据");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}
}

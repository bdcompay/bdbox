package com.bdbox.api.handler;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bdbox.api.dto.RealNameDto;
import com.bdbox.api.dto.RegisterInformationDto;
import com.bdbox.api.dto.UserDto;
import com.bdbox.constant.MailType;
import com.bdbox.constant.MobKeyType;
import com.bdbox.constant.UserStatusType;
import com.bdbox.constant.UserType;
import com.bdbox.entity.MobCode;
import com.bdbox.entity.Notification;
import com.bdbox.entity.RegisterInformation;
import com.bdbox.entity.User;
import com.bdbox.entity.UserPhotoUrl;
import com.bdbox.entity.UserRegLog;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.MessageRecordService;
import com.bdbox.service.MobCodeService;
import com.bdbox.service.NotificationService;
import com.bdbox.service.RegisterInformationService;
import com.bdbox.service.UserPhotoUrlService;
import com.bdbox.service.UserRegLogService;
import com.bdbox.service.UserService;
import com.bdbox.util.IpUtil;
import com.bdbox.web.dto.ManageUserDto;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Controller
public class UserHandler {
	@Autowired
	private UserService userService;
	@Autowired
	private MobCodeService mobCodeService;
	@Autowired
	private UserPhotoUrlService userPhotoUrlService; 
	@Autowired
	private RegisterInformationService registerInformationService;
	@Autowired
	private UserRegLogService userRegLogService;
	@Autowired 
	private NotificationService notificationService; 
	@Autowired  
	private MailNotification mailNotification; 
	@Autowired
	private MessageRecordService MessageRecordService;
	
	
	@RequestMapping(value = "/getKey.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult getKey(
			@RequestParam(required = true) String username,
			@RequestParam(required = true) String mobKeyTypeStr,HttpServletRequest request) {

		MobKeyType mobKeyType = MobKeyType.getFromString(mobKeyTypeStr);
		ObjectResult objectResult = null;
		if (mobKeyType == MobKeyType.MOBKEY_REGISTER) {
			objectResult = userService.createRegisterKey(username);
		} else if (mobKeyType == MobKeyType.MOBKEY_RESETPWD) {
			objectResult = userService.createChangePasswordKey(username);
		} else if (mobKeyType == MobKeyType.MOBKEY_FORGETPWD) {
			objectResult = userService.createForgetPasswordKey(username);
		} else if (mobKeyType == MobKeyType.MOBKEY_CHANGEINFO) {
			objectResult = userService.createChangeUserInfoKey(username);
		} else if (mobKeyType == MobKeyType.MOBKEY_APPLYPWD) {
			objectResult = userService.createApplyCode(username);
		} else {
			objectResult = new ObjectResult();
			objectResult.setMessage("请求中提交了未知验证码类型");
			objectResult.setResult(false);
			return objectResult;
		}
		try {
			if ((Boolean) objectResult.getResult()) {
				objectResult.setMessage("验证码已发送");
				objectResult.setStatus(ResultStatus.OK);
			} else {
				objectResult.setMessage("验证码发送失败，"+objectResult.getMessage());
				objectResult.setStatus(ResultStatus.FAILED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}

	@RequestMapping(value = "/registerFinish.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult registerFinish(
			@RequestParam(required = true) String username,
			@RequestParam(required = true) String password,
			@RequestParam(required = true) String mobkey,
			@RequestParam(required = true) String ip) {
		ObjectResult objectResult = new ObjectResult();
		try {
			UserDto userDto = userService.registerUser(username, password, mobkey);
			objectResult.setResult(userDto);
			if (userDto != null) {
				objectResult.setMessage("注册成功");
				//注册成功
				UserRegLog userRegLog = new UserRegLog();
				userRegLog.setUsername(username);
				userRegLog.setIp(ip);
				String ipAddr = IpUtil.getIpAddr(ip);
				userRegLog.setIpArea(ipAddr);
				userRegLogService.save(userRegLog);
				objectResult.setStatus(ResultStatus.OK);
			} else {
				objectResult.setMessage("注册失败");
				objectResult.setStatus(ResultStatus.FAILED);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}

	@RequestMapping(value = "/login.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult login(
			@RequestParam(required = true) String username,
			@RequestParam(required = true) String password) {
		ObjectResult objectResult = new ObjectResult();
		try {
			UserDto userDto = userService.login(username, password);
			objectResult.setResult(userDto);
			if (userDto != null) {
				objectResult.setMessage("登录成功");
				objectResult.setStatus(ResultStatus.OK);
			} else {
				objectResult.setMessage("登录失败");
				objectResult.setStatus(ResultStatus.FAILED);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}

	/**
	 * 
	 * 修改用户资料
	 * 
	 * @param username
	 * @param password
	 * @param mobkey
	 * @return
	 */
	@RequestMapping(value = "/changeUserInfo.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult changeUserInfo(
			@RequestParam(required = true) String username,
			@RequestParam(required = true) String newUsername,
			@RequestParam(required = true) String mobkey) {
		ObjectResult objectResult = new ObjectResult();
		try {
			UserDto userDto = userService.changeUserInfo(newUsername,username, mobkey);
			objectResult.setResult(userDto);
			if (userDto != null) {
				objectResult.setStatus(ResultStatus.OK);
			} else {
				objectResult.setMessage("更新信息失败");
				objectResult.setStatus(ResultStatus.FAILED);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}

	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public @ResponseBody ObjectResult changePassword(
			@RequestParam(required = true) String username,
			@RequestParam(required = true) String password,
			@RequestParam(required = false) String oldPassword,
			@RequestParam(required = false) String mobkey) {
		ObjectResult objectResult = new ObjectResult();
		try {
			UserDto userDto = userService.changePassword(username, password,
					mobkey,oldPassword);
			objectResult.setResult(userDto);
			if (userDto != null) {
				objectResult.setMessage("成功更改密码");
				objectResult.setStatus(ResultStatus.OK);
			} else {
				objectResult.setMessage("验证码错误");
				objectResult.setStatus(ResultStatus.FAILED);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;

	}

	@RequestMapping(value = "/getUserInfo", method = RequestMethod.POST)
	public @ResponseBody ObjectResult getUserInfo(
			@RequestParam(required = true) String username,
			@RequestParam(required = true) String systemCode) {
		ObjectResult objectResult = new ObjectResult();
		try {
			if (userService.checkSystemCode(systemCode)) {
				UserDto userDto = userService.queryUser(username);
				objectResult.setResult(userDto);
				if (userDto != null) {
					objectResult.setStatus(ResultStatus.OK);
				} else {
					objectResult.setMessage("无该用户");
					objectResult.setStatus(ResultStatus.FAILED);
				}
			} else {
				objectResult.setResult(null);
				objectResult.setMessage("授权码错误");
				objectResult.setStatus(ResultStatus.FAILED);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;

	}

	@RequestMapping(value = "/postSuggestion.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult postSuggestion(
			@RequestParam(required = true) String boxSerialNumber,
			@RequestParam(required = true) String content,
			@RequestParam(required = false) String username,
			@RequestParam(required = false) String phone,
			@RequestParam(required = false) String qq) {
		ObjectResult objectResult = new ObjectResult();
		try {
			Boolean result = userService.postSuggestion(boxSerialNumber,
					content,username,phone,qq);
			objectResult.setResult(result);
			if (result) {
				objectResult.setMessage("成功提交建议");
				objectResult.setStatus(ResultStatus.OK);
			} else {
				objectResult.setStatus(ResultStatus.FAILED);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}
	
	/**
	 * 发送邮箱验证码
	 * @param toMail
	 * @return
	 */
	@RequestMapping(value="getMailKey",method=RequestMethod.POST)
	public @ResponseBody ObjectResult getMailKey(long uid,String toAddress){
		//发送
		return userService.createMailKey(uid,toAddress);
		
	}
	
	/**
	 * 修改用户信息
	 * @param userDto
	 * @return
	 */
	@RequestMapping(value="changeUser.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult updateUser(UserDto userDto){
		User user = userService.getUser(userDto.getId());
		BeansUtil.copyBean(user, userDto, true);
		
		if(userDto.getBirthDate()!=null&&!"".equals(userDto.getBirthDate())){
			try {
				user.setBirthDate(CommonMethod.StringToCalendar(userDto.getBirthDate(),"yyyy/MM/dd"));
			} catch (ParseException e) {
				e.printStackTrace();
				System.err.println("String转换Calendar失败");
			} 
		}
		
		userService.updateUser(user);
		
		ObjectResult objectResult = new ObjectResult();
		objectResult.setMessage("修改用户信息成功");
		objectResult.setStatus(ResultStatus.OK);
		return objectResult;
	}
	
	/**
	 * 添加或修改邮箱
	 * @param mail
	 * @param mobKey
	 * @param id
	 * @return
	 */
	@RequestMapping(value="saveOrUpdateMail.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult saveOrUpdateMail(String mail,String mobKey,Long id){
		ObjectResult objectResult = new ObjectResult();
		User user = userService.getUser(id);
		if(user!=null){
			if(user.getMobileKey().equals(mobKey)){
				user.setMobileKey(null);
				user.setMail(mail);
				userService.updateUser(user);
			}else{
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("验证码错误");
			}
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("用户不存在");
		}
		return objectResult;
	}
	
	/**************************************管理平台*****************************************/
	/**
	 * 查询
	 * @param iDisplayStart	从第几个开始取值
	 * @param iDisplayLength	每页显示数量
	 * @param iColumns	该表格列的数量
	 * @param sEcho	响应次数
	 * @param iSortCol_0
	 * @param sSortDir_0	排序方式
	 * 
	 * @param userTypeStr
	 * @param username
	 * @param userStatusType
	 * @return
	 */
	@RequestMapping(value="listUser.do")
	public @ResponseBody ListParam listUser(
			@RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
			@RequestParam int iColumns, @RequestParam String sEcho,
			@RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0,
			@RequestParam(required=false) String userTypeStr,@RequestParam(required=false) String username,
			@RequestParam(required=false) String userStatusTypeStr,@RequestParam(required=false) String order){
		
		//计算页号
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		
		if (username != null) {
			if (username.equals("")) {
				username = null;
			}
		}
		
		UserType userType = UserType.switchUserType(userTypeStr);
		UserStatusType userStatusType = UserStatusType.switchUserStatusType(userStatusTypeStr);
		
		List<ManageUserDto> list = userService.query(userType, username, null, userStatusType, null, null, "createdTime DESC", page, pageSize);
		Integer count = userService.queryCount(userType, username, null, userStatusType, null, null);
		ListParam listParam = new ListParam();
		listParam.setAaData(list);
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		return listParam;
	}
	
	/**
	 * 删除
	 * @param systemCode
	 * @param id
	 * @return
	 */
	@RequestMapping(value="deleteUser.do")
	public String deleteUser(String systemCode,Long id){
		if(userService.checkSystemCode(systemCode)){
			userService.deleteUser(id);
			return "redirect:/table_user";
		}else{
			return null;
		}
	}
	
	/**
	 * 查询
	 * @param systemCode
	 * @param id
	 * @return
	 */
	@RequestMapping(value="getManageUserDto.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult getUser(String systemCode,Long id){
		ObjectResult objectResult = new ObjectResult();
		if(userService.checkSystemCode(systemCode)){
			objectResult.setMessage("查询成功！");
			objectResult.setResult(userService.getManageUserDto(id));
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setMessage("查询失败");
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	/**
	 * 修改
	 * @param manageUserDto
	 * @return
	 */
	@RequestMapping(value="updateUser.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult updateUser(ManageUserDto manageUserDto,String systemCode){
		if(userService.checkSystemCode(systemCode)){
			User user = userService.getUser(manageUserDto.getId());
			BeansUtil.copyBean(user, manageUserDto, true);
			user.setUsername(manageUserDto.getUsername());
			user.setUserStatusType(UserStatusType.switchUserStatusType(manageUserDto.getUserStatusType()));
			user.setUserType(UserType.switchUserType(manageUserDto.getUserType()));
			if(manageUserDto.getBirthDate()!=null&&!"".equals(manageUserDto.getBirthDate())){
				try {
					user.setBirthDate(CommonMethod.StringToCalendar(manageUserDto.getBirthDate(),"yyyy/MM/dd"));
				} catch (ParseException e) {
					e.printStackTrace();
					System.err.println("String转换Calendar失败");
				} 
			}
			if(manageUserDto.getFreq()!=null&&!"".equals(manageUserDto.getFreq())){
				try {
				user.setFreq(Integer.parseInt(manageUserDto.getFreq()));
				} catch (Exception e) {
					e.printStackTrace();
					System.err.println("String转换int失败");
				} 
			}
			userService.updateManageUser(user);
		}
		ObjectResult objectResult = new ObjectResult();
		objectResult.setMessage("修改成功");
		objectResult.setStatus(ResultStatus.OK);
		return objectResult;
	}
	
	/**
	 * 获取所有用户
	 * @return
	 */
	@RequestMapping(value="listAllUser.do",method=RequestMethod.GET)
	public @ResponseBody ObjectResult listAll(){
		
		List<UserDto> list = userService.listAll();
		List<String> string = new ArrayList<String>();
		for(UserDto userDto : list){
			string.add(userDto.getUsername());
		}
		ObjectResult objectResult = new ObjectResult();
		objectResult.setMessage("查询成功");
		objectResult.setResult(string);
		objectResult.setStatus(ResultStatus.OK);
		return objectResult;
	}
	
	/**
	 * 添加用户
	 * @param user
	 * @return
	 */
	@RequestMapping(value="saveUser",method=RequestMethod.POST)
	public @ResponseBody ObjectResult saveUser(UserDto userDto,HttpServletRequest request){
		String ip = IpUtil.getIp(request);
		
		User user = new User();
		userDto.getFreq();
		System.out.println(userDto.getFreq());
		BeansUtil.copyBean(user, userDto, true);
		if(userDto.getBirthDate()!=null&&!"".equals(userDto.getBirthDate())){
			try {
				user.setBirthDate(CommonMethod.StringToCalendar(userDto.getBirthDate(),"yyyy/MM/dd"));
			} catch (ParseException e) {
				e.printStackTrace();
				System.err.println("String转换Calendar失败");
			} 
		}
		if(userDto.getFreq()!=null&&!"".equals(userDto.getFreq())){
			try {
			user.setFreq(Integer.parseInt(userDto.getFreq()));
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("String转换int失败");
			} 
		}
		user.setUserStatusType(UserStatusType.switchUserStatusType(userDto.getUserStatusTypeStr()));
		userService.saveUser(user,ip);
		ObjectResult objectResult = new ObjectResult();
		objectResult.setMessage("添加成功");
		objectResult.setStatus(ResultStatus.OK);
		return objectResult;
	}
	
	/**
	 * 匹配验证码的一致性
	 * @param phone
	 * @param code
	 * @return
	 */
	@RequestMapping(value = "compareCode.do", method=RequestMethod.POST)
	public @ResponseBody ObjectResult compareCode(String phone, String code){
		MobCode mc = mobCodeService.findCodeByPhone(phone);
		ObjectResult objectResult = new ObjectResult();
		Long now = System.currentTimeMillis();
		if(mc!=null){
			if((now - mc.getCreateTime().getTimeInMillis()) > 600000){
				objectResult.setMessage("验证码已超过有效期");
				objectResult.setStatus(ResultStatus.BAD_REQUEST);
			} else {
				if(code.equals(mc.getCode())){
					objectResult.setMessage("验证通过");
					objectResult.setStatus(ResultStatus.OK);
				} else {
					objectResult.setMessage("验证失败");
					objectResult.setStatus(ResultStatus.FAILED);
				}
			}
		} else {
			objectResult.setMessage("该号码还未获取验证码");
			objectResult.setStatus(ResultStatus.NOT_EXIST);
		}
		return objectResult;
	}
	
	/**
	 * 实名制用户填写资料
	 * @param realName	实名制姓名
	 * @param identification	身份证号码
	 * @param telephone	手机号
	 * @param codeNumber  手机验证码 
	 * @return
	 */ 
	@RequestMapping(value="updateUserRealNname.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult updateUserNealNname(String realName,
			 String identification , String mobilePhone, String id){
		ObjectResult objectResult = new ObjectResult();  
		User user = userService.getUser(Long.valueOf(id));
 		user.setName(realName);
		user.setIdentification(identification);
		user.setMobilePhone(mobilePhone);
		user.setIsAutonym(false);
 		boolean result=userService.updateUser(user);
		if(result){
			objectResult.setMessage("修改用户信息成功");
			objectResult.setStatus(ResultStatus.OK);
 		}else{
 			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("资料提交失败");
		}
		return objectResult;
	}
	
	/**
	 * 实名制用户上传照片
	 * @param headPortrait	头像
	 * @param facadePortrait 身份证正面照
	 * @param revePortrait	身份证反面照
 	 * @return
	 */
	@RequestMapping(value="upload.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult saveheadPortrait(String photoUrl1,String number,
			 Long id, HttpServletRequest request){
		ObjectResult objectResult = new ObjectResult(); 
		UserPhotoUrl userPhotoUrl=userPhotoUrlService.getUserPhotoUrl(Long.valueOf(id));
		User user=userService.getUser(id);
		//如果用户已第一次上传头像则进行保存，否则只更新
		if(userPhotoUrl==null){
			UserPhotoUrl url=new UserPhotoUrl();
			if(number.equals("0")){
				url.setHeadPhotoUrl(photoUrl1);
				url.setUserId(id);
			}else if(number.equals("1")){
				url.setFacadePhotoUrl(photoUrl1);
				url.setUserId(id); 
			}else if(number.equals("2")){
				url.setRevePhotoUrl(photoUrl1);
				url.setUserId(id); 
			}
			UserPhotoUrl userPhotoUrl2=userPhotoUrlService.save(url);
			if(userPhotoUrl2!=null){
				objectResult.setMessage("头像上传成功");
				objectResult.setStatus(ResultStatus.OK);
				user.setReason("check");
				user.setIsAutonym(false);
				Calendar time=Calendar.getInstance();
				user.setIsAutonymTime(time);
				userService.updateManageUser(user); 
			}else{
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("头像上传失败");
			}
		}else{ 
			if(number.equals("0")){
				userPhotoUrl.setHeadPhotoUrl(photoUrl1);
 			}else if(number.equals("1")){
 				userPhotoUrl.setFacadePhotoUrl(photoUrl1);
 			}else if(number.equals("2")){
				userPhotoUrl.setRevePhotoUrl(photoUrl1);
 			}
			boolean result=userPhotoUrlService.updates(userPhotoUrl);
			if(result){
				objectResult.setMessage("头像上传成功");
				objectResult.setStatus(ResultStatus.OK);
				user.setReason("check");
				user.setIsAutonym(false);
				Calendar time=Calendar.getInstance();
				user.setIsAutonymTime(time);
				userService.updateManageUser(user);  
				
				//以下是用户提交实名认证提交资料后给管理员发送邮件通知
				if(number.equals("2")){ 
		 			StringBuffer content=new StringBuffer(); 
					String type="SHIMZHI";  //通知类型  
					String type_twl="admin"; 
	 				String tomail=null;            //收信箱
					String subject=null;           //主题
					String mailcentent=null;       //内容
					Notification notification=notificationService.getnotification(type,type_twl);
					if(notification!=null){ 
						if(notification.getIsemail().equals(true)){   
							tomail=notification.getTomail();
							subject=notification.getSubject();
								content.append(notification.getCenter());
								if(content!=null){
									mailcentent=content.toString(); 
								}
								String [] email=tomail.split(",");
	    						for(String str:email){
	    							try {
		    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
									} catch (Exception e) {
										e.printStackTrace();
									}
	    						}
	 						//mailNotification.sendNote(user.getUsername(),mailcentent+"【北斗盒子】");
						}
					} 
				}
				
			}else{
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("头像上传失败");
			}
		}
		return objectResult;
	}
	
	/**
	 * 获取用户身份证号码
	 * @param 用户id 
 	 * @return
	 */
	@RequestMapping(value="getIdentification.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult getIdentification(Long id, HttpServletRequest request){
		ObjectResult objectResult = new ObjectResult(); 
		User user = userService.getUser(Long.valueOf(id));
		if(user!=null){
			objectResult.setMessage(user.getIdentification());
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("");
		}
		return objectResult;
	}
	
	
	
	/**
	 * 实名制列表
	 * @param 用户id 
 	 * @return
	 */
	@RequestMapping(value="listRealNametable.do")
	public @ResponseBody ListParam listRealNametable(
			@RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
			@RequestParam int iColumns, @RequestParam String sEcho,
			@RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0){
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		List<RealNameDto> dtos = new ArrayList<RealNameDto>();
		List<User> listuser=userService.getRealUser(page,pageSize); 
		int count=userService.getRealUserCount();
  		for(User user1:listuser){
			RealNameDto realNameDto=new RealNameDto();
			realNameDto.setId(user1.getId());
			realNameDto.setName(user1.getName());
			realNameDto.setIdcard(user1.getIdentification());
			realNameDto.setPhone(user1.getMobilePhone());
			if(user1.getReason()!=null&&!user1.getReason().equals("check")){
				realNameDto.setReason(user1.getReason());
			}else{
				realNameDto.setReason(null);
			}
			if(user1.getIsAutonym()!=null&&user1.getIdentification()!=null&&user1.getIsAutonym().equals(true)){
				realNameDto.setHandle("1");
				realNameDto.setCheck("已通过");
			}else{
				if(user1.getReason()!=null&&!user1.getReason().equals("check")){
					realNameDto.setHandle("1"); 
					realNameDto.setCheck("审核不通过 "); 
				}else{
					realNameDto.setHandle("0");
					realNameDto.setCheck("审核中 "); 
				}
			}
			UserPhotoUrl  userPhotoUrl=userPhotoUrlService.getRealNameUrl(user1.getId());
			if(userPhotoUrl!=null){
 				String headurl=userPhotoUrl.getHeadPhotoUrl();
 				 String Facaurl=userPhotoUrl.getFacadePhotoUrl();
 				 String Reveurl=userPhotoUrl.getRevePhotoUrl(); 
				//String headurl="http://localhost:8080/bdhezi/upload/4418821989102966380.png";
 				realNameDto.setHeadphotourl("<img src="+headurl+" width="+100+" height="+100+">");
				realNameDto.setFanphotourl("<img src="+Reveurl+" width="+100+" height="+100+">");
				realNameDto.setZhengphotourl("<img src="+Facaurl+" width="+100+" height="+100+">");
				if(headurl!=null || Facaurl!=null || Reveurl!=null){
					dtos.add(realNameDto);
				}
			} 
 		} 
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(dtos);
		return listParam;
	}

	
	/**
	 * 审核用户实名制通过
	 * @param 用户id 
 	 * @return
	 */
	@RequestMapping(value="pass.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult pass(Long id, HttpServletRequest request){
		ObjectResult objectResult = new ObjectResult(); 
		User user = userService.getUser(Long.valueOf(id));
		if(user!=null){
  			user.setIsAutonym(true);
  			user.setReason(null);
			boolean result=userService.updateRealName(user);
			if(result){
				objectResult.setMessage("操作成！");
				objectResult.setStatus(ResultStatus.OK);
				
				//以下是实名认证审核通过后给用户发送短信通知
	 			StringBuffer content=new StringBuffer(); 
				String type="SHIMINGRENZHENGPASS";  //通知类型  
				String type_twl="customer"; 
				String mailcentent=null;       //内容
				Notification notification=notificationService.getnotification(type,type_twl);
				if(notification!=null){ 
					if(notification.getIsnote().equals(true)){   
							content.append(notification.getNotecenter());
							if(content!=null){
								mailcentent=content.toString(); 
							}
						//mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
						mailNotification.sendNote(user.getUsername(),mailcentent);
						//保存发送的短信记录
						MessageRecordService.save(user.getUsername(), 
								mailcentent, 
								MailType.switchStatus(type));
					}
				}
				
			}else{
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("操作失败！");
			}
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("操作失败");
		}
		return objectResult;
	}
	
	
	 
	/**
	 * 查看用户实名制详情
	 * @param 用户id 
 	 * @return
	 */
	@RequestMapping(value="table_detail")
	@ResponseBody
	public ModelAndView detailed(Long id, HttpServletRequest request){
 		 ModelAndView mav = new ModelAndView();  
		// ModelAndView modelAndView=new ModelAndView(); 
		User user = userService.getUser(Long.valueOf(id));
		if(user!=null){
			UserPhotoUrl  userPhotoUrl=userPhotoUrlService.getRealNameUrl(user.getId());
            if(userPhotoUrl!=null){
            	mav.addObject("RealName", user.getName());
            	mav.addObject("phone", user.getMobilePhone());
            	mav.addObject("idcard", user.getIdentification());
            	mav.addObject("headurl", userPhotoUrl.getHeadPhotoUrl());
            	mav.addObject("zhengurl", userPhotoUrl.getFacadePhotoUrl());
            	mav.addObject("fanurl", userPhotoUrl.getRevePhotoUrl());
            	mav.addObject("id", userPhotoUrl.getUserId()); 
            	if(user.getIsAutonym().equals(true)){
                	mav.addObject("Autonymstate","1"); 
            	}
            	mav.setViewName("table_detail"); 
             }
		} 
		return mav;
		 
	}
	
	
	/**
	 *  
	 * @param 用户id 
 	 * @return
	 */
	@RequestMapping(value="getIdentification2.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult getIdentification2(Long id, HttpServletRequest request){
		ObjectResult objectResult = new ObjectResult(); 
		User user = null;
		if(id!=null){

			user = userService.getUser(Long.valueOf(id));
		}
		if(user!=null){
			if(user.getIsAutonym()!=null&&user.getIsAutonym().equals(true)){
				objectResult.setMessage("1");
				objectResult.setStatus(ResultStatus.OK);
			}else{
				objectResult.setMessage("2");
				objectResult.setStatus(ResultStatus.FAILED);
			} 
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("2");
		}
		return objectResult;
	}
	
	
	/**
	 * 审核用户实名制不通过
	 * @param 用户id 
 	 * @return
	 */
	@RequestMapping(value="notpass.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult pass(Long userid,String reason, HttpServletRequest request){
		ObjectResult objectResult = new ObjectResult(); 
		User user = userService.getUser(Long.valueOf(userid));
		if(user!=null){
  			user.setIsAutonym(false);
  			user.setReason(reason);
			boolean result=userService.updateRealName(user);
			if(result){
				objectResult.setMessage("操作成！");
				objectResult.setStatus(ResultStatus.OK); 
				
				//以下是实名认证审核不通过后给用户发送短信通知
	 			StringBuffer content=new StringBuffer(); 
				String type="SHIMINGRENZHENG";  //通知类型  
				String type_twl="customer"; 
				String mailcentent=null;       //内容
				Notification notification=notificationService.getnotification(type,type_twl);
				if(notification!=null){ 
					if(notification.getIsnote().equals(true)){   
							content.append(notification.getNotecenter());
							if(content!=null){
								mailcentent=content.toString(); 
							}
						//mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
						mailNotification.sendNote(user.getUsername(),mailcentent);
						
						//保存发送的短信记录
						MessageRecordService.save(user.getUsername(), mailcentent, 
								MailType.switchStatus(type));
					}
				}
				
			}else{
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("操作失败！");
			}
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("操作失败");
		}
		return objectResult;
	}
	
	/**
	 * 库存不足用户登记信息
	 * @param realName	 姓名
	 * @param mailbox	邮箱
	 * @param telephone	手机号 
	 * @return
	 */ 
	@RequestMapping(value="InformationRegister.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult InformationRegister(String realName,
			 String mailbox , String mobilePhone, String id,String type){
		ObjectResult objectResult = new ObjectResult();  
		boolean result=false;
		Calendar birthDate= Calendar.getInstance();
		 RegisterInformation registerInformation = registerInformationService.getRealNameUrl(Long.valueOf(id), type );
		if(registerInformation!=null){
			registerInformation.setName(realName);
			registerInformation.setMail(mailbox);
			registerInformation.setTelephone(mobilePhone);
			registerInformation.setUserid(id); 
			registerInformation.setBirthDate(birthDate);
			registerInformation.setType(type);
			registerInformation.setRecord(false);
 	 		result=registerInformationService.updates(registerInformation);
		}else{ 
			RegisterInformation enty=new RegisterInformation();
			enty.setName(realName);
			enty.setMail(mailbox);
			enty.setTelephone(mobilePhone);
			enty.setUserid(id); 
			enty.setBirthDate(birthDate); 
			enty.setType(type); 
			enty.setRecord(false);
			RegisterInformation registerInformations=registerInformationService.saves(enty);
			if(registerInformations!=null){
				result=true;
			} 
		 }  
		if(result){
			objectResult.setMessage("资料提交成功");
			objectResult.setStatus(ResultStatus.OK);
 		}else{
 			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("资料提交失败");
		}
		return objectResult;
	}
	
	//查询用户信息，重新放到session里面
	@RequestMapping(value = "/queryuser.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult queryusersession(
			@RequestParam(required = true) String userid) {
		ObjectResult objectResult = new ObjectResult();
		try {
			UserDto userDto = userService.getUsersession(Long.valueOf(userid));
			objectResult.setResult(userDto);
			if (userDto != null) {
				objectResult.setMessage("查询成功");
				objectResult.setStatus(ResultStatus.OK);
			} else {
				objectResult.setMessage("查询失败");
				objectResult.setStatus(ResultStatus.FAILED);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}
	
	
	/**
	 * 查询登记信息(库存不足时登记的信息)
	 * @param 用户id 
 	 * @return
	 */
	@RequestMapping(value="listUserRegistrationtable.do")
	public @ResponseBody ListParam listUserRegistrationtable(
			@RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
			@RequestParam int iColumns, @RequestParam String sEcho,
			@RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0){
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		List<RegisterInformationDto> dtos = new ArrayList<RegisterInformationDto>();
		List<RegisterInformation> listuser=registerInformationService.getRegisterInformation(page,pageSize); 
		int count=registerInformationService.getRegisterInformationCount();
  		for(RegisterInformation user1:listuser){
  			 
  			RegisterInformationDto registerInformationDto=new RegisterInformationDto();
  			registerInformationDto.setId(String.valueOf(user1.getId()));
  			registerInformationDto.setName(user1.getName());
  			registerInformationDto.setTelephone(user1.getTelephone());
  			registerInformationDto.setMail(user1.getMail());
  			registerInformationDto.setUserid(user1.getUserid());
  			if(user1.getType().equals("RENT")){
  	  			registerInformationDto.setType("租用登记");  
  			}else{
  	  			registerInformationDto.setType("购买登记"); 
  			}
  			registerInformationDto.setBirthDate(CommonMethod.CalendarToString(user1.getBirthDate(), "yyyy/MM/dd HH:mm:ss"));
  			if(user1.getRecord().equals(true)){
  				registerInformationDto.setRecord("已通知客户");
  			}else{
  				registerInformationDto.setRecord("通知客户");
  			}
 			dtos.add(registerInformationDto);
 		} 
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(dtos);
		return listParam;
	}
	
	
	
	/**
	 *给用户发送短信
	 * @param 用户id 
 	 * @return
	 */
	@RequestMapping(value="inform.do",method=RequestMethod.POST)
	public @ResponseBody ObjectResult inform(Long id,String telephone,String name,String number, HttpServletRequest request){
		ObjectResult objectResult = new ObjectResult(); 
		boolean result=false;
		result=registerInformationService.sendinfomaton(id,telephone,name,number); 
		if(result){
			objectResult.setMessage("操作成！");
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("操作失败！");
		}
	 
		return objectResult;
	}
	
	/**
	 * 获取所有用户
	 */
	@RequestMapping(value="getAllUser.do")
	public @ResponseBody ObjectResult getAllUser(){
		ObjectResult objectResult = new ObjectResult();
		List<UserDto> userDtos = userService.getAllUser();
		
		objectResult.setStatus(ResultStatus.OK);
		objectResult.setResult(userDtos);
		return objectResult;
	}
	
	/**
	 * 通过用户名获取北斗盒子用户名账号，用户家人、用户队友
	 * @param key
	 * @param account
	 * @param pwd
	 * @return
	 */
	@RequestMapping(value="{getUserInfo.do}")
	public @ResponseBody ObjectResult getUserInfoByAccount(String key,String account,String pwd){
		return userService.getUserInfoByAccount(key, account, pwd);
	}
	
	/**
	 * 获取驴讯通绑定的盒子官网帐号
	 * @param lxtAccount
	 * @return
	 */
	@RequestMapping(value = "getUserForLxtAccount.do")
	@ResponseBody
	public ObjectResult getUserForLxtAccount(String lxtAccount){
		return userService.getUserForLxtAccount(lxtAccount);
	}
	
	/**
	 * 绑定盒子官网帐号
	 * @param account
	 * @param password
	 * @param lxtAccount
	 * @return
	 */
	@RequestMapping(value = "bindLxtAccount.do")
	@ResponseBody
	public ObjectResult bindLxtAccount(String account, String password, String lxtAccount){
		return userService.dobindLxtAccount(account, password, lxtAccount);
	}
	
	/**
	 * 解除绑定
	 * @param lxtAccount
	 * @return
	 */
	@RequestMapping(value = "unbindLxtAccount.do")
	@ResponseBody
	public ObjectResult unbindLxtAccount(String lxtAccount){
		return userService.dounbindLxtAccount(lxtAccount);
	}
}

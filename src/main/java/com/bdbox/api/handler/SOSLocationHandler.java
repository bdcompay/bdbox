package com.bdbox.api.handler;

import java.util.Calendar;
import java.util.List;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.api.dto.SOSLocationDto;
import com.bdbox.entity.SOSLocation;
import com.bdbox.service.BoxService;
import com.bdbox.service.SOSLocationService;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.util.CommonMethod;

@Controller
public class SOSLocationHandler {

	@Autowired
	private SOSLocationService sosLocationService;
	
	@Autowired
	private BoxService boxService;
	
	/**
	 * 保存
	 * @param sosLocation
	 */
	@RequestMapping(value = "saveSOSLocation")
	public void save(SOSLocation sosLocation){
		sosLocationService.save(sosLocation);
	}
	
	/**
	 * 获取单条信息
	 * @param boxId
	 * @return
	 */
	@RequestMapping(value = "getSOSLocation")
	@ResponseBody
	public ObjectResult getLocation(String cardNum){
		return sosLocationService.getLocationForBoxId(cardNum);
	}
	
	/**
	 * 获取全部信息
	 * @return
	 */
	@RequestMapping(value = "getLocationList")
	@ResponseBody
	public ObjectResult getList(){
		ObjectResult result = new ObjectResult();
		List<SOSLocationDto> list = sosLocationService.getList();
		result.setResult(list);
		return result;
	}
	
	/**
	 * 查询信息
	 * @return
	 */
	@RequestMapping(value = "searchLocationList")
	@ResponseBody
	public ObjectResult getList(String card){
		ObjectResult result = new ObjectResult();
		List<SOSLocationDto> list = sosLocationService.getList(card);
		result.setResult(list);
		return result;
	}
	
	/**
	 * 获取企业用户所有下属盒子报警信息
	 * @param ids
	 * @return
	 */
	@RequestMapping(value = "getEntUserSosLocation.do")
	@ResponseBody
	public ObjectResult getSosLocation(String ids){
		JSONObject json = JSONObject.fromObject(ids);
		return sosLocationService.getSosLocation(json);
	}
	
	/**
	 * 分页获取sos位置信息
	 * @param ids
	 * @param startTime
	 * @param endTime
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "getSOSLocations.do")
	@ResponseBody
	public ObjectResult getSosLocations(String ids, String startTime, String endTime){
		try {
			JSONObject json = JSONObject.fromObject(ids);
			Calendar sTime = null;
			Calendar eTime = null;
			if(startTime!="" && startTime!=null){
				sTime = CommonMethod.StringToCalendar(startTime, "yyyy-MM-dd HH:mm:ss");
			}
			if(endTime!="" && endTime!=null){
				eTime = CommonMethod.StringToCalendar(endTime, "yyyy-MM-dd HH:mm:ss");
			}
			return sosLocationService.getSosLocations(json, sTime, eTime);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}

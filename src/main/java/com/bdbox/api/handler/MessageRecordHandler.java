package com.bdbox.api.handler;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.api.dto.MessageRecordDto;
import com.bdbox.service.MessageRecordService;
import com.bdsdk.json.ListParam;
import com.bdsdk.util.CommonMethod;

@Controller
public class MessageRecordHandler {

	@Autowired
	private MessageRecordService messageRecordService;
	
	@RequestMapping(value = "getMsgAll")
	@ResponseBody
	public ListParam getMsgAll(String mob, String content, 
			String startTime, String endTime, @RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength, @RequestParam int iColumns,
			@RequestParam String sEcho, @RequestParam Integer iSortCol_0,
			@RequestParam String sSortDir_0){
		
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		
		Calendar sTime = null;
		Calendar eTime = null;
		ListParam listParam = new ListParam();
		try {
			if(!"".equals(startTime)){
				sTime = CommonMethod.StringToCalendar(startTime, "yyyy-MM-dd HH:mm:ss");
			}
			if(!"".equals(endTime)){
				eTime = CommonMethod.StringToCalendar(endTime, "yyyy-MM-dd HH:mm:ss");
			}
			List<MessageRecordDto> list = messageRecordService.getMsgAll(mob, content, sTime, eTime, page, pageSize);
			int count = messageRecordService.count(mob, content, sTime, eTime);
			
			
			listParam.setiDisplayLength(Integer.valueOf(iDisplayLength));
			listParam.setiDisplayStart(Integer.valueOf(iDisplayStart));
			listParam.setiTotalDisplayRecords(Integer.valueOf(count));
			listParam.setiTotalRecords(Integer.valueOf(count));
			listParam.setsEcho(sEcho);
			listParam.setAaData(list);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listParam;
	}
}

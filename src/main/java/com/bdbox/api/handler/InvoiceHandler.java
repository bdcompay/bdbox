package com.bdbox.api.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.entity.Invoice;
import com.bdbox.entity.User;
import com.bdbox.service.InvoiceService;
import com.bdbox.service.UserService;
import com.bdbox.web.dto.InvoiceDto;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;

@Controller
public class InvoiceHandler {

	@Autowired
	private InvoiceService invoiceService;
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="saveInvoice.do")
	@ResponseBody
	public ObjectResult saveInvoice(Long id, String companyName, String taxpayerIdNum, String registerAddr,
			String registerPhone, String bankName, String bankAccount, String ticketsInfo, String ticketsPhone,
			String ticketsAddr, String detailAddr, String invoiceType, Long user){
		return invoiceService.saveInvoice(id, companyName, taxpayerIdNum, registerAddr, 
				registerPhone, bankName, bankAccount, ticketsInfo, ticketsPhone, 
				ticketsAddr, detailAddr, invoiceType, user);
	}
	
	@RequestMapping(value="updateInvoice.do")
	@ResponseBody
	public ObjectResult updateInvoice(Long id, String companyName, String taxpayerIdNum, String registerAddr,
			String registerPhone, String bankName, String bankAccount, String ticketsInfo, String ticketsPhone,
			String ticketsAddr, String detailAddr){
		ObjectResult result = new ObjectResult();
		Invoice invoice = invoiceService.getInvoice(id);
		try{
			invoice.setCompanyName(companyName);
			invoice.setTaxpayerIdNum(taxpayerIdNum);
			invoice.setRegisterAddr(registerAddr);
			invoice.setRegisterPhone(registerPhone);
			invoice.setBankName(bankName);
			invoice.setBankAccount(bankAccount);
			invoice.setTicketsInfo(ticketsInfo);
			invoice.setTicketsPhone(ticketsPhone);
			invoice.setTicketsAddr(ticketsAddr);
			invoice.setDetailAddr(detailAddr);
			invoiceService.updateInvoice(invoice);
			result.setMessage("更新成功");
			result.setStatus(ResultStatus.OK);
		} catch(Exception e){
			e.printStackTrace();
			result.setMessage("更新失败");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}
	
	@RequestMapping(value="deleteInvoice.do")
	@ResponseBody
	public ObjectResult deleteInvoice(Long id){
		return invoiceService.deleteInvoice(id);
	}
	
	@RequestMapping(value="getAllInvoiceFormUser.do")
	@ResponseBody
	public ObjectResult getAllInvoiceFormUser(Long user){
		ObjectResult result = new ObjectResult();
		try {
			List<InvoiceDto> dtos = invoiceService.getAllInvoice(user);
			if(dtos!=null){
				result.setResult(dtos);
				result.setMessage("获取所有发票信息成功");
				result.setStatus(ResultStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("获取所有发票信息失败");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}
	
	@RequestMapping(value="getInvoiceForId.do")
	@ResponseBody
	public ObjectResult getInvoiceForId(Long id){
		ObjectResult result = new ObjectResult();
		try {
			InvoiceDto dto = invoiceService.getInvoiceDto(id);
			if(dto!=null){
				result.setResult(dto);
				result.setMessage("获取发票信息成功");
				result.setStatus(ResultStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("获取发票信息失败");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}
	
	@RequestMapping(value="getCheckStatus.do")
	@ResponseBody
	public ObjectResult getCheckStatus(Long id){
		ObjectResult result = new ObjectResult();
		InvoiceDto dto = invoiceService.getCheckStatus(id);
		if(dto!=null){
			result.setMessage("查找数据成功");
			result.setResult(dto);
			result.setStatus(ResultStatus.OK);
		}else{
			result.setMessage("没有此数据");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}
	
	@RequestMapping(value="setmorenInvoice.do")
	public @ResponseBody ObjectResult updateUserDefualtAddr(long userId,long id){
		ObjectResult obj = new ObjectResult();
		Invoice dtos = invoiceService.getAllInvoices(userId);
		if(dtos!=null){
		  dtos.setMoren(false);
			boolean result=invoiceService.updatemoren(dtos);
		} 
		Invoice invoice = invoiceService.getInvoice(id);
		invoice.setMoren(true);
		boolean result=invoiceService.updatemoren(invoice);
		if(!result){
			obj.setStatus(ResultStatus.FAILED);
			obj.setMessage("修改失败");
		}else{
			obj.setStatus(ResultStatus.OK);
			obj.setMessage("修改成功");
		}
		return obj;
	}
	
	@RequestMapping(value = "getUserInvoice.do")
	@ResponseBody
	public ObjectResult getUserInvoice(String phone){
		ObjectResult result = new ObjectResult();
		try {
			User user = userService.getUserByusername(phone);
			if(user!=null){
				List<InvoiceDto> dtos = invoiceService.getAllInvoice(user.getId());
				if(dtos!=null){
					result.setResult(dtos);
					result.setMessage("获取所有发票信息成功");
					result.setStatus(ResultStatus.OK);
				}
			}else{
				result.setResult(null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("获取所有发票信息失败");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}
}
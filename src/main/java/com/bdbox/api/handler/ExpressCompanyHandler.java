package com.bdbox.api.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.entity.ExpressCompany;
import com.bdbox.service.ExpressCompanyService;

@Controller
public class ExpressCompanyHandler {

	@Autowired
	private ExpressCompanyService expressCompanyService;
	
	@RequestMapping(value="getExpressCompanyAll")
	@ResponseBody
	public List<ExpressCompany> getAll(){
		return expressCompanyService.getAll();
	}
}

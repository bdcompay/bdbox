package com.bdbox.api.handler;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.springframework.beans.factory.annotation.Value;

/**
 * 用户访问驴讯通后台
 * @author hax
 *
 */
public class HttpLXTHandler {

	/**
	 * 接口Url
	 */
	@Value("${interface.lxtUrl}")
	protected String interfaceUrl;
	

	/**
	 * 生成POST请求
	 * @param requestMapping	请求方法
	 * @return
	 */
	protected PostMethod postMethod(String requestMapping){
		PostMethod postMethod = new PostMethod(interfaceUrl+requestMapping);
		postMethod.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"UTF-8");//设置编码
		return postMethod;
	}
	
	/**
	 * 执行Post请求
	 * @param postMethod
	 * @return
	 */
	protected String httpClient(PostMethod postMethod){
		try {
			HttpClient httpClient = new HttpClient();
			//执行请求
			httpClient.executeMethod(postMethod);
			//返回结果
			return	postMethod.getResponseBodyAsString();
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			postMethod.releaseConnection();//关闭请求
		}
		return null;
	}
}

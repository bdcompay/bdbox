package com.bdbox.api.handler;

import java.util.List;
import java.util.Observable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdbox.api.dto.BoxDto;
import com.bdbox.biz.ObserverBase;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxLocation;
import com.bdbox.service.BoxLocationService;
import com.bdbox.service.BoxService;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;

@Controller
@RequestMapping("/test")
public class TestApi extends Observable {
	
	@Autowired
	private BoxLocationService boxLocationService;
	@Autowired
	private BoxService boxService;
	
	@Autowired
	public TestApi(@Qualifier("observerDsi") ObserverBase observerDsi,
			@Qualifier("observerBizOthers") ObserverBase observerBizOthers) {
		addObserver(observerDsi);
		addObserver(observerBizOthers);
	}
	
	@RequestMapping("/location.do")
	@ResponseBody
	public ObjectResult getLoaction(String boxLocation){
		System.out.println("boxLocation:\n"+boxLocation);
		
		ObjectResult objectResult=new ObjectResult();
		objectResult.setResult(ResultStatus.OK);
		objectResult.setMessage("成功");
		return objectResult;
	}
	
	@RequestMapping("/message.do")
	@ResponseBody
	public ObjectResult getMessage(String boxMessage){
		System.out.println("boxMessage:\n"+boxMessage);
		
		ObjectResult objectResult=new ObjectResult();
		objectResult.setResult(ResultStatus.OK);
		objectResult.setMessage("成功");
		return objectResult;
	}
	
	@RequestMapping("/sendlocation.do")
	@ResponseBody
	public ObjectResult sendlocation(){
		
		ObjectResult objectResult=new ObjectResult();
		List<BoxDto> boxes=boxService.getUserBoxDtos(null, 1, 2000);
		if(boxes==null||boxes.size()==0){
			return objectResult;
		}
		for(int i=0;i<boxes.size();i++){
			BoxDto boxDto=boxes.get(i);
			Box box=boxService.getBox(boxDto.getId());
			BoxLocation b=new BoxLocation();
			b.setBox(box);
			b.setAltitude(box.getAltitude());
			b.setCreatedTime(box.getLastLocationTime());
			b.setPositioningTime(box.getLastLocationTime());
			b.setLatitude(box.getLatitude());
			b.setLongitude(box.getLongitude());
			sendNotify(b);
		}
		objectResult.setResult(ResultStatus.OK);
		objectResult.setMessage("成功");
		return objectResult;
	}
	
	private void sendNotify(Object object) {
		// 确保线程安全，此处如果不加锁，并发高时会导致消息丢失
		synchronized (this) {
			setChanged();
			notifyObservers(object);
		}
	}
}

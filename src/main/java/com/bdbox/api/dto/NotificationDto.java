package com.bdbox.api.dto;

 
  
 
public class NotificationDto {
	private String id; 
	/**
	 * 主题
	 */
 	private String subject;
	/**
	 * 内容
	 */
 	private String center;
	/**
	 * 通知类型
	 */
 	private String mailType;
	/**
	 * 通知方式
	 */
  	private String notificationType;
    /**
	 * 发件人
	 */
  	private String frommail;
  	/**
	 * 收件人
	 */
  	private String tomail;
    /**
	 * 发送手机号码
	 */
  	private String sendTeliPhone;
  	/**
	 * 接收手机号码
	 */
  	private String receiveTeliPhone;
  	/**
	 * 姓名
	 */
  	private String name;
	/**
	 * 是否发送邮件
	 */
  	private String isemail;
	/**
	 * 是否发送短信
	 */
  	private String isnote;
  	/**
	 * 短信内容
	 */
 	private String notecenter;
	/**
	 * 添加时间
	 */
 	private String createTime;
 	/**
	 * 操作
	 */
 	private String handdle;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCenter() {
		return center;
	}
	public void setCenter(String center) {
		this.center = center;
	}
	public String getMailType() {
		return mailType;
	}
	public void setMailType(String mailType) {
		this.mailType = mailType;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public String getFrommail() {
		return frommail;
	}
	public void setFrommail(String frommail) {
		this.frommail = frommail;
	}
	public String getTomail() {
		return tomail;
	}
	public void setTomail(String tomail) {
		this.tomail = tomail;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getHanddle() {
		return handdle;
	}
	public void setHanddle(String handdle) {
		this.handdle = handdle;
	}
	public String getSendTeliPhone() {
		return sendTeliPhone;
	}
	public void setSendTeliPhone(String sendTeliPhone) {
		this.sendTeliPhone = sendTeliPhone;
	}
	public String getReceiveTeliPhone() {
		return receiveTeliPhone;
	}
	public void setReceiveTeliPhone(String receiveTeliPhone) {
		this.receiveTeliPhone = receiveTeliPhone;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIsemail() {
		return isemail;
	}
	public void setIsemail(String isemail) {
		this.isemail = isemail;
	}
	public String getIsnote() {
		return isnote;
	}
	public void setIsnote(String isnote) {
		this.isnote = isnote;
	}
	public String getNotecenter() {
		return notecenter;
	}
	public void setNotecenter(String notecenter) {
		this.notecenter = notecenter;
	}
	
 	
}

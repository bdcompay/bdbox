package com.bdbox.api.dto;

import java.util.List;

public class ImageApiDto {
	private List<String> ImageUrls;//首页图链接

	public List<String> getImageUrls() {
		return ImageUrls;
	}

	public void setImageUrls(List<String> imageUrls) {
		ImageUrls = imageUrls;
	}
	
}

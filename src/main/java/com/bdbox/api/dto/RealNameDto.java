package com.bdbox.api.dto;

public class RealNameDto {
	
	private Long id;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 身份证号码
	 */
	private String idcard;
	/**
	 * 手机号码
	 */
	private String phone;
	/**
	 * 头像
	 */
	private String headphotourl;
	/**
	 * 身份证正面头像
	 */
	private String zhengphotourl;
	/**
	 * 身份证反面头像
	 */
	private String fanphotourl;
	/**
	 * 审核结果
	 */
	private String check ;
	/**
	 * 不通过原因
	 */
	private String reason ;
	/**
	 * 操作
	 */
	private String handle;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIdcard() {
		return idcard;
	}
	public void setIdcard(String idcard) {
		this.idcard = idcard;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getHeadphotourl() {
		return headphotourl;
	}
	public void setHeadphotourl(String headphotourl) {
		this.headphotourl = headphotourl;
	}
	public String getZhengphotourl() {
		return zhengphotourl;
	}
	public void setZhengphotourl(String zhengphotourl) {
		this.zhengphotourl = zhengphotourl;
	}
	public String getFanphotourl() {
		return fanphotourl;
	}
	public void setFanphotourl(String fanphotourl) {
		this.fanphotourl = fanphotourl;
	}
	public String getHandle() {
		return handle;
	}
	public void setHandle(String handle) {
		this.handle = handle;
	}
	public String getCheck() {
		return check;
	}
	public void setCheck(String check) {
		this.check = check;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
}

package com.bdbox.api.dto;

import java.util.Comparator;


/**
 * 消息数据表
 * 
 * @author will.yan
 */
public class BoxMessageDto implements Comparator<BoxMessageDto>{
	private Long id;
	/**
	 * 消息ID
	 */
	private Integer msgId;
	/**
	 * 消息类型
	 */
	private String msgType;

	/**
	 * 消息方向
	 */
	private String msgIoType;

	/**
	 * 数据状态类型
	 */
	private String dataStatusType;

	/**
	 * 状态描述
	 */
	private String statusDescription;
	/**
	 * 发送者盒子
	 */
	private String fromBox;

	/**
	 * 接受者盒子
	 */
	private String toBox;

	/**
	 * 家人手机号码
	 */
	private String familyMob;

	/**
	 * 家人邮箱
	 */
	private String familyMail;
	
	/**
	 * 短信内容
	 */
	private String content;

	/**
	 * 经度
	 */
	private Double longitude;

	/**
	 * 纬度
	 */
	private Double latitude;

	/**
	 * 高度
	 */
	private Double altitude;

	/**
	 * 速度
	 */
	private Double speed;

	/**
	 * 方向
	 */
	private Double direction;

	/**
	 * 创建时间
	 */
	private String createdTime;
	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	/**
	 * 编辑
	 */
	private String manager;

	public Double getAltitude() {
		return altitude;
	}

	public String getContent() {
		return content;
	}

	public String getCreatedTime() {
		return createdTime;
	}

	public String getDataStatusType() {
		return dataStatusType;
	}

	public Double getDirection() {
		return direction;
	}

	public String getFamilyMail() {
		return familyMail;
	}

	public String getFamilyMob() {
		return familyMob;
	}

	public String getFromBox() {
		return fromBox;
	}

	public Long getId() {
		return id;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public Integer getMsgId() {
		return msgId;
	}

	public String getMsgIoType() {
		return msgIoType;
	}

	public String getMsgType() {
		return msgType;
	}

	public Double getSpeed() {
		return speed;
	}

	public String getStatusDescription() {
		return statusDescription;
	}

	public String getToBox() {
		return toBox;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public void setDataStatusType(String dataStatusType) {
		this.dataStatusType = dataStatusType;
	}

	public void setDirection(Double direction) {
		this.direction = direction;
	}

	public void setFamilyMail(String familyMail) {
		this.familyMail = familyMail;
	}

	public void setFamilyMob(String familyMob) {
		this.familyMob = familyMob;
	}

	public void setFromBox(String fromBox) {
		this.fromBox = fromBox;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public void setMsgId(Integer msgId) {
		this.msgId = msgId;
	}

	public void setMsgIoType(String msgIoType) {
		this.msgIoType = msgIoType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	public void setToBox(String toBox) {
		this.toBox = toBox;
	}

	//排序
	@Override
	public int compare(BoxMessageDto o1, BoxMessageDto o2) {
		/*try {
			return CommonMethod.StringToCalendar(o2.getCreatedTime(), "yyyy-MM-dd HH:mm:ss")
					.compareTo(CommonMethod.StringToCalendar(o2.getCreatedTime(), "yyyy-MM-dd HH:mm:ss"));
		} catch (ParseException e) {
			e.printStackTrace();
			return 0;
		}*/
		return o2.getCreatedTime().compareTo(o1.getCreatedTime());
	}
}

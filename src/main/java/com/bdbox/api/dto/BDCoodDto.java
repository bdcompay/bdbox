package com.bdbox.api.dto;


public class BDCoodDto {


	 /**
	 * id
	 */ 
	private String id;
	 /**
	 * 北斗码
	 */ 
	private String bdcood;
	
	/**
	 * 优惠金额
	 */ 
	private String money;
	
	/**
	 * 推荐人
	 */ 
	private String referees;
	
	/**
	 * 推荐人
	 */
    private String username;
	
   /**
   * 有效期开始时间
   */
   private String startTime;
  
   /**
   * 有效期结束时间
   */
	private String endTime;
   
   /**
   * 生成时间
   */
	private String creatTime;
   
   /**
	 * 是否已经使用
	 */
   private String isUser;
	
   /**
	 * 订单号
	 */
   private String orderNo;
   
   /**
	 * 类别(租用和购买)
	 */
   private String type;
   

	 /**
	 * 推荐人备注名
	 */ 
	private String beizhuname;
	
	 /**
	 * 操作
	 */ 
	private String hendle;
	
	/**
	 * 抵用天数
	 */
	private int dayNum;
	
	/**
	 * 折扣
	 */
	private Double discount;
	
	/**
	 * 生成环境
	 */
	private String createCondition;
	
	/**
	 * 北斗码/券状态
	 */
	private String bdCoodStatus;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBdcood() {
		return bdcood;
	}

	public void setBdcood(String bdcood) {
		this.bdcood = bdcood;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getReferees() {
		return referees;
	}

	public void setReferees(String referees) {
		this.referees = referees;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	} 

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
   
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getCreatTime() {
		return creatTime;
	}
	
	public void setCreatTime(String creatTime) {
		this.creatTime = creatTime;
	}

	public String getIsUser() {
		return isUser;
	}

	public void setIsUser(String isUser) {
		this.isUser = isUser;
	}

	public String getBeizhuname() {
		return beizhuname;
	}

	public void setBeizhuname(String beizhuname) {
		this.beizhuname = beizhuname;
	}

	public String getHendle() {
		return hendle;
	}

	public void setHendle(String hendle) {
		this.hendle = hendle;
	}

	public int getDayNum() {
		return dayNum;
	}

	public void setDayNum(int dayNum) {
		this.dayNum = dayNum;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public String getCreateCondition() {
		return createCondition;
	}

	public void setCreateCondition(String createCondition) {
		this.createCondition = createCondition;
	}

	public String getBdCoodStatus() {
		return bdCoodStatus;
	}

	public void setBdCoodStatus(String bdCoodStatus) {
		this.bdCoodStatus = bdCoodStatus;
	}
}

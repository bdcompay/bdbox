package com.bdbox.api.dto;

import com.bdbox.constant.MsgType;

/**
 * SOS位置信息
 * @author hax
 *
 */
public class SOSLocationDto {

	private Long id;
	//盒子id
	private String boxSerialNumber;
	//经度
	private String longitude;
	//纬度
	private String latitude;
	//高程
	private String altitude;
	//方向
	private String speed;
	//方向
	private String direction;
	//位置信息来源
	private MsgType locationSource;
	//定位时间
	private String positioningTime;
	//创建时间
	private String createdTime;
	//更新时间
	private String updateTime;
	//是否紧急定位
	private String isSOSLocation;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBoxSerialNumber() {
		return boxSerialNumber;
	}
	public void setBoxSerialNumber(String boxSerialNumber) {
		this.boxSerialNumber = boxSerialNumber;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getAltitude() {
		return altitude;
	}
	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public MsgType getLocationSource() {
		return locationSource;
	}
	public void setLocationSource(MsgType locationSource) {
		this.locationSource = locationSource;
	}
	public String getPositioningTime() {
		return positioningTime;
	}
	public void setPositioningTime(String positioningTime) {
		this.positioningTime = positioningTime;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getIsSOSLocation() {
		return isSOSLocation;
	}
	public void setIsSOSLocation(String isSOSLocation) {
		this.isSOSLocation = isSOSLocation;
	}
}

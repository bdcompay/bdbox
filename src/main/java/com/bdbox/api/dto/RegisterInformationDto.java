package com.bdbox.api.dto;

import java.util.Calendar;

public class RegisterInformationDto {

	private String id;
	/**
	 * 手机号码
	 */
	private String telephone;

	/**
	 * 邮箱
	 */
	private String mail; 
	 
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 登记时间
	 */
	private String birthDate;
	
	/**
	 * 用户id
	 */
	private String userid;
	
	/**
	 * 登记类型（购买和租用）
	 */
	private String type;
	
	/**
	 * 联系客户记录
	 */
	private String record;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}  

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getRecord() {
		return record;
	}

	public void setRecord(String record) {
		this.record = record;
	}
	
	
	
}

package com.bdbox.api.dto;


import java.util.ArrayList;
import java.util.List;

public class ParamBoxLocationDataDto {
	private List<BoxLocationDto> boxLocationDtos = new ArrayList<BoxLocationDto>();
	private String boxSerialNumber;
	private String cardNumber;
	
	
	public List<BoxLocationDto> getBoxLocationDtos() {
		return boxLocationDtos;
	}
	public void setBoxLocationDtos(List<BoxLocationDto> boxLocationDtos) {
		this.boxLocationDtos = boxLocationDtos;
	}
	public String getBoxSerialNumber() {
		return boxSerialNumber;
	}
	public void setBoxSerialNumber(String boxSerialNumber) {
		this.boxSerialNumber = boxSerialNumber;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
}

package com.bdbox.api.dto;

/**
 * APP的api，如参数获取，检查更新等
 * 
 * @author will.yan
 * 
 */
public class QueryParamAppApi {
	private String appType;

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}
	
}
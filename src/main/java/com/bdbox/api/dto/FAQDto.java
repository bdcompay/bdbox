package com.bdbox.api.dto; 
 
 
public class FAQDto {
	 
	private long id;
	
	/**
	 * 问题
	 */
	private String question;
	
	/**
	 * 答案
	 */
	private String answer;
	
	/**
	 * 创建时间
	 */
	private String creatTime;
	
	/**
	 * 问题类型
	 */
	  
	private String questionType;
	
	/**
	 * 问题小类
	 */
	  
	private String smallTypeid;
	
	/**
	 * 问题小类名
	 */
	  
	private String smallTypename;
	
	/**
	 * 操作
	 */
	  
	private String handle;
	
	/**
	 * 复选框
	 */
	  
	private String checbox;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(String creatTime) {
		this.creatTime = creatTime;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public String getSmallTypeid() {
		return smallTypeid;
	}

	public void setSmallTypeid(String smallTypeid) {
		this.smallTypeid = smallTypeid;
	}

	public String getSmallTypename() {
		return smallTypename;
	}

	public void setSmallTypename(String smallTypename) {
		this.smallTypename = smallTypename;
	}

	public String getChecbox() {
		return checbox;
	}

	public void setChecbox(String checbox) {
		this.checbox = checbox;
	}

	 
	
	
	
}

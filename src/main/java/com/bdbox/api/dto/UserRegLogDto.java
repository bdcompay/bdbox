package com.bdbox.api.dto;

/**
 * 用户注册记录DTO
 * 
 * @ClassName: UserRegLogDto 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-2-2 下午3:12:23 
 * @version V5.0 
 */
public class UserRegLogDto {
	/**
	 * id
	 */

	private long id;
	/**
	 * 用户名称
	 */
	private String username;
	/**
	 * ip
	 */
	private String ip;
	/**
	 * ip所属地区
	 */
	private String ipArea;
	/**
	 * 创建用户
	 */
	private long userid;
	/**
	 * 创建时间
	 */
	private String createdTimeStr;
	
	
	
	/***********************************  分割线     ***********************************/
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getIpArea() {
		return ipArea;
	}
	public void setIpArea(String ipArea) {
		this.ipArea = ipArea;
	}
	public long getUserid() {
		return userid;
	}
	public void setUserid(long userid) {
		this.userid = userid;
	}
	public String getCreatedTimeStr() {
		return createdTimeStr;
	}
	public void setCreatedTimeStr(String createdTimeStr) {
		this.createdTimeStr = createdTimeStr;
	}
	@Override
	public String toString() {
		return "UserRegLogDto [id=" + id + ", username=" + username + ", ip="
				+ ip + ", ipArea=" + ipArea + ", userid=" + userid
				+ ", createdTimeStr=" + createdTimeStr + "]";
	}
}

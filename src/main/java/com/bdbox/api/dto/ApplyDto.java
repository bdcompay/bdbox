package com.bdbox.api.dto;



/**
 * OrderDto
 * @author WF
 *
 */
public class ApplyDto {
	private long id;
	/**
	 * 用户
	 */
	private String user;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 手机号码
	 */
	private String phone;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 是否参与试用：1=参加，0=不参加
	 */
	private String join;
	/**
	 * 性别：1=男，0=女
	 */
	private String sex;
	/**
	 * 省份
	 */
	private String province;
	/**
	 * 市区
	 */
	private String city;
	/**
	 * 户外代号
	 */
	private String code;
	/**
	 * 简介
	 */
	private String intro;
	/**
	 * 其它说明
	 */
	private String explain;
	/**
	 * 申请时间
	 */
	private String createTime;
	/**
	 * 编辑
	 */
	private String manager;
	public String getCity() {
		return city;
	}
	public String getCode() {
		return code;
	}
	public String getCreateTime() {
		return createTime;
	}
	public String getEmail() {
		return email;
	}
	public String getExplain() {
		return explain;
	}
	public long getId() {
		return id;
	}
	public String getIntro() {
		return intro;
	}
	public String getJoin() {
		return join;
	}
	public String getManager() {
		return manager;
	}
	public String getName() {
		return name;
	}
	public String getPhone() {
		return phone;
	}
	public String getProvince() {
		return province;
	}
	public String getSex() {
		return sex;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public void setExplain(String explain) {
		this.explain = explain;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public void setJoin(String join) {
		this.join = join;
	}
	public void setManager(String manager) {
		this.manager = manager;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
}

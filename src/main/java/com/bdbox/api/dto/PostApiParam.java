package com.bdbox.api.dto;

public class PostApiParam {
	private Long msgId;
	private String toBoxSerialNumber;
	private String toCardNumber;
	private String content;
	private String userPowerKey;
	private Boolean isReceived;
	private Boolean isRead;
	
	public String getToBoxSerialNumber() {
		return toBoxSerialNumber;
	}

	public void setToBoxSerialNumber(String toBoxSerialNumber) {
		this.toBoxSerialNumber = toBoxSerialNumber;
	}

	public String getToCardNumber() {
		return toCardNumber;
	}

	public void setToCardNumber(String toCardNumber) {
		this.toCardNumber = toCardNumber;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserPowerKey() {
		return userPowerKey;
	}

	public void setUserPowerKey(String userPowerKey) {
		this.userPowerKey = userPowerKey;
	}

	public Long getMsgId() {
		return msgId;
	}

	public void setMsgId(Long msgId) {
		this.msgId = msgId;
	}

	public Boolean getIsReceived() {
		return isReceived;
	}

	public void setIsReceived(Boolean isReceived) {
		this.isReceived = isReceived;
	}

	public Boolean getIsRead() {
		return isRead;
	}

	public void setIsRead(Boolean isRead) {
		this.isRead = isRead;
	}
	
}
package com.bdbox.api.dto;

import java.util.List;

/**
 * 用户信息DTO，包括用户好友、用户家人信息
 * @author jlj
 *
 */
public class UserInfoDto {
	private UserDto userDto;
	private List<FamilyDto> familyDtos;
	private List<PartnerDto> partnerDtos;
	
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
	public List<FamilyDto> getFamilyDtos() {
		return familyDtos;
	}
	public void setFamilyDtos(List<FamilyDto> familyDtos) {
		this.familyDtos = familyDtos;
	}
	public List<PartnerDto> getPartnerDtos() {
		return partnerDtos;
	}
	public void setPartnerDtos(List<PartnerDto> partnerDtos) {
		this.partnerDtos = partnerDtos;
	}
}

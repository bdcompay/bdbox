package com.bdbox.api.dto;

 
public class UserQuestionDto {

   private long id; 
	/**
	 * 我的问题
	 */
	private String myquestion;
	
	/**
	 * 用户id
	 */
	private String userid;
	
	/**
	 * 提交时间
	 */
	private String submitTime; 
	
	/**
	 * 是否已解决
	 */
	private String issolve;
	
	/**
	 * 备注
	 */
	private String remask;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMyquestion() {
		return myquestion;
	}

	public void setMyquestion(String myquestion) {
		this.myquestion = myquestion;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(String submitTime) {
		this.submitTime = submitTime;
	}

	public String getIssolve() {
		return issolve;
	}

	public void setIssolve(String issolve) {
		this.issolve = issolve;
	}

	public String getRemask() {
		return remask;
	}

	public void setRemask(String remask) {
		this.remask = remask;
	}
	
	
}

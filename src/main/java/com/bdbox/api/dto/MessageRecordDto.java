package com.bdbox.api.dto;

public class MessageRecordDto {

	private Long id;
	private String msgContent;
	private String toMob;
	private String mailType;
	private String createdTime;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMsgContent() {
		return msgContent;
	}
	public void setMsgContent(String msgContent) {
		this.msgContent = msgContent;
	}
	public String getToMob() {
		return toMob;
	}
	public void setToMob(String toMob) {
		this.toMob = toMob;
	}
	public String getMailType() {
		return mailType;
	}
	public void setMailType(String mailType) {
		this.mailType = mailType;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
}

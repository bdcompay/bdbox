package com.bdbox.api.dto;

public class RefereesUserDto {
	    /**
		 * id
		 */ 
		private String id;
		 /**
		 * 推荐人
		 */ 
		private String refereesUser;
		
		/**
		 * 备注名
		 */ 
		private String beizhuname;
		
		/**
		 * 提成金额
		 */ 
		private String mony;
		
		/**
		 * 手持北斗码数量
		 */
	    private String nums;
		
	   /**
	   * 已使用北斗码数量
	   */
	   private String usernums;
	  
	   /**
	   * 未使用北斗码数量
	   */
		private String notusenums;
		
	  /**
	   * 未过期北斗码数量
	   */
		private String notdatednums;
	   
	   /**
	   * 已过期北斗码数量
	   */
		private String datednums;
		
		 /**
		   * 查看更多
		  */
		private String manager;
		
		

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRefereesUser() {
		return refereesUser;
	}

	public void setRefereesUser(String refereesUser) {
		this.refereesUser = refereesUser;
	}

	public String getBeizhuname() {
		return beizhuname;
	}

	public void setBeizhuname(String beizhuname) {
		this.beizhuname = beizhuname;
	}

	public String getMony() {
		return mony;
	}

	public void setMony(String mony) {
		this.mony = mony;
	}

	public String getNums() {
		return nums;
	}

	public void setNums(String nums) {
		this.nums = nums;
	}

	public String getUsernums() {
		return usernums;
	}

	public void setUsernums(String usernums) {
		this.usernums = usernums;
	}

	public String getNotusenums() {
		return notusenums;
	}

	public void setNotusenums(String notusenums) {
		this.notusenums = notusenums;
	}

	public String getNotdatednums() {
		return notdatednums;
	}

	public void setNotdatednums(String notdatednums) {
		this.notdatednums = notdatednums;
	}

	public String getDatednums() {
		return datednums;
	}

	public void setDatednums(String datednums) {
		this.datednums = datednums;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	} 
	   
	   
	   

	 
}

package com.bdbox.api.dto;


public class BoxblueCodeDto {
	/**
	 *盒子ID 
	 */
	private String boxSerialNumber;
	/**
	 * 卡号
	 */
	private String cardNumber;
	/**
	 * 蓝牙密码
	 */
	private String bluetoothCode;
	public String getBoxSerialNumber() {
		return boxSerialNumber;
	}
	public void setBoxSerialNumber(String boxSerialNumber) {
		this.boxSerialNumber = boxSerialNumber;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getBluetoothCode() {
		return bluetoothCode;
	}
	public void setBluetoothCode(String bluetoothCode) {
		this.bluetoothCode = bluetoothCode;
	}
	
}

package com.bdbox.api.dto;

/**
 * app参数配置
 * 
 * @author will.yan
 */

public class AppConfigDto {
	private String versionSize;
	/**
	 * 版本号
	 */
	private Float versionCode;
	/**
	 * 版本名称
	 */
	private String versionName;
	/**
	 * 更新说明
	 */
	private String versionDetail;
	/**
	 * 下载路径
	 */
	private String downloadUrl;
	/**
	 * 创建时间
	 */
	private String createdTime;

	public String getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

	public Float getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(Float versionCode) {
		this.versionCode = versionCode;
	}

	public String getVersionDetail() {
		return versionDetail;
	}

	public void setVersionDetail(String versionDetail) {
		this.versionDetail = versionDetail;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

	public String getVersionSize() {
		return versionSize;
	}

	public void setVersionSize(String versionSize) {
		this.versionSize = versionSize;
	}
}

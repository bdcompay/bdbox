package com.bdbox.api.dto;

 
 
public class AnnouncementDto {
	 
	private long id;
	
	/**
	 * 公告内容
	 */
	private String center;
	
	/**
	 * 公告超链接
	 */
	private String hyperlink;
	
	/**
	 * 创建时间
	 */
	private String creatTime;
	
	/**
	 * 操作
	 */
	private String handle;
	
	/**
	 * 备注
	 */
	private String remark;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCenter() {
		return center;
	}

	public void setCenter(String center) {
		this.center = center;
	}

	public String getHyperlink() {
		return hyperlink;
	}

	public void setHyperlink(String hyperlink) {
		this.hyperlink = hyperlink;
	}

	public String getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(String creatTime) {
		this.creatTime = creatTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}
	
	
}

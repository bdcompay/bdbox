package com.bdbox.api.dto;

public class MyCardLocStatusPOJO {

	//cardid
	private String cardid;
	//序列号
	private String boxSerialNumber;
	//定位类型
	private String type;
	//经度
	private String longitude;
	//纬度
	private String latitude;
	//海拔高度
	private String altitude;
	//速度
	private String speed;
	//前进方向
	private String direction;
	//创建时间
	private String createdtime;
	//北斗状态
	private String bdstatus;
	
	public String getCardid() {
		return cardid;
	}
	public void setCardid(String cardid) {
		this.cardid = cardid;
	}
	public String getBoxSerialNumber() {
		return boxSerialNumber;
	}
	public void setBoxSerialNumber(String boxSerialNumber) {
		this.boxSerialNumber = boxSerialNumber;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getAltitude() {
		return altitude;
	}
	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getCreatedtime() {
		return createdtime;
	}
	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}
	public String getBdstatus() {
		return bdstatus;
	}
	public void setBdstatus(String bdstatus) {
		this.bdstatus = bdstatus;
	}
}

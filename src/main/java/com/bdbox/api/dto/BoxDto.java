package com.bdbox.api.dto;


public class BoxDto {
	
	private Long id;
	/**
	 * 盒子用户
	 */
	private String user;
	/**
	 * 北斗卡号
	 */
	private String cardNumber;
	/**
	 * 频度
	 */
	private Integer freq;

	/**
	 * 盒子SN码
	 */
	private String snNumber;
	/**
	 * 北斗卡的类型
	 */
	private String cardTypeStr;

	/**
	 *  盒子状态类型，默认为未激活
	 */
	private String boxStatusTypeStr;

	/**
	 * 最后一次定位的时间
	 */
	private String lastLocationTimeStr;
	/**
	 * 盒子序列号
	 */
	private String boxSerialNumber;
	/**
	 * 盒子名称
	 */
	private String name;
	/**
	 * 真实姓名
	 */
	private String realName;
	/**
	 * 身份证号码
	 */
	private String idNumber;
	/**
	 * 账户余额
	 */
	private Double balance = (double) 0;

	/**
	 * 最新的经度
	 */
	private Double longitude;
	/**
	 * 最新的纬度
	 */
	private Double latitude;
	/**
	 * 最新的高度
	 */
	private Double altitude;

	/**
	 * 最新的速度
	 */
	private Double speed;

	/**
	 * 最新的方向
	 */
	private Double direction;

	/**
	 * 编辑
	 */
	private String manager;
	/**
	 * 蓝牙密码
	 */
	private String bluetoothCode;
	/**
	 * 是否可以SOS语音通知
	 */
	private boolean isOpenSosCall;
	/**
	 * 盒子类型
	 */
	private String boxType;
	/**
	 * 蓝牙硬件地址
	 */
	private String BlueMacAdress;
	
	public Double getAltitude() {
		return altitude;
	}
	public Double getBalance() {
		return balance;
	}
	public String getBoxSerialNumber() {
		return boxSerialNumber;
	}
	public String getBoxStatusTypeStr() {
		return boxStatusTypeStr;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public String getCardTypeStr() {
		return cardTypeStr;
	}
	public Double getDirection() {
		return direction;
	}
	public Integer getFreq() {
		return freq;
	}

	public Long getId() {
		return id;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public String getLastLocationTimeStr() {
		return lastLocationTimeStr;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public String getManager() {
		return manager;
	}

	public String getName() {
		return name;
	}

	public String getRealName() {
		return realName;
	}

	public String getSnNumber() {
		return snNumber;
	}

	public Double getSpeed() {
		return speed;
	}

	public String getUser() {
		return user;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public void setBoxSerialNumber(String boxSerialNumber) {
		this.boxSerialNumber = boxSerialNumber;
	}

	public void setBoxStatusTypeStr(String boxStatusTypeStr) {
		this.boxStatusTypeStr = boxStatusTypeStr;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public void setCardTypeStr(String cardTypeStr) {
		this.cardTypeStr = cardTypeStr;
	}

	public void setDirection(Double direction) {
		this.direction = direction;
	}

	public void setFreq(Integer freq) {
		this.freq = freq;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public void setLastLocationTimeStr(String lastLocationTimeStr) {
		this.lastLocationTimeStr = lastLocationTimeStr;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public void setSnNumber(String snNumber) {
		this.snNumber = snNumber;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public void setUser(String user) {
		this.user = user;
	}
	public String getBluetoothCode() {
		return bluetoothCode;
	}
	public void setBluetoothCode(String bluetoothCode) {
		this.bluetoothCode = bluetoothCode;
	}
	public boolean isOpenSosCall() {
		return isOpenSosCall;
	}
	public void setOpenSosCall(boolean isOpenSosCall) {
		this.isOpenSosCall = isOpenSosCall;
	}
	public String getBoxType() {
		return boxType;
	}
	public void setBoxType(String boxType) {
		this.boxType = boxType;
	}
	public String getBlueMacAdress() {
		return BlueMacAdress;
	}
	public void setBlueMacAdress(String blueMacAdress) {
		BlueMacAdress = blueMacAdress;
	}
	
	
	
}

package com.bdbox.api.dto;


/**
 * 用户表
 * 
 * @author will.yan
 * 
 */
public class UserDto {
	private long id;

	/**
	 * 手机号码
	 */
	private String username;

	/**
	 * 用户状态类型
	 */
	private String userStatusTypeStr;
	
	/**
	 * 邮箱
	 */
	private String mail;

	/**
	 * 真实姓名
	 */
	private String name;

	/**
	 * 出生日期
	 */
	private String birthDate;

	/**
	 * 性别
	 */
	private String sex;

	/**
	 * 居住地
	 */
	private String residence;

	/**
	 * 职业
	 */
	private String job;

	/**
	 * 爱好
	 */
	private String hobby;
	
	/**
	 * 是否实名制
	 */
	private String isAutonym;
	
	/**
	 * 实名制不通过的原因
	 */
	private String reason;
	
	/**
	 * 是否启用网络数据转发服务
	 */
	private String isDsiEnable;
	/**
	 * 发送频度
	 */
	private String freq;

	public String getBirthDate() {
		return birthDate;
	}

	public String getHobby() {
		return hobby;
	}

	public long getId() {
		return id;
	}

	public String getJob() {
		return job;
	}

	public String getMail() {
		return mail;
	}

	public String getName() {
		return name;
	}

	public String getResidence() {
		return residence;
	}
	
	/**
	 * @return 用户名，即手机号码
	 */
	public String getUsername() {
		return username;
	}
	public String getUserStatusTypeStr() {
		return userStatusTypeStr;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setJob(String job) {
		this.job = job;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setResidence(String residence) {
		this.residence = residence;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setUserStatusTypeStr(String userStatusTypeStr) {
		this.userStatusTypeStr = userStatusTypeStr;
	}

	public String getIsDsiEnable() {
		return isDsiEnable;
	}

	public void setIsDsiEnable(String isDsiEnable) {
		this.isDsiEnable = isDsiEnable;
	}

	public String getFreq() {
		return freq;
	}

	public void setFreq(String freq) {
		this.freq = freq;
	}

	public String getIsAutonym() {
		return isAutonym;
	}

	public void setIsAutonym(String isAutonym) {
		this.isAutonym = isAutonym;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	

}

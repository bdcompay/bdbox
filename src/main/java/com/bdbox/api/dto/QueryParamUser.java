package com.bdbox.api.dto;

/**
 * 用于注册，修改密码
 * 
 * @author will.yan
 * 
 */
public class QueryParamUser {
	private String username;
	private String password;
	private String cardNumber;
	private String mobKey;
	private String mobKeyType;
	private String suggestionContent;
	/**
	 * 真实姓名
	 */
	private String name;

	/**
	 * 身份证号码
	 */
	private String idNumber;
	
	private String systemCode;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobKey() {
		return mobKey;
	}

	public void setMobKey(String mobKey) {
		this.mobKey = mobKey;
	}

	public String getMobKeyType() {
		return mobKeyType;
	}

	public void setMobKeyType(String mobKeyType) {
		this.mobKeyType = mobKeyType;
	}

	public String getSuggestionContent() {
		return suggestionContent;
	}

	public void setSuggestionContent(String suggestionContent) {
		this.suggestionContent = suggestionContent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

}
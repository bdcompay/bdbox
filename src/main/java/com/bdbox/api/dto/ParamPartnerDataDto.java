package com.bdbox.api.dto;

import java.util.List;

public class ParamPartnerDataDto {

	private List<PartnerDto> partnerDtos;
	private String boxSerialNumber;
	private String cardNumber;

	public List<PartnerDto> getPartnerDtos() {
		return partnerDtos;
	}

	public void setPartnerDtos(List<PartnerDto> partnerDtos) {
		this.partnerDtos = partnerDtos;
	}

	public String getBoxSerialNumber() {
		return boxSerialNumber;
	}

	public void setBoxSerialNumber(String boxSerialNumber) {
		this.boxSerialNumber = boxSerialNumber;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

}
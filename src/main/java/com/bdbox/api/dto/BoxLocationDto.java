package com.bdbox.api.dto;


import com.bdbox.constant.MsgType;



public class BoxLocationDto {

	private long id;
	/**
	 * 盒子名称
	 */
	private String boxName;
	/**
	 * 盒子用户
	 */
	private String boxUser;
	/**
	 * 盒子ID
	 */
	private String boxSerialNumber;
	/**
	 * 盒子卡号
	 */
	private String cardNumber;
	/**
	 * 经度
	 */
	private String longitude;

	/**
	 * 纬度
	 */
	private String latitude;

	/**
	 * 高程
	 */
	private Double altitude;

	/**
	 * 速度
	 */
	private Double speed;

	/**
	 * 方向
	 */
	private Double direction;
	/**
	 * 位置信息来源
	 */
	private MsgType locationSource;
	/**
	 * 定位时间
	 */
	private String positioningTimeStr;
	/**
	 * 创建时间
	 */
	private String createdTime;
	/**
	 * 紧急定位
	 */
	private Boolean emergencyLocation;

	/**
	 * 编辑
	 */
	private String manager;

	public Double getAltitude() {
		return altitude;
	}

	public String getBoxName() {
		return boxName;
	}
	public String getBoxSerialNumber() {
		return boxSerialNumber;
	}

	public String getBoxUser() {
		return boxUser;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public String getCreatedTime() {
		return createdTime;
	}
	public Double getDirection() {
		return direction;
	}


	public long getId() {
		return id;
	}

	public String getLatitude() {
		return latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public String getManager() {
		return manager;
	}

	public Double getSpeed() {
		return speed;
	}

	public void setAltitude(Double altitude) {
		this.altitude = altitude;
	}

	public void setBoxName(String boxName) {
		this.boxName = boxName;
	}

	public void setBoxSerialNumber(String boxSerialNumber) {
		this.boxSerialNumber = boxSerialNumber;
	}

	public void setBoxUser(String boxUser) {
		this.boxUser = boxUser;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	public void setDirection(Double direction) {
		this.direction = direction;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	public void setSpeed(Double speed) {
		this.speed = speed;
	}

	public MsgType getLocationSource() {
		return locationSource;
	}

	public void setLocationSource(MsgType locationSource) {
		this.locationSource = locationSource;
	}

	public String getPositioningTimeStr() {
		return positioningTimeStr;
	}

	public void setPositioningTimeStr(String positioningTimeStr) {
		this.positioningTimeStr = positioningTimeStr;
	}

	public Boolean getEmergencyLocation() {
		return emergencyLocation;
	}

	public void setEmergencyLocation(Boolean emergencyLocation) {
		this.emergencyLocation = emergencyLocation;
	}
	
}

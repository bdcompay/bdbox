package com.bdbox.api.dto;

import com.bdbox.entity.Product;

public class ProductDto extends Product {
	
	private String buyStartTimeStr;
	private String buyEndTimeStr;
	private String createdTimeStr;
	private String handle;
	private String ProductBuyTypes;
	
	public String getBuyStartTimeStr() {
		return buyStartTimeStr;
	}
	public void setBuyStartTimeStr(String buyStartTimeStr) {
		this.buyStartTimeStr = buyStartTimeStr;
	}
	public String getBuyEndTimeStr() {
		return buyEndTimeStr;
	}
	public void setBuyEndTimeStr(String buyEndTimeStr) {
		this.buyEndTimeStr = buyEndTimeStr;
	}
	public String getCreatedTimeStr() {
		return createdTimeStr;
	}
	public void setCreatedTimeStr(String createdTimeStr) {
		this.createdTimeStr = createdTimeStr;
	}
	public String getHandle() {
		return handle;
	}
	public void setHandle(String handle) {
		this.handle = handle;
	}
	public String getProductBuyTypes() {
		return ProductBuyTypes;
	}
	public void setProductBuyTypes(String productBuyTypes) {
		ProductBuyTypes = productBuyTypes;
	}
	
	
}

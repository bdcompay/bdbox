package com.bdbox.api.dto;

import java.util.Calendar;

public class DialrecordDto {
	/**
	 * 盒子ID
	 */
	private String boxSerialNumber;
	/**
	 * 消息ID，api回调的消息ID
	 */
	private long msgID;
	/**
	 * 呼叫号码
	 */
	private String dialNum;
	/**
     * 音乐url
     */
    private String  musicUrl;
    /**
     * 创建时间
     */
    private String createdtime;
	public String getBoxSerialNumber() {
		return boxSerialNumber;
	}
	public void setBoxSerialNumber(String boxSerialNumber) {
		this.boxSerialNumber = boxSerialNumber;
	}
	public long getMsgID() {
		return msgID;
	}
	public void setMsgID(long msgID) {
		this.msgID = msgID;
	}
	public String getDialNum() {
		return dialNum;
	}
	public void setDialNum(String dialNum) {
		this.dialNum = dialNum;
	}
	public String getMusicUrl() {
		return musicUrl;
	}
	public void setMusicUrl(String musicUrl) {
		this.musicUrl = musicUrl;
	}
	public String getCreatedtime() {
		return createdtime;
	}
	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}
    
}

package com.bdbox.api.dto;

import java.util.List;

public class ParamFamilyDataDto {

	private List<FamilyDto> familyDtos;
	private String boxSerialNumber;
	private String cardNumber;


	public List<FamilyDto> getFamilyDtos() {
		return familyDtos;
	}

	public void setFamilyDtos(List<FamilyDto> familyDtos) {
		this.familyDtos = familyDtos;
	}

	public String getBoxSerialNumber() {
		return boxSerialNumber;
	}

	public void setBoxSerialNumber(String boxSerialNumber) {
		this.boxSerialNumber = boxSerialNumber;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	
}
package com.bdbox.api.dto;

import com.bdbox.entity.Returns;

public class ReturnsDto extends Returns {

	/**
	 * 购买时间
	 */
	private String buyTimeStr;
	/**
	 * 申请提交时间
	 */
	private String applySubmitTimeStr;
	
	/**
	 * 申请审核时间
	 */
	private String applyAuditTimeStr;

	/**
	 * 物流单填写时间
	 */
	private String expressNumWriteTimeStr;

	/**
	 * 退货完成时间
	 */
	private String finishTimeStr;
	
	 //以下字段是属于租用新增的字段
	
	/**
	 * 租用开始时间
	 */
	private String rentStartTime;
	/**
	 * 租用结束时间
	 */
	private String rentEndTime;
	 
	/**
	 * 剩余天数
	 */
	private String surplusdate; //剩余天数
	/**
	 * 预扣租金
	 */
	private String deduct; //预扣租金
	/**
	 * 计算方式
	 */
 	private String method;//计算方式
 	/**
	 * 用户名
	 */
 	private String username;//用户名
 	/**
	 * 租用单价
	 */
 	private String unitprice;//用户名

	public String getBuyTimeStr() {
		return buyTimeStr;
	}

	public void setBuyTimeStr(String buyTimeStr) {
		this.buyTimeStr = buyTimeStr;
	}

	public String getApplySubmitTimeStr() {
		return applySubmitTimeStr;
	}

	public void setApplySubmitTimeStr(String applySubmitTimeStr) {
		this.applySubmitTimeStr = applySubmitTimeStr;
	}

	public String getApplyAuditTimeStr() {
		return applyAuditTimeStr;
	}

	public void setApplyAuditTimeStr(String applyAuditTimeStr) {
		this.applyAuditTimeStr = applyAuditTimeStr;
	}

	public String getExpressNumWriteTimeStr() {
		return expressNumWriteTimeStr;
	}

	public void setExpressNumWriteTimeStr(String expressNumWriteTimeStr) {
		this.expressNumWriteTimeStr = expressNumWriteTimeStr;
	}

	public String getFinishTimeStr() {
		return finishTimeStr;
	}

	public void setFinishTimeStr(String finishTimeStr) {
		this.finishTimeStr = finishTimeStr;
	}

	public String getRentStartTime() {
		return rentStartTime;
	}

	public void setRentStartTime(String rentStartTime) {
		this.rentStartTime = rentStartTime;
	}

	public String getRentEndTime() {
		return rentEndTime;
	}

	public void setRentEndTime(String rentEndTime) {
		this.rentEndTime = rentEndTime;
	}

	public String getSurplusdate() {
		return surplusdate;
	}

	public void setSurplusdate(String surplusdate) {
		this.surplusdate = surplusdate;
	}

	public String getDeduct() {
		return deduct;
	}

	public void setDeduct(String deduct) {
		this.deduct = deduct;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUnitprice() {
		return unitprice;
	}

	public void setUnitprice(String unitprice) {
		this.unitprice = unitprice;
	}

	 
	
	
}

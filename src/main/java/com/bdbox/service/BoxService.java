package com.bdbox.service;

import java.util.List;

import net.sf.json.JSONObject;

import com.bdbox.api.dto.BoxDto;
import com.bdbox.constant.BoxStatusType;
import com.bdbox.constant.CardType;
import com.bdbox.entity.Box;
import com.bdsdk.json.ObjectResult;

public interface BoxService {

	public Box saveBox(Box box);

	public Boolean updateBox(Box box);

	/**
	 * 绑定用户
	 * 
	 * @param boxSerialNumber
	 * @param snNumber
	 * @param userId
	 * @return
	 */
	public Boolean doBindBox(String boxSerialNumber, String snNumber,String name, Long userId,String idNumber,String realName);

	/**
	 * 解绑用户
	 * 
	 * @param boxSerialNumber
	 * @param snNumber
	 * @param userId
	 * @return
	 */
	public Boolean doUnBindBox(String boxSerialNumber, Long userId);

	public Box getBox(BoxStatusType boxStatusType,String boxSerialNumber, String cardNumber,String snNumber, Long userId);

	public BoxDto getBoxDto(String boxSerialNumber, String cardNumber,String snNumber, Long userId);

	public List<BoxDto> getUserBoxDtos(Long userId, int page, int pageSize);
	
	public List<BoxDto> query(Long userId,CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType, String snNumber,String boxName, String userName,
			String order, int page, int pageSize);

	public int queryAmount(Long userId,CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType, String snNumber,String boxName, String userName);
	
	public BoxDto getBoxDto(Long id);
	
	public Box getBox(Long id);
	
	public void deleteBox(long id);
	
	/**
	 * 查询企业用户下属北斗盒子数量
	 * @param entUserId
	 * 		企业用户ID
	 * @return
	 */
	public int countEntBoxNum(long entUserId);
	
	/**
	 * 查询企业用户下属北斗盒子（分页查询）
	 * @param entUserId
	 * 		企业用户ID
	 * @param page
	 * 		页码
	 * @param pageSize
	 * 		每页记录数
	 * @return
	 */
	public List<BoxDto> queryEntBoxs(long entUserId,int page,int pageSize);
	
	public List<BoxDto> query(Long userId,CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType, String snNumber,String boxName, String userName,Long entUserId,
			String order, int page, int pageSize);

	public int queryAmount(Long userId,CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType, String snNumber,String boxName, String userName,Long entUserId);
	
	/**
	 * 根据北斗卡号码获取数据
	 * @param cardNum
	 * @return
	 */
	public Box getBoxForCardNum(String cardNum);
	
	/**
	 * 根据盒子ID获取数据
	 * @param cardNum
	 * @return
	 */
	public Box getBoxOne(String boxSerialNumber);
	
	/**
	 * 根据盒子ID获取数据
	 * @param cardNum
	 * @return
	 */
	public List<Box> getBoxInfo(String boxSerialNumber);
	
	/**
	 * 获取企业用户所有下属卡
	 * @param entUserId
	 * @return
	 */
	public ObjectResult getEntUserBoxs(Long entUserId);
	
	/**
	 * 根据企业用户id和盒子id获取数据
	 * @param entUserId
	 * @param cardId
	 * @return
	 */
	public Box getEntUserBox(Long entUserId, String cardId);
	
	/**
	 * 搜索盒子
	 * @param entUserId
	 * @param cardId
	 * @return
	 */
	public ObjectResult searchBox(Long entUserId, String cardId);
	
	/**
	 * 根据盒子ID获取盒子信息
	 * @param json 盒子ID
	 * @return
	 */
	public ObjectResult getBoxs(JSONObject json);
}

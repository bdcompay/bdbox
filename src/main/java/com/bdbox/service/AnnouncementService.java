package com.bdbox.service;

 import java.util.List;

import com.bdbox.api.dto.AnnouncementDto;
import com.bdbox.entity.Announcement;
   
public interface AnnouncementService {
	
	public List<Announcement> getlistAnnouncementtable(int page, int pageSize);
	
	public Integer getlistAnnouncementtableCount();
	
	public Announcement save(String announcementname, String hyperlink);
	
	public Announcement get(long id);
	
	public AnnouncementDto castFrom(Announcement announcement);
	
	public boolean update(Long id, String updateannouncementname, String updatehyperlink);
	
	public boolean deleteAnnouncement(Long id);
	
	public List<Announcement> getfindAnnouncement();  

	public List<AnnouncementDto> getList(String content, int page, int pageSize);
	
	public int count(String content);
}

package com.bdbox.service;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.FileDownloadLog;
import com.bdbox.web.dto.FileDownloadLogDto;
import com.bdsdk.json.ListParam;

/**
 * 文件下载记录业务接口
 * 
 * @ClassName: FileDownloadLogService 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-2-26 下午2:22:53 
 * @version V5.0 
 */
public interface FileDownloadLogService {
	/**
	 * 添加文件下载记录（属性）
	 * 
	 * @param filename
	 * @param status
	 * @param ip
	 * @param ipArea
	 * @param username
	 * @param downloadTime
	 * @return
	 */
	public FileDownloadLog add(String filename,Boolean status,long timeConsumig,String ip,String ipArea,
			String username,Calendar downloadTime);
	
	/**
	 * 添加文件下载记录（对象）
	 * 
	 * @param fileDownloadLog
	 * @return
	 */
	public FileDownloadLog add(FileDownloadLog fileDownloadLog);
	
	/**
	 * 按条件分页查询
	 * 
	 * @param filename
	 * 		下载文件名
	 * @param status
	 * 		状态
	 * @param ip
	 * 		下载ip
	 * @param ipArea
	 * 		ip归属地
	 * @param username
	 * 		用户名称
	 * @param startTime
	 * 		开始查询下载时间
	 * @param endTime
	 * 		结束查询下载时间
	 * @param page
	 * 		页数
	 * @param pageSize
	 * 		每页显示记录数
	 * @return
	 * 		list集合
	 */
	public List<FileDownloadLogDto> listFileDownloadLog(String filename,Boolean status,String ip,String ipArea,
			String username,Calendar startTime,Calendar endTime,int page,int pageSize);
	
	/**
	 * 按条件统计
	 * 
	 * @param filename
	 * 		下载文件名
	 * @param status
	 * 		状态
	 * @param ip
	 * 		下载ip
	 * @param ipArea
	 * 		ip归属地
	 * @param username
	 * 		用户名称
	 * @param startTime
	 * 		开始查询下载时间
	 * @param endTime
	 * 		结束查询下载时间
	 * @return
	 * 		统计数量
	 */
	public int count(String filename,Boolean status,String ip,String ipArea,
			String username,Calendar startTime,Calendar endTime);
	
	/**
	 * 按条件分页查询
	 * 
	 * @param filename
	 * 		下载文件名
	 * @param status
	 * 		状态
	 * @param ip
	 * 		下载ip
	 * @param ipArea
	 * 		ip归属地
	 * @param username
	 * 		用户名称
	 * @param startTime
	 * 		开始查询下载时间
	 * @param endTime
	 * 		结束查询下载时间
	 * @param page
	 * 		页数
	 * @param pageSize
	 * 		每页显示记录数
	 * @return
	 * 		ListParam
	 */
	public ListParam fileDownloadLogs(String filename,Boolean status,String ip,String ipArea,
			String username,Calendar startTime,Calendar endTime,int page,int pageSize);
	
	
}

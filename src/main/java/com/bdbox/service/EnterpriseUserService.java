package com.bdbox.service;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.EnterpriseUser;
import com.bdbox.web.dto.EnterpriseUserDto;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;

/**
 * 企业用户业务处理接口
 * 
 * @ClassName: EnterpriseUserService 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-3-11 上午11:19:47 
 * @version V5.0 
 */
public interface EnterpriseUserService {
	
	/**
	 * 通过用户保存企业用户
	 * 
	 * @param entUser
	 * 		企业用户对象
	 * @return
	 */
	public EnterpriseUserDto save(EnterpriseUser entUser);
	
	/**
	 * 通过企业用户属性添加企业用户
	 * 
	 * @param name
	 * 		企业名称
	 * @param orgCode
	 * 		组织代码
	 * @param linkman
	 * 		联系人
	 * @param phone
	 * 		联系电话
	 * @param address
	 * 		地址
	 * @param explain
	 * 		说明
	 * @return
	 */
	public EnterpriseUserDto save(String name,String orgCode,String linkman,String phone,
			String mail,String address,String explain,boolean isDsiEnable,int freq, String password,String pushURL);

	/**
	 * 通过id删除企
	 *
	 * @param id
	 */
	public boolean delete(long id);
	
	/**
	 * 通过企业用户对象删除
	 * 
	 * @param entUser
	 */
	public boolean delete(EnterpriseUser entUser);
	
	/**
	 * 修改企业用户
	 * 
	 * @param id
	 * @param name
	 * @param orgCode
	 * @param linkman
	 * @param phone
	 * @param address
	 * @param explain
	 * @return
	 */
	public boolean update(long id,String name,String orgCode,String linkman,String phone,
			String mial,String address,String explain,boolean isDsiEnable,int freq,String pushURL);
	
	/**
	 * 修改企业用户
	 * 
	 * @param entUser
	 */
	public boolean update(EnterpriseUser entUser);
	
	/**
	 * 通过id查询
	 * @param id
	 * @return
	 */
	public EnterpriseUserDto get(long id);

	/**
	 * 多条件分页查询企业用户
	 * 
	 * @param name
	 * 		企业名称
	 * @param orgCode
	 * 		组织代码
	 * @param linkman
	 * 		联系人
	 * @param phone
	 * 		联系电话
	 * @param address
	 * 		地址
	 * @param startTime
	 * 		开始时间
	 * @param endTime
	 * 		结束时间
	 * @param page
	 * 		页码
	 * @param pageSize
	 * 		每页记录数
	 * @return
	 */
	public List<EnterpriseUserDto> listEnts(String name,String orgCode,String linkman,String phone,
			String mial,String address,Calendar startTime,Calendar endTime,int page,int pageSize);
	
	/**
	 * 多条件统计企业用户数量
	 * @param name
	 * @param orgCode
	 * @param linkman
	 * @param phone
	 * @param address
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public int count(String name,String orgCode,String linkman,String phone,
			String mial,String address,Calendar startTime,Calendar endTime);
	
	/**
	 * 统计符合jquery.datatable格式的企业用户
	 * 
	 * @param name
	 * @param orgCode
	 * @param linkman
	 * @param phone
	 * @param address
	 * @param startTime
	 * @param endTime
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public ListParam queryJqEnts(String name,String orgCode,String linkman,String phone,
			String mial,String address,Calendar startTime,Calendar endTime,int page,int pageSize);
	
	/**
	 * 绑定企业用户和北斗盒子
	 * @param entUsreId
	 * 		企业用户ID
	 * @param boxNumber
	 * 		北斗盒子号（ID）
	 * @return
	 */
	public ObjectResult doBindEntAndBox(long entUsreId,long boxNumber);
	
	/**
	 * 解绑企业用户和北斗盒子
	 * @param entUserId
	 * @param boxNumber
	 * @return
	 */
	public Boolean doUnbindEntAndBox(long entUserId,long boxNumber);
	
	/**
	 * 登录
	 * @param username
	 * @param password
	 * @return
	 */
	public ObjectResult login(String username, String password);
	
	/**
	 * 获取验证码
	 * @param phone
	 * @return
	 */
	public ObjectResult doCareateYZM(String phone);
	
	/**
	 * 修改密码
	 * @param phone
	 * @param password
	 * @return
	 */
	public ObjectResult changePwd(String phone, String password);
	
	/**
	 *获取用户 
	 * @param userPowerKey
	 * @return
	 */
	public EnterpriseUser getUserByPowerKey(String userPowerKey);
	
	/**
	 * 根据手机号码获取用户信息
	 * @param phone
	 * @return
	 */
	public EnterpriseUser getEntUser(String phone);
}

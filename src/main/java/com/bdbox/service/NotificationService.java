package com.bdbox.service;

import java.util.List; 
import com.bdbox.api.dto.NotificationDto;
import com.bdbox.entity.Notification;

public interface NotificationService {
	
	public List<Notification> getlistNotification(String tongzhifanshi,String tongzhileixing,String type_twl,int page, int pageSize);
	
	public Integer getlistNotificationcount(String tongzhifanshi,String tongzhileixing,String type_twl);
	
	public Notification saveNotification(String isemail,String tongzhileixing,String frommail, String tomail,String subject,String center,String note,String tomailname,String TeliPhone,String notecenter,String type_twl);

	public Notification getNotification(long id);
	
	public NotificationDto castFrom(Notification notification);
	
	public boolean update(long id,String isemail,String  note,String tongzhileixing,String frommail,String upedatetomailname,String updateemail,String updateTeliPhone,String updatesubject,String updatecenter,String updatenotecenter);
	
	public boolean deleteNotification(Long id);
	
	public Notification getnotification(String mailtype,String type_twl);
	
	public boolean updateNotification(Notification notification);


 
}

package com.bdbox.service;

import java.util.Calendar;
import java.util.List;

import com.bdbox.api.dto.ReturnsDto;
import com.bdbox.constant.PayType;
import com.bdbox.constant.ReturnsStatusType;
import com.bdbox.entity.Returns;
import com.bdsdk.json.ListParam;

public interface ReturnsService {
	/**
	 * 保存退货订单
	 * 
	 * @param orderId
	 * 			订单id
	 * @param productId
	 * 			产品id
	 * @param productName
	 * 			产品名称
	 * @param price
	 * 			产品价格
	 * @param buyTime
	 * 			购买时间
	 * @param returnsCount
	 * 			退货数量
	 * @param returnsCause
	 * 			退货原因
	 * @return
	 * 		退货订单
	 */
	 
	public Returns saveReturns(Long orderId,Long productId,String orderNum,String productName,long userId,Double price,Calendar buyTime,int returnsCount,String returnsCause,String buy,PayType payType);
	
	/**
	 * 删除退货订单
	 * 
	 * @param id
	 * @return
	 */
	public boolean  deleteRetuens(long id);
	
	/**
	 * 修改退货订单
	 * 
	 * @param returns
	 * @return
	 */
	public Returns updateRetuens(Returns returns);
	
	/**
	 * 根据id查询退货订单
	 * 
	 * @param id
	 * @return
	 */
	public Returns getReturnsById(long id);

	public ReturnsDto getReturnsDtoById(long id);
	
	/**
	 * 根据条件查询退货信息
	 * 
	 * @param orderId
	 * 			订单id
	 * @param returnsNum
	 * 			退款单号
	 * @param UserId
	 * 			用户id
	 * @param expressNum
	 * 			快递号
	 * @param returnsStatusType
	 * 			退货状态
	 * @param startTime
	 * 			起始查询时间(根据记录创建时间查询)
	 * @param endTime
	 * 			截止查询时间(根据记录创建时间查询)
	 * @return
	 */
	public List<Returns> listReturns(Long orderId, Long returnsNum,
			Long userId, String expressNum, ReturnsStatusType returnsStatusType,
			Calendar startTime,Calendar endTime,int page,int pageSize,String orderstutas);
	
	/**
	 * 根据条件统计记录数量
	 * 
	 * @param orderId
	 * 			订单id
	 * @param returnsNum
	 * 			退款单号
	 * @param UserId
	 * 			用户id
	 * @param expressNum
	 * 			快递号
	 * @param returnsStatusType
	 * 			退货状态
	 * @param startTime
	 * 			起始查询时间(根据记录创建时间查询)
	 * @param endTime
	 * 			截止查询时间(根据记录创建时间查询)
	 * @return
	 * 		统计数量
	 */
	public int countReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum, ReturnsStatusType returnsStatusType,
			Calendar startTime,Calendar endTime,String orderstutas );
	
	/**
	 * 根据条件查询退货信息（符合Jquery.datatable格式）
	 * 
	 * @param orderId
	 * 			订单id
	 * @param returnsNum
	 * 			退款单号
	 * @param UserId
	 * 			用户id
	 * @param expressNum
	 * 			快递号
	 * @param returnsStatusType
	 * 			退货状态
	 * @param startTime
	 * 			起始查询时间(根据记录创建时间查询)
	 * @param endTime
	 * 			截止查询时间(根据记录创建时间查询)
	 * @return orderstutas
	 *          退货订单的类型（RENT为租用，GENERAL为购买）
	 */
	public ListParam queryReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum, ReturnsStatusType returnsStatusType,
			Calendar startTime,Calendar endTime,int page,int pageSize,String orderstutas);
	
	/**
	 * 管理员审核退货申请
	 * 
	 * @param id
	 * @param auditResult
	 * 			审核结果
	 * @param cause
	 * 			原因
	 * @return
	 * 		是否成功
	 */
	public boolean doAuditApply(long id,String auditResult,String cause);
	
	
	/**
	 * 用户提交快递单号
	 * 
	 * @param id
	 * @param expressName
	 * 			快递公司名称
	 * @param expressNum
	 * 			快递单号
	 * @return
	 * 		是否成功
	 */
	public boolean doSubmitExpressNum(long id,String expressName,String expressNum);
	
	/**
	 * 管理员接收产品
	 * 
	 * @param id
	 * @return
	 */
	public boolean doReceiveProduct(long id);
	
	/**
	 * 管理员确认收货
	 * @param expressId
	 * @param endTime
	 */
	public void doConfirmSH(long expressId, String endTime);
	
	/**
	 * 管理员审核接收到的产品
	 * 
	 * @param id
	 * @param auditResult
	 * 			审核结果
	 * @param explain
	 * 			原因说明
	 * @return
	 * 		是否成功
	 */
	public boolean doAuditProduct(long id,String auditResult,String explain);
	
	/**
	 * 管理员确定退款
	 * 
	 * @param id
	 * @param buyMoney
	 * 			购买要退货产品的金额
	 * @param deductionMoney
	 * 			退货时扣减金额
	 * @param deductionCause
	 * 			退货时扣减金额的原因
	 * @param returnmoney
	 * 			实际退款金额
	 * @return
	 * 
	 */
	public boolean doRefund(long id,double buyMoney,double deductionMoney,String deductionCause,double returnmoney);
	

	/**
	 * 退货退款成功
	 * 
	 * @param id
	 */
	public boolean doReturnsSuccess(long id);
	
	ReturnsDto castFromReturns(Returns returns);
	
	public Returns queryReturn(long orderid);
	
	//租用归还管理页面查询
	public ListParam doqueryRentReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum, ReturnsStatusType returnsStatusType,
			Calendar startTime,Calendar endTime,int page,int pageSize,String orderstutas);
	
	public List<Returns> listRentReturns(Long orderId, Long returnsNum,
			Long userId, String expressNum, ReturnsStatusType returnsStatusType,
			Calendar startTime,Calendar endTime,int page,int pageSize,String orderstutas);
	
	public int countRentReturns(Long orderId, Long returnsNum, String orderNum,
			Long userId, String expressNum, ReturnsStatusType returnsStatusType,
			Calendar startTime,Calendar endTime,String orderstutas);

	public Returns getReturnsenty(long expressid);
}

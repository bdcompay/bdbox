package com.bdbox.service;

import java.util.List;

import com.bdbox.api.dto.UserQuestionDto;
import com.bdbox.entity.UserQuestion;

public interface UserQuestionService {

	//保存用户提交的问题
	public UserQuestion save(UserQuestion enty);
	//查询用户提交的所有问题
	public List<UserQuestionDto> getuserquestion(String userid);
	
	public List<UserQuestionDto> getuserquestionlist(String username,int page,int pageSize); 
 	
	public Integer getuserquestionlistcount(String username);
	
	public UserQuestionDto getquestion(long id);
	
	public UserQuestion getenty(long id);
	
	public void update(UserQuestion userQuestion);
	
	public boolean deletemyquestion(long id);




	
}

package com.bdbox.service;

import com.bdbox.constant.RefundStatus;
import com.bdbox.entity.Refund;
import com.bdbox.web.dto.RefundDto;
import java.util.Calendar;
import java.util.List;

public abstract interface RefundService {
	public abstract void saveRefund(Refund paramRefund);

	public abstract void updateRefund(Refund paramRefund);

	public abstract void deleteRefund(Long paramLong);

	public abstract Refund queryRefund(Long paramLong);

	public abstract RefundDto queryRefundDto(Long paramLong);

	public abstract Refund queryRefundUid(Long paramLong);

	public abstract List<RefundDto> listRefund(String orderNo,
			Calendar applyTime, Calendar refundTime, String userName,
			RefundStatus refundStatus, Integer page, Integer pageSize,
			String order);

	public abstract int listRefundCount(String paramString1,
			Calendar paramCalendar1, Calendar paramCalendar2,
			String paramString2, RefundStatus paramRefundStatus);

	public abstract List<RefundDto> getUserRefund(String paramString);

	/**
	 * 根据退款单号查询退款信息
	 * 
	 * @param refund_no
	 * @return
	 */
	Refund queryRefundByRefund_no(String refund_no);

	/**
	 * 查询退款状态为 退款中 的退款
	 * 
	 * @return
	 */
	List<Refund> listRefundByRefundTime();
	
	/**
	 * 查询退款
	 * 
	 * @param refund_no
	 */
	public Boolean doWxQueryRefund(String refund_no);
	

	Refund getRefund(long id);
}
package com.bdbox.service;

import java.util.Calendar;
import java.util.List;

import com.bdbox.api.dto.ProductDto;
import com.bdbox.constant.ProductBuyType;
import com.bdbox.constant.ProductType;
import com.bdbox.entity.Product;

public interface ProductService {
	/**
	 * 根据产品id查询
	 * 
	 * @param id
	 * @return
	 */
	public Product get(Long id);

	/**
	 * 分页多条件查询产品
	 * 
	 * @param name
	 *            产品名称
	 * @param productType
	 *            产品类型
	 * @param productBuyType
	 *            产品购买类型
	 * @param startTime
	 *            起始查询时间
	 * @param endTime
	 *            截止查询时间
	 * @param page
	 *            页数
	 * @param pageSize
	 *            显示数
	 * @return
	 */
	public List<Product> listBy(String name, ProductType productType,
			ProductBuyType productBuyType, Calendar startTime,
			Calendar endTime, int page, int pageSize);

	/**
	 * 统计数量
	 * 
	 * @param name
	 * @param productType
	 * @param productBuyType
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public int count(String name, ProductType productType,
			ProductBuyType productBuyType, Calendar startTime, Calendar endTime);

	/**
	 
	 * @return
	 */
	public boolean update(Long id, String name, ProductType productType, Double price,
			int saleNumber, int stockNumber, ProductBuyType productBuyType,
			boolean isOpenBuy, Calendar buyStartTime,Calendar buyEndTime, String productUrl,String color,
			String explain, Double updateRental, Double updateAntecedent, Double updateCouponPrice);
	
	/**
	 * 更新
	 * 
	 * @param product
	 */
	public boolean updateProduct(Product product);

	/**
	 * 根据产品id删除产品
	 * 
	 * @param id
	 * @return
	 */
	public boolean deleteProduct(Long id);

	/**
	 * 添加产品
	 * 
	 * @param product
	 * @return
	 */
	public Product save(String name, ProductType productType, Double price,
			int saleNumber, int stockNumber, ProductBuyType productBuyType,
			boolean isOpenBuy, Calendar buyStartTime,Calendar buyEndTime, String productUrl,String color,
			String explain, Double rental, Double antecedent, Double couponPrice);
	
	/**
	 * 把Product转换为ProductDot
	 * 
	 * @param product
	 * @return
	 */
	public ProductDto castFrom(Product product);

	/**
	 * 获取商品状态为抢购的商品库存
	 * @return
	 */
	public int getStockNumber();
}

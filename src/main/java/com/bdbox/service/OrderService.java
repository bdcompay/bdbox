package com.bdbox.service;

import com.bdbox.constant.DealStatus;
import com.bdbox.constant.ProductType;
import com.bdbox.entity.Order;
import com.bdbox.entity.Refund;
import com.bdbox.web.dto.OrderDto;
import com.bdsdk.json.ObjectResult;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

public abstract interface OrderService {
	public abstract List<OrderDto> listOrder(String paramString1,
			ProductType paramProductType, String paramString2,
			Calendar paramCalendar, String paramString3,
			DealStatus paramDealStatus, Integer paramInteger1,
			Integer paramInteger2, String paramString4,String transactionType,String dealStatusStrbd,String name);

	public abstract int listOrderCount(String paramString1,
			ProductType paramProductType, String paramString2,
			Calendar paramCalendar, String paramString3,
			DealStatus paramDealStatus,String transactionType,String dealStatusStrbd,String name);

	public abstract List<OrderDto> getUserOrder(String paramString);

	public abstract OrderDto getOrderDto(long paramLong);

	public abstract void saveOrder(Order paramOrder);

	public abstract void deleteOrder(long paramLong);

	public abstract void updateOrder(Order paramOrder);

	public abstract Order getOrder(Long paramLong);

	public abstract List<OrderDto> listAllSendOrder();

	public abstract void updateByTradeNo(String paramString);

	/**
	 * 根据订单号查询订单
	 * 
	 * @param trade_no
	 *            商户订单号
	 * @return Order对象
	 */
	Order getOrferByOrderNO(String trade_no);

	/**
	 * 查询订单接口 1).如果支付成功的订单就不查询， 2).如果查询到的交易状态与本地不一致，修改为相应的本地状态
	 * 
	 * @param trade_no
	 *            商户订单号
	 */
	public void doWxQueryOrder(String trade_no);
	
	/**
	 * 查询退款
	 * 
	 * @param refund_no
	 */
	  public Boolean doWxQueryRefund(String refund_no);
	
	  public abstract Boolean updateOrderCancel(String orderNo); 
	  
	  public abstract boolean updateOrderDelete(String orderNo);
	  
	  public abstract boolean updateOrderRefund(String orderNo);
	  
	  public abstract boolean updateOrderClose(String orderNo);

	  public abstract boolean updateOrderShipments(String orderNo);

	  public abstract boolean updateOrderrefund(String orderNo);
	  
	  public abstract List<Order> getOrder(String orderNo);
	  
	  public abstract  List<Refund> getrefundNo(String orderNo);
	  
	  Order userRentOrder(long productId, Double price, int count, Double totalPrice,
				long addressId,String user,
				String productBuyType, String rentPrice, String r_count, String leaveMessage, String r_price,String renlnameorder,String unitcost,String bdcoodnember, String zfType);
	  
	  Order userBuyOrder(long productId, Double price, int count, Double totalPrice,
				long addressId,String user, String productBuyType, Long invoice,String bdcoodnember, String zfType); 
	  
	  /**
	   * 订单支付码已失效，重新创建一个微信预支付订单，获取新交易号覆盖旧交易号
	   * 
	   * @param oid
	   * 		订单id
	   * @return
	   * 		支付号是否更新成功
	   */
	  public ObjectResult updateOrderTradeNo(long oid);
	  
	  /**
	   * 通过用户名和产品id查询是否存在订单
	   * 
	   * @param pid
	   * 		产品id
	   * @param username
	   * 		用户名
	   * @return
	   *		存在返回true，不存在返回false
	   */
	  public boolean queryOrderByPidUser(long pid,String username);
	  
	  public abstract Order getOrderstatus(String ordreNo);
	  
	  
	  //用户租用订单
	  public abstract List<OrderDto> listOrder2(String paramString1,
				ProductType paramProductType, String paramString2,
				Calendar paramCalendar, String paramString3,
				DealStatus paramDealStatus, Integer paramInteger1,
				Integer paramInteger2, String paramString4,String transactionType,String dealStatusStrbd,String name);
	  
	  public abstract int listOrderCount2(String paramString1,
				ProductType paramProductType, String paramString2,
				Calendar paramCalendar, String paramString3,
				DealStatus paramDealStatus,String transactionType,String dealStatusStrbd,String name);
	  
	  //租赁订单查询
	  public abstract List<OrderDto> getUserRentOrder(String paramString);
	  
	  public List<Order> queryBdcood();
	  
	  public List<Order> getOrderList(String deal,String trandtype);
	  
	  public abstract Order getOrd(String tranOrder);
	  
	  public void authFinish(); 
	  
	  public Map<String, Integer> statisticOrder(Calendar startTime, Calendar endTime, String statisticType, String transactionType);

}

package com.bdbox.service;

import java.util.Calendar;
import java.util.List;

import com.bdbox.api.dto.UserRegLogDto;
import com.bdbox.entity.UserRegLog;
import com.bdsdk.json.ListParam;

/**
 * 用户注册记录逻辑业务处理接口
 * 
 * @ClassName: UserRegLogService 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-2-2 下午2:06:29 
 * @version V5.0 
 */
public interface UserRegLogService{
	/**
	 * 对象保存
	 * @param userRegLog
	 * 		UserRegLog对象
	 * @return UserRegLog
	 */
	public UserRegLog save(UserRegLog userRegLog);
	/**
	 * 保存
	 * @param username
	 * 		用户名
	 * @param ip
	 * 		用户ip
	 * @param ipArea
	 * 		ip归属地区
	 * @param userid
	 * 		创建人，为null表示自己创建
	 * @return UserRegLog
	 */
	public UserRegLog save(String username,String ip,String ipArea,Long userid);
	/**
	 * id删除
	 * @param id
	 * 		用户注册记录id
	 * @return boolean
	 */
	public boolean deleteUserRegLog(long id);
	/**
	 * 对象删除
	 * @param userRegLog
	 * 		UserRegLog对象
	 * @return boolean
	 */
	public boolean deleteUserRegLog(UserRegLog userRegLog);
	/**
	 * 修改对象
	 * @param userRegLog
	 * 		UserRegLog对象
	 * @return boolean
	 */
	public boolean updateUserRegLog(UserRegLog userRegLog);
	/**
	 * 根据id获取对象
	 * @param id
	 * 		用户注册记录id
	 * @return UserRegLogDto
	 */
	public UserRegLogDto get(long id);
	/**
	 * 条件分页查询
	 * @param username
	 * 		用户名
	 * @param ip
	 * 		用户ip
	 * @param ipArea
	 * 		ip所属地区
	 * @param userid
	 * 		创建人
	 * @param startTime
	 * 		起始查询时间
	 * @param endTime
	 * 		截止查询时间
	 * @param page
	 * 		查询页数
	 * @param pagaSize
	 * 		查询记录数
	 * @return List<UserRegLogDto>
	 */
	public List<UserRegLogDto> listUserRegLog(String username,String ip,
			String ipArea,Long userid,Calendar startTime,Calendar endTime,int page,int pageSize);
	/**
	 * 条件统计数量
	 * @param username
	 * 		用户名
	 * @param ip
	 * 		用户ip
	 * @param ipArea
	 * 		ip所属地区
	 * @param userid
	 * 		创建人
	 * @param startTime
	 * 		起始查询时间
	 * @param endTime
	 * 		截止查询时间
	 * @return int
	 */
	public int count(String username,String ip,
			String ipArea,Long userid,Calendar startTime,Calendar endTime);
	/**
	 * 条件分页查询(符合Jquery.datatable格式)
	 * @param username
	 * 		用户名
	 * @param ip
	 * 		用户ip
	 * @param ipArea
	 * 		ip所属地区
	 * @param userid
	 * 		创建人
	 * @param startTime
	 * 		起始查询时间
	 * @param endTime
	 * 		截止查询时间
	 * @param page
	 * 		查询页数
	 * @param pagaSize
	 * 		查询记录数
	 * @return ListParam
	 */
	public ListParam listUserRegLogJq(String username,String ip,
			String ipArea,Long userid,Calendar startTime,Calendar endTime,int page,int pageSize);
}

package com.bdbox.service;

import java.util.Calendar;
import java.util.List;

import com.bdbox.web.dto.EntStatDailyDto;
import com.bdsdk.json.ObjectResult;

/**
 * 统计企业每日使用数量业务接口
 * @author jlj
 *
 */
public interface EntStatDailyService {
	//保存
	//通过id获取
	//修改
	//删除
	
	/**
	 * 增加企业下属盒子每日发送数量
	 * @param fromCardNumber
	 * 		发送卡号
	 * @return
	 */
	public boolean addSendNumber(String fromCardNumber);
	
	/**
	 * 通过周名查询企业使用数量，企业下属盒子发送数量
	 * @param weekName
	 * 		周（星期）名
	 * @param entId
	 * 		企业id
	 * @return
	 */
	public ObjectResult getByWeekName(String weekName,long entId);
	
	/**
	 * 通过月查询企业使用数量，企业下属盒子发送数量
	 * @param month
	 * 		月份
	 * @param entId
	 * 		企业id
	 * @return
	 */
	public ObjectResult getByMonth(String month,long entId);
	
	/**
	 * 通过时间区间统计企业用户,分页显示
	 * @param startDate
	 * @param endDate
	 * @param endId
	 * @return
	 */
	public List<EntStatDailyDto> statisticData(Calendar startDate,Calendar endDate,Long endId,int page,int pageSize);
	
	public int count(Calendar startDate, Calendar endDate,Long entId);
	
}

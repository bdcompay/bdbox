package com.bdbox.service;

import java.util.List;

import com.bdbox.api.dto.PhoneRestrictDto;
import com.bdbox.entity.PhoneRestrict;

public interface PhoneRestrictService {

	/**
	 * 保存数据
	 * @param phoneRestrict
	 */
	public void save(PhoneRestrict phoneRestrict);
	
	/**
	 * 删除数据
	 * @param id
	 */
	public void delete(Long id);
	
	/**
	 * 更新数据
	 * @param phoneRestrict
	 */
	public void update(PhoneRestrict phoneRestrict);
	
	/**
	 * 根据手机号码获取数据
	 * @param phoneNum
	 * @return
	 */
	public PhoneRestrict getPhone(String phoneNum);
	
	/**
	 * 获取所有数据
	 * @return
	 */
	public List<PhoneRestrictDto> getAll(int page, int pageSize);
	
	/**
	 * 统计
	 * @return
	 */
	public int count();
}

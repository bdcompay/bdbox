package com.bdbox.service;

import java.util.Calendar;
import java.util.List;

import net.sf.json.JSONObject;

import com.bdbox.api.dto.SOSLocationDto;
import com.bdbox.entity.SOSLocation;
import com.bdsdk.json.ObjectResult;

public interface SOSLocationService {

	/**
	 * 添加
	 * @param sosLocation
	 */
	public void save(SOSLocation sosLocation);
	
	/**
	 * 更新
	 * @param sosLocation
	 */
	public void update(SOSLocation sosLocation);
	
	/**
	 * 删除
	 * @param id
	 */
	public void delete(Long id);
	
	/**
	 * 根据ID获取位置信息
	 * @param id
	 * @return
	 */
	public SOSLocationDto getLocation(Long id);
	
	/**
	 * 获取所有的位置信息
	 * @return
	 */
	public List<SOSLocationDto> getList();
	
	/**
	 * 获取所有的位置信息
	 * @return
	 */
	public List<SOSLocationDto> getList(String cardId);
	
	/**
	 * 根据盒子ID获取位置信息
	 * @param boxId
	 * @return
	 */
	public ObjectResult getLocationForBoxId(String cardNum);
	
	/**
	 * 根据盒子ID获取位置信息
	 * @param boxId
	 * @return
	 */
	public SOSLocation getLocationByBoxId(Long boxId);
	/**
	 * 保存或者更新
	 * @param sosLocation
	 */
	public void saveOrUpdate(SOSLocation sosLocation);
	
	/**
	 * 获取企业用户所有下属盒子的SOS位置信息
	 * @param json
	 * @return
	 */
	public ObjectResult getSosLocation(JSONObject json);
	
	/**
	 * 企业用户分页获取sos位置信息
	 * @param json
	 * @param startTime
	 * @param endTime
	 * @param page
	 * @return
	 */
	public ObjectResult getSosLocations(JSONObject json, Calendar startTime, Calendar endTime);
}

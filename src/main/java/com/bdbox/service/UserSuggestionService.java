package com.bdbox.service;

import com.bdbox.web.dto.UserSuggestionDto;
import java.util.List;

public abstract interface UserSuggestionService
{
  public abstract List<UserSuggestionDto> listUserSuggestions(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2);

  public abstract int listUserSuggestionsCount(String paramString);

  public abstract void deleteUserSuggestion(long paramLong);
}
package com.bdbox.service;

import java.util.List;

import com.bdbox.entity.Invoice;
import com.bdbox.web.dto.InvoiceDto;
import com.bdsdk.json.ObjectResult;

public interface InvoiceService {

	/**
	 * 保存发票信息
	 * @param invoice
	 */
	public ObjectResult saveInvoice(Long id, String companyName, String taxpayerIdNum, String registerAddr,
			String registerPhone, String bankName, String bankAccount, String ticketsInfo, String ticketsPhone,
			String ticketsAddr, String detailAddr, String invoiceType, Long user);
	
	/**
	 * 更新发票信息
	 * @param invoice
	 */
	public void updateInvoice(Invoice invoice);
	
	/**
	 * 删除发票信息
	 * @param id
	 */
	public ObjectResult deleteInvoice(Long id);
	
	/**
	 * 获取用户所有的发票
	 * @return user
	 */
	public List<InvoiceDto> getAllInvoice(Long user);
	
	/**
	 * 获取发票信息(dto)
	 * @param id
	 * @return
	 */
	public InvoiceDto getInvoiceDto(Long id);
	
	/**
	 * 获取发票信息
	 * @param id
	 * @return
	 */
	public Invoice getInvoice(Long id);
	
	/**
	 * 根据用户id获取发票信息
	 * @param userId
	 * @return
	 */
	public InvoiceDto getCheckStatus(Long userId);
	
	/**
	 * 设置为默认发票
	 * @param userId
	 * @return
	 */ 
	public boolean updatemoren(Invoice invoice);
	
	/**
	 * 获取用户所有的发票
	 * @return user
	 */
	public Invoice getAllInvoices(Long user);
}

package com.bdbox.service;

import java.util.List;

import com.bdbox.entity.RegisterInformation;
import com.bdbox.entity.User;
import com.bdbox.entity.UserPhotoUrl;

 
public interface RegisterInformationService {
	public  RegisterInformation  getRealNameUrl(long userid,String type);
	
	public boolean updates(RegisterInformation registerInformation );
	
	public RegisterInformation saves(RegisterInformation registerInformation );

	public List<RegisterInformation> getRegisterInformation(int page, int pageSize);
	
	public Integer getRegisterInformationCount();
	
	public boolean sendinfomaton(long id,String telephone,String name,String number);
	
	public List<RegisterInformation> getRegisterInformation();
	
	public void delete(long id);





}

package com.bdbox.service;

import java.util.Calendar;
import java.util.List;

import com.bdbox.api.dto.BDCoodDto;
import com.bdbox.entity.BDCood;
import com.bdsdk.json.ObjectResult;
   
public interface BDCoodService {

	public List<BDCood> listBDcood(String bdnumber,String ordernumber,String userpeople,String refereespeople,String isuser,  Calendar startTime,Calendar endTime,Calendar creantTime,String refereespeoplename,String bdcoodtype, int page, int pageSize);
	
	public int count(String bdnumber,String ordernumber,String userpeople,String refereespeople,String isuser,  Calendar startTime,Calendar endTime,Calendar creantTime,String refereespeoplename,String bdcoodtype);
	
	public BDCoodDto castFrom(BDCood bdcood);
	
	public ObjectResult saveBdCood(String BDtype,String saveBuyStartTimeStr,String saveBuyEndTimeStr,String count,String price,String tuijianren,String remarkname,int dayNum,Double discount,String createCondition);

	public Integer getRefeesUserCount_wl(String username);
	
	public Integer getRefeesUserCount_wl1(String username);
	
	public Integer getRefeesUserCount_wl2(String username);
	
	public Integer getRefeesUserCount_wl3(String username); 
	
	public Integer getRefeesUserCount_wl4(String username);

	public BDCood getRefeesUser_wl5(String username);
	
	public BDCood getbdcood(Long id);
	
	public boolean update(BDCood bdcood);
	
	public BDCood getbdcood_wl(String bdcood);
	
	public List<BDCood> queryBdcood();






}

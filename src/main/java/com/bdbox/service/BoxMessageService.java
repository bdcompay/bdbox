package com.bdbox.service;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import com.bdbox.api.dto.BoxMessageDto;
import com.bdbox.constant.MsgIoType;
import com.bdbox.constant.MsgType;
import com.bdbox.entity.BoxMessage;
import com.bdsdk.constant.DataStatusType;
import com.bdsdk.json.ObjectResult;

public interface BoxMessageService {
	public void saveBoxMessage(BoxMessage boxMessage);

	public void updateBoxMessage(BoxMessage boxMessage);

	public List<BoxMessage> getUnReadBoxMessage(Long boxId, Calendar lastTime);
	
	public List<BoxMessage> getLastRevieveBoxMessages(Long boxId,MsgType msgType,Integer page,Integer pageSize);
	/**
	 * 查询
	 * @param boxId
	 * @param lastTime
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<BoxMessageDto> listBoxMessage(Long fromBoxId,Long toBoxId,String boxName,long userId, String order,Integer page,Integer pageSize);
	
	/**
	 * 查询数量
	 * @return
	 */
	public Integer queryCount(Long fromBoxId,Long toBoxId,String boxName,long userId);
	
	/**
	 * 查询
	 * @param msgId
	 * @param msgType
	 * @param msgIoType
	 * @param dataStatusType
	 * @param familyMob
	 * @param fromBoxId
	 * @param toBoxId
	 * @param familyMail
	 * @param startTime
	 * @param endTime
	 * @param order
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<BoxMessageDto> listBoxMessage(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxId, Long toBoxId, String familyMail,
			Calendar startTime, Calendar endTime,String boxName,long userId,String entUserName,
			String order, int page,int pageSize);
	
	
	/**
	 * 查询数量
	 * @param msgId
	 * @param msgType
	 * @param msgIoType
	 * @param dataStatusType
	 * @param fromUserId
	 * @param fromBoxId
	 * @param toBoxId
	 * @param toUserId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public Integer queryCount(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxId, Long toBoxId,String familyMail,
			Calendar startTime, Calendar endTime,String boxName,long userId,String entUserName);
	
	public void deleteBoxMessage(long id);
	/**
	 * 
	 * @param boxId
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<BoxMessage> getLastRevieveBoxMessages(Long boxId,Integer page,Integer pageSize);
	
	/**
	 * 统计用户消息
	 * @param userid
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public Map<String,Integer> statisticsUserMessage(long userid,Calendar startTime,Calendar endTime);
	
	/**
	 * 获取用户下发信息
	 * @param phoneNum
	 * @return
	 */
	public ObjectResult getEntUserMessage(String phoneNum);
	
	/**
	 * 获取用户接收的信息
	 * @param phoneNum
	 * @return
	 */
	public ObjectResult getReceiveMessage(JSONObject json);
	
	/**
	 * 根据关键字查找聊天内容
	 * @param phoneNum
	 * @param keyWord
	 * @param page
	 * @return
	 */
	public ObjectResult getEntUserMessage(String phoneNum, String keyWord);
	
	/**
	 * 查找聊天记录
	 * @param phoneNum
	 * @param boxSerialNumber
	 * @return
	 */
	public ObjectResult getBoxChatLog(String phoneNum, String boxSerialNumber);

}

package com.bdbox.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.consts.GlobalVar;
import com.bdbox.dao.EntUserPushMessageDao;
import com.bdbox.dao.EnterpriseUserDao;
import com.bdbox.entity.EntUserPushMessage;
import com.bdbox.entity.EnterpriseUser;
import com.bdbox.service.EntUserPushMessageService;
import com.bdbox.web.dto.EntUserPushMessageDto;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Service
public class EntUserPushMessageServiceImpl implements EntUserPushMessageService {
	@Autowired
	private EntUserPushMessageDao dao;
	@Autowired
	private EnterpriseUserDao entDao;

	@Override
	public void save(EntUserPushMessage entUserPushMessage) {
		dao.save(entUserPushMessage);
	}

	@Override
	public void update(EntUserPushMessage entUserPushMessage) {
		dao.update(entUserPushMessage);
	}

	@Override
	public EntUserPushMessage get(Long id) {
		return dao.get(id);
	}

	@Override
	public List<EntUserPushMessage> getUnsend() {
		return dao.query(null, null, GlobalVar.ENTERPRISE_USER_PUSH_MESSAGE_COUNT, false, null, null, 1, 9999);
	}

	@Override
	public List<EntUserPushMessageDto> getDtos(String entUser, String startTime, String endTime, Integer count,
			Boolean isSuccess, int page, int pageSize) {
		EnterpriseUser enterpriseUser=null;
		if(entUser!=null && !"".equals(entUser)){
			enterpriseUser=entDao.query(entUser);
		}
		Long entUserId=null;
		if(enterpriseUser!=null){
			entUserId=enterpriseUser.getId();
		}
		Calendar sTime=Calendar.getInstance();
		Calendar eTime=Calendar.getInstance();
		try {
			sTime = CommonMethod.StringToCalendar(startTime, "yyyy-MM-dd HH:mm:ss");
			eTime = CommonMethod.StringToCalendar(endTime, "yyyy-MM-dd HH:mm:ss");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		List<EntUserPushMessageDto> dtos=new ArrayList<>(0);
		List<EntUserPushMessage> list=dao.query(entUserId, null, count, isSuccess, sTime, eTime, 
				page, pageSize);
		if(list!=null){
			for(EntUserPushMessage e:list){
				dtos.add(case2Dto(e));
			}
		}
		return dtos;
	}

	@Override
	public int amount(String entUser, String startTime, String endTime, Integer count, Boolean isSuccess) {
		EnterpriseUser enterpriseUser=null;
		if(entUser!=null && !"".equals(entUser)){
			enterpriseUser=entDao.query(entUser);
		}
		Long entUserId=null;
		if(enterpriseUser!=null){
			entUserId=enterpriseUser.getId();
		}
		Calendar sTime=Calendar.getInstance();
		Calendar eTime=Calendar.getInstance();
		try {
			sTime = CommonMethod.StringToCalendar(startTime, "yyyy-MM-dd HH:mm:ss");
			eTime = CommonMethod.StringToCalendar(endTime, "yyyy-MM-dd HH:mm:ss");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		return dao.amount(entUserId, null, count, isSuccess, sTime, eTime);
	}

	private EntUserPushMessageDto case2Dto(EntUserPushMessage e){
		EntUserPushMessageDto dto=new EntUserPushMessageDto();
		BeansUtil.copyBean(dto, e, true);
		if(e.getEnterpriseUser()!=null){
			dto.setEntUser(e.getEnterpriseUser().getPhone());
		}
		if(e.getCreateTime()!=null){
			dto.setCreateTime(CommonMethod.CalendarToString(e.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
		}
		if(e.getBoxMessage()!=null){
			dto.setContent(e.getBoxMessage().getContent());
		}
		return dto;
	}
}

package com.bdbox.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.dao.RecipientsDao;
import com.bdbox.entity.Recipients;
import com.bdbox.service.RecipientsService;

@Service
public class RecipientsServiceImpl implements RecipientsService{
	
	@Autowired
	private RecipientsDao recipientsDao;

	@Override
	public void updateDefault(Long id) {
		recipientsDao.updateDefault(id);
	}

	@Override
	public void doSetZero() {
		recipientsDao.setZero();
	}

	@Override
	public List<Recipients> getList(Integer page, Integer pageSize) {
		return recipientsDao.list(page, pageSize);
	}

	@Override
	public int count() {
		return Integer.parseInt(String.valueOf(recipientsDao.count()));
	}

	@Override
	public Recipients getRecipients() {
		return recipientsDao.getRecipients();
	}

	@Override
	public void save(Recipients recipients) {
		recipientsDao.save(recipients);
	}

	@Override
	public void delete(Long id) {
		recipientsDao.delete(id);
	}

	@Override
	public void update(Recipients recipients) {
		recipientsDao.update(recipients);
	}

	@Override
	public Recipients getRecipientsById(Long id) {
		return recipientsDao.get(id);
	}

}

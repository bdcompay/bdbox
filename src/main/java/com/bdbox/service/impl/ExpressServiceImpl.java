package com.bdbox.service.impl;

import com.bdbox.constant.DealStatus;
import com.bdbox.constant.ExpressStatus;
import com.bdbox.dao.ExpressDao;
import com.bdbox.dao.InvoiceDao;
import com.bdbox.dao.NotificationDao;
import com.bdbox.dao.OrderDao;
import com.bdbox.dao.ReturnsDao;
import com.bdbox.entity.Express;
import com.bdbox.entity.Invoice;
import com.bdbox.entity.Notification;
import com.bdbox.entity.Order;
import com.bdbox.entity.Returns;
import com.bdbox.express.shunfeng.api.SFHandler;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.ExpressService;
import com.bdbox.service.OrderService;
import com.bdbox.util.LogUtils;
import com.bdbox.web.dto.ExpressDto;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;
import com.sf.openapi.express.sample.route.dto.RouteRespDto;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * 物流ServiceImpl
 * @author WF
 *
 */
@Service
public class ExpressServiceImpl implements ExpressService,ApplicationContextAware{
	 
	@Autowired
	private ExpressDao expressDao;
	
	@Autowired
	private ReturnsDao returnsDao;
	
	@Autowired
	private OrderDao orderDao;
	
	private ApplicationContext applicationContext; 
	@Autowired
	private NotificationDao notificationDao;
	@Autowired
	private InvoiceDao invoiceDao;
	@Autowired  
	private MailNotification mailNotification; 
	@Override
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		// TODO Auto-generated method stub
		this.applicationContext=arg0;
	}
	@Autowired private OrderService orderService;
	
	public void saveExpress(Express express) {
		// TODO Auto-generated method stub
		expressDao.save(express);
	}

	public void updateExpress(Express express) {
		// TODO Auto-generated method stub
		expressDao.update(express);
	}

	public void deleteExpress(Long user) {
		// TODO Auto-generated method stub
		expressDao.delete(user);
	}

	public Express queryExpress(Long user) {
		// TODO Auto-generated method stub
		return expressDao.get(user);
	}

	@Override
	public List<ExpressDto> listExpress(String expressName,
			String expressNumber, Calendar sendTime, Calendar completionTime,
			ExpressStatus expressStatus, Integer page, Integer pageSize,
			String order) {
		// TODO Auto-generated method stub
		List<Express> list = expressDao.listExpress(expressName, expressNumber, sendTime, completionTime, expressStatus, page, pageSize, order);
		if(list.size()>0){
			List<ExpressDto> dtos = new ArrayList<ExpressDto>();
			for (Express entity : list) {
				dtos.add(castUserToUserDto(entity));
			}
			return dtos;
		}
		return null;
	}

	@Override
	public int listExpressCount(String expressName, String expressNumber,
			Calendar sendTime, Calendar completionTime,
			ExpressStatus expressStatus) {
		// TODO Auto-generated method stub
		return expressDao.listExpressCount(expressName, expressNumber, sendTime, completionTime, expressStatus);
	}

	@Override
	public ExpressDto queryExpressDto(Long oid) {
		// TODO Auto-generated method stub
		return castUserToUserDto(expressDao.queryExpressDto(oid));
	}

	/**
	 * 将实体转换成Dto
	 * @param user
	 * @return
	 */
	private ExpressDto castUserToUserDto(Express entity) {
		ExpressDto dto = new ExpressDto();
		//将实体属性拷贝到Dto中，属性名必须相同
		BeansUtil.copyBean(dto, entity, true);
		//将Calendar类型转换为String类型斌赋值到Dto属性中
		if(entity!=null) {
			if (entity.getCompletionTime() != null) {
				dto.setCompletionTime(CommonMethod.CalendarToString(entity.getCompletionTime(), "yyyy/MM/dd HH:mm"));
			}
			dto.setSendTime(CommonMethod.CalendarToString(entity.getSendTime(), "yyyy/MM/dd HH:mm"));
			if (entity.getExpressStatus() != null) {
				dto.setExpressStatus(ExpressStatus.switchStatus3(entity.getExpressStatus()));
			}
		}
		return dto;
	}

	@Override
	public List<Express> listAllSendExpress() {
		// TODO Auto-generated method stub
		return expressDao.listAllSendExpress();
	}

	@Override
	public ExpressDto queryExpressDtoById(Long id) {
		Express express =expressDao.get(id);
		return  castUserToUserDto(express);
	}
	
 	public Express queryExpressEnty(Long id){
		Express express =expressDao.queryExpressDto(id);
		System.out.println(express.getExpressNumber());
		return express;
	}

	
	@Override
	public void doExpressApi(Express express){
		
		//kuaidiAPI(express);
	    //aikuaidi(express); 
		kuai100(express);
		
	}
	
	
	//快递100物流方法
	public void kuai100(Express express){
		//物流单号
		String num = express.getExpressNumber();
		
		try {
			//请求地址
			InputStream in= applicationContext.getResource("http://www.kuaidi100.com/query?type="+express.getExpressName()+"&postid="+num+"&id=19&valicode=&temp=0.400680293431549").getInputStream();
			//转码
			BufferedReader br = new BufferedReader(new InputStreamReader(in,"utf-8"));
			String result = null;//结果
			String data = "";//json数据
			while((result=br.readLine())!=null)data += result;
			//解析json
			JSONObject object = JSONObject.fromObject(data);
			String expressStr = "";
			if(object.get("data")!=null){
				expressStr = object.get("data").toString().replace("context", "content");
				JSONArray jsonArray = JSONArray.fromObject(expressStr);
				System.out.println("jsonArray-----"+jsonArray);
				//存取最新物流信息
				express.setExpressMessage("express:"+jsonArray.toString());
				DealStatus dealStatus = null; 

				//判断返回结果是否有物流
				if(object.get("message").equals("ok")){
					//判断物流状态
					if(object.get("status").equals("200")){
						//判断当前物流状态
						String state = (String) object.get("state");
						if(state.equals("0")){
							express.setExpressStatus(ExpressStatus.PASSAGE);
							dealStatus = DealStatus.SENT; 
	 						//order.setDealStatus(DealStatus.SENT);//已发货
						}else if(state.equals("1")){
							express.setExpressStatus(ExpressStatus.TOOK);
							dealStatus = DealStatus.SENT; 
							//order.setDealStatus(DealStatus.SENT);//已发货
						}else if(state.equals("2")){
							express.setExpressStatus(ExpressStatus.KNOTTY);
							dealStatus = DealStatus.ABNORMAL; 
							//order.setDealStatus(DealStatus.ABNORMAL);//物流异常
						}else if(state.equals("3")){
							express.setExpressStatus(ExpressStatus.SIGN);
							dealStatus = DealStatus.DEALCLOSE; 
							//order.setDealStatus(DealStatus.DEALCLOSE);//交易成功
						}else if(state.equals("4")){
							express.setExpressStatus(ExpressStatus.OUT);
							dealStatus = DealStatus.ABNORMAL; 
							//order.setDealStatus(DealStatus.ABNORMAL);//物流异常
						}else if(state.equals("5")){
							express.setExpressStatus(ExpressStatus.DELIVERY);
							dealStatus = DealStatus.SENT; 
							//order.setDealStatus(DealStatus.SENT);//已发货
						}else if(state.equals("6")){
							express.setExpressStatus(ExpressStatus.BACK);
							dealStatus = DealStatus.ABNORMAL; 
							//order.setDealStatus(DealStatus.ABNORMAL);//物流异常
						}
						/*****************操作数据库*****************/
						expressDao.update(express);//更改状态
						
						Order orderenty=null; 
						SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );  
	  					if(express.getOrderId()!=null){
	  						orderenty = orderService.getOrder(express.getOrderId()); 
							if(orderenty!=null&&orderenty.getTransactionType().equals("RENT")&&dealStatus.equals(DealStatus.DEALCLOSE)){					 
									JSONObject info=jsonArray.getJSONObject(0);
									String signTime=info.getString("time");
									System.out.println(signTime);
									Date dateExecute = sdf.parse(signTime); 
									Calendar calendar = Calendar.getInstance(); 
									calendar.setTime(dateExecute); 
									calendar.set(Calendar.DATE, calendar.get(Calendar.DATE)+1);  
									orderenty.setBeginTime(calendar);
									Calendar calendarsing = Calendar.getInstance(); 
									calendarsing.setTime(dateExecute);
									
									//客户签收订单后通知管理员
						 			StringBuffer content=new StringBuffer(); 
									String type="BUYSIGN";  //通知类型
									if(orderenty.getTransactionType().equals("RENT")){
										type="RENTSIGN";
									}
									String type_twl="admin"; 
									String invoices="";            //是否开发票
									String tomail=null;            //收信箱
									String subject=null;           //主题
									String mailcentent=null;       //内容
									Notification notification=notificationDao.getnotification(type,type_twl);
									if(notification!=null){ 
										if(notification.getIsemail().equals(true)){  
												tomail=notification.getTomail();
												subject=notification.getSubject();
												if(!orderenty.getTransactionType().equals("RENT")){  
													if(orderenty.getInvoice()!=0){
														Invoice enty=invoiceDao.getInvoice(orderenty.getInvoice());
														invoices="，发票抬头："+enty.getCompanyName();
													}
												}
												content.append(notification.getCenter()+":").append("订单号："+orderenty.getOrderNo()+",签收时间："+CommonMethod.CalendarToString(calendarsing, "yyyy/MM/dd HH:mm:ss")+"，下单数量："+orderenty.getCount()+"，收货地址："+orderenty.getSite()+"联系人："+orderenty.getName()+"，联系方式："+orderenty.getPhone()).append(invoices);
												if(content!=null){
													mailcentent=content.toString(); 
												}
												String [] email=tomail.split(",");
					    						for(String str:email){
					    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
					    						}
	 										//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
										}
									} 
									
							}else{
								JSONObject info=jsonArray.getJSONObject(0);
								String signTime=info.getString("time");
								System.out.println(signTime);
								Date dateExecute = sdf.parse(signTime); 
								Calendar calendar = Calendar.getInstance(); 
								calendar.setTime(dateExecute); 
								calendar.set(Calendar.DATE, calendar.get(Calendar.DATE));  
	 							Calendar calendarsing = Calendar.getInstance(); 
								calendarsing.setTime(dateExecute);
								
								//客户签收订单后通知管理员
					 			StringBuffer content=new StringBuffer(); 
								String type="BUYSIGN";  //通知类型
								if(orderenty.getTransactionType().equals("RENT")){
									type="RENTSIGN";
								}
								String type_twl="admin"; 
								String invoices="";            //是否开发票
								String tomail=null;            //收信箱
								String subject=null;           //主题
								String mailcentent=null;       //内容
								Notification notification=notificationDao.getnotification(type,type_twl);
								if(notification!=null){ 
									if(notification.getIsemail().equals(true)){  
											tomail=notification.getTomail();
											subject=notification.getSubject();
											if(!orderenty.getTransactionType().equals("RENT")){  
												if(orderenty.getInvoice()!=0){
													Invoice enty=invoiceDao.getInvoice(orderenty.getInvoice());
													invoices="，发票抬头："+enty.getCompanyName();
												}
											}
											content.append(notification.getCenter()+":").append("订单号："+orderenty.getOrderNo()+",签收时间："+CommonMethod.CalendarToString(calendarsing, "yyyy/MM/dd HH:mm:ss")+"，下单数量："+orderenty.getCount()+"，收货地址："+orderenty.getSite()+"联系人："+orderenty.getName()+"，联系方式："+orderenty.getPhone()).append(invoices);
											if(content!=null){
												mailcentent=content.toString(); 
											}
											String [] email=tomail.split(",");
				    						for(String str:email){
				    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
				    						}
	 									//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
									}
								} 
							}
						}else{
							Returns returns=returnsDao.queryReturns(express.getId()); 
							if(returns!=null&&dealStatus.equals(DealStatus.DEALCLOSE)&&returns.getReturnType().equals("RENT")){
								JSONObject info=jsonArray.getJSONObject(jsonArray.size()-1);
								String TookTime=info.getString("time");
								System.out.println(TookTime); 
								orderenty=orderService.getOrder(returns.getOrdreId());
								Date dateExecute = sdf.parse(TookTime); 
								Calendar calendar = Calendar.getInstance(); 
								calendar.setTime(dateExecute); 
								calendar.set(Calendar.DATE, calendar.get(Calendar.DATE)-1); 
								orderenty.setEndTime(calendar);
								returns.setEndTime(calendar);  
								returnsDao.update(returns); 
							}else{
								orderenty=orderService.getOrder(returns.getOrdreId()); 
							}
						}
	  					orderenty.setDealStatus(dealStatus);
						orderService.updateOrder(orderenty);//更改状态 
	 					/*****************操作数据库*****************/
					}else{
						//order.setDealStatus(DealStatus.SENT);//已发货
						dealStatus = DealStatus.SENT;  
						express.setExpressStatus(ExpressStatus.SENT);//已发货
						/*****************操作数据库*****************/ 
						Order orderenty=null; 
						SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );  
	  					if(express.getOrderId()!=null){
	  						orderenty = orderService.getOrder(express.getOrderId()); 
							if(orderenty!=null&&orderenty.getTransactionType().equals("RENT")&&dealStatus.equals(DealStatus.DEALCLOSE)){					 
									JSONObject info=jsonArray.getJSONObject(0);
									String signTime=info.getString("time");
									System.out.println(signTime);
									Date dateExecute = sdf.parse(signTime); 
									Calendar calendar = Calendar.getInstance(); 
									calendar.setTime(dateExecute); 
									calendar.set(Calendar.DATE, calendar.get(Calendar.DATE)+1);  
									orderenty.setBeginTime(calendar);  
							}  
						}else{
							Returns returns=returnsDao.queryReturns(express.getId()); 
							if(returns!=null&&dealStatus.equals(DealStatus.DEALCLOSE)&&returns.getReturnType().equals("RENT")){
								JSONObject info=jsonArray.getJSONObject(jsonArray.size()-1);
								String TookTime=info.getString("time");
								System.out.println(TookTime); 
								orderenty=orderService.getOrder(returns.getOrdreId());
								Date dateExecute = sdf.parse(TookTime); 
								Calendar calendar = Calendar.getInstance(); 
								calendar.setTime(dateExecute); 
								calendar.set(Calendar.DATE, calendar.get(Calendar.DATE)-1); 
								orderenty.setEndTime(calendar);
								returns.setEndTime(calendar);  
								returnsDao.update(returns); 
							}else{
								orderenty=orderService.getOrder(returns.getOrdreId()); 
							}
						}
	  					orderenty.setDealStatus(dealStatus); 
						orderService.updateOrder(orderenty);//更改状态  
						expressDao.update(express);//更改状态  
						//orderService.updateOrder(order);//更改状态
						/*****************操作数据库*****************/
					}
				}else{
					System.out.println("===============================\n异常：物流单号："+express.getExpressNumber()+",物流异常\n===============================");
					System.out.println("异常信息："+object.get("message"));
				}
			}else{
				LogUtils.loginfo(object.get("message").toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("====================================================");
			System.out.println("物流状态任务调度异常");
			System.out.println("异常出现在物流信息id为："+express.getId());
			System.out.println("其它原因：数据库被插入不符参数");
			System.out.println("其它原因：由于使用的是网页抓取数据手段，非正常请求数据，可能访问地址出现变化导致的异常，请留意！");
			System.out.println("====================================================");
		}
	}
	
	//爱快递物流方法
	public void aikuaidi(Express express){
		//Express express=expressDao.queryExpressByexpressNo(nums); 
		try {
			String num = express.getExpressNumber(); 
			String expressName = express.getExpressName();
			//测试爱快递地址 http://www.aikuaidi.cn/rest/?key=9b86cde40f764b5cbb98a7ea16b2d895&order=932023959301&id=shunfeng&ord=asc&show=json
			//请求地址
			InputStream in= applicationContext.getResource("http://www.aikuaidi.cn/rest/?key=9b86cde40f764b5cbb98a7ea16b2d895&order="+num+"&id="+expressName+"&ord=desc&show=json").getInputStream();
			//转码
			BufferedReader br = new BufferedReader(new InputStreamReader(in,"utf-8"));
			String result = null;//结果
			String data = "";//json数据
			while((result=br.readLine())!=null)data += result;
			//解析json
			JSONObject object = JSONObject.fromObject(data);
			JSONArray jsonArray = JSONArray.fromObject(object.get("data"));
			//存取最新物流信息
			express.setExpressMessage("express:"+jsonArray.toString());
			//判断返回结果是否有物流
			if(String.valueOf(object.get("errCode")).equals("0")){
				//判断物流状态
				//if(String.valueOf(object.get("status")).equals("0")){
					//获取当前物流所属订单
					DealStatus dealStatus = null;
					/**
					 * 0：查询出错（即errCode!=0），
					 * 1：暂无记录，
					 * 2：在途中，
					 * 3：派送中，
					 * 4：已签收，
					 * 5：拒收，
					 * 6：疑难件
					 * 7：退回
					 */
					//判断当前物流状态
					String state = String.valueOf(object.get("status"));
					if(state.equals("2")){
						express.setExpressStatus(ExpressStatus.PASSAGE);
						//order.setDealStatus(DealStatus.SENT);//已发货
						dealStatus = DealStatus.SENT;
					}else if(state.equals("1")){
						express.setExpressStatus(ExpressStatus.TOOK);
						//order.setDealStatus(DealStatus.SENT);//已发货
						dealStatus = DealStatus.SENT;
					}else if(state.equals("6")){
						express.setExpressStatus(ExpressStatus.KNOTTY);
						//order.setDealStatus(DealStatus.ABNORMAL);//物流异常
						dealStatus = DealStatus.ABNORMAL;
					}else if(state.equals("4")){
						express.setExpressStatus(ExpressStatus.SIGN); 
						//order.setDealStatus(DealStatus.DEALCLOSE);//交易成功
						dealStatus = DealStatus.DEALCLOSE;
					}else if(state.equals("5")){
						express.setExpressStatus(ExpressStatus.OUT);
						//order.setDealStatus(DealStatus.ABNORMAL);//物流异常
						dealStatus = DealStatus.ABNORMAL;
					}else if(state.equals("3")){
						express.setExpressStatus(ExpressStatus.DELIVERY);
						//order.setDealStatus(DealStatus.SENT);//已发货
						dealStatus = DealStatus.SENT;
					}else if(state.equals("7")){
						express.setExpressStatus(ExpressStatus.BACK);
						//order.setDealStatus(DealStatus.ABNORMAL);//物流异常
						dealStatus = DealStatus.ABNORMAL;
					}
					/*****************操作数据库*****************/
					expressDao.update(express); //更改状态
					Order order=null; 
					SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );  
  					if(express.getOrderId()!=null){
						order = orderService.getOrder(express.getOrderId()); 
						if(order!=null&&order.getTransactionType().equals("RENT")&&dealStatus.equals(DealStatus.DEALCLOSE)){					 
								JSONObject info=jsonArray.getJSONObject(0);
								String signTime=info.getString("time");
								System.out.println(signTime);
								Date dateExecute = sdf.parse(signTime); 
								Calendar calendar = Calendar.getInstance(); 
								calendar.setTime(dateExecute); 
								calendar.set(Calendar.DATE, calendar.get(Calendar.DATE)+1);  
								order.setBeginTime(calendar); 
								
								
								Calendar calendarsing = Calendar.getInstance(); 
								calendarsing.setTime(dateExecute);
								
								//客户签收订单后通知管理员
					 			StringBuffer content=new StringBuffer(); 
								String type="BUYSIGN";  //通知类型
								if(order.getTransactionType().equals("RENT")){
									type="RENTSIGN";
								}
								String type_twl="admin"; 
								String invoices="";            //是否开发票
								String tomail=null;            //收信箱
								String subject=null;           //主题
								String mailcentent=null;       //内容
								Notification notification=notificationDao.getnotification(type,type_twl);
								if(notification!=null){ 
									if(notification.getIsemail().equals(true)){  
											tomail=notification.getTomail();
											subject=notification.getSubject();
											if(!order.getTransactionType().equals("RENT")){  
												if(order.getInvoice()!=0){
													Invoice enty=invoiceDao.getInvoice(order.getInvoice());
													invoices="，发票抬头："+enty.getCompanyName();
												}
											}
											content.append(notification.getCenter()+":").append("订单号："+order.getOrderNo()+",签收时间："+CommonMethod.CalendarToString(calendarsing, "yyyy/MM/dd HH:mm:ss")+"，下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，联系方式："+order.getPhone()).append(invoices);
											if(content!=null){
												mailcentent=content.toString(); 
											}
											String [] email=tomail.split(",");
				    						for(String str:email){
				    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
				    						}
 										//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
									}
								} 
						}else{
							JSONObject info=jsonArray.getJSONObject(0);
							String signTime=info.getString("time");
							System.out.println(signTime);
							Date dateExecute = sdf.parse(signTime); 
							Calendar calendar = Calendar.getInstance(); 
							calendar.setTime(dateExecute); 
							calendar.set(Calendar.DATE, calendar.get(Calendar.DATE)+1);  
 							Calendar calendarsing = Calendar.getInstance(); 
							calendarsing.setTime(dateExecute);
							
							//客户签收订单后通知管理员
				 			StringBuffer content=new StringBuffer(); 
							String type="BUYSIGN";  //通知类型
							if(order.getTransactionType().equals("RENT")){
								type="RENTSIGN";
							}
							String type_twl="admin"; 
							String invoices="";            //是否开发票
							String tomail=null;            //收信箱
							String subject=null;           //主题
							String mailcentent=null;       //内容
							Notification notification=notificationDao.getnotification(type,type_twl);
							if(notification!=null){ 
								if(notification.getIsemail().equals(true)){  
										tomail=notification.getTomail();
										subject=notification.getSubject();
										if(!order.getTransactionType().equals("RENT")){  
											if(order.getInvoice()!=0){
												Invoice enty=invoiceDao.getInvoice(order.getInvoice());
												invoices="，发票抬头："+enty.getCompanyName();
											}
										}
										content.append(notification.getCenter()+":").append("订单号："+order.getOrderNo()+",签收时间："+CommonMethod.CalendarToString(calendarsing, "yyyy/MM/dd HH:mm:ss")+"，下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，联系方式："+order.getPhone()).append(invoices);
										if(content!=null){
											mailcentent=content.toString(); 
										}
										String [] email=tomail.split(",");
			    						for(String str:email){
			    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
			    						}
 									//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
								}
							} 
						}
					}else{
						Returns returns=returnsDao.queryReturns(express.getId()); 
						if(returns!=null&&dealStatus.equals(DealStatus.DEALCLOSE)&&returns.getReturnType().equals("RENT")){
							JSONObject info=jsonArray.getJSONObject(jsonArray.size()-1);
							String TookTime=info.getString("time");
							System.out.println(TookTime); 
							order=orderService.getOrder(returns.getOrdreId());
							Date dateExecute = sdf.parse(TookTime); 
							Calendar calendar = Calendar.getInstance(); 
							calendar.setTime(dateExecute); 
							calendar.set(Calendar.DATE, calendar.get(Calendar.DATE)-1); 
							order.setEndTime(calendar);
							returns.setEndTime(calendar);  
							returnsDao.update(returns); 
						}else{
							order=orderService.getOrder(returns.getOrdreId()); 
						}
					}
					order.setDealStatus(dealStatus);
					orderService.updateOrder(order);//更改状态
					/*****************操作数据库*****************/
				/*}else{
					//获取当前物流所属订单
					if(express.getOrderId()!=null){
						Order order = orderService.getOrder(express.getOrderId());
						order.setDealStatus(DealStatus.SENT);//已发货
						orderService.updateOrder(order);//更改状态
					}
					
					
					*//*****************操作数据库*****************//*
					express.setExpressStatus(ExpressStatus.SENT);//已发货
					expressDao.update(express);//更改状态
					
					*//*****************操作数据库*****************//*
				}*/
			}else{
				System.out.println("===============================\n异常：物流id："+express.getId()+",物流号："+express.getExpressNumber()+",物流异常\n===============================");
				//System.out.println("===============================\n异常：订单号："+order.getOrderNo()+",物流异常\n===============================");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("====================================================");
			System.out.println("物流状态任务调度异常");
			System.out.println("异常出现在物流信息id为："+express.getId());
			System.out.println("其它原因：数据库被插入不符参数");
			System.out.println("其它原因：由于使用的是网页抓取数据手段，非正常请求数据，可能访问地址出现变化导致的异常，请留意！");
			System.out.println("====================================================");
		}
	}
	
	//快递api物流方法
	public void kuaidiAPI(Express express){
		try {
			String num = express.getExpressNumber().replaceAll(" ", "");
			String expressName = express.getExpressName().replaceAll(" ", "");
			//请求地址
			InputStream in= applicationContext.getResource("http://api.kuaidi.com/openapi.html?id=088c720a7b4c62f93528e45e34806f84&com="+expressName+"&nu="+num+"&show=0&muti=0&order=desc").getInputStream();
			//转码
			BufferedReader br = new BufferedReader(new InputStreamReader(in,"utf-8"));
			String result = null;//结果
			String data = "";//json数据
			while((result=br.readLine())!=null)data += result;
			//解析json
			JSONObject object = JSONObject.fromObject(data);
			String expressStr = "";
			if(object.get("data")!=null){
				expressStr = object.get("data").toString().replace("context", "content");
			}
			JSONArray jsonArray = JSONArray.fromObject(expressStr);
			//存取最新物流信息
			express.setExpressMessage("express:"+jsonArray.toString());
			boolean bl = (boolean) object.get("success");
			//判断返回结果是否有物流
			if(bl){
				//判断物流状态
				if(!String.valueOf(object.get("status")).equals("0")){
					//获取当前物流所属订单
					DealStatus dealStatus = null;
					//判断当前物流状态
					String state = String.valueOf(object.get("status"));
					if(state.equals("3")){
						express.setExpressStatus(ExpressStatus.PASSAGE);
						//order.setDealStatus(DealStatus.SENT);//已发货
						dealStatus = DealStatus.SENT;
					}else if(state.equals("4")){
						express.setExpressStatus(ExpressStatus.TOOK);
						//order.setDealStatus(DealStatus.SENT);//已发货
						dealStatus = DealStatus.SENT;
					}else if(state.equals("5")){
						express.setExpressStatus(ExpressStatus.KNOTTY);
						//order.setDealStatus(DealStatus.ABNORMAL);//物流异常
						dealStatus = DealStatus.ABNORMAL;
					}else if(state.equals("6")){
						express.setExpressStatus(ExpressStatus.SIGN); 
						//order.setDealStatus(DealStatus.DEALCLOSE);//交易成功
						dealStatus = DealStatus.DEALCLOSE;
					}else if(state.equals("5")){
						express.setExpressStatus(ExpressStatus.OUT);
						//order.setDealStatus(DealStatus.ABNORMAL);//物流异常
						dealStatus = DealStatus.ABNORMAL;
					}else if(state.equals("8")){
						express.setExpressStatus(ExpressStatus.DELIVERY);
						//order.setDealStatus(DealStatus.SENT);//已发货
						dealStatus = DealStatus.SENT;
					}else if(state.equals("7") || state.equals("9")){
						express.setExpressStatus(ExpressStatus.BACK);
						//order.setDealStatus(DealStatus.ABNORMAL);//物流异常
						dealStatus = DealStatus.ABNORMAL;
					}
					/*****************操作数据库*****************/
					expressDao.update(express); //更改状态
					Order order=null; 
					SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );  
  					if(express.getOrderId()!=null){
						order = orderService.getOrder(express.getOrderId()); 
						if(order!=null&&order.getTransactionType().equals("RENT")&&dealStatus.equals(DealStatus.DEALCLOSE)){					 
								JSONObject info=jsonArray.getJSONObject(0);
								String signTime=info.getString("time");
								System.out.println(signTime);
								Date dateExecute = sdf.parse(signTime); 
								Calendar calendar = Calendar.getInstance(); 
								calendar.setTime(dateExecute); 
								calendar.set(Calendar.DATE, calendar.get(Calendar.DATE)+1);  
								order.setBeginTime(calendar); 
								
								
								Calendar calendarsing = Calendar.getInstance(); 
								calendarsing.setTime(dateExecute);
								
								//客户签收订单后通知管理员
					 			StringBuffer content=new StringBuffer(); 
								String type="BUYSIGN";  //通知类型
								if(order.getTransactionType().equals("RENT")){
									type="RENTSIGN";
								}
								String type_twl="admin"; 
								String invoices="";            //是否开发票
								String tomail=null;            //收信箱
								String subject=null;           //主题
								String mailcentent=null;       //内容
								Notification notification=notificationDao.getnotification(type,type_twl);
								if(notification!=null){ 
									if(notification.getIsemail().equals(true)){  
											tomail=notification.getTomail();
											subject=notification.getSubject();
											if(!order.getTransactionType().equals("RENT")){  
												if(order.getInvoice()!=0){
													Invoice enty=invoiceDao.getInvoice(order.getInvoice());
													invoices="，发票抬头："+enty.getCompanyName();
												}
											}
											content.append(notification.getCenter()+":").append("订单号："+order.getOrderNo()+",签收时间："+CommonMethod.CalendarToString(calendarsing, "yyyy/MM/dd HH:mm:ss")+"，下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，联系方式："+order.getPhone()).append(invoices);
											if(content!=null){
												mailcentent=content.toString(); 
											}
											String [] email=tomail.split(",");
				    						for(String str:email){
				    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
				    						}
 										//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
									}
								} 
						}else{
							JSONObject info=jsonArray.getJSONObject(0);
							String signTime=info.getString("time");
							System.out.println(signTime);
							Date dateExecute = sdf.parse(signTime); 
							Calendar calendar = Calendar.getInstance(); 
							calendar.setTime(dateExecute); 
							calendar.set(Calendar.DATE, calendar.get(Calendar.DATE)+1);  
 							Calendar calendarsing = Calendar.getInstance(); 
							calendarsing.setTime(dateExecute);
							
							//客户签收订单后通知管理员
				 			StringBuffer content=new StringBuffer(); 
							String type="BUYSIGN";  //通知类型
							if(order.getTransactionType().equals("RENT")){
								type="RENTSIGN";
							}
							String type_twl="admin"; 
							String invoices="";            //是否开发票
							String tomail=null;            //收信箱
							String subject=null;           //主题
							String mailcentent=null;       //内容
							Notification notification=notificationDao.getnotification(type,type_twl);
							if(notification!=null){ 
								if(notification.getIsemail().equals(true)){  
										tomail=notification.getTomail();
										subject=notification.getSubject();
										if(!order.getTransactionType().equals("RENT")){  
											if(order.getInvoice()!=0){
												Invoice enty=invoiceDao.getInvoice(order.getInvoice());
												invoices="，发票抬头："+enty.getCompanyName();
											}
										}
										content.append(notification.getCenter()+":").append("订单号："+order.getOrderNo()+",签收时间："+CommonMethod.CalendarToString(calendarsing, "yyyy/MM/dd HH:mm:ss")+"，下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，联系方式："+order.getPhone()).append(invoices);
										if(content!=null){
											mailcentent=content.toString(); 
										}
										String [] email=tomail.split(",");
			    						for(String str:email){
			    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
			    						}
 									//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
								}
							} 
						}
					}else{
						Returns returns=returnsDao.queryReturns(express.getId()); 
						if(returns!=null&&dealStatus.equals(DealStatus.DEALCLOSE)&&returns.getReturnType().equals("RENT")){
							JSONObject info=jsonArray.getJSONObject(jsonArray.size()-1);
							String TookTime=info.getString("time");
							System.out.println(TookTime); 
							order=orderService.getOrder(returns.getOrdreId());
							Date dateExecute = sdf.parse(TookTime); 
							Calendar calendar = Calendar.getInstance(); 
							calendar.setTime(dateExecute); 
							calendar.set(Calendar.DATE, calendar.get(Calendar.DATE)-1); 
							order.setEndTime(calendar);
							returns.setEndTime(calendar);  
							returnsDao.update(returns); 
						}else{
							order=orderService.getOrder(returns.getOrdreId()); 
						}
					}
					order.setDealStatus(dealStatus);
					orderService.updateOrder(order);//更改状态
					/*****************操作数据库*****************/
				}else{
					LogUtils.loginfo("运单号：【"+num+"】当前状态号为："+object.get("status")+"，暂无记录、单号没有任何跟踪记录或出现异常");
				}
			}else{
				LogUtils.loginfo("===============================\n异常：物流id："+express.getId()+",物流号："+express.getExpressNumber()+",物流异常\n===============================");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("====================================================");
			System.out.println("物流状态任务调度异常");
			System.out.println("异常出现在物流信息id为："+express.getId());
			System.out.println("其它原因：数据库被插入不符参数");
			System.out.println("其它原因：由于使用的是网页抓取数据手段，非正常请求数据，可能访问地址出现变化导致的异常，请留意！");
			System.out.println("====================================================");
		}
	}
	
	/**
	 * 使用顺丰物流官方路由查询接口查询物流信息
	 * @param express
	 */
	public void doShunfeng(Express express){
		try {
			//获取路由信息
			List<RouteRespDto> rrds = SFHandler.selectRoute(express.getExpressNumber());
			//物流状态
			String status = "";
			//物流最后信息时间
			String time = "";
			//订单状态
			DealStatus dealStatus = null;
			if(rrds!=null){
				for (RouteRespDto rrd : rrds) {
					status = rrd.getOpcode();
					time = rrd.getAcceptTime();
				}
				//存储最新的物流信息
				express.setExpressMessage(SFHandler.formatStr(rrds));
				//判断物流状态
				/**
				 * 上门收件	50    	已收件
				 * 寄转第三方快递	627 	快件转寄XXX  单号OOO
			     * 到转第三方快递	626 	快件转寄XXX  单号OOO
				 * 问题件/滞留件	70    	由于%s, 派件不成功
				 * 				99    	快件已退回/转寄(单号为:755123456789)--注：原单退回
				 * 				631 	快件已退回(单号为:755123456789)--注：新单退回
				 * 				33    	快件派送不成功（原因：XXX），已送返 邵阳武冈服务点
				 * 				34    	正在派送途中,请您准备签收（派件人:姜超,电话:18173927687）
				 * 派件操作	80    	已签收,感谢使用顺丰,期待再次为您服务
				 * 输单操作	8000	在官网"运单资料&签收图", 可查看签收人信息
				 *
				 */
				if("31".equals(status) || "30".equals(status)){
					//途中
					express.setExpressStatus(ExpressStatus.PASSAGE);
					dealStatus = DealStatus.SENT;
				} else if("50".equals(status)) {
					//揽件
					express.setExpressStatus(ExpressStatus.TOOK);
					dealStatus = DealStatus.SENT;
				} else if("80".equals(status) || "8000".equals(status)) {
					//已签收
					express.setExpressStatus(ExpressStatus.SIGN);
					dealStatus = DealStatus.DEALCLOSE;
				} else if("34".equals(status)) {
					//派送中
					express.setExpressStatus(ExpressStatus.DELIVERY);
				} else if("70".equals(status)) {
					//疑难
					express.setExpressStatus(ExpressStatus.KNOTTY);
					dealStatus = DealStatus.ABNORMAL;
				} else if("99".equals(status) || "631".equals(status)) {
					//退签
					express.setExpressStatus(ExpressStatus.OUT);
					dealStatus = DealStatus.ABNORMAL;
				} else if("33".equals(status)) {
					//返回
					express.setExpressStatus(ExpressStatus.BACK);
					dealStatus = DealStatus.ABNORMAL;
				} else {
					//疑难
					express.setExpressStatus(ExpressStatus.KNOTTY);
					dealStatus = DealStatus.ABNORMAL;
				}
				//更新物流信息
				expressDao.update(express);
				Order order=null; 
				SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );  
				if(express.getOrderId()!=null){
					order = orderService.getOrder(express.getOrderId()); 
					if(order!=null&&order.getTransactionType().equals("RENT")
							&& dealStatus.equals(DealStatus.DEALCLOSE)){					 
							System.out.println(time);
							Date dateExecute = sdf.parse(time); 
							Calendar calendar = Calendar.getInstance(); 
							calendar.setTime(dateExecute); 
							calendar.set(Calendar.DATE, calendar.get(Calendar.DATE)+1);  
							order.setBeginTime(calendar);  
					}  
				}
				order.setDealStatus(dealStatus);
				orderService.updateOrder(order);//更改状态
			}
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.logerror("获取物流信息异常", e);
			System.out.println("===============================\n异常："
					+ "物流id："+express.getId()+","
					+ "物流号："+express.getExpressNumber()+",物流异常\n===============================");
		}

	}

	
	public static void main(String[] args){
		String s="{'message':'ok','nu':'933506240025','ischeck':'1','com':'shunfeng','status':'200','condition':'F00','state':'3','data':[{'time':'2016-01-19 10:06:47','context':'已签收,感谢使用顺丰,期待再次为您服务','ftime':'2016-01-19 10:06:47'},{'time':'2016-01-19 08:00:42','context':'正在派送途中,请您准备签收(派件人:许文保,电话:13924225036)','ftime':'2016-01-19 08:00:42'},{'time':'2016-01-19 02:49:41','context':'快件离开【广州新塘集散中心】,正发往 【广州黄埔开发大道营业部】','ftime':'2016-01-19 02:49:41'},{'time':'2016-01-18 21:43:13','context':'快件离开【广州萝岗南翔二路营业部】,正发往 【广州新塘集散中心】','ftime':'2016-01-18 21:43:13'}]}";
		String ss=s.replace("context", "content");
		System.out.println(ss);
		
	}

	/**
	 * 获取在顺丰官网下单的物流信息
	 */
	@Override
	public List<Express> getAllSelExpress() {
		return expressDao.getAllSelExpress();
	}

	@Override
	public void saveOrUpdate(Express paramExpress) {
		// TODO Auto-generated method stub
		expressDao.saveOrUpdate(paramExpress);
	}

	/**
	 * 根据订单号获取有电子运单路径的订单信息
	 */
	@Override
	public Express queryExpressByOrderId(Long orderId) {
		return expressDao.queryExpressByOrderId(orderId);
	} 
}

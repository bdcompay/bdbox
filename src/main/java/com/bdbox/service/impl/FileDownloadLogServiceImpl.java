package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.dao.FileDownloadLogDao;
import com.bdbox.entity.FileDownloadLog;
import com.bdbox.service.FileDownloadLogService;
import com.bdbox.util.LogUtils;
import com.bdbox.web.dto.FileDownloadLogDto;
import com.bdsdk.json.ListParam;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

/**
 * 文件下载记录业务实现类
 * 
 * @ClassName: FileDownloadLogServiceImpl 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-2-26 下午2:22:08 
 * @version V5.0 
 */
@Service
public class FileDownloadLogServiceImpl implements FileDownloadLogService {
	@Autowired
	private FileDownloadLogDao fileDownloadLogDao;

	/*
	 * (non-Javadoc)
	 * @see com.bdhezi.service.FileDownloadLogService#add(java.lang.String, java.lang.Boolean, long, java.lang.String, java.lang.String, java.lang.String, java.util.Calendar)
	 */
	@Override
	public FileDownloadLog add(String filename, Boolean status,long timeConsumig, String ip,
			String ipArea, String username, Calendar downloadTime) {
		try{
			FileDownloadLog fileDownloadLog = new FileDownloadLog();
			fileDownloadLog.setFilename(filename);
			fileDownloadLog.setStatus(status);
			fileDownloadLog.setTimeConsumig(timeConsumig);
			fileDownloadLog.setIp(ip);
			fileDownloadLog.setIpArea(ipArea);
			fileDownloadLog.setUsername(username);
			fileDownloadLog.setDownloadTime(downloadTime);
			return fileDownloadLogDao.save(fileDownloadLog);
		}catch(Exception e){
			LogUtils.logerror("添加下载文件记录（属性）发生错误", e);
		}
		return null;
		
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdhezi.service.FileDownloadLogService#add(com.bdhezi.web.entity.FileDownloadLog)
	 */
	@Override
	public FileDownloadLog add(FileDownloadLog fileDownloadLog) {
		try{
			return  fileDownloadLogDao.save(fileDownloadLog);
		}catch(Exception e){
			LogUtils.logerror("添加文件下载记录（对象）发生错误", e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.FileDownloadLogService#listFileDownloadLog(java.lang.String, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar, int, int)
	 */
	@Override
	public List<FileDownloadLogDto> listFileDownloadLog(String filename,
			Boolean status, String ip, String ipArea, String username,
			Calendar startTime, Calendar endTime, int page, int pageSize) {
		List<FileDownloadLogDto> dtos = new ArrayList<FileDownloadLogDto>();
		try{
			List<FileDownloadLog> fileDownloadLogs = fileDownloadLogDao.query(filename, status, ip, ipArea, username, startTime, endTime, page, pageSize);
			for(FileDownloadLog fileDownloadLog:fileDownloadLogs){
				dtos.add(castFrom(fileDownloadLog));
			}
		}catch(Exception e){
			LogUtils.logerror("按条件分页查询文件下载记录 发生错误", e);
		}
		return dtos;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.FileDownloadLogService#count(java.lang.String, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public int count(String filename, Boolean status, String ip, String ipArea,
			String username, Calendar startTime, Calendar endTime) {
		// TODO Auto-generated method stub
		try{
			return fileDownloadLogDao.count(filename, status, ip, ipArea, username, startTime, endTime);
		}catch(Exception e){
			LogUtils.logerror("按条件统计文件下载记录 发生错误", e);
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.FileDownloadLogService#fileDownloadLogs(java.lang.String, java.lang.Boolean, java.lang.String, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar, int, int)
	 */
	@Override
	public ListParam fileDownloadLogs(String filename, Boolean status,
			String ip, String ipArea, String username, Calendar startTime,
			Calendar endTime, int page, int pageSize) {
		// TODO Auto-generated method stub
		List<FileDownloadLogDto> dtos = new ArrayList<FileDownloadLogDto>();
		int count = 0;
		try{
			count = fileDownloadLogDao.count(filename, status, ip, ipArea, username, startTime, endTime);
			List<FileDownloadLog> fileDownloadLogs = fileDownloadLogDao.query(filename, status, ip, ipArea, username, startTime, endTime, page, pageSize);
			for(FileDownloadLog fileDownloadLog:fileDownloadLogs){
				dtos.add(castFrom(fileDownloadLog));
			}
			
		}catch(Exception e){
			LogUtils.logerror("按条件分页查询文件下载记录（jquery.datatable格式） 发生错误", e);
		}
		
		ListParam listParam = new ListParam();
		listParam.setAaData(dtos);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		return listParam;
	}
	
	/**
	 * 实体转dto
	 * 
	 * @param fileDownloadLog
	 * @return
	 */
	private FileDownloadLogDto castFrom(FileDownloadLog fileDownloadLog){
		FileDownloadLogDto dto = new FileDownloadLogDto();
		BeansUtil.copyBean(dto, fileDownloadLog, true);
		if(fileDownloadLog.getCreatedtime()!=null){
			dto.setCreatedtimeStr(CommonMethod.CalendarToString(fileDownloadLog.getCreatedtime(), "yyyy-MM-dd HH:mm:ss"));
		}
		if(fileDownloadLog.getDownloadTime()!=null){
			dto.setDownloadTimeStr(CommonMethod.CalendarToString(fileDownloadLog.getDownloadTime(), "yyyy-MM-dd HH:mm:ss"));
		}
		return dto;
	}

}

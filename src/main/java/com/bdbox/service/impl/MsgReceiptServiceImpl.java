package com.bdbox.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.dao.MsgReceiptDao;
import com.bdbox.entity.MsgReceipt;
import com.bdbox.service.MsgReceiptService;

@Service
public class MsgReceiptServiceImpl implements MsgReceiptService {
	@Autowired
	private MsgReceiptDao msgReceiptDao;

	public void saveMsgReceipt(MsgReceipt msgReceipt) {
		// TODO Auto-generated method stub
		msgReceiptDao.save(msgReceipt);
	}

}

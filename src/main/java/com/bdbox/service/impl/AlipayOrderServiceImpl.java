package com.bdbox.service.impl;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.constant.AilpayTradeStatus;
import com.bdbox.constant.DealStatus;
import com.bdbox.dao.AlipayOrderDao;
import com.bdbox.dao.InvoiceDao;
import com.bdbox.dao.NotificationDao;
import com.bdbox.dao.OrderDao;
import com.bdbox.entity.AlipayOrder;
import com.bdbox.entity.Invoice;
import com.bdbox.entity.Notification;
import com.bdbox.entity.Order;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.AlipayOrderService;
import com.bdsdk.util.CommonMethod;

@Service
public class AlipayOrderServiceImpl implements AlipayOrderService{
	
	@Autowired
	private AlipayOrderDao alipayOrderDao;
	
	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private InvoiceDao invoiceDao;  
	@Autowired  
	private MailNotification mailNotification; 
	@Autowired 
	private NotificationDao notificationDao;
	
	private static final String DATETYPE = "yyyy-MM-dd HH:mm:ss";
	private static String flag="";//标记发邮件的次数，每个支付订单只发一次


	@Override
	public void saveOrUpdate(Map<String, String> params) {
		AlipayOrder ao = new AlipayOrder();
		ao.setOutTradeNo(params.get("out_trade_no"));
		ao.setSubject(params.get("subject"));
		ao.setTotalFee(Double.parseDouble(params.get("total_fee")));
		ao.setTradeNo(params.get("trade_no"));
		ao.setSellerId(params.get("seller_id"));
		ao.setSellerEmail(params.get("seller_email"));
		ao.setBuyerId(params.get("burer_id"));
		ao.setBuyerEmail(params.get("buyer_email"));
		try {
			ao.setGmtCreate(CommonMethod.StringToCalendar(params.get("gmt_create"), DATETYPE));
			ao.setAilpayTradeStatus(AilpayTradeStatus.switchStatus(params.get("trade_status")));
			ao.setGmtPayment(CommonMethod.StringToCalendar(params.get("gmt_payment"), DATETYPE));
			ao.setGmtClose(CommonMethod.StringToCalendar(params.get("gmt_close"), DATETYPE));
			//保存支付宝订单
			alipayOrderDao.saveOrUpdate(ao);
			//根据订单号获取订单
			Order order = orderDao.getOrderstatus(params.get("out_trade_no"));
			order.setBuyTime(CommonMethod.StringToCalendar(params.get("gmt_payment"), DATETYPE));
			order.setTrade_no(params.get("trade_no"));
			//更新订单交易状态
			String status = String.valueOf(params.get("trade_status"));
			if("TRADE_CLOSED".equals(status)){
				order.setDealStatus(DealStatus.CANCELORDER);
			} else if ("TRADE_SUCCESS".equals(status)){
				order.setDealStatus(DealStatus.PAID);
				//订单支付成功后，给管理员发送邮件通知
				StringBuffer content=new StringBuffer(); 
				String type="BUYPAY";  //通知类型
				String type_twl="admin"; 
				String invoices="";            //是否开发票
				String tomail=null;            //收信箱
				String subject=null;           //主题
				String mailcentent=null;       //内容
				if(!order.getOrderNo().equals(flag)){ 
		    		flag=order.getOrderNo(); 
					if("RENT".equals(order.getTransactionType())){ 
			    		type="RENTPAY";
		    			Notification notification=notificationDao.getnotification(type,type_twl);
		    			if(notification!=null){ 
		    				if(notification.getIsemail().equals(true)){   
	    						tomail=notification.getTomail();
	    						subject=notification.getSubject();  
	    						content.append(notification.getCenter())
	    						.append("订单号："+order.getOrderNo()+","
	    						+ "下单时间："+CommonMethod.CalendarToString(order.getCreatedTime(), "yyyy-MM-dd HH:mm:ss")+"，"
	    						+ "下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，"
	    						+ "联系方式："+order.getPhone()).append(invoices);
	    						if(content!=null){
	    							mailcentent=content.toString(); 
	    						}
	    						String [] email=notification.getTomail().split(",");
	    						for(String str:email){
	    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
	    						}
		    				}
		    			}
		            }else{
		            	Notification notification=notificationDao.getnotification(type,type_twl);
		    			if(notification!=null){ 
		    				if(notification.getIsemail().equals(true)){  
	    						tomail=notification.getTomail();
	    						subject=notification.getSubject(); 
	    						if(order.getInvoice()!=0){
	    							Invoice enty=invoiceDao.getInvoice(order.getInvoice());
	    							invoices="，发票抬头："+enty.getCompanyName();
	    						}
	    						content.append(notification.getCenter()+":")
	    						.append("订单号："+order.getOrderNo()+","
	    						+ "下单时间："+CommonMethod.CalendarToString(order.getCreatedTime(), "yyyy-MM-dd HH:mm:ss")+"，"
	    						+ "下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，"
	    						+ "联系方式："+order.getPhone()).append(invoices);
	    						if(content!=null){
	    							mailcentent=content.toString(); 
	    						}
	    						String [] email=tomail.split(",");
	    						for(String str:email){
	    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
	    						}
		    				}
		    			} 
		            }
				}
			} else if ("TRADE_FINISHED".equals(status)){
				order.setDealStatus(DealStatus.DEALCLOSE);
				order.setCompleteTime(Calendar.getInstance());
			}
			//更新订单
			orderDao.update(order);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public AlipayOrder getOrderForTradeNo(String tradeNo) {
		return alipayOrderDao.getOrderForTradeNo(tradeNo);
	}

}

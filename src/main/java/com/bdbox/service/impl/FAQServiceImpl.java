package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service; 

import com.bdbox.api.dto.FAQDto;
import com.bdbox.constant.QuestionType;
import com.bdbox.dao.FAQDao;
import com.bdbox.dao.SmallTypeDao;
import com.bdbox.entity.FAQ;
import com.bdbox.entity.SmallType;
import com.bdbox.service.FAQService;
import com.bdbox.util.LogUtils;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;
 
@Service
public class FAQServiceImpl implements FAQService {
	@Autowired
	private FAQDao fAQDao;
	@Autowired
	private SmallTypeDao smallTypeDao;
	public List<FAQ> getlistFAQtable(String questiontype,int page, int pageSize){
		return fAQDao.getlistFAQtable(questiontype,page,pageSize); 
	}

	public Integer getlistFAQtable(String questiontype){
		return fAQDao.getlistFAQtable(questiontype); 
	} 
	
	public FAQ saveFaq(String questionname, String questionvlaue,String questiontype,String addsmalltype,String num){
		FAQ faq=new FAQ();
		QuestionType str=QuestionType.switchStatus(questiontype);
		if("".equals(questionname)){
			return faq;
		}
		if("".equals(questionvlaue)){
			return faq;
		}
		if("new".equals(num)){
			if("".equals(questiontype)){
				return faq;
			}
		}
		SmallType enty=smallTypeDao.gettype(null, addsmalltype);
		if(enty!=null){
			faq.setSmallTypename(enty.getName());
		}
		faq.setQuestion(questionname);
		faq.setAnswer(questionvlaue);
		faq.setQuestionType(str);
		faq.setSmallTypeid(addsmalltype);; 
		fAQDao.save(faq);
		return faq;
	}

	public FAQ getfaq(long id){
		return fAQDao.get(id);
	}
	
	public FAQDto castFrom(FAQ faq){
		FAQDto dto = new FAQDto();
		BeansUtil.copyBean(dto, faq, true);
		if(faq.getCreatTime()!=null){
			dto.setCreatTime(CommonMethod.CalendarToString(faq.getCreatTime(), "yyyy-MM-dd HH:mm:ss"));
 		}
		if(faq.getQuestionType()!=null&&faq.getQuestionType().equals(QuestionType.GENERALQUESTION)){
			dto.setQuestionType("常见问题"); 
		}else if(faq.getQuestionType()!=null&&faq.getQuestionType().equals(QuestionType.NEWGUIDE)){
			dto.setQuestionType("新手指南"); 
		}else if(faq.getQuestionType()!=null&&faq.getQuestionType().equals(QuestionType.OUTDOORS)){
			dto.setQuestionType("户外知识"); 
		}else if(faq.getQuestionType()!=null&&faq.getQuestionType().equals(QuestionType.NOUNEXPLAIN)){
			dto.setQuestionType("名词解释"); 
		}
		return dto;
	}
	
	public boolean update(Long id, String updatequestionname, String updatequestionvlaue,String updatequestiontype,String updatesmalltype){
		try {
			QuestionType str=QuestionType.switchStatus(updatequestiontype); 
			FAQ fAQ = fAQDao.get(id);
			fAQ.setQuestion(updatequestionname);
			fAQ.setAnswer(updatequestionvlaue);
			fAQ.setQuestionType(str); 
			fAQ.setSmallTypeid(updatesmalltype);
 			fAQDao.update(fAQ);
			LogUtils.loginfo("修改ID为：" + id + " 的问题");
			return true;
		} catch (Exception e) {
			LogUtils.logerror("修改ID为：" + id + " 的问题失败", e);
		}
		return false;
	}
	
	public boolean deleteFAQ(Long id){
		try {
			fAQDao.delete(id);
			LogUtils.loginfo("删除ID为：" + id + " 的问题成功");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.logerror("删除ID为：" + id + " 的问题失败", e);
		}
		return false;
	}

    //修改问题类型
	public boolean updates(String updatetype, String updatetype_twl,String updatetypename){
		boolean result=false;
		if(!"".equals(updatetype_twl)){
			 List<FAQ> getFAQlist=	fAQDao.getFAQlist(updatetype_twl);
			 if(!getFAQlist.isEmpty()){
				 for(FAQ faq:getFAQlist){
					 faq.setSmallTypename(updatetypename);
					 fAQDao.update(faq);
					 result=true;
				 }
			 }
		}
		return result;
	}
	
	public Map getFAQquestionlist(String lefttype,String keyword, String toptype,int page,int pageSize){
	   List<FAQ> FAQlist=fAQDao.getFAQquestionlist(lefttype,keyword,toptype,page,pageSize);
	   int count = fAQDao.counts(lefttype,keyword,toptype);
	  /* List dtos = new ArrayList();
	   if(!FAQlist.isEmpty()){
		   for(FAQ enty:FAQlist){
			   FAQDto dto=new FAQDto();
			   dto.setId(enty.getId());
			   dto.setAnswer(enty.getAnswer());
			   dto.setQuestion(enty.getQuestion());
			   dto.setSmallTypeid(enty.getSmallTypeid());
			   dto.setQuestionType(enty.getQuestionType().toString());
			   dtos.add(dto);
		   }
	   }*/
	   Map<String, Object> map = new HashMap<String, Object>();
		map.put("page", page);
		map.put("records", count);
		int totalpage = count/10;
		if(count%10!=0){
			totalpage+=1;
		}
		map.put("totalpage", totalpage);
		int displayStart = (page-1)*10+1;
		map.put("displayStart", displayStart);
		
		int displayEnd = page*10;
		if(count<displayEnd){
			displayEnd = count;
		}
		map.put("displayEnd", displayEnd);
		map.put("faq", FAQlist); 
	   return map;
   }  

}

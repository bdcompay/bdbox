package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.UserDto;
import com.bdbox.api.dto.UserInfoDto;
import com.bdbox.constant.UserStatusType;
import com.bdbox.constant.UserType;
import com.bdbox.control.ControlSms;
import com.bdbox.dao.MobCodeDao;
import com.bdbox.dao.UserDao;
import com.bdbox.dao.UserRegLogDao;
import com.bdbox.dao.UserSuggestionDao;
import com.bdbox.entity.MobCode;
import com.bdbox.entity.User;
import com.bdbox.entity.UserRegLog;
import com.bdbox.entity.UserSuggestion;
import com.bdbox.service.BoxFamilyService;
import com.bdbox.service.BoxPartnerService;
import com.bdbox.service.UserService;
import com.bdbox.util.IpUtil;
import com.bdbox.util.LogUtils;
import com.bdbox.util.MailUtil;
import com.bdbox.web.dto.ManageUserDto;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;
	@Autowired
	private UserSuggestionDao userSuggestionDao;
	@Autowired
	private ControlSms controlSms;
	@Autowired
	private MobCodeDao mobCodeDao;
	@Autowired
	private UserRegLogDao userRegLogDao;
	@Autowired
	private BoxFamilyService boxFamilyService;
	@Autowired
	private BoxPartnerService boxPartnerService;
	
	@Value("${system.code}")
	private String authorizationSystemCode;
	@Value("${email.emailSmtp}")
	private String emailSmtp;
	@Value("${email.emailUsername}")
	private String emailUsername;
	@Value("${email.emailPassword}")
	private String emailPassword;

	public User queryUser(UserType userType, String username, String password,
			UserStatusType userStatusType, String mail) {
		// TODO Auto-generated method stub
		if (password != null) {
			password = CommonMethod.getMD5(password);
		}
		User user = userDao.queryUser(userType, username, password,
				userStatusType, null, mail);
		return user;
	}

	public UserDto queryUser(String username) {
		User user = userDao.queryUser(null, username, null, null, null, null);
		if (user != null) {
			UserDto userDto = castUserToUserDto(user);
			return userDto;
		}
		return null;
	}

	public Boolean checkSystemCode(String systemCode) {
		if (systemCode.equals(authorizationSystemCode)) {
			return true;
		}
		return false;
	}

	public ObjectResult createRegisterKey(String mob) {
		User user = userDao.queryUser(null, mob, null, null, null, null);
		String mobKey = CommonMethod.getMobKey(6);
		ObjectResult objectResult = new ObjectResult();
		if (user == null) {
			user = new User();
			user.setCreatedTime(Calendar.getInstance());
			user.setUsername(mob);
			user.setUserStatusType(UserStatusType.NOTACTIVATED);
			// 密码默认为30位随机码，确保未激活的账户无法进行登录
			user.setPassword(CommonMethod.getRandomString(30));
			user.setMobileKey(mobKey);
			userDao.save(user);
			
			user.setUserPowerKey(CommonMethod.getRandomString(8)
					+ String.valueOf(user.getId()));
			userDao.update(user);
			
			objectResult.setResult(true);
		} else {
			if (user.getUserStatusType() == UserStatusType.NOTACTIVATED) {
				user.setCreatedTime(Calendar.getInstance());
				user.setUsername(mob);
				user.setUserStatusType(UserStatusType.NOTACTIVATED);
				// 密码默认为30位随机码，确保未激活的账户无法进行登录
				user.setPassword(CommonMethod.getRandomString(30));
				user.setMobileKey(mobKey);
				userDao.update(user);
				objectResult.setResult(true);
			} else {
				objectResult.setResult(false);
				objectResult.setMessage("该手机号已注册");
				return objectResult;
			}
		}
		// 通知手机
		controlSms.sendMessage(mob, mobKey, "SMS_7395039");
		return objectResult;
	}

	public UserDto registerUser(String username, String password,
			String mobkey) {
		if (mobkey == null || mobkey.equals("")) {
			return null;
		}
		User user = userDao.queryUser(null, username, null,
				UserStatusType.NOTACTIVATED, mobkey, null);
		if (user != null) {
			user.setUserStatusType(UserStatusType.NORMAL);
			password = CommonMethod.getMD5(password);
			user.setPassword(password);
			user.setMobileKey(mobkey);
			userDao.update(user);
			UserDto userDto = castUserToUserDto(user);
			return userDto;
		}
		return null;
	}

	public UserDto changePassword(String username, String password,
			String mobkey,String oldPassword) {
		
		User user = new User();
		if (mobkey != null || !"".equals(mobkey)) {
			user = userDao.queryUser(null, username, null,UserStatusType.NORMAL, mobkey, null);
		}else if (oldPassword!=null||!"".equals(oldPassword)) {
			user = userDao.queryUser(null, username, oldPassword,UserStatusType.NORMAL, null, null);
		}else{//当验证码和旧密码都为空时，无验证条件返回null
			return null;
		}
		
		if (user != null) {
			password = CommonMethod.getMD5(password);
			user.setMobileKey(null);
			user.setPassword(password);
			userDao.update(user);
			UserDto userDto = castUserToUserDto(user);
			return userDto;
		}
		return null;
	}

	/*public User saveUser(User user) {
		User existUser = queryUser(null, user.getUsername(), null,
				user.getUserStatusType(), null);
		if (existUser == null) {
			User newUser = userDao.save(user);
			return newUser;
		}
		return null;
	}
*/
	
	public User saveUser(User user,String ip) {
		User existUser = queryUser(null, user.getUsername(), null,
				user.getUserStatusType(), null);
		if (existUser == null) {
			User newUser = userDao.save(user);
			newUser.setUserPowerKey(CommonMethod.getRandomString(8)
					+ String.valueOf(newUser.getId()));
			userDao.update(newUser);
			
			//注册成功
			UserRegLog userRegLog = new UserRegLog();
			userRegLog.setUsername(user.getUsername());
			userRegLog.setUserid(1);
			userRegLog.setIp(ip);
			String ipaddr = IpUtil.getIpAddr(ip);
			userRegLog.setIpArea(ipaddr);
			userRegLogDao.save(userRegLog);
			return newUser;
		}
		return null;
	}
	public Boolean updateUser(User user) {
		User existUser = queryUser(null, user.getUsername(), null,
				user.getUserStatusType(), null);
		if (existUser != null) {
			BeansUtil.copyBean(existUser, user, true);
			userDao.update(existUser);
			return true;
		}
		return false;
	}
	
	public void updateManageUser(User user){
		userDao.update(user);
	}

	public ObjectResult createChangePasswordKey(String mob) {
		User user = userDao.queryUser(null, mob, null, null, null, null);
		String mobKey = CommonMethod.getMobKey(6);
		ObjectResult objectResult = new ObjectResult();
		if (user != null) {
			user.setMobileKey(mobKey);
			userDao.update(user);
			objectResult.setResult(true);
		} else {
			objectResult.setResult(false);
			objectResult.setMessage("该手机号未注册");
			return objectResult;
		}
		// 通知手机
		controlSms.sendMessage(mob, mobKey, "SMS_7395037");
		return objectResult;
	}

	public Boolean postSuggestion(String boxSerialNumber, String content) {
		try {
			UserSuggestion userSuggestion = new UserSuggestion();
			userSuggestion.setContent(content);
			userSuggestion.setFromStr(boxSerialNumber);
			userSuggestionDao.save(userSuggestion);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public ObjectResult createForgetPasswordKey(String mob) {
		User user = userDao.queryUser(null, mob, null, null, null, null);
		String mobKey = CommonMethod.getMobKey(6);
		ObjectResult objectResult = new ObjectResult();
		if (user != null) {
			user.setMobileKey(mobKey);
			userDao.update(user);
			objectResult.setResult(true);
		} else {
			objectResult.setResult(false);
			objectResult.setMessage("该手机号未注册");
			return objectResult;
		}
		// 通知手机
		controlSms.sendMessage(mob, mobKey, "SMS_7395037");
		return objectResult;
	}

	public ObjectResult createChangeUserInfoKey(String mob) {
		User user = userDao.queryUser(null, mob, null, null, null, null);
		String mobKey = CommonMethod.getMobKey(6);
		ObjectResult objectResult = new ObjectResult();
		if (user != null) {
			user.setMobileKey(mobKey);
			userDao.update(user);
			objectResult.setResult(true);
		} else {
			objectResult.setResult(false);
			objectResult.setMessage("该手机号未注册");
			return objectResult;
		}
		// 通知手机
		controlSms.sendMessage(mob, mobKey, "SMS_7395036");
		return objectResult;
	}

	public User queryUser(long id) {
		// TODO Auto-generated method stub
		return userDao.get(id);
	}

	public Boolean checkMobKey(String mob, String mobKey) {
		// TODO Auto-generated method stub
		User user = userDao.queryUser(null, mob, null, null, mobKey, null);
		return user != null ? true : false;
	}

	/**
	 * 实体转换Dto
	 * @param user
	 * @return
	 */
	private UserDto castUserToUserDto(User user) {
		UserDto userDto = new UserDto();
		userDto.setUserStatusTypeStr(user.getUserStatusType().str());
		BeansUtil.copyBean(userDto, user, true);
		if(user.getIsAutonym()!=null){
			userDto.setIsAutonym(user.getIsAutonym().toString());
		}else{
			userDto.setIsAutonym(null);
		}
		if(user.getReason()!=null){
			userDto.setReason(user.getReason());
		}else{
			userDto.setReason(null);
		}
		if(user.getBirthDate()!=null)userDto.setBirthDate(CommonMethod.CalendarToString(user.getBirthDate(), "yyyy/MM/dd"));
		return userDto;
	}

	/**
	 * 实体转换Dto
	 * @param user
	 * @return
	 */
	private ManageUserDto castUserToManageUserDto(User user){
		ManageUserDto manageUserDto = new ManageUserDto();
		BeansUtil.copyBean(manageUserDto, user, true); 
		manageUserDto.setFreq(String.valueOf(user.getFreq())); 
		manageUserDto.setUserStatusType(user.getUserStatusType().str());
		manageUserDto.setUserType(user.getUserType().str());
		manageUserDto.setCreatedTime(CommonMethod.CalendarToString(user.getCreatedTime(), "yyyy/MM/dd HH:mm"));
		manageUserDto.setUpdateTime(CommonMethod.CalendarToString(user.getUpdateTime(), "yyyy/MM/dd HH:mm"));
		if(user.getBirthDate()!=null)manageUserDto.setBirthDate(CommonMethod.CalendarToString(user.getBirthDate(), "yyyy/MM/dd"));
		return manageUserDto;
	}
	
	public UserDto changeUserInfo(String newUsername,String username, String mobkey) {
		if (mobkey == null || mobkey.equals("")) {
			return null;
		}
		User user = userDao.queryUser(null, username, null,
				UserStatusType.NORMAL, mobkey, null);
		if (user != null) {
			user.setUserStatusType(UserStatusType.NORMAL);
			user.setMobileKey(null);
			user.setUsername(newUsername);
			userDao.update(user);
			UserDto userDto = castUserToUserDto(user);
			return userDto;
		}
		return null;
	}

	public UserDto login(String username, String password) {
		// TODO Auto-generated method stub
		password = CommonMethod.getMD5(password);

		User user = userDao.queryUser(null, username, password,
				UserStatusType.NORMAL, null, null);
		if (user != null) {
			// 更新北斗卡号绑定的用户
			user.setUpdateTime(Calendar.getInstance());
			userDao.update(user);
			UserDto userDto = castUserToUserDto(user);
			return userDto;
		} else {
			return null;
		}

	}

	public UserDto getUserDto(Long id) {
		// TODO Auto-generated method stub
		return castUserToUserDto(userDao.get(id));
	}

	public User getUser(Long id) {
		// TODO Auto-generated method stub
		return userDao.get(id);
	}

	public List<ManageUserDto> query(UserType userType, String username,
			String password, UserStatusType userStatusType, String mobKey,
			String mail, String order, int page, int pageSize) {
		// TODO Auto-generated method stub
		List<User> list = userDao.query(userType, username, password, userStatusType, mobKey, mail, order, page, pageSize);
		List<ManageUserDto> dtos = new ArrayList<ManageUserDto>();
		for (User user : list) {
			dtos.add(castUserToManageUserDto(user));
		}
		return dtos;
	}

	public Integer queryCount(UserType userType, String username,
			String password, UserStatusType userStatusType, String mobKey,
			String mail) {
		// TODO Auto-generated method stub
		return userDao.queryCount(userType, username, password, userStatusType, mobKey, mail);
	}

	public void deleteUser(Long id){
		userDao.delete(id);
	}

	public ManageUserDto getManageUserDto(Long id) {
		// TODO Auto-generated method stub
		return castUserToManageUserDto(userDao.get(id));
	}

	public List<UserDto> listAll() {
		// TODO Auto-generated method stub
		List<User> list =  userDao.listAll();
		List<UserDto> dtos = new ArrayList<UserDto>();
		for (User user : list) {
			dtos.add(castUserToUserDto(user));
		}
		return dtos;
	}

	
	public ObjectResult createMailKey(long uid,String toAddress) {
		// TODO Auto-generated method stub
		ObjectResult objectResult = new ObjectResult();
		User user = userDao.get(uid);
		if(user!=null){
			String mobKey = CommonMethod.getMobKey(6);//获取验证码

			//标题
			String subject = "【北斗盒子】会员验证码确认";
			//内容
			String content = "<div style=\"width:700px;margin:0 auto;border-bottom:1px solid #ccc;margin-bottom:30px;margin-top:50px;\"></div><div style=\"width:680px;padding:0 10px;margin:0 auto;\"><div style=\"line-height:1.5;font-size:14px;margin-bottom:25px;color:#4d4d4d;\">"
					+ "<strong style=\"display:block;margin-bottom:15px;\">亲爱的会员：<span style=\"color:#f60;font-size: 16px;\"></span>您好！</strong><strong style=\"display:block;margin-bottom:15px;\">"
					+ "您正在北斗盒子官网操作，请在输入框中输入验证码："
					+ "<span style=\"color:#f60;font-size: 24px\"><span style=\"border-bottom-width: 1px; border-bottom-style: dashed; border-bottom-color: rgb(204, 204, 204); z-index: 1; position: static;\" t=\"7\" onclick=\"return false;\">"
					+ mobKey+"</span></span>，以完成操作。</strong></div><div style=\"margin-bottom:30px;\"><small style=\"display:block;margin-bottom:20px;font-size:12px;\"><p style=\"color:#747474;\">"
					+ "注意：此操作可能会修改您的密码、登录邮箱或绑定手机。如非本人操作，请及时登录并修改密码以保证帐户安全<br>（工作人员不会向你索取此验证码，请勿泄漏！)</p></small>"
					+ "</div></div><div style=\"width:700px;margin:0 auto;\"><div style=\"padding:10px 10px 0;border-top:1px solid #ccc;color:#747474;margin-bottom:20px;line-height:1.3em;font-size:12px;\">"
					+ "<p>此为系统邮件，请勿回复<br>请保管好您的邮箱，避免账号被他人盗用</p><p>北斗盒子版权所有</p></div></div>";
			
			//发送
			MailUtil.sendMail(emailSmtp,emailUsername,emailUsername,emailPassword,toAddress, subject, content);
			
			user.setMobileKey(mobKey);//设置最新验证码
			userDao.update(user);//修改验证码
			
			objectResult.setMessage("发送验证码成功");
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setMessage("用户不存在");
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}

	
	public Boolean postSuggestion(String boxSerialNumber, String content,String username,String phone,String qq) {
		try {
			UserSuggestion userSuggestion = new UserSuggestion();
			userSuggestion.setContent(content);
			userSuggestion.setFromStr(boxSerialNumber);
			userSuggestion.setPhone(phone);
			userSuggestion.setUsername(username);
			userSuggestion.setQq(qq);
			userSuggestionDao.save(userSuggestion);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	/**
	 * 获取手机验证码
	 */
	public ObjectResult createApplyCode(String mob) {
		MobCode mc = mobCodeDao.findCodeByMob(mob);
		String mobKey = CommonMethod.getMobKey(6);
		ObjectResult objectResult = new ObjectResult();
		if (mc != null){
			mc.setPhone(mob);
			mc.setCode(mobKey);
			mc.setCreateTime(Calendar.getInstance());
			mobCodeDao.update(mc);
			objectResult.setResult(true);
		} else {
			mc = new MobCode();
			mc.setPhone(mob);
			mc.setCode(mobKey);
			mc.setCreateTime(Calendar.getInstance());
			mobCodeDao.save(mc);
			objectResult.setResult(true);
		}
		// 通知手机
		String contentStr = "尊敬的用户，您的验证码为：" + mobKey + "（十分钟内有效）";
		controlSms.sendSmsOhterMessage(mob, contentStr);
		return objectResult;
	}

	public User getUserByPowerKey(String userPowerKey) {
		// TODO Auto-generated method stub
		return userDao.queryUser(userPowerKey);
	}
	 
	public User getUserByusername(String user){
		return userDao.getUserByusername(user);

	}
	
	//获取实名制用户
	public List<User> getRealUser(int page, int pageSize){
		return userDao.getRealUser(page,pageSize);
	}
	
	public Integer getRealUserCount(){
		return userDao.getRealUserCount();
	}
	
	public boolean updateRealName(User user){
		if(user!=null){
			userDao.update(user);
			return true;
		}else{
			return false;

		}
	}


	public UserDto getUsersession(Long id){
		User user= userDao.get(id); 
		UserDto userdto=castUserToUserDto(user);
		return userdto;
	}
	
	
	public String getUserSeclectOptions(Long selectUserId, int page, int pageSize) {
		List<User> users = userDao.queryuser_twl(page, pageSize);
		StringBuffer userSelectHtml = new StringBuffer("<option value=''>无</option>");
		for (User user : users) {
			String name="";
			if(user.getName()!=null){
				name="("+user.getName()+")";
			}else{
				name="";
			}
			
			if (selectUserId != null){
				if (user.getId() == selectUserId) {
					userSelectHtml.append("<option value='")
							.append(user.getUsername())
							.append("' selected=\"selected\">")
							.append(user.getUsername()).append(name).append("</option>");
				} else {
					userSelectHtml.append("<option value='")
							.append(user.getUsername()).append("' >")
							.append(user.getUsername()).append(name).append("</option>");
				}
			} else {
				userSelectHtml.append("<option value='").append(user.getUsername())
						.append("' >").append(user.getUsername()).append(name)
						.append("</option>");
			}

		}
		return userSelectHtml.toString();

	}
	
	public List<User> getRefeesUser(String usernomber,String refereespeoplename, int page, int pageSize){
		return userDao.getRefeesUser(usernomber,refereespeoplename,page, pageSize);

	}
	
	public Integer getRefeesUserCount(String usernomber,String refereespeoplename){
		return userDao.getRefeesUserCount(usernomber,refereespeoplename);
	}
	
	public User getUserByuser(String user){
		return userDao.getUserByuser(user);

	}

	@Override
	public List<UserDto> getAllUser() {
		// TODO Auto-generated method stub
		List<User> users = userDao.listAll();
		List<UserDto> dtos = new ArrayList<UserDto>();
		for(User user:users){
			UserDto dto = castUserToUserDto(user);
			dtos.add(dto);
		}
		return dtos;
	}

	@Override
	public ObjectResult getUserInfoByAccount(String key, String account,
			String pwd) {
		// TODO Auto-generated method stub
		ObjectResult objectResult = new ObjectResult();
		try{
			//验证系统调用码是否正确
			if(!authorizationSystemCode.equals(key)){
				objectResult.setMessage("无权限");
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.UNSUPPORTED);
				return objectResult;
			}

			//验证用户账号是否正确,获取用户账号信息
			UserDto udto = login(account, pwd);
			if(udto == null){
				objectResult.setMessage("用户名或密码错误");
				objectResult.setResult(null);
				objectResult.setStatus(ResultStatus.NOT_EXIST);
				return objectResult;
			}
			
			//获取用户好友信息
			
			//获取用户家人信息
			
			//返回参数
			UserInfoDto dto = new UserInfoDto();
			dto.setUserDto(udto);
			
			objectResult.setMessage("成功");
			objectResult.setResult(dto);
			objectResult.setStatus(ResultStatus.OK);
			
		}catch(Exception e){
			LogUtils.logerror("获取用户账号、好友、家人信息发生错误", e);
			objectResult.setMessage("系统错误");
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}

	@Override
	public ObjectResult getUserForLxtAccount(String lxtAccount) {
		ObjectResult result = new ObjectResult();
		User user = userDao.getUserForLxtAccount(lxtAccount);
		if(user!=null){
			result.setResult(castUserToUserDto(user));
			result.setStatus(ResultStatus.OK);
		}else{
			result.setMessage("还未绑定盒子官网用户");
			result.setResult(null);
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}

	@Override
	public ObjectResult dobindLxtAccount(String account, String password, String lxtAccount) {
		ObjectResult result = new ObjectResult();
		User user = userDao.getUserByusername(account);
		if(user!=null){
			if(CommonMethod.getMD5(password).equals(user.getPassword())){
				User u = userDao.getUserForLxtAccount(lxtAccount);
				if(u==null){
					userDao.bindLxtAccount(account, lxtAccount);
					result.setMessage("绑定成功");
					result.setStatus(ResultStatus.OK);
				}else{
					result.setMessage("你已绑定其他帐号、请解绑后再进行绑定");
					result.setStatus(ResultStatus.FAILED);
				}
			}else{
				result.setMessage("输入密码有误");
				result.setStatus(ResultStatus.FAILED);
			}
		}else{
			result.setMessage("查无此用户");
			result.setStatus(ResultStatus.NOT_EXIST);
		}
		return result;
	}

	@Override
	public ObjectResult dounbindLxtAccount(String lxtAccount) {
		ObjectResult result = new ObjectResult();
		User user = userDao.getUserForLxtAccount(lxtAccount);
		if(user!=null){
			userDao.unbindLxtAccount(user.getUsername());
			result.setMessage("解绑成功");
			result.setStatus(ResultStatus.OK);
		}else{
			result.setMessage("查无此用户");
			result.setStatus(ResultStatus.NOT_EXIST);
		}
		return result;
	}

}
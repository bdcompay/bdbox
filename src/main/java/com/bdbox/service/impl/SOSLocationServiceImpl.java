package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.SOSLocationDto;
import com.bdbox.dao.BoxDao;
import com.bdbox.dao.SOSLocationDao;
import com.bdbox.entity.Box;
import com.bdbox.entity.SOSLocation;
import com.bdbox.service.SOSLocationService;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.CommonMethod;

@Service
public class SOSLocationServiceImpl implements SOSLocationService{
	
	@Autowired
	private SOSLocationDao sosLocationDao;
	
	@Autowired
	private BoxDao boxDao;

	@Override
	public void save(SOSLocation sosLocation) {
		SOSLocation sosLo = sosLocationDao.getLocationForBoxId(sosLocation.getBox().getId());
		//sos位置信息，一个盒子只存取最新的一条
		if(sosLo!=null){
			sosLo = sosLocation;
			sosLocationDao.update(sosLo);
		}else{
			sosLocationDao.save(sosLocation);
		}
	}

	@Override
	public void update(SOSLocation sosLocation) {
		sosLocationDao.update(sosLocation);
	}

	@Override
	public void delete(Long id) {
		sosLocationDao.delete(id);
	}

	@Override
	public SOSLocationDto getLocation(Long id) {
		return castToDto(sosLocationDao.get(id));
	}

	@Override
	public List<SOSLocationDto> getList(String cardId) {
		List<Box> boxs = null;
		if(cardId!=null && cardId!=""){
			boxs = boxDao.getBoxInfo(cardId);
		}
		List<SOSLocationDto> listDto = new ArrayList<>();
		List<SOSLocation> list = new ArrayList<>();
		for (Box box : boxs) {
			SOSLocation sos = sosLocationDao.getLocation(box.getId());
			if(sos!=null){
				list.add(sos);
			}
		}
		for (SOSLocation sos : list) {
			listDto.add(castToDto(sos));
		}
		return listDto;
	}
	
	@Override
	public List<SOSLocationDto> getList() {
		List<SOSLocationDto> listDto = new ArrayList<>();
		List<SOSLocation> list = sosLocationDao.getLocation();
		for (SOSLocation sos : list) {
			listDto.add(castToDto(sos));
		}
		return listDto;
	}

	@Override
	public ObjectResult getLocationForBoxId(String cardNum) {
		ObjectResult result = new ObjectResult();
		Box box = boxDao.getBoxId(cardNum);
		if(box!=null){
			SOSLocation sos = sosLocationDao.getLocationForBoxId(box.getId());
			if(sos!=null){
				result.setResult(castToDto(sos));
				result.setStatus(ResultStatus.OK);
				result.setMessage("查找成功");
			}else{
				result.setStatus(ResultStatus.FAILED);
				result.setMessage("没有此设备");
			}
		}else{
			result.setStatus(ResultStatus.FAILED);
			result.setMessage("没有此设备");
		}
		return result;
	}
	
	/**
	 * 转换dto
	 * @param sos
	 * @return
	 */
	public SOSLocationDto castToDto(SOSLocation sos){
		SOSLocationDto sosLo = new SOSLocationDto();
		sosLo.setId(sos.getId());
		sosLo.setBoxSerialNumber(sos.getBox().getBoxSerialNumber());
		sosLo.setLongitude(String.valueOf(sos.getLongitude()));
		sosLo.setLatitude(String.valueOf(sos.getLatitude()));
		sosLo.setAltitude(String.valueOf(sos.getAltitude()));
		sosLo.setSpeed(String.valueOf(sos.getSpeed()));
		sosLo.setDirection(String.valueOf(sos.getDirection()));
		sosLo.setLocationSource(sos.getLocationSource());
		sosLo.setPositioningTime(CommonMethod.CalendarToString(sos.getPositioningTime(), "yyyy-MM-dd HH:mm:ss"));
		sosLo.setCreatedTime(CommonMethod.CalendarToString(sos.getCreatedTime(), "yyyy-MM-dd HH:mm:ss"));
		String uTime = "";
		if(!"".equals(sos.getUpdateTime()) && sos.getUpdateTime()!=null){
			uTime = CommonMethod.CalendarToString(sos.getUpdateTime(), "yyyy-MM-dd HH:mm:ss");
		}else{
			uTime = CommonMethod.CalendarToString(sos.getCreatedTime(), "yyyy-MM-dd HH:mm:ss");
		}
		sosLo.setUpdateTime(uTime);
		sosLo.setIsSOSLocation(String.valueOf(sos.getIsSOSLocation()));
		return sosLo;
	}

	@Override
	public SOSLocation getLocationByBoxId(Long boxId) {
		// TODO Auto-generated method stub
		 return sosLocationDao.getLocationForBoxId(boxId);
	}

	@Override
	public void saveOrUpdate(SOSLocation sosLocation) {
		// TODO Auto-generated method stub
		 sosLocationDao.saveOrUpdate(sosLocation);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public ObjectResult getSosLocation(JSONObject json) {
		ObjectResult result = new ObjectResult();
		try {
			//定义存储盒子id的list
			List<Long> ids = new ArrayList<>();
			//获取json所有的key
			Set<String> set = json.keySet();
			//循环获取value并add进list中
			for (String string : set) {
				ids.add(Long.parseLong(json.get(string).toString()));
			}
			
			List<SOSLocationDto> dtos = new ArrayList<>();
			List<SOSLocation> list = sosLocationDao.getSosLocation(ids);
			for (SOSLocation sosLocation : list) {
				dtos.add(castToDto(sosLocation));
			}
			result.setStatus(ResultStatus.OK);
			result.setResult(dtos);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResultStatus.SYS_ERROR);
			result.setMessage("系统异常");
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ObjectResult getSosLocations(JSONObject json, Calendar startTime,
			Calendar endTime) {
		ObjectResult result = new ObjectResult();
		try {
			//定义存储盒子id的list
			List<Long> ids = new ArrayList<>();
			//获取json所有的key
			Set<String> set = json.keySet();
			//循环获取value并add进list中
			for (String string : set) {
				Box box = boxDao.getBoxId(string);
				ids.add(box.getId());
			}
			
			List<SOSLocationDto> dtos = new ArrayList<>();
			List<SOSLocation> list = sosLocationDao.getSosLocations(ids, startTime, endTime);
			if(list.size()>0){
				for (SOSLocation sos : list) {
					dtos.add(castToDto(sos));
				}
			}
			result.setResult(dtos);
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("系统异常");
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		return result;
	}
}

package com.bdbox.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;  

import com.bdbox.control.ControlSms;
import com.bdbox.dao.RegisterInformationDao;
import com.bdbox.entity.RegisterInformation;
import com.bdbox.entity.User;
import com.bdbox.service.RegisterInformationService;
@Service
public class RegisterInformationServiceImpl implements RegisterInformationService {
	@Autowired
	private RegisterInformationDao registerInformationDao;

	@Autowired
	private ControlSms controlSms;
	
	public  RegisterInformation  getRealNameUrl(long userid,String type){
		
		return registerInformationDao.getRealNameUrl(userid,type);
	}
	
	public boolean updates(RegisterInformation registerInformation ){
		registerInformationDao.update(registerInformation);
		return true;
	}
	
	public RegisterInformation saves(RegisterInformation registerInformation ){
		RegisterInformation registerInformationd=registerInformationDao.save(registerInformation);
		return registerInformationd;
	}
	
	//获取实名制用户
	public List<RegisterInformation> getRegisterInformation(int page, int pageSize){
			return registerInformationDao.getRegisterInformation(page,pageSize);
		}

	public Integer getRegisterInformationCount(){
		return registerInformationDao.getRegisterInformationCount();
	}
	
	
	public boolean sendinfomaton(long id,String telephone,String name,String number){
		boolean result=false;
		if(id!=0&&telephone!=null&&name!=null){
			String content=""; 
			if(number.equals("1")){
				content="尊敬的"+name+"先生/女士您好，北斗盒子现可购买的库存已充足，请前往北斗盒子官网进行购买吧！"; 
			}else{
				content="尊敬的"+name+"先生/女士您好，北斗盒子现可租用的库存已充足，请前往北斗盒子官网进行租用吧！"; 
			}
 			boolean flag=controlSms.sendSmsOhterMessage_wl(telephone, content);
 			if(flag){
 				RegisterInformation registerInformation=registerInformationDao.get(id);
 	 			if(registerInformation!=null){
 	 				registerInformation.setRecord(true);
 	 				registerInformationDao.update(registerInformation);
 	 				result=true;
 	 			}else{
 	 				result=false;
 	 			}
 			}else{
 				result=false;
 			}
		}else{
			result=false;
		}
		return result;
	}

	public List<RegisterInformation> getRegisterInformation(){
		return registerInformationDao.getRegisterInformation();
	}
	
	public void delete(long id){
		registerInformationDao.delete(id);
	}




}

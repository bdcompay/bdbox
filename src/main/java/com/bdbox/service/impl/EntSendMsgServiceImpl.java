package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.dao.EntSendMsgDao;
import com.bdbox.dao.EnterpriseUserDao;
import com.bdbox.entity.EntSendMsg;
import com.bdbox.service.EntSendMsgService;
import com.bdbox.util.LogUtils;
import com.bdbox.web.dto.EntSendMsgDto;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

/**
 * 企业用户发送消息相关业务接口实现类
 * 
 * @ClassName: EntSendMsgServiceImpl 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-3-22 下午4:02:20 
 * @version V5.0 
 */
@Service
public class EntSendMsgServiceImpl  implements EntSendMsgService {
	/**
	 * 企业用户发送消息DAO
	 */
	@Autowired
	private EntSendMsgDao entSendMsgDao;
	/**
	 * 企业用户DAO
	 */
	@Autowired
	private EnterpriseUserDao enterpriseUserDao;
	
	/*
	 * 通过对象保存消息记录
	 * (non-Javadoc)
	 * @see com.bdbox.service.EntSendMsgService#save(com.bdbox.entity.EntSendMsg)
	 */
	@Override
	public EntSendMsgDto save(EntSendMsg entSendMsg) {
		try{
			EntSendMsg esMsg = entSendMsgDao.save(entSendMsg);
			return cast(esMsg);
		}catch(Exception e){
			LogUtils.logerror("通过对象保存企业发送消息错误", e);
		}
		return null;
	}

	/*
	 * 通过参数保存消息记录
	 * (non-Javadoc)
	 * @see com.bdbox.service.EntSendMsgService#save(java.lang.String, java.lang.String, java.lang.Long)
	 */
	@Override
	public EntSendMsgDto save(String toBoxSerialNumber, String content,
			Long entUserId) {
		try{
			EntSendMsg esMsg = new EntSendMsg();
			esMsg.setContent(content);
			esMsg.setEntUser(enterpriseUserDao.get(entUserId));
			esMsg.setToBoxSerialNumber(toBoxSerialNumber);
			entSendMsgDao.save(esMsg);
			return cast(esMsg);
		}catch(Exception e){
			LogUtils.logerror("通过参数保存企业发送消息错误", e);
		}
		return null;
	}

	/*
	 * 通过id删除消息
	 * (non-Javadoc)
	 * @see com.bdbox.service.EntSendMsgService#deleteEntSendMsg(long)
	 */
	@Override
	public boolean deleteEntSendMsg(long id) {
		try{
			entSendMsgDao.delete(id);
			return true;
		}catch(Exception e){
			LogUtils.logerror("通过id删除消息错误", e);
		}
		return false;
	}

	/*
	 * 更新企业发送消息
	 * (non-Javadoc)
	 * @see com.bdbox.service.EntSendMsgService#updateEntSendMsg(com.bdbox.entity.EntSendMsg)
	 */
	@Override
	public boolean updateEntSendMsg(EntSendMsg entSendMsg) {
		try{
			entSendMsgDao.update(entSendMsg);
			return true;
		}catch(Exception e){
			LogUtils.logerror("更新企业发送消息错误", e);
		}
		return false;
	}

	/*
	 * 通过id获取企业发送消息DTO
	 * (non-Javadoc)
	 * @see com.bdbox.service.EntSendMsgService#get(long)
	 */
	@Override
	public EntSendMsgDto get(long id) {
		try{
			EntSendMsg esMsg = entSendMsgDao.get(id);
			return cast(esMsg);
		}catch(Exception e){
			LogUtils.logerror("通过id获取企业发送消息错误", e);
		}
		return null;
	}

	/*
	 * 不定条件分页查询企业发送消息
	 * (non-Javadoc)
	 * @see com.bdbox.service.EntSendMsgService#listEntSendMsg(java.lang.String, java.util.Calendar, java.util.Calendar, java.lang.String, int, int)
	 */
	@Override
	public List<EntSendMsgDto> listEntSendMsg(String toBoxSerialNumber,
			Calendar startTime, Calendar endTime, String entUserName, int page,
			int pageSize) {
		List<EntSendMsgDto> dtos = new ArrayList<EntSendMsgDto>();
		try{
			List<EntSendMsg> msgs = entSendMsgDao.query(toBoxSerialNumber, startTime, endTime, entUserName, page, pageSize);
			for(EntSendMsg msg:msgs){
				dtos.add(cast(msg));
			}
		}catch(Exception e){
			LogUtils.logerror("不定条件分页查询企业发送消息错误", e);
		}
		return dtos;
	}

	/*
	 * 不定条件统计企业发送消息数量
	 * (non-Javadoc)
	 * @see com.bdbox.service.EntSendMsgService#count(java.lang.String, java.util.Calendar, java.util.Calendar, java.lang.String)
	 */
	@Override
	public int count(String toBoxSerialNumber, Calendar startTime,
			Calendar endTime, String entUserName) {
		try{
			return entSendMsgDao.count(toBoxSerialNumber, startTime, endTime, entUserName);
		}catch(Exception e){
			LogUtils.logerror("不定条件统计企业发送消息数量错误", e);
		}
		return 0;
	}
	
	/**
	 * 类型转换 实体转DTO
	 * @param esMsg
	 * @return
	 */
	private EntSendMsgDto cast(EntSendMsg esMsg){
		EntSendMsgDto dto = new EntSendMsgDto();
		BeansUtil.copyBean(dto, esMsg, true);
		if(esMsg.getCreatedTime()!=null){
			dto.setCreatedTimeStr(CommonMethod.CalendarToString(esMsg.getCreatedTime(), "yyyy-MM-dd HH:mm:ss"));
		}
		if(esMsg.getEntUser()!=null){
			dto.setEntUserName(esMsg.getEntUser().getName());
		}
		return dto;
	}

}

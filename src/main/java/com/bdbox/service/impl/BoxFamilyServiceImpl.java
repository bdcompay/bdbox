package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.FamilyDto;
import com.bdbox.constant.UserStatusType;
import com.bdbox.constant.UserType;
import com.bdbox.dao.BoxFamilyDao;
import com.bdbox.dao.UserDao;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxFamily;
import com.bdbox.entity.User;
import com.bdbox.service.BoxFamilyService;
import com.bdbox.service.BoxService;
import com.bdsdk.util.CommonMethod;

@Service
public class BoxFamilyServiceImpl implements BoxFamilyService {
	@Autowired
	private BoxFamilyDao boxFamilyDao;
	@Autowired
	private BoxService boxService;
	@Autowired
	private UserDao userDao;

	public void saveBoxFamily(BoxFamily boxFamily) {
		// TODO Auto-generated method stub
		boxFamilyDao.save(boxFamily);
	}

	public List<BoxFamily> getBoxFamilys(Long boxId) {
		// TODO Auto-generated method stub
		List<BoxFamily> boxFamilies = boxFamilyDao.query(boxId, null, 1, 100);
		return boxFamilies;
	}

	public boolean updateBoxFamily(List<FamilyDto> familyDtos,
			String cardNumber, String boxSerialsNumber) {
		// TODO Auto-generated method stub
		// 判断该box是否存在
		Box box = boxService.getBox(null,boxSerialsNumber, cardNumber, null, null);
		if (box != null) {
			// 清除该Box对应的家人列表
			List<BoxFamily> boxFamilies = getBoxFamilys(box.getId());
			for (BoxFamily boxFamily : boxFamilies) {
				boxFamilyDao.delete(boxFamily.getId());
			}
			// 保存最新的家人列表
			for (FamilyDto familyDto : familyDtos) {
				User user = userDao.queryUser(null, familyDto.getPhone(), null,
						null, null, null);
				if (user == null) {
					user = new User();
					user.setCreatedTime(Calendar.getInstance());
					user.setUsername(familyDto.getPhone());
					user.setUserStatusType(UserStatusType.NOTACTIVATED);
					user.setUserType(UserType.ROLE_COMMON);
					userDao.save(user);
				} else {
					user.setMail(familyDto.getMail());
					userDao.update(user);
				}

				BoxFamily boxFamily = new BoxFamily();
				boxFamily.setBox(box);
				boxFamily.setFamilyName(familyDto.getName());
				boxFamily.setFamilyMob(familyDto.getPhone());
				boxFamily.setFamilyMail(familyDto.getMail());
				boxFamilyDao.save(boxFamily);
			}
			return true;
		} else {
			return false;
		}

	}

	public boolean updateFamilySingle(FamilyDto familyDto, String cardNumber,
			String boxSerialsNumber) {
		// 判断该box是否存在
		Box box = boxService.getBox(null,boxSerialsNumber, cardNumber, null, null);
		if (box != null) {
			// 修改
			User user = userDao.queryUser(null, familyDto.getPhone(), null,
					null, null, null);
			if (user == null) {
				user = new User();
				user.setCreatedTime(Calendar.getInstance());
				user.setUsername(familyDto.getPhone());
				user.setUserStatusType(UserStatusType.NOTACTIVATED);
				user.setUserType(UserType.ROLE_COMMON);
				userDao.save(user);
				
				user.setUserPowerKey(CommonMethod.getRandomString(8)
						+ String.valueOf(user.getId()));
				userDao.update(user);
			}

			BoxFamily boxFamily = new BoxFamily();
			boxFamily.setId(familyDto.getId());
			boxFamily.setBox(box);
			boxFamily.setFamilyName(familyDto.getName());
			boxFamily.setFamilyMail(familyDto.getMail());
			boxFamily.setFamilyMob(familyDto.getPhone());
			boxFamilyDao.update(boxFamily);
			return true;
		} else {
			return false;
		}
	}

	public List<FamilyDto> getFamilyDtos(String cardNumber,
			String boxSerialsNumber) {
		// TODO Auto-generated method stub
		// 判断该box是否存在
		Box box = boxService.getBox(null,boxSerialsNumber, cardNumber, null, null);
		if (box == null) {
			return null;
		}
		List<BoxFamily> boxFamilies = boxFamilyDao.query(box.getId(), null, 1,
				100);
		List<FamilyDto> familyDtos = new ArrayList<FamilyDto>();
		for (BoxFamily boxFamily : boxFamilies) {
			FamilyDto familyDto = castBoxFamilyToFamilyDto(boxFamily);
			familyDtos.add(familyDto);
		}
		return familyDtos;
	}

	private FamilyDto castBoxFamilyToFamilyDto(BoxFamily boxFamily) {
		FamilyDto familyDto = new FamilyDto();
		familyDto.setId(boxFamily.getId());
		familyDto.setName(boxFamily.getFamilyName());
		familyDto.setPhone(boxFamily.getFamilyMob());
		familyDto.setMail(boxFamily.getFamilyMail());
		return familyDto;
	}

	public BoxFamily getBoxFamily(Long boxId, String familyMob) {
		// TODO Auto-generated method stub
		List<BoxFamily> boxFamilies = boxFamilyDao
				.query(boxId, familyMob, 1, 1);
		if (boxFamilies != null && boxFamilies.size() != 0) {
			return boxFamilies.get(0);
		}
		return null;
	}

	public void saveFamily(BoxFamily boxFamily) {
		// TODO Auto-generated method stub
		boxFamilyDao.save(boxFamily);
	}

	public void deleteFamily(Long id) {
		// TODO Auto-generated method stub
		boxFamilyDao.delete(id);
	}

}

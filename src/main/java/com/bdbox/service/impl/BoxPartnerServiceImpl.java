package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.PartnerDto;
import com.bdbox.dao.BoxPartnerDao;
import com.bdbox.dao.UserDao;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxPartner;
import com.bdbox.service.BoxPartnerService;
import com.bdbox.service.BoxService;

@Service
public class BoxPartnerServiceImpl implements BoxPartnerService {
	@Autowired
	private BoxPartnerDao boxPartnerDao;
	@Autowired
	private BoxService boxService;
	@Autowired
	private UserDao userDao;

	public void saveBoxPartner(BoxPartner boxPartner) {
		// TODO Auto-generated method stub
		boxPartnerDao.save(boxPartner);
	}

	public List<BoxPartner> getBoxPartners(Long boxId) {
		// TODO Auto-generated method stub
		List<BoxPartner> boxPartners = boxPartnerDao.query(boxId, null, 1, 100);
		return boxPartners;
	}

	public boolean updateBoxPartner(List<PartnerDto> partnerDtos,
			String cardNumber, String boxSerialsNumber) {
		// TODO Auto-generated method stub
		// 判断该box是否存在
		Box box = boxService.getBox(null,boxSerialsNumber, cardNumber, null, null);
		if (box != null) {
			// 清除该Box对应的队友列表
			List<BoxPartner> boxPartners = getBoxPartners(box.getId());
			for (BoxPartner boxPartner : boxPartners) {
				boxPartnerDao.deleteObject(boxPartner);
			}
			// 保存最新的队友列表
			for (PartnerDto partnerDto : partnerDtos) {
				Box partnerBox = boxService.getBox(null,
						partnerDto.getBoxSerialNumber(), null, null, null);
				if (partnerBox == null) {
					return false;
				}
				BoxPartner boxPartner = new BoxPartner();
				boxPartner.setBox(box);
				boxPartner.setPartnerBoxSerialNumber(partnerDto.getBoxSerialNumber());
				boxPartner.setPartnerName(partnerDto.getName());
				boxPartnerDao.save(boxPartner);
			}
			return true;
		} else {
			return false;
		}

	}

	public List<PartnerDto> getPartnerDtos(String cardNumber,
			String boxSerialsNumber) {
		// TODO Auto-generated method stub
		// 判断该box是否存在
		Box box = boxService.getBox(null,boxSerialsNumber, cardNumber, null, null);
		if (box == null) {
			return null;
		}

		List<BoxPartner> boxPartners = boxPartnerDao.query(box.getId(), null,
				1, 10);
		List<PartnerDto> partnerDtos = new ArrayList<PartnerDto>();
		for (BoxPartner boxPartner : boxPartners) {
			PartnerDto partnerDto = castBoxPartnerToPartnerDto(boxPartner);
			partnerDtos.add(partnerDto);
		}
		return partnerDtos;
	}

	private PartnerDto castBoxPartnerToPartnerDto(BoxPartner boxPartner) {
		PartnerDto partnerDto = new PartnerDto();
		partnerDto.setId(boxPartner.getId());
		partnerDto.setName(boxPartner.getPartnerName());
		partnerDto.setBoxSerialNumber(boxPartner.getPartnerBoxSerialNumber());
		return partnerDto;
	}

	public BoxPartner getBoxPartner(Long boxId, String partnerBoxSerialNumber) {
		// TODO Auto-generated method stub
		List<BoxPartner> boxPartners = boxPartnerDao.query(boxId, partnerBoxSerialNumber,
				1, 1);
		if (boxPartners != null && boxPartners.size() != 0) {
			return boxPartners.get(0);
		}
		return null;
	}

	public boolean updatePartnerSingle(PartnerDto partnerDto) {
		// 判断该box是否存在
		BoxPartner partner = boxPartnerDao.get(partnerDto.getId());
		partner.setPartnerName(partnerDto.getName());
		partner.setPartnerBoxSerialNumber(partnerDto.getBoxSerialNumber());
		boxPartnerDao.update(partner);
		return true;
	}

	public void savePartner(BoxPartner boxPartner) {
		// TODO Auto-generated method stub
		boxPartnerDao.save(boxPartner);
	}

	public void deletePartner(Long id) {
		// TODO Auto-generated method stub
		boxPartnerDao.delete(id);
	}
}

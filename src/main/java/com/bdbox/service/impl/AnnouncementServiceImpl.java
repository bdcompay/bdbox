package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.AnnouncementDto;
import com.bdbox.dao.AnnouncementDao;
import com.bdbox.entity.Announcement;
import com.bdbox.service.AnnouncementService;
import com.bdbox.util.LogUtils;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Service
public class AnnouncementServiceImpl implements AnnouncementService {
	@Autowired
	private AnnouncementDao announcementDao;
	
	public List<Announcement> getlistAnnouncementtable(int page, int pageSize){
		return announcementDao.getlistAnnouncementtable(page,pageSize);
	}
	
	public Integer getlistAnnouncementtableCount(){
		return announcementDao.getlistAnnouncementtableCount();
	}
	
	public Announcement save(String announcementname, String hyperlink){
		Announcement announcement=new Announcement();
		if("".equals(announcementname)){
			return announcement ;
		}else{
			announcement.setCenter(announcementname);
			if(!"".equals(hyperlink)){
				announcement.setHyperlink(hyperlink);
			 }
			return announcementDao.save(announcement); 
		}
		 
	}
	
	public Announcement get(long id){
		return announcementDao.get(id);
	}
	
	public AnnouncementDto castFrom(Announcement announcement){
		AnnouncementDto dto = new AnnouncementDto();
		BeansUtil.copyBean(dto, announcement, true);
		if(announcement.getCreatTime()!=null){
			dto.setCreatTime(CommonMethod.CalendarToString(announcement.getCreatTime(), "yyyy-MM-dd HH:mm:ss"));
 		} 
		return dto;
	}

	public boolean update(Long id, String updateannouncementname, String updatehyperlink){
		try {
			Announcement announcement = announcementDao.get(id);
			announcement.setCenter(updateannouncementname);
			if(!"".equals(updatehyperlink)){
				announcement.setHyperlink(updatehyperlink);
			} 
			announcementDao.update(announcement);
			LogUtils.loginfo("修改ID为：" + id + " 的公告");
			return true;
		} catch (Exception e) {
			LogUtils.logerror("修改ID为：" + id + " 的公告失败", e);
		}
		return false;
	}
	
	public boolean deleteAnnouncement(Long id){
		try {
			announcementDao.delete(id);
			LogUtils.loginfo("删除ID为：" + id + " 的公告成功");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.logerror("删除ID为：" + id + " 的公告失败", e);
		}
		return false;
	}
	
	public List<Announcement> getfindAnnouncement(){
		return announcementDao.getfindAnnouncement();
	}

	@Override
	public List<AnnouncementDto> getList(String content, int page, int pageSize) {
		List<AnnouncementDto> listDto = new ArrayList<>();
		List<Announcement> list = announcementDao.getList(content, page, pageSize);
		for (Announcement announcement : list) {
			listDto.add(castFrom(announcement));
		}
		return listDto;
	}

	@Override
	public int count(String content) {
		return announcementDao.announcementCount(content);
	} 
}

package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.ApplyDto;
import com.bdbox.constant.UserStatusType;
import com.bdbox.control.ControlSms;
import com.bdbox.dao.ApplyDao;
import com.bdbox.dao.UserDao;
import com.bdbox.entity.Apply;
import com.bdbox.entity.User;
import com.bdbox.service.ApplyService;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;
@Service
public class ApplyServiceImpl implements ApplyService{

	@Autowired
	private ApplyDao applyDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private ControlSms controlSms;
	
	public List<ApplyDto> listApply(String code, String phone,
			Calendar createTime, String sex, Boolean join, Integer page,
			Integer pageSize, String apply) {
		// TODO Auto-generated method stub
		List<Apply> list = applyDao.listApply(code, phone, createTime, sex, join, page, pageSize, apply);
		List<ApplyDto> dtos = new ArrayList<ApplyDto>();
		for(Apply entity : list)dtos.add(castUserToUserDto(entity));
		return dtos;
	}

	public int applyCount(String code, String phone, Calendar createTime,
			String sex, Boolean join) {
		// TODO Auto-generated method stub
		return applyDao.applyCount(code, phone, createTime, sex, join);
	}

	
	/**
	 * 将实体转换成Dto
	 * @param user
	 * @return
	 */
	private ApplyDto castUserToUserDto(Apply apply) {
		ApplyDto dto = new ApplyDto();
		//将实体属性拷贝到Dto中，属性名必须相同
		BeansUtil.copyBean(dto, apply, true);
		//将Calendar类型转换为String类型斌赋值到Dto属性中
		dto.setCreateTime(CommonMethod.CalendarToString(apply.getCreateTime(), "yyyy/MM/dd HH:mm"));
		if(apply.isJoin()){
			dto.setJoin("参与");
		}else{
			dto.setJoin("不参与");
		}
		
		//性别
		if(dto.getSex()!=null&&!dto.getSex().equals("")){
			if(dto.getSex().equals("1")){
				dto.setSex("男");
			}else if(dto.getSex().equals("2")){
				dto.setSex("女");
			}
		}
		if(apply.getUser()!=null){
			dto.setUser(apply.getUser().getName());
		}
		
		return dto;
	}

	public void saveApply(Apply apply) {
		Apply app = applyDao.save(apply);
		String contentStr = "";
		if(app != null){
			User user = userDao.queryUser(null, app.getPhone(), null, null, null, null);
			if(user == null){
				user = new User();
				String pwd = CommonMethod.getRandomString(8);
				user.setUsername(app.getPhone());
				user.setCreatedTime(app.getCreateTime());
				user.setUpdateTime(app.getCreateTime());
				user.setPassword(CommonMethod.getMD5(pwd));
				user.setUserStatusType(UserStatusType.NORMAL);
				User u = userDao.save(user);
				u.setUserPowerKey(CommonMethod.getRandomString(8)
						+ String.valueOf(u.getId()));
				userDao.update(u);
				
				app.setUser(u);
				applyDao.update(app);
				contentStr = "您的号码"+app.getPhone()
						+"已成功预约北斗盒子的抢购，届时您可使用此号码作为帐号登录官网即可参与抢购【密码："+pwd+"】";
			} else {
				app.setUser(user);
				applyDao.update(app);
				contentStr = "您的号码"+app.getPhone()
						+"已成功预约北斗盒子的抢购，届时您可使用此号码作为帐号登录官网即可参与抢购";
			}
			controlSms.sendSmsOhterMessage(app.getPhone(), contentStr);
		}
	}

	public void updateApply(Apply apply) {
		// TODO Auto-generated method stub
		applyDao.update(apply);
	}

	public void deleteApply(Long id) {
		// TODO Auto-generated method stub
		applyDao.delete(id);
	}

	public ApplyDto queryApplyDto(Long id) {
		// TODO Auto-generated method stub
		return castUserToUserDto(applyDao.get(id));
	}

	public Apply queryApply(Long id) {
		// TODO Auto-generated method stub
		return applyDao.get(id);
	}

	public ApplyDto getApply(String phone) {
		// TODO Auto-generated method stub
		Apply apply = applyDao.getApply(phone);
		if(apply!=null){
			return castUserToUserDto(apply);
		}else {
			return null;
		}
	}

	public ApplyDto getApplyUid(Long uid) {
		// TODO Auto-generated method stub
		Apply apply = applyDao.getApplyUid(uid);
		if(apply!=null){
			return castUserToUserDto(apply);
		}else {
			return null;
		}
	}
}

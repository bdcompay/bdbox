package com.bdbox.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.alipay.util.UtilDate;
import com.bdbox.constant.DealStatus;
import com.bdbox.constant.ReturnsStatusType;
import com.bdbox.constant.UnionpayRespCode;
import com.bdbox.dao.InvoiceDao;
import com.bdbox.dao.NotificationDao;
import com.bdbox.dao.OrderDao;
import com.bdbox.dao.ReturnsDao;
import com.bdbox.dao.UnionpayOrderDao;
import com.bdbox.dao.UnionpayRefundDao;
import com.bdbox.entity.Invoice;
import com.bdbox.entity.Notification;
import com.bdbox.entity.Order;
import com.bdbox.entity.Returns;
import com.bdbox.entity.UnionpayOrder;
import com.bdbox.entity.UnionpayRefund;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.UnionpayRefundService;
import com.bdbox.util.LogUtils;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.CommonMethod;

@Service
public class UnionpayRefundServiceImpl implements UnionpayRefundService{
	
	@Autowired
	private UnionpayRefundDao unionpayRefundDao;
	
	@Autowired
	private UnionpayOrderDao unionpayOrderDao;
	
	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private ReturnsDao returnsDao;
	
	@Autowired 
	private NotificationDao notificationDao;
	
	@Autowired  
	private MailNotification mailNotification; 
	
	@Autowired
	private InvoiceDao invoiceDao;  

	private static String flag1="";//标记发邮件的次数，每个支付订单只发一次

	@Override
	public ObjectResult save(Map<String, String> rspData, String orderId) {
		ObjectResult result = new ObjectResult();
		Boolean flag = false;
		UnionpayRefund refund = unionpayRefundDao.getRefundByQueryId(rspData.get("origQryId"));
		if(refund==null){
			refund = new UnionpayRefund();
			flag = true;
		}
		try {
			refund.setOrderId(rspData.get("orderId"));
			refund.setTxnAmt(Double.parseDouble(rspData.get("txnAmt")));
			refund.setTxnTime(CommonMethod.StringToCalendar(rspData.get("txnTime"), UtilDate.dtLong));
			refund.setQueryId(rspData.get("queryId"));
			refund.setOrigQryId(rspData.get("origQryId"));
			String respCode = rspData.get("respCode");
			refund.setRespCode(UnionpayRespCode.switchStatus(respCode));
			refund.setRespMsg(rspData.get("respMsg"));
			//获取订单信息
			Order order = orderDao.getOrderstatus(orderId);
			Returns rt = unionpayRefundDao.getReturns(order.getOrderNo());
			if("00".equals(respCode)){
				order.setDealStatus(DealStatus.REFUNDED);
				if(rt!=null){
					rt.setReturnsStatusType(ReturnsStatusType.FINISH);
				}
				result.setMessage("退款成功");
				result.setStatus(ResultStatus.OK);
				//订单支付成功后，给管理员发送邮件通知
				StringBuffer content=new StringBuffer(); 
				String type="BUYPAY";  //通知类型
				String type_twl="admin"; 
				String invoices="";            //是否开发票
				String tomail=null;            //收信箱
				String subject=null;           //主题
				String mailcentent=null;       //内容
				if(!order.getOrderNo().equals(flag1)){ 
		    		flag1 = order.getOrderNo(); 
					if("RENT".equals(order.getTransactionType())){ 
			    		type="RENTPAY";
		    			Notification notification = notificationDao.getnotification(type,type_twl);
		    			if(notification!=null){ 
		    				if(notification.getIsemail().equals(true)){   
	    						tomail=notification.getTomail();
	    						subject=notification.getSubject();  
	    						content.append(notification.getCenter())
	    						.append("订单号："+order.getOrderNo()+","
	    						+ "下单时间："+CommonMethod.CalendarToString(order.getCreatedTime(), "yyyy-MM-dd HH:mm:ss")+"，"
	    						+ "下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，"
	    						+ "联系方式："+order.getPhone()).append(invoices);
	    						if(content!=null){
	    							mailcentent=content.toString(); 
	    						}
	    						String [] email=notification.getTomail().split(",");
	    						for(String str:email){
	    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
	    						}
		    				}
		    			}
		            }else{
		            	Notification notification=notificationDao.getnotification(type,type_twl);
		    			if(notification!=null){ 
		    				if(notification.getIsemail().equals(true)){  
	    						tomail=notification.getTomail();
	    						subject=notification.getSubject(); 
	    						if(order.getInvoice()!=null && order.getInvoice()!=0){
	    							Invoice enty = invoiceDao.getInvoice(order.getInvoice());
	    							invoices="，发票抬头："+enty.getCompanyName();
	    						}
	    						content.append(notification.getCenter()+":")
	    						.append("订单号："+order.getOrderNo()+","
	    						+ "下单时间："+CommonMethod.CalendarToString(order.getCreatedTime(), "yyyy-MM-dd HH:mm:ss")+"，"
	    						+ "下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，"
	    						+ "联系方式："+order.getPhone()).append(invoices);
	    						if(content!=null){
	    							mailcentent=content.toString(); 
	    						}
	    						String [] email=tomail.split(",");
	    						for(String str:email){
	    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
	    						}	
		    				}
		    			} 
		            }
				}
			}else{
				order.setDealStatus(DealStatus.REFUNDFAIL);
				if(rt!=null){
					rt.setReturnsStatusType(ReturnsStatusType.REFUNDFAIL);
				}
				result.setMessage("退款失败");
				result.setStatus(ResultStatus.FAILED);
			}
			//保存
			if(flag)
				unionpayRefundDao.save(refund);
			else
				unionpayRefundDao.update(refund);
			//更新
			orderDao.update(order);
			if(rt!=null)
				returnsDao.update(rt);
		} catch (Exception e) {
			LogUtils.logerror("退款异常:", e);
			result.setMessage("退款失败");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}

	@Override
	public void update(Map<String, String> rspData) {
		try {
			UnionpayRefund refund = unionpayRefundDao.getRefund(rspData.get("queryId"));
			refund.setOrderId(rspData.get("orderId"));
			refund.setTxnAmt(Double.parseDouble(rspData.get("txnAmt")));
			refund.setTxnTime(CommonMethod.StringToCalendar(rspData.get("txnTime"), UtilDate.dtLong));
			refund.setQueryId(rspData.get("queryId"));
			String respCode = rspData.get("respCode");
			refund.setRespCode(UnionpayRespCode.switchStatus(respCode));
			refund.setRespMsg(rspData.get("respMsg"));
			refund.setTraceNo(rspData.get("traceNo"));
			refund.setTraceTime(rspData.get("traceTime"));
			refund.setSettleDate(rspData.get("settleDate"));
			refund.setSettleAmt(rspData.get("settleAmt"));
			//获取订单信息
			UnionpayOrder uo = unionpayOrderDao.getOrderByQueryId(refund.getOrigQryId());
			Order order = orderDao.getOrderstatus(uo.getOrderId());
			Returns rt = unionpayRefundDao.getReturns(order.getOrderNo());
			if("00".equals(respCode)){
				order.setDealStatus(DealStatus.REFUNDED);
				if(rt!=null){
					rt.setReturnsStatusType(ReturnsStatusType.FINISH);
				}
			}else{
				order.setDealStatus(DealStatus.REFUNDFAIL);
				if(rt!=null){
					rt.setReturnsStatusType(ReturnsStatusType.REFUNDFAIL);
				}
			}
			//更新
			unionpayRefundDao.update(refund);
			//更新
			orderDao.update(order);
			if(rt!=null)
				returnsDao.update(rt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(UnionpayRefund unionpayRefund) {
		unionpayRefundDao.delete(unionpayRefund.getId());
	}

	@Override
	public UnionpayRefund getUnionpayRefund(String orderId) {
		return unionpayRefundDao.getUnionpayRefund(orderId);
	}

}

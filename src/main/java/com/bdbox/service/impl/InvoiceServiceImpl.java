package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.constant.CheckStatus;
import com.bdbox.constant.InvoiceType;
import com.bdbox.dao.InvoiceDao;
import com.bdbox.dao.NotificationDao;
import com.bdbox.dao.OrderDao;
import com.bdbox.dao.QualificationCheckDao;
import com.bdbox.entity.Invoice;
import com.bdbox.entity.Order;
import com.bdbox.entity.QualificationCheck;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.InvoiceService;
import com.bdbox.web.dto.InvoiceDto;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Service
public class InvoiceServiceImpl implements InvoiceService{
	
	@Autowired
	private InvoiceDao invoiceDao;
	
	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private QualificationCheckDao qualificationCheckDao;
	@Autowired
	private NotificationDao notificationDao;
	@Autowired  
	private MailNotification mailNotification; 

	@Override
	public ObjectResult saveInvoice(Long id, String companyName, String taxpayerIdNum, String registerAddr,
			String registerPhone, String bankName, String bankAccount, String ticketsInfo, String ticketsPhone,
			String ticketsAddr, String detailAddr, String invoiceType, Long user){
		ObjectResult result = new ObjectResult();
		Invoice invoice = new Invoice();
		try{
			//设置参数到实体中
			invoice.setCompanyName(companyName);
			invoice.setTaxpayerIdNum(taxpayerIdNum);
			invoice.setRegisterAddr(registerAddr);
			invoice.setRegisterPhone(registerPhone);
			invoice.setBankName(bankName);
			invoice.setBankAccount(bankAccount);
			invoice.setTicketsInfo(ticketsInfo);
			invoice.setTicketsPhone(ticketsPhone);
			invoice.setTicketsAddr(ticketsAddr);
			invoice.setDetailAddr(detailAddr);
			InvoiceType type = null;
			CheckStatus status = null;
			//判断发票类型
			if("增值税普通发票".equals(invoiceType)){
				type = InvoiceType.switchStatusForEng(invoiceType);
				status = CheckStatus.PASS;
			} else if ("增值税专用发票".equals(invoiceType)){
				type = InvoiceType.switchStatusForEng(invoiceType);
				status = CheckStatus.CHECKING;
				
			}
			invoice.setCheckStatus(status);
			invoice.setInvoiceType(type);
			invoice.setUser(user);
			invoice.setCreateTime(Calendar.getInstance());
			Long idd = null;
			//保存操作
			if(id==null || id==-1){
				idd = invoiceDao.saveInvoice(invoice);
				result.setMessage("保存成功");
				result.setResult(idd);
				result.setStatus(ResultStatus.OK);
			} else {
				invoice.setId(id);
				invoiceDao.update(invoice);
				result.setMessage("更新成功");
				result.setResult(id);
				result.setStatus(ResultStatus.OK);
			}
			
			/*//以下是用户提交发票资料后邮件通知管理员审核
 			StringBuffer content=new StringBuffer(); 
			String types="FPIAO";  //通知类型  
			String type_twl="admin"; 
			String tomail=null;            //收信箱
			String subject=null;           //主题
			String mailcentent=null;       //内容
			Notification notification=notificationDao.getnotification(types,type_twl);
			if(notification!=null){ 
				if(notification.getIsemail().equals(true)){   
					tomail=notification.getTomail();
					subject=notification.getSubject();
						content.append(notification.getCenter());
						if(content!=null){
							mailcentent=content.toString(); 
						}
						String [] email=tomail.split(",");
						for(String str:email){
	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
						}
 					//mailNotification.sendNote(user.getUsername(),mailcentent+"【北斗盒子】");
				}
			} */
			
			
		} catch(Exception e){
			e.printStackTrace();
			result.setMessage("保存失败");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}

	@Override
	public void updateInvoice(Invoice invoice) {
		invoiceDao.update(invoice);
		/*//以下是用户提交发票资料后邮件通知管理员审核
			StringBuffer content=new StringBuffer(); 
		String types="FPIAO";  //通知类型  
		String type_twl="admin"; 
		String tomail=null;            //收信箱
		String subject=null;           //主题
		String mailcentent=null;       //内容
		Notification notification=notificationDao.getnotification(types,type_twl);
		if(notification!=null){ 
			if(notification.getIsemail().equals(true)){   
				tomail=notification.getTomail();
				subject=notification.getSubject();
					content.append(notification.getCenter());
					if(content!=null){
						mailcentent=content.toString(); 
					}
					String [] email=tomail.split(",");
					for(String str:email){
    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
					}
 				//mailNotification.sendNote(user.getUsername(),mailcentent+"【北斗盒子】");
			}
		} */
	}

	@Override
	public ObjectResult deleteInvoice(Long id) {
		ObjectResult result = new ObjectResult();
		try {
			//删除发票
			invoiceDao.delete(id);
			//获取订单信息并把删除的发票id从订单信息中删除
			Order order = invoiceDao.getOrder(id);
			if(order!=null){
				order.setInvoice(null);
				orderDao.update(order);
			}
			//如果是专用发票、则需要删除对应的审核资料
			QualificationCheck qc = qualificationCheckDao.getQualificationCheck(id);
			if(qc!=null){
				qualificationCheckDao.delete(qc.getId());
			}
			result.setMessage("删除成功");
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("删除失败");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}

	@Override
	public List<InvoiceDto> getAllInvoice(Long user) {
		List<InvoiceDto> dtos = new ArrayList<>();
		List<Invoice> list = invoiceDao.getAllInvoice(user);
		for (Invoice i : list) {
			dtos.add(castDto(i));
		}
		return dtos;
	}

	@Override
	public InvoiceDto getInvoiceDto(Long id) {
		return castDto(invoiceDao.getInvoice(id));
	}
	
	public InvoiceDto castDto(Invoice invoice){
		InvoiceDto dto = new InvoiceDto();
		BeansUtil.copyBean(dto, invoice, true);
		if(invoice!=null){
			if(invoice.getCreateTime()!=null){
				dto.setCreatedTime(CommonMethod.CalendarToString(invoice.getCreateTime(), "yyyy-MM-dd"));
			}
			if(invoice.getMoren()!=null&&invoice.getMoren().equals(true)){
				dto.setMorendto("1");
			}else{
				dto.setMorendto("0");
			}
		}
		return dto;
	}

	@Override
	public Invoice getInvoice(Long id) {
		return invoiceDao.getInvoice(id);
	}

	@Override
	public InvoiceDto getCheckStatus(Long userId) {
		return castDto(invoiceDao.getCheckStatus(userId));
	}
	
	/**
	 * 设置为默认发票
	 * @param userId
	 * @return
	 */ 
	public boolean updatemoren(Invoice invoice){
		boolean result=false;
		if(invoice==null){
			result=false;
		}else{
			invoiceDao.update(invoice);
			result=true;
		}
		return result;
		
	}
	
	public Invoice getAllInvoices(Long user){
	      Invoice list = invoiceDao.getAllInvoices(user);
        return list;
	}

}

package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.BoxLocationDto;
import com.bdbox.api.dto.MyCardLocStatusPOJO;
import com.bdbox.dao.BoxDao;
import com.bdbox.dao.BoxLocationDao;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxLocation;
import com.bdbox.service.BoxLocationService;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Service
public class BoxLocationServiceImpl implements BoxLocationService {
	@Autowired
	private BoxLocationDao boxLocationDao;
	@Autowired
	private BoxDao boxDao;

	public void saveBoxLocation(BoxLocation boxLocation) {
		// TODO Auto-generated method stub
		boxLocationDao.save(boxLocation);
	}

	public List<BoxLocationDto> listBoxLocations(Long boxId,String cardNumber,String boxName,String userName,String boxSerialNumber,Calendar createdTime, String order,
			Integer page, Integer pageSize) {
		// TODO Auto-generated method stub
		List<BoxLocation> list = boxLocationDao.listBoxLocations(boxId,cardNumber, boxName, userName, boxSerialNumber, createdTime, order, page, pageSize);
		List<BoxLocationDto> boxLocationDtos = new ArrayList<BoxLocationDto>();
		for (BoxLocation boxLocation : list) {
			boxLocationDtos.add(switchDto(boxLocation));
		}
		
		return boxLocationDtos;
	}

	public Integer listBoxLocationsCount(Long boxId,String cardNumber,String boxName,String userName,String boxSerialNumber,Calendar createdTime) {
		// TODO Auto-generated method stub
		return boxLocationDao.listBoxLocationsCount(boxId,cardNumber, boxName, userName, boxSerialNumber, createdTime);
	}
	
	@Override
	public List<BoxLocationDto> queryBoxLocations(Long boxId,
			String cardNumber, String boxName, String userName,
			String boxSerialNumber, Calendar startTime, Calendar endTime,Long userId,
			String order, Integer page, Integer pageSize) {
		List<BoxLocationDto> boxLocationDtos = new ArrayList<BoxLocationDto>();
		// TODO Auto-generated method stub
		List<BoxLocation> list = boxLocationDao.queryBoxLocations(boxId, cardNumber, boxName, userName, boxSerialNumber, startTime, endTime, userId,order, page, pageSize);
		for (BoxLocation boxLocation : list) {
			boxLocationDtos.add(switchDto(boxLocation));
		}
		return boxLocationDtos;
	}

	@Override
	public int count(Long boxId, String cardNumber, String boxName,
			String userName, String boxSerialNumber, Calendar startTime,
			Calendar endTime,Long userId) {
		// TODO Auto-generated method stub
		return boxLocationDao.amount(boxId, cardNumber, boxName, userName, boxSerialNumber, startTime, endTime,userId);
	}
	
	/**
	 * POJO转换DTO
	 * @param boxMessage
	 * @return
	 */
	private BoxLocationDto switchDto(BoxLocation boxLocation){
		
		BoxLocationDto boxLocationDto = new BoxLocationDto();
		
		BeansUtil.copyBean(boxLocationDto, boxLocation, true);
		
		boxLocationDto.setCreatedTime(CommonMethod.CalendarToString(boxLocation.getCreatedTime(), "yyyy/MM/dd HH:mm:ss"));
		
		boxLocationDto.setBoxSerialNumber(boxLocation.getBox().getBoxSerialNumber());
		
		boxLocationDto.setBoxName(boxLocation.getBox().getName());
		
		if(boxLocation.getBox().getUser()!=null){
			boxLocationDto.setBoxUser(boxLocation.getBox().getUser().getUsername());
		}
		
		boxLocationDto.setCardNumber(boxLocation.getBox().getCardNumber());
		
		boxLocationDto.setLongitude(switchDegrees(boxLocation.getLongitude()));
		
		boxLocationDto.setLatitude(switchDegrees(boxLocation.getLatitude()));
		
		boxLocationDto.setManager("<a class=\"btn btn-large btn-primary\" href=\"javascript:void(0);\" onclick=\"queryLocation("+boxLocation.getId()+")\">[查看地图]</a>");
		
		return boxLocationDto;
	}

	public void deleteBoxLocation(Long id) {
		// TODO Auto-generated method stub
		boxLocationDao.delete(id);
	}
	
	/**
	 * 将度数转换成 度分秒
	 * @param degrees
	 * @return
	 */
	private String switchDegrees(Double degrees){
		double value = Math.abs(degrees);  
		double v1 = Math.floor(value);//度  
		double v2 = Math.floor((value - v1) * 60);//分  
		double v3 = Math.round((value - v1) * 3600 % 60);//秒  
        String val =  v1 + "°" + v2 + "'" + v3 + "\"";  
        return val.replace(".0", "");
	}

	public BoxLocationDto getBoxLocationDto(Long id) {
		// TODO Auto-generated method stub
		BoxLocation boxLocation = boxLocationDao.get(id);
		BoxLocationDto boxLocationDto = new BoxLocationDto();
		BeansUtil.copyBean(boxLocationDto, boxLocation, true);
		boxLocationDto.setCreatedTime(CommonMethod.CalendarToString(boxLocation.getCreatedTime(), "yyyy/MM/dd HH:mm"));
		boxLocationDto.setBoxSerialNumber(boxLocation.getBox().getBoxSerialNumber());
		boxLocationDto.setBoxName(boxLocation.getBox().getName());
		if(boxLocation.getBox().getUser()!=null){
			boxLocationDto.setBoxUser(boxLocation.getBox().getUser().getUsername());
		}
		boxLocationDto.setCardNumber(boxLocation.getBox().getCardNumber());
		boxLocationDto.setLatitude(Double.toString(boxLocation.getLatitude()));
		boxLocationDto.setLongitude(Double.toString(boxLocation.getLongitude()));
		return boxLocationDto;
	}

	@Override
	public List<MyCardLocStatusPOJO> getBoxLocationDto(Long boxId,
			Calendar startTime, Calendar endTime,Long userId) {
		List<BoxLocation> list = boxLocationDao.getBoxLocation(boxId, startTime, endTime,userId);
		List<MyCardLocStatusPOJO> dtos = new ArrayList<>();
		if(list!=null){
			for (BoxLocation bl : list) {
				dtos.add(castDto(bl));
			}
		}
		return dtos;
	}

	@Override
	public List<MyCardLocStatusPOJO> queryBoxLocationDto(String id,
			Calendar startTime, Calendar endTime) {
		Box box = boxDao.getBox(id);
		List<MyCardLocStatusPOJO> dtos = new ArrayList<>();
		if(box!=null){
			List<BoxLocation> list = boxLocationDao.getBoxLocation(box.getId(), startTime, endTime,null);
			if(list!=null){
				for (BoxLocation bl : list) {
					dtos.add(castDto(bl));
				}
			}
		}
		return dtos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ObjectResult getAllLocation(JSONObject json) {
		ObjectResult result = new ObjectResult();
		try {
			//获取json所有的key
			Set<String> set = json.keySet();

			List<BoxLocation> list = new ArrayList<>();
			
			//循环获取value并add进list中
			for (String string : set) {
				Box box = boxDao.getBoxId(json.get(string).toString());
				list.addAll(boxLocationDao.getAllLocation(box.getId()));
			}
			
			List<BoxLocationDto> dtos = new ArrayList<>();
			for (BoxLocation bl : list) {
				dtos.add(castToDto(bl));
			}
			result.setResult(dtos);
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResultStatus.SYS_ERROR);
			result.setMessage("系统异常");
		}
		return result;
	}
	
	protected BoxLocationDto castToDto(BoxLocation bl){
		BoxLocationDto dto = new BoxLocationDto();
		dto.setId(bl.getBox().getId());
		dto.setBoxName(bl.getBox().getName());
		dto.setBoxUser(bl.getBox().getRealName());
		dto.setBoxSerialNumber(bl.getBox().getBoxSerialNumber());
		dto.setCardNumber(bl.getBox().getCardNumber());
		dto.setLongitude(String.valueOf(bl.getLongitude()));
		dto.setLatitude(String.valueOf(bl.getLatitude()));
		dto.setAltitude(bl.getAltitude());
		dto.setSpeed(bl.getSpeed());
		dto.setDirection(bl.getDirection());
		dto.setLocationSource(bl.getLocationSource());
		dto.setCreatedTime(CommonMethod.CalendarToString(bl.getCreatedTime(), "yyyy-MM-dd HH:mm:ss"));
		dto.setPositioningTimeStr(CommonMethod.CalendarToString(bl.getPositioningTime(), "yyyy-MM-dd HH:mm:ss"));
		return dto;
	}
	
	protected MyCardLocStatusPOJO castDto(BoxLocation bl){
		MyCardLocStatusPOJO dto = new MyCardLocStatusPOJO();
		dto.setCardid(String.valueOf(bl.getId()));
		dto.setBoxSerialNumber(bl.getBox().getBoxSerialNumber());
		dto.setLongitude(String.valueOf(bl.getLongitude()));
		dto.setAltitude(String.valueOf(bl.getAltitude()));
		dto.setLatitude(String.valueOf(bl.getLatitude()));
		dto.setSpeed(String.valueOf(bl.getSpeed()));
		dto.setDirection(String.valueOf(bl.getDirection()));
		dto.setCreatedtime(CommonMethod.CalendarToString(bl.getCreatedTime(), "yyyy-MM-dd HH:mm:ss"));
		return dto;
	}

	@Override
	public ObjectResult getEntUserLocation(String id, Calendar startTime,
			Calendar endTime, Integer page, Integer pageSize) {
		ObjectResult result = new ObjectResult();
		try {
			Box box = boxDao.getBox(id);
			
			List<BoxLocationDto> dtos = new ArrayList<>();
			int count = 0;
			if(box!=null){
				List<BoxLocation> list = boxLocationDao.getEntUserLocation(box.getId(), startTime, endTime, page, pageSize);
				count = boxLocationDao.amount(box.getId(), null, null, null, null, startTime, endTime,null);
				for (BoxLocation bl : list) {
					dtos.add(castToDto(bl));
				}
			}
			Map<String, Object> map = new HashMap<String, Object>();
			
			//当前页
			map.put("page", page);
			//总记录数
			map.put("records", count);
			//总页数
			int totalpage = count/pageSize;
			if(count%pageSize!=0){
				totalpage+=1;
			}
			
			map.put("totalpage", totalpage);
			//起始记录
			int displayStart = (page-1)*pageSize+1;
			map.put("displayStart", displayStart);
			
			//截止记录
			int displayEnd = page*pageSize;
			if(count<displayEnd){
				displayEnd = count;
			}
			map.put("displayEnd", displayEnd);
			
			//数据
			map.put("returns", dtos);
			result.setResult(map);
			result.setStatus(ResultStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("系统异常");
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ObjectResult getEntUserLocations(Calendar startTime, Calendar endTime, JSONObject json) {
		ObjectResult result = new ObjectResult();
		try {
			//定义存储盒子id的list
			List<Long> ids = new ArrayList<>();
			//获取json所有的key
			Set<String> set = json.keySet();
			//循环获取value并add进list中
			for (String string : set) {
				Box box = boxDao.getBoxId(string);
				ids.add(box.getId());
			}
			List<BoxLocationDto> dtos = new ArrayList<>();
			List<BoxLocation> list = boxLocationDao.getEntUserLocations(startTime, endTime, ids);
			if(list.size()>0){
				for (BoxLocation bl : list) {
					dtos.add(castToDto(bl));
				}
			}
			result.setResult(dtos);
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("系统异常");
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		return result;
	}

	@Override
	public List<MyCardLocStatusPOJO> listUserBoxLocations(Long boxId, String cardNumber, String order, Integer page,
			Integer pageSize) {
		List<BoxLocation> list = boxLocationDao.listBoxLocations(boxId,cardNumber, null, null, null, null,
				order, page, pageSize);
		List<MyCardLocStatusPOJO> dtos=new ArrayList<>(0);
		if(list!=null){
			for(BoxLocation bl:list){
				dtos.add(castDto(bl));
			}
		}
		return dtos;
	}

}

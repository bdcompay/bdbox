package com.bdbox.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.dao.MobCodeDao;
import com.bdbox.entity.MobCode;
import com.bdbox.service.MobCodeService;

@Service
public class MobCodeServiceImpl implements MobCodeService{
	
	@Autowired
	private MobCodeDao mobCodeDao;

	@Override
	public void update(MobCode mobCode) {
		mobCodeDao.update(mobCode);
	}

	@Override
	public void save(MobCode mobCode) {
		mobCodeDao.save(mobCode);
	}

	@Override
	public MobCode findCodeByPhone(String phone) {
		return mobCodeDao.findCodeByMob(phone);
	}

}

package com.bdbox.service.impl;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.control.ControlSms;
import com.bdbox.dao.BoxDao;
import com.bdbox.dao.EnterpriseUserDao;
import com.bdbox.dao.MobCodeDao;
import com.bdbox.entity.Box;
import com.bdbox.entity.EnterpriseUser;
import com.bdbox.entity.MobCode;
import com.bdbox.service.EnterpriseUserService;
import com.bdbox.util.LogUtils;
import com.bdbox.web.dto.EnterpriseUserDto;
import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

/**
 * 企业用户业务处理
 * 
 * @ClassName: EnterpriseUserServiceImpl 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-3-11 上午10:38:34 
 * @version V5.0 
 */
@Service
public class EnterpriseUserServiceImpl implements EnterpriseUserService {
	/**
	 * 企业用户DAO
	 */
	@Autowired
	private EnterpriseUserDao enterpriseUserDao;
	/**
	 * 盒子DAO
	 */
	@Autowired
	private BoxDao boxDao;
	
	@Autowired
	private MobCodeDao mobCodeDao;
	
	@Autowired
	private ControlSms controlSms;
	
	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.EnterpriseUserService#save(com.bdbox.entity.EnterpriseUser)
	 */
	@Override
	public EnterpriseUserDto save(EnterpriseUser entUser) {
		try{
			return  cast(enterpriseUserDao.save(entUser));
		}catch(Exception e){
			LogUtils.logerror("保存企业用户错误", e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.EnterpriseUserService#save(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public EnterpriseUserDto save(String name, String orgCode, String linkman,
			String phone,String mail, String address, String explain,boolean isDsiEnable,int freq, String password,String pushURL) {
		try{
			EnterpriseUser entUser = new EnterpriseUser();
			entUser.setAddress(address);
			entUser.setExplain1(explain);
			entUser.setLinkman(linkman);
			entUser.setName(name);
			entUser.setOrgCode(orgCode);
			entUser.setPhone(phone);
			entUser.setMail(mail);
			entUser.setIsDsiEnable(isDsiEnable);
			entUser.setFreq(freq);
			entUser.setPassword(CommonMethod.getMD5(password));
			entUser.setPushURL(pushURL);
			enterpriseUserDao.save(entUser);
			entUser.setUserPowerKey(CommonMethod.getRandomString(8)+String.valueOf(entUser.getId()));
			return cast(entUser);
		}catch(Exception e){
			LogUtils.logerror("通过属性参数创建保存企业用户错误", e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.EnterpriseUserService#delete(long)
	 */
	@Override
	public boolean delete(long id) {
		try{
			enterpriseUserDao.delete(id);
			return true;
		}catch(Exception e){
			LogUtils.logerror("通过id删除企业用户错误", e);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.EnterpriseUserService#delete(com.bdbox.entity.EnterpriseUser)
	 */
	@Override
	public boolean delete(EnterpriseUser entUser) {
		try{
			enterpriseUserDao.deleteObject(entUser);
			return true;
		}catch(Exception e){
			LogUtils.logerror("通过企业用户对象删除企业用户错误",e);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.EnterpriseUserService#update(com.bdbox.entity.EnterpriseUser)
	 */
	@Override
	public boolean update(EnterpriseUser entUser) {
		try{
			enterpriseUserDao.update(entUser);
			return true;
		}catch(Exception e){
			LogUtils.logerror("通过企业用户对象更新错误", e);
		}
		return false;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.EnterpriseUserService#update(long, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean update(long id,String name,String orgCode,String linkman,String phone,
			String mail,String address,String explain,boolean isDsiEnable,int freq,String pushURL){
		try{
			EnterpriseUser entUser = enterpriseUserDao.get(id);
			if(entUser==null){
				LogUtils.loginfo("id为："+id+"的企业用户不存在。");
				return false;
			}
			entUser.setAddress(address);
			entUser.setExplain1(explain);
			entUser.setLinkman(linkman);
			entUser.setName(name);
			entUser.setOrgCode(orgCode);
			entUser.setPhone(phone);
			entUser.setMail(mail);
			entUser.setIsDsiEnable(isDsiEnable);
			entUser.setFreq(freq);
			entUser.setPushURL(pushURL);
			
			enterpriseUserDao.update(entUser);
			return true;
		}catch(Exception e){
			LogUtils.logerror("通过企业用户属性更新错误", e);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.EnterpriseUserService#get(long)
	 */
	@Override
	public EnterpriseUserDto get(long id) {
		try{
			return cast(enterpriseUserDao.get(id));
		}catch(Exception e){
			LogUtils.logerror("通过id查询企业用户错误", e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.EnterpriseUserService#listEnts(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar, int, int)
	 */
	@Override
	public List<EnterpriseUserDto> listEnts(String name, String orgCode,
			String linkman, String phone,String mial, String address, Calendar startTime,
			Calendar endTime, int page, int pageSize) {
		List<EnterpriseUserDto> dtos = new ArrayList<EnterpriseUserDto>();
		try{
			List<EnterpriseUser> entUsers = enterpriseUserDao.queryEnterpriseUsers(name, orgCode, linkman, 
																phone, mial,address, startTime, endTime, page, pageSize);
			for(EnterpriseUser entUser:entUsers){
				dtos.add(cast(entUser));
			}
			return dtos;
		}catch(Exception e){
			LogUtils.logerror("多条件分页查询企业用户错误", e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.EnterpriseUserService#count(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public int count(String name, String orgCode, String linkman, String phone,
			String mial,String address, Calendar startTime, Calendar endTime) {
		try{
			return enterpriseUserDao.amount(name, orgCode, linkman, phone,mial, address, startTime, endTime);
		}catch(Exception e){
			LogUtils.logerror("多条件统计记录数量错误", e);
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.EnterpriseUserService#queryJqEnts(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Calendar, java.util.Calendar, int, int)
	 */
	@Override
	public ListParam queryJqEnts(String name, String orgCode, String linkman,
			String phone, String mial,String address, Calendar startTime, Calendar endTime,
			int page, int pageSize) {
		List<EnterpriseUserDto> dtos = new ArrayList<EnterpriseUserDto>();
		int count = 0;
		try{
			dtos = listEnts(name, orgCode, linkman, phone, mial,address, startTime, endTime, page, pageSize);
			count = count(name, orgCode, linkman, phone,mial, address, startTime, endTime);
		}catch(Exception e){
			LogUtils.logerror("查询符合Jquery.datatable格式的数据错误", e);
		}
		
		ListParam listParam = new ListParam();
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setAaData(dtos);
		return listParam;
	}
	
	/**
	 * 类型转换
	 * 
	 * @param entUser
	 * @return
	 */
	private EnterpriseUserDto cast(EnterpriseUser entUser){
		EnterpriseUserDto dto = new EnterpriseUserDto();
		if(entUser==null) return null;
		BeansUtil.copyBean(dto, entUser, true);
		if(entUser.getCreatedTime()!=null){
			dto.setCreatedTimeStr(CommonMethod.CalendarToString(entUser.getCreatedTime(), "yyyy-MM-dd HH:mm:ss"));
		}
		if(entUser.getLastSendBoxMessageTime()!=null){
			dto.setLastSendBoxMessageTimeStr(CommonMethod.CalendarToString(entUser.getLastSendBoxMessageTime(), "yyyy-MM-dd HH:mm:ss"));
		}
		int boxNum = boxDao.countByEntUserId(entUser.getId());
		dto.setBoxNum(boxNum);
		return dto;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.EnterpriseUserService#doBindEntAndBox(long, long)
	 */
	@Override
	public ObjectResult doBindEntAndBox(long entUsreId, long boxNumber) {
		ObjectResult objectResult = new ObjectResult();
		try{
			EnterpriseUser ent = enterpriseUserDao.get(entUsreId);
			List<Box> boxs = boxDao.queryBoxId(boxNumber);
			if(boxs.size()==0){
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("无此北斗盒子");
				return objectResult;
			}
			Box box = boxs.get(0);
			if(box.getEntUser()!=null){
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setMessage("已绑定企业用户 "+box.getEntUser().getName());
				return objectResult;
			}
			//绑定
			box.setEntUser(ent);
			boxDao.update(box);
			objectResult.setStatus(ResultStatus.OK);
		}catch(Exception e){
			LogUtils.logerror("绑定企业用户和北斗盒子错误", e);
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("系统错误");
		}
		return objectResult;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.EnterpriseUserService#doUnbindEntAndBox(long, long)
	 */
	@Override
	public Boolean doUnbindEntAndBox(long entUserId, long boxNumber) {
		try{
			List<Box> boxs = boxDao.queryBoxId(boxNumber);
			if(boxs.size()==0)  
				return false;
			Box box = boxs.get(0);
			box.setEntUser(null);
			boxDao.update(box);
			return true;
		}catch(Exception e){
			LogUtils.logerror("解绑企业用户和北斗盒子错误", e);
		}
		return null;
	}

	@Override
	public ObjectResult login(String username, String password) {
		ObjectResult result = new ObjectResult();
		try {
			EnterpriseUser user = enterpriseUserDao.query(username);
			//判断是否有该用户
			if(user!=null){
				//MD5加密
				password = CommonMethod.getMD5(password);
				//用户存在则判断密码是否匹配
				if(password.equals(user.getPassword()==null?"":user.getPassword())){
					result.setMessage("登录成功");
					result.setResult(cast(user));	
					result.setStatus(ResultStatus.OK);
				}else{
					result.setMessage("您输入的密码有误");
					result.setStatus(ResultStatus.FAILED);
				}
			}else{
				result.setMessage("您输入的帐号有误或不存在");
				result.setStatus(ResultStatus.NOT_EXIST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("系统异常");
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		return result;
	}

	@Override
	public ObjectResult doCareateYZM(String phone) {
		ObjectResult objectResult = new ObjectResult();
		try {
			MobCode mc = mobCodeDao.findCodeByMob(phone);
			String mobKey = CommonMethod.getMobKey(6);
			if (mc != null){
				mc.setPhone(phone);
				mc.setCode(mobKey);
				mc.setCreateTime(Calendar.getInstance());
				mobCodeDao.update(mc);
			} else {
				mc = new MobCode();
				mc.setPhone(phone);
				mc.setCode(mobKey);
				mc.setCreateTime(Calendar.getInstance());
				mobCodeDao.save(mc);
			}
			// 通知手机
			String contentStr = "尊敬的用户，您的验证码为：" + mobKey + "（十分钟内有效）";
			controlSms.sendSmsOhterMessage(phone, contentStr);
			objectResult.setMessage("获取成功");
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setResult(mobKey);
		} catch (Exception e) {
			e.printStackTrace();
			objectResult.setMessage("获取失败");
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}

	@Override
	public ObjectResult changePwd(String phone, String password) {
		ObjectResult result = new ObjectResult();
		try {
			EnterpriseUser user = enterpriseUserDao.query(phone);
			if(user!=null){
				if(!"".equals(password)){
					password = CommonMethod.getMD5(password);
				}
				user.setPassword(password);
				enterpriseUserDao.update(user);
				result.setMessage("修改成功");
				result.setStatus(ResultStatus.OK);
			}else{
				result.setMessage("不存在此帐号");
				result.setStatus(ResultStatus.NOT_EXIST);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("修改失败");
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}
	
	public EnterpriseUser getUserByPowerKey(String userPowerKey) {
		// TODO Auto-generated method stub
		return enterpriseUserDao.queryEnterpriseUserByPowerKey(userPowerKey);
	}

	@Override
	public EnterpriseUser getEntUser(String phone) {
		return enterpriseUserDao.query(phone);
	}
	
}

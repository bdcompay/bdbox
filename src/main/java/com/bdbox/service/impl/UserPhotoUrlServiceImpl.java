package com.bdbox.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service; 

import com.bdbox.dao.UserPhotoUrlDao;
import com.bdbox.entity.UserPhotoUrl;
import com.bdbox.service.UserPhotoUrlService;

@Service
public class UserPhotoUrlServiceImpl implements UserPhotoUrlService {
	@Autowired
	private UserPhotoUrlDao userPhotoUrlDao;
	
	public UserPhotoUrl save(	 UserPhotoUrl userPhotoUrl){ 
		return userPhotoUrlDao.save(userPhotoUrl);
	}
	
	public UserPhotoUrl getUserPhotoUrl(Long userid ){
		return userPhotoUrlDao.getUserPhotoUrl(userid);
	}
	
	public boolean updates(UserPhotoUrl userPhotoUrl ){
		boolean result;
		if(userPhotoUrl!=null){
			userPhotoUrlDao.update(userPhotoUrl);  
			result=true;
		}else{
			result=false;
		}
		return result;
	}
	
	public  UserPhotoUrl  getRealNameUrl(long userid){
		return userPhotoUrlDao.getRealNameUrl(userid);
	}
	


}

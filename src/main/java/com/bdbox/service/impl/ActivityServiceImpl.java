package com.bdbox.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.constant.Status;
import com.bdbox.dao.ActivityDao;
import com.bdbox.entity.Activity;
import com.bdbox.service.ActivityService;
import com.bdbox.web.dto.ActivityDto; 

@Service
public class ActivityServiceImpl implements ActivityService {

	@Autowired
	private ActivityDao activityDao;
	
	public List<ActivityDto> queryActivity(Integer page,Integer pageSize){
		List<ActivityDto> listdto = new ArrayList<ActivityDto>(); 
		List<Activity> list=activityDao.list(page, pageSize);
		for(Activity u:list){
			ActivityDto usd = castFrom(u); 
			listdto.add(usd);
		}
		return listdto;
	}
	
	protected ActivityDto castFrom(Activity usm){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ActivityDto uscmd = new ActivityDto(); 
		if(usm!=null){
			uscmd.setId(usm.getId());
			if(usm.getStatus().equals(Status.RUSH)){
				uscmd.setStatus("抢购");
			}
			if(usm.getStatus().equals(Status.ORDER)){
				uscmd.setStatus("预约");
			}
			if(usm.getStatus().equals(Status.COMMONBUY)){
				uscmd.setStatus("普通购买");
			}
			if(usm.getStartTime()!=null){
				uscmd.setStartTime(sdf.format(usm.getStartTime().getTime()));
			}else{
				uscmd.setStartTime(null);
			}
			if(usm.getEndTime()!=null){
				uscmd.setEndTime(sdf.format(usm.getEndTime().getTime())); 
			}else{
				uscmd.setEndTime(null); 

			}
		}else{
			uscmd.setId(null);
			uscmd.setStatus(null);
			uscmd.setStartTime(null);
			uscmd.setEndTime(null);  
		}
		
		return uscmd;
	}
	
	public int counts(){
		String sql="select count(*) from Activity u where 1=1";
		int count = activityDao.counts(sql);
		return count;
	}
	
	public ActivityDto getActivity(Long id){
		Activity activity=activityDao.get(id);
		ActivityDto usd = castFrom(activity); 
		return usd;
	}
	
	public Activity getActivitys(Long id){
		Activity activity=activityDao.get(id); 
		return activity;
	}
	
	public boolean updateActivity(Activity activity){
	   if(activity!=null){
			activityDao.update(activity);
			return true;
	    }else{
	    	return false;
	    }
	}


	/**
	 * 获取活动信息
	 * @return
	 */
	public List<ActivityDto> findActivity(){
		List<Activity> list = activityDao.listAll();
		List<ActivityDto> dtoList = new ArrayList<>();
		for (Activity a : list) {
			dtoList.add(castFrom(a));
		}
		return dtoList;
	}

}

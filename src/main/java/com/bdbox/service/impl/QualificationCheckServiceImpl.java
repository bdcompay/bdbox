package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.dao.NotificationDao;
import com.bdbox.dao.QualificationCheckDao;
import com.bdbox.entity.Notification;
import com.bdbox.entity.QualificationCheck;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.QualificationCheckService;
import com.bdbox.web.dto.QualificationCheckDto;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Service
public class QualificationCheckServiceImpl implements QualificationCheckService{

	@Autowired
	private QualificationCheckDao qualificationCheckDao;
	@Autowired
	private NotificationDao notificationDao;
	@Autowired  
	private MailNotification mailNotification; 
	
	@Override
	public ObjectResult save(String businessLicense, String taxReg, 
			String orgCode, String generalTax, String invoiceDatum, Long invoice, Long id){
		ObjectResult result = new ObjectResult();
		QualificationCheck qc = null;
		//判断更新和添加
		if(id==null || id==-1){
			qc = new QualificationCheck();
		}else{
			qc = qualificationCheckDao.getQualificationCheck(id);
		}
		//防止为空
		if(!"".equals(businessLicense) && businessLicense!=null)
			qc.setBusinessLicense(businessLicense);
		if(!"".equals(taxReg) && taxReg!=null)
			qc.setTaxReg(taxReg);
		if(!"".equals(orgCode) && orgCode!=null)
			qc.setOrgCode(orgCode);
		if(!"".equals(generalTax) && generalTax!=null)
			qc.setGeneralTax(generalTax);
		if(!"".equals(invoiceDatum) && invoiceDatum!=null)
			qc.setInvoiceDatum(invoiceDatum);
		qc.setBusLicStatus(false);
		qc.setTaxRegStatus(false);
		qc.setOrgCodeStauts(false);
		qc.setGeneralTaxStatus(false);
		qc.setInvoiceDatumStatus(false);
		qc.setCreateTime(Calendar.getInstance());
		qc.setInvoice(invoice);
		try {
			if(id==null || id==-1){
				qualificationCheckDao.save(qc);
				result.setMessage("保存资料成功");
				result.setStatus(ResultStatus.OK);
			} else {
				qc.setBusLicRemark(null);
				qc.setTaxRegRemark(null);
				qc.setOrgCodeRemark(null);
				qc.setGeneralTaxRemark(null);
				qc.setInvoiceDatumRemark(null);
				qualificationCheckDao.update(qc);
				result.setMessage("更新资料成功");
				result.setStatus(ResultStatus.OK);
			}
			
			//以下是用户提交发票资料后邮件通知管理员审核
 			StringBuffer content=new StringBuffer(); 
			String types="FPIAO";  //通知类型  
			String type_twl="admin"; 
			String tomail=null;            //收信箱
			String subject=null;           //主题
			String mailcentent=null;       //内容
			Notification notification=notificationDao.getnotification(types,type_twl);
			if(notification!=null){ 
				if(notification.getIsemail().equals(true)){   
					tomail=notification.getTomail();
					subject=notification.getSubject();
						content.append(notification.getCenter());
						if(content!=null){
							mailcentent=content.toString(); 
						}
						String [] email=tomail.split(",");
						for(String str:email){
	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
						}
 					//mailNotification.sendNote(user.getUsername(),mailcentent+"【北斗盒子】");
				}
			} 
			
		} catch (Exception e) {
			result.setMessage("保存资料失败");
			result.setStatus(ResultStatus.FAILED);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public void update(QualificationCheck qualificationCheck) {
		qualificationCheckDao.update(qualificationCheck);
	}

	@Override
	public void delete(Long id) {
		qualificationCheckDao.delete(id);
	}

	@Override
	public QualificationCheckDto getDto(Long id) {
		return castDto(qualificationCheckDao.get(id));
	}

	@Override
	public List<QualificationCheckDto> query(int page, int pageSize) {
		List<QualificationCheck> list = qualificationCheckDao.query(page, pageSize);
		List<QualificationCheckDto> dto = new ArrayList<>();
		for (QualificationCheck qc : list) {
			dto.add(castDto(qc));
		}
		return dto;
	}

	@Override
	public int queryAcount() {
		return qualificationCheckDao.queryAcount();
	}
	
	public QualificationCheckDto castDto(QualificationCheck qualificationCheck){
		QualificationCheckDto dto = new QualificationCheckDto();
		BeansUtil.copyBean(dto, qualificationCheck, true);
		if(qualificationCheck!=null){
			if(qualificationCheck.getCreateTime()!=null){
				dto.setCreatedTime(CommonMethod.CalendarToString(qualificationCheck.getCreateTime(), "yyyy-MM-dd"));
			}
		}
		return dto;
	}

	@Override
	public QualificationCheck get(Long id) {
		return qualificationCheckDao.get(id);
	}

	@Override
	public QualificationCheckDto getQualificationCheck(Long invoiceId) {
		return castDto(qualificationCheckDao.getQualificationCheck(invoiceId));
	}

}

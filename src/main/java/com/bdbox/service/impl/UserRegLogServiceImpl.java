package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.UserRegLogDto;
import com.bdbox.dao.UserRegLogDao;
import com.bdbox.entity.UserRegLog;
import com.bdbox.service.UserRegLogService;
import com.bdbox.util.LogUtils;
import com.bdsdk.json.ListParam;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

/**
 * 用户注册记录逻辑业务处理实现类
 * 
 * @ClassName: UserRegLogServiceImpl 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-2-2 下午3:28:56 
 * @version V5.0 
 */
@Service
public class UserRegLogServiceImpl implements UserRegLogService {
	/**
	 * 用户注册记录DAO
	 */
	@Autowired
	public UserRegLogDao userRegLogDao;

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.UserRegLogService#save(com.bdbox.entity.UserRegLog)
	 */
	@Override
	public UserRegLog save(UserRegLog userRegLog) {
		try{
			return userRegLogDao.save(userRegLog);
		}catch(Exception e){
			LogUtils.logerror("对象保存用户注册记录错误", e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.UserRegLogService#save
	 */
	@Override
	public UserRegLog save(String username, String ip, String ipArea,
			Long userid) {
		try{
			UserRegLog userRegLog = new UserRegLog();
			userRegLog.setIp(ip);
			userRegLog.setIpArea(ipArea);
			userRegLog.setUserid(userid);
			userRegLog.setUsername(username);
			return userRegLogDao.save(userRegLog);
		}catch(Exception e){
			LogUtils.logerror("保存用户注册记录错误", e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.UserRegLogService#deleteUserRegLog(long)
	 */
	@Override
	public boolean deleteUserRegLog(long id) {
		try{
			userRegLogDao.delete(id);
			return true;
		}catch(Exception e){
			LogUtils.logerror("根据id删除用户注册记录错误", e);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.UserRegLogService#deleteUserRegLog(com.bdbox.entity.UserRegLog)
	 */
	@Override
	public boolean deleteUserRegLog(UserRegLog userRegLog) {
		try{
			userRegLogDao.deleteObject(userRegLog);
			return true;
		}catch(Exception e){
			LogUtils.logerror("根据对象删除用户注册记录错误", e);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.UserRegLogService#updateUserRegLog(com.bdbox.entity.UserRegLog)
	 */
	@Override
	public boolean updateUserRegLog(UserRegLog userRegLog) {
		try{
			userRegLogDao.update(userRegLog);
			return true;
		}catch(Exception e){
			LogUtils.logerror("修改用户注册记录错误", e);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.UserRegLogService#get(long)
	 */
	@Override
	public UserRegLogDto get(long id) {
		try{
			return castFrom(userRegLogDao.get(id));
		}catch(Exception e){
			LogUtils.logerror("根据id获取用户注册记录错误", e);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.UserRegLogService#listUserRegLog
	 */
	@Override
	public List<UserRegLogDto> listUserRegLog(String username, String ip,
			String ipArea, Long userid, Calendar startTime, Calendar endTime,
			int page, int pageSize) {
		List<UserRegLogDto> dtos = new ArrayList<UserRegLogDto>();
		try{
			List<UserRegLog> userRegLogs = userRegLogDao.listUserRegLog(username, ip, ipArea, startTime, endTime, userid, page, pageSize);
			for(UserRegLog userRegLog:userRegLogs){
				UserRegLogDto dto = new UserRegLogDto();
				dto = castFrom(userRegLog);
				dtos.add(dto);
			}
		}catch(Exception e){
			LogUtils.logerror("条件查询List格式的用户注册记录错误", e);
		}
		return dtos;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.UserRegLogService#count
	 */
	@Override
	public int count(String username, String ip, String ipArea, Long userid,
			Calendar startTime, Calendar endTime) {
		try{
			return userRegLogDao.count(username, ip, ipArea, startTime, endTime, userid);
		}catch(Exception e){
			LogUtils.logerror("条件统计用户注册记录数量", e);
		}
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.UserRegLogService#listUserRegLogJq
	 */
	@Override
	public ListParam listUserRegLogJq(String username, String ip,
			String ipArea, Long userid, Calendar startTime, Calendar endTime,
			int page, int pageSize) {
		List<UserRegLogDto> dtos = new ArrayList<UserRegLogDto>();
		int count = 0;
		try{
			List<UserRegLog> logs = new ArrayList<UserRegLog>();
			count = userRegLogDao.count(username, ip, ipArea, startTime, endTime, userid);
			logs = userRegLogDao.listUserRegLog(username, ip, ipArea, startTime, endTime, userid, page, pageSize);
			for(UserRegLog userRegLog:logs){
				UserRegLogDto dto = new UserRegLogDto();
				dto = castFrom(userRegLog);
				dtos.add(dto);
			}
		}catch(Exception e){
			LogUtils.logerror("条件查询符合Jquery.datatable格式的用户注册记录错误", e);
		}
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(pageSize);
		listParam.setiDisplayStart(page);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setAaData(dtos);
		return listParam;
	}
	
	/**
	 * 类型转换，实体转DTO
	 * @param userRegLog
	 * @return
	 */
	private UserRegLogDto castFrom(UserRegLog userRegLog){
		UserRegLogDto dto = new UserRegLogDto();
		BeansUtil.copyBean(dto, userRegLog, true);
		dto.setCreatedTimeStr(CommonMethod.CalendarToString(userRegLog.getCreatedtime(), "yyyy-MM-dd HH:mm:ss"));
		return dto;
	}

}

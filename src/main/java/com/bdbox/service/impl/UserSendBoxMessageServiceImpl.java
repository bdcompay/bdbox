package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;  

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service; 

import com.bdbox.dao.BoxDao;
import com.bdbox.dao.UserSendBoxMessageDao;
import com.bdbox.entity.Box;
import com.bdbox.entity.User;
import com.bdbox.entity.UserSendBoxMessage;
import com.bdbox.service.UserSendBoxMessageService;
import com.bdbox.web.dto.SdkUserSendDto;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;
@Service
public class UserSendBoxMessageServiceImpl implements UserSendBoxMessageService {
	@Autowired
	private UserSendBoxMessageDao sdkUserSendDao;
	@Autowired
	private BoxDao  boxDao;
	public List<SdkUserSendDto> sdkUserSend(Long serialNumber,Calendar startTime,Calendar endTime,String order, Integer page,Integer pageSize){
		// TODO Auto-generated method stub
		//查询盒子对于的用户
		List<UserSendBoxMessage> list; 
		/*Long userid=null;
		if(serialNumber!=0){
			List<Box> box=boxDao.queryBoxId(serialNumber);
			if(box.size()!=0){
				for(Box o:box){
					User user=o.getUser();
					 userid=user.getId();
				}
			}
		}
		*/
		if(serialNumber==0){
			serialNumber=null;
		}  
		list = sdkUserSendDao.sdkUserSend(serialNumber ,startTime,endTime,null, order , page, pageSize); 
		List<SdkUserSendDto> sdkUserSendDto = new ArrayList<SdkUserSendDto>();
		for (UserSendBoxMessage userSendBoxMessage : list) {
			sdkUserSendDto.add(switchDto(userSendBoxMessage));
		} 
		return sdkUserSendDto;
	}
	
	private SdkUserSendDto switchDto(UserSendBoxMessage userSendBoxMessage){
		
	    SdkUserSendDto sdkUserSendDto = new SdkUserSendDto(); 
		BeansUtil.copyBean(sdkUserSendDto, userSendBoxMessage, true); 
		if(userSendBoxMessage.getFromUser()!=null){
			sdkUserSendDto.setFromUser(userSendBoxMessage.getFromUser().getUsername());
		}
		if(userSendBoxMessage.getIsRead()==true){
			sdkUserSendDto.setIsRead("1");
		}else{
			sdkUserSendDto.setIsRead("0");
		}	
		if(userSendBoxMessage.getIsReceived()==true){
			sdkUserSendDto.setIsReceived("1");;
		}else{
			sdkUserSendDto.setIsReceived("0");;
		}
		if(sdkUserSendDto.getFromUser()==null||sdkUserSendDto.getFromUser().equals("")){
			sdkUserSendDto.setFromUser(userSendBoxMessage.getEntUser().getName()+"("+userSendBoxMessage.getEntUser().getPhone()+")");
		}
		if(userSendBoxMessage.getSendTime()!=null){
			sdkUserSendDto.setSendTime(CommonMethod.CalendarToString(userSendBoxMessage.getSendTime(), "yyyy/MM/dd HH:mm"));
		}
		if(userSendBoxMessage.getCreatedTime()!=null){
			sdkUserSendDto.setCreatedTime(CommonMethod.CalendarToString(userSendBoxMessage.getCreatedTime(), "yyyy/MM/dd HH:mm"));
		} 
		sdkUserSendDto.setBoxSerialNumber(userSendBoxMessage.getBoxSerialNumber());  
		sdkUserSendDto.setMsgId(String.valueOf(userSendBoxMessage.getMsgId()));
		return sdkUserSendDto;
	}
	
	public Integer count(Long serialNumber,Calendar startTime,Calendar endTime){
	 /*
		List<UserSendBoxMessage> list;
		Long userid=null;
		if(serialNumber!=0){
			List<Box> box=boxDao.queryBoxId(serialNumber);
			if(box.size()!=0){
				for(Box o:box){
					User user=o.getUser();
					 userid=user.getId();
				}
			}
		}
		*/
		if(serialNumber==0){
			serialNumber=null;
		}   
		return sdkUserSendDao.count(serialNumber,startTime,endTime,null); 
	}
	
	  public void deleteSdkUserSend(long id){
		  sdkUserSendDao.delete(Long.valueOf(id));
	  }

	@Override
	public void saveUserSendboxMessage(UserSendBoxMessage userSendboxMessage) {
		// TODO Auto-generated method stub
		sdkUserSendDao.save(userSendboxMessage);
	}



}

package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.DialrecordDto;
import com.bdbox.dao.DialrecordDao;
import com.bdbox.entity.Dialrecord;
import com.bdbox.service.DialrecordService;
import com.bdsdk.util.CommonMethod;
/**
 * 待拨号号码
 * @author zouzhiwen
 *
 */
@Service
public class DialrecordServiceImpl implements DialrecordService {
	@Autowired
	public DialrecordDao dialrecordDao;

	@Override
	public Dialrecord saveDialrecord(Dialrecord dialrecord) {
		// TODO Auto-generated method stub
		return dialrecordDao.save(dialrecord);
	}

	@Override
	public void saveOrUpdateDialrecord(Dialrecord dialrecord) {
		// TODO Auto-generated method stub
		 dialrecordDao.saveOrUpdate(dialrecord);
	}

	@Override
	public void deleteDialrecord(Dialrecord dialrecord) {
		// TODO Auto-generated method stub
		 dialrecordDao.delete(dialrecord.getId());
	}

	@Override
	public List<Dialrecord> queryDialrecord(String dialNum,Boolean hasSend,String boxSerialNumber,Integer page, Integer pageSize) {
		// TODO Auto-generated method stub
		StringBuffer hql = new StringBuffer("from Dialrecord t where 1=1");
		
		if (dialNum != null) {
			hql.append(" and t.dialNum='").append(dialNum.toString())
					.append("'");
		}
		if (boxSerialNumber != null) {
			hql.append(" and t.boxSerialNumber='").append(boxSerialNumber.toString())
					.append("'");
		}
		if (hasSend != null) {
			hql.append(" and t.hasSend='").append(hasSend.toString())
					.append("'");
		}
		List<Dialrecord> lt=dialrecordDao.getList(hql.toString(), page, pageSize);
		
		return lt;
	}
	
	@Override
	public List<DialrecordDto> queryDialrecordDto(String dialNum,Boolean hasSend,String boxSerialNumber,Integer page, Integer pageSize) {
		// TODO Auto-generated method stub
		List<Dialrecord> lt=queryDialrecord(dialNum,hasSend,boxSerialNumber, page, pageSize);
		List<DialrecordDto> lto=new ArrayList<DialrecordDto>();
		for(int i=0;i<lt.size();i++){
			Dialrecord dialrecord=lt.get(i);
			DialrecordDto dialrecordDto=new DialrecordDto();
			dialrecordDto=castDialrecordToDialrecordDto(dialrecord);
			lto.add(dialrecordDto);
		}
		return lto;
	}
	
	/**
	 * 转成DTO
	 * @param dialrecord
	 * @return
	 */
	public DialrecordDto castDialrecordToDialrecordDto(Dialrecord dialrecord){
		DialrecordDto dialrecordDto=new DialrecordDto();
		dialrecordDto.setBoxSerialNumber(dialrecord.getBoxSerialNumber());
		dialrecordDto.setCreatedtime((CommonMethod.CalendarToString(dialrecord.getCreatedtime(), "yyyy-MM-dd HH:mm:ss")));
		dialrecordDto.setDialNum(dialrecord.getDialNum());
		dialrecordDto.setMsgID(dialrecord.getId());
		dialrecordDto.setMusicUrl(dialrecord.getMusicUrl());
		return dialrecordDto;
	}

	@Override
	public Dialrecord getDialrecord(Long id) {
		// TODO Auto-generated method stub
		return dialrecordDao.get(id);
	}

	@Override
	public void updateDialrecord(Dialrecord dialrecord) {
		// TODO Auto-generated method stub
		dialrecordDao.update(dialrecord);
	}
}

package com.bdbox.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service; 

import com.bdbox.api.dto.FAQDto;
import com.bdbox.api.dto.NotificationDto;
import com.bdbox.constant.MailType;
import com.bdbox.constant.NotificationType;
import com.bdbox.constant.QuestionType;
import com.bdbox.dao.FAQDao;
import com.bdbox.dao.NotificationDao;
import com.bdbox.entity.FAQ;
import com.bdbox.entity.Notification;
import com.bdbox.service.NotificationService;
import com.bdbox.util.LogUtils;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;
@Service
public class NotificationServiceImpl implements NotificationService {
	@Autowired
	private NotificationDao notificationDao;
	
	public List<Notification> getlistNotification(String tongzhifanshi,String tongzhileixing,String type_twl,int page, int pageSize){
		return notificationDao.getlistNotification(tongzhifanshi,tongzhileixing,type_twl,page,pageSize); 
	}

	public Integer getlistNotificationcount(String tongzhifanshi,String tongzhileixing,String type_twl){
		return notificationDao.getlistNotification(tongzhifanshi,tongzhileixing,type_twl); 
	} 
	
	public Notification saveNotification(String isemail,String tongzhileixing,String frommail, String tomail,String subject,String center,String note,String tomailname,String TeliPhone,String notecenter,String type_twl){
		Notification notification=new Notification(); 
		if("admin".equals(type_twl)){
			if("".equals(tomail)){
				return notification;
			}
			if("".equals(subject)){
				return notification;
			}
			if("".equals(center)){
				return notification;
			}
		}
		if("".equals(tongzhileixing)){
			return notification;
		}
		if("".equals(frommail)){
			return notification;
		}  
		MailType maitype=MailType.switchStatus(tongzhileixing);
		if("1".equals(isemail)){
			notification.setIsemail(true);
		}
		if("1".equals(note)){
			notification.setIsnote(true);
		} 
		notification.setMailType(maitype);
		notification.setCenter(center);
		notification.setFrommail(frommail);
		notification.setTomail(tomail); 
		notification.setSubject(subject);
		notification.setName(tomailname);
		notification.setReceiveTeliPhone(TeliPhone);
		notification.setNotecenter(notecenter);
		notification.setTypetwl(type_twl);
 		notificationDao.save(notification);
		return notification;
	}

	public Notification getNotification(long id){
		return notificationDao.get(id);
	}
	
	public NotificationDto castFrom(Notification notification){
		NotificationDto dto = new NotificationDto();
		BeansUtil.copyBean(dto, notification, true);
		dto.setId(String.valueOf(notification.getId()));
		if(notification.getIsemail().equals("1")){
			dto.setIsemail("1");
		}
		if(notification.getIsnote().equals("1")){
			dto.setIsnote("1");
		}
		if(notification.getCreateTime()!=null){
			dto.setCreateTime(CommonMethod.CalendarToString(notification.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
 		}
		if(notification.getMailType().equals(MailType.APPLYRENTRETURN)){
			dto.setMailType("申请归还通知");  
		}else if(notification.getMailType().equals(MailType.APPLYRETURN)){
			dto.setMailType("申请退货通知"); 
		}else if(notification.getMailType().equals(MailType.BUTREFUND)){
			dto.setMailType("购买申请退款通知"); 
	    }else if(notification.getMailType().equals(MailType.BUYAPPLYRENTRETURN)){
		   dto.setMailType("退货审核申请通过通知"); 
        }else if(notification.getMailType().equals(MailType.BUYORDERNOTPAY)){
			dto.setMailType("购买下单通知"); 
	    }else if(notification.getMailType().equals(MailType.BUYPAY)){
			dto.setMailType("购买付款通知"); 
	    }else if(notification.getMailType().equals(MailType.BUYRENT)){
			dto.setMailType("退货退款"); 
		}else if(notification.getMailType().equals(MailType.BUYSIGN)){
			dto.setMailType("购买签收通知"); 
	    }else if(notification.getMailType().equals(MailType.DAOQITIXIN)){
		   dto.setMailType("到期通知"); 
        }else if(notification.getMailType().equals(MailType.FAPIAOSHENHE)){
			dto.setMailType("发票审核不通过通知"); 
	    }else if(notification.getMailType().equals(MailType.FAPIAOSHENHEPASS)){
			dto.setMailType("发票审核通过通知"); 
	    }else if(notification.getMailType().equals(MailType.RENTAPPLYRENTRETURN)){
			dto.setMailType("归还审核申请通过通知"); 
		}else if(notification.getMailType().equals(MailType.RENTORDERNOTPAY)){
			dto.setMailType("租赁下单通知"); 
	    }else if(notification.getMailType().equals(MailType.RENTPAY)){
		   dto.setMailType("租赁付款通知"); 
        }else if(notification.getMailType().equals(MailType.RENTREFUND)){
			dto.setMailType("租赁申请退款通知"); 
	    }else if(notification.getMailType().equals(MailType.RENTRETURN)){
			dto.setMailType("归还退款"); 
	    }else if(notification.getMailType().equals(MailType.RENTSEND)){
			dto.setMailType("租赁发货通知"); 
	    }else if(notification.getMailType().equals(MailType.RENTSIGN)){
			dto.setMailType("租赁签收通知"); 
	    }else if(notification.getMailType().equals(MailType.RETURNSIGN)){
			dto.setMailType("填写归还单通知"); 
		}else if(notification.getMailType().equals(MailType.SHIMINGRENZHENG)){
			dto.setMailType("实名认证审核未通过通知"); 
	    }else if(notification.getMailType().equals(MailType.SHIMINGRENZHENGPASS)){
		   dto.setMailType("实名认证审核通过通知"); 
        }else if(notification.getMailType().equals(MailType.WRITERETURN)){
			dto.setMailType("填写退货单通知"); 
	    }else if(notification.getMailType().equals(MailType.XUQITIXIN)){
			dto.setMailType("续期通知"); 
	    }else if(notification.getMailType().equals(MailType.buySEND)){
			dto.setMailType("购买发货通知"); 
	    }else if(notification.getMailType().equals(MailType.BUYSTOCK)){
			dto.setMailType("库存不足通知（购买）"); 
	    }else if(notification.getMailType().equals(MailType.RENTSTOCK)){
			dto.setMailType("库存不足通知（租赁）"); 
	    }else if(notification.getMailType().equals(MailType.SHIMZHI)){
			dto.setMailType("实名制审核通知"); 
	    }else if(notification.getMailType().equals(MailType.FPIAO)){
			dto.setMailType("发票审核通知"); 
	    }else if(notification.getMailType().equals(MailType.BUYDAOHUO)){
			dto.setMailType("到货通知（购买）"); 
	    }else if(notification.getMailType().equals(MailType.RENTDAOHUO)){
			dto.setMailType("到货通知（租赁）"); 
	    }else if(notification.getMailType().equals(MailType.ACTIVTI)){
			dto.setMailType("活动到期时间通知"); 
	    }else if(notification.getMailType().equals(MailType.ONTHOUR)){
			dto.setMailType("下单1小时未付款通知"); 
	    }else if(notification.getMailType().equals(MailType.TENHOUR)){
			dto.setMailType("下单10小时未付款通知"); 
	    }else if(notification.getMailType().equals(MailType.tuihuoshenqingtongguo)){
			dto.setMailType("退货审核通过退款"); 
	    }else if(notification.getMailType().equals(MailType.guihuanshenqingtongguo)){
			dto.setMailType("归还审核通过退款"); 
	    }else if(notification.getMailType().equals(MailType.submitquestion)){
			dto.setMailType("用户提交问题"); 
	    }else if(notification.getMailType().equals(MailType.solvequestion)){
			dto.setMailType("解答用户问题"); 
	    }
		/*if(notification.getNotificationType().toString().equals(NotificationType.MAIL)){
			dto.setNotificationType("邮件");
		}else{
			dto.setNotificationType("短信");
		}*/
		if(notification.getIsemail().equals(true)){
			dto.setIsemail("邮件");
		}
		if(notification.getIsnote().equals(true)){
			dto.setIsnote("短信");
		}
		return dto;
	}
	
	public boolean update(long id,String isemail,String  note,String tongzhileixing,String frommail,String upedatetomailname,String updateemail,String updateTeliPhone,String updatesubject,String updatecenter,String updatenotecenter){
		try {
 			Notification notification = notificationDao.get(id);
			MailType maitype=MailType.switchStatus(tongzhileixing);
			if("1".equals(isemail)){
				notification.setIsemail(true);
			}
			if("1".equals(note)){
				notification.setIsnote(true);
			} 
			notification.setMailType(maitype);
			notification.setCenter(updatecenter);
			notification.setFrommail(frommail);
			notification.setTomail(updateemail); 
			notification.setSubject(updatesubject);
			notification.setName(upedatetomailname);
			notification.setReceiveTeliPhone(updateTeliPhone);
			notification.setNotecenter(updatenotecenter);
			LogUtils.loginfo("修改ID为：" + id + " 的通知");
			return true;
		} catch (Exception e) {
			LogUtils.logerror("修改ID为：" + id + " 的通知失败", e);
		}
		return false;
	}
	
	public boolean deleteNotification(Long id){
		try {
			notificationDao.delete(id);
			LogUtils.loginfo("删除ID为：" + id + " 的通知成功");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.logerror("删除ID为：" + id + " 的通知失败", e);
		}
		return false;
	}
	public Notification getnotification(String mailtype,String type_twl)
	{
		return notificationDao.getnotification(mailtype,type_twl);
	}
	
	public boolean updateNotification(Notification notification){
		notificationDao.update(notification);
		return true;
	}


}

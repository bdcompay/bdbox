package com.bdbox.service.impl;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.bouncycastle.jce.provider.JDKDSASigner.noneDSA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.consts.GlobalVar;
import com.bdbox.dao.EntUserPushLocationDao;
import com.bdbox.dao.EnterpriseUserDao;
import com.bdbox.entity.EntUserPushLocation;
import com.bdbox.entity.EnterpriseUser;
import com.bdbox.service.EntUserPushLocationService;
import com.bdbox.web.dto.EntUserPushLocationDto;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Service
public class EntUserPushLocationServiceImpl implements EntUserPushLocationService {
	@Autowired
	private EntUserPushLocationDao dao;
	@Autowired
	private EnterpriseUserDao entDao;

	@Override
	public void save(EntUserPushLocation entUserPushLocation) {
		dao.save(entUserPushLocation);
	}

	@Override
	public EntUserPushLocation get(Long id) {
		return dao.get(id);
	}

	@Override
	public void update(EntUserPushLocation entUserPushLocation) {
		dao.update(entUserPushLocation);
	}

	@Override
	public List<EntUserPushLocation> getUnsend() {
		return dao.query(null, null, GlobalVar.ENTERPRISE_USER_PUSH_LOCATION_COUNT, false, null, null, 1, 99999);
	}

	@Override
	public List<EntUserPushLocationDto> getDtos(String entUser, String startTime, String endTime, Integer count,
			Boolean isSuccess,int page,int pageSize) {
		EnterpriseUser enterpriseUser=null;
		if(entUser!=null && !"".equals(entUser)){
			enterpriseUser=entDao.query(entUser);
		}
		Long entUserId=null;
		if(enterpriseUser!=null){
			entUserId=enterpriseUser.getId();
		}
		Calendar sTime=Calendar.getInstance();
		Calendar eTime=Calendar.getInstance();
		try {
			sTime = CommonMethod.StringToCalendar(startTime, "yyyy-MM-dd HH:mm:ss");
			eTime = CommonMethod.StringToCalendar(endTime, "yyyy-MM-dd HH:mm:ss");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		List<EntUserPushLocation> list=dao.query(entUserId, null, count, isSuccess, sTime, eTime, page, pageSize);
		List<EntUserPushLocationDto> dtos=new ArrayList<>(0);
		if(list!=null){
			for(EntUserPushLocation e:list){
				dtos.add(case2Dto(e));
			}
		}
		return dtos;
	}

	@Override
	public int amount(String entUser, String startTime, String endTime, Integer count, Boolean isSuccess) {
		EnterpriseUser enterpriseUser=null;
		if(entUser!=null && !"".equals(entUser)){
			enterpriseUser=entDao.query(entUser);
		}
		Long entUserId=null;
		if(enterpriseUser!=null){
			entUserId=enterpriseUser.getId();
		}
		Calendar sTime=Calendar.getInstance();
		Calendar eTime=Calendar.getInstance();
		try {
			sTime = CommonMethod.StringToCalendar(startTime, "yyyy-MM-dd HH:mm:ss");
			eTime = CommonMethod.StringToCalendar(endTime, "yyyy-MM-dd HH:mm:ss");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		return dao.amount(entUserId, null, count, isSuccess, sTime, eTime);
	}

	private EntUserPushLocationDto case2Dto(EntUserPushLocation entUserPushLocation){
		EntUserPushLocationDto dto=new EntUserPushLocationDto();
		BeansUtil.copyBean(dto, entUserPushLocation, true);
		if(entUserPushLocation.getEnterpriseUser()!=null){
			dto.setEntUser(entUserPushLocation.getEnterpriseUser().getPhone());
		}
		if(entUserPushLocation.getCreateTime()!=null){
			dto.setCreateTime(CommonMethod.CalendarToString(entUserPushLocation.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
		}
		if(entUserPushLocation .getBoxLocation()!=null){
			String content="";
			content+="经度:"+entUserPushLocation.getBoxLocation().getLongitude();
			content+=" 纬度:"+entUserPushLocation.getBoxLocation().getLatitude();
			content+=" 高度:"+entUserPushLocation.getBoxLocation().getAltitude();
			if(null!=entUserPushLocation.getBoxLocation().getSpeed()){
				content+=" 速度:"+entUserPushLocation.getBoxLocation().getSpeed();
			}
			if(null!=entUserPushLocation.getBoxLocation().getDirection()){
				content+=" 方向:"+entUserPushLocation.getBoxLocation().getDirection();
			}
			dto.setContent(content);
		}
		return dto;
	}
}

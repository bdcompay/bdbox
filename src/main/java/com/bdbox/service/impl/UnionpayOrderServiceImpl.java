package com.bdbox.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.alipay.util.UtilDate;
import com.bdbox.constant.DealStatus;
import com.bdbox.constant.PayCardType;
import com.bdbox.constant.UnionpayRespCode;
import com.bdbox.constant.UnionpayType;
import com.bdbox.dao.InvoiceDao;
import com.bdbox.dao.NotificationDao;
import com.bdbox.dao.OrderDao;
import com.bdbox.dao.UnionpayOrderDao;
import com.bdbox.entity.Invoice;
import com.bdbox.entity.Notification;
import com.bdbox.entity.Order;
import com.bdbox.entity.UnionpayOrder;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.UnionpayOrderService;
import com.bdsdk.util.CommonMethod;

@Service
public class UnionpayOrderServiceImpl implements UnionpayOrderService{
	
	@Autowired
	private UnionpayOrderDao unionpayOrderDao;
	
	@Autowired
	private OrderDao orderDao;
	
	@Autowired 
	private NotificationDao notificationDao;
	
	@Autowired  
	private MailNotification mailNotification; 
	
	@Autowired
	private InvoiceDao invoiceDao;  

	private static String flag1="";//标记发邮件的次数，每个支付订单只发一次

	@Override
	public void save(Map<String, String> rspData) {
		try {
			boolean isNull = false;
			UnionpayOrder uorder = unionpayOrderDao.getUnionpayOrder(rspData.get("orderId"));
			if(uorder==null){
				uorder = new UnionpayOrder();
				isNull = true;
			}
			String respCode = rspData.get("respCode");
			uorder.setOrderId(rspData.get("orderId"));
			uorder.setTxnAmt(Double.parseDouble(rspData.get("txnAmt")));
			uorder.setTxnTime(CommonMethod.StringToCalendar(rspData.get("txnTime"), UtilDate.dtLong));
			uorder.setQueryId(rspData.get("queryId"));
			uorder.setTraceNo(rspData.get("traceNo"));
			uorder.setTraceTime(rspData.get("traceTime"));
			uorder.setRespCode(UnionpayRespCode.switchStatus(respCode));
			uorder.setRespMsg(rspData.get("respMsg"));
			uorder.setAccNo(rspData.get("accNo"));
			uorder.setPayCardType(PayCardType.switchStatus(rspData.get("payCardType")));
			uorder.setUnionpayType(UnionpayType.switchStatus(rspData.get("payType")));
			uorder.setSettleDate(rspData.get("settleDate"));
			uorder.setSettleAmt(rspData.get("settleAmt"));
			//防止重复保存订单信息
			if(isNull){
				//保存订单信息
				unionpayOrderDao.save(uorder);
			}else{
				//更新订单信息
				unionpayOrderDao.update(uorder);
			}
			//修改商户订单信息
			if("00".equals(respCode)){
				Order order = orderDao.getOrderstatus(rspData.get("orderId"));
				order.setDealStatus(DealStatus.PAID);
				orderDao.update(order);
				//订单支付成功后，给管理员发送邮件通知
				StringBuffer content=new StringBuffer(); 
				String type="BUYPAY";  //通知类型
				String type_twl="admin"; 
				String invoices="";            //是否开发票
				String tomail=null;            //收信箱
				String subject=null;           //主题
				String mailcentent=null;       //内容
				if(!order.getOrderNo().equals(flag1)){ 
		    		flag1 = order.getOrderNo(); 
					if("租用".equals(order.getTransactionType())){ 
			    		type="RENTPAY";
		    			Notification notification = notificationDao.getnotification(type,type_twl);
		    			if(notification!=null){ 
		    				if(notification.getIsemail().equals(true)){   
	    						tomail=notification.getTomail();
	    						subject=notification.getSubject();  
	    						content.append(notification.getCenter())
	    						.append("订单号："+order.getOrderNo()+","
	    						+ "下单时间："+CommonMethod.CalendarToString(order.getCreatedTime(), "yyyy-MM-dd HH:mm:ss")+"，"
	    						+ "下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，"
	    						+ "联系方式："+order.getPhone()).append(invoices);
	    						if(content!=null){
	    							mailcentent=content.toString(); 
	    						}
	    						String [] email=notification.getTomail().split(",");
	    						for(String str:email){
	    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
	    						}
		    				}
		    			}
		            }else{
		            	Notification notification=notificationDao.getnotification(type,type_twl);
		    			if(notification!=null){ 
		    				if(notification.getIsemail().equals(true)){  
	    						tomail=notification.getTomail();
	    						subject=notification.getSubject(); 
	    						if(order.getInvoice()!=0){
	    							Invoice enty = invoiceDao.getInvoice(order.getInvoice());
	    							invoices="，发票抬头："+enty.getCompanyName();
	    						}
	    						content.append(notification.getCenter()+":")
	    						.append("订单号："+order.getOrderNo()+","
	    						+ "下单时间："+CommonMethod.CalendarToString(order.getCreatedTime(), "yyyy-MM-dd HH:mm:ss")+"，"
	    						+ "下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，"
	    						+ "联系方式："+order.getPhone()).append(invoices);
	    						if(content!=null){
	    							mailcentent=content.toString(); 
	    						}
	    						String [] email=tomail.split(",");
	    						for(String str:email){
	    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
	    						}	
		    				}
		    			} 
		            }
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(Map<String, String> rspData) {
		
	}

	@Override
	public void delete(UnionpayOrder unionpayOrder) {
		unionpayOrderDao.delete(unionpayOrder.getId());
	}

	@Override
	public UnionpayOrder getUnionpayOrder(String orderId) {
		return unionpayOrderDao.getUnionpayOrder(orderId);
	}

}

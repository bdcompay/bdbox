package com.bdbox.service.impl;

import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.dao.FamilyStatusDao;
import com.bdbox.entity.Box;
import com.bdbox.entity.FamilyStatus;
import com.bdbox.entity.User;
import com.bdbox.service.FamilyStatusService;

@Service
public class FamilyStatusServiceImpl implements FamilyStatusService {

	@Autowired
	private FamilyStatusDao familyStatusDao;

	public void updateFamilyStatus(FamilyStatus familyStatus) {
		// TODO Auto-generated method stub
		familyStatusDao.update(familyStatus);

	}

	public FamilyStatus saveFamilyStatus(FamilyStatus familyStatus) {
		// TODO Auto-generated method stub
		return familyStatusDao.save(familyStatus);
	}

	public FamilyStatus getFamilyStatus(Long boxId,String familyMob) {
		// TODO Auto-generated method stub
		List<FamilyStatus> familyStatus = familyStatusDao.query(boxId,
				familyMob, 1, 1);
		if (familyStatus != null && familyStatus.size() != 0) {
			return familyStatus.get(0);
		}
		return null;
	}

	public void updateFamilyStatus(Box box, String familyMob) {
		// TODO Auto-generated method stub
		// 更新sms消息状态
		FamilyStatus familyStatus = getFamilyStatus(box.getId(), null);
		if (familyStatus == null) {
			familyStatus = new FamilyStatus();
			familyStatus.setBox(box);
			familyStatus.setCreatedTime(Calendar.getInstance());
			familyStatus.setLastDataUpdateTime(Calendar.getInstance());
			familyStatus.setFamilyMob(familyMob);
			saveFamilyStatus(familyStatus);
		} else {
			familyStatus.setLastDataUpdateTime(Calendar.getInstance());
			familyStatus.setFamilyMob(familyMob);
			updateFamilyStatus(familyStatus);
		}
	}
}

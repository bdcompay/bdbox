package com.bdbox.service.impl;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.dao.EntStatDailyDao;
import com.bdbox.dao.EnterpriseUserDao;
import com.bdbox.entity.Box;
import com.bdbox.entity.EntStatDaily;
import com.bdbox.service.BoxService;
import com.bdbox.service.EntStatDailyService;
import com.bdbox.service.EnterpriseUserService;
import com.bdbox.util.LogUtils;
import com.bdbox.util.RemindDateUtils;
import com.bdbox.web.dto.EntStatDailyDto;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;

/**
 * 企业统计每日数据service实现类
 * @author jlj
 *
 */
@Service
public class EntStatDailyServiceImpl implements EntStatDailyService {
	
	@Autowired
	private EntStatDailyDao entStatDailyDao;
	@Autowired
	private BoxService boxService;
	@Autowired
	private EnterpriseUserDao enterpriseUserDao;

	/*
	 * 增加企业下属盒子每日发送数量
	 * (non-Javadoc)
	 * @see com.bdbox.service.EntStatDailyService#addSendNumber(java.lang.String)
	 */
	@Override
	public boolean addSendNumber(String fromCardNumber) {
		// TODO Auto-generated method stub
		try{
			//通过北斗卡号获取企业用户
			Box box = boxService.getBoxForCardNum(fromCardNumber);
			if(box == null ){
				LogUtils.loginfo("统计企业每日发送数量错误，卡号："+fromCardNumber+" 非内部北斗卡");
				return false;
			}
			if(box.getEntUser()==null){
				LogUtils.loginfo("统计企业每日发送数量错误，卡号："+fromCardNumber+" 未绑定企业");
				return false;
			}
			
			//查询企业本日记录
			EntStatDaily entStatDaily = entStatDailyDao.get(Calendar.getInstance(), box.getEntUser().getId());
			//增加发送数量
			if(entStatDaily ==null ){
				//无记录，创建一条新记录
				entStatDaily = new EntStatDaily();
				entStatDaily.setBoxSendNumber(1);
				entStatDaily.setDate(Calendar.getInstance());
				entStatDaily.setEnterpriseUser(box.getEntUser());
			}else{
				//发送数量加一
				entStatDaily.setBoxSendNumber(entStatDaily.getBoxSendNumber()+1);
			}
			//持久化到数据库
			entStatDailyDao.saveOrUpdate(entStatDaily);
			return true;
		}catch(Exception e){
			LogUtils.logerror("增加企业下属盒子每日发送数量错误", e);
		}
		return false;
	}

	/*
	 * 通过周名获取企业每日统计数据DTO
	 * (non-Javadoc)
	 * @see com.bdbox.service.EntStatDailyService#getByWeekName(java.lang.String, long)
	 */
	@Override
	public ObjectResult getByWeekName(String weekName, long entId) {
		// TODO Auto-generated method stub
		ObjectResult objectResult = new ObjectResult();
		try{
			//获取周起始时间
			Calendar startTime = RemindDateUtils.getWeekStartTime(weekName);
			if(startTime == null){
				objectResult.setMessage("weekName参数错误");
				objectResult.setStatus(ResultStatus.FAILED);
				objectResult.setResult(null);
				return objectResult;
			}
			
			//本周的最后一天
			Calendar entTime = (Calendar)startTime.clone();
			entTime.add(Calendar.DATE, 6);

			//查询本周数量
			List<EntStatDailyDto> dtos = entStatDailyDao.statistic(startTime, entTime, entId,1,1);
			if(dtos.size() == 1)
				objectResult.setResult(dtos.get(0));
			objectResult.setMessage("成功");
			objectResult.setStatus(ResultStatus.OK);
			
		}catch(ParseException e){
			LogUtils.logerror("通过周名获取企业每日统计数据DTO错误", e);
			objectResult.setMessage("weekName参数错误");
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setResult(null);
		}catch(Exception e){
			LogUtils.logerror("通过周名获取企业每日统计数据DTO错误", e);
			objectResult.setMessage("系统错误");
			objectResult.setStatus(ResultStatus.SYS_ERROR);
			objectResult.setResult(null);
		}
		return objectResult;
	}

	/*
	 * 通过月份获取企业每日统计数据
	 * (non-Javadoc)
	 * @see com.bdbox.service.EntStatDailyService#getByMonth(java.lang.String, long)
	 */
	@Override
	public ObjectResult getByMonth(String month, long entId) {
		// TODO Auto-generated method stub
		try{
			
		}catch(Exception e){
			LogUtils.logerror("通过月份获取企业每日统计数据DTO错误", e);
		}
		return null;
	}

	/*
	 * 通过时间区间分页统计用户使用数量
	 * (non-Javadoc)
	 * @see com.bdbox.service.EntStatDailyService#statisticData(java.util.Calendar, java.util.Calendar, long, int, int)
	 */
	@Override
	public List<EntStatDailyDto> statisticData(Calendar startDate, Calendar endDate,
			Long entId, int page, int pageSize) {
		// TODO Auto-generated method stub
		try{
			//查询本周数量
			List<EntStatDailyDto> dtos = entStatDailyDao.statistic(startDate, endDate, entId,page,pageSize);
			for(EntStatDailyDto dto:dtos){
				dto.setEnterpriseUser(enterpriseUserDao.get(dto.getEnterpriseUser().getId()));
			}
			return dtos;
		}catch(Exception e){
			LogUtils.logerror("通过时间区间分页统计用户使用数量错误", e);
		}
		return null;
	}
	
	public int count(Calendar startDate, Calendar endDate,Long entId){
		return entStatDailyDao.statisticCount(startDate, endDate, entId);
	}

}

package com.bdbox.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.dao.ExpressCompanyDao;
import com.bdbox.entity.ExpressCompany;
import com.bdbox.service.ExpressCompanyService;

@Service
public class ExpressCompanyServiceImpl implements ExpressCompanyService {
	
	@Autowired
	private ExpressCompanyDao expressCompanyDao;

	@Override
	public List<ExpressCompany> getAll() {
		return expressCompanyDao.listAll();
	}

}

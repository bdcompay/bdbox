package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.PhoneRestrictDto;
import com.bdbox.dao.PhoneRestrictDao;
import com.bdbox.entity.PhoneRestrict;
import com.bdbox.service.PhoneRestrictService;

@Service
public class PhoneRestrictServiceImpl implements PhoneRestrictService{

	@Autowired
	private PhoneRestrictDao phoneRestrictDao;
	
	@Override
	public void save(PhoneRestrict phoneRestrict) {
		phoneRestrictDao.save(phoneRestrict);
	}

	@Override
	public void delete(Long id) {
		phoneRestrictDao.delete(id);
	}

	@Override
	public void update(PhoneRestrict phoneRestrict) {
		phoneRestrictDao.update(phoneRestrict);
	}

	@Override
	public PhoneRestrict getPhone(String phoneNum) {
		return phoneRestrictDao.getPhone(phoneNum);
	}

	@Override
	public List<PhoneRestrictDto> getAll(int page, int pageSize) {
		List<PhoneRestrictDto> dtos = new ArrayList<>();
		List<PhoneRestrict> list = phoneRestrictDao.list(page, pageSize);
		for (PhoneRestrict phoneRestrict : list) {
			dtos.add(castToDto(phoneRestrict));
		}
		return dtos;
	}

	@Override
	public int count() {
		return phoneRestrictDao.count();
	}
	
	public PhoneRestrictDto castToDto(PhoneRestrict phoneRestrict){
		PhoneRestrictDto dto = new PhoneRestrictDto();
		dto.setId(phoneRestrict.getId());
		dto.setPhoneNum(phoneRestrict.getPhoneNum());
		dto.setRemark(phoneRestrict.getRemark());
		return dto;
	}

}

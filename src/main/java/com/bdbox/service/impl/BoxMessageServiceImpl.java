package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.BoxMessageDto;
import com.bdbox.constant.MsgIoType;
import com.bdbox.constant.MsgType;
import com.bdbox.dao.BoxDao;
import com.bdbox.dao.BoxMessageDao;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxMessage;
import com.bdbox.service.BoxMessageService;
import com.bdbox.util.RemindDateUtils;
import com.bdsdk.constant.DataStatusType;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Service
public class BoxMessageServiceImpl implements BoxMessageService {
	@Autowired
	private BoxMessageDao boxMessageDao;
	
	@Autowired
	private BoxDao boxDao;

	public void updateBoxMessage(BoxMessage boxMessage) {
		// TODO Auto-generated method stub
		boxMessageDao.update(boxMessage);
	}

	public void saveBoxMessage(BoxMessage boxMessage) {
		// TODO Auto-generated method stub
		if(boxMessage.getToBox() != null && boxMessage.getToBox().getUser() != null){
			boxMessage.setToUserId(boxMessage.getToBox().getUser().getId());
		}
		if(boxMessage.getFromBox() != null && boxMessage.getFromBox().getUser()!=null ){
			boxMessage.setFromUserId(boxMessage.getFromBox().getUser().getId());
		}
		boxMessageDao.save(boxMessage);
	}

	public List<BoxMessage> getUnReadBoxMessage(Long boxId, Calendar lastTime) {
		// TODO Auto-generated method stub
		// 寻找盒子到盒子的消息

		List<BoxMessage> boxMessages = boxMessageDao.query(null, null, null,
				null, null, null, boxId, null, lastTime, null, null, 0,
				"createdTime desc", 1, 50);
		return boxMessages;
	}

	public List<BoxMessageDto> listBoxMessage(Long fromBoxId,Long toBoxId,String boxName,long userId, String order,
			Integer page, Integer pageSize) {
		// TODO Auto-generated method stub
		List<BoxMessage> boxMessages = new ArrayList<BoxMessage>();
		if (fromBoxId!=null) {//发送信息
			boxMessages = boxMessageDao.query(null, null, null,null, null, fromBoxId, null, null, null, null, boxName, userId, order, page,pageSize);
		}else if (toBoxId!=null) {//接收信息
			boxMessages = boxMessageDao.query(null, null, null,null, null, null, toBoxId, null, null, null, boxName, userId, order, page,pageSize);
		}
		if (boxMessages.size() < 1)return null;// 小于1等于空

		List<BoxMessageDto> list = new ArrayList<BoxMessageDto>();
		for (BoxMessage bm : boxMessages) {
			list.add(switchDto(bm));
		}
		return list;
	}

	public Integer queryCount(Long fromBoxId,Long toBoxId,String boxName,long userId) {
		// TODO Auto-generated method stub
		return boxMessageDao.queryCount(null, null, null, null, null, fromBoxId,toBoxId, null, null, null, boxName, userId);
	}
	
	//
	public List<BoxMessageDto> listBoxMessage(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxId, Long toBoxId, String familyMail,
			Calendar startTime, Calendar endTime,String boxName,long userId, String entUserName,
			String order, int page,
			int pageSize) {
		
		List<BoxMessage> list = new ArrayList<BoxMessage>();
		List<BoxMessageDto> boxMessageDtos = new ArrayList<BoxMessageDto>();
		
		//不按企业用户查询
		if(entUserName==null){
			if((fromBoxId==null||fromBoxId==0)&&(toBoxId==null||toBoxId==0)){
				 list = boxMessageDao.query(msgId, msgType, msgIoType, dataStatusType, familyMob, null, null, familyMail, startTime, endTime, boxName, userId, order, page, pageSize);
				 for(BoxMessage boxMessage:list){
						boxMessageDtos.add(switchDto(boxMessage));
					}
			}else if((fromBoxId!=null&&fromBoxId!=0)&&(toBoxId!=null&&toBoxId!=0)){
				list = boxMessageDao.querys(msgId, msgType, msgIoType, dataStatusType, familyMob, fromBoxId, toBoxId, familyMail, startTime, endTime, boxName, userId, order, page, pageSize);
				for(BoxMessage boxMessage:list){
					boxMessageDtos.add(switchDto(boxMessage));
				}
			}else{
				if(toBoxId!=null&&toBoxId!=0){
					list = boxMessageDao.query(msgId, msgType, msgIoType, dataStatusType, familyMob, null, toBoxId, familyMail, startTime, endTime, boxName, userId, order, page, pageSize);
					for(BoxMessage boxMessage:list){
						boxMessageDtos.add(switchDto(boxMessage));
					}
				}
				if(fromBoxId!=null&&fromBoxId!=0){
					list = boxMessageDao.query(msgId, msgType, msgIoType, dataStatusType, familyMob, fromBoxId, null, familyMail, startTime, endTime, boxName, userId, order, page, pageSize);
					for(BoxMessage boxMessage:list){
						boxMessageDtos.add(switchDto(boxMessage));
					}
				}
			}
		}else{  //按企业用户查询
			list = boxMessageDao.querys(msgId, msgType, msgIoType, dataStatusType, familyMob, fromBoxId, toBoxId, familyMail, entUserName, startTime, endTime, boxName, userId, order, page, pageSize);
			for(BoxMessage boxMessage:list){
				boxMessageDtos.add(switchDto(boxMessage));
			}
		}
		
		return boxMessageDtos;
	}

	public Integer queryCount(Integer msgId, MsgType msgType,
			MsgIoType msgIoType, DataStatusType dataStatusType,
			String familyMob, Long fromBoxId, Long toBoxId, String familyMail,
			Calendar startTime, Calendar endTime,String boxName,long userId,String entUserName) {
		
		int count = 0;
		//不是企业用户查询
		if(entUserName==null){
			if((fromBoxId==null||fromBoxId==0)&&(toBoxId==null||toBoxId==0)){
				count = count+boxMessageDao.queryCount(msgId, msgType, msgIoType, dataStatusType, familyMob, null, null, familyMail, startTime, endTime, boxName, userId);
			}else if((fromBoxId!=null&&fromBoxId!=0)&&(toBoxId!=null&&toBoxId!=0)){
				count = count+boxMessageDao.queryCount(msgId, msgType, msgIoType, dataStatusType, familyMob, fromBoxId, toBoxId, familyMail, startTime, endTime, boxName, userId); 
			}
			else{
				if(toBoxId!=null&&toBoxId!=0){
					count = count+boxMessageDao.queryCount(msgId, msgType, msgIoType, dataStatusType, familyMob, null, toBoxId, familyMail, startTime, endTime, boxName, userId);
				}
				if(fromBoxId!=null&&fromBoxId!=0){
					count = count+boxMessageDao.queryCount(msgId, msgType, msgIoType, dataStatusType, familyMob, fromBoxId, null, familyMail, startTime, endTime, boxName, userId);
				}
			}
		}else{  //企业用户查询
			count = boxMessageDao.querysAmount(msgId, msgType, msgIoType, dataStatusType, familyMob, fromBoxId, toBoxId, familyMail, entUserName, startTime, endTime, boxName, userId);
		}
		
		return count;
	}

	/**
	 * POJO转换DTO
	 * 
	 * @param boxMessage
	 * @return
	 */
	private BoxMessageDto switchDto(BoxMessage boxMessage) {
		BoxMessageDto boxMessageDto = new BoxMessageDto();

		BeansUtil.copyBean(boxMessageDto, boxMessage, true);

		boxMessageDto.setCreatedTime(CommonMethod.CalendarToString(
				boxMessage.getCreatedTime(), "yyyy-MM-dd HH:mm"));

		//发送者：判断盒子发送、邮箱发送、手机发送
		if (boxMessage.getFromBox()!=null&&boxMessage.getFromBox().getCardNumber()!=null) {
			boxMessageDto.setFromBox("【盒子】"+boxMessage.getFromBox().getBoxSerialNumber());
		}else if(boxMessage.getFamilyMob()!=null&&!"".equals(boxMessage.getFamilyMob())){
			boxMessageDto.setFromBox("【手机】"+boxMessage.getFamilyMob());
		}else if (boxMessage.getFamilyMail()!=null&&!"".equals(boxMessage.getFamilyMail())) {
			boxMessageDto.setFromBox("【邮箱】"+boxMessage.getFamilyMail());
		}

		if(boxMessage.getDataStatusType()==null){
			//处理null异常
		}else if (boxMessage.getDataStatusType().toString().equals("SUCCESS")) {
			boxMessageDto.setDataStatusType("发送成功");
		} else if (boxMessage.getDataStatusType().toString().equals("FAIL")) {
			boxMessageDto.setDataStatusType("发送失败");
		} else if (boxMessage.getDataStatusType().toString().equals("WAIT")) {
			boxMessageDto.setDataStatusType("发送中");
		} else if (boxMessage.getDataStatusType().toString().equals("QUEUE")) {
			boxMessageDto.setDataStatusType("等待发送");
		} else if (boxMessage.getDataStatusType().toString().equals("SENT")) {
			boxMessageDto.setDataStatusType("发送");
		}
		
		if(boxMessage.getMsgIoType()==null){
			
		}else if (boxMessage.getMsgIoType().toString().equals("IO_BOXTOFAMILY")) {
			boxMessageDto.setMsgIoType("北斗到家人");
		} else if (boxMessage.getMsgIoType().toString()
				.equals("IO_FAMILYTOBOX")) {
			boxMessageDto.setMsgIoType("家人到北斗");
		} else if (boxMessage.getMsgIoType().toString().equals("IO_BOXTOBOX")) {
			boxMessageDto.setMsgIoType("北斗到队友");
		} else if (boxMessage.getMsgIoType().toString()
				.equals("IO_BOXTOSYSTEM")) {
			boxMessageDto.setMsgIoType("北斗到平台");
		} else if (boxMessage.getMsgIoType().toString().equals("IO_BOXTOALL")) {
			boxMessageDto.setMsgIoType("北斗到全部");
		} else if (boxMessage.getMsgIoType().toString()
				.equals("IO_BOXGETPARTNERLOCATION")) {
			boxMessageDto.setMsgIoType("北斗获取队友位置");
		}	else if (boxMessage.getMsgIoType().toString()
				.equals("IO_EntUserTOBOX")) {
			boxMessageDto.setMsgIoType("监控平台企业用户到盒子");
		}
		if(boxMessage.getMsgType()==null){
			
		}if (boxMessage.getMsgType().toString().equals("MSG_SOS")) {
			boxMessageDto.setMsgType("报警信息");
		} else if (boxMessage.getMsgType().toString().equals("MSG_LOCATION")) {
			boxMessageDto.setMsgType("位置信息");
		} else if (boxMessage.getMsgType().toString().equals("MSG_SAFE")) {
			boxMessageDto.setMsgType("报平安信息");
		}else if (boxMessage.getMsgType().toString().equals("MSG_RECEIVED")) {
			boxMessageDto.setMsgType("平台消息送达");
		}else if (boxMessage.getMsgType().toString().equals("MSG_READED")) {
			boxMessageDto.setMsgType("平台消息已阅");
		}else if (boxMessage.getMsgType().toString().equals("MSG_SDKSEND")) {
			boxMessageDto.setMsgType("平台下发");
		}else if (boxMessage.getMsgType().toString().equals("MSG_TASK")) {
			boxMessageDto.setMsgType("任务中");
		}else if (boxMessage.getMsgType().toString().equals("MSG_OK")) {
			boxMessageDto.setMsgType("OK消息");
		}

		if (boxMessage.getToBox()!=null&&boxMessage.getToBox().getCardNumber()!=null) {
			boxMessageDto.setToBox("【盒子】"+boxMessage.getToBox().getBoxSerialNumber());
		}else if(boxMessage.getFamilyMob()!=null&&!"".equals(boxMessage.getFamilyMob())){
			boxMessageDto.setToBox("【手机】"+boxMessage.getFamilyMob());
		}else if (boxMessage.getFamilyMail()!=null&&!"".equals(boxMessage.getFamilyMail())) {
			boxMessageDto.setToBox("【邮箱】"+boxMessage.getFamilyMail());
		}

		return boxMessageDto;
	}

	public void deleteBoxMessage(long id) {
		// TODO Auto-generated method stub
		boxMessageDao.delete(id);
	}

	@Override
	public List<BoxMessage> getLastRevieveBoxMessages(Long boxId, Integer page,
			Integer pageSize) {
		// TODO Auto-generated method stub
		List<BoxMessage> boxMessages =boxMessageDao.query(null, null, null,
				null, null, null, boxId, null, null, null, null, 0,
				"createdTime asc", page, pageSize);
		return boxMessages;
	}
	@Override
	public List<BoxMessage> getLastRevieveBoxMessages(Long boxId, MsgType msgType,
			Integer page, Integer pageSize) {
		// TODO Auto-generated method stub
		List<BoxMessage> boxMessages =boxMessageDao.querylastRevieved(null, msgType, null,
				null, null, null, boxId, null, null, null, null, 0,
				"createdTime desc", page, pageSize);
		return boxMessages;
	}
	//统计用户发送的北斗短报文数量
	public int countUserSendMessage(long userid,Calendar startTime,Calendar endTime){
		return boxMessageDao.countUserSendBdmsg(userid, startTime, endTime);
	}
	//统计用户接收的北斗短报文数量
	public int countUserRevMessage(long userid,Calendar startTime,Calendar endTime){
		return boxMessageDao.countUserRevBdmsg(userid, startTime, endTime);
	}
	//统计用发送给家人的短信数量
	public int countUserToFamily(long userid,Calendar startTime,Calendar endTime){
		return boxMessageDao.countUserToFamilyMsg(userid, startTime, endTime);
	}
	//统计家人回复给用户的短信数量
	public int countFamilyToUser(long userid,Calendar startTime,Calendar endTime){
		return boxMessageDao.countFamilyToUser(userid, startTime, endTime);
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdbox.service.BoxMessageService#statisticsUserMessage(long, java.util.Calendar, java.util.Calendar)
	 */
	@Override
	public Map<String, Integer> statisticsUserMessage(long userid,
			Calendar startTime, Calendar endTime) {
		// TODO Auto-generated method stub
		Map<String, Integer> map = new HashMap<String, Integer>();
		//统计条件查询时间内的数量
		int countUserSend = boxMessageDao.countUserSendBdmsg(userid, startTime, endTime);
		int countUserRev = boxMessageDao.countUserRevBdmsg(userid, startTime, endTime);
		int countToFamily = boxMessageDao.countUserToFamilyMsg(userid, startTime, endTime);
		int countToUser = boxMessageDao.countFamilyToUser(userid, startTime, endTime);
		map.put("userSend", countUserSend);
		map.put("userRev", countUserRev);
		map.put("toFamily", countToFamily);
		map.put("toUser", countToUser);
		//统计今天的数量
		Calendar todayStartTime = Calendar.getInstance();
		Calendar todayEndTime = Calendar.getInstance();
		todayStartTime.setTime(RemindDateUtils.getCurrentDayStartTime());
		todayEndTime.setTime(RemindDateUtils.getCurrentDayEndTime());
		
		int todayUserSend = boxMessageDao.countUserSendBdmsg(userid, todayStartTime, todayEndTime);
		int todayUserRev = boxMessageDao.countUserRevBdmsg(userid, todayStartTime, todayEndTime);
		int todayToFamily = boxMessageDao.countUserToFamilyMsg(userid, todayStartTime, todayEndTime);
		int todayToUser = boxMessageDao.countFamilyToUser(userid, todayStartTime, todayEndTime);
		
		map.put("todayUserSend", todayUserSend);
		map.put("todayUserRev", todayUserRev);
		map.put("todayToFamily", todayToFamily);
		map.put("todayToUser", todayToUser);
		
		//统计本月的数量
		Calendar monthStartTime = Calendar.getInstance();
		Calendar monthEndTime = Calendar.getInstance();
		monthStartTime.setTime(RemindDateUtils.getCurrentMonthStartTime());
		monthEndTime.setTime(RemindDateUtils.getCurrentMonthEndTime());
		
		int monthUserSend = boxMessageDao.countUserSendBdmsg(userid, monthStartTime, monthEndTime);
		int monthUserRev = boxMessageDao.countUserRevBdmsg(userid, monthStartTime, monthEndTime);
		int monthToFamily = boxMessageDao.countUserToFamilyMsg(userid, monthStartTime, monthEndTime);
		int monthToUser = boxMessageDao.countFamilyToUser(userid, monthStartTime, monthEndTime);
		
		map.put("monthUserSend", monthUserSend);
		map.put("monthUserRev", monthUserRev);
		map.put("monthToFamily", monthToFamily);
		map.put("monthToUser", monthToUser);
		
		//统计总数量
		int totalUserSend = boxMessageDao.countUserSendBdmsg(userid, null, null);
		int totalUserRev = boxMessageDao.countUserRevBdmsg(userid, null, null);
		int totalToFamily = boxMessageDao.countUserToFamilyMsg(userid, null, null);
		int totalToUser = boxMessageDao.countFamilyToUser(userid, null, null);
		
		map.put("totalUserSend", totalUserSend);
		map.put("totalUserRev", totalUserRev);
		map.put("totalToFamily", totalToFamily);
		map.put("totalToUser", totalToUser);
		
		
		return map;
	}

	@Override
	public ObjectResult getEntUserMessage(String phoneNum) {
		ObjectResult result = new ObjectResult();
		try {
			List<BoxMessageDto> dtos = new ArrayList<>();
			List<BoxMessage> list = boxMessageDao.getEntUserMessage(phoneNum);
			for (BoxMessage bm : list) {
				dtos.add(switchDto(bm));
			}
			result.setResult(dtos);
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("系统异常");
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ObjectResult getReceiveMessage(JSONObject json) {
		ObjectResult result = new ObjectResult();
		try {
			//定义存储盒子id的list
			List<Long> ids = new ArrayList<>();
			//获取json所有的key
			Set<String> set = json.keySet();
			//循环获取value并add进list中
			for (String string : set) {
				ids.add(Long.parseLong(json.get(string).toString()));
			}
			
			List<BoxMessageDto> dtos = new ArrayList<>();
			for (Long id : ids) {
				List<BoxMessage> list = boxMessageDao.getReceiveMessage(id);
				for (BoxMessage bm : list) {
					dtos.add(switchDto(bm));
				}
			}
			//进行排序
			dtos.sort(new BoxMessageDto());
			result.setResult(dtos);
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("系统异常");
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		return result;
	}

	@Override
	public ObjectResult getEntUserMessage(String phoneNum, String keyWord) {
		ObjectResult result = new ObjectResult();
		try {
			List<BoxMessageDto> dtos = new ArrayList<>();
			List<BoxMessage> list = boxMessageDao.getEntUserMessage(phoneNum, keyWord);
			for (BoxMessage bm : list) {
				dtos.add(switchDto(bm));
			}
			result.setResult(dtos);
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResultStatus.SYS_ERROR);
			result.setMessage("系统异常");
		}
		return result;
	}

	@Override
	public ObjectResult getBoxChatLog(String phoneNum, String boxSerialNumber) {
		ObjectResult result = new ObjectResult();
		try {
			Box box = boxDao.getBoxId(boxSerialNumber);
			List<BoxMessageDto> dtos = new ArrayList<>();
			List<BoxMessage> list = boxMessageDao.getBoxChatLog(phoneNum, box.getId());
			for (BoxMessage boxMessage : list) {
				dtos.add(switchDto(boxMessage));
			}
			result.setResult(dtos);
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResultStatus.SYS_ERROR);
			result.setMessage("系统异常");
		}
		return result;
	}
}

package com.bdbox.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.dao.AddressDao;
import com.bdbox.entity.Address;
import com.bdbox.service.AddressService;
import com.bdbox.util.LogUtils;

@Service
public class AddressServiceImpl implements AddressService {
	@Autowired
	private AddressDao addressDao;

	@Override
	public Address save(Long userId,String name,String mob,String email,String province,String city,String county,
			String detailedAddress,String postCode, String areaCode) {
		Address address = new Address();
		
		address.setUserId(userId);
		address.setName(name);
		address.setEmail(email);
		address.setMob(mob);
		address.setProvince(province);
		address.setCity(city);
		address.setCounty(county);
		address.setDetailedAddress(detailedAddress);
		address.setPostCode(postCode);
		address.setAreaCode(areaCode);
		return addressDao.save(address);
	}

	@Override
	public boolean deleteAddress(Long id) {
		try{
			addressDao.delete(id);
			LogUtils.loginfo("删除收货地址成功，ID为："+id);
			return true;
		}catch(Exception e){
			LogUtils.logerror("删除ID为："+id+" 的收货地址失败", e);
		}
		return false;
		
	}

	@Override
	public boolean update(Long id,Long userId,String name,String mob,String email,String province,String city,String county,
			String detailedAddress,String postCode, String areaCode) {
		try{
			Address address = addressDao.get(id);
			if(address == null)  return false;
			
			address.setUserId(userId);
			address.setName(name);
			address.setEmail(email);
			address.setMob(mob);
			address.setProvince(province);
			address.setCity(city);
			address.setCounty(county);
			address.setDetailedAddress(detailedAddress);
			address.setPostCode(postCode);
			address.setAreaCode(areaCode);
			addressDao.update(address);
			LogUtils.loginfo("修改ID为："+id+" 的收货地址失败");
			return true;
		}catch(Exception e){
			LogUtils.logerror("修改ID为："+id+" 的收货地址失败", e);
		}
		return false;
	}

	@Override
	public List<Address> getAddressByUserid(Long userId) {
		List<Address> list = addressDao.listByUserid(userId);
		if(list!=null && list.size()>0){
			return list;
		}
		return null;
	}

	@Override
	public Address get(Long id) {
		return addressDao.get(id);
	}

	@Override
	public Address getAddress(Long userId) {
		return addressDao.getAddress(userId);
	}



}

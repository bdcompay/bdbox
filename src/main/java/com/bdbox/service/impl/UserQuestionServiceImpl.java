package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service; 

import com.bdbox.api.dto.UserQuestionDto;
import com.bdbox.dao.UserQuestionDao;
import com.bdbox.entity.UserQuestion;
import com.bdbox.service.UserQuestionService;
import com.bdsdk.util.CommonMethod;

@Service
public class UserQuestionServiceImpl implements UserQuestionService {
	@Autowired
	private UserQuestionDao userQuestionDao;
	
	public UserQuestion save(UserQuestion enty){
		return userQuestionDao.save(enty);
	}
	
	public List<UserQuestionDto> getuserquestion(String userid){
		List<UserQuestion> list= userQuestionDao.getuserquestion(userid);
		List dtos=new ArrayList();
		if(!list.isEmpty()){
			for(UserQuestion o:list){
				UserQuestionDto dto=copy(o);
 				dtos.add(dto); 
 			}
		}
		return dtos;
	}
	
	
	public List<UserQuestionDto> getuserquestionlist(String username,int page,int pageSize){
		List<UserQuestion> list= userQuestionDao.getuserquestionlist(username,page,pageSize);
		List dtos=new ArrayList();
		if(!list.isEmpty()){
			for(UserQuestion o:list){
				UserQuestionDto dto=copy(o);
 				dtos.add(dto); 
			}
		}
		return dtos;
	}
 	
	public Integer getuserquestionlistcount(String username){
		return userQuestionDao.getuserquestionlistcount(username);
	}
	
	public UserQuestionDto copy(UserQuestion userQuestion){
		UserQuestionDto dto=new UserQuestionDto();
		dto.setId(userQuestion.getId());
		if(userQuestion.getIssolve().equals(true)){
			dto.setIssolve("是");
		}else{
			dto.setIssolve("否");
		}
		dto.setMyquestion(userQuestion.getMyquestion());
		dto.setSubmitTime(CommonMethod.CalendarToString(userQuestion.getSubmitTime(), "yyyy/MM/dd"));
		dto.setUserid(userQuestion.getUserid());
		return dto;
	}
	
	public UserQuestionDto getquestion(long id){
		UserQuestion enty=userQuestionDao.get(id);
		UserQuestionDto dto=new UserQuestionDto(); 
		if(enty!=null){
			dto=copy(enty);
		}
		return dto;
	}
	
	public UserQuestion getenty(long id){
		UserQuestion enty=userQuestionDao.get(id);
		if(enty!=null){
			return enty;
		}
		return null;

	}
	
	
	public void update(UserQuestion userQuestion){
		userQuestionDao.update(userQuestion);
	}
	
	public boolean deletemyquestion(long id){
		try{
		    userQuestionDao.delete(id);
		    return true;
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}






}

package com.bdbox.service.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.ReturnsDto;
import com.bdbox.constant.DealStatus;
import com.bdbox.constant.ExpressStatus;
import com.bdbox.constant.PayType;
import com.bdbox.constant.RefundStatus;
import com.bdbox.constant.ReturnsStatusType;
import com.bdbox.dao.BDCoodDao;
import com.bdbox.dao.ExpressDao;
import com.bdbox.dao.OrderDao;
import com.bdbox.dao.ProductDao;
import com.bdbox.dao.RefundDao;
import com.bdbox.dao.ReturnsDao;
import com.bdbox.dao.UserDao;
import com.bdbox.entity.BDCood;
import com.bdbox.entity.Express;
import com.bdbox.entity.Order;
import com.bdbox.entity.Product;
import com.bdbox.entity.Refund;
import com.bdbox.entity.Returns;
import com.bdbox.entity.User;
import com.bdbox.service.OrderService;
import com.bdbox.service.ReturnsService;
import com.bdbox.util.LogUtils;
import com.bdbox.util.NumberBuilder;
import com.bdbox.wx.api.WxUnifiedOrder;
import com.bdbox.wx.util.CommonUtil;
import com.bdsdk.json.ListParam;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Service
public class ReturnsServiceImpl implements ReturnsService {

	@Autowired
	private ReturnsDao returnsDao;
	@Autowired
	private ExpressDao expressDao;
	@Autowired
	private OrderDao orderDao;
	@Autowired
	private WxUnifiedOrder wxUnifiedOrder;
	@Autowired
	private RefundDao refundDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private ProductDao productDao;
	@Autowired
	private OrderService orderService;
	@Autowired
	private BDCoodDao bdcoodDao;

	@Override
	public Returns saveReturns(Long orderId, Long productId, String orderNum, String productName, long userId,
			Double price1, Calendar buyTime, int returnsCount, String returnsCause, String buy, PayType payType) {
		// 退货号
		String returnsNum = NumberBuilder.getReturnsNum();
		Order order = orderDao.get(orderId);
		// 单价
		double price = order.getPrice();

		Returns returns = returnsDao.queryReturn(orderId);
		if (returns == null) {
			returns = new Returns();
		}

		returns.setPrice(price);
		returns.setOrderNum(orderNum);
		returns.setReturnsNum(returnsNum);
		returns.setApplySubmitTime(Calendar.getInstance());
		returns.setReturnsStatusType(ReturnsStatusType.APPLYPASS);
		BigDecimal bd = new BigDecimal(Double.parseDouble(String.valueOf(price * returnsCount)));
		returns.setBuyMoney(bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
		returns.setCount(returnsCount);
		returns.setOrdreId(orderId);
		returns.setProductId(productId);
		returns.setProductName(productName);
		returns.setBuyTime(buyTime);
		returns.setReturnsCause(returnsCause);
		returns.setUserId(userId);
		returns.setReturnType(buy);
		returns.setPayType(payType);
		return returnsDao.save(returns);
	}

	@Override
	public boolean deleteRetuens(long id) {
		try {
			returnsDao.delete(id);
			return true;
		} catch (Exception e) {
			LogUtils.logerror("删除退货订单失败", e);
		}
		return false;
	}

	@Override
	public Returns updateRetuens(Returns returns) {
		try {
			returnsDao.update(returns);
			return returnsDao.get(returns.getId());
		} catch (Exception e) {
			LogUtils.logerror("修改退货订单失败", e);
		}
		return null;
	}

	@Override
	public Returns getReturnsById(long id) {
		return returnsDao.get(id);
	}

	@Override
	public List<Returns> listReturns(Long orderId, Long returnsNum, Long userId, String expressNum,
			ReturnsStatusType returnsStatusType, Calendar startTime, Calendar endTime, int page, int pageSize,
			String orderstutas) {
		List<Returns> list = new ArrayList<Returns>();
		try {
			list = returnsDao.listReturns(orderId, returnsNum, null, userId, expressNum, returnsStatusType, startTime,
					endTime, page, pageSize, orderstutas);
		} catch (Exception e) {
			LogUtils.logerror("查询退货订单失败", e);
		}
		return list;
	}

	@Override
	public int countReturns(Long orderId, Long returnsNum, String orderNum, Long userId, String expressNum,
			ReturnsStatusType returnsStatusType, Calendar startTime, Calendar endTime, String orderstutas) {
		int count = 0;
		try {
			count = returnsDao.countReturns(orderId, returnsNum, orderNum, userId, expressNum, returnsStatusType,
					startTime, endTime, orderstutas);
		} catch (Exception e) {
			LogUtils.logerror("根据条件统计退货数量失败", e);
		}
		return count;
	}

	@Override
	public ListParam queryReturns(Long orderId, Long returnsNum, String orderNum, Long userId, String expressNum,
			ReturnsStatusType returnsStatusType, Calendar startTime, Calendar endTime, int page, int pageSize,
			String orderstutas) {
		List<ReturnsDto> listDto = new ArrayList<ReturnsDto>();
		int count = 0;
		try {
			List<Returns> list = returnsDao.listReturns(orderId, returnsNum, orderNum, userId, expressNum,
					returnsStatusType, startTime, endTime, page, pageSize, orderstutas);
			for (Returns r : list) {
				ReturnsDto dto = castFromReturns(r);
				// System.out.println(dto.getApplySubmitTime());
				listDto.add(dto);
			}
			count = returnsDao.countReturns(orderId, returnsNum, orderNum, userId, expressNum, returnsStatusType,
					startTime, endTime, orderstutas);
		} catch (Exception e) {
			LogUtils.logerror("查询退货订单失败(符合jquery.datatable格式)", e);
		}

		ListParam listParam = new ListParam();
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setAaData(listDto);
		return listParam;
	}

	@Override
	public boolean doAuditApply(long id, String auditResult, String cause) {
		try {
			Returns returns = returnsDao.get(id);
			returns.setApplyAuditTime(Calendar.getInstance());
			if ("OK".equals(auditResult)) {
				returns.setReturnsStatusType(ReturnsStatusType.APPLYPASS);
			} else {
				returns.setReturnsStatusType(ReturnsStatusType.APPLYNOTPASS);
				returns.setExplain(cause);
			}
			return true;
		} catch (Exception e) {
			LogUtils.logerror("管理员审核退货请求失败", e);
		}
		return false;
	}

	@Override
	public boolean doSubmitExpressNum(long id, String expressName, String expressNum) {
		try {
			Express express = new Express();
			express.setExpressStatus(ExpressStatus.SENT);
			express.setExpressName(expressName);
			express.setExpressNumber(expressNum);
			express.setSendTime(Calendar.getInstance());
			expressDao.save(express);

			Returns returns = returnsDao.get(id);
			returns.setExpressId(express.getId());
			returns.setExpressName(expressName);
			returns.setExpressNum(expressNum);
			returns.setExpressNumWriteTime(Calendar.getInstance());
			returns.setReturnsStatusType(ReturnsStatusType.EXPRESS);
			returnsDao.update(returns);

			return true;
		} catch (Exception e) {
			LogUtils.logerror("用户提交物流单号失败", e);
		}
		return false;
	}

	@Override
	public boolean doReceiveProduct(long id) {
		try {
			Returns returns = returnsDao.get(id);
			returns.setReturnsStatusType(ReturnsStatusType.AUDIT);
			returnsDao.update(returns);
			return true;
		} catch (Exception e) {
			LogUtils.logerror("管理员接收产品同步数据失败", e);
		}
		return false;
	}

	@Override
	public boolean doAuditProduct(long id, String auditResult, String explain) {
		try {
			Returns returns = returnsDao.get(id);
			if ("OK".equals(auditResult)) {
				returns.setReturnsStatusType(ReturnsStatusType.AUDITPASS);
			} else {
				returns.setReturnsStatusType(ReturnsStatusType.AUDITNOTPASS);
				returns.setExplain(explain);
			}
			returnsDao.update(returns);
			return true;
		} catch (Exception e) {
			LogUtils.logerror("管理员审核产品同步数据失败", e);
		}
		return false;
	}

	@Override
	public boolean doRefund(long id, double buyMoney, double deductionMoney, String deductionCause,
			double returnmoney) {
		try {
			Returns returns = returnsDao.get(id);
			Order order = orderDao.get(returns.getOrdreId());

			// 调用微信退款接口
			double total_fee = order.getCount() * order.getPrice();
			double refund_fee = buyMoney;

			String out_trade_no = order.getTrade_no();
			// 生成退款单号
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
			String refundNo1 = df.format(date);
			Random r = new Random(8999);
			int refundNo2 = r.nextInt() + 1000;
			String refundNo = refundNo1 + refundNo2;
			int totalfee = (int) (total_fee * 100);
			int refundfee = (int) (returnmoney * 100);
			String total_feeStr = String.valueOf(totalfee);
			String refund_feeStr = String.valueOf(refundfee);
			String XMLData = wxUnifiedOrder.refund(null, out_trade_no, refundNo, total_feeStr, refund_feeStr);
			Map map = CommonUtil.xmlToArray(XMLData);

			// 创建退款订单
			if ("SUCCESS".equals(map.get("result_code"))) {
				// 保存退款信息
				Refund refund = new Refund();
				refund.setApplyTime(order.getBuyTime());
				// refund.setOrderId(order.getId());
				// refund.setOrderNo(order.getOrderNo());
				refund.setPrice(refund_fee);
				refund.setRefundNo(refundNo);
				refund.setRefundStatus(RefundStatus.APPLY);
				refundDao.save(refund);

				// 修改退货状态
				// 修改退货订单
				returns.setDeductionMoney(deductionMoney);
				returns.setDeductionCause(deductionCause);
				returns.setReturnMoney(returnmoney);
				returns.setReturnsStatusType(ReturnsStatusType.REFUND);
				returns.setRefundId(refund.getId());
				returns.setFinishTime(Calendar.getInstance());
				returnsDao.update(returns);
				LogUtils.loginfo("微信申请退货退款成功,退货单号：" + returns.getReturnsNum() + ",退款单号:" + refundNo + "  ");
				return true;
			} else {
				LogUtils.logwarn("退货退款调用微信申请退款失败，失败原因：" + map.get("err_code_des"), null);
			}
		} catch (Exception e) {
			LogUtils.logerror("管理员审核产品同步数据失败", e);
		}
		return false;
	}

	@Override
	public boolean doReturnsSuccess(long id) {
		Returns returns = returnsDao.get(id);
		try {
			Refund refund = refundDao.get(returns.getRefundId());
			refund.setRefundStatus(RefundStatus.ACCOMPLISH);
			refundDao.update(refund);

			returns.setReturnsStatusType(ReturnsStatusType.FINISH);
			returns.setFinishTime(Calendar.getInstance());
			returnsDao.update(returns);
			LogUtils.loginfo("退货单号：" + returns.getReturnsNum() + " 退货成功。退款单号: " + refund.getRefundNo());
			return true;
		} catch (Exception e) {
			LogUtils.logerror("退货单号：" + returns.getReturnsNum() + " 退货成功。", e);
			return false;
		}
	}

	public ReturnsDto castFromReturns(Returns returns) {
		ReturnsDto returnsDto = new ReturnsDto();
		SimpleDateFormat m_simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
		long surplus = 0; // 剩余天数
		long exceed = 0; // 超期天数
		long count = returns.getCount(); // 退款数量
		Order order = null;
		long day = 0;// 实际租用天数
		// 将实体属性拷贝到Dto中，属性名必须相同
		try {
			BeansUtil.copyBean(returnsDto, returns, true);
			// 将Calendar类型转换为String类型斌赋值到Dto属性中
			if (returns.getReturnsCause() == null || returns.getReturnsCause().equals("")) {
				returnsDto.setReturnsCause("无");
			}

			if (returns.getDeductionCause() == null) {
				returnsDto.setDeductionCause("无");
			}
			if (returns.getApplyAuditTime() != null) {
				returnsDto.setApplyAuditTimeStr(
						CommonMethod.CalendarToString(returns.getApplyAuditTime(), "yyyy/MM/dd HH:mm"));
			}
			if (returns.getApplySubmitTime() != null) {
				returnsDto.setApplySubmitTimeStr(
						CommonMethod.CalendarToString(returns.getApplySubmitTime(), "yyyy/MM/dd HH:mm"));
			}
			if (returns.getBuyTime() != null) {
				returnsDto.setBuyTimeStr(CommonMethod.CalendarToString(returns.getBuyTime(), "yyyy/MM/dd HH:mm"));
			}
			if (returns.getExpressNumWriteTime() != null) {
				returnsDto.setExpressNumWriteTimeStr(
						CommonMethod.CalendarToString(returns.getExpressNumWriteTime(), "yyyy/MM/dd HH:mm"));
			}
			if (returns.getFinishTime() != null) {
				returnsDto.setFinishTimeStr(CommonMethod.CalendarToString(returns.getFinishTime(), "yyyy/MM/dd HH:mm"));
			}


			long orderid = returns.getOrdreId();
			order = orderDao.get(orderid);
			if (order.getName() != null) {
				returnsDto.setUsername(order.getName());
			} else {
				returnsDto.setUsername(null);
			}
			
			// 租用订单
			if (returns.getReturnType().equals("RENT")) {
				if (order.getBeginTime() != null) { // 判断订单发货后的签收时间是否为空
					returnsDto
							.setRentStartTime(CommonMethod.CalendarToString(order.getBeginTime(), "yyyy/MM/dd HH:mm"));
					Date date = new Date(); // 当前时间
					m_simpledateformat.format(date);
					Date nowtime = m_simpledateformat.parse(m_simpledateformat.format(date));
					Calendar complete = order.getBeginTime(); // 租用开始时间
					Date start = m_simpledateformat.parse(m_simpledateformat.format(complete.getTime()));
					String countday = order.getNumberdate(); // 租用天数
					if (returns.getEndTime() == null) {
						day = ((nowtime.getTime() - start.getTime()) / (1000 * 3600 * 24)) + 1;
						surplus = Long.valueOf(countday) - day;
						if (surplus > 0) {
							returnsDto.setSurplusdate("剩余" + String.valueOf(surplus) + "天");
						} else if (surplus == 0) {
							returnsDto.setSurplusdate("剩余" + String.valueOf(surplus) + "天");
						} else if (surplus < 0) {
							exceed = day - Long.valueOf(countday);
							returnsDto.setSurplusdate("超期" + exceed + "天");
						}
					} else {
						Calendar complete2 = returns.getEndTime();
						Date end = m_simpledateformat.parse(m_simpledateformat.format(complete2.getTime()));
						day = ((end.getTime() - start.getTime()) / (1000 * 3600 * 24)) + 1;
						surplus = Long.valueOf(countday) - day; // 租用天数-（租用结束时间-租用开始时间）的值大于0，则未超期
						if (surplus < 0) {
							exceed = day - Long.valueOf(countday);
							returnsDto.setSurplusdate("超期" + exceed + "天");
						} else if (surplus == 0) {
							returnsDto.setSurplusdate("剩余" + surplus + "天");
						} else if (surplus > 0) {
							returnsDto.setSurplusdate("剩余" + surplus + "天");
						}
						returnsDto.setRentEndTime(
								CommonMethod.CalendarToString(returns.getEndTime(), "yyyy/MM/dd HH:mm"));
					}

				}
				// 计算租用盒子的费用，实名制用户30/天，非实名制50/天
				if (order.getNumberdate() != null && !"".equals(order.getNumberdate())) {
					long productid = order.getProductId();
					Product product = productDao.get(productid);
					long num = Long.valueOf(order.getNumberdate()); // 租用天数
					double sum = 0; // 扣取租金总和
					String user = order.getUser();
					User username = userDao.getUserByusername(user);
					String renlnameorder = order.getRenlnameorder();
					double unitcost = 0;
					// 抵用天数
					int length = 0;
					// 优惠折扣
					Double discount = 0.0;
					if (order.getBdcood() != null) {
						String bdcood = order.getBdcood();
						String[] str = bdcood.split(",");
						for (String string : str) {
							BDCood bdCood = bdcoodDao.getbdcoods_wl(string);
							length += bdCood.getDayNum();
							try {
								if(bdCood.getDiscount()!=null)
									discount = bdCood.getDiscount() / 10;
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
					
					Double dis = discount > 0 ? discount : 1.0;
					
					String disStr = discount > 0 ? "x" + String.valueOf(discount*10) + "折" : "";

					if (order.getUnitcost() == null) {
						unitcost = 0;
					} else {
						unitcost = Double.valueOf(order.getUnitcost());
					}
					if (renlnameorder != null && !renlnameorder.equals("") && renlnameorder.equals("1")) {
						if (surplus >= 0) {
							if (length < day) {
								sum = count * (day - length) * unitcost * dis;
								returnsDto.setMethod(count + "台" + "x" + (day - length) + "天" + "x" + unitcost + "元" + disStr);
							} else {
								sum = count * 0 * unitcost * dis;
								returnsDto.setMethod(count + "台" + "x" + 0 + "天" + "x" + unitcost + "元" + disStr);
							}
							returnsDto.setUnitprice(String.valueOf(unitcost));
							BigDecimal bd = new BigDecimal(Double.parseDouble(String.valueOf(sum)));
							returnsDto
									.setDeduct(String.valueOf(bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
						} else if (surplus != 0 && surplus < 0) {
							long sumsum = exceed + num; // 租用天数+超期天数累加
							sum = count * (sumsum - length) * unitcost * dis;
							returnsDto.setMethod(count + "台" + "x" + "(" + (num - length) + "天" + "+" + exceed + "天"
									+ ")" + "x" + unitcost + "元" + disStr);
							returnsDto.setUnitprice(String.valueOf(unitcost));
							BigDecimal bd = new BigDecimal(Double.parseDouble(String.valueOf(sum)));
							returnsDto
									.setDeduct(String.valueOf(bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
						}
					} else {
						if (surplus >= 0) {
							if (length < day) {
								sum = count * (day - length) * unitcost * dis;
								returnsDto.setMethod(count + "台" + "x" + (day - length) + "天" + "x" + unitcost + "元" + disStr);
							} else {
								sum = count * 0 * unitcost * dis;
								returnsDto.setMethod(count + "台" + "x" + 0 + "天" + "x" + unitcost + "元" + disStr);
							}
							returnsDto.setUnitprice(String.valueOf(unitcost));
							BigDecimal bd = new BigDecimal(Double.parseDouble(String.valueOf(sum)));
							returnsDto
									.setDeduct(String.valueOf(bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
						} else if (surplus != 0 && surplus < 0) {
							long sumsum = exceed + num; // 租用天数+超期天数累加
							sum = count * (sumsum - length) * unitcost * dis;
							returnsDto.setMethod(count + "台" + "x" + "(" + (num - length) + "天" + "+" + exceed + "天"
									+ ")" + "x" + unitcost + "元" + disStr);
							returnsDto.setUnitprice(String.valueOf(unitcost));
							BigDecimal bd = new BigDecimal(Double.parseDouble(String.valueOf(sum)));
							returnsDto
									.setDeduct(String.valueOf(bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue()));
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return returnsDto;
	}

	@Override
	public ReturnsDto getReturnsDtoById(long id) {
		Returns returns = returnsDao.get(id);
		ReturnsDto dto = castFromReturns(returns);
		return dto;
	}

	public Returns queryReturn(long orderid) {
		Returns returns = returnsDao.queryReturn(orderid);
		return returns;
	}

	// 租用归还管理页面查询
	@Override
	public ListParam doqueryRentReturns(Long orderId, Long returnsNum, String orderNum, Long userId, String expressNum,
			ReturnsStatusType returnsStatusType, Calendar startTime, Calendar endTime, int page, int pageSize,
			String orderstutas) {
		List<ReturnsDto> listDto = new ArrayList<ReturnsDto>();
		int count = 0;
		try {
			List<Returns> list = returnsDao.queryRentReturns(orderId, returnsNum, orderNum, userId, expressNum,
					returnsStatusType, startTime, endTime, page, pageSize, orderstutas);
			for (Returns r : list) {
				ReturnsDto dto = castFromReturns(r);
				// System.out.println(dto.getApplySubmitTime());
				double prt = Double.valueOf(dto.getDeduct());
				r.setRentprice(prt);
				returnsDao.update(r);
				listDto.add(dto);
			}
			count = returnsDao.countRentReturns(orderId, returnsNum, orderNum, userId, expressNum, returnsStatusType,
					startTime, endTime, orderstutas);
		} catch (Exception e) {
			LogUtils.logerror("查询退货订单失败(符合jquery.datatable格式)", e);
			e.printStackTrace();
		}

		ListParam listParam = new ListParam();
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setAaData(listDto);
		return listParam;
	}

	public List<Returns> listRentReturns(Long orderId, Long returnsNum, Long userId, String expressNum,
			ReturnsStatusType returnsStatusType, Calendar startTime, Calendar endTime, int page, int pageSize,
			String orderstutas) {
		List<Returns> list = new ArrayList<Returns>();
		try {
			list = returnsDao.listRentReturns(orderId, returnsNum, null, userId, expressNum, returnsStatusType,
					startTime, endTime, page, pageSize, orderstutas);
		} catch (Exception e) {
			LogUtils.logerror("查询退货订单失败", e);
		}
		return list;
	}

	public int countRentReturns(Long orderId, Long returnsNum, String orderNum, Long userId, String expressNum,
			ReturnsStatusType returnsStatusType, Calendar startTime, Calendar endTime, String orderstutas) {
		int count = 0;
		try {
			count = returnsDao.countRentReturns(orderId, returnsNum, orderNum, userId, expressNum, returnsStatusType,
					startTime, endTime, orderstutas);
		} catch (Exception e) {
			LogUtils.logerror("根据条件统计退货数量失败", e);
		}
		return count;
	}

	public Returns getReturnsenty(long expressid) {
		return returnsDao.getReturnsenty(expressid);
	}

	@Override
	public void doConfirmSH(long expressId, String endTime) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			Returns returns = returnsDao.queryReturns(expressId);
			Order order = null;
			if (returns != null && returns.getReturnType().equals("RENT")) {
				order = orderService.getOrder(returns.getOrdreId());
				Date dateExecute = sdf.parse(endTime);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(dateExecute);
				calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 1);
				order.setEndTime(calendar);
				returns.setEndTime(calendar);
				returnsDao.update(returns);
			} else {
				order = orderService.getOrder(returns.getOrdreId());
			}
			order.setDealStatus(DealStatus.DEALCLOSE);
			orderService.updateOrder(order);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}

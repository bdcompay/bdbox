package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.ProductDto;
import com.bdbox.constant.ProductBuyType;
import com.bdbox.constant.ProductType;
import com.bdbox.dao.ProductDao;
import com.bdbox.entity.Product;
import com.bdbox.service.ProductService;
import com.bdbox.util.LogUtils;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductDao productDao;

	@Override
	public Product get(Long id) {
		return productDao.get(id);
	}

	@Override
	public Product save(String name, ProductType productType, Double price,
			int saleNumber, int stockNumber, ProductBuyType productBuyType,
			boolean isOpenBuy, Calendar buyStartTime,Calendar buyEndTime, String productUrl,String color,
			String explain, Double rental, Double antecedent, Double couponPrice) {
		Product product = new Product();
		
		product.setName(name);
		product.setPrice(price);
		product.setColor(color);
		product.setStockNumber(stockNumber);
		product.setSaleNumber(saleNumber);
		product.setProductType(productType);
		product.setProductBuyType(productBuyType);
		product.setIsOpenBuy(isOpenBuy);
		product.setBuyStartTime(buyStartTime);
		product.setBuyEndTime(buyEndTime);
		product.setProductUrl(productUrl);
		product.setCreatedTime(Calendar.getInstance());
		product.setExplain(explain);
		product.setRentPrice(rental);
		product.setRentAutonymPrice(antecedent);
		
		
		return productDao.save(product);
	}

	@Override
	public boolean update(Long id, String name, ProductType productType, Double price,
			int saleNumber, int stockNumber, ProductBuyType productBuyType,
			boolean isOpenBuy, Calendar buyStartTime,Calendar buyEndTime, String productUrl,String color,
			String explain, Double updateRental, Double updateAntecedent, Double updateCouponPrice){
		try {
			Product product = productDao.get(id);
			product.setName(name);
			product.setPrice(price);
			product.setColor(color);
			/*product.setStockNumber(stockNumber);
			product.setSaleNumber(saleNumber);*/
			product.setProductType(productType);
			product.setProductBuyType(productBuyType);
			product.setIsOpenBuy(isOpenBuy);
			product.setBuyStartTime(buyStartTime);
			product.setBuyEndTime(buyEndTime);
			product.setProductUrl(productUrl);
			product.setCreatedTime(Calendar.getInstance());
			product.setExplain(explain);
			product.setRentPrice(updateRental);
			product.setRentAutonymPrice(updateAntecedent);
			product.setCouponPrice(updateCouponPrice);
			
			productDao.update(product);
			LogUtils.loginfo("修改ID为：" + id + " 的产品成功");
			return true;
		} catch (Exception e) {
			LogUtils.logerror("修改ID为：" + id + " 的产品失败", e);
		}
		return false;
	}

	@Override
	public boolean deleteProduct(Long id) {
		try {
			productDao.delete(id);
			LogUtils.loginfo("删除ID为：" + id + " 的产品成功");
			return true;
		} catch (Exception e) {
			LogUtils.logerror("删除ID为：" + id + " 的产品失败", e);
		}
		return false;
	}

	@Override
	public List<Product> listBy(String name, ProductType productType,
			ProductBuyType productBuyType, Calendar startTime,
			Calendar endTime, int page, int pageSize) {
		List<Product> products = new ArrayList<Product>();
		try {
			Product product = new Product();
			product.setName(name);
			product.setProductType(productType);
			product.setProductBuyType(productBuyType);
			products = productDao.query(product, startTime, endTime, page,
					pageSize);
		} catch (Exception e) {
			LogUtils.logerror("查询产品异常", e);
		}
		return products;
	}

	@Override
	public int count(String name, ProductType productType,
			ProductBuyType productBuyType, Calendar startTime, Calendar endTime) {
		int count = 0;
		try {
			Product product = new Product();
			product.setName(name);
			product.setProductType(productType);
			product.setProductBuyType(productBuyType);
			count = productDao.queryAmount(product, startTime, endTime);
		} catch (Exception e) {
			LogUtils.logerror("统计查询数量异常", e);
		}
		return count;
	}

	@Override
	public ProductDto castFrom(Product product) {
		ProductDto dto = new ProductDto();
		BeansUtil.copyBean(dto, product, true);
		if(product.getCreatedTime()!=null){
			dto.setCreatedTimeStr(CommonMethod.CalendarToString(product.getCreatedTime(), "yyyy-MM-dd HH:mm:ss"));
		}
		if(product.getBuyStartTime()!=null){
			dto.setBuyStartTimeStr(CommonMethod.CalendarToString(product.getBuyStartTime(), "yyyy-MM-dd HH:mm:ss"));
		}
		if(product.getBuyEndTime()!=null){
			dto.setBuyEndTimeStr(CommonMethod.CalendarToString(product.getBuyEndTime(), "yyyy-MM-dd HH:mm:ss"));
		}
		if(product.getProductBuyType()!=null){
			if(product.getProductBuyType().toString().equals("SHOPPINGRUSH")){
				dto.setProductBuyTypes("抢购"); 
			}
			if(product.getProductBuyType().toString().equals("GENERAL")){
				dto.setProductBuyTypes("普通"); 
			}
			if(product.getProductBuyType().toString().equals("RENT")){
				dto.setProductBuyTypes("租用"); 
			}
		}
		return dto;
	}

	@Override
	public boolean updateProduct(Product product) {
		try{
			productDao.update(product);
			return true;
		}catch(Exception e){
			LogUtils.logerror("更新产品时异常", e);
		}
		return false;
	}

	@Override
	public int getStockNumber() {
		Product product = productDao.getStockNumber();
		return product.getStockNumber();
	}

}

package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.BoxDto;
import com.bdbox.constant.BoxStatusType;
import com.bdbox.constant.CardType;
import com.bdbox.dao.BoxDao;
import com.bdbox.dao.BoxMessageDao;
import com.bdbox.dao.SOSLocationDao;
import com.bdbox.dao.UserDao;
import com.bdbox.entity.Box;
import com.bdbox.entity.BoxMessage;
import com.bdbox.entity.SOSLocation;
import com.bdbox.entity.User;
import com.bdbox.service.BoxService;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Service
public class BoxServiceImpl implements BoxService {
	@Autowired
	private BoxDao boxDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private SOSLocationDao sosLocationDao;

	public Box saveBox(Box box) {
		// TODO Auto-generated method stub
		return boxDao.save(box);
	}

	public Boolean updateBox(Box box) {
		// TODO Auto-generated method stub
		boxDao.update(box);
		return true;
	}

	public Box getBox(BoxStatusType boxStatusType,String boxSerialNumber, String cardNumber,
			String snNumber, Long userId) {
		// TODO Auto-generated method stub
		List<Box> boxs = boxDao.query(userId, null,cardNumber, boxSerialNumber,
				boxStatusType, snNumber,null,null, null, 1, 1);
		if (boxs.size() > 0) {
			return boxs.get(0);
		}
		return null;
	}

	public BoxDto getBoxDto(String boxSerialNumber, String cardNumber,String snNumber, Long userId) {
		List<Box> boxs = boxDao.query(userId, null,cardNumber, boxSerialNumber,
				null, snNumber, null,null,null, 1, 1);
		if (boxs.size() > 0) {
			return castBoxToBoxDto(boxs.get(0));
		}
		return null;
	}

	/**
	 * 获取用户的北斗盒子列表
	 * 
	 * @param userId
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<BoxDto> getUserBoxDtos(Long userId, int page, int pageSize) {
		List<Box> boxs = boxDao.query(userId,null, null, null, BoxStatusType.NORMAL, null, null,null,null,
				page, pageSize);
		List<BoxDto> boxDtos = new ArrayList<BoxDto>();
		for (Box box : boxs) {
			BoxDto boxDto = castBoxToBoxDto(box);
			boxDtos.add(boxDto);
		}
		return boxDtos;
	}

	private BoxDto castBoxToBoxDto(Box box) {
		BoxDto boxDto = new BoxDto();
		BeansUtil.copyBean(boxDto, box, true);
		boxDto.setId(box.getId());
		boxDto.setBoxStatusTypeStr(box.getBoxStatusType().str());
		boxDto.setCardTypeStr(box.getCardType().str());
		boxDto.setLastLocationTimeStr(CommonMethod.CalendarToString(
				box.getLastLocationTime(), null));
		if (box.getUser()!=null) {
			boxDto.setUser(box.getUser().getUsername());
		}
		boxDto.setBoxType(box.getBoxType()==null?"":box.getBoxType().getName());
		return boxDto;
	}

	public Boolean doBindBox(String boxSerialNumber, String snNumber,String name, Long userId,String idNumber,String realName) {
		List<Box> boxs = boxDao.query(null,null, null, boxSerialNumber, null,
				snNumber, null,null,null, 1, 1);
		User user = userDao.get(userId);
		if (user == null) {
			return false;
		}
		if (boxs.size() > 0) {
			Box box = boxs.get(0);
			box.setUser(user);
			box.setRealName(realName);
			box.setName(name);
			box.setIdNumber(idNumber);
			box.setBoxStatusType(BoxStatusType.NORMAL);
			boxDao.update(box);
			return true;
		} else {
			return false;
		}
	}

	public Boolean doUnBindBox(String boxSerialNumber, Long userId) {
		List<Box> boxs = boxDao.query(userId, null,null, boxSerialNumber, null,
				null, null,null,null, 1, 1);
		if (boxs.size() > 0) {
			Box box = boxs.get(0);
			box.setUser(null);
			box.setBoxStatusType(BoxStatusType.NOTACTIVATED);
			boxDao.update(box);
			return true;
		} else {
			return false;
		}
	}

	public List<BoxDto> query(Long userId,CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType,
			String snNumber,String boxName, String userName, String order, int page, int pageSize) {
		// TODO Auto-generated method stub
		List<Box> list = boxDao.query(userId,cardType, cardNumber, boxSerialNumber, boxStatusType, snNumber,boxName,userName, order, page, pageSize);
		List<BoxDto> boxDtos = new ArrayList<BoxDto>();
		for (Box box : list) {
			boxDtos.add(castBoxToBoxDto(box));
		}
		return boxDtos;
	}

	public int queryAmount(Long userId,CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType, String snNumber,String boxName, String userName) {
		// TODO Auto-generated method stub
		return boxDao.queryAmount(userId,cardType, cardNumber, boxSerialNumber, boxStatusType, snNumber,boxName, userName);
	}

	public BoxDto getBoxDto(Long id) {
		// TODO Auto-generated method stub
		return castBoxToBoxDto(boxDao.get(id));
	}
	
	public Box getBox(Long id) {
		// TODO Auto-generated method stub
		return boxDao.get(id);
	}

	@Autowired
	private BoxMessageDao boxMessageDao;

	public void deleteBox(long id) {
		// TODO Auto-generated method stub
		
		Box box = boxDao.get(id);
		long boxSerialNumber = Long.parseLong(box.getBoxSerialNumber());
		// 消息
		if(boxSerialNumber!=0){
			Integer boxMessageDaoCount1 = boxMessageDao.queryAmount(null, null,
					null, null, null, boxSerialNumber, null, null, null, null, null, 0);
			Integer boxMessageDaoCount2 = boxMessageDao.queryAmount(null, null,
					null, null, null, null, boxSerialNumber, null, null, null, null, 0);
			if (boxMessageDaoCount1 > 0) {
				List<BoxMessage> list = boxMessageDao.query(null, null, null, null,
						null, boxSerialNumber, null, null, null, null, null, 0, null, 1,
						boxMessageDaoCount1);
				for (BoxMessage boxMessage : list) {
					boxMessageDao.delete(boxMessage.getId());
				}
			}
			
			if (boxMessageDaoCount2 > 0) {
				List<BoxMessage> list = boxMessageDao.query(null, null, null, null,
						null, null, boxSerialNumber, null, null, null, null, 0, null, 1,
						boxMessageDaoCount1);
				for (BoxMessage boxMessage : list) {
					boxMessageDao.delete(boxMessage.getId());
				}
			}
			
			//删除SOS_Location
			List<SOSLocation> sosLocations=sosLocationDao.getLocationsForBoxId(id);
			if(sosLocations!=null){
				for(SOSLocation sos:sosLocations){
					sosLocationDao.deleteObject(sos);
				}
			}
		}
		try{
		//删除b_sos_location
		sosLocationDao.deleByBoxId(id);
		}catch (Exception e){
			e.printStackTrace();
		}
		
		boxDao.delete(id);
	}

	@Override
	public int countEntBoxNum(long entUserId) {
		return boxDao.countByEntUserId(entUserId);
	}

	@Override
	public List<BoxDto> queryEntBoxs(long entUserId, int page, int pageSize) {
		List<BoxDto> dtos = new ArrayList<BoxDto>();
		List<Box> boxs = boxDao.queryByEntUserId(entUserId, page, pageSize);
		for(Box box:boxs){
			dtos.add(castBoxToBoxDto(box));
		}
		return dtos;
	}

	@Override
	public List<BoxDto> query(Long userId, CardType cardType,
			String cardNumber, String boxSerialNumber,
			BoxStatusType boxStatusType, String snNumber, String boxName,
			String userName, Long entUserId, String order, int page,
			int pageSize) {
		// TODO Auto-generated method stub
				List<Box> list = boxDao.query(userId,cardType, cardNumber, boxSerialNumber, boxStatusType, snNumber,boxName,userName,entUserId, order, page, pageSize);
				List<BoxDto> boxDtos = new ArrayList<BoxDto>();
				for (Box box : list) {
					boxDtos.add(castBoxToBoxDto(box));
				}
				return boxDtos;
	}

	@Override
	public int queryAmount(Long userId, CardType cardType, String cardNumber,
			String boxSerialNumber, BoxStatusType boxStatusType,
			String snNumber, String boxName, String userName, Long entUserId) {
		return boxDao.queryAmount(userId,cardType, cardNumber, boxSerialNumber, boxStatusType, snNumber,boxName, userName,entUserId);
	}

	@Override
	public Box getBoxForCardNum(String cardNum) {
		return boxDao.getBox(cardNum);
	}

	@Override
	public List<Box> getBoxInfo(String boxSerialNumber) {
		return boxDao.getBoxInfo(boxSerialNumber);
	}

	@Override
	public Box getBoxOne(String boxSerialNumber) {
		return boxDao.getBoxId(boxSerialNumber);
	}

	@Override
	public ObjectResult getEntUserBoxs(Long entUserId) {
		ObjectResult result = new ObjectResult();
		try {
			List<Box> boxs = boxDao.getEntUserBoxs(entUserId);
			List<BoxDto> boxDtos = new ArrayList<BoxDto>();
			for (Box box : boxs) {
				boxDtos.add(castBoxToBoxDto(box));
			}
			result.setStatus(ResultStatus.OK);
			result.setResult(boxDtos);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		return result;
	}

	@Override
	public Box getEntUserBox(Long entUserId, String cardId) {
		return boxDao.getEntUserBox(entUserId, cardId);
	}

	@Override
	public ObjectResult searchBox(Long entUserId, String cardId) {
		ObjectResult result = new ObjectResult();
		try {
			List<BoxDto> dtos = new ArrayList<>();
			List<Box> list = boxDao.searchBox(entUserId, cardId);
			if(list!=null){
				for (Box box : list) {
					dtos.add(castBoxToBoxDto(box));
				}
			}
			result.setResult(dtos);
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setMessage("系统异常");
			result.setStatus(ResultStatus.SYS_ERROR);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ObjectResult getBoxs(JSONObject json) {
		ObjectResult result = new ObjectResult();
		try {
			//定义存储盒子的list
			List<BoxDto> boxs = new ArrayList<>();
			//获取json所有的key
			Set<String> set = json.keySet();
			//循环获取value并add进list中
			for (String string : set) {
				boxs.add(castBoxToBoxDto(boxDao.getBoxId(string)));
			}
			result.setResult(boxs);
			result.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			result.setStatus(ResultStatus.FAILED);
		}
		return result;
	}
}

package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.MessageRecordDto;
import com.bdbox.constant.MailType;
import com.bdbox.dao.MessageRecordDao;
import com.bdbox.entity.MessageRecord;
import com.bdbox.service.MessageRecordService;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Service
public class MessageRecordServiceImpl implements MessageRecordService{

	@Autowired
	private MessageRecordDao messageRecordDao;
	
	@Override
	public List<MessageRecordDto> getMsgAll(String mob, String content,
			Calendar startTime, Calendar endTime, int page, int pageSize) {
		List<MessageRecordDto> dtos = new ArrayList<>();
		try {
			List<MessageRecord> list = messageRecordDao
					.getMsgAll(mob, content, startTime, endTime, page, pageSize);
			for (MessageRecord mr : list) {
				dtos.add(castToDto(mr));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dtos;
	}

	/**
	 * 转换DTO
	 * @param mr
	 * @return
	 */
	protected MessageRecordDto castToDto(MessageRecord mr){
		MessageRecordDto dto = new MessageRecordDto();
		BeansUtil.copyBean(dto, mr, true);
		dto.setId(mr.getId());
		dto.setCreatedTime(CommonMethod.CalendarToString(mr.getCreatedTime(), "yyyy-MM-dd HH:mm:ss"));
		dto.setMailType(MailType.switchStatusForString(mr.getMailType()));
		return dto;
	}

	@Override
	public int count(String mob, String content, Calendar startTime, Calendar endTime) {
		return messageRecordDao.queryCount(mob, content, startTime, endTime);
	}

	@Override
	public void save(String mob, String msgContent, MailType type) {
		MessageRecord mr = new MessageRecord();
		mr.setMailType(type);
		mr.setMsgContent(msgContent);
		mr.setToMob(mob);
		messageRecordDao.save(mr);
	}
}

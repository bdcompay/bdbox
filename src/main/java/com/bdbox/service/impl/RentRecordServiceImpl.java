package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.dao.impl.RentRecordDaoImpl;
import com.bdbox.entity.RentRecord;
import com.bdbox.service.RentRecordService;
import com.bdbox.web.dto.RentRecordDto;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

@Service
public class RentRecordServiceImpl implements RentRecordService{
	
	@Autowired
	private RentRecordDaoImpl rentRecordDaoImpl;

	@Override
	public List<RentRecordDto> query(String user, Calendar startTime,
			Calendar endTime, int page, int pageSize) {
		List<RentRecordDto> dto = new ArrayList<>();
		List<RentRecord> list = rentRecordDaoImpl.query(user, startTime, endTime, page, pageSize);
		for (RentRecord rentRecord : list) {
			dto.add(castToDto(rentRecord));
		}
		return dto;
	}

	@Override
	public int queryAmount(String user, Calendar startTime, Calendar endTime) {
		return rentRecordDaoImpl.queryAmount(user, startTime, endTime);
	}

	@Override
	public RentRecord save(RentRecord rentRecord) {
		return rentRecordDaoImpl.save(rentRecord);
	}

	@Override
	public void update(RentRecord rentRecord) {
		rentRecordDaoImpl.update(rentRecord);
	}

	@Override
	public void delete(Long id) {
		rentRecordDaoImpl.delete(id);
	}
	

	@Override
	public List<RentRecordDto> getUserRentRecord(String user) {
		List<RentRecordDto> dto = new ArrayList<>();
		List<RentRecord> list = rentRecordDaoImpl.getUserRentRecord(user);
		for (RentRecord rentRecord : list) {
			dto.add(castToDto(rentRecord));
		}
		return dto;
	}
	
	public RentRecordDto castToDto(RentRecord rentRecord){
		RentRecordDto dto = new RentRecordDto();
		BeansUtil.copyBean(dto, rentRecord, true);
		if(rentRecord.getRecordTime()!=null){
			dto.setRecordTime(CommonMethod.CalendarToString(
					rentRecord.getRecordTime(), "yyyy-MM-dd HH:mm:ss"));
		}
		return dto;
	}

}

package com.bdbox.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.BDCoodDto;
import com.bdbox.constant.BdcoodStatus;
import com.bdbox.dao.BDCoodDao;
import com.bdbox.dao.UserDao;
import com.bdbox.entity.BDCood;
import com.bdbox.entity.User;
import com.bdbox.service.BDCoodService;
import com.bdbox.util.LogUtils;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;
@Service
public class BDCoodServiceImpl implements BDCoodService {
	@Autowired
	private BDCoodDao bdcoodDao;
	@Autowired
	private UserDao userDao;

	@Override
	public List<BDCood> listBDcood(String bdnumber,String ordernumber,String userpeople,String refereespeople,String isuser,  Calendar startTime,Calendar endTime,Calendar creantTime,String refereespeoplename,String bdcoodtype ,int page, int pageSize) {
		List<BDCood> bdcood = new ArrayList<BDCood>();
		try {
			 
			bdcood = bdcoodDao.querybdcood(bdnumber,ordernumber,userpeople,refereespeople,isuser,startTime, endTime,creantTime,refereespeoplename,bdcoodtype, page,pageSize);
		} catch (Exception e) {
			LogUtils.logerror("查询产品异常", e);
		}
		return bdcood;
	}
	
	@Override
	public int count(String bdnumber,String ordernumber,String userpeople,String refereespeople,String isuser,  Calendar startTime,Calendar endTime,Calendar creantTime,String refereespeoplename,String bdcoodtype) {
		int count = 0;
		try { 
			count = bdcoodDao.queryAmount(bdnumber,ordernumber,userpeople,refereespeople,isuser,startTime, endTime,creantTime,refereespeoplename,bdcoodtype);
		} catch (Exception e) {
			LogUtils.logerror("统计查询数量异常", e);
		}
		return count;
	}
	
	@Override
	public BDCoodDto castFrom(BDCood bdcood) {
		BDCoodDto dto = new BDCoodDto();
		BeansUtil.copyBean(dto, bdcood, true);
		if(bdcood.getCreatTime()!=null){
			dto.setCreatTime(CommonMethod.CalendarToString(bdcood.getCreatTime(), "yyyy-MM-dd HH:mm:ss"));
		}
		if(bdcood.getEndTime()!=null){
			dto.setEndTime(CommonMethod.CalendarToString(bdcood.getEndTime(), "yyyy-MM-dd HH:mm:ss"));
		}
		if(bdcood.getStartTime()!=null){
			dto.setStartTime(CommonMethod.CalendarToString(bdcood.getStartTime(), "yyyy-MM-dd HH:mm:ss"));
		} 
		if(bdcood.getBdcoodStatus().toString().equals("NOTUSER")){
			dto.setIsUser("未使用");
		} 
		if(bdcood.getBdcoodStatus().toString().equals("USER")){
			dto.setIsUser("已使用");
		}
		if(bdcood.getBdcoodStatus().toString().equals("EXCEED")){
			dto.setIsUser("已过期");
		}
		if(bdcood.getBdcoodStatus().toString().equals("NOTEXCEED")){
			dto.setIsUser("未过期");
		}
		if(bdcood.getRemakname()!=null){
			dto.setBeizhuname(bdcood.getRemakname()); 
		}
		dto.setBdCoodStatus(bdcood.getBdcoodStatus().toString());
		dto.setId(String.valueOf(bdcood.getId()));
		dto.setBdcood(bdcood.getBdcood());
		dto.setBeizhuname(bdcood.getRemakname()); 
		dto.setDayNum(bdcood.getDayNum());
		dto.setDiscount(bdcood.getDiscount());
		return dto;
	}
	
	
	public ObjectResult saveBdCood(String BDtype,String saveBuyStartTimeStr,String saveBuyEndTimeStr,String count,String price,String tuijianren,String remarkname,int dayNum,Double discount,String createCondition){
		String head="";
 		int numbers=0;//记录重复的数据
		int numbersuccses=0; //记录保存成功的数据
		Calendar StartTime = null;
		Calendar EndTime = null;
		try{ 
			StartTime = CommonMethod.StringToCalendar(saveBuyStartTimeStr, "yyyy-MM-dd HH:mm:ss"); 
			EndTime = CommonMethod.StringToCalendar(saveBuyEndTimeStr, "yyyy-MM-dd HH:mm:ss"); 
		}catch(Exception e){
			LogUtils.logerror("修改产品信息--字符串格式时间转Calendar时间异常", e);
		}
		ObjectResult objectResult=new ObjectResult();
		Set<String> setlist = new HashSet<String>(); 
		if(BDtype.equals("BDM") || BDtype.equals("BDM-zk")){
			head="BDM";
		 }else{
			head="BDQ";
		 }
		int counts=Integer.valueOf(count); 
		for(int i=0;i<counts;i++){
			String str="";
			for(int j=0;j<12;j++){
				str=str+(char)(Math.random()*26+'A'); 
			}
 			String bdcoodnumber=head+str;
			setlist.add(bdcoodnumber);
		} 
		List<BDCoodDto> BDCoodList = new ArrayList<>();
		for(String bdcood:setlist){
			BDCood bdCood=bdcoodDao.queryBdcood(bdcood);
			if(bdCood==null){
				BDCood bdCoodentys=new BDCood();
				bdCoodentys.setBdcood(bdcood);
				bdCoodentys.setStartTime(StartTime);
				bdCoodentys.setEndTime(EndTime);
				bdCoodentys.setType(BDtype);
				bdCoodentys.setBdcoodStatus(BdcoodStatus.NOTUSER);
				bdCoodentys.setMoney(price);
				bdCoodentys.setReferees(tuijianren);
				bdCoodentys.setRemakname(remarkname);
				bdCoodentys.setDayNum(dayNum);
				bdCoodentys.setDiscount(discount);
				bdCoodentys.setCreateCondition(createCondition);
				BDCood bdCoodenty = bdcoodDao.save(bdCoodentys);
				if(bdCoodenty!=null){
					numbersuccses=numbersuccses+1;
					BDCoodList.add(castFrom(bdCoodenty));
				}else{
					numbers=numbers+1;
				}
			}else{
				numbers=numbers+1;
			}
		} 
		//更新用户表对应的用户为推荐人
 		if(!"".equals(tuijianren)){
			  User  user=userDao.getUserByusername(tuijianren); 
			  user.setIsRefes(true);
			  user.setRecommend(remarkname);
			  userDao.update(user);
		}
		
		//更新用户表推荐人的备注名是否和北斗码推荐人的备注名一致，不一致，则修改
		List<BDCood> listeny=bdcoodDao.getUserByuser_twl(tuijianren);
	    if(!listeny.isEmpty()){
	    	for(BDCood bd:listeny){
	    		if(!bd.getRemakname().equals(remarkname)){
	    			bd.setRemakname(remarkname);
	    			bdcoodDao.update(bd);
	    		}
	    	}
	    }
		
		if(BDtype.equals("BDM") || BDtype.equals("BDM-zk") ){
			if(numbers==0){
					objectResult.setMessage("成功生成"+String.valueOf(numbersuccses)+"个北斗码");
					objectResult.setStatus(ResultStatus.OK);
					objectResult.setResult(BDCoodList);
			}else{
				if(numbersuccses==0){
					objectResult.setMessage("生成北斗码失败,请重新操作！");
					objectResult.setStatus(ResultStatus.FAILED);
				}else{
					int num=counts-numbers;
					objectResult.setMessage("成功生成"+String.valueOf(num)+"北斗码，其余"+numbers+"个失败");
				}
			}
		}else{
			if(numbers==0){
				objectResult.setMessage("成功生成"+String.valueOf(numbersuccses)+"张北斗券");
				objectResult.setStatus(ResultStatus.OK);
				objectResult.setResult(BDCoodList);
		}else{
			if(numbersuccses==0){
				objectResult.setMessage("生成北斗券失败,请重新操作！");
				objectResult.setStatus(ResultStatus.FAILED);
			}else{
				int num=counts-numbers;
				objectResult.setMessage("成功生成"+String.valueOf(num)+"张北斗券，其余"+numbers+"张失败");
			}
		}
		}
		return objectResult;
	}

	public Integer getRefeesUserCount_wl(String username){
		return bdcoodDao.getRefeesUserCount_wl(username);
	}

	public Integer getRefeesUserCount_wl1(String username){
		return bdcoodDao.getRefeesUserCount_wl1(username);
	}
	
	public Integer getRefeesUserCount_wl2(String username){
		return bdcoodDao.getRefeesUserCount_wl2(username);
	}
	
	public Integer getRefeesUserCount_wl3(String username){
		return bdcoodDao.getRefeesUserCount_wl3(username);
	}
	
	public Integer getRefeesUserCount_wl4(String username){
		return bdcoodDao.getRefeesUserCount_wl4(username);
	}
	
	public BDCood getRefeesUser_wl5(String username){
		return bdcoodDao.getRefeesUser_wl5(username); 
	}
	
	public BDCood getbdcood(Long id){
		return bdcoodDao.get(id);
	}
	
	public boolean update(BDCood bdcood){
		boolean result=false;
		if(bdcood!=null){
			bdcoodDao.update(bdcood);
			result=true;
		}else{
			result=false;
		}
		return result;
	}

	public BDCood getbdcood_wl(String bdcood){
		return bdcoodDao.getbdcoods_wl(bdcood);
	}
	
	public List<BDCood> queryBdcood(){
		return bdcoodDao.queryBdcood();
	}





}

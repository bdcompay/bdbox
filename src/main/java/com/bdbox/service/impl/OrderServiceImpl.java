package com.bdbox.service.impl;

import com.bdbox.constant.BdcoodStatus;
import com.bdbox.constant.DealStatus;
import com.bdbox.constant.OrderType;
import com.bdbox.constant.PayType;
import com.bdbox.constant.ProductType;
import com.bdbox.constant.RefundStatus;
import com.bdbox.dao.AddressDao;
import com.bdbox.dao.BDCoodDao;
import com.bdbox.dao.ExpressDao;
import com.bdbox.dao.InvoiceDao;
import com.bdbox.dao.NotificationDao;
import com.bdbox.dao.OrderDao;
import com.bdbox.dao.ProductDao;
import com.bdbox.dao.RefundDao;
import com.bdbox.dao.UnionpayOrderDao;
import com.bdbox.dao.UserDao;
import com.bdbox.entity.Address;
import com.bdbox.entity.BDCood;
import com.bdbox.entity.Express;
import com.bdbox.entity.Invoice;
import com.bdbox.entity.Notification;
import com.bdbox.entity.Order;
import com.bdbox.entity.Product;
import com.bdbox.entity.Refund;
import com.bdbox.entity.Returns;
import com.bdbox.entity.UnionpayOrder;
import com.bdbox.entity.User;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.OrderService;
import com.bdbox.service.RefundService;
import com.bdbox.service.ReturnsService;
import com.bdbox.unionpay.acp.consume.UnionPay;
import com.bdbox.util.LogUtils;
import com.bdbox.util.RemindDateUtils;
import com.bdbox.util.UtilDate;
import com.bdbox.web.dto.OrderDto;
import com.bdbox.wx.api.WxUnifiedOrder;
import com.bdbox.wx.util.CommonUtil;
import com.bdbox.wx.util.OrderNumUtil;
import com.bdbox.wx.util.QRCodeUtil;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.jdom.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sun.util.logging.resources.logging;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private RefundService refundService;
	@Autowired
	private OrderDao orderDao;
	@Autowired
	private ProductDao productDao;
	@Autowired
	private AddressDao addressDao;
	@Autowired
	private WxUnifiedOrder wxUnifiedOrder;
	@Autowired
	private RefundDao refundDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private ReturnsService returnsService;
	@Autowired
	private ExpressDao expressDao;
	@Autowired
	private BDCoodDao bdcoodDao;
	@Autowired
	private NotificationDao notificationDao;
	@Autowired
	private InvoiceDao invoiceDao;
	@Autowired
	private MailNotification mailNotification;
	@Autowired
	private UnionpayOrderDao unionpayOrderDao;

	public List<OrderDto> listOrder(String user, ProductType productType, String productName, Calendar buyTime,
			String orderNo, DealStatus dealStatus, Integer page, Integer pageSize, String order, String transactionType,
			String dealStatusStrbd, String name) {
		List list = this.orderDao.listOrder(user, productType, productName, buyTime, orderNo, dealStatus, page,
				pageSize, order, transactionType, dealStatusStrbd, name);
		List dtos = new ArrayList();
		Order entity;
		for (Iterator localIterator = list.iterator(); localIterator.hasNext(); dtos.add(castUserToUserDto(entity)))
			entity = (Order) localIterator.next();
		return dtos;
	}

	public int listOrderCount(String user, ProductType productType, String productName, Calendar buyTime,
			String orderNo, DealStatus dealStatus, String transactionType, String dealStatusStrbd,String name) {
		return this.orderDao.listOrderCount(user, productType, productName, buyTime, orderNo, dealStatus,
				transactionType, dealStatusStrbd,name);
	}

	public List<OrderDto> getUserOrder(String user) {
		List list = this.orderDao.getUserOrder(user);
		List dtos = new ArrayList();
		Order entity;
		for (Iterator localIterator = list.iterator(); localIterator.hasNext(); dtos.add(castUserToUserDto(entity)))
			entity = (Order) localIterator.next();
		return dtos;
	}

	public OrderDto getOrderDto(long id) {
		return castUserToUserDto((Order) this.orderDao.get(Long.valueOf(id)));
	}

	public void saveOrder(Order order) {
		this.orderDao.save(order);
	}

	public void deleteOrder(long id) {
		this.orderDao.delete(Long.valueOf(id));
	}

	public void updateOrder(Order order) {
		this.orderDao.update(order);
	}

	private OrderDto castUserToUserDto(Order order) {
		OrderDto dto = new OrderDto();
		try {
			SimpleDateFormat m_simpledateformat = new SimpleDateFormat("yyyy-MM-dd");
			long surplus = 0; // 剩余天数
			long exceed = 0; // 超期天数
			long count = 0; // 退款数量
			int xiadancount = order.getCount(); // 下单数量
			int shengyu = 0; // 已经归还的台数
			long sumss = 0;// 下单天数和超期天数总和
			BeansUtil.copyBean(dto, order, true);
			Returns returns = returnsService.queryReturn(order.getId());
			if (returns != null && returns.getCount() != 0) {
				count = returns.getCount();
			} else {
				count = 1;
			}
			if (order.getDealStatus().toString().equals("PAID")) {
				dto.setSehnyutaishu(order.getCount() + "台");
			} else {
				dto.setSehnyutaishu("无");
			}
			// 计算租用盒子租用的剩余天数或者是超期天数
			if (order.getBeginTime() != null) {
				Date date = new Date(); // 当前时间
				m_simpledateformat.format(date);
				Date nowtime = m_simpledateformat.parse(m_simpledateformat.format(date));
				Calendar complete = order.getBeginTime(); // 租用开始时间
				Date start = m_simpledateformat.parse(m_simpledateformat.format(complete.getTime()));
				String countday = order.getNumberdate(); // 租用天数

				// 计算到期时间
				Calendar daoqitime = order.getBeginTime(); // 租用开始时间
				String daoqi = m_simpledateformat.format(daoqitime.getTime());
				Date dateExecute = m_simpledateformat.parse(daoqi);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(dateExecute);
				calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + (Integer.valueOf(countday) - 1));
				dto.setExpiretime("到期时间：" + CommonMethod.CalendarToString(calendar, "yyyy/MM/dd"));
				String user = order.getUser();
				User username = userDao.getUserByusername(user);

				// 计算剩余归还的台数
				// int xiadancount=order.getCount(); //下单数量
				// int shengyu = 0; //已经归还的台数
				List<Returns> list = returnsService.listReturns(order.getId(), null, null, null, null, null, null, 1,
						9999, null);
				if (list != null && !list.isEmpty()) {
					for (Returns r : list) {
						// if(r.getReturnsStatusType().toString().equals("EXPRESS")||r.getReturnsStatusType().toString().equals("REFUND")||r.getReturnsStatusType().toString().equals("FINISH")){
						int rcount = r.getCount();
						shengyu += rcount;
						// }
					}
					if (xiadancount == shengyu) {
						dto.setSehnyutaishu("0台");
					} else {
						dto.setSehnyutaishu(String.valueOf(xiadancount - shengyu) + "台");
					}
				} else {
					dto.setSehnyutaishu(order.getCount() + "台");
				}

				// 计算剩余天数和超期天数
				if (order.getEndTime() == null) {
					long day = ((nowtime.getTime() - start.getTime()) / (1000 * 3600 * 24)) + 1;
					surplus = Long.valueOf(countday) - day;
					if (surplus > 0) {
						dto.setSurplusdate("剩余" + String.valueOf(surplus) + "天");
						dto.setNumb(countday);
						sumss = Long.valueOf(countday);
					} else if (surplus == 0) {
						dto.setSurplusdate("剩余" + String.valueOf(surplus) + "天");
						dto.setNumb(countday);
						sumss = Long.valueOf(countday);
					} else if (surplus < 0) {
						exceed = day - Long.valueOf(countday);
						dto.setSurplusdate("超期" + exceed + "天");
						dto.setNumb(String.valueOf(exceed + Long.valueOf(countday)));
						sumss = exceed + Long.valueOf(countday);
					}
				} else {
					if (shengyu == order.getCount()) {
						Calendar complete2 = order.getEndTime();
						Date end = m_simpledateformat.parse(m_simpledateformat.format(complete2.getTime()));
						long day = ((end.getTime() - start.getTime()) / (1000 * 3600 * 24)) + 1;
						surplus = Long.valueOf(countday) - day; // 租用天数-（租用结束时间-租用开始时间）的值大于0，则未超期
						if (surplus < 0) {
							exceed = day - Long.valueOf(countday);
							dto.setSurplusdate("超期" + exceed + "天");
							dto.setNumb(String.valueOf(exceed + Long.valueOf(countday)));
							sumss = exceed + Long.valueOf(countday);
						} else if (surplus == 0) {
							dto.setSurplusdate("剩余" + surplus + "天");
							dto.setNumb(countday);
							sumss = Long.valueOf(countday);
						} else if (surplus > 0) {
							dto.setSurplusdate("剩余" + surplus + "天");
							dto.setNumb(countday);
							sumss = Long.valueOf(countday);
						}
					} else {
						long day = ((nowtime.getTime() - start.getTime()) / (1000 * 3600 * 24)) + 1;
						surplus = Long.valueOf(countday) - day;
						if (surplus > 0) {
							dto.setSurplusdate("剩余" + String.valueOf(surplus) + "天");
							dto.setNumb(countday);
							sumss = Long.valueOf(countday);
						} else if (surplus == 0) {
							dto.setSurplusdate("剩余" + String.valueOf(surplus) + "天");
							dto.setNumb(countday);
							sumss = Long.valueOf(countday);
						} else if (surplus < 0) {
							exceed = day - Long.valueOf(countday);
							dto.setSurplusdate("超期" + exceed + "天");
							dto.setNumb(String.valueOf(exceed + Long.valueOf(countday)));
							sumss = exceed + Long.valueOf(countday);
						}
					}
				}
				double unitcost = 0;
				if (order.getUnitcost() == null) {
					unitcost = 0;
				} else {
					unitcost = Double.valueOf(order.getUnitcost());
				}
				long productid = order.getProductId();
				Product product = productDao.get(productid);
				if (username.getIsAutonym() != null) {
					long day = ((nowtime.getTime() - start.getTime()) / (1000 * 3600 * 24)) + 1;
					surplus = Long.valueOf(countday) - day;
					if (surplus < 0) {
						exceed = day - Long.valueOf(countday);
						day = exceed + Long.valueOf(countday);
					}
					int length = 0;
					Double discount = 0.0;
					if (order.getBdcood() != null) {
						String bdcood = order.getBdcood();
						String[] str = bdcood.split(",");
						for (String string : str) {
							BDCood bdCood = bdcoodDao.getbdcoods_wl(string);
							length += bdCood.getDayNum();
							try {
								if(bdCood.getDiscount()!=null)
									discount = bdCood.getDiscount() / 10;
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
					Double dis = discount > 0 ? discount : 1.0;
					if (order.getRenlnameorder() != null && order.getRenlnameorder().equals("1")) {
						if (xiadancount == shengyu) {
							dto.setSummony("");
						} else {
							BigDecimal bd = new BigDecimal(Double
									.parseDouble(String.valueOf((day - length) * unitcost * (xiadancount - shengyu) * dis)));
							dto.setSummony("租金合计：" + bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() + "元");
						}
					} else {
						if (xiadancount == shengyu) {
							dto.setSummony("");
						} else {
							BigDecimal bd = new BigDecimal(Double
									.parseDouble(String.valueOf((day - length) * unitcost * (xiadancount - shengyu) * dis)));
							dto.setSummony("租金合计：" + bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue() + "元");
						}
					}
				}
			} else {
				dto.setExpiretime("到期时间：无");
				dto.setSummony("租金合计：0元");
			}
			// 押金的金额，如果押金为空
			if (order.getSumantecedent() == null) {
				dto.setSumantecedent("0");
			}
			Express express = expressDao.queryExpressDto(order.getId());
			if (express != null) {
				dto.setExpressNumber(express.getExpressNumber());
			} else {
				dto.setExpressNumber(null);
			}
			dto.setNumberdate(order.getNumberdate());

			dto.setCreatedTime(CommonMethod.CalendarToString(order.getCreatedTime(), "yyyy/MM/dd HH:mm:ss"));
			dto.setCompleteTime(CommonMethod.CalendarToString(order.getCompleteTime(), "yyyy/MM/dd HH:mm"));
			dto.setBuyTime(CommonMethod.CalendarToString(order.getBuyTime(), "yyyy/MM/dd HH:mm"));
			if (order.getBeginTime() != null) {
				dto.setRentStartTime(CommonMethod.CalendarToString(order.getBeginTime(), "yyyy/MM/dd HH:mm"));
			}
			if (order.getEndTime() != null) {
				dto.setRentEndTime(CommonMethod.CalendarToString(order.getEndTime(), "yyyy/MM/dd HH:mm"));
			}
			if ((order.getProductType().toString() != null) && (!order.getProductType().toString().equals(""))
					&& (order.getProductType().toString().equals("TERMINAL"))) {
				dto.setProductType("终端设备");
			}

			if (order.getDealStatus().toString().equals("PAID")) {
				dto.setDealStatus("等待发货");
			} else if (order.getDealStatus().toString().equals("DEALCLOSE"))
				dto.setDealStatus("交易完成");
			else if (order.getDealStatus().toString().equals("WAITPAID"))
				dto.setDealStatus("未付款");
			else if (order.getDealStatus().toString().equals("SENT"))
				dto.setDealStatus("已发货");
			else if (order.getDealStatus().toString().equals("REBATES"))
				dto.setDealStatus("退款中");
			else if (order.getDealStatus().toString().equals("REFUNDED"))
				dto.setDealStatus("退款成功");
			else if (order.getDealStatus().toString().equals("CANCELREFUND"))
				dto.setDealStatus("关闭退款");
			else if (order.getDealStatus().toString().equals("CANCELORDER"))
				dto.setDealStatus("订单已取消");
			else if (order.getDealStatus().toString().equals("TOAUDIT"))
				dto.setDealStatus("待审核");
			else if (order.getDealStatus().toString().equals("WAITREFUND"))
				dto.setDealStatus("待退款");
			else if (order.getDealStatus().toString().equals("REFUNDFAIL"))
				dto.setDealStatus("退款失败");
			else if (order.getDealStatus().toString().equals("ABNORMAL"))
				dto.setDealStatus("物流异常");
			else {
				dto.setDealStatus("");
			}

			int shengyus = 0;
			List<Returns> list = returnsService.listReturns(order.getId(), null, null, null, null, null, null, 1, 9999,
					null);
			if (list != null && !list.isEmpty()) {
				for (Returns r : list) {
					int rcount = r.getCount();
					shengyus += rcount;
				}
			}
			if (xiadancount == shengyus && order.getTransactionType().equals("RENT")) {
				if (order.getDealStatus().toString().equals("DEALCLOSE")) {
					dto.setDealStatus("交易完成");
				}
			} else if (xiadancount != shengyus && order.getTransactionType().equals("RENT")) {

				if (order.getDealStatus().toString().equals("DEALCLOSE")) {
					if (order.getRenlnameorder() != null && order.getRenlnameorder().equals("1")) {
						if (sumss > 117) {
							dto.setDealStatus("交易完成");
						} else {
							dto.setDealStatus("等待归还");
						}
					} else {
						if (sumss > 71) {
							dto.setDealStatus("交易完成");
						} else {
							dto.setDealStatus("等待归还");
						}
					}
				}
			}
			if (order.getColor().equals("black"))
				dto.setColor("黑色");
			else if (order.getColor().equals("white")) {
				dto.setColor("白色");
			}
			if (order.getVersions().equals("bluetooth"))
				dto.setVersions("蓝牙版");
			else if (order.getVersions().equals("wifi")) {
				dto.setVersions("Wifi版");
			}
			dto.setProductId(String.valueOf(order.getProductId()));
			if (order.getTransactionType() != null) {
				if (order.getTransactionType().equals("RENT")) {
					dto.setTransactionType("租用");
				} else if (order.getTransactionType().equals("GENERAL")) {
					dto.setTransactionType("购买");
				} else if (order.getTransactionType().equals("")) {
					dto.setTransactionType("购买");
				}
			} else {
				dto.setTransactionType("购买");
			}
			if (order.getBdcood() != null && !"".equals(order.getBdcood())) {
				dto.setBdcood(order.getBdcood());
				String bdcood = order.getBdcood();
				String[] str = bdcood.split(",");
				int lengths = str.length;
				BDCood bd = bdcoodDao.getbdcoods_wl(order.getBdcood());
				if (bd != null) {
					dto.setBdcoodname(bd.getReferees());
				} else {
					if (order.getTransactionType().equals("RENT") && lengths != 0) {
						dto.setBdcoodname(String.valueOf(lengths));
					} else {
						dto.setBdcoodname("无");
					}
				}
			} else {
				dto.setBdcood("无");
				dto.setBdcoodname("无");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (order.getPayType() != null) {
			String zfType = PayType.switchStatusToString(order.getPayType());
			dto.setZfType(zfType);
		}
		return dto;
	}

	public Order getOrder(Long id) {
		return (Order) this.orderDao.get(id);
	}

	public List<OrderDto> listAllSendOrder() {
		List list = this.orderDao.listAllSendOrder();
		List dtos = new ArrayList();
		Order entity;
		for (Iterator localIterator = list.iterator(); localIterator.hasNext(); dtos.add(castUserToUserDto(entity)))
			entity = (Order) localIterator.next();
		return dtos;
	}

	public void updateByTradeNo(String tradeNo) {
		this.orderDao.updateOrderByTradeNo(tradeNo);
	}

	/**
	 * 查询订单接口 1).如果支付成功的订单就不查询， 2).如果查询到的交易状态与本地不一致，修改为相应的本地状态
	 * 
	 * @param trade_no
	 *            商户订单号
	 */
	public void doWxQueryOrder(String trade_no) {
		try {
			Order order = getOrferByOrderNO(trade_no);
			if (order == null)
				return;
			if ("DEALCLOSE".equals(order.getDealStatus()._str()))
				return;

			String XMLData = wxUnifiedOrder.queyOrder(null, trade_no);
			Map map = CommonUtil.xmlToArray(XMLData);
			if ("SUCCESS".equals(map.get("result_code"))) {
				// SUCCESS—支付成功
				if ("SUCCESS".equals(map.get("trade_state")))
					order.setDealStatus(DealStatus.PAID);
				// REFUND—转入退款
				if ("REFUND".equals(map.get("trade_state")))
					order.setDealStatus(DealStatus.REBATES);
				// NOTPAY—未支付
				if ("NOTPAY".equals(map.get("trade_state")))
					order.setDealStatus(DealStatus.WAITPAID);
				// CLOSED—已关闭
				if ("CLOSED".equals(map.get("trade_state")))
					order.setDealStatus(DealStatus.CANCELORDER);
				// REVOKED—已撤销（刷卡支付）
				if ("REVOKED".equals(map.get("trade_state")))
					order.setDealStatus(DealStatus.CANCELORDER);
				// USERPAYING--用户支付中
				if ("USERPAYING".equals(map.get("trade_state")))
					order.setDealStatus(DealStatus.WAITPAID);
				// PAYERROR--支付失败(其他原因，如银行返回失败)
				if ("PAYERROR".equals(map.get("trade_state"))) {
					order.setDealStatus(DealStatus.CANCELORDER);
					order.setExplain("PAYERROR--支付失败(其他原因，如银行返回失败)");
				}

				updateOrder(order);
				if (order.getDealStatus() == DealStatus.CANCELORDER) {
					if (order.getBdcood() != null && order.getBdcood() != "") {
						String[] bdticet = order.getBdcood().split(",");
						for (String str : bdticet) {
							BDCood bdcood = bdcoodDao.getbdcoods_wl(str);
							bdcood.setBdcoodStatus(BdcoodStatus.NOTUSER);
							bdcood.setOrderNo(null);
							bdcoodDao.update(bdcood);
						}
					}
				}

				LogUtils.loginfo("查询订单:" + trade_no + "  成功,订单状态为：" + map.get("trade_state"));
			} else {
				LogUtils.loginfo("查询订单:" + trade_no + "  失败，失败原因：" + map.get("err_code_des"));
			}
		} catch (Exception e) {
			LogUtils.logerror("查询订单 " + trade_no + " 异常", e);
		}
	}

	/**
	 * 查询退款
	 * 
	 * @param refund_no
	 */
	public Boolean doWxQueryRefund(String refund_no) {
		Boolean result = null;
		try {
			// 查询退款信息
			Refund refund = refundService.queryRefundByRefund_no(refund_no);
			if (refund == null) {
				result = false;
				return result;
			}
			String XMLData = wxUnifiedOrder.refundQuery(null, null, refund_no, null);
			Map map = CommonUtil.xmlToArray(XMLData);
			if ("SUCCESS".equals(map.get("result_code"))) {
				Order order = getOrder(refund.getOrderId());
				if ("SUCCESS".equals(map.get("refund_status_0"))) { // SUCCESS—退款成功
					// 更新退款信息
					refund.setRefundStatus(RefundStatus.ACCOMPLISH);
					// 退款成功具体时间不确定，在此以系统获知退款成功时间为退款时间
					refund.setRefundTime(Calendar.getInstance());
					refundService.updateRefund(refund);

					// 更新订单状态
					order.setDealStatus(DealStatus.REFUNDED);
					updateOrder(order);

					LogUtils.loginfo("微信退款成功,订单号：" + order.getOrderNo() + ",退款单号:" + refund_no + "  ");
					result = true;
				} else {
					// PROCESSING—退款处理中
					if ("PROCESSING".equals(map.get("refund_status_0"))) {
						LogUtils.loginfo("退款单号:" + refund_no + " 的微信退款正在处理中 ");
					} else {
						String explain = "";
						// FAIL—退款失败
						if ("PROCESSING".equals(map.get("refund_status_0"))) {
							explain = "FAIL—退款失败";
						}
						// NOTSURE—未确定，需要商户原退款单号重新发起
						if ("NOTSURE".equals(map.get("refund_status_0"))) {
							explain = "NOTSURE—未确定，需要商户原退款单号重新发起";
						}
						// CHANGE—转入代发，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，
						// 资金回流到商户的现金帐号，需要商户人工干预，通过线下或者财付通转账的方式进行退款。
						if ("CHANGE".equals(map.get("refund_status_0"))) {
							explain = "CHANGE—转入代发，退款到银行发现用户的卡作废或者冻结了，" + "导致原路退款银行卡失败，资金回流到商户的现金帐号，需要商户人工干预，通过线下或"
									+ "者财付通转账的方式进行退款。";
						}
						refund.setRefundStatus(RefundStatus.FAIL);
						refund.setExplain(explain);
						refundService.updateRefund(refund);

						order.setDealStatus(DealStatus.REFUNDFAIL);
						order.setExplain(explain);
						updateOrder(order);

						LogUtils.logwarn("退款单号:" + refund_no + "退款失败，失败原因：" + explain, null);
					}
					result = false;
				}
			} else {
				LogUtils.logwarn("微信查询退款失败，退款单号:" + refund_no + ",失败原因：" + map.get("err_code_des"), null);
				result = false;
			}
		} catch (Exception e) {
			LogUtils.logerror("微信查询退款异常，退款单号:" + refund_no, e);
		}
		return result;
	}

	@Override
	public Order getOrferByOrderNO(String trade_no) {
		String hql = " from Order where 1=1 and trade_no='" + trade_no + "'";
		List<Order> orders = orderDao.getList(hql, 1, 1);
		if (orders != null && orders.size() > 0) {
			return orders.get(0);
		}
		return null;
	}

	public Boolean updateOrderCancel(String orderNo) {
		Boolean result = null;
		Map map = new HashMap();
		try {
			List<Order> list = this.orderDao.getOder(orderNo);
			if (list.size() != 0) {
				for (Order o : list) {
					if ("未付款".equals(o.getDealStatus()._str())) {
						String resutl = wxUnifiedOrder.closeOrder(o.getTrade_no());
						map = CommonUtil.xmlToArray(resutl);
						if (map.get("result_code").equals("SUCCESS")) {
							LogUtils.loginfo("微信" + o.getTrade_no() + "订单关闭成功");
							o.setDealStatus(DealStatus.CANCELORDER);
							this.orderDao.update(o);
							if (o.getDealStatus() == DealStatus.CANCELORDER) {
								if (o.getBdcood() != null && o.getBdcood() != "") {
									String[] bdticet = o.getBdcood().split(",");
									for (String str : bdticet) {
										BDCood bdcood = bdcoodDao.getbdcoods_wl(str);
										bdcood.setBdcoodStatus(BdcoodStatus.NOTUSER);
										bdcood.setOrderNo(null);
										bdcoodDao.update(bdcood);
									}
								}
							}

							Product product = productDao.get(o.getProductId());
							product.setSaleNumber(product.getSaleNumber() - o.getCount());
							product.setStockNumber(product.getStockNumber() + o.getCount());
							productDao.update(product);

							result = true;
						} else {
							result = false;
						}
					} else {
						result = false;
					}
				}
			} else {
				result = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public boolean updateOrderDelete(String orderNo) {
		Boolean result = null;
		try {
			List<Order> list = this.orderDao.getOder(orderNo);
			if (list.size() != 0) {
				for (Order o : list) {
					o.setCancelstate("yes");
					this.orderDao.update(o);
					result = true;
				}
			} else {
				result = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updateOrderRefund(String orderNo) {
		Boolean result = null;
		Map map = new HashMap();
		try {
			List<Order> list = this.orderDao.getOder(orderNo);
			if (list.size() != 0) {
				for (Order o : list) {
					if ("付款成功".equals(o.getDealStatus()._str())) {
						o.setDealStatus(DealStatus.TOAUDIT);
						this.orderDao.update(o);
						result = true;

						// 以下是下单成功后给管理员发送邮件通知
						StringBuffer content = new StringBuffer();
						String type = "BUTREFUND"; // 通知类型
						String type_twl = "admin";
						if (o.getTransactionType().equals("RENT")) {
							type = "RENTREFUND";
						}
						String invoices = ""; // 是否开发票
						String tomail = null; // 收信箱
						String subject = null; // 主题
						String mailcentent = null; // 内容
						Notification notification = notificationDao.getnotification(type, type_twl);
						if (notification != null) {
							if (notification.getIsemail().equals(true)) {
								tomail = notification.getTomail();
								subject = notification.getSubject();
								if (o.getInvoice() != 0) {
									Invoice enty = invoiceDao.getInvoice(o.getInvoice());
									invoices = "，发票抬头：" + enty.getCompanyName();
								}
								content.append(notification.getCenter() + ":")
										.append("订单号：" + o.getOrderNo() + ",下单时间："
												+ CommonMethod.CalendarToString(Calendar.getInstance(),
														"yyyy/MM/dd HH:mm:ss")
												+ "，下单数量：" + o.getCount() + "，收货地址：" + o.getSite() + "联系人："
												+ o.getName() + "，联系方式：" + o.getPhone())
										.append(invoices);
								if (content != null) {
									mailcentent = content.toString();
								}
								mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
								// mailNotification.sendNote(notification.getReceiveTeliPhone(),
								// notification.getNotecenter());
							}
						}

					} else {
						result = false;
					}
				}
			} else {
				result = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updateOrderClose(String orderNo) {
		Boolean result = null;
		try {
			List<Order> list = this.orderDao.getOder(orderNo);
			if (list.size() != 0) {
				for (Order o : list) {
					if ("待审核".equals(o.getDealStatus()._str())) {
						o.setDealStatus(DealStatus.PAID);
						this.orderDao.update(o);
						result = true;
					} else {
						result = false;
					}
				}
			} else {
				result = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updateOrderShipments(String orderNo) {
		Boolean result = null;
		try {
			List<Order> list = this.orderDao.getOder(orderNo);
			if (list.size() != 0) {
				for (Order o : list) {
					if ("待审核".equals(o.getDealStatus()._str())) {
						o.setDealStatus(DealStatus.WAITREFUND);
						this.orderDao.update(o);
						result = true;
					} else {
						result = false;
					}
				}
			} else {
				result = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public boolean updateOrderrefund(String orderNo) {
		Boolean result = null;
		Map map = new HashMap();
		try {
			List<Order> list = this.orderDao.getOder(orderNo);
			if (list.size() != 0) {
				for (Order o : list) {
					if ("待退款".equals(o.getDealStatus()._str())) {

						String resutl = wxUnifiedOrder.refundQuery(o.getTrade_no(), null, null, null);
						map = CommonUtil.xmlToArray(resutl);
						if (map.get("return_code").equals("SUCCESS")) {
							LogUtils.loginfo("微信" + o.getTrade_no() + "退款成功，系统将在3个工作日内将退款返回买家账号");
							o.setDealStatus(DealStatus.REBATES);
							this.orderDao.update(o);
							result = true;
						} else {
							result = false;
						}
					} else {
						result = false;
					}
				}
			} else {
				result = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<Order> getOrder(String orderNo) {
		List<Order> list = this.orderDao.getOder(orderNo);
		if (list.size() != 0) {
			return list;
		} else {
			return null;
		}
	}

	public List<Refund> getrefundNo(String orderNo) {
		List<Refund> list = this.refundDao.getrefundNo(orderNo);
		if (list.size() != 0) {
			return list;
		} else {
			return null;
		}
	}

	@Override
	public Order userBuyOrder(long productId, Double price, int count, Double totalPrice, long addressId, String user,
			String productBuyType, Long invoice, String bdcoodnember, String zfType) {
		try {
			// 获得产品信息
			Product product = productDao.get(productId);
			if (product.getStockNumber() < 1) {
				LogUtils.loginfo("产品" + product.getName() + "无货");
				return null;
			}

			// 获取收货地址信息
			Address address = addressDao.get(addressId);
			// 填写购买订单信息
			Order order = new Order();

			// 调用微信统一下单接口
			String orderNo = UtilDate.getOrderNum();
			String trade_no = OrderNumUtil.getNum();

			if ("wechat".equals(zfType)) {
				SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
				parameters.put("body", product.getName());
				parameters.put("out_trade_no", trade_no);
				// 把价格转为以分为单位，并转为String类型
				String wxprice = String.valueOf((int) (totalPrice * 100));
				parameters.put("total_fee", wxprice);

				Map map = null;
				try {
					map = CommonUtil.xmlToArray(this.wxUnifiedOrder.getDynamicCodeUrl(parameters));
				} catch (JDOMException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				String qrcode = QRCodeUtil.getCode((String) map.get("code_url"));
				order.setQRCode(qrcode);
				order.setQrcodeTime(String.valueOf(new Date().getTime() + 3600000L));
			}

			String site2 = address.getProvince() + address.getCity() + address.getCounty()
					+ address.getDetailedAddress() + "   \t\t邮编：" + address.getPostCode();
			order.setAreaCode(address.getAreaCode());
			// 订单自身信息
			order.setTrade_no(trade_no);
			order.setUser(user);
			order.setCreatedTime(Calendar.getInstance());
			order.setOrderNo(orderNo);
			order.setPrice(price);
			order.setCount(count);
			order.setDealStatus(DealStatus.WAITPAID);
			order.setAreaCode(address.getAreaCode());

			order.setCancelstate("no");

			// 产品信息
			order.setProductId(product.getId());
			order.setProductName(product.getName());
			order.setProductType(product.getProductType());
			order.setProductUrl(product.getProductUrl());
			order.setColor(product.getColor());
			order.setVersions("1.0");
			order.setPayAmount(totalPrice);
			// order.setMinImg(img);

			order.setTransactionType(productBuyType);

			// 收货信息
			order.setSite(site2);
			order.setName(address.getName());
			order.setPhone(Long.valueOf(address.getMob()));
			// order.setEmail(address.get);

			// 保存发票
			order.setInvoice(invoice);
			// 保存北斗码
			order.setBdcood(bdcoodnember);
			if (bdcoodnember != null && !bdcoodnember.equals("")) {
				order.setOrderType(OrderType.BDORDER);
				BDCood enty = bdcoodDao.getbdcoods_wl(bdcoodnember);
				enty.setBdcoodStatus(BdcoodStatus.USER);
				enty.setOrderNo(orderNo);
				bdcoodDao.update(enty);
			} else {
				order.setOrderType(OrderType.COMMONORDER);
			}

			// 保存支付方式
			PayType payType = PayType.switchStatus(zfType);
			order.setPayType(payType);

			// 保存订单信息
			this.orderDao.save(order);

			// 修改产品库存
			product.setSaleNumber(product.getSaleNumber() + count);
			product.setStockNumber(product.getStockNumber() - count);
			productDao.update(product);

			return order;
		} catch (Exception e) {
			LogUtils.logerror("用户购买商品，下单时异常", e);
		}
		return null;
	}

	@Override
	public Order userRentOrder(long productId, Double price, int count, Double totalPrice, long addressId, String user,
			String productBuyType, String rentPrice, String r_count, String leaveMessage, String r_price,
			String renlnameorder, String unitcost, String bdcoodnember, String zfType) {
		try {
			// 获得产品信息
			Product product = productDao.get(productId);
			if (product.getStockNumber() < 1) {
				LogUtils.loginfo("产品" + product.getName() + "无货");
				return null;
			}

			// 获取收货地址信息
			Address address = addressDao.get(addressId);

			// 填写购买订单信息
			Order order = new Order();

			// 调用微信统一下单接口
			String orderNo = UtilDate.getOrderNum();
			String trade_no = OrderNumUtil.getNum();

			if ("wechat".equals(zfType)) {
				SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
				parameters.put("body", product.getName());
				parameters.put("out_trade_no", trade_no);
				// 把价格转为以分为单位，并转为String类型
				String wxprice = String.valueOf((int) (totalPrice * 100));
				parameters.put("total_fee", wxprice);

				Map map = null;
				try {
					map = CommonUtil.xmlToArray(this.wxUnifiedOrder.getDynamicCodeUrl(parameters));
				} catch (JDOMException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				String qrcode = QRCodeUtil.getCode((String) map.get("code_url"));
				order.setQRCode(qrcode);
				order.setQrcodeTime(String.valueOf(new Date().getTime() + 3600000L));
			}

			String site2 = address.getProvince() + address.getCity() + address.getCounty()
					+ address.getDetailedAddress() + "   \t\t邮编：" + address.getPostCode();
			order.setAreaCode(address.getAreaCode());
			// 订单自身信息
			order.setTrade_no(trade_no);
			order.setUser(user);
			order.setCreatedTime(Calendar.getInstance());
			order.setOrderNo(orderNo);
			order.setPrice(price);
			order.setCount(count);
			order.setDealStatus(DealStatus.WAITPAID);

			order.setCancelstate("no");

			// 产品信息
			order.setProductId(product.getId());
			order.setProductName(product.getName());
			order.setProductType(product.getProductType());
			order.setProductUrl(product.getProductUrl());
			order.setColor(product.getColor());
			order.setVersions("1.0");
			order.setPayAmount(totalPrice);
			// order.setMinImg(img);

			// 租用信息
			order.setTransactionType(productBuyType);
			order.setRental(rentPrice);
			order.setNumberdate(r_count);
			order.setLeavemessage(leaveMessage);
			order.setSumrental(r_price);
			order.setSumantecedent(String.valueOf(totalPrice));
			order.setAntecedent(String.valueOf(price));

			// 收货信息
			order.setSite(site2);
			order.setName(address.getName());
			order.setPhone(Long.valueOf(address.getMob()));
			order.setRenlnameorder(renlnameorder);
			order.setUnitcost(unitcost);

			if (bdcoodnember != null && !bdcoodnember.equals("")) {
				order.setBdcood(bdcoodnember);
				order.setOrderType(OrderType.BDORDER);
			} else {
				order.setOrderType(OrderType.COMMONORDER);
			}

			// order.setEmail(address.get);

			// 保存支付方式
			PayType payType = PayType.switchStatus(zfType);
			order.setPayType(payType);

			// 保存订单信息
			this.orderDao.save(order);
			String[] bdcoodnembers = null;
			if (bdcoodnember != null && !bdcoodnember.equals("")) {
				bdcoodnembers = bdcoodnember.split(",");
			}
			if (bdcoodnembers != null) {
				for (String str : bdcoodnembers) {
					order.setOrderType(OrderType.BDORDER);
					BDCood enty = bdcoodDao.getbdcoods_wl(str);
					enty.setBdcoodStatus(BdcoodStatus.USER);
					enty.setOrderNo(orderNo);
					bdcoodDao.update(enty);
				}
			}

			// 修改产品库存
			product.setSaleNumber(product.getSaleNumber() + count);
			product.setStockNumber(product.getStockNumber() - count);
			productDao.update(product);

			return order;
		} catch (Exception e) {
			e.printStackTrace();
			LogUtils.logerror("用户购买商品，下单时异常", e);
		}
		return null;
	}

	@Override
	public boolean queryOrderByPidUser(long pid, String username) {
		try {
			List<Order> orders = orderDao.queryOrderByPidUser(pid, username);
			if (orders != null && orders.size() > 0)
				return true;
		} catch (Exception e) {
			LogUtils.logerror("通过产品id和用户名查找订单异常", e);
		}
		return false;
	}

	public Order getOrderstatus(String ordreNo) {
		Order order = orderDao.getOrderstatus(ordreNo);
		return order;
	}

	// 租用订单
	public List<OrderDto> listOrder2(String user, ProductType productType, String productName, Calendar buyTime,
			String orderNo, DealStatus dealStatus, Integer page, Integer pageSize, String order, String transactionType,
			String dealStatusStrbd,String name) {
		List list = this.orderDao.listOrder2(user, productType, productName, buyTime, orderNo, dealStatus, page,
				pageSize, order, transactionType, dealStatusStrbd, name);
		List dtos = new ArrayList();
		Order entity;
		for (Iterator localIterator = list.iterator(); localIterator.hasNext(); dtos.add(castUserToUserDto(entity)))
			entity = (Order) localIterator.next();
		return dtos;
	}

	public int listOrderCount2(String user, ProductType productType, String productName, Calendar buyTime,
			String orderNo, DealStatus dealStatus, String transactionType, String dealStatusStrbd,String name) {
		return this.orderDao.listOrderCount2(user, productType, productName, buyTime, orderNo, dealStatus,
				transactionType, dealStatusStrbd, name);
	}

	// 租赁订单查询
	public List<OrderDto> getUserRentOrder(String user) {
		List list = this.orderDao.getUserRentOrder(user);
		List dtos = new ArrayList();
		Order entity;
		for (Iterator localIterator = list.iterator(); localIterator.hasNext(); dtos.add(castUserToUserDto(entity)))
			entity = (Order) localIterator.next();
		return dtos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.bdbox.service.OrderService#updateOrderTradeNo(long)
	 */
	@Override
	public ObjectResult updateOrderTradeNo(long oid) {
		/*
		 * 代码流程: 1.查询微信预订单是否失效 2.为失效，关闭微信订单，是否成功关闭 3.成功关闭，重新下单获得新微信订单号
		 * 4.更新订单中的交易号
		 *
		 */
		ObjectResult objectResult = new ObjectResult();
		String message = "";
		try {
			Order order = orderDao.get(oid);

			String XMLData = wxUnifiedOrder.queyOrder(null, order.getTrade_no());
			Map map = CommonUtil.xmlToArray(XMLData);
			if ("SUCCESS".equals(map.get("result_code"))) {
				// SUCCESS—支付成功
				if ("SUCCESS".equals(map.get("trade_state"))) {
					message = "订单id为：" + oid + " 已支付成功";
					objectResult.setStatus(ResultStatus.FAILED);

				}
				// REFUND—转入退款
				else if ("REFUND".equals(map.get("trade_state")))
					order.setDealStatus(DealStatus.REBATES);
				// NOTPAY—未支付
				else if ("NOTPAY".equals(map.get("trade_state")))
					order.setDealStatus(DealStatus.WAITPAID);
				// CLOSED—已关闭
				else if ("CLOSED".equals(map.get("trade_state")))
					order.setDealStatus(DealStatus.CANCELORDER);
				// REVOKED—已撤销（刷卡支付）
				else if ("REVOKED".equals(map.get("trade_state")))
					order.setDealStatus(DealStatus.CANCELORDER);
				// USERPAYING--用户支付中
				else if ("USERPAYING".equals(map.get("trade_state"))) {
					message = "订单id为：" + oid + " 的订单用户正在支付中";
					objectResult.setStatus(ResultStatus.FAILED);
				}
				// PAYERROR--支付失败(其他原因，如银行返回失败)
				else if ("PAYERROR".equals(map.get("trade_state"))) {
					order.setDealStatus(DealStatus.CANCELORDER);
				}
			}
			// 订单支付完成或正在支付中
			if (objectResult.getStatus().equals(ResultStatus.FAILED)) {
				LogUtils.loginfo(message);
				objectResult.setMessage(message);
				return objectResult;
			}

			// 订单未关闭
			if (!"CLOSED".equals(map.get("trade_state"))) {
				// 关闭订单
				String colosedXMLData = wxUnifiedOrder.closeOrder(order.getTrade_no());
				Map colosedMap = CommonUtil.xmlToArray(colosedXMLData);
				if (colosedMap.get("result_code").equals("SUCCESS")) {
					LogUtils.loginfo("订单id为：" + oid + " 的订单关闭成功");
				} else {
					message = "订单id为：" + oid + " 的订单关闭失败";
					LogUtils.loginfo(message);
					objectResult.setMessage(message);
					return objectResult;
				}
			}

			// 更新订单微信交易号
			// String orderNo = UtilDate.getOrderNum();
			String trade_no = OrderNumUtil.getNum();
			SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
			parameters.put("body", order.getProductName());
			parameters.put("out_trade_no", trade_no);
			// 把价格转为以分为单位，并转为String类型
			double totalPrice = order.getPrice() * order.getCount();
			String wxprice = String.valueOf((int) (totalPrice * 100));
			parameters.put("total_fee", wxprice);

			Map paymap = null;
			try {
				paymap = CommonUtil.xmlToArray(this.wxUnifiedOrder.getDynamicCodeUrl(parameters));
			} catch (JDOMException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			String qrcode = QRCodeUtil.getCode((String) paymap.get("code_url"));
			order.setTrade_no(trade_no);
			order.setQRCode(qrcode);
			order.setQrcodeTime(String.valueOf(new Date().getTime() + 3600000L));
			order.setValidBeginTime(Calendar.getInstance());
			orderDao.update(order);

			objectResult.setStatus(ResultStatus.OK);
		} catch (Exception e) {
			LogUtils.logerror("更新订单支付交易号错误", e);
			message = "系统错误";
			objectResult.setStatus(ResultStatus.FAILED);
		}
		objectResult.setMessage(message);
		return objectResult;
	}

	public List<Order> queryBdcood() {
		return orderDao.queryBdcood();
	}

	public List<Order> getOrderList(String deal, String trandtype) {
		return orderDao.getOrderList(deal, trandtype);
	}

	public Order getOrd(String tranOrder) {
		return orderDao.getOrd(tranOrder);
	}

	/**
	 * 判断租用日期是否超过30天、是则直接扣款到公司账户
	 */
	@Override
	public void authFinish() {
		try {
			List<Order> orderList = orderDao.getUnionPayRentOrders();
			// 当前时间
			Calendar now = Calendar.getInstance();
			if (orderList != null) {
				for (Order order : orderList) {
					// 租用开始时间
					Calendar beginTime = order.getBeginTime();
					// 相差天数
					Long dayNum = ((now.getTimeInMillis() - beginTime.getTimeInMillis()) / 24 * 60 * 60 * 1000);
					// 如果租用超过30天还未归还盒子则直接扣款到公司帐户
					if (dayNum >= 29) {
						UnionpayOrder uo = unionpayOrderDao.getUnionpayOrder(order.getOrderNo());
						// 银联预授权操作
						UnionPay.authFinish(String.valueOf(uo.getTxnAmt()), uo.getQueryId());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Map<String, Integer> statisticOrder(Calendar startTime, Calendar endTime, String statisticType,
			String transactionType) {
		Map<String, Integer> map = new TreeMap<>();
		try {
			if ("day".equals(statisticType)) {
				// 天统计
				Date start = startTime.getTime();
				Date end = endTime.getTime();
				map = statistic(start, end, transactionType);
			} else if ("week".equals(statisticType)) {
				// 周统计
				Date start = RemindDateUtils.getCurrentWeekDayStartTime();
				Date end = RemindDateUtils.getCurrentWeekDayEndTime();
				map = statistic(start, end, transactionType);
			} else if ("month".equals(statisticType)) {
				// 月统计
				Date start = RemindDateUtils.getCurrentMonthStartTime();
				Date end = RemindDateUtils.getCurrentMonthEndTime();
				map = statistic(start, end, transactionType);
			} else if ("season".equals(statisticType)) {
				// 季统计
				Date start = RemindDateUtils.getCurrentQuarterStartTime();
				Date end = RemindDateUtils.getCurrentQuarterEndTime();
				map = statistic(start, end, transactionType);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	protected Map<String, Integer> statistic(Date start, Date end, String transactionType) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, Integer> map = new TreeMap<>();
		// 一天的毫秒值
		long time = 86400000;
		// 存储变化值
		Date zdy = new Date();
		// 开始时间
		Calendar sTime = Calendar.getInstance();
		// 结束时间
		Calendar eTime = Calendar.getInstance();
		// 天数
		long num = (end.getTime() - start.getTime()) / time;
		// 遍历将统计的‘天数-数量’添加进map中
		for (int i = 0; i <= num; i++) {
			zdy.setTime(start.getTime() + (i * time));
			sTime.setTime(zdy);
			eTime.setTime(RemindDateUtils.getRamdomDayEndTime(zdy));
			int count = orderDao.statisticOrder(sTime, eTime, transactionType);
			map.put(sdf.format(zdy), count);
		}
		return map;
	}

}

package com.bdbox.service.impl;

import com.bdbox.dao.UserSuggestionDao;
import com.bdbox.entity.UserSuggestion;
import com.bdbox.service.UserSuggestionService;
import com.bdbox.web.dto.ManageUserDto;
import com.bdbox.web.dto.UserSuggestionDto;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserSuggestionServiceImpl
  implements UserSuggestionService
{

  @Autowired
  private UserSuggestionDao userSuggestionDao;

  public List<UserSuggestionDto> listUserSuggestions(String fromStr, String order, Integer page, Integer pageSize)
  {
    List<UserSuggestion> list = this.userSuggestionDao.listUserSuggestions(fromStr, order, page, pageSize);
 	List<UserSuggestionDto> dtos = new ArrayList<UserSuggestionDto>();

    for (UserSuggestion userSuggestion : list) {
      dtos.add(castToDto(userSuggestion));
    }
    return dtos;
  }

  public int listUserSuggestionsCount(String fromStr)
  {
    return this.userSuggestionDao.listUserSuggestionsCount(fromStr);
  }

  private UserSuggestionDto castToDto(UserSuggestion userSuggestion)
  {
    UserSuggestionDto userSuggestionDto = new UserSuggestionDto();
    BeansUtil.copyBean(userSuggestionDto, userSuggestion, true);
    userSuggestionDto.setCreatedTime(CommonMethod.CalendarToString(userSuggestion.getCreatedTime(), "yyyy/MM/dd HH:mm"));
    return userSuggestionDto;
  }

  public void deleteUserSuggestion(long id)
  {
    this.userSuggestionDao.delete(Long.valueOf(id));
  }
}
package com.bdbox.service.impl;

import com.bdbox.constant.DealStatus;
import com.bdbox.constant.RefundStatus;
import com.bdbox.constant.ReturnsStatusType;
import com.bdbox.dao.OrderDao;
import com.bdbox.dao.RefundDao;
import com.bdbox.dao.ReturnsDao;
import com.bdbox.entity.Order;
import com.bdbox.entity.Refund;
import com.bdbox.entity.Returns;
import com.bdbox.service.RefundService;
import com.bdbox.util.LogUtils;
import com.bdbox.web.dto.RefundDto;
import com.bdbox.wx.api.WxUnifiedOrder;
import com.bdbox.wx.util.CommonUtil;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RefundServiceImpl implements RefundService {

	@Autowired
	private RefundDao refundDao;
	@Autowired
	private OrderDao orderDao;
	@Autowired
	private ReturnsDao returnsDao;
	@Autowired
	private WxUnifiedOrder wxUnifiedOrder;

	public void saveRefund(Refund refund) {
		this.refundDao.save(refund);
	}

	public void updateRefund(Refund refund) {
		this.refundDao.update(refund);
	}

	public void deleteRefund(Long id) {
		this.refundDao.delete(id);
	}

	public Refund queryRefund(Long id) {
		return (Refund) this.refundDao.get(id);
	}

	public Refund queryRefundUid(Long oid) {
		return this.refundDao.queryRefundUid(oid);
	}

	public List<RefundDto> listRefund(String orderNo, Calendar applyTime,
			Calendar refundTime, String userName, RefundStatus refundStatus,
			Integer page, Integer pageSize, String order) {
		List list = this.refundDao.listRefund(orderNo, applyTime, refundTime,
				userName, refundStatus, page, pageSize, order);
		List dtos = new ArrayList();
		Refund entity;
		for (Iterator localIterator = list.iterator(); localIterator.hasNext(); dtos
				.add(castUserToUserDto(entity)))
			entity = (Refund) localIterator.next();
		return dtos;
	}

	public int listRefundCount(String orderNo, Calendar applyTime,
			Calendar refundTime, String userName, RefundStatus refundStatus) {
		return this.refundDao.listRefundCount(orderNo, applyTime, refundTime,
				userName, refundStatus);
	}

	private RefundDto castUserToUserDto(Refund refund) {
		RefundDto dto = new RefundDto();

		BeansUtil.copyBean(dto, refund, true);

		if (refund.getApplyTime() != null) {
			dto.setApplyTime(CommonMethod.CalendarToString(
					refund.getApplyTime(), "yyyy/MM/dd HH:mm"));
		}
		if (refund.getRefundTime() != null)
			dto.setRefundTime(CommonMethod.CalendarToString(
					refund.getRefundTime(), "yyyy/MM/dd HH:mm"));
		else {
			dto.setRefundTime("暂无");
		}

		if (refund.getRefundStatus().toString().equals("APPLY"))
			dto.setRefundStatus("退款中");
		else if (refund.getRefundStatus().toString().equals("ACCOMPLISH"))
			dto.setRefundStatus("退款成功");
		else if (refund.getRefundStatus().toString().equals("CANCEL")) {
			dto.setRefundStatus("关闭退款");
		}
		return dto;
	}

	public List<RefundDto> getUserRefund(String user) {
		List list = this.refundDao.listAll();
		List dtos = new ArrayList();
		Refund entity;
		for (Iterator localIterator = list.iterator(); localIterator.hasNext(); dtos
				.add(castUserToUserDto(entity)))
			entity = (Refund) localIterator.next();
		return dtos;
	}

	public RefundDto queryRefundDto(Long id) {
		return castUserToUserDto((Refund) this.refundDao.get(id));
	}

	@Override
	public Refund queryRefundByRefund_no(String refund_no) {
		String hql = "from Refund where 1=1 and refundNo='" + refund_no + "'";
		List<Refund> refunds = refundDao.getList(hql, 1, 1);
		if (refunds != null && refunds.size() > 0) {
			return refunds.get(0);
		}
		return null;
	}

	@Override
	public List<Refund> listRefundByRefundTime() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Refund getRefund(long id) {
		return refundDao.get(id);
	}
	
	/**
	 * 查询退款
	 * 
	 * @param refund_no
	 */
	public Boolean doWxQueryRefund(String refund_no) {
		  Boolean result=null;  
		try {
			// 查询退款信息
			Refund refund = queryRefundByRefund_no(refund_no);
			if (refund == null){
				result=false;
				return result;
			} 
			String XMLData = wxUnifiedOrder.refundQuery(null, null, refund_no,
					null);
			Map map = CommonUtil.xmlToArray(XMLData);
			if ("SUCCESS".equals(map.get("result_code"))) {
				
				
				
				if ("SUCCESS".equals(map.get("refund_status_0"))) { // SUCCESS—退款成功
					// 更新退款信息
					refund.setRefundStatus(RefundStatus.ACCOMPLISH);
					// 退款成功具体时间不确定，在此以系统获知退款成功时间为退款时间
					refund.setRefundTime(Calendar.getInstance());
					refundDao.update(refund);
					if(refund.getOrderId()!=null){
						Order order = orderDao.get(refund.getOrderId());
						// 更新订单状态
						order.setDealStatus(DealStatus.REFUNDED);
						orderDao.update(order);
						LogUtils.loginfo("微信退款成功,订单号：" + order.getOrderNo()
								+ ",退款单号:" + refund_no + "  ");
					}else{
						//根据退款单id查询退货信息
						String hql = "from Returns where refundId="+refund.getId();
						List<Returns> list = returnsDao.getList(hql, 1, 1);
						Returns returns = list.get(0);
						
						returns.setReturnsStatusType(ReturnsStatusType.FINISH);
						returnsDao.update(returns);
						LogUtils.loginfo("微信退款成功,退货单号：" + returns.getReturnsNum()
								+ ",退款单号:" + refund_no + "  ");
					}
					

					
					result=true; 
				} else {
					// PROCESSING—退款处理中
					if ("PROCESSING".equals(map.get("refund_status_0"))) {
						LogUtils.loginfo("退款单号:" + refund_no + " 的微信退款正在处理中 ");
					} else {
						String explain = "";
						// FAIL—退款失败
						if ("PROCESSING".equals(map.get("refund_status_0"))) {
							explain = "FAIL—退款失败";
						}
						// NOTSURE—未确定，需要商户原退款单号重新发起
						if ("NOTSURE".equals(map.get("refund_status_0"))) {
							explain = "NOTSURE—未确定，需要商户原退款单号重新发起";
						}
						// CHANGE—转入代发，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，
						// 资金回流到商户的现金帐号，需要商户人工干预，通过线下或者财付通转账的方式进行退款。
						if ("CHANGE".equals(map.get("refund_status_0"))) {
							explain = "CHANGE—转入代发，退款到银行发现用户的卡作废或者冻结了，"
									+ "导致原路退款银行卡失败，资金回流到商户的现金帐号，需要商户人工干预，通过线下或"
									+ "者财付通转账的方式进行退款。";
						}
						refund.setRefundStatus(RefundStatus.FAIL);
						refund.setExplain(explain);
						refundDao.update(refund);
						
						if(refund.getOrderId()!=null){
							Order order = orderDao.get(refund.getOrderId());
							order.setDealStatus(DealStatus.REFUNDFAIL);
							order.setExplain(explain);
							orderDao.update(order);
							LogUtils.logwarn("退款单号:" + refund_no + "退款失败，失败原因："
									+ explain, null);
						}else{
							//根据退款单id查询退货信息
							String hql = "from Returns where refundId="+refund.getId();
							List<Returns> list = returnsDao.getList(hql, 1, 1);
							Returns returns = list.get(0);
							
							returns.setReturnsStatusType(ReturnsStatusType.REFUNDFAIL);
							returns.setExplain(explain);
							returnsDao.update(returns);
							LogUtils.loginfo("微信退款成功,退货单号：" + returns.getReturnsNum()
									+ ",退款单号:" + refund_no + "  ");
						}
					}
					result=false; 
				}
			} else {
				LogUtils.logwarn(
						"微信查询退款失败，退款单号:" + refund_no + ",失败原因："
								+ map.get("err_code_des"), null);
				    result=false; 
			}
		} catch (Exception e) {
			LogUtils.logerror("微信查询退款异常，退款单号:" + refund_no, e);
		} 
		return result;
	}
}
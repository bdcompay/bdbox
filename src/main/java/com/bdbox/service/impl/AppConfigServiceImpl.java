package com.bdbox.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.api.dto.AppConfigDto;
import com.bdbox.constant.AppType;
import com.bdbox.dao.AppconfigDao;
import com.bdbox.entity.AppConfig;
import com.bdbox.service.AppConfigService;
import com.bdsdk.util.CommonMethod;

@Service
public class AppConfigServiceImpl implements AppConfigService {
	@Autowired
	private AppconfigDao appconfigDao;

	public AppConfigDto getAppConfig(AppType appType) {
		AppConfig appConfig = appconfigDao.query(appType);
		if (appConfig != null) {
			AppConfigDto appConfigDto = new AppConfigDto();
			appConfigDto.setCreatedTime(CommonMethod.CalendarToString(
					appConfig.getCreatedTime(), null));
			appConfigDto.setDownloadUrl(appConfig.getDownloadUrl());
			appConfigDto.setVersionCode(appConfig.getVersionCode());
			appConfigDto.setVersionDetail(appConfig.getVersionDetail());
			appConfigDto.setVersionName(appConfig.getVersionName());
			appConfigDto.setVersionSize(appConfig.getVersionSize());
			return appConfigDto;
		}
		return null;
	}

}

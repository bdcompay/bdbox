package com.bdbox.service.impl;

import java.util.Calendar;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdbox.constant.AlipayErrorCode;
import com.bdbox.constant.DealStatus;
import com.bdbox.constant.MailType;
import com.bdbox.constant.RefundStatus;
import com.bdbox.constant.ReturnsStatusType;
import com.bdbox.dao.AlipayRefundDao;
import com.bdbox.dao.NotificationDao;
import com.bdbox.dao.OrderDao;
import com.bdbox.dao.ReturnsDao;
import com.bdbox.entity.AlipayOrder;
import com.bdbox.entity.AlipayRefund;
import com.bdbox.entity.Notification;
import com.bdbox.entity.Order;
import com.bdbox.entity.Returns;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.AlipayRefundService;
import com.bdbox.service.MessageRecordService;

@Service
public class AlipayRefundServiceImpl implements AlipayRefundService{

	@Autowired
	private AlipayRefundDao alipayRefundDao;
	
	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private ReturnsDao returnsDao;
	@Autowired  
	private MailNotification mailNotification; 
	@Autowired 
	private NotificationDao notificationDao;
	@Autowired
	private MessageRecordService MessageRecordService;

	@Override
	public void saveOrUpdate(AlipayRefund alipayRefund) {
		alipayRefundDao.saveOrUpdate(alipayRefund);
	}

	@Override
	public void update(Map<String, String> params) {

		//批量退款数据中的详细信息
		String result_details = params.get("result_details");
		
		String detail_data = "";
		String[] datas = null;
		if(result_details.contains("\\&")){
			String[] str = result_details.split("\\&");
			detail_data = str[0];
			datas = detail_data.split("\\^");
		}else{
			datas = result_details.split("\\^");
		}
		
		AlipayRefund refund = alipayRefundDao.getAlipayRefund(datas[0]);
		Order order = alipayRefundDao.getOrder(datas[0]);
		Returns rt = alipayRefundDao.getReturns(order.getOrderNo());
		try {
			refund.setFinishTime(Calendar.getInstance());
			//修改状态
			if ("SUCCESS".equals(datas[2])) {
				refund.setRefundStatus(RefundStatus.ACCOMPLISH);
				order.setDealStatus(DealStatus.REFUNDED);
				if(rt!=null){
					rt.setReturnsStatusType(ReturnsStatusType.FINISH);
					//以下是管理员审核通过后给用户发送退款通知
		 			StringBuffer content=new StringBuffer(""); 
					String type="tuihuoshenqingtongguo";  //通知类型 
					String type_twl="customer"; 
					String dismony="";
					String returnmony="退回金额："+datas[1];
					String buymony="购买金额："+rt.getBuyMoney(); 
					if(rt.getDeductionMoney()!=0){
						 dismony="扣减金额："+rt.getDeductionMoney()+","; 
					}
					if(rt.getReturnType().equals("RENT")){
						type="guihuanshenqingtongguo";
						buymony="押金："+rt.getBuyMoney();
						dismony="租金："+rt.getDeductionMoney(); 

					} 
					String invoices="";            //是否开发票
					String tomail=null;            //收信箱
					String subject=null;           //主题
					String mailcentent=null;       //内容
					Notification notification=notificationDao.getnotification(type,type_twl);
					if(notification!=null){ 
						if(notification.getIsnote().equals(true)){  
							tomail=notification.getTomail();
							subject=notification.getSubject();  
							content.append(notification.getNotecenter()).append("退款单号："+rt.getReturnsNum()).append("，"+buymony+"，").append(dismony+"，").append(returnmony);
							if(content!=null){
								mailcentent=content.toString(); 
							}
							mailNotification.sendNote(String.valueOf(order.getPhone()), notification.getNotecenter()+mailcentent);
							//保存发送的短信记录
							MessageRecordService.save(String.valueOf(order.getPhone()), 
									notification.getNotecenter()+mailcentent, MailType.switchStatus(type));
						}
					}
					
				}
				refund.setAlipayErrorCode(null);
				
				if(order!=null){
					//以下是下单成功后给管理员发送邮件通知
		 			StringBuffer content=new StringBuffer(); 
					String type="BUYRENT";  //通知类型
					if(order.getTransactionType().equals("RENT")){
						type="RENTRETURN";
					}
					String type_twl="customer";  
					Notification notification=notificationDao.getnotification(type,type_twl);
					if(notification!=null){ 
						if(notification.getIsnote().equals(true)){   
							//mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
							mailNotification.sendNote(String.valueOf(order.getPhone()), notification.getNotecenter()+"退款金额："+datas[1]);
							//保存发送的短信记录
							MessageRecordService.save(String.valueOf(order.getPhone()), 
									notification.getNotecenter()+"退款金额："+datas[1], 
									MailType.switchStatus(type));
						}
					}
				}
				
			} else if("TRADE_HAS_CLOSED".equals(datas[2])) {
				refund.setRefundStatus(RefundStatus.CANCEL);
				order.setDealStatus(DealStatus.CANCELREFUND);
				if(rt!=null){
					rt.setReturnsStatusType(ReturnsStatusType.CANCEL);
				}
				refund.setAlipayErrorCode(null);
			} else {
				order.setDealStatus(DealStatus.REFUNDFAIL);
				refund.setRefundStatus(RefundStatus.FAIL);
				if(rt!=null){
					rt.setReturnsStatusType(ReturnsStatusType.REFUNDFAIL);
				}
				refund.setAlipayErrorCode(AlipayErrorCode.switchStatus(datas[2]));
			}
			refund.setResultDetail(result_details);
			//更新退款记录
			alipayRefundDao.update(refund);
			//更新订单
			orderDao.update(order);
			//更新退货订单
			returnsDao.update(rt);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public AlipayOrder getAlipayOrder(String outTradeNo) {
		return alipayRefundDao.getAlipayOrder(outTradeNo);
	}

	@Override
	public AlipayRefund getAlipayRefund(String tradeNo) {
		return alipayRefundDao.getAlipayRefund(tradeNo);
	}
}

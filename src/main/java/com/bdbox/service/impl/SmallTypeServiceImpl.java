package com.bdbox.service.impl;

import java.util.List;  

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;  

import com.bdbox.dao.FAQDao;
import com.bdbox.dao.SmallTypeDao;
import com.bdbox.entity.FAQ;
import com.bdbox.entity.SmallType;
import com.bdbox.service.SmallTypeService;
 
@Service
public class SmallTypeServiceImpl implements SmallTypeService {
	@Autowired
	private SmallTypeDao smallTypeDao;
	@Autowired
	private FAQDao fAQDao;
	public String gettypeSeclectOptions(String selecttype){
		List<SmallType> smallType = smallTypeDao.gettypelist(selecttype);
		StringBuffer userSelectHtml = new StringBuffer("<option value=''>无</option>");
		for (SmallType small : smallType) {  
			if (selecttype != null){
				if (small.getQuestionid() == selecttype) {
					userSelectHtml.append("<option value='")
							.append(small.getSmallType())
							.append("' selected=\"selected\">")
							.append(small.getName()).append("</option>");
				} else {
					userSelectHtml.append("<option value='")
							.append(small.getSmallType()).append("' >")
							.append(small.getName()).append("</option>");
				}
			} else {
				userSelectHtml.append("<option value='").append(small.getSmallType())
						.append("' >").append(small.getName()).append("</option>");
			}

		}
		return userSelectHtml.toString();
	}
	
	public SmallType saveFaqtype(String addtype, String Englishtypename,String chaintypename){
		SmallType enty=new SmallType();
		try{
			enty.setName(chaintypename);
			enty.setSmallType(Englishtypename);
			enty.setQuestionid(addtype);
			smallTypeDao.save(enty);
			return enty;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	public boolean update(String addtype, String Englishtypename,String chaintypename){
		boolean restult=false; 
		SmallType enty=smallTypeDao.gettype(addtype,Englishtypename);
		try{
			if(enty!=null){
				enty.setName(chaintypename);
				smallTypeDao.update(enty);
				//查询FAQ问答表
				List<FAQ> list=fAQDao.getfaqlist(addtype,Englishtypename);
				if(!list.isEmpty()){
					for(FAQ faq:list){
						//更新FAQ问答表
						faq.setSmallTypename(chaintypename);
						fAQDao.update(faq);
						restult=true;
					}
				}
			}else{
				restult=false;
			}
 		}catch(Exception E){
			E.printStackTrace();
		}
		return restult;
	}

	public SmallType gettype(String addtype,String Englishtypename){
		return smallTypeDao.gettype(addtype, Englishtypename);
	}
	
	public void updates(SmallType enty){
		smallTypeDao.update(enty);
	}
	
	public List<SmallType> getalltype(String lefttype){
		return smallTypeDao.gettypelist(lefttype);
	}




}

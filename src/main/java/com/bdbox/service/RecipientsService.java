package com.bdbox.service;

import java.util.List;

import com.bdbox.entity.Recipients;

public interface RecipientsService {

	/**
	 * 更改默认收件人信息
	 * @param id
	 */
	public void updateDefault(Long id);
	
	/**
	 * 设置所有信息默认收件人信息为0
	 */
	public void doSetZero();
	
	/**
	 * 分页获取数据
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<Recipients> getList(Integer page,Integer pageSize);
	
	/**
	 * 获取所有信息数量
	 * @return
	 */
	public int count();
	
	/**
	 * 获取默认的收件人信息
	 * @return
	 */
	public Recipients getRecipients();
	
	/**
	 * 添加数据
	 * @param recipients
	 */
	public void save(Recipients recipients);
	
	/**
	 * 删除数据
	 * @param recipients
	 */
	public void delete(Long id);
	
	/**
	 * 更新数据
	 * @param recipients
	 */
	public void update(Recipients recipients);
	
	/**
	 * 根据ID获取收件人信息
	 * @param id
	 * @return
	 */
	public Recipients getRecipientsById(Long id);
}

package com.bdbox.service;

import java.util.Map;

import com.bdbox.entity.AlipayOrder;
import com.bdbox.entity.AlipayRefund;

public interface AlipayRefundService {

	/**
	 * 保存或更新数据
	 * @param alipayRefund
	 */
	public void saveOrUpdate(AlipayRefund alipayRefund);
	
	/**
	 * 更新数据
	 * @param params
	 */
	public void update(Map<String, String> params);
	
	/**
	 * 根据订单号获取支付宝订单
	 * @param outTradeNo
	 * @return
	 */
	public AlipayOrder getAlipayOrder(String outTradeNo);
	
	/**
	 * 根据交易号获取退款订单
	 * @param tradeNo
	 * @return
	 */
	public AlipayRefund getAlipayRefund(String tradeNo);
}

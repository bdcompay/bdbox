package com.bdbox.service;

import com.bdbox.entity.MobCode;

public interface MobCodeService {

	/**
	 * 更新记录
	 * @param mobCode
	 */
	public void update(MobCode mobCode);
	
	/**
	 * 保存记录
	 * @param mobCode
	 */
	public void save(MobCode mobCode);
	
	/**
	 * 查询单条记录
	 * @param phone
	 */
	public MobCode findCodeByPhone(String phone);
}

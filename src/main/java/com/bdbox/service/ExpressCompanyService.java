package com.bdbox.service;

import java.util.List;

import com.bdbox.entity.ExpressCompany;

public interface ExpressCompanyService {

	public List<ExpressCompany> getAll();
}

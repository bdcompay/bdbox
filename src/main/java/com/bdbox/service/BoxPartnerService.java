package com.bdbox.service;

import java.util.List;

import com.bdbox.api.dto.PartnerDto;
import com.bdbox.entity.BoxPartner;

public interface BoxPartnerService {
	public void saveBoxPartner(BoxPartner boxPartner);

	public List<BoxPartner> getBoxPartners(Long boxId);

	public BoxPartner getBoxPartner(Long boxId, String partnerBoxSerialNumber);

	public boolean updateBoxPartner(List<PartnerDto> partnerDtos,
			String cardNumber, String boxSerialsNumber);

	public List<PartnerDto> getPartnerDtos(String cardNumber,
			String boxSerialsNumber);

	/**
	 * 修改
	 * 
	 * @param familyDtos
	 * @param cardNumber
	 * @param boxSerialsNumber
	 * @return
	 */
	public boolean updatePartnerSingle(PartnerDto partnerDto);
	
	/**
	 * 添加
	 * @param boxPartner
	 */
	public void savePartner(BoxPartner boxPartner);
	
	/**
	 * 删除
	 * @param id
	 */
	public void deletePartner(Long id);

}

package com.bdbox.service;

import java.util.List;

import com.bdbox.api.dto.UserDto;
import com.bdbox.constant.UserStatusType;
import com.bdbox.constant.UserType;
import com.bdbox.entity.User;
import com.bdbox.web.dto.ManageUserDto;
import com.bdsdk.json.ObjectResult;

public interface UserService {

	//public User saveUser(User user);
	public User saveUser(User user,String ip);

	public Boolean updateUser(User user);
	public void updateManageUser(User user);
	
	/**
	 * 获取用户ManageUserDto
	 * @param id
	 * @return
	 */
	public ManageUserDto getManageUserDto(Long id);
	
	/**
	 * 获取用户Dto
	 * @param id
	 * @return
	 */
	public UserDto getUserDto(Long id);
	
	/**
	 * 获取用户
	 * @param id
	 * @return
	 */
	public User getUser(Long id);
	
	/**
	 * 用户登录
	 * 
	 * @param username
	 * @param password
	 * @param cardNumber
	 * @return
	 */
	public UserDto login(String username, String password);

	/**
	 * 查询用户
	 * 
	 * @param bsUserRoleType
	 * @param username
	 * @return
	 */
	public User queryUser(UserType userType, String username, String password,
			UserStatusType userStatusType, String mail);
	
	/**
	 * 查询用户
	 */
	public UserDto queryUser(String username);

	/**
	 * 验证系统授权码
	 * 
	 * @param systemCode
	 * @return
	 */
	public Boolean checkSystemCode(String systemCode);

	/**
	 * 生成手机注册验证码
	 * 
	 * @param mob
	 * @return
	 */
	public ObjectResult createRegisterKey(String mob);

	/**
	 * 生成手机修改密码验证码
	 * 
	 * @param mob
	 * @return
	 */
	public ObjectResult createChangePasswordKey(String mob);

	/**
	 * 生成忘记密码验证码
	 * 
	 * @param mob
	 * @return
	 */
	public ObjectResult createForgetPasswordKey(String mob);

	/**
	 * 生成修改用户资料验证码
	 * 
	 * @param mob
	 * @return
	 */
	public ObjectResult createChangeUserInfoKey(String mob);
	
	/**
	 * 生成用户预约验证码
	 * @param mob
	 * @return
	 */
	public ObjectResult createApplyCode(String mob);

	/**
	 * 验证手机验证码
	 * 
	 * @param mob
	 * @param mobKey
	 * @return 返回OnlineKey，如失败则返回null
	 */
	public Boolean checkMobKey(String mob, String mobKey);

	/**
	 * 注册用户
	 * 
	 * @param username
	 * @param password
	 * @param name
	 * @param idNumber
	 * @param sosContactName
	 * @param sosContactPhone
	 * @param mobkey
	 * @return
	 */
	public UserDto registerUser(String username, String password,
			String mobkey);

	/**
	 * 更改密码
	 * 
	 * @param username
	 * @param password
	 * @param onlineKey
	 * @return
	 */
	public UserDto changePassword(String username, String password,
			String mobkey,String oldPassword);

	/**
	 * 更改资料
	 * 
	 * @param username
	 * @param newUsername
	 * @param mobkey
	 * @return
	 */
	public UserDto changeUserInfo(String newUsername,String username,String mobkey);

	/**
	 * 提交用户建议
	 * 
	 * @param username
	 * @param content
	 * @return
	 */
	public Boolean postSuggestion(String boxSerialNumber, String content);
	
	/**
	 * 查询
	 * @param userType
	 * @param username
	 * @param password
	 * @param userStatusType
	 * @param mobKey
	 * @param mail
	 * @param order
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<ManageUserDto> query(UserType userType, String username,
			String password, UserStatusType userStatusType, String mobKey,
			String mail, String order, int page, int pageSize);
	
	/**
	 * 数量
	 * @param userType	用户类型
	 * @param username	手机号
	 * @param password	密码
	 * @param userStatusType	用户状态
	 * @param mobKey	验证码
	 * @param mail	邮箱
	 * @return
	 */
	public Integer queryCount(UserType userType, String username,
			String password, UserStatusType userStatusType, String mobKey,
			String mail);

	/**
	 * 删除
	 * @param id
	 */
	public void deleteUser(Long id);
	
	/**
	 * 获取所有用户
	 * @return
	 */
	public List<UserDto> listAll();
	
	/**
	 * 邮箱发送验证码
	 * @param toMail
	 */
	public ObjectResult createMailKey(long uid,String toAddress);
	
	/**
	 * 提交用户建议v1.70
	 * @param boxSerialNumber
	 * @param content
	 * @param username
	 * @param phone
	 * @param qq
	 * @return
	 */
	public Boolean postSuggestion(String boxSerialNumber, String content,String username,String phone,String qq);
	
	/**
	 *获取用户 
	 * @param userPowerKey
	 * @return
	 */
	public User getUserByPowerKey(String userPowerKey);
	
	 
	public User getUserByusername(String user);
	
	//获取实名制用户
	public List<User> getRealUser(int page, int pageSize);
	
	public Integer getRealUserCount();
	
	public boolean updateRealName(User user);
	
	public UserDto getUsersession(Long id);
	
	public String getUserSeclectOptions(Long selectUserId, int page, int pageSize);
	
	public List<User> getRefeesUser(String usernomber,String refereespeoplename, int page, int pageSize);
	
	public Integer getRefeesUserCount(String usernomber,String refereespeoplename);
	
	public User getUserByuser(String user);
	
	/**
	 * 获取所有用户
	 * 
	 * @return
	 */
	public List<UserDto> getAllUser();


	/**
	 * 获取用户信息，好友、家人
	 * @param key
	 * 		系统调用码，简单验证
	 * @param account
	 * 		用户账号
	 * @param pwd
	 * 		用户密码
	 * @return
	 */
	public ObjectResult getUserInfoByAccount(String key,String account,String pwd);
	
	/**
	 * 获取驴讯通绑定的盒子帐号信息
	 * @return
	 */
	public ObjectResult getUserForLxtAccount(String lxtAccount);

	/**
	 * 绑定
	 * @param account
	 * @param password
	 * @param lxtAccount
	 */
	public ObjectResult dobindLxtAccount(String account, String password, String lxtAccount);
	
	/**
	 * 解绑
	 * @param lxtAccount
	 */
	public ObjectResult dounbindLxtAccount(String lxtAccount);

}

package com.bdbox.service;

import java.util.List;

import com.bdbox.entity.EntUserPushMessage;
import com.bdbox.web.dto.EntUserPushMessageDto;

/**
 * 企业用户推送通讯记录的业务处理
 * @author dezhi.zhang
 * @createTime 2017/01/19
 */
public interface EntUserPushMessageService {
	public void save(EntUserPushMessage entUserPushMessage);
	public void update(EntUserPushMessage entUserPushMessage);
	public EntUserPushMessage get(Long id);
	
	/**
	 * 获取所有发送失败的记录
	 * @return
	 */
	public List<EntUserPushMessage> getUnsend();
	
	public List<EntUserPushMessageDto> getDtos(String entUser,String startTime,String endTime,Integer count,Boolean isSuccess,int page,int pageSize);
	
	/**
	 * 获取推送位置信息的数量
	 * @return
	 */
	public int amount(String entUser,String startTime,String endTime,Integer count,Boolean isSuccess);
}

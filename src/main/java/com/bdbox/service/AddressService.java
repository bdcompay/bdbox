package com.bdbox.service;

import java.util.List;

import com.bdbox.entity.Address;

public interface AddressService {
	/**
	 * 保存收货地址
	 * 
	 * @param address
	 * 			收货地址对象
	 * @return
	 */
	public Address save(Long userId,String name,String mob,String email,String province,String city,String county,
			String detailedAddress,String postCode, String areaCode);
	
	/**
	 * 根据id删除收货地址
	 * 
	 * @param id
	 * @return
	 */
	public boolean deleteAddress(Long id);
	
	/**
	 * 根据id获取收货地址
	 * 
	 * @param id
	 * @return
	 */
	public Address get(Long id);
	
	/**
	 * 更新收货地址
	 * 
	 * @param address
	 * @return
	 */
	public boolean update(Long id,Long userId,String name,String mob,String email,String province,String city,String county,
			String detailedAddress,String postCode, String areaCode);
	
	/**
	 * 根据用户id获得收货地址
	 * 
	 * @param userId
	 * 			用户id
	 * @return
	 */
	public List<Address> getAddressByUserid(Long userId);
	
	/**
	 * 根据用户id获得收货地址
	 * @param userId
	 * @return
	 */
	public Address getAddress(Long userId);
}

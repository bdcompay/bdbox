package com.bdbox.service;

import java.util.List;

import com.bdbox.api.dto.DialrecordDto;
import com.bdbox.dao.DialrecordDao;
import com.bdbox.entity.Dialrecord;

public interface DialrecordService {
	public Dialrecord getDialrecord(Long id);
	
	public Dialrecord saveDialrecord(Dialrecord dialrecord);
	
	public void saveOrUpdateDialrecord(Dialrecord dialrecord);
	
	public void updateDialrecord(Dialrecord dialrecord);
	
	public void deleteDialrecord(Dialrecord dialrecord);
	/**
	 * 查询SOS待拨号号码
	 * @param dialNum
	 * @param hasSend
	 * @param boxSerialNumber
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<Dialrecord> queryDialrecord(String dialNum,Boolean hasSend,String boxSerialNumber,Integer page,Integer pageSize);
	/**
	 *  查询SOS待拨号号码
	 * @param dialNum
	 * @param hasSend
	 * @param boxSerialNumber
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<DialrecordDto> queryDialrecordDto(String dialNum,Boolean hasSend,String boxSerialNumber,Integer page,Integer pageSize);

}

package com.bdbox.service;

import java.util.List;

import com.bdbox.entity.Address;
import com.bdbox.entity.UserPhotoUrl;

public interface UserPhotoUrlService {

	public UserPhotoUrl save(UserPhotoUrl userPhotoUrl);
	
	public UserPhotoUrl getUserPhotoUrl(Long id );
	
	public boolean updates(UserPhotoUrl userPhotoUrl );
	
	public  UserPhotoUrl  getRealNameUrl(long userid);

}

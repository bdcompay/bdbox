package com.bdbox.service;

import com.bdbox.constant.ExpressStatus;
import com.bdbox.entity.Express;
import com.bdbox.web.dto.ExpressDto;
import java.util.Calendar;
import java.util.List;

public abstract interface ExpressService
{
  public abstract void saveExpress(Express paramExpress);

  public abstract void updateExpress(Express paramExpress);

  public abstract void deleteExpress(Long paramLong);

  public abstract Express queryExpress(Long paramLong);

  public abstract List<ExpressDto> listExpress(String paramString1, String paramString2, Calendar paramCalendar1, Calendar paramCalendar2, ExpressStatus paramExpressStatus, Integer paramInteger1, Integer paramInteger2, String paramString3);

  public abstract int listExpressCount(String paramString1, String paramString2, Calendar paramCalendar1, Calendar paramCalendar2, ExpressStatus paramExpressStatus);

  public abstract ExpressDto queryExpressDto(Long paramLong);
  
  public abstract ExpressDto queryExpressDtoById(Long id);

  public abstract List<Express> listAllSendExpress();
  
  public abstract Express queryExpressEnty(Long id); 
  
  public abstract void doExpressApi(Express express); 
  
  public abstract List<Express> getAllSelExpress();
  
  public abstract void doShunfeng(Express express);
  
  public abstract Express queryExpressByOrderId(Long orderId);
  
  public abstract void saveOrUpdate(Express paramExpress);
  
}

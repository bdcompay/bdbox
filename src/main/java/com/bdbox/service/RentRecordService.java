package com.bdbox.service;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.RentRecord;
import com.bdbox.web.dto.RentRecordDto;

public interface RentRecordService {

	/**
	 * 分页查询所有租用记录
	 * @param user
	 * @param startTime
	 * @param endTime
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<RentRecordDto> query(String user, Calendar startTime, 
			Calendar endTime, int page, int pageSize);
	
	/**
	 * 查询数量
	 * @param user
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public int queryAmount(String user, Calendar startTime, Calendar endTime);
	
	/**
	 * 添加记录
	 * @param rentRecord
	 * @return
	 */
	public RentRecord save(RentRecord rentRecord);
	
	/**
	 * 更新记录
	 * @param eentRecord
	 * @return
	 */
	public void update(RentRecord rentRecord);
	
	/**
	 * 删除记录
	 * @param rentRecord
	 * @return
	 */
	public void delete(Long id);
	
	/**
	 * 根据用户获取租用记录
	 * @param user
	 * @return
	 */
	public List<RentRecordDto> getUserRentRecord(String user);
}

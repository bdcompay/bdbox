package com.bdbox.service;

import java.util.Calendar;
import java.util.List;

import com.bdbox.api.dto.MessageRecordDto;
import com.bdbox.constant.MailType;

public interface MessageRecordService {

	/**
	 * 分页获取短信通知数据
	 * @param mob
	 * @param contet
	 * @param sTime
	 * @param eTime
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<MessageRecordDto> getMsgAll(String mob, String content, 
			Calendar startTime, Calendar endTime, int page, int pageSize);
	
	/**
	 * 统计分页的数量
	 * @param mob
	 * @param contet
	 * @param sTime
	 * @param eTime
	 * @return
	 */
	public int count(String mob, String content, 
			Calendar startTime, Calendar endTime);
	
	/**
	 * 保存短信记录
	 * @param mob
	 * @param msgContent
	 * @param type
	 * @param createdTime
	 */
	public void save(String mob, String msgContent, MailType type);
}

package com.bdbox.service;

import java.util.List;

import com.bdbox.entity.SmallType;

public abstract interface SmallTypeService {

	public String gettypeSeclectOptions(String selecttype);
	
	public SmallType saveFaqtype(String addtype, String Englishtypename,String chaintypename);
	
	public boolean update(String addtype, String Englishtypename,String chaintypename);
	
	public SmallType gettype(String addtype,String Englishtypename);

	public void updates(SmallType enty);
	
	public List<SmallType> getalltype(String lefttype);




}

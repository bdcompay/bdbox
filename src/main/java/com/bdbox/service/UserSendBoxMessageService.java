package com.bdbox.service;

import java.util.Calendar;
import java.util.List; 

import com.bdbox.entity.UserSendBoxMessage;
import com.bdbox.web.dto.SdkUserSendDto;

public interface UserSendBoxMessageService {

	public List<SdkUserSendDto> sdkUserSend(Long serialNumber,Calendar startTime,Calendar endTime,String order, Integer page,Integer pageSize);

	public Integer count(Long serialNumber,Calendar startTime,Calendar endTime);
	
	public abstract void deleteSdkUserSend(long id);

	public void saveUserSendboxMessage(UserSendBoxMessage userSendboxMessage);


}

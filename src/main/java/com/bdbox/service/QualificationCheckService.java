package com.bdbox.service;

import java.util.List;

import com.bdbox.entity.QualificationCheck;
import com.bdbox.web.dto.QualificationCheckDto;
import com.bdsdk.json.ObjectResult;

public interface QualificationCheckService {

	/**
	 * 保存审核资料
	 * @param qualificationCheck
	 */
	public ObjectResult save(String businessLicense, String taxReg, 
			String orgCode, String generalTax, String invoiceDatum, Long invoice, Long id);
	
	/**
	 * 修改审核资料
	 * @param qualificationCheck
	 */
	public void update(QualificationCheck qualificationCheck);
	
	/**
	 * 删除审核资料
	 * @param id
	 */
	public void delete(Long id);
	
	/**
	 * 根据id获取审核资料(dto)
	 * @param id
	 * @return
	 */
	public QualificationCheckDto getDto(Long id);
	
	/**
	 * 根据id获取审核资料
	 * @param id
	 * @return
	 */
	public QualificationCheck get(Long id);
	
	/**
	 * 分页获取审核资料
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<QualificationCheckDto> query(int page, int pageSize);
	
	/**
	 * 获取审核资料数量
	 * @return
	 */
	public int queryAcount();
	
	/**
	 * 根据发票ID获取审核信息
	 * @param invoiceId
	 * @return
	 */
	public QualificationCheckDto getQualificationCheck(Long invoiceId);
}

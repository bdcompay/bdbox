package com.bdbox.service;

import java.util.List; 
import java.util.Map;

import com.bdbox.api.dto.FAQDto;
import com.bdbox.entity.FAQ;

public abstract interface FAQService {
	
	public List<FAQ> getlistFAQtable(String questiontype,int page, int pageSize);

	public Integer getlistFAQtable(String questiontype);
	
	public FAQ saveFaq(String questionname, String questionvlaue,String questiontype,String addsmalltype,String num);

	public FAQ getfaq(long id);
	
	public FAQDto castFrom(FAQ faq);
	
	public boolean update(Long id, String updatequestionname, String updatequestionvlaue,String updatequestiontype,String updatesmalltype);
	
	public boolean deleteFAQ(Long id);
	
	public boolean updates(String updatetype, String updatetype_twl,String updatetypename);
	
	public Map getFAQquestionlist(String lefttype,String keyword, String toptype,int page,int pageSize);





}

package com.bdbox.service;

import com.bdbox.entity.Box;
import com.bdbox.entity.FamilyStatus;

public interface FamilyStatusService {

	public FamilyStatus getFamilyStatus(Long boxId, String familyMob);

	public void updateFamilyStatus(FamilyStatus familyStatus);

	public FamilyStatus saveFamilyStatus(FamilyStatus familyStatus);

	public void updateFamilyStatus(Box box, String familyMob);
}

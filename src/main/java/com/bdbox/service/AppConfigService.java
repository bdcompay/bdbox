package com.bdbox.service;

import com.bdbox.api.dto.AppConfigDto;
import com.bdbox.constant.AppType;

public interface AppConfigService {

	public AppConfigDto getAppConfig(AppType appType);

}

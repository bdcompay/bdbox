package com.bdbox.service;

import java.util.Map;

import com.bdbox.entity.UnionpayOrder;

public interface UnionpayOrderService {

	public void save(Map<String, String> rspData);
	
	public void update(Map<String, String> rspData);
	
	public void delete(UnionpayOrder unionpayOrder);
	
	public UnionpayOrder getUnionpayOrder(String orderId);
}

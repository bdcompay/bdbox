package com.bdbox.service;

 import java.util.List;

import com.bdbox.entity.Activity;
import com.bdbox.web.dto.ActivityDto;

public interface ActivityService {

	public List<ActivityDto> queryActivity(Integer page,Integer pageSize);
	
	public int counts();
	
	public ActivityDto getActivity(Long id);
	
	public Activity getActivitys(Long id);
	
	public boolean updateActivity(Activity activity);

	public List<ActivityDto> findActivity();

}

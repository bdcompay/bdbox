package com.bdbox.service;

import java.util.Map;

import com.bdbox.entity.UnionpayRefund;
import com.bdsdk.json.ObjectResult;

public interface UnionpayRefundService {

	public ObjectResult save(Map<String, String> rspData, String orderId);
	
	public void update(Map<String, String> rspData);
	
	public void delete(UnionpayRefund unionpayRefund);
	
	public UnionpayRefund getUnionpayRefund(String orderId);
}

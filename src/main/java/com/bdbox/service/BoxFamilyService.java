package com.bdbox.service;

import java.util.List;

import com.bdbox.api.dto.FamilyDto;
import com.bdbox.entity.BoxFamily;

public interface BoxFamilyService {
	public void saveBoxFamily(BoxFamily boxFamily);

	public List<BoxFamily> getBoxFamilys(Long boxId);

	public BoxFamily getBoxFamily(Long boxId, String familyMob);

	/**
	 * 更新
	 * @param familyDtos
	 * @param cardNumber
	 * @param boxSerialsNumber
	 * @return
	 */
	public boolean updateBoxFamily(List<FamilyDto> familyDtos,
			String cardNumber, String boxSerialsNumber);
	
	/**
	 * 修改
	 * @param familyDtos
	 * @param cardNumber
	 * @param boxSerialsNumber
	 * @return
	 */
	public boolean updateFamilySingle(FamilyDto familyDto,
			String cardNumber, String boxSerialsNumber);

	public List<FamilyDto> getFamilyDtos(String cardNumber,
			String boxSerialsNumber);
	
	/**
	 * 添加
	 * @param boxFamily
	 */
	public void saveFamily(BoxFamily boxFamily);
	
	/**
	 * 删除
	 * @param id
	 */
	public void deleteFamily(Long id);
}

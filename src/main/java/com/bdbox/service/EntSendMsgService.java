package com.bdbox.service;

import java.util.Calendar;
import java.util.List;

import com.bdbox.entity.EntSendMsg;
import com.bdbox.web.dto.EntSendMsgDto;

/**
 * 企业用户发送消息相关业务接口
 * 
 * @ClassName: EntSendMsgService 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-3-22 下午3:41:21 
 * @version V5.0 
 */
public interface EntSendMsgService {
	
	/**
	 * 通过对象保存企业用户发送的消息
	 * @param entSendMsg
	 * 		企业用户发送消息对象
	 * @return
	 */
	public EntSendMsgDto save(EntSendMsg entSendMsg);
	
	/**
	 * 通过参数保存企业用户发送的消息
	 * @param toBoxSerialNumber
	 * 		接收盒子序列号
	 * @param content
	 * 		消息内容
	 * @param entUserId
	 * 		所属企业用户ID
	 * @return
	 */
	public EntSendMsgDto save(String toBoxSerialNumber,String content,Long entUserId);
	
	/**
	 * 通过id删除企业用户发送的消息
	 * @param id
	 * @return
	 */
	public boolean deleteEntSendMsg(long id);
	
	/**
	 * 通过对象更新企业用户发送的消息
	 * @param entSendMsg
	 * 		企业用户发送消息对象
	 * @return
	 */
	public boolean updateEntSendMsg(EntSendMsg entSendMsg);
	
	/**
	 * 通过id查询企业用户发送消息
	 * @param id
	 * @return	
	 */
	public EntSendMsgDto get(long id);
	
	/**
	 * 多条件查询企业用户发送的消息
	 * @param toBoxSerialNumber
	 * 		接收盒子序列号
	 * @param startTime
	 * 		开始查询时间
	 * @param endTime
	 * 		结束查询时间
	 * @param entUserName
	 * 		企业用户名称(支持模糊查询)
	 * @param page
	 * 		起始查询位置
	 * @param pageSize
	 * 		查询记录数量
	 * @return 企业发送消息DTO集合
	 */
	public List<EntSendMsgDto> listEntSendMsg(String toBoxSerialNumber,Calendar startTime,Calendar endTime,String entUserName,
			int page,int pageSize);
	
	/**
	 * 多条件统计企业用户发送的消息数量
	 * @param toBoxSerialNumber
	 * @param startTime
	 * @param endTime
	 * @param entUserName
	 * @return  统计数量
	 */
	public int count(String toBoxSerialNumber,Calendar startTime,Calendar endTime,String entUserName);
}

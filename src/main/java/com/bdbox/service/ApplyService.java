package com.bdbox.service;

import java.util.Calendar;
import java.util.List;

import com.bdbox.api.dto.ApplyDto;
import com.bdbox.entity.Apply;


public interface ApplyService {

	/**
	 * 数量
	 * @param code	户外代码
	 * @param phone	手机号码
	 * @param createTime	申请时间
	 * @param sex	性别
	 * @param join	试用
	 * @param page	页号
	 * @param pageSize	页大小
	 * @param apply	排序
	 * @return	List
	 */
	public List<ApplyDto> listApply(String code,String phone,Calendar createTime,String sex,Boolean join,
			Integer page,Integer pageSize,String apply);
	
	/**
	 * 数量
	 * @param code	户外代码
	 * @param phone	手机号码
	 * @param createTime	申请时间
	 * @param sex	性别
	 * @param join	试用
	 * @return
	 */
	public int applyCount(String code,String phone,Calendar createTime,String sex,Boolean join);
	
	/**
	 * 添加
	 * @param apply
	 */
	public void saveApply(Apply apply);
	
	/**
	 * 修改
	 * @param apply
	 */
	public void updateApply(Apply apply);
	
	/**
	 * 删除
	 * @param apply
	 */
	public void deleteApply(Long id);
	/**
	 * 查询
	 * @param id
	 * @return
	 */
	public ApplyDto queryApplyDto(Long id);
	
	/**
	 * 查询
	 * @param id
	 * @return
	 */
	public Apply queryApply(Long id);
	
	/**
	 * 根据手机号码获取订单
	 * @param phone
	 * @return
	 */
	public ApplyDto getApply(String phone);
	/**
	 * 根据用户id查询订单
	 * @param uid
	 * @return
	 */
	public ApplyDto getApplyUid(Long uid);
}

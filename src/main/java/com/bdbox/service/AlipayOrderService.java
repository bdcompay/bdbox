package com.bdbox.service;

import java.util.Map;

import com.bdbox.entity.AlipayOrder;

public interface AlipayOrderService {

	public void saveOrUpdate(Map<String,String> params);
	
	public AlipayOrder getOrderForTradeNo(String tradeNo);
}

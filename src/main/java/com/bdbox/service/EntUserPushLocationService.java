package com.bdbox.service;

import java.util.List;

import com.bdbox.entity.EntUserPushLocation;
import com.bdbox.web.dto.EntUserPushLocationDto;

/**
 * 企业用户推送位置记录的业务处理
 * @author dezhi.zhang
 * @createTime 2017/01/19
 */
public interface EntUserPushLocationService {
	public void save(EntUserPushLocation entUserPushLocation);
	public EntUserPushLocation get(Long id);
	public void update(EntUserPushLocation entUserPushLocation);
	
	/**
	 * 获取所有为发送成功的消息，重发
	 * @return
	 */
	public List<EntUserPushLocation> getUnsend();
	
	/**
	 * 获取推送位置信息的DTO
	 * @return
	 */
	public List<EntUserPushLocationDto> getDtos(String entUser,String startTime,String endTime,Integer count,
			Boolean isSuccess,int page,int pageSize);
	
	/**
	 * 获取推送位置信息的数量
	 * @return
	 */
	public int amount(String entUser,String startTime,String endTime,Integer count,
			Boolean isSuccess);
}

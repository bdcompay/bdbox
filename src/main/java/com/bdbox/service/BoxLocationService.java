package com.bdbox.service;

import java.util.Calendar;
import java.util.List;

import net.sf.json.JSONObject;

import com.bdbox.api.dto.BoxLocationDto;
import com.bdbox.api.dto.MyCardLocStatusPOJO;
import com.bdbox.entity.BoxLocation;
import com.bdsdk.json.ObjectResult;

public interface BoxLocationService {
	public void saveBoxLocation(BoxLocation boxLocation);
	
	/**
	 * 
	 * 获取盒子位置记录
	 * @param boxId	盒子id
	 * @param cardNumber 北斗卡号
	 * @param boxName	盒子名称
	 * @param userName	用户名称
	 * @param boxSerialNumber	盒子ID
	 * @param createdTime	创建时间
	 * @param order	排序
	 * @param page	页号
	 * @param pageSize	页大小
	 * @return
	 */
	public List<BoxLocationDto> listBoxLocations(Long boxId,String cardNumber,String boxName,String userName,String boxSerialNumber,Calendar createdTime,String order,Integer page,Integer pageSize);
	
	/**
	 * 
	 * 获取盒子位置记录数量
	 * @param boxId	盒子id
	 * @param cardNumber 北斗卡号
	 * @param boxName	盒子名称
	 * @param userName	用户名称
	 * @param boxSerialNumber	盒子ID
	 * @param createdTime	创建时间
	 * @param order	排序
	 * @param page	页号
	 * @param pageSize	页大小
	 * @return
	 */
	public Integer listBoxLocationsCount(Long boxId,String  cardNumber,String boxName,String userName,String boxSerialNumber,Calendar createdTime);
	
	/**
	 * 删除
	 * @param id
	 */
	public void deleteBoxLocation(Long id);
	
	/**
	 * 查询
	 * @param id
	 * @return
	 */
	public BoxLocationDto getBoxLocationDto(Long id);
	
	/**
	 * 获取盒子位置记录（可通过时间区间）
	 * 
	 * @param boxId	盒子id
	 * @param cardNumber 北斗卡号
	 * @param boxName	盒子名称
	 * @param userName	用户名称
	 * @param boxSerialNumber	盒子ID
	 * @param startTime	开始查询时间
	 * @param endTime	结束查询时间
	 * @param order	排序
	 * @param page	页号
	 * @param pageSize	页大小
	 * @return
	 */
	public List<BoxLocationDto> queryBoxLocations(Long boxId,String cardNumber,String boxName,String userName,String boxSerialNumber,Calendar startTime,Calendar endTime,Long userId,String order,Integer page,Integer pageSize);
	/**
	 * 条件统计盒子位置记录数量（可通过时间区间）
	 * 
	 * @param boxId	盒子id
	 * @param cardNumber 北斗卡号
	 * @param boxName	盒子名称
	 * @param userName	用户名称
	 * @param boxSerialNumber	盒子ID
	 * @param startTime	开始查询时间
	 * @param endTime	结束查询时间
	 * @return
	 */
	public int count(Long boxId,String cardNumber,String boxName,String userName,String boxSerialNumber,Calendar startTime,Calendar endTime,Long userId);

	/**
	 * 查询轨迹
	 * @param boxId
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<MyCardLocStatusPOJO> getBoxLocationDto(Long boxId, Calendar startTime, Calendar endTime,Long userId);
	
	/**
	 * 查询轨迹
	 * @param id
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public List<MyCardLocStatusPOJO> queryBoxLocationDto(String id, Calendar startTime, Calendar endTime);
	
	/**
	 * 获取企业用户所有下属盒子的定位信息
	 * @param json
	 * @return
	 */
	public ObjectResult getAllLocation(JSONObject json);
	
	/**
	 * 获取定位信息(调度系统)
	 * @param id
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public ObjectResult getEntUserLocation(String id, Calendar startTime, Calendar endTime, Integer page, Integer pageSize);
	
	/**
	 * 分页获取定位信息
	 * @param json
	 * @param page
	 * @return
	 */
	public ObjectResult getEntUserLocations(Calendar startTime, Calendar endTime, JSONObject json);
	
	/**
	 * 列出企业用户的盒子位置
	 * @param boxId
	 * @param cardNumber
	 * @param order
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public List<MyCardLocStatusPOJO> listUserBoxLocations(Long boxId,String cardNumber,String order,Integer page,Integer pageSize);
}

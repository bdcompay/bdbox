package com.bdbox.task;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bdbox.entity.Express;
import com.bdbox.service.ExpressService;
import com.bdbox.service.OrderService;

/**
 * 物流信息任务调度
 */
@Component
public class ExpressTask implements ApplicationContextAware{

	@Autowired private ExpressService expressService;
	
	@Autowired private OrderService orderService;
	
	private ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		// TODO Auto-generated method stub
		this.applicationContext=arg0;
	}
	
	//上传执行时间
	private static String lastDate = "";
	
	/**
	 * 
	 * 每三十分钟执行一次
	 * 
	 * 抓取网页动态信息，判断当前订单物流状态，即时更新数据信息
	 * 
	 * 思想：
	 * 1、使用spring任务调度，每小时刷新订单物流状态
	 * 2、遍历所有物流信息，排除已收货物流，实时更新物流状态
	 * 3、访问源为网页请求数据地址，返回数据类型为json格式
	 * 4、将json数据解析，将各个字段的数据进行判断（判断状态）
	 * 5、最后更新数据库
	 */
    //@Scheduled(cron = "0 0/2 * * * ?")
	@Scheduled(cron = "0 0/30 * * * ?")
	public void execute(){
		
		//当前时间
		String date = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
		//判断当前执行时间是否与上一次执行时间相同；避免重复执行
		if(!lastDate.equals(date)){
			lastDate = date;//刷新上次执行时间
			System.out.println("当前时间："+date+"执行了一次任务调度（物流信息状态更新）");
			//获取所有物流信息
			List<Express> list = expressService.listAllSendExpress();
			List<Express> exps = expressService.getAllSelExpress();
			//判断不为空
			if(list.size()>0){
				//遍历
				for (Express express : list) {
					expressService.doExpressApi(express); 
				}
			}else{
				System.out.println("无物流信息可更新！");
			}
			if(exps.size()>0){
				for (Express express : exps) {
					expressService.doShunfeng(express);
				}
			}else{
				System.out.println("无物流信息可更新！");
			}
		}
	}
	
	/**
	  CRON表达式    含义 
		"0 0 12 * * ?"    每天中午十二点触发 
		"0 15 10 ? * *"    每天早上10：15触发 
		"0 15 10 * * ?"    每天早上10：15触发 
		"0 15 10 * * ? *"    每天早上10：15触发 
		"0 15 10 * * ? 2005"    2005年的每天早上10：15触发 
		"0 * 14 * * ?"    每天从下午2点开始到2点59分每分钟一次触发 
		"0 0/5 14 * * ?"    每天从下午2点开始到2：55分结束每5分钟一次触发 
		"0 0/5 14,18 * * ?"    每天的下午2点至2：55和6点至6点55分两个时间段内每5分钟一次触发 
		"0 0-5 14 * * ?"    每天14:00至14:05每分钟一次触发 
		"0 10,44 14 ? 3 WED"    三月的每周三的14：10和14：44触发 
		"0 15 10 ? * MON-FRI"    每个周一、周二、周三、周四、周五的10：15触发 
	 */
	
	
}

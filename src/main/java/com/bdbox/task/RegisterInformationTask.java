package com.bdbox.task;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled; 
import org.springframework.stereotype.Component;

import com.bdbox.constant.BdcoodStatus;
import com.bdbox.constant.MailType;
import com.bdbox.entity.BDCood;
import com.bdbox.entity.Notification;
import com.bdbox.entity.Order;
import com.bdbox.entity.Product;
import com.bdbox.entity.RegisterInformation;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.BDCoodService;
import com.bdbox.service.MessageRecordService;
import com.bdbox.service.NotificationService;
import com.bdbox.service.OrderService;
import com.bdbox.service.ProductService;
import com.bdbox.service.RegisterInformationService;
import com.bdsdk.util.CommonMethod;
@Component
public class RegisterInformationTask {

	@Autowired
	private OrderService orderService; 
	@Autowired
	private BDCoodService bdcoodService;
	@Autowired
	private ProductService productService;
	@Autowired
	private RegisterInformationService registerInformationService;
	@Autowired  
	private MailNotification mailNotification; 
	@Autowired 
	private NotificationService notificationService;
	@Autowired
	private MessageRecordService MessageRecordService;
	private static int flagbuy=0;//标记发邮件的次数，库存不足时只通知一次
	private static int flagrent=0;//标记发邮件的次数，库存不足时只通知一次
	private static int flagactivity=0;//活动到期截止时间到期时提醒管理员延长活动的截止时间，只通知一次
	private static int flaginfo=0;//客户的租赁到期时提醒客户，只提醒一次
	private static Calendar sendTime=Calendar.getInstance(); //记录上次给客户发送到期通知的时间




	
	 //@Scheduled(cron = "0/5 * * * * ?")
	@Scheduled(cron = "0 0/20 * * * ?")
	public void queryRegisterInformation(){
		try{
		SimpleDateFormat m_simpledateformat = new SimpleDateFormat("yyyy-MM-dd");  
		//查询信息登记表，待库存有货，给登记的用户发送短信通知	
  		List<RegisterInformation> listuser=registerInformationService.getRegisterInformation(); 
		if(listuser.size()>0){
			for(RegisterInformation registerInformation:listuser){
				if(registerInformation!=null){ 
					Product product=null;
					StringBuffer content=new StringBuffer(); 
					String type="RENTDAOHUO";  //通知类型 
					String type_twl="customer"; 
					String mailcentent=null;       //内容
					if(registerInformation.getType().equals("RENT")){
						long id=3;
						product=productService.get(id);
						if(product!=null&&product.getStockNumber()!=0){
							//以下管理员审核通过后短信通知客户 
							Notification notification=notificationService.getnotification(type,type_twl);
 							if(notification!=null){ 
								if(notification.getIsnote().equals(true)){    
										content.append(notification.getNotecenter());
										if(content!=null){
											mailcentent=content.toString(); 
										}
									  //mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
									  //发送邮件通知
									  mailNotification.sendNote(registerInformation.getTelephone(),mailcentent);
									  //保存发送的短信记录
									  MessageRecordService.save(registerInformation.getTelephone(), 
											  mailcentent, MailType.switchStatus(type));
									  //发送邮件后从登记表中删除该记录
									  registerInformationService.delete(registerInformation.getId());
								}
							}
						}
					}else{ 
						long id=1;
						product=productService.get(id);
						type="BUYDAOHUO";
						if(product!=null&&product.getStockNumber()!=0){
							//以下管理员审核通过后短信通知客户 
							Notification notification=notificationService.getnotification(type,type_twl);
 							if(notification!=null){ 
								if(notification.getIsnote().equals(true)){    
										content.append(notification.getNotecenter());
										if(content!=null){
											mailcentent=content.toString(); 
										}
									  //mailNotification.sendMailErrorMessage(tomail, subject, mailcentent);
									  //发送邮件通知
									  mailNotification.sendNote(registerInformation.getTelephone(),mailcentent);
									  //保存发送的短信记录
									  MessageRecordService.save(registerInformation.getTelephone(), 
											  mailcentent, MailType.switchStatus(type));
									  //发送邮件后从登记表中删除该记录
									  registerInformationService.delete(registerInformation.getId());
								}
							}
						}
					}
				}
			}
		}else{
			System.out.println("*************信息登记表不数据可查询*************");
		} 

		//查询库存表，库存不足时，邮件通知管理员,当活动时间过期时,邮件通知管理员
 		long buyid=1;
 		String type="BUYSTOCK";  //通知类型 
		String type_twl="admin"; 
		String mailcentent=null;
		String subject=null;  
		String tomail=null;  
		for(int i=0;i<2;i++){
			if(i==1){
				buyid=3;
				type="RENTSTOCK";
			}
			Product product=productService.get(buyid);
			Notification notification=notificationService.getnotification(type,type_twl); 
			if(product!=null&&notification!=null){
				if(product.getStockNumber()==0){ 
					if(i==1){
						if(flagbuy==0){
							mailcentent=notification.getCenter();
							subject=notification.getSubject();
							tomail=notification.getTomail();
							String [] email=tomail.split(",");
							for(String str:email){
		    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
							}
							flagbuy++; 
						}
 					}else{
 						if(flagrent==0){
							mailcentent=notification.getCenter();
							subject=notification.getSubject();
							tomail=notification.getTomail();
							String [] email=tomail.split(",");
							for(String str:email){
		    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
							}
							flagrent++;
 						}
 					}
				}else{
					flagbuy=0;
					flagrent=0;
				}
				if(product.getBuyEndTime()!=null){
					type="ACTIVTI";
	  				Date date=new Date();			//当前时间
	  				m_simpledateformat.format(date);
	  				Date nowtime=m_simpledateformat.parse(m_simpledateformat.format(date));
	  				Calendar endTime=product.getBuyEndTime();     //租用开始时间 
	  				Date start=m_simpledateformat.parse(m_simpledateformat.format(endTime.getTime())); 
					long day=((start.getTime()-nowtime.getTime())/ (1000*3600*24)); 
	  				if(day<0){
	  					Notification notification2=notificationService.getnotification(type,type_twl); 
	  					if(notification2!=null){
	  						if(flagactivity<2){
		  						mailcentent=notification2.getCenter();
								subject=notification2.getSubject();
								tomail=notification2.getTomail();
								String [] email=tomail.split(",");
	    						for(String str:email){
	    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
	    						}
	    						flagactivity++;
	  						}
 	  					}

	  				}else{
	  					flagactivity=0;
	  				}
				} 
			}

		} 
		
		//查询订单表，主要是查询租赁订单，给租赁到期的用户发送短信提醒
		Calendar nowTime=Calendar.getInstance();//获取当前时间，跟上次发送到期通知的时间做比较
		Date now=m_simpledateformat.parse(m_simpledateformat.format(nowTime.getTime())); 
		Date last =m_simpledateformat.parse(m_simpledateformat.format(sendTime.getTime())); 
		if((now.getTime()-last.getTime())/(1000*3600*24)>2){  //每天两天发一次
		String deal="DEALCLOSE";
		String trandtype="RENT";
		Date date=new Date();			//当前时间
		m_simpledateformat.format(date);
		Date nowtime=m_simpledateformat.parse(m_simpledateformat.format(date)); 
    		List<Order> listorder=orderService.getOrderList(deal,trandtype); 
  		if(!listorder.isEmpty()){
  			for(Order o:listorder){
  				if(o.getBeginTime()!=null){  
  				String countday=o.getNumberdate();      //租用天数  
  				Calendar daoqitime=o.getBeginTime();     //租用开始时间 
  				String daoqi=m_simpledateformat.format(daoqitime.getTime());
  				Date dateExecute = m_simpledateformat.parse(daoqi);
  				Calendar calendar = Calendar.getInstance(); 
  				calendar.setTime(dateExecute); 
  				calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + (Integer.valueOf(countday)-1)); 
  				Date start=m_simpledateformat.parse(m_simpledateformat.format(calendar.getTime())); 
				long day=((start.getTime()-nowtime.getTime())/ (1000*3600*24))+1; 
  				if(day==1){
 					String typed="DAOQITIXIN";  //通知类型 
					String type_twld="customer"; 
 					Notification notification=notificationService.getnotification(typed,type_twld);
					if(notification!=null){
						  mailNotification.sendNote(String.valueOf(o.getPhone()),notification.getNotecenter()+"订单号："+o.getOrderNo()); 
					} 
  				}
  				}
  				
   			}
  			sendTime=Calendar.getInstance();
  		} 
		}
		
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}

package com.bdbox.task;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bdbox.constant.DealStatus;
import com.bdbox.constant.ProductBuyType;
import com.bdbox.entity.Invoice;
import com.bdbox.entity.Notification;
import com.bdbox.entity.Order;
import com.bdbox.entity.Product;
import com.bdbox.notification.MailNotification;
import com.bdbox.service.InvoiceService;
import com.bdbox.service.NotificationService;
import com.bdbox.service.OrderService;
import com.bdbox.service.ProductService;
import com.bdbox.util.LogUtils;
import com.bdbox.web.dto.OrderDto;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;
import com.bdsdk.util.CommonMethod;

/**
 * 微信查询订单
 * @author zouzhiwen
 *
 */
@Component
public class OrderTask  {

	@Autowired
	private OrderService orderService;
	@Autowired
	private ProductService productService;
	@Autowired  
	private MailNotification mailNotification; 
	@Autowired 
	private NotificationService notificationService;
	@Autowired
	private InvoiceService invoiceService;
	/*private ApplicationContext applicationContext;

	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}*/
	private static String flag="";//标记发邮件的次数，每个支付订单只发一次


	@Scheduled(cron = "0 0/5 * * * ?")
	public void execute() {
		try {
			LogUtils.loginfo("执行查询订单一次");
			//查询未付款的订单
			List<OrderDto> orderdtos = this.orderService.listOrder(null, null, null,
					null, null, DealStatus.WAITPAID, Integer.valueOf(0),
					Integer.valueOf(2147483647), null,null,null,null);
			
			for (OrderDto o : orderdtos) {
				Order order = this.orderService.getOrder(Long.valueOf(o
						.getId()));
				orderService.doWxQueryOrder(order.getTrade_no());
			}
			
			//处理未付款超时的订单
			List<OrderDto> list = this.orderService.listOrder(null, null, null,
					null, null, DealStatus.WAITPAID, Integer.valueOf(0),
					Integer.valueOf(2147483647), null,null,null,null);

			for (OrderDto o : list) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
				long d = sdf.parse(o.getCreatedTime()).getTime();
				long now = new Date().getTime();
				Order order = this.orderService.getOrder(Long.valueOf(o.getId()));
				Product product = productService.get(order.getProductId());
				//间隔时间（两小时）
				long intervalTime = 1000*60*60*2;
				if (now - d >= intervalTime) { 
					if(product.getProductBuyType().equals(ProductBuyType.SHOPPINGRUSH)){
						//抢购类型，超过两个小时设置为超时订单
						order.setDealStatus(DealStatus.CANCELORDER);
						
						product.setSaleNumber(product.getSaleNumber()-order.getCount());
						product.setStockNumber(product.getStockNumber()+order.getCount());
						this.orderService.updateOrder(order);
						productService.updateProduct(product);
					}else {
						//超过24小时
						long intervalTime24 = 1000*60*60*24;
						if(now - d >= intervalTime24){
							//抢购类型，超过两个小时设置为超时订单
							order.setDealStatus(DealStatus.CANCELORDER);
							
							product.setSaleNumber(product.getSaleNumber()-order.getCount());
							product.setStockNumber(product.getStockNumber()+order.getCount());
							this.orderService.updateOrder(order);
							productService.updateProduct(product);
						}else{
							//普通购买
							ObjectResult objectResult = null;
							if(order.getValidBeginTime()==null){
								objectResult = orderService.updateOrderTradeNo(order.getId());
							}else{
								long date = order.getValidBeginTime().getTimeInMillis();
								long nowtime = new Date().getTime();
								//超过两小时
								if (nowtime - date >= intervalTime) {
									objectResult = orderService.updateOrderTradeNo(order.getId());
								}
							}
							if(objectResult==null) continue ;
							if(objectResult.getStatus().equals(ResultStatus.OK)){
								LogUtils.loginfo("订单号为："+order.getOrderNo()+" 的订单微信交易号更新成功！");
							}else{
								LogUtils.loginfo("订单号为："+order.getOrderNo()+" 的订单微信交易号更新失败，失败原因"+objectResult.getMessage());
							}
						}
						
					}
					
					
				}
				
				//以下是在规定时间内未付款的订单将会邮件通知管理员
				long purchase = 1000*60*60;//抢购超过1小时未付款，则邮件通知管理员
				long purchasepu = 1000*60*60*10;//普通购买超过10小时未付款，则邮件通知管理员
				long nowTime = new Date().getTime(); //当前时间
				long creattime = sdf.parse(o.getCreatedTime()).getTime();  //订单的创建时间 
				StringBuffer content=new StringBuffer(); 
				String type="ONTHOUR";   //通知类型
				String type_twl="admin";
				String invoices="";            //是否开发票
				String tomail=null;            //收信箱
				String subject=null;           //主题
				String mailcentent=null;       //内容
				Notification notification=null;
				if(product.getProductBuyType().equals(ProductBuyType.SHOPPINGRUSH)){ 
					if(order.getOrderNo().equals(flag)){  
						flag=order.getOrderNo();
					notification=notificationService.getnotification(type,type_twl);  
					if (nowTime - creattime >= purchase){
						if(notification!=null){ 
							if(notification.getIsemail().equals(true)){  
									tomail=notification.getTomail();
									subject=notification.getSubject(); 
									if(order.getInvoice()!=0){
										Invoice enty=invoiceService.getInvoice(order.getInvoice());
										invoices="，发票抬头："+enty.getCompanyName();
									}
								   content.append(notification.getCenter()+":").append("订单号："+order.getOrderNo()+",下单时间："+CommonMethod.CalendarToString(Calendar.getInstance(), "yyyy/MM/dd HH:mm:ss")+"，下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，联系方式："+order.getPhone()).append(invoices);
									if(content!=null){
										mailcentent=content.toString(); 
									}
									String [] email=tomail.split(",");
		    						for(String str:email){
		    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
		    						}
 								//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
							}
						}
					}
				}else{
					if(nowTime - creattime >= purchasepu){
						type="TENHOUR";
						notification=notificationService.getnotification(type,type_twl); 
						if(notification!=null){ 
							if(notification.getIsemail().equals(true)){  
									tomail=notification.getTomail();
									subject=notification.getSubject(); 
									if(!order.getTransactionType().equals("RENT")){ 
										if(order.getInvoice()!=0){
											Invoice enty=invoiceService.getInvoice(order.getInvoice());
											invoices="，发票抬头："+enty.getCompanyName();
										}
									}
								   content.append(notification.getCenter()+":").append("订单号："+order.getOrderNo()+",下单时间："+CommonMethod.CalendarToString(Calendar.getInstance(), "yyyy/MM/dd HH:mm:ss")+"，下单数量："+order.getCount()+"，收货地址："+order.getSite()+"联系人："+order.getName()+"，联系方式："+order.getPhone()).append(invoices);
									if(content!=null){
										mailcentent=content.toString(); 
									}
									String [] email=tomail.split(",");
		    						for(String str:email){
		    	    					mailNotification.sendMailErrorMessage(str, subject, mailcentent); 
		    						}
 								//mailNotification.sendNote(notification.getReceiveTeliPhone(), notification.getNotecenter());
							}
						}
					}
				}  
			}
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	//定时(30分钟)查询退款中的退款的微信退款状态
}
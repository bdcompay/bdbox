package com.bdbox.task;

import java.util.Calendar;
import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled; 
import org.springframework.stereotype.Component;

import com.bdbox.constant.BdcoodStatus;
import com.bdbox.entity.BDCood;
import com.bdbox.service.BDCoodService;
@Component 
public class BDCoodTask2 {
	 

	@Autowired
	private BDCoodService bdcoodService;
	
	@Scheduled(cron = "0 0/5 * * * ?")
	//查询过期的北斗码，将转台设置为过期
	public void queryBdcood(){
		try{
			System.out.println("开始查询北斗表，查询过期的北斗码"); 
		List<BDCood> list=bdcoodService.queryBdcood();
		if(list.size()>0){
		for(BDCood bdcood:list){
			if(bdcood!=null){
 				Calendar calendar = Calendar.getInstance(); 
				Calendar endtime=bdcood.getEndTime();
 				long now=calendar.getTimeInMillis();
 				long end=endtime.getTimeInMillis();
 				if(now>end){
 					BDCood  bdcoodenty=	bdcoodService.getbdcood_wl(bdcood.getBdcood());
 					bdcoodenty.setBdcoodStatus(BdcoodStatus.EXCEED);
					boolean result=bdcoodService.update(bdcoodenty);
					if(result){
						System.out.println("更新北斗码的状态！");
					}
 				}
			}
		}
		}else{
			System.out.println("北斗码无数据可更新");
		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}

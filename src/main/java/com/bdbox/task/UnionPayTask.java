package com.bdbox.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bdbox.service.OrderService;

/**
 * 银联相关操作任务调度
 * @author hax
 *
 */
@Component
public class UnionPayTask {
	
	@Autowired
	private OrderService orderService;

	/**
	 * 定时查询有没有盒子租用超过三十天的
	 * 时间周期：每天的中午12点
	 */
	@Scheduled(cron = "0 0 12 * * ?")
	public void authFinish(){
		orderService.authFinish();
	}
}

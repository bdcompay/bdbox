package com.bdbox.task;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bdbox.constant.RefundStatus;
import com.bdbox.service.OrderService;
import com.bdbox.service.RefundService;
import com.bdbox.util.LogUtils;
import com.bdbox.web.dto.RefundDto;

@Component
public class RefundTask {
	@Autowired
	private RefundService refundService;
	@Autowired
	private OrderService orderService;
	
	/**
	 * 30分钟查看一次微信退款状态
	 */
	@Scheduled(cron = "0 0/1 * * * ?")
	public void execute() {
		LogUtils.loginfo("执行查询退款一次");
		try{
			List<RefundDto> list = refundService.listRefund(null, null, null, null,
					RefundStatus.APPLY, 0, 999999, null);
			for(RefundDto rd : list){
				/*Refund refund = refundService.getRefund(rd.getId());
				refundService.doWxQueryRefund(refund.getRefundNo());*/
				refundService.doWxQueryRefund(rd.getRefundNo());
			}
		}catch(Exception e){
			LogUtils.logerror("查询退款异常", e);
		}
		
	}
}

package com.bdbox.consts;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

/**
 * 全局变量，主要是用于一些将来可能改变的参数上
 * @author dezhi.zhang
 * @createTime 2017/01/19
 */
public class GlobalVar {
	/**
	 * 企业用户位置信息最大推送次数
	 */
	public static final Integer ENTERPRISE_USER_PUSH_LOCATION_COUNT=2;
	/**
	 * 企业用户通讯信息最大推送次数
	 */
	public static final Integer ENTERPRISE_USER_PUSH_MESSAGE_COUNT=2;
	
	/**
	 * 记录线程执行后的结果
	 */
	public static List<Future<Object>> PushDataList=new ArrayList<>(0);
}

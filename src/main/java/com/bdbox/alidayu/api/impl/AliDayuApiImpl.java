package com.bdbox.alidayu.api.impl;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import com.bdbox.alidayu.api.AliDayuApi;
import com.bdbox.alidayu.bean.ConfigBean;
import com.bdbox.alidayu.util.HttpRequester;
import com.bdbox.alidayu.util.ParamUtils;

public class AliDayuApiImpl implements AliDayuApi {

	@Override
	public Map<String, String> sendSms(String signName, String param, String recNum, String templateCode) {
		// TODO Auto-generated method stub
		Map<String, String> paramMap = new HashMap<String, String>();
		try {
			//api名称、类型、公共参数、simpleJson、md5
			paramMap.put("method", ConfigBean.SMS_SEND);
			paramMap.putAll(ParamUtils.publicParamMap);
			
			//send短信参数
			paramMap.put("sms_type", ConfigBean.SMS_TYPE);
			if(param != null){
				paramMap.put("sms_param", param);
			}
			paramMap.put("sms_free_sign_name", signName);
			paramMap.put("rec_num", recNum);
			paramMap.put("sms_template_code", templateCode);
			
			//签名、获取最后请求参数
			paramMap = ParamUtils.getRquestParam(paramMap);
			
			//执行请求
			String result = HttpRequester.httpPostBasic(ConfigBean.URL, paramMap);
			
			//简单json解析
			paramMap.clear();
			if(result.contains("error_response")){
				//请求错误
				JSONObject json =  JSONObject.fromObject(JSONObject.fromObject(result).get("error_response"));
				paramMap.put("status", "ERROR");
				paramMap.put("msg", json.get("msg").toString());
			}else if(result.contains("result")){
				//成功
				paramMap.put("status", "OK");
			}else{
				//未知
				paramMap.put("status", "ERROR");
				paramMap.put("msg", "未知错误");
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			paramMap.clear();
			paramMap.put("status", "SYSERROR");
			paramMap.put("msg", "系统错误");
		}
		return paramMap;
	}

}

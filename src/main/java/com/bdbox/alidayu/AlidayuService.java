package com.bdbox.alidayu;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import com.bdbox.alidayu.api.AliDayuApi;
import com.bdbox.alidayu.factory.AlidayuApiFactory;
import com.bdbox.alidayu.util.ParamUtils;

public class AlidayuService {
	
	private AliDayuApi aliDayuApi;
	
	/**
	 * 初始化公共参数
	 * @param url
	 * 		API URL
	 * @param method
	 * 		是	API接口名称。
	 * @param appKey
	 * 		是	TOP分配给应用的AppKey
	 * @param secret
	 * 		是 	密钥
	 * @param session
	 * 		否	用户登录授权成功后，TOP颁发给应用的授权信息，详细介绍请点击这里。当此API文档的标签上注明：
	 * 			“需要授权”，则此参数必传；“不需要授权”，则此参数不需要传；“可选授权”，则此参数为可选。
	 * @param format
	 * 		否	响应格式。默认为xml格式，可选值：xml，json。
	 * @param v
	 * 		是	API协议版本，可选值：2.0。
	 * @param partnerId
	 * 		否	合作伙伴身份标识。
	 * @param targetAppKey
	 * 		否	被调用的目标AppKey，仅当被调用的API为第三方ISV提供时有效。
	 * @param simplify
	 * 		否	是否采用精简JSON返回格式，仅当format=json时有效，默认值为：false。
	 * @param signMethod
	 * 		是	签名的摘要算法，可选值为：hmac，md5。
	 * @return
	 */
	public boolean init(String appKey,String secret,String smsSign){
		ParamUtils.configParamMap.put("app_key", appKey);
		ParamUtils.secret = secret;
		ParamUtils.smsSign = smsSign;
		
		String msg = null;
		for(String key:ParamUtils.configParamMap.keySet()){
			if(ParamUtils.configParamMap.get(key) == null){
				if(msg==null){
					msg = key+"不能为null";
				}else{
					msg += ","+key+"不能为null";
				}
			}
		}
		if(msg != null){
			System.out.println(msg);
			return false;
		}
		aliDayuApi = AlidayuApiFactory.create();
		
		return true;
	}
	
	
	//发送短信
	public Map<String, String> sendSms(String param,String recNum,String templateCode){
		if(aliDayuApi == null) return null;
		return aliDayuApi.sendSms(ParamUtils.smsSign, param, recNum, templateCode);
	}
	
	
	
	public static void main(String[] args) {
		//String url = "http://gw.api.taobao.com/router/rest";
		//String method = "alibaba.aliqin.fc.sms.num.send";
		//String session = null;
		//String format = "json";
		//String v = "2.0";
		/*String partnerId = null;
		String targetAppKey = null;*/
		//boolean simplify = false;
		//String signMethod = "md5";
		
		String appKey = "23404913";
		String secret = "71d10b6a880c52a0d49c791efa6462ad";
		
		AlidayuService service =  new AlidayuService();
		service.init(appKey, secret,"驴讯通");
		
		//发送短信
		//String extend = "123456";
		//String type = "normal";
		String signName = "驴讯通";
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("code", "123659");
		map.put("product", "驴讯通");
		
		String param = JSONObject.fromObject(map).toString();
		String recNum = "13631468075";
		String templateCode = "SMS_7395039";
		service.sendSms(param, recNum, templateCode);
		
		
		
	}
	
}

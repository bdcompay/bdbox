package com.bdbox.alidayu.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.bdbox.alidayu.bean.ConfigBean;
import com.taobao.api.Constants;
import com.taobao.api.internal.util.StringUtils;

/**
 * 
 * @author jlj
 *
 */
public class ParamUtils {
	/**
	 * 密钥
	 */
	public static String secret;
	/**
	 * 签名
	 */
	public static String smsSign;
	
	/**
	 * 公共参数
	 */
	public static Map<String, String> publicParamMap = new HashMap<String, String>();
	static{
		publicParamMap.put("v", ConfigBean.VERSION_2);
		publicParamMap.put("format", "json");
		publicParamMap.put("sign_method", "md5");
		publicParamMap.put("simplify", String.valueOf(true));
	}
	
	/**
	 * 配置参数
	 */
	public static Map<String, String> configParamMap = new HashMap<String, String>();
	
	public static Map<String,String> getRquestParam(Map<String, String> params) throws NoSuchAlgorithmException, IOException{
		params.putAll(configParamMap);
		String signMethod = params.get("sign_method");
		params.put("timestamp", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
		params.put("sign", signTopRequest(params, secret, signMethod));
		return params;
	}

	/**
	 * 请求签名
	 * @param params
	 * @param secret
	 * @param signMethod
	 * @return
	 * @throws IOException 
	 * @throws NoSuchAlgorithmException 
	 * @throws Exception
	 */
	public static String signTopRequest(Map<String, String> params,
			String secret, String signMethod) throws IOException, NoSuchAlgorithmException {
		// 第一步：检查参数是否已经排序
		String[] keys = params.keySet().toArray(new String[0]);
		Arrays.sort(keys);

		// 第二步：把所有参数名和参数值串在一起
		StringBuilder query = new StringBuilder();
		if (Constants.SIGN_METHOD_MD5.equals(signMethod)) {
			query.append(secret);
		}
		for (String key : keys) {
			String value = params.get(key);
			if (StringUtils.areNotEmpty(key, value)) {
				query.append(key).append(value);
			}
		}

		// 第三步：使用MD5/HMAC加密
		byte[] bytes;
		if (Constants.SIGN_METHOD_HMAC.equals(signMethod)) {
			bytes = encryptHMAC(query.toString(), secret);
		} else {
			query.append(secret);
			bytes = encryptMD5(query.toString());
		}

		// 第四步：把二进制转化为大写的十六进制
		return byte2hex(bytes);
	}

	/**
	 * HMAC加密
	 * @param data
	 * @param secret
	 * @return
	 * @throws IOException
	 */
	public static byte[] encryptHMAC(String data, String secret) throws IOException{
		byte[] bytes = null;
		try {
			SecretKey secretKey = new SecretKeySpec(
					secret.getBytes(Constants.CHARSET_UTF8), "HmacMD5");
			Mac mac = Mac.getInstance(secretKey.getAlgorithm());
			mac.init(secretKey);
			bytes = mac.doFinal(data.getBytes(Constants.CHARSET_UTF8));
		} catch (GeneralSecurityException gse) {
			throw new IOException(gse.toString());
		}
		return bytes;
	}
	
	/** 
     * MD5加密 
     *  
     * @param data 
     * @return 
	 * @throws NoSuchAlgorithmException 
	 * @throws UnsupportedEncodingException 
     * @throws Exception 
     */
	public static byte[] encryptMD5(String data) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		//return encryptMD5(data.getBytes(Constants.CHARSET_UTF8));
		MessageDigest md5 = MessageDigest.getInstance("MD5");  
        md5.update(data.getBytes(Constants.CHARSET_UTF8));  
        return md5.digest();  
	}

    /**
     * 二进制转十六进制
     * @param bytes
     * @return
     */
	public static String byte2hex(byte[] bytes) {
		StringBuilder sign = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(bytes[i] & 0xFF);
			if (hex.length() == 1) {
				sign.append("0");
			}
			sign.append(hex.toUpperCase());
		}
		return sign.toString();
	}

}

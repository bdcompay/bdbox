package com.bdbox.alidayu.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;

import com.bdbox.util.LogUtils;

/**
 * Http请求器
 * 
 * @ClassName: HttpRequester 
 * @author lijun.jiang@bdwise.com  
 * @date 2016-4-18 下午4:21:16 
 * @version V5.0 
 */
public class HttpRequester {
	
	/**
	 * httpPost基础请求
	 * 
	 * @param url
	 * 		请求URL
	 * @param map
	 * 		请求参数
	 * @return
	 */
	public static String httpPostBasic(String url,Map<String,String> map){
		//创建HttpClient
		HttpClient httpClient = new DefaultHttpClient();
		//连接超时
		/*httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 1000);
		httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 6000);*/
		//创建HttpPost
		HttpPost httpPost = new HttpPost(url);
		
		try {
			//设置参数
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			Set<String> set = map.keySet();
			for(String key:set){
				nvps.add(new BasicNameValuePair(key, map.get(key)));
			}
			
			//设置HttpPost参数
			httpPost.setEntity(new UrlEncodedFormEntity(nvps,"utf8"));
		
			//执行httpPost请求
			HttpResponse resp = httpClient.execute(httpPost);
			
			//处理响应
			BufferedReader reader = new BufferedReader(new InputStreamReader(resp.getEntity().getContent(),"utf8"));
			StringBuffer result = new StringBuffer();
			String inputLine = null;
			if((inputLine = reader.readLine()) !=null ){
				result.append(inputLine);
			}
			return result.toString();
		} catch (UnsupportedEncodingException e) {
			LogUtils.logerror("HttpPost基础请求，不支持编码错误", e);
		} catch (ClientProtocolException e) {
			LogUtils.logerror("HttpPost基础请求，客服端协议错误", e);
		} catch (IOException e) {
			LogUtils.logerror("HttpPost基础请求，IO错误", e);
		} catch(Exception e){
			LogUtils.logerror("HttpPost基础请求，系统错误", e);
		} finally{
			//释放连接
			httpPost.releaseConnection();
		}
		return null;
	}
	
	/**
	 * HttpPostMime请求
	 * 
	 * @param url
	 * 		请求URL
	 * @param map
	 * 		请求参数
	 * @return
	 */
	public static String httpPostMime(String url,Map<String,Object> map){
		//创建HttpClient
		HttpClient httpClient = new DefaultHttpClient();
		//连接超时
		httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 1000);
		httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, 6000);
		//创建HttpPost
		HttpPost httpPost = new HttpPost(url);
		
		try {
			//设置参数
			MultipartEntity mulEntity = new MultipartEntity();
			Set<String> set = map.keySet();
			for(String key:set){
				if(map.get(key).getClass().equals(File.class)){
					mulEntity.addPart(key, new FileBody((File)map.get(key)));
				}else{
					mulEntity.addPart(key,new StringBody((String)map.get(key)));
				}
			}
			
			//设置HttpPost参数
			httpPost.setEntity(mulEntity);
		
			//执行httpPost请求
			HttpResponse resp = httpClient.execute(httpPost);
			
			//处理响应
			BufferedReader reader = new BufferedReader(new InputStreamReader(resp.getEntity().getContent(),"utf8"));
			StringBuffer result = new StringBuffer();
			String inputLine = null;
			if((inputLine = reader.readLine()) !=null ){
				result.append(inputLine);
			}
			return result.toString();
		} catch (UnsupportedEncodingException e) {
			LogUtils.logerror("HttpPost基础请求，不支持编码错误", e);
		} catch (ClientProtocolException e) {
			LogUtils.logerror("HttpPost基础请求，客服端协议错误", e);
		} catch (IOException e) {
			LogUtils.logerror("HttpPost基础请求，IO错误", e);
		} catch(Exception e){
			LogUtils.logerror("HttpPost基础请求，系统错误", e);
		} finally{
			//释放连接
			httpPost.releaseConnection();
		}
		return null;
	}
	
	public static String sendGet(String url, Map<String, String> paramMap) {
		// 发送请求参数
		String param = "";
		if (paramMap != null) {
			Set<String> set = paramMap.keySet();
			for (String key : set) {
				if (param == null) {
					param = key + "=" + paramMap.get(key);
				} else {
					param += "&" + key + "=" + paramMap.get(key);
				}
			}
			param = "?" + param;
		}

		String urlNameString = url + param;
		//System.out.println(urlNameString);
		// HttpGet连接对象
		HttpGet httpRequest = new HttpGet(urlNameString);
		try {
			// 取得HttpClient对象
			HttpClient httpclient = new DefaultHttpClient();
			// 请求HttpClient取得HttpResponse
			HttpResponse resp = httpclient.execute(httpRequest);
			// 处理响应
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					resp.getEntity().getContent(), "utf8"));
			StringBuffer result = new StringBuffer();
			String inputLine = null;
			if ((inputLine = reader.readLine()) != null) {
				result.append(inputLine);
			}
			return result.toString();
		} catch (Exception e) {
			LogUtils.logerror("HttpGET请求错误，系统错误", e);
		}
		return null;
	}
}

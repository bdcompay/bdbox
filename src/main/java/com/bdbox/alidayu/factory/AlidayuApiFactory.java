package com.bdbox.alidayu.factory;

import com.bdbox.alidayu.api.AliDayuApi;
import com.bdbox.alidayu.api.impl.AliDayuApiImpl;

public class AlidayuApiFactory {
	public static AliDayuApi create(){
		return new AliDayuApiImpl();
	}
}

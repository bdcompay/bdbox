package com.bdbox.alidayu.bean;

/**
 * API配置参数
 * @author jlj
 *
 */
public class ConfigBean {
	
	/**
	 * URL
	 */
	public static final String URL = "http://gw.api.taobao.com/router/rest";
	
	/**
	 * API 版本
	 */
	public static final String VERSION_2 = "2.0";
	
	/**
	 * 多方通话
	 */
	//alibaba.aliqin.fc.voice.num.doublecall (多方通话)
	public static final String VOICE_DOUBLECALL = "alibaba.aliqin.fc.voice.num.doublecall"; 
			
	/**
	 * 文本转语音通知
	 */
	//alibaba.aliqin.fc.tts.num.singlecall (文本转语音通知)
	public static final String TTC_SINGLECALL = "alibaba.aliqin.fc.tts.num.singlecall";
	
	/**
	 * 语音通知
	 */
	//alibaba.aliqin.fc.voice.num.singlecall (语音通知)
	public static final String VOICE_SINGLECALL = "alibaba.aliqin.fc.voice.num.singlecall";
	
	/**
	 * 短信发送
	 */
	//alibaba.aliqin.fc.sms.num.send (短信发送)
	public static final String SMS_SEND = "alibaba.aliqin.fc.sms.num.send";
	
	public static final String SMS_TYPE = "normal";
	
	/**
	 * 短信发送记录查询
	 */
	//alibaba.aliqin.fc.sms.num.query (短信发送记录查询)
	public static final String SMS_QUERY = "alibaba.aliqin.fc.sms.num.query";
	
	/**
	 * 流量直充查询
	 */
	//alibaba.aliqin.fc.flow.query (流量直充查询)
	public static final String FLOW_QUERY = "alibaba.aliqin.fc.flow.query";
	
	/**
	 * 流量直充
	 */
	//alibaba.aliqin.fc.flow.charge (流量直充)
	public static final String FLOW_CHARGE = "alibaba.aliqin.fc.flow.charge";
	
	/**
	 * 流量直充档位表
	 */
	//alibaba.aliqin.fc.flow.grade (流量直充档位表)
	public static final String FLOE_GRADE = "alibaba.aliqin.fc.flow.grade";
	
	/**
	 * 流量直充分省接口
	 */
	//alibaba.aliqin.fc.flow.charge.province (流量直充分省接口)
	public static final String FLOW_CHARGE_PROVINCE = "alibaba.aliqin.fc.flow.charge.province";
}

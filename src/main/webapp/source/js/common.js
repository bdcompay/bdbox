	

//格式化JSON字符串
function jsonformat(jsonStr){
	if(jsonStr == null || jsonStr ==""){
		return "";
	}
	var result = "";
	try{
		eval("("+jsonStr+")");
	}
	catch(e){
		result = undefined;
	}
	if(result === undefined){
		return jsonStr;
	}
	else{
		var isInQuot = false;
		var deep = 0;//当前缩进深度
		var ch = "";
		var ch_pre = "";
		
		for(var i=0;i<jsonStr.length;i++){
			ch_pre = ch;
			ch = jsonStr.slice(i,i+1);
			//是在引号内还是在引号外
			if(ch=="\"" && ch_pre!="\\"){
				isInQuot = !isInQuot;
			}
			
			
			//开始添加缩进和回车
			if(!isInQuot){
				//清除引号外的空白字符
				if(ch.match(/[\r\t\s]/)) continue;
				if(ch == ","){
					result = result + ",\r" + getSpace(deep);
				}
				else if(ch == ":"){
					result = result + " : ";
				}
				else if(ch == "{"){
					deep ++;
					result = result + "{\r" + getSpace(deep);
				}
				else if(ch == "}"){
					deep --;
					result = result + "\r" + getSpace(deep) + "}";
				}
				else if(ch == "["){
					deep ++;
					result = result + "[\r" + getSpace(deep);
				}
				else if(ch == "]"){
					deep --;
					result = result + "\r" + getSpace(deep) + "]";
				}
				else{
					result += ch;
				}
			}
			else{
				result += ch;
			}
		}
		return result;
	}
}

//根据缩进深度返回tab
function getSpace(deep){
	var tab = "";
	for(var j=0;j<deep;j++){
		tab += "    "
	}
	return tab;
}

//对Date的扩展，将 Date 转化为指定格式的String   
//月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，   
//年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)   
//例子：   
//(new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423   
//(new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18   
Date.prototype.Format = function(fmt)   
{ //author: meizz   
var o = {   
 "M+" : this.getMonth()+1,                 //月份   
 "d+" : this.getDate(),                    //日   
 "h+" : this.getHours(),                   //小时   
 "m+" : this.getMinutes(),                 //分   
 "s+" : this.getSeconds(),                 //秒   
 "q+" : Math.floor((this.getMonth()+3)/3), //季度   
 "S"  : this.getMilliseconds()             //毫秒   
};   
if(/(y+)/.test(fmt))   
 fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));   
for(var k in o)   
 if(new RegExp("("+ k +")").test(fmt))   
fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));   
return fmt;   
}  

//调整父级frame大小以适应本frame的变化
function resizeWin(){
	//如果当前window不是最外层window,而是一个子frame.
	if(!isThisTopWindow()){
		//将外层window的title改为本frame的title
		title($("title").text());
		
		//调用parent的resizeFrame()方法将iframe的高度调节至适应自身的高度
		var minHeight = 700;
		if(window.parent.resizeFrame){
			window.parent.resizeFrame(minHeight);
			//var _h = $("html").outerHeight(true);
			var _h = window.document.documentElement.scrollHeight;
			if(_h > minHeight){
				window.parent.resizeFrame(_h);
			}
		}
	}
}

//本iframe是最外层的window吗
function isThisTopWindow(){
	return !(window.location.host == window.parent.location.host && window.location.href != window.parent.location.href);
}

//得到URL中传递的参数
function getUrlArg(name){
	var queryString = window.location.search;
	var arg = null;
	var reg = /\?/g;
	queryString = queryString.replace(reg,"");
	if(queryString.length>0){
	    var parameters = queryString.split("&");  
	    var pos, paraName, paraValue;  
	    for(var i=0; i<parameters.length; i++)  
	    {  
	        pos = parameters[i].indexOf('=');  
	        if(pos == -1) { continue; }  
	   
	        paraName = parameters[i].substring(0, pos);  
	        paraValue = parameters[i].substring(pos + 1);  
	   
	        if(paraName == name) arg = decodeURIComponent(paraValue);
	    }  
	}
	return arg;
}

//返回最顶层的window,用于在iframe中得到最外层的window
function getTopWindow(){
	var top, eWindow = window; 
	try { 
	    while(eWindow.location.host == eWindow.parent.location.host && eWindow.location.href != eWindow.parent.location.href){ 
	   		eWindow = eWindow.parent; 
		} 
		top = eWindow; 
	} catch(e) { 
		top = eWindow; 
	}
	return top;
}

//将最外层window的title改为t
function title(t){
	var top = getTopWindow();
	top.document.title = t;
}



//全局的JSON返回信息预处理
function doCheck(resp){
	
	if(!resp.status){
		alert("服务器发生错误,请稍后重试!");
		return;
	}
	if(resp.status == "NO_PERMISSION"){
		alert("无此权限,请以其它账号登录后再操作.","提示");
		return;
	}
	if(resp.status == "AUTH_FAILURE"){
		alert("您需要重新登录!");
		var top = getTopWindow();
		top.location = $web("main-login");
		return;
	}
}

//根据对象属性及dom的ID 填充form
function fillForm(obj){
	//遍历对象属性
	for(var prop in obj){
		var _dom = $("#"+prop).get(0);
		if(_dom == undefined){
			_dom = $("input[name="+prop+"]").get(0);
		}
		if(_dom != undefined){
			var val = obj[prop];
			if(val == null){
				continue;
			}
			var type = _dom.tagName;
			if(val.match && val.match(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/) != null ){
				val = val.replace("T"," ");
			}
			
			if(type == "INPUT"){
				if($("#"+prop).attr("type") =="text"){
					$("#"+prop).attr("value",val);
				}
				else if($("#"+prop).attr("type") =="checkbox"){
					$("#"+prop).attr("checked",val);
				}
				else{
					$("input[name="+prop+"][value="+val+"]").attr("checked", true);
				}
			}
			else if(type == "SELECT"){
				$("#"+prop).attr("value",val);
			}
			else if(type == "TEXTAREA" || type=="SPAN" || type=="DIV"){
				$("#"+prop).text(val);
			}
		}
	}
}

var hexcase=0;
//MD5
function hex_md5(a){ if(a=="") return a; return rstr2hex(rstr_md5(str2rstr_utf8(a)))}function hex_hmac_md5(a,b){return rstr2hex(rstr_hmac_md5(str2rstr_utf8(a),str2rstr_utf8(b)))}function md5_vm_test(){return hex_md5("abc").toLowerCase()=="900150983cd24fb0d6963f7d28e17f72"}function rstr_md5(a){return binl2rstr(binl_md5(rstr2binl(a),a.length*8))}function rstr_hmac_md5(c,f){var e=rstr2binl(c);if(e.length>16){e=binl_md5(e,c.length*8)}var a=Array(16),d=Array(16);for(var b=0;b<16;b++){a[b]=e[b]^909522486;d[b]=e[b]^1549556828}var g=binl_md5(a.concat(rstr2binl(f)),512+f.length*8);return binl2rstr(binl_md5(d.concat(g),512+128))}function rstr2hex(c){try{hexcase}catch(g){hexcase=0}var f=hexcase?"0123456789ABCDEF":"0123456789abcdef";var b="";var a;for(var d=0;d<c.length;d++){a=c.charCodeAt(d);b+=f.charAt((a>>>4)&15)+f.charAt(a&15)}return b}function str2rstr_utf8(c){var b="";var d=-1;var a,e;while(++d<c.length){a=c.charCodeAt(d);e=d+1<c.length?c.charCodeAt(d+1):0;if(55296<=a&&a<=56319&&56320<=e&&e<=57343){a=65536+((a&1023)<<10)+(e&1023);d++}if(a<=127){b+=String.fromCharCode(a)}else{if(a<=2047){b+=String.fromCharCode(192|((a>>>6)&31),128|(a&63))}else{if(a<=65535){b+=String.fromCharCode(224|((a>>>12)&15),128|((a>>>6)&63),128|(a&63))}else{if(a<=2097151){b+=String.fromCharCode(240|((a>>>18)&7),128|((a>>>12)&63),128|((a>>>6)&63),128|(a&63))}}}}}return b}function rstr2binl(b){var a=Array(b.length>>2);for(var c=0;c<a.length;c++){a[c]=0}for(var c=0;c<b.length*8;c+=8){a[c>>5]|=(b.charCodeAt(c/8)&255)<<(c%32)}return a}function binl2rstr(b){var a="";for(var c=0;c<b.length*32;c+=8){a+=String.fromCharCode((b[c>>5]>>>(c%32))&255)}return a}function binl_md5(p,k){p[k>>5]|=128<<((k)%32);p[(((k+64)>>>9)<<4)+14]=k;var o=1732584193;var n=-271733879;var m=-1732584194;var l=271733878;for(var g=0;g<p.length;g+=16){var j=o;var h=n;var f=m;var e=l;o=md5_ff(o,n,m,l,p[g+0],7,-680876936);l=md5_ff(l,o,n,m,p[g+1],12,-389564586);m=md5_ff(m,l,o,n,p[g+2],17,606105819);n=md5_ff(n,m,l,o,p[g+3],22,-1044525330);o=md5_ff(o,n,m,l,p[g+4],7,-176418897);l=md5_ff(l,o,n,m,p[g+5],12,1200080426);m=md5_ff(m,l,o,n,p[g+6],17,-1473231341);n=md5_ff(n,m,l,o,p[g+7],22,-45705983);o=md5_ff(o,n,m,l,p[g+8],7,1770035416);l=md5_ff(l,o,n,m,p[g+9],12,-1958414417);m=md5_ff(m,l,o,n,p[g+10],17,-42063);n=md5_ff(n,m,l,o,p[g+11],22,-1990404162);o=md5_ff(o,n,m,l,p[g+12],7,1804603682);l=md5_ff(l,o,n,m,p[g+13],12,-40341101);m=md5_ff(m,l,o,n,p[g+14],17,-1502002290);n=md5_ff(n,m,l,o,p[g+15],22,1236535329);o=md5_gg(o,n,m,l,p[g+1],5,-165796510);l=md5_gg(l,o,n,m,p[g+6],9,-1069501632);m=md5_gg(m,l,o,n,p[g+11],14,643717713);n=md5_gg(n,m,l,o,p[g+0],20,-373897302);o=md5_gg(o,n,m,l,p[g+5],5,-701558691);l=md5_gg(l,o,n,m,p[g+10],9,38016083);m=md5_gg(m,l,o,n,p[g+15],14,-660478335);n=md5_gg(n,m,l,o,p[g+4],20,-405537848);o=md5_gg(o,n,m,l,p[g+9],5,568446438);l=md5_gg(l,o,n,m,p[g+14],9,-1019803690);m=md5_gg(m,l,o,n,p[g+3],14,-187363961);n=md5_gg(n,m,l,o,p[g+8],20,1163531501);o=md5_gg(o,n,m,l,p[g+13],5,-1444681467);l=md5_gg(l,o,n,m,p[g+2],9,-51403784);m=md5_gg(m,l,o,n,p[g+7],14,1735328473);n=md5_gg(n,m,l,o,p[g+12],20,-1926607734);o=md5_hh(o,n,m,l,p[g+5],4,-378558);l=md5_hh(l,o,n,m,p[g+8],11,-2022574463);m=md5_hh(m,l,o,n,p[g+11],16,1839030562);n=md5_hh(n,m,l,o,p[g+14],23,-35309556);o=md5_hh(o,n,m,l,p[g+1],4,-1530992060);l=md5_hh(l,o,n,m,p[g+4],11,1272893353);m=md5_hh(m,l,o,n,p[g+7],16,-155497632);n=md5_hh(n,m,l,o,p[g+10],23,-1094730640);o=md5_hh(o,n,m,l,p[g+13],4,681279174);l=md5_hh(l,o,n,m,p[g+0],11,-358537222);m=md5_hh(m,l,o,n,p[g+3],16,-722521979);n=md5_hh(n,m,l,o,p[g+6],23,76029189);o=md5_hh(o,n,m,l,p[g+9],4,-640364487);l=md5_hh(l,o,n,m,p[g+12],11,-421815835);m=md5_hh(m,l,o,n,p[g+15],16,530742520);n=md5_hh(n,m,l,o,p[g+2],23,-995338651);o=md5_ii(o,n,m,l,p[g+0],6,-198630844);l=md5_ii(l,o,n,m,p[g+7],10,1126891415);m=md5_ii(m,l,o,n,p[g+14],15,-1416354905);n=md5_ii(n,m,l,o,p[g+5],21,-57434055);o=md5_ii(o,n,m,l,p[g+12],6,1700485571);l=md5_ii(l,o,n,m,p[g+3],10,-1894986606);m=md5_ii(m,l,o,n,p[g+10],15,-1051523);n=md5_ii(n,m,l,o,p[g+1],21,-2054922799);o=md5_ii(o,n,m,l,p[g+8],6,1873313359);l=md5_ii(l,o,n,m,p[g+15],10,-30611744);m=md5_ii(m,l,o,n,p[g+6],15,-1560198380);n=md5_ii(n,m,l,o,p[g+13],21,1309151649);o=md5_ii(o,n,m,l,p[g+4],6,-145523070);l=md5_ii(l,o,n,m,p[g+11],10,-1120210379);m=md5_ii(m,l,o,n,p[g+2],15,718787259);n=md5_ii(n,m,l,o,p[g+9],21,-343485551);o=safe_add(o,j);n=safe_add(n,h);m=safe_add(m,f);l=safe_add(l,e)}return Array(o,n,m,l)}function md5_cmn(h,e,d,c,g,f){return safe_add(bit_rol(safe_add(safe_add(e,h),safe_add(c,f)),g),d)}function md5_ff(g,f,k,j,e,i,h){return md5_cmn((f&k)|((~f)&j),g,f,e,i,h)}function md5_gg(g,f,k,j,e,i,h){return md5_cmn((f&j)|(k&(~j)),g,f,e,i,h)}function md5_hh(g,f,k,j,e,i,h){return md5_cmn(f^k^j,g,f,e,i,h)}function md5_ii(g,f,k,j,e,i,h){return md5_cmn(k^(f|(~j)),g,f,e,i,h)}function safe_add(a,d){var c=(a&65535)+(d&65535);var b=(a>>16)+(d>>16)+(c>>16);return(b<<16)|(c&65535)}function bit_rol(a,b){return(a<<b)|(a>>>(32-b))};

//获取表单数据,formId:表单标签ID属性
function getFormData(formId){
	var data = {};
	var lastRaio = "";
	$("#" + formId + " input,select,textarea").each(function(index, Element){
		if($(this).attr("disabled")!="disabled"){
			var v = $(this).val();
			var i = $(this).attr("id");
			if($(this).attr("type")=="checkbox"){
				v = $(this).attr("checked") ? true : false;
			}
			if((i != null && i != undefined)){
				//data += "&" + i + "=" + v;
				data[i] = v;
			}
			if($(this).attr("type")=="radio"){
				var thisRadio = $(this).attr("name");
				if(thisRadio != lastRaio){
					var rv = $("input[type=radio][name="+thisRadio+"]:checked").val();
					//data += "&" + thisRadio + "=" + rv;
					data[thisRadio] = rv;
					lastRaio = thisRadio;
				}
			}
		}
	});
	return data;
}

//将yyy-MM-dd hh:mm:ss格式的文本转为日期
function parseDate(dateString){
	//2013-07-12 13:23:38
	var dates = dateString.split(/[T\-\s:]/);
	var date = new Date();
	date.setFullYear(parseInt(dates[0]));
	date.setMonth(parseInt(dates[1].replace(/^0/,""))-1);
	date.setDate(parseInt(dates[2].replace(/^0/,"")));
	date.setHours(parseInt(dates[3].replace(/^0/,"")));
	date.setMinutes(parseInt(dates[4].replace(/^0/,"")));
	date.setSeconds(parseInt(dates[5].replace(/^0/,"")));
	date.setMilliseconds(0);
	return date;
}


function $ajax(url){
	return  ctx + "/ajax/" + url;
}

function $web(url){
	return ctx + "/web/" + url;
}

//注册自定义过滤器
function regFilter(){
	//判断input,true返回param1,false返回param2
	angular.module('myFilters', [])
	.filter('ifx', function() {
		  return function(input,param1,param2) {
		    return input ? param1 : param2;
		  };
	})
	.filter('dymd', function() {
		return function(input,param1) {
			if(input instanceof Date){
				return input.Format(param1);
			}
			else if(typeof input == 'string'){
				return parseDate(input).Format(param1);
			}
			else return input;
		};
	})
	//人性化显示时间,"xx分钟/小时/天前/后"
	.filter('hdate',function(){
		return function(input) {
		    var dis = parseDate(input).getTime() - new Date().getTime();
		    var isBefore = (dis >= 0 ? "后":"前"),time;
		    var adis = Math.abs(dis);
		    if( adis< 60 * 1000){
		    	time = "不到1分钟";
		    }
		    else if(adis < 60 * 60 * 1000){
		    	time = Math.round(adis/(60*1000)) + "分钟";
		    }
		    else if(adis <24 * 60 * 60 * 1000){
		    	time = Math.round(adis/(60*60*1000)) + "小时";
		    }
		    else if(adis <30 * 24 * 60 * 60 * 1000){
		    	time = Math.round(adis/(24*60*60*1000)) + "天";
		    }
		    else if(adis <12 * 30 * 24 * 60 * 60 * 1000){
		    	time = Math.round(adis/(30*24*60*60*1000)) + "月";
		    }
		    else{
		    	time = Math.round(adis/(12*30*24*60*60*1000)) + "年";
		    }
		    return time+isBefore;
		  };
	});
}
function logout(){
	$.ajax({
			url : $ajax("Manager-logout"),
			type : "POST",
			success : function(response, textStatus, jqXHR) {
				if(response.status == "OK"){
					window.location.reload(false);
				}
			}
		});
}

var CONSTANT ={
	week:[{d:1,n:"一"},{d:2,n:"二"},{d:3,n:"三"},{d:4,n:"四"},{d:5,n:"五"},{d:6,n:"六"},{d:7,n:"日"}],
	category:[{id:1,n:'音乐'},{id:2,n:'戏剧'},{id:3,n:'棋牌'},{id:4,n:'聚会'},	{id:5,n:'电影'}, {id:6,n:'美食'},{id:7,n:'运动'},	{id:8,n:'文化'},	{id:9,n:'旅行'}],
	cities:[{id:1,name:"广州"},{id:2,name:"北京"},{id:3,name:"上海"},{id:4,name:"深圳"},{id:5,name:"香港"}],
	priorities:[{n:"普通活动",v:"NORMAL"},{n:"推荐活动",v:"RECOMMENDATION"},{n:"广告活动",v:"ADVERT"},{n:"重要活动",v:"IMPORTANT"}],
	ticketTypes:[{n:"普通票",v:"NORMAL"},{n:"嘉宾票",v:"VIP"},{n:"兑换票",v:"EXCHANGES"}]
};

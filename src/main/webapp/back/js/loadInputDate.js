$(document).ready(function(){
	//加载日期框日期
	loadInputDate();
	$('.start').focus(function(e){
		setStart('start');
		WdatePicker({dateFmt:"yyyy-MM-dd HH:mm:ss",minDate:'%y-%M-%d'});
	});
	$('.end').focus(function(){
		setEnd('end');
		WdatePicker({dateFmt:"yyyy-MM-dd HH:mm:ss",minDate:$('.start').val()});
	});
	
	$('.savestart').focus(function(e){
		setStart('savestart');
		WdatePicker({dateFmt:"yyyy-MM-dd HH:mm:ss",minDate:'%y-%M-%d'});
	});
	$('.saveend').focus(function(){
		setEnd('saveend');
		WdatePicker({dateFmt:"yyyy-MM-dd HH:mm:ss",minDate:$('.savestart').val()});
	});
	
	$('.gen').focus(function(){
		WdatePicker({dateFmt:"yyyy-MM-dd HH:mm:ss"});
	});
	
});
//加载日期框日期
function loadInputDate(){
	var $start=$('.start');
	var $end=$('.end');
	if($start.val()==''){$start.val(todayToDateStr());}
	if($end.val()==''){$end.val(tomorrowToDateStr());}
}
/**
 * 返回明天的日期转换成字符串形式
 */
function tomorrowToDateStr() {
	var dt=new Date().getTime()+24*60*60*1000;
	var tdt=new Date(dt);
	var str=dateToStr(tdt);
	return str;
}
/**
 * 返回当前日期的字符串形式
 */
function todayToDateStr() {
	var dt= new Date();
	var str=dateToStr(dt);
	return str;
}
/**
 * 日期格式转换成字符串格式
 */
function dateToStr(getInputDate) {
	//获得输入的年
	var getInputYear=getInputDate.getUTCFullYear();
	//获得输入的月
	var getInputMonth=getInputDate.getMonth()+1;
	//获得输入的天
	var getInputDay=getInputDate.getDate();
	if (getInputYear<1000) {
		getInputYear=1900+getInputYear;
	}
	//当月份小于10时，月份前面补零
	if(getInputMonth<10){
		getInputMonth="0"+getInputMonth;
	}
	//当日期小于10时，日期前面补零
	if(getInputDay<10){
		getInputDay="0"+getInputDay;
	}
	var dtStr=getInputYear+"-"+getInputMonth+"-"+getInputDay+" 00:00:00";
	return dtStr;
}
//字符串转日期格式，strDate要转为日期格式的字符串
function getDate(strDate) {
    var date = eval('new Date(' + strDate.replace(/\d+(?=-[^-]+$)/,
     function (a) { return parseInt(a, 10) - 1; }).match(/\d+/g) + ')');
    return date;
}
//今天的起始时间(2000-01-01 00:00:00)
function setStart(e){
	if($('.'+e).val()==''){
		var time=new Date().getTime()+24*60*60*1000;
		var dt=new Date(time);
		var year = dt.getFullYear();    //获取完整的年份(4位,1970-????)
		var month = dt.getMonth()+1;    //获取当前月份(0-11,0代表1月)
		var d = dt.getDate();        	//获取当前日(1-31)
		var dtime = year+"-"+month+"-"+d+" 00:00:00";
		$('.'+e).val(dtime);
	}
}
//明天的起始时间(2000-01-01 00:00:00)
function setEnd(e){
	if($('.'+e).val()==''){
		var time=new Date().getTime()+24*60*60*1000;
		var dt=new Date(time);
		var year = dt.getFullYear();    //获取完整的年份(4位,1970-????)
		var month = dt.getMonth()+1;    //获取当前月份(0-11,0代表1月)
		var d = dt.getDate();        	//获取当前日(1-31)
		var dtime = year+"-"+month+"-"+d+" 00:00:00";
		$('.'+e).val(dtime);
	}
}
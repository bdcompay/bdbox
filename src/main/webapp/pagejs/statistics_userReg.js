statistic_regnumber();
		function statistic_regnumber(){
			var queryStartTime = $('#queryStartTime').val();
			var queryEndTime = $('#queryEndTime').val();
			if(queryStartTime=='' || queryEndTime==''){
				queryStartTime = todayToDateStr();
				queryEndTime = tomorrowToDateStr();
			}
			$.ajax({
				type:"POST",
				url:"statisticRegNumber.do",
				data:{"startTimeStr":queryStartTime,"endTimeStr":queryEndTime},
				datatype:"json",
				success:function(res){
					if(res.status=="OK"){
						$('#regnumber').text(res.result.queryCount);
						$('#todayNumber').text(res.result.todayCount);
						$('#monthNumber').text(res.result.monthCount);
						$('#totalNumber').text(res.result.allUserCount);
					}else{
						alert("操作失败");
						$('#regnumber').text(0);
					}
				},
				error:function(res){
					
				}
			});
		}
		
		$("#statistic").click(function(){
			statistic_regnumber();
		});
$(function() {
					//DataTable
					var oTable = $('.messageTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'listUserRegLog.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "username",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "createdTimeStr",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "ip",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "ipArea",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "69px",
																			"sClass" : "opera",
																			"mDataProp" : "userid",
																			"fnRender" : function(obj) {
																				var created = obj.aData['userid'];
																				if(created==1){
																					return "管理员";
																				}
																				return "用户"; 
																			} 
																		}]
														
													}));

					
					$("#query").click(function test() {
						oTable.fnDraw();
					});
					
					
					
					
					
					
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	var startTimeStr = $('#startTimeStr').val();
	var endTimeStr = $('#endTimeStr').val();
	var username = $('#username').val();
	
	aoData.push({name:"startTimeStr",value:startTimeStr},
			{name:"endTimeStr",value:endTimeStr},
			{name:"username",value:username});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}

/******************************时间控件*********************************/
$(".form_date").datetimepicker({
	language: "zh-CN",
	weekStart: 1,
	todayBtn: 1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	minView: 2,
	forceParse: 0,
	format: "yyyy/mm/dd"
});
/**************************************DataTable******************************************/
$(function() {
	//DataTable
	var oTable = $('.pushMessageTable')
		.dataTable(
			$.extend(
				dparams, {
					"sAjaxSource": 'listPushMessageRecord.do',
					"fnServerData": retrieveData, // 自定义数据获取函数
					"aoColumns": [{
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "entUser",
						"bSortable": false
					}, {
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "content",
						"bSortable": false
					}, {
						"sWidth": "130px",
						"sClass": "center",
						"mDataProp": "count",
						"bSortable": false
					}, {
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "isSuccess",
						"bSortable": false
					}, {
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "error",
						"bSortable": false
					}, {
						"sWidth": "130px",
						"sClass": "center",
						"mDataProp": "createTime",
						"bSortable": false
					}]
				}));

	$("#mycheck").click(function test() {
		oTable.fnDraw();
	});

});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	var entUserAccount = $("#entUserAccount").val();
	var maxCount = $("#maxCount").val();
	var isSuccess = $("#isSuccess").val();
	var startTimeStr = $("#startTimeStr").val().trim();
	var endTimeStr = $("#endTimeStr").val().trim();
	if(startTimeStr.length == 0) {
		startTimeStr = todayToDateStr();
	}
	if(endTimeStr.length == 0) {
		endTimeStr = tomorrowToDateStr();
	}
	
	if(isSuccess!="all"){
		aaData.push({
			"name":"isSuccess",
			"value":isSuccess
		});
	}
	aoData.push(
		{
		"name":"entUserAccount",
		"value":entUserAccount
		},
		{
		"name":"count",
		"value":maxCount
		},
		{
		"name":"startTime",
		"value":startTimeStr
		},
		{
		"name":"endTime",
		"value":endTimeStr
		}
	);

	$.ajax({
		"type": "GET",
		"url": sSource,
		"dataType": "json",
		"data": aoData,
		"success": function(resp) {
			fnCallback(resp);
		},
		"error": function(resp) {}
	});

}
/**************************************DataTable******************************************/
$(function() {
					//DataTable
					var oTable = $('.messageTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'queryEntUsers.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "name",
																			"bSortable" : false
																		},
																		/*{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "orgCode",
																			"bSortable" : false
																		},*/
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "linkman",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "phone",
																			"bSortable" : false
																		},	
																		{
																			"sWidth" : "50px",
																			"sClass" : "center",
																			"mDataProp" : "boxNum",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "69px",
																			"sClass" : "opera",
																			"mDataProp" : "isDsiEnable",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "69px",
																			"sClass" : "opera",
																			"mDataProp" : "userPowerKey",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "50px",
																			"sClass" : "opera",
																			"mDataProp" : "freq",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "69px",
																			"sClass" : "opera",
																			"mDataProp" : "lastSendBoxMessageTimeStr",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "69px",
																			"sClass" : "opera",
																			"mDataProp" : "createdTimeStr",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "69px",
																			"sClass" : "opera",
																			"mDataProp" : "explain1",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "opera",
																			"mDataProp" : "pushURL",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "70px",
																			"sClass" : "center",
																			"mDataProp" : "id",
																			"fnRender" : function(obj) {
																				var id = obj.aData['id'];
																				var entname = obj.aData['name'];
																				var boxnumber = obj.aData['boxNum'];
																				var result = "<a href=\"javascript:void(0);\" onclick=\"getEntUser("+id+")\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#updateModal\"><i class=\"halflings-icon white edit\"></i></a>"
																							+"<a class=\"btn btn-danger\" href=\"javascript:void(0);\" onclick=\"deleteEntUser("+id+",'"+entname+"',"+boxnumber+")\"><i class=\"halflings-icon trash white\"></i></a><br />"
																							+"<a class=\"btn btn-success\" href=\"ent_boxs.html?id="+id+"\"  >下属盒子</a>"; 																		 
																				return result;
																			},
																			"bSortable" : false
																		}]
														
													}));

					
					$("#query").click(function test() {
						oTable.fnDraw();
					});
					
					
					
					
					
					
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	var entname = $('#entname').val();
	var linkman = $('#linkman').val();
	
	aoData.push(
			{name:"name",value:entname},
			{name:"linkman",value:linkman});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});
}

/*********************************  参数验证  ********************************/
/*
 * 判断是不是不为空
 * 字符串为null或字符串去掉前后空格后长度为0返回:false
 * 否则返回：true
 */
function is_noEmpty($str){
	if($str==null) return false;
	if($str.trim().length==0) return false;
	return true;
}

/*
 * 判断是否为数字
 * 是数字:true
 * 不是数字:false
 */
function is_number($number){
	var reg=/^\d+$/;
	if(reg.test($number)) return true;
	return false;
}

/*
 * 判断是否为手机号
 * 手机号码格式
 * 只允许以13、15、17、18开头的号码
 * 如：13012345678、15929224344、17012345643、18201234676
 * 符合以上格式：true
 * 不符合：false
 */
function is_phone($mob){
	var reg = /^(?:13\d|15\d|17\d|18\d)-?\d{5}(\d{3}|\*{3})|(^\d{3}-\d{8}$)|(^\d{4}-\d{7}$)$/;
	if(reg.test($mob.trim())) return true;
	return false;
}

/*
 * 判断是否为座机号
 * 固定电话号码格式
 * 因为固定电话格式比较复杂，情况比较多，主要验证了以下类型
 * 如：010-12345678、0912-1234567、(010)-12345678、(0912)1234567、(010)12345678、(0912)-1234567、01012345678、09121234567
 * 符合以上格式：true
 * 不符合：false
 */
function is_telephone($telephone){
	var reg = /^(^0\d{2}-?\d{8}$)|(^0\d{3}-?\d{7}$)|(^\(0\d{2}\)-?\d{8}$)|(^\(0\d{3}\)-?\d{7}$)$/;
	if(reg.test($telephone)) return true;
	return false;
}

/*
 * 判断邮箱是否符合格式
 * 符合邮箱格式：true
 * 不符合：false
 */
function is_mail($mail){
	var reg = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if(reg.test($mail)) return true;
	return false;
}

/*
 * 邮编
 */
function is_postCode($postCode){
	var reg = /^\d{6}$/;
	if(reg.test($postCode)) return true;
	return false;
} 

/*
 * ip
 */
function is_ip($ip){
	var reg=/^((([1-9]\d?)|(1\d{2})|(2[0-4]\d)|(25[0-5]))\.){3}(([1-9]\d?)|(1\d{2})|(2[0-4]\d)|(25[0-5]))$/;
	if(reg.test($ip)) return true;
	return false;
}

/*
 * 只能是中文汉字
 */
function is_chineseChar($chineseChar){
	var reg=/^[\u4e00-\u9fa5]+$/;
	if(reg.test($chineseChar)) return true;
	return false;
}

/*
 * 网址
 * 只允许http、https、ftp这三种
 * 如：http://www.baidu.com
 */
function is_Web($web){
	var reg=/^(([hH][tT]{2}[pP][sS]?)|([fF][tT][pP]))\:\/\/[wW]{3}\.[\w-]+\.\w{2,4}(\/.*)?$/;
	if(reg.test($web)) return true;
	return false;
}

//参数验证正确
function showok(element){
	var error = "#"+element+"Error";
	$("#"+element).removeClass('input_error');
	$(error).text("");
}

//参数验证错误提醒
function showerror(element,errormessage){
	var error = "#"+element+"Error";
	$("#"+element).addClass('input_error');
	$(error).text(errormessage);
}


//参数验证是否通过标志
	var flag1 = false;	//企业名称
	var flag2 = false;	//组织代码
	var flag3 = false;	//联系人
	var flag4 = false;	//联系电话
	var flag5 = false;	//邮箱
	var flag6 = false;	//联系地址
	var flag7 = false;	//频度
	var flag8 = false;  //推送URL


//企业名称  
function checkEntName($id){
	if(!is_noEmpty($("#"+$id).val())){
		showerror($id,"不能为空");
	}else{
		showok($id);
		flag1= true;
		return ;
	}
	flag1 = false;
}
$("#saveEntName,#updateEntName").blur(function(){
	var $id = $.trim($(this).attr("id"));
	checkEntName($id);
});

//组织代码  
function checkOrgCode($id){
	/*if(!is_noEmpty($("#"+$id).val())){
		showerror($id,"不能为空");
	}else{
		showok($id);
		flag2= true;
		return ;
	}
	flag2= false;*/
}
$("#saveOrgCode,#updateOrgCode").blur(function(){
	var $id = $.trim($(this).attr("id"));
	checkOrgCode($id);
});

//联系人	
function checkLinkman($id){
	if(!is_noEmpty($("#"+$id).val())){
		showerror($id,"不能为空");
	}else{
		showok($id);
		flag3= true;
		return ;
	}
	flag3= false;
}
$("#saveLinkman,updateLinkman").blur(function(){
	var $id = $.trim($(this).attr("id"));
	checkLinkman($id);
});

//联系电话 
function checkPhone($id){
	if(!is_noEmpty($("#"+$id).val())){
		showerror($id,"不能为空");
	}else if(!is_phone($("#"+$id).val()) && !is_telephone($("#"+$id).val())){
		showerror($id,"格式错误");
	}else{
		showok($id);
		flag4= true;
		return ;
	}
	flag4= false;
}
$("#savePhone,#updatePhone").blur(function(){
	var $id = $.trim($(this).attr("id"));
	checkPhone($id);
});

//邮箱	
function checkMail($id){
	if(!is_noEmpty($("#"+$id).val())){
		showok($id);
		flag5= true;
		return ;
	}else if(!is_mail($("#"+$id).val())){
		showerror($id,"格式错误");
	}else {
		showok($id);
		flag5= true;
		return ;
	}
	flag5= false;
}
$("#saveMail,#updateMail").blur(function(){
	var $id = $.trim($(this).attr("id"));
	checkMail($id);
});

//联系地址 	
function checkAddress($id){
	/*if(!is_noEmpty($("#"+$id).val())){
		showerror($id,"不能为空");
	}else{
		showok($id);
		flag6= true;
		return ;
	}
	flag6= false;*/
}
$("#saveAddress,#updateAddress").blur(function(){
	var $id = $.trim($(this).attr("id"));
	checkAddress($id);
});

//频度
function checkFreq($id){
	if(!is_noEmpty($("#"+$id).val())){
		showerror($id,"不能为空");
	}else if(!is_number($("#"+$id).val())){
		showerror($id,"不是数字");
	}else {
		showok($id);
		flag7= true;
		return ;
	}
	flag7= false;
}

//推送URL
function checkPushURL($id){
	if(is_noEmpty($("#"+$id).val()) && !is_Web($("#"+$id).val())){
		showerror($id,"不是URL地址");
	}else {
		showok($id);
		flag8= true;
		return ;
	}
	flag8= false;
}
$("#saveFreq,#updateFreq").blur(function(){
	var $id = $.trim($(this).attr("id"));
	checkFreq($id);
});

//添加企业用提交前参数验证
function saveSubmitCheck(){
	checkEntName("saveEntName");
	checkOrgCode("saveOrgCode");
	checkLinkman("saveLinkman");
	checkPhone("savePhone");
	checkMail("saveMail");
	checkAddress("saveAddress");
	checkFreq("saveFreq");
	checkPushURL("savePushURL");
}

//编辑企业用提交前参数验证
function updateSubmitCheck(){
	checkEntName("updateEntName");
	checkOrgCode("updateOrgCode");
	checkLinkman("updateLinkman");
	checkPhone("updatePhone");
	checkMail("updateMail");
	checkAddress("updateAddress");
	checkFreq("updateFreq");
	checkPushURL("updatePushURL");
}


/************************************  功能操作  **************************************/
/*
 * 添加企业用户
 */
$(document).on("click","#saveSubmit",function(){
	//重复提交
	var disabled = $(this).attr("disabled");
	if(disabled=="disabled") return false;
	//验证参数
	saveSubmitCheck();
	if(!(flag1&&flag3&&flag4&&flag5&&flag7)) return false;
	//禁止按钮，防止重复提交
    $(this).attr("disabled","disabled");
    //获取参数数据
    var entname = $("#saveEntName").val();
    var orgCode = $("#saveOrgCode").val();
    var linkman = $("#saveLinkman").val();
    var phone = $("#savePhone").val();
    var mail = $("#saveMail").val();
    var address = $("#saveAddress").val();
    var isDsiEnable = $("#saveIsDsiEnable").val();
    var freq = $("#saveFreq").val();
    var explain = $("#saveExplain").val();
    var pushURL = $("#savePushURL").val();
    var password = $("#password").val();
    var data = {"entname":entname,"orgCode":orgCode,"linkman":linkman,
    		"phone":phone,"mail":mail,"address":address,"explain":explain,
    		"isDsiEnable":isDsiEnable,"freq":freq,"password":password,"pushURL":pushURL};
    $.ajax({
    	"type":"POST",
    	"url":"addEntUser.do",
    	"data":data,
    	"datatype":"json",
    	"success":function(res){
    		if(res.status=="OK"){
    			location.reload();
    		}
    		$(this).attr("disabled","");
    	}
    });
});

/*
 * 删除企业用户
 * 删除企业用户前需将所有的下属卡解绑
 * @param id 企业用户id
 * @param entname 企业用户名称
 * @param boxnumber 企业用户下属盒子数量
 */
function deleteEntUser(id,entname,boxnumber){
	if(boxnumber>0){
		alert("请解绑 "+entname+" 的所有下属北斗盒子！");
		return false;
	}
	if(confirm("你确定要删除"+entname+"吗？")){
		$.ajax({
			"type":"POST",
			"url":"deleteEntUser.do",
			"data":{"id":id},
			"datatype":"json",
			"success":function(res){
				if(res.status=="OK"){
					location.reload();
				}else{
					alert(res.message);
				}
			}
		});
	}
}

/*
 * 编辑企业用户
 * 从后台查询企业用户信息，并在编辑企业用户页面显示
 */
function getEntUser(id){
	$.ajax({
		"type":"POST",
		"url":"getEntUser.do",
		"data":{"id":id},
		"datatype":"json",
		"success":function(res){
			if(res.status=="OK"){
				var entUser = res.result;
				//获取参数数据
				$("#updateId").val(entUser.id);
			    $("#updateEntName").val(entUser.name);
			    $("#updateOrgCode").val(entUser.orgCode);
			    $("#updateLinkman").val(entUser.linkman);
			    $("#updatePhone").val(entUser.phone);
			    $("#updateMail").val(entUser.mail);
			    $("#updateAddress").val(entUser.address);
			    $("#updateExplain").val(entUser.explain1);
			    $("#updateFreq").val(entUser.freq);
			    $("#updatePushURL").val(entUser.pushURL);
			    $("#updatePassword").val(entUser.password);
			    if(entUser.isDsiEnable){
			    	$("#updateIsDsiEnable").get(0).options[0].selected = true; 
			    }else{
			    	$("#updateIsDsiEnable").get(0).options[1].selected = true; 
			    }
			}
		}
	});
}

/*
 * 企业用户编辑提交
 * 
 */
$(document).on("click","#updateSubmit",function(){
	
	//重复提交
	var disabled = $(this).attr("disabled");
	if(disabled=="disabled") return false;
	//验证参数
	updateSubmitCheck();
	
	if(!(flag1&&flag3&&flag4&&flag5&&flag7)) return false;
	//禁止按钮，防止重复提交
    $(this).attr("disabled","disabled");
    //获取参数数据
    var id = $("#updateId").val();
    var entname = $("#updateEntName").val();
    var orgCode = $("#updateOrgCode").val();
    var linkman = $("#updateLinkman").val();
    var phone = $("#updatePhone").val();
    var mail = $("#updateMail").val();
    var address = $("#updateAddress").val();
    var explain = $("#updateExplain").val();
    var pushURL = $("#updatePushURL").val();
    var isDsiEnable = $("#updateIsDsiEnable").val();
    var freq = $("#updateFreq").val();
    
	var data = {"id":id,"entname":entname,"orgCode":orgCode,"linkman":linkman,
			"phone":phone,"mail":mail,"address":address,"explain":explain,
			"isDsiEnable":isDsiEnable,"freq":freq,"pushURL":pushURL};
	$.ajax({
		"type":"POST",
		"url":"updateEntUser.do",
		"data":data,
		"datatype":"json",
		"success":function(res){
			if(res.status=="OK"){
				if(res.status=="OK"){
	    			location.reload();
	    		}
			}
			$(this).attr("disabled","");
		}
	});
});














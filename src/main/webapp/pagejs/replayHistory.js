var initParams={};

var playSpeed=1000;//轨迹回放默认速度
var dataArray;//轨迹回放坐标缓冲数组
var k=1;//回放停顿点
var run_status=1;//1运行，2暂停，3停止
var map_center=true;//地图自动居中

$(document).ready(function(){
	$('#cardid').on({
		focus:function(e){
		   if($(this).val()==this.defaultValue){
			   $(this).removeClass('input_active');
			   $(this).val("");
		   }
	    },
	    blur:function(e){
	    	if($(this).val()==''){
	    		$(this).addClass('input_active');
	    		$(this).val(this.defaultValue);
	    	}
	    }
	});
	
	$("#check").click(function() {
		var reg = /^[1-9][0-9]{5}$/;
		var $cardid = $('#cardid');
		var $cardidVal = $.trim($cardid.val());
		var startTime = $("#startTime").val();
		var endTime = $("#endTime").val();
		var $cardidtip=$('#tips_wrong');
		$cardidtip.parent().css('display','none');
		if($cardidVal==''||$cardidVal=='输入北斗卡号') {
			$cardidtip.html("请输入北斗卡号")
			.parent().css('display','inline-block');
			return false;
		}else{
			if (!reg.test($cardidVal)) {
				$cardidtip.html("卡号开头是由大于'0'的6位数字组成")
				.parent().css('display','inline-block');
				return false;
			}
		}
		$(this).hide();
		$('#checking').show();
		InitMapType("baidu", "destop");
	    getloc($cardidVal,startTime,endTime);
	});
	$('#run,#stop,#pause,#hide,#show,#default').mouseover(function(){
		$(this).addClass('b-hover');
	}).mouseout(function(){
		$(this).removeClass('b-hover');
	});
	$('#speedset').hover(function(){
		$(this).find('button').children('span').addClass('s-ico-hover');
		$(this).find('ul').eq(0).css('display','block');
	},function(){
		$(this).find('button').children('span').removeClass('s-ico-hover');
		$(this).find('ul').eq(0).css('display','none');
	});
	$('#speed-level>li').hover(function(){
		$(this).addClass('speed-hover');
	},function(){
		$(this).removeClass('speed-hover');
	}).click(function(){
		var params=this.id.split('&');
		var restep=parseInt(params[0]);
		var retime=parseInt(params[1]);
		playSpeed=restep;
	});
	
	
	//开始
	$("#run").click(function() {
		run_status=1;
	});
	//暂停
	$("#pause").click(function() {
		run_status=2;
	});
	//停止
	$("#stop").click(function() {
		run_status=3;
	});
	
	$("#default").click(function() {
		playSpeed=1000;
	});
});

var map;//= new BMap.Map('map_canvas');
InitMapType("baidu", "destop");
map.centerAndZoom(new BMap.Point(113.1234, 23.2355), 13);
map.enableContinuousZoom();
var lushu;
var _locspeed = 1000;
//var _speedrate = 1;
//控制速度的两个参数 ,实际速度=restep*(1000/_retime) 米/秒 强烈建议使用整数
/* 速度控制说明
 * 10m/s _restep=1,_retime=100;
 * 50m/s _restep=1,_retime=20;
 * 100m/s _restep=1,_retime=10;
 * 1km/s _restep=10,_retime=10;
 * 5km/s _restep=50,_retime=10;
 * 10km/s _restep=100,_retime=10;
 **/
var _restep=1;
var _retime=100;
var diag=new Dialog();
diag.width=250;
diag.height=150;
diag.InnerHtml="<div style='width:100%;height:270px;background-color:white'><div style='padding-top:120px;font-size:20px'>轨迹数据导入中...</div></div>";
diag.imagespath = 'map/images/';

initParams['rdata'] = "";
function getloc(cardid,startTime,endTime) {
	$.ajax( {
		type:'post',
		url:'queryCardlocreplay.do',
		data:{'cardId':cardid,'startTime':startTime,'endTime':endTime},
		dataType:'json',
		success:function(data){
			$('#check').show();
		   	$('#checking').hide();
			if(data.result.length<=0){
				if(data.message==null){
					cardidtip();
				}else{
					$('#tips_wrong').html(data.message).parent().css('display','inline-block');
				}
				return;
			}
			if(data.length<1){
				cardidtip();
				return;
			}
			$('#tips_wrong').html("");
			var list = data.result;//数据集合
			var paths=list.length;//数据长度
			dataArray=new Array(paths);//转换后的坐标集
			var myP = new BMap.Point(list[0].longitude,list[0].latitude);    //起点
			BMap.Convertor.ERPtranslate(myP,0,list[0],CallbackSetCenterpoit);       //真实经纬度转成百度坐标
			//整组坐标转换
			function changePoints(j){
				var point=new BMap.Point(list[j].longitude,list[j].latitude);//坐标对象
				BMap.Convertor.ERPtranslate(point,0,j,ArrayChangeCallbackpoit); //坐标整组转换
				if(j < paths-1){
		             setTimeout(function(){j++; changePoints(j);},300);
		            }
			}
			//启动转换
			setTimeout(function(){
					changePoints(0);
	        	},500);
			
			
			//回放函数
			function replayMkPoint(i){
					if(run_status==2){
					 i--;//暂停
					}
					else if(run_status==3){
						return;//结束
					}
					else{
						//坐标对象
						if(dataArray[i]!=null){
							var point=dataArray[i];
							if(mycheangePoit==null){
								mycheangePoit=point;
							}else{
								if(point.lat!=0&&point.lng!=0){
									var a=(map.getDistance(mycheangePoit,point)).toFixed(2);//计算两点距离
									s_mileage=(parseFloat(s_mileage)+parseFloat(a)).toFixed(2);//计算距离起点的距离
									var polyline = new BMap.Polyline([mycheangePoit,point], {strokeColor:"red", strokeWeight:3, strokeOpacity:0.5});
									var location=list[i];
//									myIcon.setImageUrl(getVehicPathByLocation(location));//设置图标路径
//									carMk.setIcon(myIcon);
									carMk.setPosition(point);
									map.addOverlay(polyline);
									 /*速度Label*/
									speedLabel.setContent(location.createdtime);
//									speedLabel.setPosition(point);
									carMk.setLabel(speedLabel);
									
									
									mycheangePoit=point;
									 //此处应该新增信息点  images/map/sred.png
									var pngUrl="map/blue.png";
									var myIcon_1 = new BMap.Icon(pngUrl, new BMap.Size(28,20));
									var baidurunmarker_1 = new BMap.Marker(point,{icon:myIcon_1});  // 创建标注
									map.addOverlay(baidurunmarker_1); 
									 //标志点弹窗内容
									gehtml=["<div class='infoBoxContent' style='padding-bottom:10px;'><div class='infoBoxtitle' style='color: #FFF;'><strong>盒子ID</strong><span>("+location.boxSerialNumber+")</span></div>"
									         ,"<div class='infoBoxlist' style='padding-top:5px; height: 100px;'><ul><li><div class='infoBoxleft' style='width:100%'><span>时间：</span><span>"+location.createdtime+"</span></div></li>"
									         ,"<li><div class='infoBoxleft'><span>经度：</span><span>"+ location.longitude+"</span></div><div class='infoBoxleftdata'><span>纬度：</span><span>"+location.latitude+"</span></div></li>"
											 ,"<li><div class='infoBoxleft'><span>高程：</span><span>"+location.altitude+"m</span></div></li>"
											 ,"</ul></div>"
											 ,"</div>"];
									var infoBox_1 = new BMapLib.InfoBox( map,gehtml.join( "" ), {
										boxStyle : {
											background : "url('map/tipbox.gif') no-repeat center top",
											width : "293px",
											height : "145px"
										},
										closeIconMargin : "7px 7px 0 0",
										enableAutoPan: true,
										align:INFOBOX_AT_TOP
									  });
										 baidurunmarker_1.addEventListener("click", function (){//本事件在标注被点击时出发，弹出Infobox
										      infoBox_1.open( baidurunmarker_1 );
												});
										 baidurunmarker_1.addEventListener("remove", function (){//本事件在标注被移除时出发，关闭Infobox,移出时主要发生在标注的位置发生改变时
									  });
								}
							}
						}else{
								i--;
								k++;
								if(playSpeed>900){
									//等待2s
									if(k>2){
										i++;
										k=1;
									}
								}
								else if(playSpeed>500){
									//等待2s
									if(k>4){
										i++;
										k=1;
									}
								}
								else if(playSpeed>300){
									//等待2s
									if(k>6){
										i++;
										k=1;
									}
								}
								else if(playSpeed>150){
									//等待2s
									if(k>12){
										i++;
										k=1;
									}
								}
								else if(playSpeed>100){
									//等待2s
									if(k>18){
										i++;
										k=1;
									}
								}
								else {
									//等待2s
									if(k>40){
										i++;
										k=1;
									}
								}
							}
						
						if(map_center){
							//假如地图放大级别大于14,才采取自动居中
							if(map.getZoom()>14&&map.getZoom()<20){
								if(i%10==0&&mycheangePoit!=null){
									map.setCenter(mycheangePoit);
								}
							}
						}
					}
					
					if(i < paths-1){
			             setTimeout(function(){i++; replayMkPoint(i);},playSpeed);
			            }else{
			            	//回放完毕，更改终点图标
			            	var endicon=new BMap.Icon("map/end.png",new BMap.Size(30,30),{
			            		anchor: new BMap.Size(15, 15),
			            		imageOffset: new BMap.Size(0, 0) // 设置图片偏移
			            	 });
			            	carMk.setIcon(endicon);
			            	speedLabel.setContent("终点");
			            	carMk.setLabel(speedLabel);
			            }
					
		           };
		   //启动回放函数        
		   setTimeout(function(){
			   replayMkPoint(1);
		        },5000);
		}
	});
}


var mycheangePoit;
var carMk;
var myIcon; 
var speedLabel;
//回放第一个点时加载
CallbackSetCenterpoit=function(point,location){
	map.centerAndZoom(point, 15);
	mycheangePoit=point;
//	myIcon= new BMap.Icon(getVehicPathByLocation(location), new BMap.Size(50,50),{
//		anchor: new BMap.Size(25, 25),
//		imageOffset: new BMap.Size(0, 0) // 设置图片偏移
//	 });
//	carMk = new BMap.Marker(point,{icon:myIcon});  // 创建标注
	carMk = new BMap.Marker(point);  // 创建标注
	 /*起点图标*/
	var starticon=new BMap.Icon("map/start.png",new BMap.Size(30,30));
	 /*起点标注*/
	var bpstart=new BMap.Marker(point,{icon:starticon});
	map.addOverlay(bpstart);
	 /*速度Label*/
	startlabel = new BMap.Label("起点",{offset:new BMap.Size(20,-10)});
    bpstart.setLabel(startlabel);
    speedLabel = new BMap.Label(location.createdtime,{offset:new BMap.Size(20,-10)});
	map.addOverlay(carMk);
	s_mileage=0;//距离起点距离
	 //标志点弹窗内容
	gehtml=["<div class='infoBoxContent' style='padding-bottom:10px;'><div class='infoBoxtitle' style='color: #FFF;'><strong>盒子ID</strong><span>("+cardNum+")</span></div>"
	         ,"<div class='infoBoxlist' style='padding-top:5px; height: 100px;'><ul><li><div class='infoBoxleft' style='width:100%'><span>时间：</span><span>"+location.createdtime+"</span></div></li>"
	         ,"<li><div class='infoBoxleft'><span>经度：</span><span>"+ location.longitude+"</span></div><div class='infoBoxleftdata'><span>纬度：</span><span>"+location.latitude+"</span></div></li>"
			 ,"<li><div class='infoBoxleft'><span>高程：</span><span>"+location.altitude+"m</span></div></li>"
			 ,"</ul></div>"
			 ,"</div>"];
	var infoBox_1 = new BMapLib.InfoBox( map,gehtml.join( "" ), {
		boxStyle : {
			background : "url('map/tipbox.gif') no-repeat center top",
			width : "293px",
			height : "145px"
		},
		closeIconMargin : "7px 7px 0 0",
		enableAutoPan: true,
		align:INFOBOX_AT_TOP
	  });
	bpstart.addEventListener("click", function (){//本事件在标注被点击时出发，弹出Infobox
		      infoBox_1.open( bpstart );
				});
	bpstart.addEventListener("remove", function (){//本事件在标注被移除时出发，关闭Infobox,移出时主要发生在标注的位置发生改变时
	  });
}


//整组坐标转换
ArrayChangeCallbackpoit=function(point,i){
	dataArray[i]=point;
}

//获取小车图标路径
function getVehicPathByLocation(location){
	 var direction=location.direction;//方向
	 var Pdirection=getDirectionByLocation(direction);//图片方向
	 var pictureUrl="images/car/car-red-"+Pdirection+".png";
	 return pictureUrl;
}
//图片方向函数
function getDirectionByLocation(direction) {
	if (direction>=270+22.5 || direction<=22.5){ 
		return "n";
	}else if (direction>=22.5 && direction<=45+22.5){
		return "en";
	}else if (direction>=45+22.5 && direction<=90+22.5){
		return "e";
	}else if (direction>=90+22.5 && direction<=180-22.5){
		return "es";
	}else if (direction>=180-22.5 && direction<=180+22.5){
		return "s";
	}else if (direction>=180+22.5 && direction<=225){
		return "ws";
	}else if (direction>=225 && direction<=270-22.5){
		return "w";
	}else if (direction>=270-22.5 && direction<=270+22.5){
		return "wn";
	}else{
		return "n";
	}
}

//轨迹回放图片方向函数
function getDirectionByReplayLocation(direction) {
	if (direction>=270+22.5 || direction<=22.5){ 
		return "bluen";
	}else if (direction>=22.5 && direction<=45+22.5){
		return "bluene";
	}else if (direction>=45+22.5 && direction<=90+22.5){
		return "bluee";
	}else if (direction>=90+22.5 && direction<=180-22.5){
		return "bluese";
	}else if (direction>=180-22.5 && direction<=180+22.5){
		return "blues";
	}else if (direction>=180+22.5 && direction<=225){
		return "bluesw";
	}else if (direction>=225 && direction<=270-22.5){
		return "bluew";
	}else if (direction>=270-22.5 && direction<=270+22.5){
		return "bluenw";
	}else{
		return "bluen";
	}
}


//绘制信息窗口
function showLocation(location){
	var content = '<div style="margin:0;line-height:20px;padding:2px;">' +
		'经度：'+location.longitude+'<br/>'+
		'纬度：'+location.latitude+'<br/>'+
		'时间：'+location.locationTime+'<br/>'+
		'速度：'+location.speed+'<br/>'+
		'方向：'+getDirectionLanguage(location.direction)+'<br/>'+
   '</div>';
	var searchInfoWindow = new BMapLib.SearchInfoWindow(map,content,{title:location.car_id,width:170,height:145});
	return searchInfoWindow;
}

//文字方向函数
function getDirectionLanguage(direction) {
	if (direction>=270+22.5 || direction<=22.5){ 
		return "正北";
	}else if (direction>=22.5 && direction<=45+22.5){
		return "东北";
	}else if (direction>=45+22.5 && direction<=90+22.5){
		return "正东";
	}else if (direction>=90+22.5 && direction<=180-22.5){
		return "东南";
	}else if (direction>=180-22.5 && direction<=180+22.5){
		return "正南";
	}else if (direction>=180+22.5 && direction<=225){
		return "西南";
	}else if (direction>=225 && direction<=270-22.5){
		return "正西";
	}else if (direction>=270-22.5 && direction<=270+22.5){
		return "西北";
	}else{
		return "正比";
	}
}

function cardidtip(){
	$('#tips_wrong').text('无该卡号的相关位置信息')
	.parent().css('display','inline-block');
}

function LongLat() {
	var Ldiag = new Dialog();
	var tips = "<div style='text-align:left;margin:10px 28px;'><p style='padding:0;font-size:12px;color:blue;'><a style='color:red;font-size:14px'>*</a>说明：地图上卡号为\"000000\"的点就是您输入坐标的位置 </p><p style='padding:0;color:blue;font-size:12px;'>坐标示例：经度 113.123456,纬度 23.123456</p><div>";
	Ldiag.Width=300;
	Ldiag.Height=185;
	Ldiag.imagespath = './map/images/';
	Ldiag.Title = "输入坐标在地图上定位";
	Ldiag.InnerHtml = '<div style="padding-top:20px">经度:<input id="longi" type="text" /></div><div style="padding-top:20px">纬度:<input id="lati"  type="text" /></div>' + tips;
	Ldiag.OKEvent = function() {
		var longitude = $("#longi").val();
		var latitude = $("#lati").val();
		addnewterloc("0", "0"/* string类型，报警标志,报警定位:1,其他:0 */,
				"坐标"/* 定位类型,string表示,GPS/RD/RN */, "离线"/* string表示，GSM状态 */,
				"离线"/* string表示，北斗状态 */
				, longitude/* string表示，经度 */, latitude/* string表示，纬度 */,
				"0"/* string表示，高程 */, "0"/* string表示，速度 */,
				0/* string表示，方向 */, "坐标定位"/* string表示，定位时间 */,
				"000000"/* string表示，卡号 */, "map/"/* string表示，显示图标路径 */);
		map.setZoom(18);
		TakeCenter(longitude, latitude);
		Ldiag.close();//点击确定后调用的方法
	};
	Ldiag.show();
}
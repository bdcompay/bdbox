/**************************************其它******************************************/
//输入提示
//$.getJSON("queryUsername.do", function(data){
	//$("#username").autocomplete({
		//source:[data.aaData]
	//}); 
//});

/**************************************其它******************************************/
/**************************************DataTable******************************************/
$(function() {
					//DataTable
					var oTable = $('.messageTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'SdkUserSend.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "fromUser",
																	"bSortable" : false,
																	"fnRender" : function(obj) {
																		var fromUser = obj.aData['fromUser'];
																		var result;
																		 if(fromUser==null){
																			 result="null";
																		 }else{
																			 result=fromUser; 
																		 }
																		return result;
																	}
																},
																{
																	"sWidth" : "80px",
																	"sClass" : "center",
																	"mDataProp" : "boxSerialNumber",
																	"bSortable" : false
																},
																{
																	"sWidth" : "80px",
																	"sClass" : "center",
																	"mDataProp" : "msgId",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "content",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "createdTime",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "sendTime",
																	"bSortable" : false
																},
																{
																	"sWidth" : "110px",
																	"sClass" : "center",
																	"mDataProp" : "isReceived",
																	"bSortable" : false,
																	"fnRender" : function(obj) {
																		var isReceived = obj.aData['isReceived'];
																		var result;
																		 if(isReceived=='1'){
																			 result="是";
																		 }else{
																			 result="否"; 
																		 }
																		return result;
																	}
																},
																{
																	"sWidth" : "110px",
																	"sClass" : "center",
																	"mDataProp" : "isRead",
																	"bSortable" : false,
																	"fnRender" : function(obj) {
																		var isRead = obj.aData['isRead'];
																		var result;
																		 if(isRead=='1'){
																			 result="是";
																		 }else{
																			 result="否"; 
																		 }
																		return result;
																	}
																},{
																	"sWidth" : "69px",
																	"sClass" : "opera",
																	"mDataProp" : "manager",
																	"bSortable" : false,
																	"fnRender" : function(obj) {
																		var id = obj.aData['id'];
																		var result = "<a class=\"btn btn-large btn-danger\" href=\"javascript:if(confirm('确认要删除吗？'))location=\'deleteSdkUserSend.do?id="+id+"\'\">"
																					+"<i class=\"halflings-icon trash white\"></i>"
																					+"</a>";
																		return result;
																	}
 																	
																}]
													}));

					
					$("#mycheck").click(function test() {
						oTable.fnDraw();
					});
					
					
					
					
					
					
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	var fromboxSerialNumber = $("#fromboxSerialNumber").val(); 
	//var toboxSerialNumber = $("#toboxSerialNumber").val()
	//var familyMob = $("#familyMob").val();
	//var familyMail = $("#familyMail").val();
	//var boxName = $("#boxName").val();
	//var userName = $("#userName").val();
	var startTimeStr = $("#startTimeStr").val();
	var endTimeStr = $("#endTimeStr").val();

	//var endTimeStr = $("#endTimeStr").val();
	aoData.push({
		"name" : "fromboxSerialNumber",
		"value" : fromboxSerialNumber
	},  {
		"name" : "startTimeStr",
		"value" : startTimeStr
	},{
		"name" : "endTimeStr",
		"value" : endTimeStr
	});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/**************************************DataTable******************************************/
$(".form_date").datetimepicker({
	language: "zh-CN",
	weekStart: 1,
	todayBtn: 1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	minView: 2,
	forceParse: 0,
	format: "yyyy/mm/dd"
});
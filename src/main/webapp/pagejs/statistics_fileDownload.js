statistic_regnumber();
		function statistic_regnumber(){
			var queryStartTime = $('#queryStartTime').val();
			var queryEndTime = $('#queryEndTime').val();
			var statusInt = $('#downloadStatus').val();
			var filename = $('#filename').val();
			if(queryStartTime=='' || queryEndTime==''){
				queryStartTime = todayToDateStr();
				queryEndTime = tomorrowToDateStr();
			}
			$.ajax({
				type:"POST",
				url:"statisticDownloadNumber.do",
				data:{"startTimeStr":queryStartTime,"endTimeStr":queryEndTime,"statusInt":statusInt,"filename":filename},
				datatype:"json",
				success:function(res){
					if(res.status=="OK"){
						$('#downloadNumber').text(res.result.queryCount);
						$('#todayNumber').text(res.result.todayCount);
						$('#monthNumber').text(res.result.monthCount);
						$('#totalNumber').text(res.result.allCount);
					}else{
						alert("操作失败");
						$('#downloadNumber').text(0);
					}
				},
				error:function(res){
					
				}
			});
		}
		
		$("#statistic").click(function(){
			statistic_regnumber();
		});
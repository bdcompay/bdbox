/**************************************其它******************************************/
 
/**************************************DataTable******************************************/
 $(function() { 
 	 var oTable2 = $('.orderTable').dataTable($.extend(
										dparams,
										{
											"sAjaxSource" : 'listBDCood.do',
											"fnServerData" : retrieveData2,// 自定义数据获取函数
											"aoColumns" : [
													{
														"sWidth" : "10px",
														"sClass" : "center",
														"mDataProp" : "bdcood",
														"bSortable" : false
													},
													{
														"sWidth" : "40px",
														"sClass" : "center",
														"mDataProp" : "type",
														"bSortable" : false,
														"fnRender" : function(obj) {
															var type = obj.aData['type'];
															if(type == "BDM")
																return "北斗码";
															if(type == "BDM-zk")
																return "北斗码折扣券";
															if(type == "BDQ")
																return "北斗券";
															if(type == "BDQ-zk")
																return "北斗券折扣券";
														}
													},
													{
														"sWidth" : "40px",
														"sClass" : "center",
														"mDataProp" : "money",
														"bSortable" : false,
														"fnRender" : function(obj) {
															var money = obj.aData['money'];
															var discount = obj.aData['discount'];
															var dayNum = obj.aData['dayNum'];
															if(money!=null){
																return money;
															}else{
																if(discount!=null)
																	return discount + "折";
																else
																	return "可抵用"+dayNum+"天";
															}
														}
													},
													{
														"sWidth" : "40px",
														"sClass" : "center",
														"mDataProp" : "referees",
														"bSortable" : false
													},
													{
														"sWidth" : "30px",
														"sClass" : "center",
														"mDataProp" : "beizhuname",
														"bSortable" : false
													},
													{
														"sWidth" : "40px",
														"sClass" : "center",
														"mDataProp" : "username",
														"bSortable" : false,
													},
													{
														"sWidth" : "60px",
														"sClass" : "center",
														"mDataProp" : "startTime",
														"bSortable" : false
													},
													{
														"sWidth" : "60px",
														"sClass" : "center",
														"mDataProp" : "endTime",
														"bSortable" : false
													},
													{
														"sWidth" : "40px",
														"sClass" : "center",
														"mDataProp" : "creatTime",
														"bSortable" : false
													},
													{
														"sWidth" : "60px",
														"sClass" : "center",
														"mDataProp" : "isUser",
														"bSortable" : false
													},
													{
														"sWidth" : "40px",
														"sClass" : "center",
														"mDataProp" : "orderNo",
														"bSortable" : false
													},
													{
														"sWidth" : "10px",
														"sClass" : "center",
														"mDataProp" : "hendle",
														"bSortable" : false,
														"fnRender" : function(obj) {
															var id = obj.aData['id'];
															var result="";
															var orderNo = obj.aData['orderNo'];
															if(orderNo==null||orderNo==""){  
															   result = "<a href=\"javascript:void(0);\" onclick=\"updatebdcod("+id+")\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"halflings-icon white edit\"></i>修改</a>";
														    }else{
														       result="已使用";
														    }
															  return result;
														}
													}]
										}));
		
		$("#mycheck").click(function test() {
			oTable2.fnDraw();
		}); 
});
 
// 自定义数据获取函数
function retrieveData2(sSource, aoData, fnCallback) {
	var bdnumber = $("#bdnumber").val();
 	var ordernumber = $("#ordernumber").val();
	var userpeople = $("#userpeople").val();
	var refereespeople = $("#refereespeople").val();
	var refereespeoplename = $("#refereespeoplename").val();
	var bdcoodtype = $("#bdcoodtype").val();

	var isuser = $("#isuser").val();
	if(isuser=="是否已使用"){
		isuser="";
	}
	var startTimeStr = $("#startTimeStr").val();
	var endTimeStr = $("#endTimeStr").val();
	var creantTime = $("#creantTime").val(); 
	aoData.push({
		"name" : "bdnumber",
		"value" : bdnumber
	}, {
		"name" : "ordernumber",
		"value" : ordernumber
	},  {
		"name" : "userpeople",
		"value" : userpeople
	},{
		"name" : "refereespeople",
		"value" : refereespeople
	}, {
		"name" : "isuser",
		"value" : isuser
	},  {
		"name" : "startTimeStrs",
		"value" : startTimeStr
	},{
		"name" : "endTimeStr",
		"value" : endTimeStr
	},  {
		"name" : "creantTime",
		"value" : creantTime
	},{
		"name" : "refereespeoplename",
		"value" : refereespeoplename
	},{
		"name" : "bdcoodtype",
		"value" : bdcoodtype
	});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/**************************************DataTable******************************************/


//生成北斗码的弹出框
function newincreased(){  
	    var tuijianren = $("#tuijianren").val();
		if (tuijianren == null) {
			tuijianren = "";
		}
		$.getJSON("queryUserOptions.do?selectUserId=" + tuijianren
				+ "&userType=ENT&page=1&pageSize=9999", function(data) {
			var d = data;
			if (d != '') {
				var s = $('#tuijianren');
				s.empty(); // 清除select中的 option
				$.each(d, function(index, item) {
					s.append(item);
					$("#tuijianren").trigger("liszt:updated");
				});
			} else {
				alert('初始化推荐人错误！');
				d = '';
			}
		});
		
		$('[data-rel="chosen"],[rel="chosen"]').chosen({
			no_results_text : "没有匹配结果"
		}); 
		 
 }

//改变事件1
function changes(){
	var flag = $("#BDtype").val();
 	if(flag=="BDQ"){ 
 		$("#createTle").text("生成北斗券");
		$(updateRent).css("display","none");
	    $(".dayNum").show();
 	}else if(flag=="BDM"){
 		$("#createTle").text("生成北斗码");
		$(updateRent).css("display","block");
	    $(".dayNum").hide();
	    $(".price").show();
	    $(".discount").hide();
	}else{
		$("#createTle").text("生成北斗码");
		$(updateRent).css("display","block");
	    $(".dayNum").hide();
	    $(".price").hide();
	    $(".discount").show();
	}
}


//改变事件2
function changes2(){
	var tuijianren = $("#tuijianren").val();
	if(tuijianren!=""){
		$("#remarkname").attr("disabled", false);
	}else{
		$("#remarkname").attr("disabled", true);
	}
 	$.post("gettuijianren.do",{"tuijianren":tuijianren},function(res){ 
 		$("#remarkname").val(res.message);
 	},"json");
}

$("#dayNum, #udayNum").keyup(function(){
	var num = $(this).val();
	if(num==0 && num!=""){
		$(this).val(1);
	}
});


//修改弹出框
function updatebdcod(id){ 
	$("#udayNum").val(0);
 	$.post("getbdcood.do",{"id":id},function(res){
		if(res.result.type=="已使用"){
			$("#bdtypeid option[value='USER']").attr("selected",true);
		}else if(res.result.status=="未使用"){
			$("#bdtypeid option[value='NOTUSER']").attr("selected",true);
		}else if(res.result.status=="已过期"){
			$("#bdtypeid option[value='EXCEED']").attr("selected",true);
		}else if(res.result.status=="未过期"){
			$("#bdtypeid option[value='NOTEXCEED']").attr("selected",true);
		}      
		$("#updateBuyStartTimeStr").val(res.result.startTime);
		$("#beid").val(res.result.bdcood); 
		$("#updateBuyEndTimeStr").val(res.result.endTime);
		$("#ids").val(res.result.id); 
		if(res.result.type=="BDQ"){
			$(".udayNum").show();
			$("#editTle").text("编辑北斗券");
			$("#udayNum").val(res.result.dayNum==0?1:res.result.dayNum);
		}else{
			$("#editTle").text("编辑北斗码");
			$(".udayNum").hide();
		}
		
	},"json");
}

//修改北斗码提交事件
function sumits(){
	var id = $("#ids").val();
	var bdcood = $("#beid").val();
	var updateBuyStartTimeStr = $("#updateBuyStartTimeStr").val();
	var updateBuyEndTimeStr = $("#updateBuyEndTimeStr").val();
	var bdtypeid = $("#bdtypeid").val(); 
	var dayNum = $("#udayNum").val();
	 
	if(updateBuyStartTimeStr==""||updateBuyEndTimeStr==""){
		alert("开始购买时间和截止购买日期不能为空！");
		return; 
	}  
	if(bdtypeid.length==0){
		alert("北斗码状态不能为空");
		return false;
	} 
	 
	$.post("updateBDCood.do",{
		"id":id,
		"bdcood":bdcood,
		"updateBuyStartTimeStr":updateBuyStartTimeStr,
		"updateBuyEndTimeStr":updateBuyEndTimeStr,
		"bdtypeid":bdtypeid,
		"dayNum":dayNum
	},function(res){
		if(res.status=="OK"){
			alert("修改成功！");
			window.location.href=window.location.href;
		}else{
			alert("修改失败！");
			return;
		}
	},"json");
}
 
$("#discount").keyup(function(){
	$("#discountError").text("");
	var discount = $(this).val();
	if(discount < 0 || discount >= 10){
		if(discount!=""){
			$("#discountError").text("折扣范围0~10");
			$(this).val(9);
		}
	}
});

$("#saveSubmit").click(function(){  
	var BDtype = $("#BDtype").val();
	var saveBuyStartTimeStr = $("#saveBuyStartTimeStr").val();
	var saveBuyEndTimeStr = $("#saveBuyEndTimeStr").val();
	var count = $("#count").val();
	var price = $("#price").val();
	var tuijianren = $("#tuijianren").val();
	var remarkname = $("#remarkname").val(); 
	var dayNum = $("#dayNum").val();
	var discount = $("#discount").val();
	
	if(BDtype.length==0){
		$("#message").text("北斗码性质不能为空"); 
 		return ;
	}
	if(saveBuyStartTimeStr==""||saveBuyEndTimeStr==""){
		$("#message").text("有效期的开始时间和有效期的结束时间不能为空");
 		return; 
	} 
	if(count.length==0){
		$("#message").text("请输入要生成北斗码的数量"); 
 		return ;
	}  
	if(BDtype=="BDM"){
		if(price.length==0){
			$("#message").text("请输入金额");  
 			return;
		}
		if(price.indexOf(".")>0){
			//分割
			var str = price.split(".");
			//截取
			var xs = str[1].substring(0, 2);
			//拼接
			price = str[0]+"."+xs;
		}
	}else{
		price="";
	} 
	
	if(BDtype=="BDM" || BDtype=="BDM-zk" || BDtype=="BDQ-zk"){
		dayNum = 0;
	}
 
	if(tuijianren!=""){ 
		if(remarkname==""){
 			$("#message").text("请输入推荐人备注名"); 
	 		return;
		}
	}
	if(BDtype=="BDM-zk"){
		if(discount==0 || discount==""){
			$("#message").text("折扣不能为空或0"); 
	 		return;
		}
	}
	if(BDtype=="BDQ"){
		if(dayNum=="" || dayNum==0){
			$("#message").text("抵用天数不能为空或0"); 
	 		return;
		}
	}
	
	$.post("createBDcood.do",{
		"BDtype":BDtype,
		"saveBuyStartTimeStr":saveBuyStartTimeStr,
		"saveBuyEndTimeStr":saveBuyEndTimeStr,
		"count":count,
		"price":price,
		"tuijianren":tuijianren,
		"remarkname":remarkname,
		"dayNum":dayNum,
		"discount":discount,
		"createCondition": "bdbox"
	},function(res){
		if(res.status=="OK"){
			alert(res.message);
			window.location.href=window.location.href;
		}else{
			alert(res.message);
			window.location.href=window.location.href;
		} 
	},"json");
});

 
 //**********************************************************************************************************************

$(".form_date").datetimepicker({
	language: "zh-CN",
	weekStart: 1,
	todayBtn: 1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	minView: 2,
	forceParse: 0,
	format: "yyyy/mm/dd"
});

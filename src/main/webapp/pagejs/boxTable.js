/** ************************************其它***************************************** */
// 输入提示
$.getJSON("listAllUser.do", function(data) {
	$("#updateUser").autocomplete({
		source : [ data.result ]
	});
});

/** ************************************其它***************************************** */
/** ************************************DataTable***************************************** */
$(function() {
	// DataTable
	var oTable = $('#boxTable')
			.dataTable(
					$
							.extend(
									dparams,
									{
										"sAjaxSource" : 'listBox.do',
										"fnServerData" : retrieveData,// 自定义数据获取函数
										"aoColumns" : [
												{
													"sWidth" : "300px",
													"sClass" : "center",
													"mDataProp" : "user",
													"bSortable" : false,
													"fnRender" : function(obj) {
														var user = obj.aData['user']==null?"":obj.aData['user'];
														var idNumber = obj.aData['idNumber']==null?"":obj.aData['idNumber'];
														var name = obj.aData['name']==null?"":obj.aData['name'];
														return "<b>用户：</b>"
																+ user
																+ "<br><b>身份证：</b>"
																+ idNumber
																+ "<br><b>盒子名称：</b>"
																+ name;
													}
												},
												
												{
													"sWidth" : "150px",
													"sClass" : "center",
													"mDataProp" : "boxSerialNumber",
													"bSortable" : false,
													"fnRender" : function(obj) {
														var boxSerialNumber = obj.aData['boxSerialNumber']==null?"":obj.aData['boxSerialNumber'];
														var snNumber = obj.aData['snNumber']==null?"":obj.aData['snNumber'];
														var boxStatusTypeStr = obj.aData['snNumber']==""?"":obj.aData['boxStatusTypeStr'];
														return "<b>盒子ID：</b>"
																+ boxSerialNumber
																+ "<br><b>Sn码：</b>"
																+ snNumber
																+ "<br><b>盒子状态：</b>"
																+ boxStatusTypeStr
													}
												},
												{
													"sWidth" : "150px",
													"sClass" : "center",
													"mDataProp" : "cardNumber",
													"bSortable" : false,
													"fnRender" : function(obj) {
														var cardNumber = obj.aData['cardNumber']==null?"":obj.aData['cardNumber'];
														var freq = obj.aData['freq']==null?"":obj.aData['freq'];
														var cardTypeStr = obj.aData['cardTypeStr']==""?"":obj.aData['cardTypeStr'];
														return "<b>卡号：</b>"
																+ cardNumber
																+ "<br><b>频度：</b>"
																+ freq
																+ "<br><b>类型：</b>"
																+ cardTypeStr;
													}
												},
												
												{
													"sWidth" : "130px",
													"sClass" : "center",
													"mDataProp" : "boxType",
													"bSortable" : false
												},
												
												{
													"sWidth" : "300px",
													"sClass" : "center",
													"mDataProp" : "bluetoothCode",
													"bSortable" : false,
													"fnRender" : function(obj) {
														var bluetoothCode = obj.aData['bluetoothCode']==null?"":obj.aData['bluetoothCode'];
														var blueMacAdress = obj.aData['blueMacAdress']==null?"":obj.aData['blueMacAdress'];
														var cardTypeStr = obj.aData['cardTypeStr']==""?"":obj.aData['cardTypeStr'];
														return "<b>密码：</b>"
																+ bluetoothCode
																+ "<br><b>MAC地址：</b>"
																+ blueMacAdress;
													}
												},
												
												{
													"sWidth" : "330px",
													"sClass" : "center",
													"mDataProp" : "lastLocationTimeStr",
													"bSortable" : false
												},
												
												{
													"sWidth" : "200px",
													"sClass" : "opera",
													"mDataProp" : "manager",
													"fnRender" : function(obj) {
														var id = obj.aData['id'];
														var result = "<a href=\"javascript:void(0);\" onclick=\"update("
																+ id
																+ ")\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"halflings-icon white edit\"></i></a>"
																+ "<a class=\"btn btn-danger\" href=\"javascript:if(confirm('确认要删除吗？'))location=\'deleteBox.do?id="
																+ id
																+ "\'\"><i class=\"halflings-icon trash white\"></i></a>"
														return result;
													},
													"bSortable" : false

												} ]
									}));

	$("#mycheck").click(function test() {
		oTable.fnDraw();
	});

});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	var cardTypeStr = $("#cardTypeStr").val();
	var boxStatusTypeStr = $("#boxStatusTypeStr").val();
	var boxSerialNumber = $("#boxSerialNumber").val();
	var snNumber = $("#snNumber").val();
	var cardNumber = $("#cardNumber").val();
	var boxName = $("#boxName").val();
	var userName = $("#userName").val();

	aoData.push({
		"name" : "cardTypeStr",
		"value" : cardTypeStr
	}, {
		"name" : "boxStatusTypeStr",
		"value" : boxStatusTypeStr
	}, {
		"name" : "boxSerialNumber",
		"value" : boxSerialNumber
	}, {
		"name" : "snNumber",
		"value" : snNumber
	}, {
		"name" : "cardNumber",
		"value" : cardNumber
	}, {
		"name" : "boxName",
		"value" : boxName
	}, {
		"name" : "userName",
		"value" : userName
	});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/** ************************************DataTable***************************************** */
function update(id) {

	$.post("getBox.do", {
		"boxId" : id,
		"systemCode" : "ids2j3jdunjejh2i32jdjalalghywu020mvgwe872e3iu"
	},
			function(res) {
				if (res.result.cardTypeStr == "一级卡") {
					$("#updateCardType option[value='LEVEL_1']").attr(
							"selected", true);
				} else if (res.result.cardTypeStr == "二级卡") {
					$("#updateCardType option[value='LEVEL_2']").attr(
							"selected", true);
				} else if (res.result.cardTypeStr == "三级卡") {
					$("#updateCardType option[value='LEVEL_3']").attr(
							"selected", true);
				} else if (res.result.cardTypeStr == "四级卡") {
					$("#updateCardType option[value='LEVEL_4']").attr(
							"selected", true);
				}

				if (res.result.boxStatusTypeStr == "已激活") {
					$("#updateBoxStatusType option[value='NORMAL']").attr(
							"selected", true);
				} else if (res.result.boxStatusTypeStr == "未激活") {
					$("#updateBoxStatusType option[value='NOTACTIVATED']")
							.attr("selected", true);
				} else if (res.result.boxStatusTypeStr == "禁止") {
					$("#updateBoxStatusType option[value='STOP']").attr(
							"selected", true);
				}

				if (res.result.freq == "60") {
					$("#updateFreq option[value='60']").attr("selected", true);
				} else if (res.result.freq == "20") {
					$("#updateFreq option[value='20']").attr("selected", true);
				}
				
				if(res.result.boxType=="放牧终端"){
					$("#boxType option[value='GRAZE']").attr("selected", true);
				}else if(res.result.boxType=="一代盒子"){
					$("#boxType option[value='ONE']").attr("selected", true);
				}else if(res.result.boxType=="二代盒子"){
					$("#boxType option[value='TWO']").attr("selected", true);
				}
					
				
				$("#updateId").val(res.result.id);
				$("#updateName").val(res.result.name);
				$("#updateIdNumber").val(res.result.idNumber);
				$("#updateCardNumber").val(res.result.cardNumber);
				$("#updateBoxSerialNumber").val(res.result.boxSerialNumber);
				$("#updateSnNumber").val(res.result.snNumber);
				$("#updateBalance").val(res.result.balance);
				$("#updateUser").val(res.result.user);
				$("#bluetoothCode").val(res.result.bluetoothCode);

				if (res.result.openSosCall == true) {
					$("#isOpneSosCall option[value='1']")
							.attr("selected", true);
				} else {
					$("#isOpneSosCall option[value='0']")
							.attr("selected", true);
				}

				// 获取队友信息
				$.post("getPartner.do", {
					"boxSerialNumber" : res.result.boxSerialNumber,
					"cardNumber" : res.result.cardNumber
				}, function(res) {
					var values = "";
					$.each(res.result, function(key, val) {
						values = values + val.boxSerialNumber;
						if (res.result.length - 1 != key) {
							values = values + ",";
						}
					});
					if (values != "") {
						$("#updatePartner").val(values);
					} else {
						$("#updatePartner").val("暂无队友信息");
					}
				}, "json");
				// 家人
				$.post("getFamily.do", {
					"boxSerialNumber" : res.result.boxSerialNumber,
					"cardNumber" : res.result.cardNumber
				}, function(res) {
					var values = "";
					$.each(res.result, function(key, val) {
						values = values + val.phone;
						if (res.result.length - 1 != key) {
							values = values + ",";
						}
					});
					if (values != "") {
						$("#updateFamily").val(values);
					} else {
						$("#updateFamily").val("暂无家人信息");
					}
				}, "json");

			}, "json");
}

$("#updateSubmit").click(function() {
	var username = $("#updateUser").val();
	var cardTypeStr = $("#updateCardType").val();
	var boxStatusTypeStr = $("#updateBoxStatusType").val();
	var freq = $("#updateFreq").val();
	var name = $("#updateName").val();
	var idNumber = $("#updateIdNumber").val();
	var cardNumber = $("#updateCardNumber").val();
	var boxSerialNumber = $("#updateBoxSerialNumber").val();
	var snNumber = $("#updateSnNumber").val();
	var balance = $("#updateBalance").val();
	var id = $("#updateId").val();
	var bluetoothCode = $("#bluetoothCode").val();
	var isOpneSosCall = $("#isOpneSosCall").val();
	var boxType = $("#boxType").val();

	$.post("update_manage_box.do", {
		"cardTypeStr" : cardTypeStr,
		"boxStatusTypeStr" : boxStatusTypeStr,
		"username" : username,
		"freq" : freq,
		"name" : name,
		"idNumber" : idNumber,
		"cardNumber" : cardNumber,
		"boxSerialNumber" : boxSerialNumber,
		"snNumber" : snNumber,
		"balance" : balance,
		"id" : id,
		"bluetoothCode" : bluetoothCode,
		"isOpenSosCall" : isOpneSosCall,
		"boxType":boxType
	}, function(res) {
		if (res.status == "OK") {
			alert("修改成功！");
			window.location.href = window.location.href;
		} else {
			alert(res.message);
			return;
		}
	}, "json");

});

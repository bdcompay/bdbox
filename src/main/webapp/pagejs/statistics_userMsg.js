/**
 * 统计用户短报文与短信数量
 */

statisticUserMsg();
//统计
function statisticUserMsg(){
	var startTimeStr = $("#startTimeStr").val();
	var endTimeStr = $("#endTimeStr").val();
	if(startTimeStr=='' || endTimeStr==''){
		startTimeStr = todayToDateStr();
		endTimeStr = tomorrowToDateStr();
	}
	var userid = $("#userid").val();
	
	var aaData = {"startTimeStr":startTimeStr,"endTimeStr":endTimeStr,"userid":userid};
	
	$.ajax({
		type:"POST",
		url:"statisticsUserMessage.do",
		data:aaData,
		datatype:"json",
		success:function(res){
			if(res.status=="OK"){
				var result = res.result;
				//按时间条件查询数量
				$("#userSend").text(result.userSend);
				$("#userRev").text(result.userRev);
				$("#toFamily").text(result.toFamily);
				$("#toUser").text(result.toUser);
				//统计今日数量
				$("#todayUserSend").text(result.todayUserSend);
				$("#todayUserRcv").text(result.todayUserRev);
				$("#todayBoxToFamily").text(result.todayToFamily);
				$("#todayFamilyToBox").text(result.todayToUser);
				//统计本月数量
				$("#monthUserSend").text(result.monthUserSend);
				$("#monthUserRcv").text(result.monthUserRev);
				$("#monthBoxToFamily").text(result.monthToFamily);
				$("#monthFamilyToBox").text(result.monthToUser);
				//统计总数量
				$("#totalUserSend").text(result.totalUserSend);
				$("#totalUserRcv").text(result.totalUserRev);
				$("#totalBoxToFamily").text(result.totalToFamily);
				$("#totalFamilyToBox").text(result.totalToUser);
			}else{
				alert("操作失败，"+res.message);
			}
		},
		error:function(e){
			
		}
	});
}

$("#statistic").click(function(){
	statisticUserMsg();
});



/*****************************检索下拉框******************************/
/**
 * 加载检索下拉框
 */
$(function(){
	$('.dept_select').chosen();
});

//是否加载数据标示
var $_flag = false;
//获取用户名称
$(document).on("click",".chzn-container",function(){
	if($_flag) return ;
	var $select = $(this).prev();
	$select.empty();
	$.ajax({
		"type":"POST",
		"url":"getAllUser.do",
		"data":{},
		"datatype":"json",
		"async":true,
		"success":function(res){
			var users = res.result;
			var options = "";
			options+='<option value="0">请选择用户</option>';
			for(var i in users){
				options+='<option value="'+users[i].id+'">'+users[i].username+'</option>';
			}
			$select.append(options);
			$select.trigger("liszt:updated");
			$_flag = true;
		},
		"error":function(e){
			alert("获取用户数据失败，"+e);
		}
	});
});
/**************************************其它******************************************/
 

/**************************************其它******************************************/
/**************************************DataTable******************************************/
$(function() {
					//DataTable
					var oTable = $('.userTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'listFAQtable.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																{
																	"sWidth" : "50px",
																	"sClass" : "center",
																	"mDataProp" : "checbox",
																	"bSortable" : false,
																	"fnRender" : function(obj) {
																		var id = obj.aData['id'];
																		result = "<input type='checkbox' style='text-align:center;' class='box' id='chkSon2' name='chkSon' value='"+id+"' onclick='ChkSonClick('chkSon','chkAll')'></input>";
																		return result;
																	}
																},
																{
																	"sWidth" : "80px",
																	"sClass" : "center",
																	"mDataProp" : "id",
																	"bSortable" : false
																},
																{
																	"sWidth" : "350px",
																	"sClass" : "center",
																	"mDataProp" : "question",
																	"bSortable" : false,
																	"fnRender" : function(obj) {
																		var question = obj.aData['question'];
																		result = '<div style="width:350px;word-wrap:break-word ">'+question+'</div>';
																		return result;
																	}
																}, 
																{
																	"sWidth" : "300px",
																	"sClass" : "center",
																	"mDataProp" : "answer",
																	"bSortable" : false,
																	 "fnRender" : function(obj) {
																		var answer = obj.aData['answer'];
																		result = '<div style="width:300px;word-wrap:break-word ">'+answer+'</div>'
																		return result;
																	} 
																}, 
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "creatTime",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "questionType",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "smallTypename",
																	"bSortable" : false
																},
																{
																	"sWidth" : "80px",
																	"sClass" : "opera",
																	"mDataProp" : "handle",
																	"fnRender" : function(obj) {
																		var id = obj.aData['id'];
 																		var result = "<a href=\"javascript:void(0);\" onclick=\"update("+id+")\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"halflings-icon white edit\"></i>修改</a>"
																					+"<a class=\"btn btn-danger\" href=\"javascript:void(0);\" onclick=\"deleteProduct("+id+")\"><i class=\"halflings-icon trash white\"></i>删除</a>";
  																		return result;
																	},
																	"bSortable" : false
																	
																}]
													}));

					
					$("#mycheck").click(function test() {
						oTable.fnDraw();
					});
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) { 
	var questiontype=$("#questiontype").val();
	aoData.push({
		"name" : "questiontype",
		"value" : questiontype
	});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/**************************************DataTable******************************************/

//删除问题
function deleteProduct(id){
	if(!confirm('确认要删除吗？')){
		return false;
	}
	$.post("deleteFAQ.do",{
		"id":id,
	},function(res){
		if(res.status=="OK"){
			window.location.href=window.location.href;
		}else{
			alert("删除失败！");
			return;
		}
	},"json");
}

//修改问题弹出框js
function update(id){
	var flat="";
 /*	$.post("getFAQ.do",{
		"id":id
	},function(res){  
  		if(res.result.questionType=="常见问题"){
 			$("#updatequestiontype option[value='GENERALQUESTION']").attr("selected",true);
 			flat="GENERALQUESTION";
 		} 
		if(res.result.questionType=='新手指南'){
 			$("#updatequestiontype option[value='NEWGUIDE']").attr("selected",true);
 			flat="NEWGUIDE";
 		} 
		if(res.result.questionType=="户外知识"){
 			$("#updatequestiontype option[value='OUTDOORS']").attr("selected",true);
 			flat="OUTDOORS"; 
		} 
		if(res.result.questionType=="名词解释"){
 			$("#updatequestiontype option[value='NOUNEXPLAIN']").attr("selected",true);
 			flat="NOUNEXPLAIN"; 
		} 
 		$("#updateId").val(id);
		$("#updatequestionname").val(res.result.question);
		$("#updatequestionvlaue").val(res.result.answer);  
	},"json");
     querystype2(flat,'2');*/ 
    $.ajax({
		"type":"POST",
		"url":"getFAQ.do",
		"async":false,
		"data":{"id":id},
		"datatype":"json",
		"success":function(res){
			if(res.result.questionType=="常见问题"){
	 			$("#updatequestiontype option[value='GENERALQUESTION']").attr("selected",true);
	 			flat="GENERALQUESTION";
	 		} 
			if(res.result.questionType=='新手指南'){
	 			$("#updatequestiontype option[value='NEWGUIDE']").attr("selected",true);
	 			flat="NEWGUIDE";
	 		} 
			if(res.result.questionType=="户外知识"){
	 			$("#updatequestiontype option[value='OUTDOORS']").attr("selected",true);
	 			flat="OUTDOORS"; 
			} 
			if(res.result.questionType=="名词解释"){
	 			$("#updatequestiontype option[value='NOUNEXPLAIN']").attr("selected",true);
	 			flat="NOUNEXPLAIN"; 
			} 
	 		$("#updateId").val(id);
			$("#updatequestionname").val(res.result.question);
			$("#updatequestionvlaue").val(res.result.answer);  	 
		} 
	});
     querystype2(flat,'2');  
} 

//修改问题提交按钮
$("#updateSubmit").click(function(){
 	var id = $("#updateId").val();
	var updatequestionname = $("#updatequestionname").val();
	var updatequestionvlaue = $("#updatequestionvlaue").val();  
	var updatequestiontype = $("#updatequestiontype").val();
	var updatesmalltype = $("#updatesmalltype").val();  
	if(updatesmalltype==""){
		$("#updatemessageerror").text("类型不能为空！");
 		return;
	}
	if(updatequestionname.length==0){
		$("#updatemessageerror").text("问题不能为空，请先填写内容！");
 		return;
	}
	if(updatequestionvlaue.length==0){
		$("#updatemessageerror").text("答案不能为空，请填先填写答案");
 		return;
	}
	if(updatequestionvlaue.length>300){
		var length=updatequestionvlaue.length;
		var num=length-300;
		$("#infomation").text("答案不能超过300个字，目前已经超出"+num+"个字！");
 		return;
	} 
	$.post("updateFAQ.do",{
		"id":id,
		"updatequestionname":updatequestionname,
		"updatequestionvlaue":updatequestionvlaue,
		"updatequestiontype":updatequestiontype,
		"updatesmalltype":updatesmalltype
	},function(res){
		if(res.status=="OK"){
			alert("修改成功！");
			window.location.href=window.location.href;
		}else{
			alert("修改失败！");
			return;
		}
	},"json");
}); 

//新增问题确定按钮
$("#saveSubmit").click(function(){
 	var questionname = $("#questionname").val();
	var questionvlaue = $("#questionvlaue").val(); 
	var addquestiontype = $("#addquestiontype").val();
	var addsmalltype = $("#addsmalltype").val(); 
 	if(questionname.length==0){
		$("#savainfomation").text("问题不能为空！");
 		return;
	}
	if(questionvlaue.length==0){
		$("#savainfomation").text("答案不能为空！");
 		return;
	} 
	if(questionvlaue.length>300){
		var length=questionvlaue.length;
		var nun=length-300;
		$("#savainfomation").text("答案不能超过300字，目前已经超出"+nun+"个字！！");
 		return;
	} 
	$.post("addFAQ.do",{
		"questionname":questionname,
		"questionvlaue":questionvlaue,
		"questiontype":addquestiontype,
		"addsmalltype":addsmalltype,
		"num":"new"
	},function(res){
		if(res.status=="OK"){
			alert("添加成功！");
			window.location.href=window.location.href;
		}else{
			alert("添加失败！");
			return;
		}
	},"json");
});


//新增类型提交按钮
$("#saveaddtype").click(function(){
 	var addtype = $("#addtype").val();
	var Englishtypename = $("#Englishtypename").val(); 
	var chaintypename = $("#chaintypename").val();
 	if(Englishtypename.length==0){
		$("#addmessageerrortype").text("类型的英文名称不能为空！");
 		return;
	}
	if(chaintypename.length==0){
		$("#addmessageerrortype").text("新增类型的中文名称不能为空！");
 		return;
	} 
	var name=Englishtypename.toUpperCase();
	$.post("addFAQtype.do",{
		"addtype":addtype,
		"Englishtypename":name,
		"chaintypename":chaintypename
	},function(res){
		if(res.status=="OK"){
			alert("添加成功！");
			window.location.href=window.location.href;
		}else{
			alert("添加失败！");
			return;
		}
	},"json");
});

//新增FAQ问题
function newincreased(){
 	querystype2('GENERALQUESTION','1');  
} 

//显示和隐藏下拉框
function newtype(){
	$("#type1").show();
	$("#type2").hide();
	$("#type3").hide();
	$("#type4").hide();
 	querystype('GENERALQUESTION','1'); 
}

//查询所属归类的六个小类
function querystype(typeid,num){
    if(typeid==null){
    	typeid="";
    }
	$.getJSON("queryTypeOptions.do?updatetype=" + typeid + "&userType=ENT&page=1&pageSize=2000", function(data) {
  		var d = data;
  		var s;
		if (d != '') {
			if(num=='1'){
				s = $('#type_1');
			}else if(num=='2'){
				s = $('#type_2'); 
			}else if(num=='3'){
				s = $('#type_3'); 
			}else if(num=='4'){
				s = $('#type_4'); 
			}  
			s.empty(); // 清除select中的 option
			$.each(d, function(index, item) {
				s.append(item);
				if(num=='1'){
					$("#type_1").trigger("liszt:updated");
				}else if(num=='2'){
					$("#type_2").trigger("liszt:updated");
				}else if(num=='3'){
					$("#type_3").trigger("liszt:updated");
				}else if(num=='4'){
					$("#type_4").trigger("liszt:updated");
				}  
 			});
		} else {
			alert('初始化类型错误！');
			d = '';
		}
	});
 	
	if(num=='1'){
		$('[data-rel="chosen1"],[rel="chosen1"]').chosen({
			no_results_text : "没有匹配结果"
		});
 	}else if(num=='2'){
 		$('[data-rel="chosen2"],[rel="chosen2"]').chosen({
 			no_results_text : "没有匹配结果"
 		}); 
 	}else if(num=='3'){
 		$('[data-rel="chosen3"],[rel="chosen3"]').chosen({
 			no_results_text : "没有匹配结果"
 		});
 	}else if(num=='4'){
 		$('[data-rel="chosen4"],[rel="chosen4"]').chosen({
 			no_results_text : "没有匹配结果"
 		});
 	} 
}


//改变事件1
function changes(){
	var flag = $("#updatetype").val();
  	if(flag=="GENERALQUESTION"){ 
 		$("#type1").show();
 		$("#type2").hide();
 		$("#type3").hide();
 		$("#type4").hide();
  		querystype(flag,'1'); 
 	}else if(flag=="NEWGUIDE"){
 		$("#type1").hide();
 		$("#type2").show();
 		$("#type3").hide();
 		$("#type4").hide();
  		querystype(flag,'2'); 
 	}else if(flag=="OUTDOORS"){
 		$("#type1").hide();
 		$("#type2").hide();
 		$("#type3").show();
 		$("#type4").hide();
  		querystype(flag,'3');  
 	}else if(flag=="NOUNEXPLAIN"){
 		$("#type1").hide();
 		$("#type2").hide();
 		$("#type3").hide();
 		$("#type4").show();
  		querystype(flag,'4');  
 	}  
}
//改变事件2
function onchange2(){
	var flag = $("#addquestiontype").val();
	querystype2(flag,'1');
}
//改变事件1
function onchange3(){
	var flag = $("#updatequestiontype").val();
	querystype2(flag,'2');
}


function querystype2(type,nums){
 	if(type==null){
		type="";
	} 
	$.getJSON("queryTypeOptions.do?updatetype=" + type
			+ "&userType=ENT&page=1&pageSize=2000", function(data) {
		var d = data;
		if (d != '') {
			var s;
			if(nums=="1"){
				s = $('#addsmalltype'); 
			}else{
				s = $('#updatesmalltype');
			}
 			s.empty(); // 清除select中的 option
			$.each(d, function(index, item) {
				s.append(item);
				if(nums=="1"){
					$("#addsmalltype").trigger("liszt:updated");
				}else{
					$("#updatesmalltype").trigger("liszt:updated");
				}
 			});
		} else {
			alert('初始化推荐人错误！');
			d = '';
		}
	});  
	if(nums=="1"){
		$('[data-rel="chosen0"],[rel="chosen0"]').chosen({
			no_results_text : "没有匹配结果"
		});  
	}else{
		$('[data-rel="chosen5"],[rel="chosen5"]').chosen({
			no_results_text : "没有匹配结果"
		}); 
	} 
}
 

//修改问题提交按钮
$("#saveSubmittype").click(function(){
 	var updatetype = $("#updatetype").val();
   	var updatetypename = $("#updatetypename").val(); 
 	var type= "";
 	if(updatetype=="GENERALQUESTION"){
 		type=$("#type_1").val(); 
	}
 	if(updatetype=="NEWGUIDE"){
 		type=$("#type_2").val(); 
	} 
 	if(updatetype=="OUTDOORS"){
 		type=$("#type_3").val(); 
	} 
 	if(updatetype=="NOUNEXPLAIN"){
 		type=$("#type_4").val(); 
	}  
	if(updatetypename.length==0){
		$("#messageerrortype").text("修改名称不能为空！");
 		return;
	}
	 
	$.post("updateFAQtype.do",{ 
		"updatetype":updatetype,
		"type":type,
		"updatetypename":updatetypename 
	},function(res){
		if(res.status=="OK"){
			alert("修改成功！");
			window.location.href=window.location.href;
		}else{
			alert("修改失败！");
			return;
		}
	},"json");
}); 

//--列头全选框被单击---
function ChkAllClick(chkSon, chkAll){
 var arrSon = document.getElementsByName(chkSon);
 var cbAll = document.getElementById(chkAll);
 var tempState=cbAll.checked;
 for(var i=0;i<arrSon.length;i++) {
  if(arrSon[i].checked!=tempState)
           arrSon[i].click();
 }
}

//--子项复选框被单击---
function ChkSonClick(chkSon, chkAll) {
 var arrSon = document.getElementsByName(chkSon);
 var cbAll = document.getElementById(chkAll);
 for(var i=0; i<arrSon.length; i++) {
     if(!arrSon[i].checked) {
     cbAll.checked = false;
     return;
     }
 }
 cbAll.checked = true;
}  

//批量删除
function deletes(){ 
	var id="";
	var sel=document.getElementsByName("chkSon"); 
	for( var i=0;i<sel.length;i++){ 
	if(sel[i].checked==true)
	id+=sel[i].value+",";
	}
	if(id==""){
	alert("请至少选择一条记录");
	return false;
	}
	if(window.confirm("确定删除吗？")){
		$.ajax({
			"type" : "GET",
			"url" : "deletefaq.do",
			"dataType" : "json",
			"data" : "id=" +id,
			"success" : function(data) {
				if (data.result == true) {
					alert("操作成功");
					window.location.href=window.location.href;
				} else {
					alert("操作失败");
					window.location.href=window.location.href; 
				}

			},
			"error" : function(resp) {
				alert("操作失败");
			}
		});
		} 
}

//调至用户提交问题的后台管理页面
function link(){
	window.location.href="table_userquestion.html";
}
 
 
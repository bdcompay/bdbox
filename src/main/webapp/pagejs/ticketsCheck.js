$(function() {
 	var oTable = $('.userTable')
			.dataTable(
					$
						 .extend(
							 	dparams, {
										"sAjaxSource" : 'getQualificationCheckAll.do',
										"fnServerData" : retrieveData,// 自定义数据获取函数
										"aoColumns" : [
												{
													"sWidth" : "50px",
													"sClass" : "center",
													"mDataProp" : "id",
													"bSortable" : false
												},
												{
													"sWidth" : "200px",
													"sClass" : "center",
													"mDataProp" : "businessLicense",
													"bSortable" : false,
													"fnRender" : function(obj) {
														var businessLicense = obj.aData['businessLicense'];
														return "<img width='200' height='200' src='"+businessLicense+"'>";
													}
												}, 
												{
													"sWidth" : "200px",
													"sClass" : "center",
													"mDataProp" : "taxReg",
													"bSortable" : false,
													"fnRender" : function(obj) {
														var taxReg = obj.aData['taxReg'];
														return "<img width='200' height='200' src='"+taxReg+"'>";
													}
												},
												{
													"sWidth" : "200px",
													"sClass" : "center",
													"mDataProp" : "orgCode",
													"bSortable" : false,
													"fnRender" : function(obj) {
														var orgCode = obj.aData['orgCode'];
														return "<img width='200' height='100' src='"+orgCode+"'>";
													}
												},
												{
													"sWidth" : "200px",
													"sClass" : "center",
													"mDataProp" : "generalTax",
													"bSortable" : false,
													"fnRender" : function(obj) {
														var generalTax = obj.aData['generalTax'];
														return "<img width='200' height='200' src='"+generalTax+"'>";
													}
												},
												{
													"sWidth" : "200px",
													"sClass" : "center",
													"mDataProp" : "invoiceDatum",
													"bSortable" : false,
													"fnRender" : function(obj) {
														var invoiceDatum = obj.aData['invoiceDatum'];
														return "<img width='166' src='"+invoiceDatum+"'>";
													}
												},
												{
													"sWidth" : "200px",
													"sClass" : "center",
													"mDataProp" : "busLicStatus",
													"bSortable" : false,
													"fnRender" : function(obj) {
														var busLicStatus = obj.aData['busLicStatus'];
														var taxRegStatus = obj.aData['taxRegStatus'];
														var orgCodeStauts = obj.aData['orgCodeStauts'];
														var generalTaxStatus = obj.aData['generalTaxStatus'];
														var invoiceDatumStatus = obj.aData['invoiceDatumStatus'];
														var busLicRemark = obj.aData['busLicRemark'];
														var taxRegRemark = obj.aData['taxRegRemark'];
														var orgCodeRemark = obj.aData['orgCodeRemark'];
														var generalTaxRemark = obj.aData['generalTaxRemark'];
														var invoiceDatumRemark = obj.aData['invoiceDatumRemark'];
														if(busLicStatus && taxRegStatus && orgCodeStauts && generalTaxStatus && invoiceDatumStatus){
															return "审核通过";
														}else if(busLicRemark!=null || taxRegRemark!=null || orgCodeRemark!=null || generalTaxRemark!=null || invoiceDatumRemark!=null){
															if(!busLicStatus || !taxRegStatus || !orgCodeStauts || !generalTaxStatus || !invoiceDatumStatus)
																return "审核不通过";
														}else{
															return "待审核";
														}
													}
												},
												{
													"sWidth" : "400px",
													"sClass" : "opera",
													"mDataProp" : "createTime",
													"fnRender" : function(obj) {
														var id = obj.aData['id'];
														var result = 
															"<a class=\"btn btn-primary\" href=\"table_checkDetail.html?id="+id+"\">查看详情</a>"; 																		 
														return result;
													},
													"bSortable" : false 
												}]
									}));

	$("#mycheck").click(function test() {
		oTable.fnDraw();
	});
					
});

//自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
 

	aoData.push();
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);

			$('td img').zoomify();
		},
		"error" : function(resp) {
		}
	});

}
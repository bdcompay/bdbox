

	$(document).ready(function(){
		weekselect();
		
		$.jqplot.config.enablePlugins = true;
		
		//选择时间
		$(document).on("focus","#d121",function(){
			//onfocus="WdatePicker()"
			WdatePicker({
				isShowWeek:true,
				readOnly:true,
				onpicked:weekselect,
				errDealMode:3
			});
		});
		
		//分页选中点击页码
		$(document).on("click","#paginate span a",function(){
			var currpage = $("#paginate span .paginate_active").text();
			var clickpage = $(this).text();
			if(currpage == clickpage){
				return ;
			}
			chart(clickpage);
			//alert("当前显示页面"+page+",选中点击"+$(this).text()+"页码");
		});
		//首页
		$(document).on("click","#first",function(){
			chart(1);
		});
		//上一页
		$(document).on("click","#previous",function(){
			var page = $("#paginate span .paginate_active").text();
			page == 1?chart(1):chart(page - 1);
		});
		//下一页
		$(document).on("click","#next",function(){
			var page = $("#paginate span .paginate_active").text();
			page==endpage?chart(endpage):chart(parseInt(page)+1);
		});
		//尾页
		$(document).on("click","#last",function(){
			var page = $("#paginate span .paginate_active").text();
			chart(endpage);
		});
	});
	
	
	/***************************** 时间处理 **************************************/
		
		//格式化日期：yyyy-MM-dd
	    function formatDate(date) {
	        var myyear = date.getFullYear();
	        var mymonth = date.getMonth()+1;
	        var myweekday = date.getDate();

	        if(mymonth < 10){
	            mymonth = "0" + mymonth;
	        }
	        if(myweekday < 10){
	            myweekday = "0" + myweekday;
	        }
	        return (myyear+"-"+mymonth + "-" + myweekday);
	    }
		
		/* 
		 * 获取日期是本年的第几周
		 *（1）一周的起始日为周日，最后一天是周六
	 	 *（2）以1.1所在周为一年的第一周
	 	 */
	 	function getWeek(date){
			//年
			var year = date.getFullYear();
			//获取本年1月1号
			var start = new Date(year,0,1);
			//获取本年1月1号是一周中的第几天
	 		var day = start.getDay()+1;
			//获取今天是本年中的第几天
	 		var dayNumber = Math.ceil((date-start)/(24*60*60*1000));
	 		//获取今天是本年的第几周
	 		var week;
	 		if(day>0){
	 			week=Math.ceil((dayNumber-7+day)/7)+1;
	 		}else{
	 			week=Math.ceil((dayNumber-day)/7);
	 		}
	 		//本年最后一天
	 		var last = new Date(year,11,31);
	 		//本年最后一天是星期几
	 		var lday = last.getDay();
	 		//今天离最后一天还有几天
	 		var tolastday = Math.ceil((last-date)/(24*60*60*1000));
	 		//判断本周是否有一天在下一年
	 		if(tolastday<=lday && lday<6){
	 			week = 1;
	 		}
	 		return week;
		}
	 	
	 	/*
		 * 获取年周
		 */
		function getWeekName(date){
			//获取周
			var weekNumber = getWeek(date); 
			//获取年
			var year = date.getFullYear();
			var month = date.getMonth();
			if(month>0 && weekNumber==1){
				++year;
			}
			var weekname = year+"W"+weekNumber;
			return weekname;
		}
		
		//通过周获取时间段内的时间
		function getDateFromWeek(yearweek){
			//yearWeek格式2016W1:2016年第一周
			var arr = yearweek.split("W");
			if(arr.length<2) return ; 
			var year = arr[0];
			var week = arr[1];
			if(year>1970 && year<2100){
				//获取本年1月1号
				var start = new Date(year,0,1);
				//获取本年1月1号是一周中的第几天
		 		var day = start.getDay();
				//本年的开始时间+加上周的时间=具体时间
				var date = new Date((start/1000+(week-1)*7*24*60*60)*1000);
				return date;
			}
		}
		
	  	//获取指定时间周的开始时间
		function getWeekStartDate(date){
		    var dayOfWeek = date.getDay();         //今天本周的第几天
		    var day = date.getDate();              //当前日
		    var month = date.getMonth();           //当前月
		    var year = date.getFullYear();         //当前年
		    var getWeekStartDate = new Date(year, month, day - dayOfWeek);
		    return getWeekStartDate;
		}
	  	
	  	//周结束时间
	  	function getWeekEndDate(date){
	  		var dayOfWeek = date.getDay();         //今天本周的第几天
		    var day = date.getDate();              //当前日
		    var month = date.getMonth();           //当前月
		    var year = date.getFullYear();         //当前年
		    var getWeekEndDate = new Date(year, month, day + (6 - dayOfWeek));
		    return getWeekEndDate;
	  	}
		
	  	
	/******************************* 图表 **************************************/
	endpage = 1,weekName = "";
	//画图
	function chart(page){
		//ajax获取统计数据
		var url = "getEntWeekStatDate.do";
		var pageSize = 10;
		var data = {"weekName":weekName,"page":page,"pageSize":10};
		$.ajax({
			"type":"POST",
			"url":url,
			"data":data,
			"datatype":"json",
			"success":function(res){
				$("#chart1").empty();
				if(res.status=="OK"){
					pageNumber(res.message,page,pageSize)
					var plotdata = chartData(res);
					var options = chartOptions(res);
					$.jqplot('chart1', plotdata, options);
				}else{
					$("#chart1").text(res.message);
				}
			}
		});
		
	}
	
	//尾页
	var endpage = 1;
	//生成页码html
	function pageNumber(count,page,pageSize){
		//计算页数
		var pageNumber = Math.ceil(count/pageSize);
		//获取显示的起始页码和结束页码
		var startpage = 1;
		endpage = 1;
		if(pageNumber>5 && page>2){
			startpage = page-2;
			if(page+2<=pageNumber){
				endpage = page+2;
			}else{
				endpage = page+(pageNumber-page);
				startpage = startpage -(2-(pageNumber-page));
			}
		}else{
			endpage = pageNumber;
		}
		var as = "";
		//成功页码按钮
		for(var i=startpage;i<=endpage;i++){
			if(i==page){
				as += '<a tabindex="0" class="paginate_active">'+i+'</a>';
			}else{
				as += '<a tabindex="0" class="paginate_button">'+i+'</a>';
			}
		}
		$("#paginate span").text('').append(as);
		var st = $("#info").find("strong");
		st.eq(0).text(" "+((page-1)*pageSize+1)+" ");
		if(endpage==page){
			st.eq(1).text(" "+count+" ");
		}else{
			st.eq(1).text(" "+((page-1)*pageSize+pageSize)+" ");
		}
		st.eq(2).text(" "+count+" ");
	}
	
	
	//处理数据为绘图数据
	function chartData(res){
		var data,entSend=new Array(10);
		var arr = res.result;
		
		for(var i=0;i<arr.length;i++){
			entSend[i] = arr[i].boxSendNumber;
		
		}
		for(var i=arr.length;i<10;i++){
			entSend[i] = 0;
		}
		data = [entSend];
		return data;
	}
	
	//处理绘图参数
	function chartOptions(res){
		var arr = res.result;
		var ticks=new Array(),max = 0;
		for(var i=0;i<arr.length;i++){
			ticks[i] = arr[i].enterpriseUser.name;
			
			if(arr[i].boxSendNumber > max){
				max = arr[i].boxSendNumber;
			}
		/*	if(arr[i].toUserCardData>max){
				max = arr[i].toUserCardData;
				
			}
			if(arr[i].toUserSmsData>max){
				max = arr[i].toUserSmsData;
				
			}
			if(arr[i].fromUserSmsData>max){
				max = arr[i].fromUserSmsData;
				
			}*/
		}
		for(var i=arr.length;i<10;i++){
			ticks[i] = "无";
		}
		
		max = Math.ceil(max*1.2/100)*100;
		if(max == 0){
			max = 100;
		}
		var weekStartDate = formatDate(getWeekStartDate(date));
		var weekEndDate = formatDate(getWeekEndDate(date));
		var title = '企业周使用数量('+weekStartDate+"至"+weekEndDate+')';
		
		var options = {
				title: {
		              text: title,  //设置当前图的标题
		              show: true,	//设置当前图的标题是否显示
		              fontFamily:'SimHei',
		              fontWeight:'bold', 
		              fontSize:'16pt'
		        },
		        seriesDefaults:{
		      		renderer:$.jqplot.BarRenderer, 
		      		//rendererOptions:{barPadding:10, barMargin:10},
		      		pointLabels: { show: true , formatString: '%d'}
	      		},
	      		legend: {
		      		show:true, 
		      		location: 'ne'
	      		},
	      		series: [{
	                   label: '企业下属盒子发送'
	               },
	               ],
	            axes:{
	      		xaxis:{
	      			renderer:$.jqplot.CategoryAxisRenderer, 
	      			rendererOptions:{
	      				sortMergedLabels:true,
	      				tickRenderer:$.jqplot.CanvasAxisTickRenderer
	      			},
	      			tickOptions:{
	      				fontFamily:'SimHei', 
	      				angle:-40, 
	      				fontWeight:'bold', 
	      				fontStretch:1,
	      				fontSize:'12pt'
	      			},
	      			ticks: ticks
	      		},
	      		yaxis:{
	      			min:0,
	      			max:max, 
	      			numberTicks:6
	      		}
	      	  },
	      		highlighter: {
	      		    show: true,
	      		    sizeAdjust: 15
	      		}
	      	
	          }
		return options;
	}
	
	/*
	 * 选择查询时间 
	 */
	function weekselect(){
		var timeStr = $dp.$('d121').value;
		if(timeStr.length==0){
			weekName = getWeekName(new Date());
		}else{
			date = new Date(timeStr.replace(/-/g,"/"));
			weekName = getWeekName(date);
		}
		weekName1 = weekName.split("W")[0]+"年"+weekName.split("W")[1]+"周";
		$dp.$('d121').value=weekName1;
		
		chart(1);
	}
	
	

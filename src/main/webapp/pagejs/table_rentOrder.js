/**************************************其它******************************************/
var oidStr;
var oTable2;
//查看更多

//租用订单查看更多按钮
function query(id){
	oidStr = id;
	$(".sendBox").hide();//隐藏订单发货
	$(".queryBox").show();//显示订单信息
	$(".updateExpress").hide();//隐藏修改快递
	
	$("#queryOrder").hide();//隐藏返回查看按钮
	$("#submitOrder").hide();//隐藏提交信息按钮
	$("#orderSend").hide();//隐藏订单发货
	$("#expressSubmit").hide();//隐藏查看物流
	$("#submitExpress").hide();//隐藏修改快递按钮
	$("#printExpress").hide();//隐藏打印物流单发货按钮
	$("#checkDz").hide();//隐藏查看电子运单按钮
	
	$.post("getOrder.do",
			{	"id":id 
			},function(data){
 		if(data.dealStatus=="等待发货"){
			$("#orderSend").show();//显示订单发货按钮
			$("#printExpress").show();//显示打印物流单发货按钮
			$("#proId").val(id);
		}else if(data.dealStatus!="等待发货"&&data.dealStatus!="取消订单"&&data.dealStatus!="未付款"){
			$("#expressSubmit").show();//显示查看物流
			$("#checkDz").show();//显示查看电子运单按钮
			$("#proId").val(id);
		} 
		//参数
		$("#user").val(data.user);
		$("#createdTime").val(data.createdTime);
		$("#orderNo").val(data.orderNo);
		$("#transactionType").val(data.transactionType); 
		$("#levermessage").val(data.leavemessage);  
		$("#expressNumber").val(data.expressNumber);  
		$("#bdcood").val(data.bdcood);  
		$("#bdcoodname").val(data.bdcoodname);  
		var payType = "";
		switch (data.zfType) {
		case "wechat":
			payType = "微信支付";break;
		case "alipay":
			payType = "支付宝支付";break;
		case "unionpay":
			payType = "银联支付";break;
		default:
			break;
		}
		$("#payType").val(payType);
		$("#trade_no").val(data.trade_no);
	});
}

//订单发货(顺丰)
function shipments(){
	var id = $("#proId").val();
    if(confirm("确定发货？！")){
	   $.post("saveExpressInfo.do", {"orderId":id}, function(data){
			if(data.status=="OK"){
				alert(data.message);
				window.location.href = "wayBill?id="+id;
			}else{
				alert(data.message);
			}
	   });
    }
}

//查看电子运单
function checkWayBill(){
	var id = $("#proId").val();
	$.post("getExpressByOrderId.do", {"orderId": id}, function(data){
		if(data!=null && data!=""){
			window.location.href = "wayBill?id="+id;
		} else {
			alert("此订单无电子运单号！");
		}
	});
}
//租用订单发货
function query1(id,dealStatus){
 	oidStr = id;
	$(".sendBox").hide();//隐藏订单发货
	$(".queryBox").show();//显示订单信息
	$(".updateExpress").hide();//隐藏修改快递
	
	$("#queryOrder").hide();//隐藏返回查看按钮
	$("#submitOrder").hide();//隐藏提交信息按钮
	$("#orderSend").hide();//隐藏订单发货
	$("#expressSubmit").hide();//隐藏查看物流
	$("#submitExpress").hide();//隐藏修改快递按钮
	$("#printExpress").hide();//隐藏打印物流号发货按钮
	$("#checkDz").hide();//隐藏查看电子运单按钮
	
	$.ajax({
		"type":"POSt",
		"url":"getOrder1.do",
		"data":{"id":id,"dealStatus":dealStatus},
		"datatype":"json",
		"async":false,
		"success":function(data){
 			if(data!=""){
				if(data.dealStatus=="等待发货"){
					$("#orderSend").show();//显示订单发货按钮
					$("#printExpress").show();//显示打印物流单号发货按钮
					$("#proId").val(id);
				}else if(data.dealStatus!="等待发货"&&data.dealStatus!="取消订单"&&data.dealStatus!="未付款"){
					$("#expressSubmit").show();//显示查看物流
					$("#proId").val(id);
				}
			}else{ 
				alert("此订单非等待发货状态！");
				document.location="table_order.html"; 
			} 
 			//参数
 			$("#user").val(data.user);
 			$("#buyTime").val(data.buyTime);
 			$("#orderNo").val(data.orderNo);
 			$("#transactionType").val(data.transactionType); 
 			$("#levermessage").val(data.leavemessage);  
 			$("#expressNumber").val(data.expressNumber);  
 			$("#bdcood").val(data.bdcood);  
 			$("#bdcoodname").val(data.bdcoodname);  
 			var payType = "";
 			switch (data.zfType) {
 			case "wechat":
 				payType = "微信支付";break;
 			case "alipay":
 				payType = "支付宝支付";break;
 			case "unionpay":
 				payType = "银联支付";break;
 			default:
 				break;
 			}
 			$("#payType").val(payType);
		}
	});
}

//点击订单发货触发
$("#orderSend").click(function(){
	$(".queryBox").hide();//隐藏订单信息
	$(".sendBox").show();//显示订单信息
	$(".updateExpress").hide();//隐藏修改快递
	$("#orderSend").hide();//隐藏订单发货按钮
	$("#printExpress").hide();//显示打印物流单号发货按钮
	$("#expressSubmit").hide();//隐藏查看物流
	$("#queryOrder").show();//显示返回查看按钮
	$("#submitOrder").show();//显示提交信息按钮
	$("#submitExpress").hide();//隐藏修改快递按钮
	$("#myModalLabel").text("订单发货");
	$("#checkDz").hide();//隐藏查看电子运单按钮
});
//点击返回查看触发
$("#queryOrder").click(function(){
	$(".queryBox").show();//隐藏订单信息
	$(".sendBox").hide();//显示订单信息
	$(".updateExpress").hide();//隐藏修改快递
	$("#expressSubmit").hide();//隐藏查看物流
	$("#orderSend").show();//显示订单发货按钮
	$("#printExpress").hide();//显示打印物流单号发货按钮
	$("#queryOrder").hide();//隐藏返回查看按钮
	$("#submitOrder").hide();//隐藏提交信息按钮
	$("#submitExpress").hide();//隐藏修改快递按钮
	$("#checkDz").hide();//隐藏查看电子运单按钮
	$("#myModalLabel").text("订单查看");
});

//点击修改快递触发
function updateexpress(oid,dealStatus){
	oidStr = oid;
	$(".queryBox").hide();//隐藏订单信息
	$(".sendBox").hide();//隐藏订单信息
	$(".updateExpress").show();//显示修改快递
	$("#orderSend").hide();//隐藏订单发货按钮
	$("#printExpress").show();//隐藏打印物流单号发货按钮
	$("#checkDz").hide();//显示查看电子运单按钮
	$("#expressSubmit").hide();//隐藏查看物流
	$("#queryOrder").hide();//显示返回查看按钮
	$("#submitOrder").hide();//显示提交信息按钮
	$("#submitExpress").show();//显示修改快递按钮
	$("#myModalLabel").text("修改快递");
	$.ajax({
		"type":"POST",
		"url":"getExpressInfo.do",
		"data":{"oid":oid},
		"datatype":"json",
		"success":function(res){
			if(res!=""){
				$("#updateLogisticsNum").val(res.expressNumber);
			}else{
 				document.location="table_order.html";  
			}
		}
	});
	
}
//验证快递单号
var flag1 = false;
function checkupdatelogisticsnum(){
	var $cause =$.trim($('#updateLogisticsNum').val());
	if($cause.length==0){
		$("#updateLogisticsNumError").text("请输入快递单号");
		flag1 = false;
	}else if($cause.length!=12){
		$("#updateLogisticsNumError").text("顺丰快递单号为12位");
		flag1 = false;
	}else{
		$("#updateLogisticsNumError").text("");
		flag1 = true;
	}
}

$("#updateLogisticsNum").on({
	blur:function(){
		checkupdatelogisticsnum();
	}
});

//点击快递修改，提交修改后的物流
$(document).on("click","#submitExpress",function(){
	checkupdatelogisticsnum();
	if(!flag1){
		return false;
	}
	var expressName = '';
	var updateLogisticsNum = $("#updateLogisticsNum").val();
	$.ajax({
		"type":"POST",
		"url":"updateExpress.do",
		"data":{"oid":oidStr,"expressName":expressName,"expressNumber":updateLogisticsNum},
		"datatype":"json",
		"success":function(res){
			if(res.status=='OK'){
				$('#myModal').modal('hide')
				oTable2.fnDraw();
			}else{
				alert("修改失败");
			}
		}
	});
});

//提交发货订单触发
$("#submitOrder").click(function(){
	$("#submitOrder").attr("disabled","disabled");
	var logisticsCompany = $("#logisticsCompany").val();
 	var logisticsNum = $.trim($("#logisticsNum").val());
	if(logisticsNum!=null&&logisticsNum!=""){
		$.post("saveExpress.do","oid="+oidStr+"&expressName="+logisticsCompany+"&numStr="+logisticsNum,function(data){
			if(data=="ok"){
				alert("提交成功！");
				window.location.href=window.location.href;
			}else{
				alert("提交失败！请核对物流单号");
				$("#submitOrder").attr("disabled");
			}
		});
	}
});
/**************************************其它******************************************/

$(function() {
	//DataTable
	  oTable2 = $('.orderTable').dataTable($.extend(
									dparams,
									{
										"sAjaxSource" : 'rentlistOrder.do',
										"fnServerData" : retrieveData2,// 自定义数据获取函数
										"aoColumns" : [
												{
													"sWidth" : "30px",
													"sClass" : "center",
													"mDataProp" : "orderNo",
													"bSortable" : false
												},
												{
													"sWidth" : "100px",
													"sClass" : "center",
													"mDataProp" : "productName",
													"bSortable" : false,
													"fnRender" : function(obj) {
														return "[租赁]北斗盒子";
													}
												},
												{
													"sWidth" : "20px",
													"sClass" : "center",
													"mDataProp" : "count",
													"bSortable" : false
												},
												{
													"sWidth" : "100px",
													"sClass" : "center",
													"mDataProp" : "name",
													"bSortable" : false,
													"fnRender" : function(obj) {
														var name = obj.aData['name'];
														var phone = obj.aData['phone'];
														var site = obj.aData['site'];
														return "<b>姓名：</b>"+name+"<br><b>电话：</b>"+phone+"<br><b>地址：</b>"+site;
													}
												},
												{
													"sWidth" : "10px",
													"sClass" : "center",
													"mDataProp" : "sumantecedent",
													"bSortable" : false
												},
												{
													"sWidth" : "40px",
													"sClass" : "center",
													"mDataProp" : "numberdate",
													"bSortable" : false
												},
												{
													"sWidth" : "50px",
													"sClass" : "center",
													"mDataProp" : "surplusdate",
													"bSortable" : false
												},
												{
													"sWidth" : "80px",
													"sClass" : "center",
													"mDataProp" : "rentStartTime",
													"bSortable" : false
												},
												{
													"sWidth" : "80px",
													"sClass" : "center",
													"mDataProp" : "rentEndTime",
													"bSortable" : false
												},
												{
													"sWidth" : "50px",
													"sClass" : "center",
													"mDataProp" : "dealStatus",
													"bSortable" : false
												},
												{
													"sWidth" : "40px",
													"sClass" : "center",
													"mDataProp" : "manager",
													"bSortable" : false
												},
												{
													"sWidth" : "20px",
													"sClass" : "center",
													"mDataProp" : "handle",
													"bSortable" : false,
													"fnRender" : function(obj) {
														var id = obj.aData['id'];
														var dealStatus = obj.aData['dealStatus'];
														var handle = obj.aData['handle'];
														if(dealStatus == "等待发货"){
															return handle + 
															"<br><a onclick='edit("+id+")' href='javascript:void(0)' data-toggle='modal' data-target='#editOrder'>编辑</a>";
														}else{
															return handle;
														}
													}
												}]
									}));
	
	$("#mycheck").click(function test() {
		oTable2.fnDraw();
	}); 
	
	//设置模态框可拖拽
	$("#myModal").draggable({
	    footer: "modal-footer"
	});
	
	$("#updateOrder").click(function(){
		var linkman = $("#linkman").val();
		var linkmob = $("#linkmob").val();
		var address = $("#address").val();  
		var proId = $("#productId").val();
		var startTime = $("#startTime").val();
		var endTime = $("#endTime").val();
		
		if(linkman == ""){
			alert("联系人不能为空");
			return;
		}
		if(linkmob == ""){
			alert("联系号码不能为空");
			return;
		}
		if(address == ""){
			alert("收货地址不能为空");
			return;
		}
		
		$.post("editOrder.do", {
			"linkman": linkman, 
			"linkmob": linkmob, 
			"invoice": 0,
			"address": address,
			"proId": proId,
			"startTime": startTime,
			"endTime": endTime
			}, function(data){
			if(data.status == "OK"){
				alert("修改成功！");
				window.location.reload();
			}else{
				alert(data.message);
			}
		}, "json");
	});
}); 

function edit(id){
	$.post("getOrder.do", {"id": id}, function(data){
		//参数
		$("#linkman").val(data.name);
		$("#linkmob").val(data.phone);
		$("#startTime").val(data.rentStartTime);
		$("#endTime").val(data.rentEndTime);
		$("#address").val(data.site);  
		$("#productId").val(data.id);
	});
}

//审核退款
function shipmentsOrder(orderNo,dealStatus) {
 	if (window.confirm('该操作是执行退款审核，确定要执行么？')) { 
		$.post("shipmentsOrder.do",{ 
			"orderNo":orderNo,
			"dealStatus":dealStatus
		}, function(data){ 
 			if(data=='true'){
				alert("操作成功！");
				oTable2.fnDraw();
 			}else if(data=='null'){
 				alert("订单非审核退款状态，请刷新页面后操作！"); 
				oTable2.fnDraw(); 
			}else{
				alert("操作失败！");
				oTable2.fnDraw(); 
			} 
		});  	
	}
}

//退款
function refund(orderNo,dealStatus,zfType) {
 	if (window.confirm('该操作是执行退款，确定要退款么？')) {
 		if(zfType=="wechat"){
 			$.post("refund.do",{
					"orderNo":orderNo,
					"dealStatus":dealStatus},
					function(data){ 
	 			if(data=='true'){
					alert("操作成功！");
					oTable2.fnDraw(); 
	 			}else if(data=='null'){
	 				alert("订单非待退款状态，请刷新页面后再操作！"); 
					oTable2.fnDraw(); 
				}else{
					alert("操作失败！");
					oTable2.fnDraw(); 
				} 
			}); 
 		} else if(zfType=="alipay"){
 			$("#refund_orderNo").val(orderNo);
 			$("#refundForm").submit();
 		} else if(zfType="unionpay"){
 			$.get("unionpay/authFinish", {"orderId":orderNo, "rentMoney":0}, function(data){
 				alert(data.message);
 				oTable2.fnDraw();
 			});
 		}
	}
}

//查询退款
function queryRefund(orderNo,dealStatus) {
 		$.post("queryRefund.do",{
 				"orderNo":orderNo,
 				"dealStatus":dealStatus},
 				function(data){ 
 			if(data=='true'){
				alert("操作成功！");
				oTable2.fnDraw(); 
 			}else if(data=='null'){
 				alert("订单非待退款中，请刷新页面后再操作！"); 
				oTable2.fnDraw(); 
 			}else{
 				alert("操作失败！");
				oTable2.fnDraw();

			} 
		});  	
 }

//显示物流信息
function getExpress(){
	var oid = $("#proId").val();
    //清空表格信息
	$("#tab").empty();
	//发起物流信息查询
	$.post("getExpressInfo.do","oid="+oid,function(result){
			var message = result.expressMessage;
			var express_info = "";
			if(message!=null && message!=""){
				message = "{"+message+"}";
				var msg = eval('('+message+')');
				var ex_max_len = msg.express.length;
				var express = new Array(ex_max_len);
				$.each(msg.express, function(i, v){
					if(i == ex_max_len-1){
						express[ex_max_len-1] = "<tr>"+
							"<td class='row1'>"+v.time+"</td>"+
							"<td class='status-first'>&nbsp;</td>"+
							"<td>"+v.content+"</td>"+
						"</tr>";
					} else if(i == 0 && result.expressStatus != "签收"){
						express[0] = "<tr class='last'>"+
							"<td class='row1' style='color: #FF8c00;'>"+v.time+"</td>"+
							"<td class='status-wait'>&nbsp;</td>"+
							"<td style='color: #FF8c00;'>"+v.content+"</td>"+
						"</tr>";
					} else if(i == 0 && result.expressStatus == "签收"){
						express[0] = "<tr class='last'>"+
							"<td class='row1' style='color: #FF8c00;'>"+v.time+"</td>"+
							"<td class='status-check'>&nbsp;</td>"+
							"<td style='color: #FF8c00;'>"+v.content+"</td>"+
						"</tr>";
					} else {
						express[i] = "<tr>"+
							"<td class='row1'>"+v.time+"</td>"+
							"<td class='status'>&nbsp;</td>"+
							"<td>"+v.content+"</td>"+
						"</tr>";
					}
				});
				for(var i=express.length-1; i>=0 ;i--){
					express_info += express[i];
				}
			}else {
				express_info = "<div style='color: #FF8c00; text-align: center; font-size: 18px; font-weight: bold;'>暂无物流信息...</div>";
			}
			$("#tab").append(express_info);
		},"json");
}

//自定义数据获取函数
function retrieveData2(sSource, aoData, fnCallback) {
	var orderNumbers = $("#orderNumbers").val();//用户
	var userNumber = $("#userNumber").val();//订单编号
	var dealStatusStr = $("#dealStatusStr").val();//订单状态 
	var dealStatusStrbd = $("#dealStatusStrbd").val();//订单状态 
	var name = $("#name").val();
 	aoData.push({
		"name" : "userNumber",
		"value" : userNumber
	}, {
		"name" : "orderNumbers",
		"value" : orderNumbers
	}, {
		"name" : "dealStatusStr",
		"value" : dealStatusStr
	},{
		"name" : "dealStatusStrbd",
		"value" : dealStatusStrbd
	},{
		"name" : "name",
		"value" : name
	});
	$.ajax({
	"type" : "GET",
	"url" : sSource,
	"dataType" : "json",
	"data" : aoData,
	"success" : function(resp) {
	fnCallback(resp);
	},
	"error" : function(resp) {
	}
	});

}

 
/**************************************DataTable******************************************/
 


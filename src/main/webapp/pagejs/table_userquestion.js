/**************************************其它******************************************/
 

/**************************************其它******************************************/
/**************************************DataTable******************************************/
$(function() {
					//DataTable
					var oTable = $('.userTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'userquestiontable.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [ 
																{
																	"sWidth" : "80px",
																	"sClass" : "center",
																	"mDataProp" : "id",
																	"bSortable" : false
																},
																{
																	"sWidth" : "200px",
																	"sClass" : "center",
																	"mDataProp" : "userid",
																	"bSortable" : false 
																}, 
																{
																	"sWidth" : "400px",
																	"sClass" : "center",
																	"mDataProp" : "myquestion",
																	"bSortable" : false,
																	 "fnRender" : function(obj) {
																		var myquestion = obj.aData['myquestion'];
																		result = '<div style="width:400px;word-wrap:break-word ">'+myquestion+'</div>'
																		return result;
																	} 
																}, 
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "submitTime",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "issolve",
																	"bSortable" : false
																}, 
																{
																	"sWidth" : "150x",
																	"sClass" : "opera",
																	"mDataProp" : "remask",
																	"fnRender" : function(obj) {
																		var id = obj.aData['id'];
																		var issolve = obj.aData['issolve'];
																		var result="";
																		if(issolve=="是"){
																			result="已解答";
																		}else{
																			result = "<a href=\"javascript:void(0);\" onclick=\"update("+id+")\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"halflings-icon white edit\"></i>解答用户问题</a>";
																		} 
   																		return result;
																	},
																	"bSortable" : false
																	
																}]
													})); 
					
					$("#mycheck").click(function test() {
						oTable.fnDraw();
					});
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) { 
	var username=$("#username").val();
	aoData.push({
		"name" : "username",
		"value" : username
	});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/**************************************DataTable******************************************/

 

//修改问题弹出框js
function update(id){
 	$.post("getquestion.do",{
		"id":id
	},function(res){  
 		$("#updatequestionname").val(res.result.myquestion);
 		$("#updateId").val(id);
 	},"json");
 	var flat="GENERALQUESTION"; 
  	querystype2(flat,'2');
} 

//修改问题提交按钮
$("#updateSubmit").click(function(){
 	var updatequestionname = $("#updatequestionname").val();
	var updatequestionvlaue = $("#updatequestionvlaue").val();  
	var updatequestiontype = "USERQUESTION";
	var updatesmalltype = "";  
	var id=$("#updateId").val();
	/*if(updatesmalltype==""){
		$("#updatemessageerror").text("类型不能为空！");
 		return;
	} */
	if(updatequestionvlaue.length==0){
		$("#updatemessageerror").text("答案不能为空，请填先填写答案");
 		return;
	} 
	$.post("addFAQ.do",{
		"id":id,
		"questionname":updatequestionname,
		"questionvlaue":updatequestionvlaue,
		"questiontype":updatequestiontype,
		"addsmalltype":updatesmalltype,
		"num":id
	},function(res){
		if(res.status=="OK"){
			alert("修改成功！");
			window.location.href=window.location.href;
		}else{
			alert("修改失败！");
			return;
		}
	},"json");
}); 

  
 
  
/*//改变事件1
function onchange3(){
	var flag = $("#updatequestiontype").val();
	querystype2(flag,'2');
}*/

/*
function querystype2(type,nums){
	if(type==null){
		type="";
	} 
	$.getJSON("queryTypeOptions.do?updatetype=" + type
			+ "&userType=ENT&page=1&pageSize=2000", function(data) {
		var d = data;
		if (d != '') {
			var s;
			if(nums=="1"){
				s = $('#addsmalltype'); 
			}else{
				s = $('#updatesmalltype');
			}
 			s.empty(); // 清除select中的 option
			$.each(d, function(index, item) {
				s.append(item);
				if(nums=="1"){
					$("#addsmalltype").trigger("liszt:updated");
				}else{
					$("#updatesmalltype").trigger("liszt:updated");
				}
 			});
		} else {
			alert('初始化推荐人错误！');
			d = '';
		}
	});  
	if(nums=="1"){
		$('[data-rel="chosen0"],[rel="chosen0"]').chosen({
			no_results_text : "没有匹配结果"
		});  
	}else{
		$('[data-rel="chosen5"],[rel="chosen5"]').chosen({
			no_results_text : "没有匹配结果"
		}); 
	} 
}*/
 
 
 
 
/**************************************其它******************************************/
 

/**************************************其它******************************************/
/**************************************DataTable******************************************/
$(function() {
	// DataTable
	var oTable = $('.userTable')
			.dataTable($.extend(dparams, {
					"sAjaxSource" : 'listAnnouncementtable.do',
					"fnServerData" : retrieveData,// 自定义数据获取函数
					"aoColumns" : [
							{
								"sWidth" : "50px",
								"sClass" : "center",
								"mDataProp" : "id",
								"bSortable" : false
							},
							{
								"sWidth" : "500px",
								"sClass" : "center",
								"mDataProp" : "center",
								"bSortable" : false,
								"fnRender" : function(obj) {
									var center = obj.aData['center'];
									result = '<div style="width:350px;word-wrap:break-word ">'
											+ center
											+ '</div>';
									return result;
								}
							},
							{
								"sWidth" : "300px",
								"sClass" : "center",
								"mDataProp" : "hyperlink",
								"bSortable" : false,
								"fnRender" : function(obj) {
									var hyperlink = obj.aData['hyperlink'];
									result = '<div style="width:300px;word-wrap:break-word ">'
											+ hyperlink
											+ '</div>';
									return result;
								}
							},
							{
								"sWidth" : "100px",
								"sClass" : "center",
								"mDataProp" : "creatTime",
								"bSortable" : false
							},
							{
								"sWidth" : "80px",
								"sClass" : "opera",
								"mDataProp" : "handle",
								"fnRender" : function(obj) {
									var id = obj.aData['id'];
									var result = "<a href=\"javascript:void(0);\" onclick=\"update("
											+ id
											+ ")\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"halflings-icon white edit\"></i>修改</a>"
											+ "<a class=\"btn btn-danger\" href=\"javascript:void(0);\" onclick=\"deleteProduct("
											+ id
											+ ")\"><i class=\"halflings-icon trash white\"></i>删除</a>";
									return result;
								},
								"bSortable" : false

							} ]
				}));

	$("#mycheck").click(function test() {
		oTable.fnDraw();
	});

});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) { 
	aoData.push({ 
	});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/**************************************DataTable******************************************/
function deleteProduct(id){
	if(!confirm('确认要删除吗？')){
		return false;
	}
	$.post("deleteAnnouncement.do",{
		"id":id,
	},function(res){
		if(res.status=="OK"){
			window.location.href=window.location.href;
		}else{
			alert("删除失败！");
			return;
		}
	},"json");
}

//修改公告弹出框js
function update(id){
	$.post("getAnnouncement.do",{
		"id":id
	},function(res){ 
		$("#updateId").val(id);
		$("#updateannouncementname").val(res.result.center);
		$("#updatehyperlink").val(res.result.hyperlink); 
	},"json");
} 

//修改公告提交按钮
$("#updateSubmit").click(function(){
 	var id = $("#updateId").val();
	var updateannouncementname = $("#updateannouncementname").val();
	var updatehyperlink = $("#updatehyperlink").val();  
	if(updateannouncementname.length==0){
		$("#updatemessageerror").text("公告内容不能为空，请输入公告内容！");
 		return;
	} 
	$.post("updateAnnouncement.do",{
		"id":id,
		"updateannouncementname":updateannouncementname,
		"updatehyperlink":updatehyperlink 
	},function(res){
		if(res.status=="OK"){
			alert("修改成功！");
			window.location.href=window.location.href;
		}else{
			alert("修改失败！");
			return;
		}
	},"json");
}); 

//新增公告确定按钮
$("#saveSubmit").click(function(){
	var announcementname = $("#announcementname").val();
	var hyperlink = $("#hyperlink").val(); 
	if(announcementname.length==0){
		$("#messageerror").text("公告内容不能为空！");
 		return;
	}
	if(announcementname.length>61){
		$("#messageerror").text("公告内容字数不能超过60字");
		alert("产品名称不能为空");
		return;
	} 
	$.post("addAnnouncement.do",{
		"announcementname":announcementname,
		"hyperlink":hyperlink 
	},function(res){
		if(res.status=="OK"){
			alert("添加成功！");
			window.location.href=window.location.href;
		}else{
			alert("添加失败！");
			return;
		}
	},"json");
});
 
 
/**************************************DataTable******************************************/
$(function() {
					//DataTable
					var oTable = $('.suggestionTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'listUserSuggestion.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "fromStr",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "content",
																	"bSortable" : false
																},
																{
																	"sWidth" : "130px",
																	"sClass" : "center",
																	"mDataProp" : "createdTime",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "username",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "phone",
																	"bSortable" : false
																},
																{
																	"sWidth" : "130px",
																	"sClass" : "center",
																	"mDataProp" : "qq",
																	"bSortable" : false
																},
																{
																	"sWidth" : "69px",
																	"sClass" : "opera",
																	"mDataProp" : "manager",
																	"fnRender" : function(obj) {
																		var id = obj.aData['id'];
																		var result = "<a class=\"btn btn-large btn-danger\" href=\"javascript:if(confirm('确认要删除吗？'))location=\'deleteUserSuggestion.do?id="+id+"\'\">"
																					+"<i class=\"halflings-icon trash white\"></i>"
																					+"</a>";
																		return result;
																	},
																	"bSortable" : false
																	
																} ]
													}));

					
					$("#mycheck").click(function test() {
						oTable.fnDraw();
					}); 
					
					
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/**************************************DataTable******************************************/

/**************************************其它******************************************/

/**************************************其它******************************************/
/**************************************DataTable******************************************/
$(function() {
					//DataTable
					var oTable = $('.userTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'listRefereesUser.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "id",
																	"bSortable" : false
																},
																{
																	"sWidth" : "60px",
																	"sClass" : "center",
																	"mDataProp" : "refereesUser",
																	"bSortable" : false
																},
																{
																	"sWidth" : "40px",
																	"sClass" : "center",
																	"mDataProp" : "beizhuname",
																	"bSortable" : false
																},
																{
																	"sWidth" : "40px",
																	"sClass" : "center",
																	"mDataProp" : "mony",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "nums",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "usernums",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "notusenums",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "notdatednums",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "datednums",
																	"bSortable" : false
																}, 
																{
																	"sWidth" : "60px",
																	"sClass" : "center",
																	"mDataProp" : "manager", 
																	"bSortable" : false,
																	"fnRender" : function(obj) {
																		var refereesUser = obj.aData['refereesUser'];
 																		var  result = "<a class=\"btn btn-primary\" href=\"table_bdCood_detail.html?refereespeople="+refereesUser+"\">查看详情</a>"; 																		 
				 														  return result;
				 														 
																	}
																	
																}]
													}));

					
					$("#mycheck").click(function test() {
						oTable.fnDraw();
					});
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	var username = $("#refereespeople").val(); 
	var refereespeoplename = $("#refereespeoplename").val();

	aoData.push({
		"name" : "username",
		"value" : username
	},{
		"name" : "refereespeoplename",
		"value" : refereespeoplename
	});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/**************************************DataTable******************************************/
function update(id){
	
	$.post("getManageUserDto.do",{
		"id":id,
		"systemCode":"ids2j3jdunjejh2i32jdjalalghywu020mvgwe872e3iu"
	},function(res){
		if(res.result.userType=="普通"){
			$("#updateUserTypeStr option[value='ROLE_COMMON']").attr("selected",true);
		}else if(res.result.userType=="管理"){
			$("#updateUserTypeStr option[value='ROLE_MANAGER']").attr("selected",true);
		}
		
		if(res.result.userStatusType=="正常"){
			$("#updateUserStatusTypeStr option[value='NORMAL']").attr("selected",true);
		}else if(res.result.userStatusType=="未激活"){
			$("#updateUserStatusTypeStr option[value='NOTACTIVATED']").attr("selected",true);
		}else if(res.result.userStatusType=="禁止"){
			$("#updateUserStatusTypeStr option[value='STOP']").attr("selected",true);
		}
		$("#updateId").val(res.result.id);
		$("#updateUsername").val(res.result.username);
		$("#updateMail").val(res.result.mail);
		
		$("#updateName").val(res.result.name);
		$("#updateBirthDate").val(res.result.birthDate);
		if(res.result.sex=="保密"){
			$("#updateSex option[value='保密']").attr("selected",true);
		}else if(res.result.sex=="男"){
			$("#updateSex option[value='男']").attr("selected",true);
		}else if(res.result.sex=="女"){
			$("#updateSex option[value='女']").attr("selected",true);
		}
		$("#updateResidence").val(res.result.residence);
		$("#updateJob").val(res.result.job);
		$("#updateHobby").val(res.result.hobby);
		if(res.result.isDsiEnable=="1"){
			$("#isDsiEnable option[value='true']").attr("selected",true);
		}else if(res.result.sex=="0"){
			$("#updateSex option[value='false']").attr("selected",true);
		}
		$("#freq").val(res.result.freq);

		 
		
		//省市区
		var province = ""
		var city = ""
		var area = "";
		if (res.result.residence != null && res.result.residence != ""
				&& res.result.residence != "null") {
			var splitstr = new Array();
			splitstr = res.result.residence.split('/');
			province = splitstr[0];
			city = splitstr[1];
			area = splitstr[2];
		}
		new PCAS("province", "city", "area", province, city, area);//初始化三级联动
		
		//年月日
		if(res.result.birthDate!=null&&res.result.birthDate!=""&&res.result.birthDate!="null"){
			var splitstr= new Array();
			splitstr=res.result.birthDate.split('/');
			$("#select_year").attr("rel",splitstr[0]);
			$("#select_month").attr("rel",splitstr[1]);
			$("#select_day").attr("rel",splitstr[2]);
			//初始化
			  $.ms_DatePicker({  
		          YearSelector: "#select_year",  
		          MonthSelector: "#select_month",  
		          DaySelector: "#select_day"  
		  }); 
		}	
	},"json");
}

$("#updateSubmit").click(function(){
	var userType = $("#updateUserTypeStr").val();
	var userStatusType = $("#updateUserStatusTypeStr").val();
	var username = $("#updateUsername").val();
	var mail = $("#updateMail").val();
	var id = $("#updateId").val();
	var province = $("#province").val();
	var city = $("#city").val();
	var area = $("#area").val();
	var select_year = $("#select_year").val();
	var select_month = $("#select_month").val();
	var select_day = $("#select_day").val();
	var updateName = $("#updateName").val();
	var updateSex = $("#updateSex").val();
	var updateJob = $("#updateJob").val();
	var updateHobby = $("#updateHobby").val();
	var isDsiEnable = $("#isDsiEnable").val();
	var freq = $("#freq").val();
	var residence;
	if(province!=""&&city!=""&&area!=""){
		residence = province+"/"+city+"/"+area;
	}else{
		residence = null;
	}
	
	var birthDate;
	if(select_year!=0&&select_month!=0&&select_day!=0){
		birthDate = select_year+"/"+select_month+"/"+select_day;
	}else{
		birthDate = null;
	}
	
	var cknum=/^[1-9]\d*$/;
	if(!cknum.test(freq)){
		alert("频度为正整数，请重新输入！");
		return;
	}
		$.post("updateUser.do",{
		"userType":userType,
		"userStatusType":userStatusType,
		"username":username,
		"mail":mail,
		"birthDate":birthDate,
		"residence":residence,
		"name":updateName,
		"sex":updateSex,
		"job":updateJob,
		"hobby":updateHobby,
		"systemCode":"ids2j3jdunjejh2i32jdjalalghywu020mvgwe872e3iu",
		"id":id,
		"isDsiEnable":isDsiEnable,
		"freq":freq
	},function(res){
		if(res.status=="OK"){
			alert("修改成功！");
			window.location.href=window.location.href;
		}else{
			alert("修改失败！");
			return;
		}
	},"json");
	
});

$("#saveSubmit").click(function(){
	var userType = $("#saveUserTypeStr").val();
	var userStatusType = $("#saveUserStatusTypeStr").val();
	var username = $("#saveUsername").val();
	var mail = $("#saveMail").val();
	var saveName = $("#saveName").val();
	var select_year2 = $("#select_year2").val();
	var select_month2 = $("#select_month2").val();
	var select_day2 = $("#select_day2").val();
	var province2 = $("#province2").val();
	var city2 = $("#city2").val();
	var area2 = $("#area2").val();
	var saveJob = $("#saveJob").val();
	var saveHobby = $("#saveHobby").val();
	var saveSex = $("#saveSex").val();
	var savefreq = $("#savefreq").val();
	var saveisDsiEnable = $("#saveisDsiEnable").val();
	var residence;
	if(province2!=""&&city2!=""&&area2!=""){
		residence = province2+"/"+city2+"/"+area2;
	}else{
		residence = null;
	}
	
	var birthDate;
	if(select_year2!=0&&select_month2!=0&&select_day2!=0){
		birthDate = select_year2+"/"+select_month2+"/"+select_day2;
	}else{
		birthDate = null;
	}
	
	var cknum=/^[1-9]\d*$/;
	if(!cknum.test(savefreq)){
		alert("频度为正整数，请重新输入！");
		return;
	}
	$.post("saveUser",{
		"userType":userType,
		"userStatusTypeStr":userStatusType,
		"username":username,
		"mail":mail,
		"name":saveName,
		"birthDate":birthDate,
		"residence":residence,
		"sex":saveSex,
		"job":saveJob,
		"hobby":saveHobby,
		"freq":savefreq,
		"isDsiEnable":saveisDsiEnable
	},function(res){
		if(res.status=="OK"){
			alert("添加成功！");
			window.location.href=window.location.href;
		}else{
			alert("添加失败！");
			return;
		}
	},"json");
});

$("#saveUserModel").click(function(){
	//初始化年月日
	  $.ms_DatePicker({  
        YearSelector: "#select_year2",  
        MonthSelector: "#select_month2",  
        DaySelector: "#select_day2"  
	  }); 
	  new PCAS("province2", "city2", "area2");//初始化三级联动
});

/***************年月日三级联动*******************/


(function($){
	$.extend({
	ms_DatePicker: function (options) {
	            var defaults = {
	                YearSelector: "#sel_year",
	                MonthSelector: "#sel_month",
	                DaySelector: "#sel_day",
	                FirstText: "--",
	                FirstValue: 0
	            };
	            var opts = $.extend({}, defaults, options);
	            var $YearSelector = $(opts.YearSelector);
	            var $MonthSelector = $(opts.MonthSelector);
	            var $DaySelector = $(opts.DaySelector);
	            var FirstText = opts.FirstText;
	            var FirstValue = opts.FirstValue;

	            // 初始化
	            var str = "<option value=\"" + FirstValue + "\">" + FirstText + "</option>";
	            $YearSelector.html(str);
	            $MonthSelector.html(str);
	            $DaySelector.html(str);

	            // 年份列表
	            var yearNow = new Date().getFullYear();
				var yearSel = $YearSelector.attr("rel");
	            for (var i = yearNow; i >= 1900; i--) {
					var sed = yearSel==i?"selected":"";
					var yearStr = "<option value=\"" + i + "\" " + sed+">" + i + "</option>";
	                $YearSelector.append(yearStr);
	            }

	            // 月份列表
				var monthSel = $MonthSelector.attr("rel");
	            for (var i = 1; i <= 12; i++) {
					var sed = monthSel==i?"selected":"";
	                var monthStr = "<option value=\"" + i + "\" "+sed+">" + i + "</option>";
	                $MonthSelector.append(monthStr);
	            }

	            // 日列表(仅当选择了年月)
	            function BuildDay() {
	                if ($YearSelector.val() == 0 || $MonthSelector.val() == 0) {
	                    // 未选择年份或者月份
	                    $DaySelector.html(str);
	                } else {
	                    $DaySelector.html(str);
	                    var year = parseInt($YearSelector.val());
	                    var month = parseInt($MonthSelector.val());
	                    var dayCount = 0;
	                    switch (month) {
	                        case 1:
	                        case 3:
	                        case 5:
	                        case 7:
	                        case 8:
	                        case 10:
	                        case 12:
	                            dayCount = 31;
	                            break;
	                        case 4:
	                        case 6:
	                        case 9:
	                        case 11:
	                            dayCount = 30;
	                            break;
	                        case 2:
	                            dayCount = 28;
	                            if ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)) {
	                                dayCount = 29;
	                            }
	                            break;
	                        default:
	                            break;
	                    }
						
						var daySel = $DaySelector.attr("rel");
	                    for (var i = 1; i <= dayCount; i++) {
							var sed = daySel==i?"selected":"";
							var dayStr = "<option value=\"" + i + "\" "+sed+">" + i + "</option>";
	                        $DaySelector.append(dayStr);
	                    }
	                }
	            }
	            $MonthSelector.change(function () {
	                BuildDay();
	            });
	            $YearSelector.change(function () {
	                BuildDay();
	            });
				if($DaySelector.attr("rel")!=""){
					BuildDay();
				}
	        } // End ms_DatePicker
	});
	})(jQuery);

 
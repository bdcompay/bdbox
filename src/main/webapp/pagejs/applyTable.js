/**************************************其它******************************************/
//输入提示
//$.getJSON("queryUsername.do", function(data){
	//$("#username").autocomplete({
		//source:[data.aaData]
	//}); 
//});

/**************************************其它******************************************/
/**************************************DataTable******************************************/
$(function() {
					//DataTable
					var oTable = $('.applyTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'listApply.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "user",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "name",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "phone",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "email",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "join",
																	"bSortable" : false
																},
																{
																	"sWidth" : "30px",
																	"sClass" : "center",
																	"mDataProp" : "sex",
																	"bSortable" : false
																},
																{
																	"sWidth" : "130px",
																	"sClass" : "center",
																	"mDataProp" : "province",
																	"bSortable" : false
																},
																{
																	"sWidth" : "120px",
																	"sClass" : "center",
																	"mDataProp" : "city",
																	"bSortable" : false
																},
																{
																	"sWidth" : "130px",
																	"sClass" : "center",
																	"mDataProp" : "code",
																	"bSortable" : false
																},
																{
																	"sWidth" : "130px",
																	"sClass" : "center",
																	"mDataProp" : "createTime",
																	"bSortable" : false
																},
																{
																	"sWidth" : "330px",
																	"sClass" : "center",
																	"mDataProp" : "intro",
																	"bSortable" : false
																},
																{
																	"sWidth" : "330px",
																	"sClass" : "center",
																	"mDataProp" : "explain",
																	"bSortable" : false
																},
																{
																	"sWidth" : "69px",
																	"sClass" : "opera",
																	"mDataProp" : "manager",
																	"fnRender" : function(obj) {
																		var id = obj.aData['id'];
																		var result = "<a class=\"btn btn-large btn-danger\" href=\"javascript:if(confirm('确认要删除文章吗？'))location=\'deleteApply.do?id="+id+"\'\">"
																					+"<i class=\"halflings-icon trash white\"></i>"
																					+"</a>";
																		return result;
																	},
																	"bSortable" : false
																	
																} ]
													}));

					
					$("#mycheck").click(function test() {
						oTable.fnDraw();
					});
					
					
					
					
					
					
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	var createTimeStr = $("#createTimeStr").val();
	var joinStr = $("#joinStr").val();
	var sex = $("#sex").val();
	var code = $("#code").val();
	var phone = $("#phone").val();
	aoData.push({
		"name" : "createTimeStr",
		"value" : createTimeStr
	}, {
		"name" : "joinStr",
		"value" : joinStr
	},  {
		"name" : "sex",
		"value" : sex
	}, {
		"name" : "code",
		"value" : code
	},{
		"name" : "phone",
		"value" : phone
	});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/**************************************DataTable******************************************/
/*****************************************************************************************/
//导出
$("#export").click(function(){
	$("#export").attr("disabled","disabled");
	if (confirm("是否确认导出？")){
		$.post("exportApply.do",function(result){
			$("#export").removeAttr("disabled");
			window.location.href=result;
		});
	}else{
		$("#export").removeAttr("disabled");
	}
});

/**************************************其它******************************************/
 
 
$(function() {
					//DataTable
					var oTable = $('.userTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'listNotification.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																{
																	"sWidth" : "50px",
																	"sClass" : "center",
																	"mDataProp" : "id",
																	"bSortable" : false
																},
																{
																	"sWidth" : "200px",
																	"sClass" : "center",
																	"mDataProp" : "notecenter",
																	"bSortable" : false,
																	 "fnRender" : function(obj) {
																		var notecenter = obj.aData['notecenter'];
																		result = '<div style="width:200px;word-wrap:break-word ">'+notecenter+'</div>'
																		return result;
																	} 
																}, 
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "mailType",
																	"bSortable" : false
																},
																/*{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "subject",
																	"bSortable" : false 
																}, 
																{
																	"sWidth" : "200px",
																	"sClass" : "center",
																	"mDataProp" : "center",
																	"bSortable" : false,
																	 "fnRender" : function(obj) {
																		var center = obj.aData['center'];
																		result = '<div style="width:200px;word-wrap:break-word ">'+center+'</div>'
																		return result;
																	} 
																},  
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "name",
																	"bSortable" : false
																}, 
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "tomail",
																	"bSortable" : false
																}, 
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "receiveTeliPhone",
																	"bSortable" : false
																},*/
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "createTime",
																	"bSortable" : false
																},
																{
																	"sWidth" : "80px",
																	"sClass" : "opera",
																	"mDataProp" : "handdle",
																	"fnRender" : function(obj) {
																		var id = obj.aData['id'];
 																		var result = "<a href=\"javascript:void(0);\" onclick=\"update("+id+")\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"halflings-icon white edit\"></i>修改</a>"
																					+"<a class=\"btn btn-danger\" href=\"javascript:void(0);\" onclick=\"deleteProduct("+id+")\"><i class=\"halflings-icon trash white\"></i>删除</a>";
  																		return result;
																	},
																	"bSortable" : false
																	
																}]
													}));

					
					$("#mycheck").click(function test() {
						oTable.fnDraw();
					});
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) { 
	//var tongzhifanshi=$("#tongzhifanshi").val();
	var tongzhileixing=$("#tongzhileixing").val();
  	var type_twl="customer";

	aoData.push({
		"name" : "tongzhileixing",
		"value" : tongzhileixing},
		{"name" : "type_twl",
		"value" : type_twl
	});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/**************************************DataTable******************************************/

//删除问题
function deleteProduct(id){
	if(!confirm('确认要删除吗？')){
		return false;
	}
	$.post("deleteNotification.do",{
		"id":id,
	},function(res){
		if(res.status=="OK"){
			window.location.href=window.location.href;
		}else{
			alert("删除失败！");
			return;
		}
	},"json");
}

//修改问题弹出框js
function update(id){ 
	$.post("getNotification.do",{
		"id":id
	},function(res){ 
		if(res.result.mailType=="购买下单通知"){
			$("#updatetongzhileixing option[value='BUYORDERNOTPAY']").attr("selected",true);
		}else if(res.result.mailType=="租赁下单通知"){
			$("#updatetongzhileixing option[value='RENTORDERNOTPAY']").attr("selected",true);
		}else if(res.result.mailType=="购买付款通知"){
			$("#updatetongzhileixing option[value='BUYPAY']").attr("selected",true);
		}else if(res.result.mailType=="租赁付款通知"){
			$("#updatetongzhileixing option[value='RENTPAY']").attr("selected",true);
		}else if(res.result.mailType=="购买申请退款通知"){
			$("#updatetongzhileixing option[value='BUTREFUND']").attr("selected",true);
		}else if(res.result.mailType=="租赁申请退款通知"){
			$("#updatetongzhileixing option[value='RENTREFUND']").attr("selected",true);
		}else if(res.result.mailType=="租赁发货通知"){
			$("#updatetongzhileixing option[value='RENTSEND']").attr("selected",true);
		}else if(res.result.mailType=="购买签收通知"){
			$("#updatetongzhileixing option[value='BUYSIGN']").attr("selected",true);
		}else if(res.result.mailType=="租赁签收通知"){
			$("#updatetongzhileixing option[value='RENTSIGN']").attr("selected",true);
		}else if(res.result.mailType=="申请退货通知"){
			$("#updatetongzhileixing option[value='APPLYRETURN']").attr("selected",true);
		}else if(res.result.mailType=="申请归还通知"){
			$("#updatetongzhileixing option[value='APPLYRENTRETURN']").attr("selected",true);
		}else if(res.result.mailType=="退货审核申请通过通知"){
			$("#updatetongzhileixing option[value='BUYAPPLYRENTRETURN']").attr("selected",true);
		}else if(res.result.mailType=="归还审核申请通过通知"){
			$("#updatetongzhileixing option[value='RENTAPPLYRENTRETURN']").attr("selected",true);
		}else if(res.result.mailType=="填写退货单通知"){
			$("#updatetongzhileixing option[value='WRITERETURN']").attr("selected",true);
		}else if(res.result.mailType=="填写归还单通知"){
			$("#updatetongzhileixing option[value='RETURNSIGN']").attr("selected",true);
		}else if(res.result.mailType=="退货退款"){
			$("#updatetongzhileixing option[value='BUYRENT']").attr("selected",true);
		}else if(res.result.mailType=="归还退款"){
			$("#updatetongzhileixing option[value='RENTRETURN']").attr("selected",true);
		}else if(res.result.mailType=="续期通知"){
			$("#updatetongzhileixing option[value='XUQITIXIN']").attr("selected",true);
		}else if(res.result.mailType=="到期通知"){
			$("#updatetongzhileixing option[value='DAOQITIXIN']").attr("selected",true);
		}else if(res.result.mailType=="实名认证审核通过通知"){
			$("#updatetongzhileixing option[value='SHIMINGRENZHENGPASS']").attr("selected",true);
		}else if(res.result.mailType=="实名认证审核未通过通知"){
			$("#updatetongzhileixing option[value='SHIMINGRENZHENG']").attr("selected",true);
		}else if(res.result.mailType=="发票审核通过通知"){
			$("#updatetongzhileixing option[value='FAPIAOSHENHEPASS']").attr("selected",true);
		}else if(res.result.mailType=="发票审核不通过通知"){
			$("#updatetongzhileixing option[value='FAPIAOSHENHE']").attr("selected",true);
		}else if(res.result.mailType=="购买发货通知"){
			$("#updatetongzhileixing option[value='buySEND']").attr("selected",true);
		}else if(res.result.mailType=="库存不足通知（购买）"){
			$("#updatetongzhileixing option[value='BUYSTOCK']").attr("selected",true);
		}else if(res.result.mailType=="库存不足通知（租赁）"){
			$("#updatetongzhileixing option[value='RENTSTOCK']").attr("selected",true);
		}else if(res.result.mailType=="实名制审核通知"){
			$("#updatetongzhileixing option[value='SHIMZHI']").attr("selected",true);
		}else if(res.result.mailType=="发票审核通知"){
			$("#updatetongzhileixing option[value='FPIAO']").attr("selected",true);
		}else if(res.result.mailType=="到货通知（购买）"){
			$("#updatetongzhileixing option[value='BUYDAOHUO']").attr("selected",true);
		}else if(res.result.mailType=="到货通知（租赁）"){
				$("#updatetongzhileixing option[value='RENTDAOHUO']").attr("selected",true);
	    }else if(res.result.mailType=="活动到期时间通知"){
			$("#updatetongzhileixing option[value='ACTIVTI']").attr("selected",true);
        }else if(res.result.mailType=="退货审核通过退款"){
				$("#updatetongzhileixing option[value='tuihuoshenqingtongguo']").attr("selected",true);
	    }else if(res.result.mailType=="归还审核通过退款"){
			$("#updatetongzhileixing option[value='guihuanshenqingtongguo']").attr("selected",true);
        }else if(res.result.mailType=="解答用户问题"){
			$("#updatetongzhileixing option[value='solvequestion']").attr("selected",true);
        }       
		
   		if(res.result.isnote=="短信"){ 
			$('#last input:checkbox:first').attr("checked",'checked'); 
		}
		if(res.result.isemail=="邮件"){
			$('#firsts input:checkbox:first').attr("checked",'checked');  
 		}
 		$("#ids").val(id);
		//$("#updatesubject").val(res.result.subject);
		//$("#updatecenter").val(res.result.center);
		//$("#upedatetomailname").val(res.result.name);
		//$("#updateemail").val(res.result.tomail);  
		//$("#updateTeliPhone").val(res.result.receiveTeliPhone);
		$("#updatenotecenter").val(res.result.notecenter);  
	},"json");
} 

//修改问题提交按钮
$("#updateSubmit").click(function(){ 
	var id=$("#ids").val();
	var isemail=""; 
	var note="";
 	var tongzhileixing = $("#updatetongzhileixing").val(); //发送类型
 	var frommail = $("#hidden").val();               //发送邮箱
 	var upedatetomailname = "";                   //接收邮箱
 	var updateemail = "";               //主题
 	var updateTeliPhone = "";                 //邮件内容
 	var updatesubject = "";          //名字
 	var updatecenter = "";            //手机号码
 	var updatenotecenter = $("#updatenotecenter").val();          //手机内容
  	/*if($('#checkboxmail').is(':checked')){
  		if(updateemail==""){
 			$("#updatemessageerror").text("收信人邮箱不能为空！");
 	 		return;
 		}
 		if(updatesubject==""){
 			$("#updatemessageerror").text("主题不能为空！");
 	 		return;
 		} 
 		if(updatecenter==""){
 			$("#updatemessageerror").text("邮件内容不能为空！");
 	 		return;
 		} 
 		isemail=1;
 	} */
	if($('#checkboxnote').is(':checked')){ 
 		if(updatenotecenter==""){
 			$("#updatemessageerror").text("短信内容不能为空！");
 	 		return;
 		} 
 		note=1;
 	} 
	 
	if(!$('#updatecheckboxnote').is(':checked')&&!$('#updatecheckboxmail').is(':checked')){
 		$("#updatemessageerror").text("请选选择通知方式！"); 
			return;
	} 
	if(tongzhileixing=="通知类型"){
 		$("#updatemessageerror").text("请选择通知类型！"); 
 		return;
	}  
	$.post("updateNotification.do",{
		"id":id,
		"isemail":isemail,
		"note":note,
		"tongzhileixing":tongzhileixing,
		"frommail":frommail,
		"upedatetomailname":upedatetomailname,
		"updateemail":updateemail,
		"updateTeliPhone":updateTeliPhone,
		"updatesubject":updatesubject,
		"updatecenter":updatecenter,
		"updatenotecenter":updatenotecenter 

	},function(res){
		if(res.status=="OK"){
			alert("修改成功！");
			window.location.href=window.location.href;
		}else{
			alert("修改失败！");
			return;
		}
	},"json");
}); 

//新增问题确定按钮
$("#saveSubmit").click(function(){
 	var isemail=""; 
	var note="";
 	var tongzhileixing = $("#savetongzhileixing").val(); //发送类型
 	var frommail = $("#hidden").val();               //发送邮箱
 	var toemail = "";                   //接收邮箱
 	var subject = "";               //主题
 	var center = $("#center").val();                 //邮件内容
 	var tomailname = "";          //名字
 	var TeliPhone = "";            //手机号码
 	var notecenter = $("#notecenter").val();          //手机内容
 	var type_twl = $("#type_twl").val();             //类别（客户）

  	/*if($('#checkboxmail').is(':checked')){
  		if(email==""){
 			$("#messageerror").text("收信人邮箱不能为空！");
 	 		return;
 		}
 		if(subject==""){
 			$("#messageerror").text("主题不能为空！");
 	 		return;
 		}
 		if(subject==""){
 			$("#messageerror").text("主题不能为空！");
 	 		return;
 		}
 		if(center==""){
 			$("#messageerror").text("邮件内容不能为空！");
 	 		return;
 		}
 		isemail=1;
 	} */
	if($('#checkboxnote').is(':checked')){ 
 		if(notecenter==""){
 			$("#messageerror").text("短信内容不能为空！");
 	 		return;
 		} 
 		note=1;
 	} 
	if(!$('#checkboxnote').is(':checked')&&!$('#checkboxmail').is(':checked')){
 		$("#messageerror").text("请选选择通知方式！"); 
			return;
	} 
	if(tongzhileixing=="通知类型"){
 		$("#messageerror").text("请选择通知类型！"); 
 		return;
	}
 	$.post("addNotification.do",{
		"isemail":isemail,
		"note":note,
		"tongzhileixing":tongzhileixing,
		"frommail":frommail,
		"toemail":toemail,
		"subject":subject,
		"center":center,
		"tomailname":tomailname,
		"TeliPhone":TeliPhone,
		"notecenter":notecenter,
		"type_twl":type_twl
	},function(res){
		if(res.status=="OK"){
			alert("添加成功！");
			window.location.href=window.location.href;
		}else{
			alert("添加失败！");
			return;
		}
	},"json");
});
 
 
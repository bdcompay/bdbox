

/*************************************************
 * 该js为datatable插件的属性						 *
 * 每个使用datatable插件的页面都应添加该js		 *
 * ***********************************************/

 
/**************************************DataTable******************************************/
var flag = true;
var initParams = {};
var dparams = {
	//分页样式
	"sPaginationType": "full_numbers",
	"oLanguage": {
		/*描述当分页组件的下拉菜单的选项被改变的时候发生的动作*/
		"sLengthMenu": "每页显示 _MENU_ 条记录",
		/*当对数据进行过滤操作后，如果没有要显示的数据，会在表格记录中显示该字符串*/
		"sZeroRecords": "抱歉， 没有找到符合信息！",
		/*该属性给终端用户提供当前页面的展示信息，字符串中的变量会随着表格的更新被动态替换，而且可以被任意移动和删除*/
		"sInfo": '从 <strong> _START_ </strong>到<strong> _END_ </strong>/共<strong> _TOTAL_ </strong>条数据',
		/*当用户过滤表格中的信息的时候，该字符串会被附加到信息字符串的后面，从而给出过滤器强度的直观概念*/
		"sInfoFiltered": "(从 _MAX_ 条数据中检索)",
		/*汉化*/
		"oPaginate": {
			"sFirst": "首页",
			"sPrevious": "前一页",
			"sNext": "后一页",
			"sLast": "尾页"
		},
		"sSearch": "搜索：",//描述用户在输入框输入过滤条件时的动作
		/*当表格处理用户动作（例如排序或者类似行为）的时候显示的字符串*/
		"sProcessing": "正在加载中....",  
		//"sProcessing": '<img src="pageimages/loading.gif" /><label>请稍等，数据正在加载中...</label>'
	},
	"aLengthMenu": [[10, 20, 35,50],[10, 20, 35,50]],//手动设置每页显示数量
	"bDeferRender":true,//表格每一行新增的元素只有在需要被画出来时才会被DataTable创建出来
	"bFilter": false,//是否对数据进行过滤
	"bAutoWidth": false,//是否自动计算列宽
	"bProcessing": true,//当表格在处理的时候（比如排序操作）是否显示“处理中...”
	"bServerSide": true,//配置使用服务器端处理的DataTable
	"bInfo": true,//是否显示表格信息
	"bLengthChange": true, //改变每页显示数据数量
	"iDisplayLength": 10//单页显示的数据的条数
}
/**************************************DataTable******************************************/

$(document).ready(function(){ 
	$(".hidden-tablet").addClass("limout");
	var myuri = location.href;
	var navs = $(".panel-body li a");
	for(var i=0;i<navs.length;i++){
		if(myuri==navs[i].href){
			var tablet = $(".panel-body li a").eq(i).find(".hidden-tablet");
			var collapse = $(".panel-body li a").eq(i).parent("li").parent("ul").parent(".panel-body").parent(".panel-collapse");
			collapse.removeClass("collapse");
			collapse.addClass("active");
			
			var heading = collapse.prev();
			heading.removeClass("mout");
			heading.addClass("selected");
			
			tablet.removeClass("limout");
			tablet.addClass("liselected");
		}
	}
}); 


//点击一级菜单
$(document).on("click",".panel-heading",function(){
	if($(this).next().hasClass("collapse")){
		$(".panel-collapse").removeClass("collapse");
		$(".panel-collapse").removeClass("active");
		$(".panel-collapse").addClass("collapse");
		$(this).next().removeClass("collapse");
		$(this).next().addClass("active");
	}else{
		$(this).next().removeClass("active");
		$(this).next().addClass("collapse");
	}
});

//一级菜单移入移出
$('.panel-heading').on({
	mouseover : function(e) {
		if(!$(this).hasClass("selected")){
			$(this).removeClass("mout");
			$(this).addClass("mover");
		}
	},
	mouseout: function(e) {
		if(!$(this).hasClass("selected")){
			$(this).removeClass("mover");
			$(this).addClass("mout");
		}
	}
});

//二级菜单移入移出
$('.hidden-tablet').on({
	mouseover : function(e) {
		if(!$(this).hasClass("liselected")){
			$(this).removeClass("limout");
			$(this).addClass("limover");
		}
	},
	mouseout: function(e) {
		if(!$(this).hasClass("liselected")){
			$(this).removeClass("limover");
			$(this).addClass("limout");
		}
	}
});
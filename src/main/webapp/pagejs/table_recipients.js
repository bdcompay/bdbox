var oTable;
$(function() {
		//DataTable
		oTable = $('#boxTable')
				.dataTable(
						$.extend(
								dparams,
								{
									"sAjaxSource" : 'getRecipients.do',
									"fnServerData" : retrieveData,// 自定义数据获取函数
									"aoColumns" : [
											{
												"sWidth" : "30px",
												"sClass" : "center",
												"mDataProp" : "id",
												"bSortable" : false
											},
											{
												"sWidth" : "100px",
												"sClass" : "center",
												"mDataProp" : "company",
												"bSortable" : false
											},
											{
												"sWidth" : "100px",
												"sClass" : "center",
												"mDataProp" : "contact",
												"bSortable" : false
											},
											{
												"sWidth" : "80px",
												"sClass" : "center",
												"mDataProp" : "tel",
												"bSortable" : false
											},
											{
												"sWidth" : "100px",
												"sClass" : "center",
												"mDataProp" : "province",
												"bSortable" : false
											},{
												"sWidth" : "100px",
												"sClass" : "center",
												"mDataProp" : "city",
												"bSortable" : false
											},{
												"sWidth" : "100px",
												"sClass" : "center",
												"mDataProp" : "county",
												"bSortable" : false
											},{
												"sWidth" : "100px",
												"sClass" : "center",
												"mDataProp" : "address",
												"bSortable" : false
											},{
												"sWidth" : "60px",
												"sClass" : "center",
												"mDataProp" : "shipperCode",
												"bSortable" : false
											},{
												"sWidth" : "100px",
												"sClass" : "center",
												"mDataProp" : "mobile",
												"bSortable" : false
											},
											{
												"sWidth" : "220px",
												"sClass" : "opera",
												"mDataProp" : "isDefault",
												"fnRender" : function(obj) {
													var id = obj.aData['id'];
													var isDefault = obj.aData['isDefault'];
													var result = "";
													result += "<a href=\"javascript:void(0);\" onclick=\"getRecipientsById("+id+")\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"halflings-icon white edit\"></i>修改</a>&nbsp";
													result += "<a href=\"javascript:void(0);\" onclick=\"deleteRecipients("+id+")\" class=\"btn btn-danger\"><i class=\"halflings-icon trash white\"></i>删除</a>&nbsp";
													if(isDefault==0){
														result += "<a href=\"javascript:void(0);\" onclick=\"update("+id+")\" class=\"btn btn-info\"><i class=\"halflings-icon white edit\"></i>设为默认</a>";
													}
													return result;
												},
												"bSortable" : false
												
											} ]
								}));
		$("#mycheck").click(function test() {
			oTable.fnDraw();
		});
		
		new PCAS('province','city','county','','','');
		
		$("#province, #city").change(function(){
		    var sf=$("#province").val();
		    var cs=$("#city").val();
		    var xc=$("#county").val();
			cschange(sf, cs, xc, "#areaCode");
		});
		
		$("#u_province, #u_city").change(function(){
		    var sf=$("#u_province").val();
		    var cs=$("#u_city").val();
		    var xc=$("#u_county").val();
			cschange(sf, cs, xc, "#updateAreaCode");
		});
		
		
});

//获取区号
function cschange(sf, cs, xc, id){
    for(var i=0;i<sfArr.length;i++){
        //筛选省份
        if(sf!=""){
            if(sfArr[i]!=sf){
                continue;
            }
        }
        var qh = getqh(sf);
        if(qh!=""){
	        $(id).val(qh);
	    }
     	
        //筛选市县
        for(var j=0;j<csArr[i].length;j++){
	        if(cs=="省直辖行政单位" || cs=="县"){
	        	if(csArr[i][j][0]==xc)
	            	$(id).val(csArr[i][j][2]);
		    }
            if(cs!=""){
                if(csArr[i][j][0]!=cs){
                    continue;
                }
            }
	        $(id).val(csArr[i][j][2]);
        }
    }
}

//获取直辖市区号
function getqh(cs){
	switch(cs){
		case "北京市":return "010";break;
		case "重庆市":return "023";break;
		case "上海市":return "021";break;
		case "天津市":return "022";break;
		default: return ""; break;
	}
}

//自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	aoData.push({});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});
}
function deleteRecipients(id){
	if(confirm("确定删除？！")){
		$.post("deleteRecipients.do", {"id": id}, function(data){
			alert(data.message);
			oTable.fnDraw();
		});
	}
}
function getRecipientsById(id){
	$.post("getRecipientsById.do", {"id": id}, function(data){
		$("#id").val(data.result.id);
		$("#company").val(data.result.company);
		$("#contact").val(data.result.contact);
		$("#tel").val(data.result.tel);
		new PCAS('u_province','u_city','u_county', data.result.province, data.result.city, data.result.county);
		$("#address").val(data.result.address);
		$("#shipperCode").val(data.result.shipperCode);
		$("#mobile").val(data.result.mobile);
		$("#isDefault").val(data.result.isDefault);
		$("#updateAreaCode").val(data.result.areaCode);
	});
}

function update(id){
	$.post("updateDefault.do", {"id":id}, function(data){
		alert(data.message);
		oTable.fnDraw();
	});
}

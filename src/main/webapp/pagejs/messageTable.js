/**************************************其它******************************************/
//输入提示
//$.getJSON("queryUsername.do", function(data){
	//$("#username").autocomplete({
		//source:[data.aaData]
	//}); 
//});

/**************************************其它******************************************/
/**************************************DataTable******************************************/
$(function() {
					//DataTable
					var oTable = $('.messageTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'listBoxMessage.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "fromBox",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "toBox",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "content",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "dataStatusType",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "msgIoType",
																	"bSortable" : false
																},
																{
																	"sWidth" : "30px",
																	"sClass" : "center",
																	"mDataProp" : "msgType",
																	"bSortable" : false
																},
																{
																	"sWidth" : "130px",
																	"sClass" : "center",
																	"mDataProp" : "createdTime",
																	"bSortable" : false
																},
																{
																	"sWidth" : "69px",
																	"sClass" : "opera",
																	"mDataProp" : "manager",
																	"fnRender" : function(obj) {
																		var id = obj.aData['id'];
																		var result = "<a class=\"btn btn-large btn-danger\" href=\"javascript:if(confirm('确认要删除吗？'))location=\'deleteBoxMessage?id="+id+"\'\">"
																					+"<i class=\"halflings-icon trash white\"></i>"
																					+"</a>";
																		return result;
																	},
																	"bSortable" : false
																	
																} ]
													}));

					
					$("#mycheck").click(function test() {
						oTable.fnDraw();
					});
					
					//回车
					 $('Input').bind('keypress',function(event){
				            if(event.keyCode == "13")    
				            {
				            	oTable.fnDraw();
				            }
				    });
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	var boxSerialNumber = $("#fromboxSerialNumber").val(); 
	var toboxSerialNumber = $("#toboxSerialNumber").val()
	var familyMob = $("#familyMob").val();
	var familyMail = $("#familyMail").val();
	var boxName = $("#boxName").val();
	var userName = $("#userName").val();
	var startTimeStr = $("#startTimeStr").val();
	var endTimeStr = $("#endTimeStr").val();
	var entUserName = $("#entUserName").val();
	
	aoData.push({
		"name" : "familyMail",
		"value" : familyMail
	}, {
		"name" : "familyMob",
		"value" : familyMob
	},  {
		"name" : "userName",
		"value" : userName
	},  {
		"name" : "boxName",
		"value" : boxName
	},  {
		"name" : "startTimeStr",
		"value" : startTimeStr
	},{
		"name" : "endTimeStr",
		"value" : endTimeStr
	},{
		"name" : "boxSerialNumber",
		"value" : boxSerialNumber
	},{
		"name" : "toboxSerialNumber",
		"value" : toboxSerialNumber
	},{
		"name" : "entUserName",
		"value" : entUserName
	}
	);
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/**************************************DataTable******************************************/
$(".form_date").datetimepicker({
	language: "zh-CN",
	weekStart: 1,
	todayBtn: 1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	minView: 2,
	forceParse: 0,
	format: "yyyy/mm/dd"
});

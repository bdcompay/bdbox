/**************************************其它******************************************/
//输入提示
//$.getJSON("queryUsername.do", function(data){
	//$("#username").autocomplete({
		//source:[data.aaData]
	//}); 
//});

/**************************************其它******************************************/
/**************************************DataTable******************************************/
$(function() {
					//DataTable
					var oTable = $('.userTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'listProductDatatable.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																{
																	"sWidth" : "50px",
																	"sClass" : "center",
																	"mDataProp" : "id",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "name",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "productType",
																	"fnRender" : function(obj) {
																		var productType = obj.aData['productType'];
																		var result = "终端设备";
																		return result;
																	},
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "color",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "price",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "saleNumber",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "stockNumber",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "productBuyType",
																	"fnRender" : function(obj) {
																		var productBuyType = obj.aData['productBuyType'];
																		var result = "无";
																		if(productBuyType=="GENERAL"){
																			result = "普通";
																		}else if(productBuyType=="SHOPPINGRUSH"){
																			result = "抢购";
																		}else if(productBuyType=="RENT"){
																			result = "租用";
																		}
																		return result;
																	},
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "isOpenBuy",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "buyStartTimeStr",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "buyEndTimeStr",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "productUrl",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "explain",
																	"bSortable" : false
																},
																{
																	"sWidth" : "400px",
																	"sClass" : "opera",
																	"mDataProp" : "handle",
																	"fnRender" : function(obj) {
																		var id = obj.aData['id'];
																		var name = obj.aData['name'];
																		var result = "<a href=\"javascript:void(0);\" onclick=\"update("+id+")\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"halflings-icon white edit\"></i>编辑</a>"
																					+"<a class=\"btn btn-danger\" href=\"javascript:void(0);\" onclick=\"deleteProduct('"+id+"','"+name+"')\"><i class=\"halflings-icon trash white\"></i>删除</a>"
																					+"<a href=\"javascript:void(0);\" onclick=\"updateWarehouse("+id+")\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#myModalupdate\"><i class=\"halflings-icon white edit\"></i>库存</a>"
 																		return result;
																	},
																	"bSortable" : false
																	
																} ]
													}));

					
					$("#mycheck").click(function test() {
						oTable.fnDraw();
					});
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	var name = $("#name").val();
	var productTypeStr = $("#productTypeStr").val();
	var productBuyTypeStr = $("#productBuyTypeStr").val();

	aoData.push({
		"name" : "name",
		"value" : name
	}, {
		"name" : "productType",
		"value" : productTypeStr
	},  {
		"name" : "productBuyType",
		"value" : productBuyTypeStr
	});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/**************************************DataTable******************************************/
function deleteProduct(id,name){
	if(!confirm('确认要删除'+name+'吗？')){
		return false;
	}
	$.post("deleteProduct.do",{
		"id":id,
	},function(res){
		if(res.status=="OK"){
			window.location.href=window.location.href;
		}else{
			alert("删除失败！");
			return;
		}
	},"json");
}


function update(id){
	$.post("getProduct.do",{
		"id":id
	},function(res){
		if(res.result.productBuyType=="普通" || res.result.productBuyType=="GENERAL" ){
			$("#updateProductBuyTypeStr option[value='GENERAL']").attr("selected",true);
		}else if(res.result.productBuyType=="抢购" || res.result.productBuyType=="SHOPPINGRUSH" ){
			$("#updateProductBuyTypeStr option[value='SHOPPINGRUSH']").attr("selected",true);
		}else if(res.result.productButType=="租用" || res.result.productBuyType=="RENT"){
			$("#updateProductBuyTypeStr option[value='RENT']").attr("selected",true);
		}
		
		if(res.result.isOpenBuy==true){
			$("#updateIsOpenBuy option[value='true']").attr("selected",true);
		}else{
			$("#updateIsOpenBuy option[value='false']").attr("selected",true);
		}
		if(res.result.productButType=="租用" || res.result.productBuyType=="RENT")
			$("#updateRent").css("display","block");
		else
			$("#updateRent").css("display","none");
		$("#updateId").val(id);
		$("#updateProductname").val(res.result.name);
		$("#updateProductPrice").val(res.result.price);
		$("#updateProductColor").val(res.result.color);
		$("#updateSaleNumber").val(res.result.saleNumber);
		$("#updateStockNumber").val(res.result.stockNumber);
		$("#updateBuyStartTimeStr").val(res.result.buyStartTimeStr);
		$("#updateBuyEndTimeStr").val(res.result.buyEndTimeStr);
		$("#updateProductURL").val(res.result.productURL);
		$("#updateProductExplain").val(res.result.explain);
		$("#updateAntecedent").val(res.result.rentAutonymPrice);
		$("#updateRental").val(res.result.rentPrice);
		$("#updateCouponPrice").val(res.result.couponPrice);
	},"json");
}

function updateWarehouse(id){
	$.post("getProduct1.do",{"id":id},
			function(res){ 
		$("#updateIdd").val(id); 
		$("#updateSaleNumberd").val(res.result.saleNumber);
		$("#updateStockNumberd").val(res.result.stockNumber); 
	},"json");
}

$("#updateSubmit").click(function(){
	var id = $("#updateId").val();
	var name = $("#updateProductname").val();
	var productType = $("#updateProductStatusTypeStr").val();
	var price = $("#updateProductPrice").val();
	var saleNumber = $("#updateSaleNumber").val();
	var stockNumber = $("#updateStockNumber").val();
	var productBuyType = $("#updateProductBuyTypeStr").val();
	var isOpenBuy = $("#updateIsOpenBuy").val();
	var buyStartTimeStr =$("#updateBuyStartTimeStr").val();
	var buyEndTimeStr = $("#updateBuyEndTimeStr").val();
	var productUrl = $("#updateProductURL").val();
	var explain = $("#updateProductExplain").val();
	var color = $("#updateProductColor").val();
	var updateRental = $("#updateRental").val();
	var updateAntecedent = $("#updateAntecedent").val();
	var updateCouponPrice = $("#updateCouponPrice").val();
	 
	if(buyStartTimeStr==""||buyEndTimeStr==""){
		alert("开始购买时间和截止购买日期不能为空！");
		return;
		
	}
	
	if(id.length<1){
		return ;
	}
	
	if(name.length==0){
		alert("产品名称不能为空");
		return false;
	}
	if(price.length==0){
		alert("请输入产品价格");
		return false;
	}
	if(saleNumber.length==0){
		saleNumber = 0;
	}
	
	if(stockNumber.length==0){
		stockNumber = 0;
	}
	
	$.post("updateProduct.do",{
		"id":id,
		"name":name,
		"productType":productType,
		"price":price,
		"saleNumber":saleNumber,
		"stockNumber":stockNumber,
		"productBuyType":productBuyType,
		"isOpenBuy":isOpenBuy,
		"buyStartTimeStr":buyStartTimeStr,
		"buyEndTimeStr":buyEndTimeStr,
		"productUrl":productUrl,
		"color":color,
		"explain":explain,
		"updateRental":updateRental,
		"updateAntecedent":updateAntecedent,
		"updateCouponPrice":updateCouponPrice
	},function(res){
		if(res.status=="OK"){
			alert("修改成功！");
			window.location.href=window.location.href;
		}else{
			alert("修改失败！");
			return;
		}
	},"json");
});




$("#saveSubmit").click(function(){
	var name = $("#saveProductname").val();
	var productType = $("#saveProductStatusTypeStr").val();
	var price = $("#saveProductPrice").val();
	var saleNumber = $("#saveSaleNumber").val();
	var stockNumber = $("#saveStockNumber").val();
	var productBuyType = $("#saveProductBuyTypeStr").val();
	var isOpenBuy = $("#saveIsOpenBuy").val();
	var buyStartTimeStr =$("#saveBuyStartTimeStr").val();
	var buyEndTimeStr = $("#saveBuyEndTimeStr").val();
	var productUrl = $("#saveProductURL").val();
	var explain = $("#saveProductExplain").val();
	var color = $("#saveProductColor").val();
	var rental = $("#rental").val();
	var antecedent = $("#antecedent").val();
	var couponPrice = $("#couponPrice").val();
	
	if(buyStartTimeStr==""||buyEndTimeStr==""){
		alert("开始购买时间和截止购买日期不能为空！");
		return;
		
	}
	if(name.length==0){
		alert("产品名称不能为空");
		return false;
	}
	if(price.length==0){
		alert("请输入产品价格");
		return false;
	}
	if(saleNumber.length==0){
		saleNumber = 0;
	}
	
	if(stockNumber.length==0){
		stockNumber = 0;
	}
	
	$.post("addProduct.do",{
		"name":name,
		"productType":productType,
		"price":price,
		"saleNumber":saleNumber,
		"stockNumber":stockNumber,
		"productBuyType":productBuyType,
		"isOpenBuy":isOpenBuy,
		"buyStartTimeStr":buyStartTimeStr,
		"buyEndTimeStr":buyEndTimeStr,
		"productUrl":productUrl,
		"color":color,
		"explain":explain,
		"rental":rental,
		"antecedent":antecedent,
		"couponPrice":couponPrice
	},function(res){
		if(res.status=="OK"){
			alert("添加成功！");
			window.location.href=window.location.href;
		}else{
			alert("添加失败！");
			return;
		}
	},"json");
});

//验证库存修改数量
var flag1 = false;  
function ckeckstockcount(){
	var updateStockNumberd = $("#updateStockNumberd").val();
	var stocktype = $("#stocktype").val();
	var storageNumberd = $("#storageNumberd").val(); 
	var num = storageNumberd-updateStockNumberd;
	var reg = /^\d*$/;
	if(!reg.test(storageNumberd)){
		$("#storageNumberErrord").text("必须是数字");
		flag1 =  false;
	}else if(storageNumberd<1){
		$("#storageNumberErrord").text("最小数为1");
		$("#storageNumberd").val(1);
		flag1 =  false;
	}else if(storageNumberd>999999){
		$("#storageNumberd").val(999999);
		$("#storageNumberErrord").text("最大数为999999");
		flag1 =  false;
	}else if(stocktype=="getout" && num>0){
		$("#storageNumberErrord").text("取出数量不能大于库存数量");
		flag1 =  false;
	}else {
		$("#storageNumberErrord").text("");
		flag1 =  true;
	}
}


//修改
$(document).on("keyup","#storageNumberd",function(){
	ckeckstockcount();
});

$("#updateSubmits").click(function(){
	ckeckstockcount();
	if(!flag1){
		return false;
	}
	var id = $("#updateIdd").val();
	var stocktype = $("#stocktype").val();
	var storageNumberd = $("#storageNumberd").val(); 
	
	if(id.length<1){
		return ;
	}
	
	$.post("updateWarehouseProduct.do",{
		"id":id, 
		"stocktype":stocktype,
		"storageNumberd":storageNumberd 
	},function(res){
		if(res.status=="OK"){
			alert("操作成功！");
			window.location.href=window.location.href;
		}else{
			alert(res.message);
			
			return;
		}
	},"json");
});

function change(id, showId){
	var flag = $(id).val();
	if(flag=="RENT")
		$(showId).css("display","block");
	else
		$(showId).css("display","none");
}

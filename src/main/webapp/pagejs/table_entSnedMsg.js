$(function() {
	//DataTable
	var oTable = $('.messageTable')
		.dataTable($.extend(
			dparams, {
				"sAjaxSource": 'listEndSendMsd.do',
				"fnServerData": retrieveData, // 自定义数据获取函数
				"aoColumns": [{
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "toBoxSerialNumber",
						"bSortable": false
					},

					{
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "content",
						"bSortable": false
					},
					{
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "createdTimeStr",
						"bSortable": false
					},
					{
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "entUserName",
						"bSortable": false
					}
				]

			}));
	$("#query").click(function test() {
		oTable.fnDraw();
	});
	//回车
	$('Input').bind('keypress', function(event) {
		if(event.keyCode == "13") {
			oTable.fnDraw();
		}
	});
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	var startTimeStr = $('#startTimeStr').val();
	var endTimeStr = $('#endTimeStr').val();
	var toBoxSerialNumber = $('#toBoxSerialNumber').val();
	var entUserName = $('#entUserName').val();
	if(startTimeStr == '' || endTimeStr == '') {
		startTimeStr = todayToDateStr();
		endTimeStr = tomorrowToDateStr();
	}

	aoData.push({ name: "toBoxSerialNumber", value: toBoxSerialNumber }, { name: "entUserName", value: entUserName }, { name: "startTimeStr", value: startTimeStr }, { name: "endTimeStr", value: endTimeStr });
	$.ajax({
		"type": "GET",
		"url": sSource,
		"dataType": "json",
		"data": aoData,
		"success": function(resp) {
			fnCallback(resp);
		},
		"error": function(resp) {}
	});
}

/*********************************  参数验证  ********************************/

/************************************  功能操作  **************************************/
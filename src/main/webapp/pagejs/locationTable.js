/**************************************其它******************************************/
//输入提示
//$.getJSON("queryUsername.do", function(data){
//$("#username").autocomplete({
//source:[data.aaData]
//}); 
//});

/**************************************其它******************************************/
/**************************************DataTable******************************************/
$(function() {
	//DataTable
	var oTable = $('.locationTable')
		.dataTable(
			$.extend(
				dparams, {
					"sAjaxSource": 'listBoxLocation.do',
					"fnServerData": retrieveData, // 自定义数据获取函数
					"aoColumns": [{
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "boxName",
						"bSortable": false
					}, {
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "boxUser",
						"bSortable": false
					}, {
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "boxSerialNumber",
						"bSortable": false
					}, {
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "cardNumber",
						"bSortable": false
					}, {
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "longitude",
						"bSortable": false
					}, {
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "latitude",
						"bSortable": false
					}, {
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "altitude",
						"bSortable": false
					}, {
						"sWidth": "100px",
						"sClass": "center",
						"mDataProp": "speed",
						"bSortable": false
					}, {
						"sWidth": "30px",
						"sClass": "center",
						"mDataProp": "direction",
						"bSortable": false
					}, {
						"sWidth": "130px",
						"sClass": "center",
						"mDataProp": "createdTime",
						"bSortable": false
					}, 
																{
																	"sWidth" : "130px",
																	"sClass" : "center",
																	"mDataProp" : "emergencyLocation",
																	
																	"fnRender" : function(obj) {
																		var en=obj.aData['emergencyLocation'];
																		if (en==true){
																			en='是'
																		}else{
																			en='否'
																		};
																		return en;
																	},
																	
																	"bSortable" : false
																},{
						"sWidth": "69px",
						"sClass": "opera",
						"mDataProp": "manager",
						"fnRender": function(obj) {
							var id = obj.aData['id'];
							var result = "<a class=\"btn btn-large btn-primary\" href=\"javascript:void(0);\" onclick=\"queryLocation(" + id + ")\"><i class=\"halflings-icon globe white\"></i></a>" +
								"<a class=\"btn btn-large btn-danger\" href=\"javascript:if(confirm('确认要删除吗？'))location=\'deleteBoxlocation?id=" + id + "\'\"><i class=\"halflings-icon trash white\"></i></a>";
							return result;
						},
						"bSortable": false

					}]
				}));

	$("#mycheck").click(function test() {
		oTable.fnDraw();
	});

});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	var cardNumber = $("#cardNumber").val();
	var boxName = $("#boxName").val();
	var userName = $("#userName").val();
	var boxSerialNumber = $("#boxSerialNumber").val();
	var startTimeStr = $("#startTimeStr").val().trim();
	var endTimeStr = $("#endTimeStr").val().trim();
	if(startTimeStr.length == 0) {
		startTimeStr = todayToDateStr();
	}
	if(endTimeStr.length == 0) {
		endTimeStr = tomorrowToDateStr();
	}

	aoData.push({
		"name": "cardNumber",
		"value": cardNumber
	}, {
		"name": "boxName",
		"value": boxName
	}, {
		"name": "userName",
		"value": userName
	}, {
		"name": "boxSerialNumber",
		"value": boxSerialNumber
	}, {
		"name": "startTimeStr",
		"value": startTimeStr
	}, {
		"name": "endTimeStr",
		"value": endTimeStr
	});
	$.ajax({
		"type": "GET",
		"url": sSource,
		"dataType": "json",
		"data": aoData,
		"success": function(resp) {
			fnCallback(resp);
		},
		"error": function(resp) {}
	});

}
/**************************************DataTable******************************************/
$(".form_date").datetimepicker({
	language: "zh-CN",
	autoclose: true,
	todayBtn: true,
	format: "yyyy/mm/dd hh:ii"
});
/**************************************百度地图******************************************/
//百度地图API功能
var map = new BMap.Map("allmap");
var point = new BMap.Point(116.404, 39.915); //初始化位置（经纬）
map.centerAndZoom(point, 12); //初始化位置，初始化大小
map.enableScrollWheelZoom(); // 启用滚轮放大缩小，默认禁用
map.enableContinuousZoom(); // 启用地图惯性拖拽，默认禁用

/**导航定位控件Start**/
//添加带有定位的导航控件
var navigationControl = new BMap.NavigationControl({
	// 靠左上角位置
	anchor: BMAP_ANCHOR_TOP_LEFT,
	// LARGE类型
	type: BMAP_NAVIGATION_CONTROL_LARGE,
	// 启用显示定位
	enableGeolocation: true
});
map.addControl(navigationControl);
//添加定位控件
var geolocationControl = new BMap.GeolocationControl();
geolocationControl.addEventListener("locationSuccess", function(e) {
	// 定位成功事件
	var address = '';
	address += e.addressComponent.province;
	address += e.addressComponent.city;
	address += e.addressComponent.district;
	address += e.addressComponent.street;
	address += e.addressComponent.streetNumber;
	alert("当前定位地址为：" + address);
});
geolocationControl.addEventListener("locationError", function(e) {
	// 定位失败事件
	alert(e.message);
});
map.addControl(geolocationControl);
/**导航定位控件End**/

map.addControl(new BMap.ScaleControl()); // 添加比例尺控件
map.addControl(new BMap.OverviewMapControl()); //添加缩略地图控件
map.addControl(new BMap.MapTypeControl()); //添加地图类型控件

//用经纬度设置地图中心点
function queryLocation(id) {
	$.post("getBoxLocation", "id=" + id, function(res) {
		if(res.status == "OK") {
			var longitude = res.result.longitude;
			var latitude = res.result.latitude;
			//经纬不为空时执行
			if(longitude != "" && latitude) {

				//显示地图
				$("body").css("overflow", "hidden"); //禁止滚动
				$(".mask_layer").show(); //背景
				$("#allmap").show().css({
					"z-index": 1000,
					"position": "fixed"
				});

				//位置
				var coords = longitude + "," + latitude;
				//坐标转换
				$.ajax({
					type: "post",
					async: false,
					url: "http://api.map.baidu.com/geoconv/v1/",
					dataType: "jsonp",
					jsonp: "callback", //传递给请求处理程序或页面的，用以获得jsonp回调函数名的参数名(一般默认为:callback)     
					data: {
						ak: "487f3e46069cd9f3592e574167ce885a",
						coords: coords,
						to: 5,
						from: 1
					},
					success: function(res) {
						map.clearOverlays(); //清除标注，避免新旧覆盖物共存。
						var new_point = new BMap.Point(res.result[0].x, res.result[0].y); //地图位置
						map.centerAndZoom(new_point, 14); //  位置
						var marker = new BMap.Marker(new_point); // 创建标注
						map.addOverlay(marker); // 将标注添加到地图中
						marker.setAnimation(BMAP_ANIMATION_BOUNCE); // 跳动的动画
					},
					error: function() {
						alert('查看地图失败');
					}
				});

				//点击背景关闭
				$(".mask_layer").click(function() {
					$("body").css("overflow", ""); //禁止滚动
					$(".mask_layer").hide(); //背景
					$("#allmap").hide();
				});
			} else {
				alert("无定位信息，无法查看地图！");
			}
		} else {
			alert("查询地图异常");
		}
	}, "json");
}

/**************************************百度地图******************************************/
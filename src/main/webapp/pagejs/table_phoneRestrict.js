var oTable;
$(function() {
		//DataTable
		oTable = $('#boxTable').dataTable(
						$.extend(dparams,{
									"sAjaxSource" : 'getAll.do',
									"fnServerData" : retrieveData,// 自定义数据获取函数
									"aoColumns" : [
											{
												"sWidth" : "30px",
												"sClass" : "center",
												"mDataProp" : "id",
												"bSortable" : false
											},
											{
												"sWidth" : "100px",
												"sClass" : "center",
												"mDataProp" : "phoneNum",
												"bSortable" : false
											},
											{
												"sWidth" : "100px",
												"sClass" : "center",
												"mDataProp" : "remark",
												"bSortable" : false
											},
											{
												"sWidth" : "220px",
												"sClass" : "opera",
												"mDataProp" : "cz",
												"fnRender" : function(obj) {
													var id = obj.aData['id'];
													return "<a href=\"javascript:void(0);\" onclick=\"deleteRecipients("+id+")\" class=\"btn btn-danger\"><i class=\"halflings-icon trash white\"></i>删除</a>&nbsp";
												},
												"bSortable" : false
												
											} ]
								}));
		$("#mycheck").click(function test() {
			oTable.fnDraw();
		});
});
//自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	aoData.push({});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});
}
function deleteRecipients(id){
	if(confirm("确定删除？！")){
		$.post("deletePhone.do", {"id": id}, function(data){
			alert(data.message);
			oTable.fnDraw();
		});
	}
}

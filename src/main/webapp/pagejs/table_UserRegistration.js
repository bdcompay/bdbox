/**************************************其它******************************************/
//输入提示
//$.getJSON("queryUsername.do", function(data){
	//$("#username").autocomplete({
		//source:[data.aaData]
	//}); 
//});

/**************************************其它******************************************/
/**************************************DataTable******************************************/
$(function() {
					//DataTable
					var oTable = $('.messageTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'listUserRegistrationtable.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "id",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "telephone",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "mail",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "name",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "birthDate",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "30px",
																			"sClass" : "center",
																			"mDataProp" : "type",
																			"bSortable" : false
																		}, 
																		{
																			"sWidth" : "69px",
																			"sClass" : "opera",
																			"mDataProp" : "record",
																			"fnRender" : function(obj) {
																				var record = obj.aData['record'];
																				var telephone = obj.aData['telephone']; 
																				var name = obj.aData['name']; 
																				var type = obj.aData['type']; 
																				var id = obj.aData['id']; 
																				var result="";
																				if(record=="已通知客户"){
																					result="已通知客户"; 
																				}else{
																					var number="";
																					if(type=="租用登记"){
																						number="2";
																					}else if(type=="购买登记"){
																						number="1";
																					}
		 																			result = "<a href=\"javascript:void(0);\" onclick=\"inform("+id+",'"+telephone+"','"+name+"','"+number+"')\" class=\"btn btn-info\">通知客户</a>";
																				}
																				return result; 
																			} 
																		}]
													}));

					
					$("#mycheck").click(function test() {
						oTable.fnDraw();
					});
					
					
					
					
					
					
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	 
	aoData.push();
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}

function inform(id,telephone,name,number){  
	 $.post("inform.do",{
			"id":id,
			"telephone":telephone,
			"name":name,
			"number":number
		},function(date){ 
			if(date.status=="OK"){
				alert("已经通知客户！");
				window.location.href="table_UserRegistration.html";
			}else{
				alert("通知客户失败，请重新操作！");
				return;
			}
		},"json");  
}
/**************************************DataTable******************************************/
$(".form_date").datetimepicker({
	language: "zh-CN",
	weekStart: 1,
	todayBtn: 1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	minView: 2,
	forceParse: 0,
	format: "yyyy/mm/dd"
});
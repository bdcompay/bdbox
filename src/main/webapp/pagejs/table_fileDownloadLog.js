$(function() {
					//DataTable
					var oTable = $('.messageTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'fileDownloadLogs.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "filename",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "status",
																			"bSortable" : false,
																			"fnRender" : function(obj) {
																				var status = obj.aData['status'];
																				if(status){
																					return "成功";
																				}else{
																					return "失败";
																				}
																			} 
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "downloadTimeStr",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "timeConsumig",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "ip",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "ipArea",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "69px",
																			"sClass" : "opera",
																			"mDataProp" : "username",
																			"bSortable" : false
																		}]
														
													}));

					
					$("#query").click(function test() {
						oTable.fnDraw();
					});
					
					
					
					
					
					
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	var startTimeStr = $('#startTimeStr').val();
	var endTimeStr = $('#endTimeStr').val();
	var username = $('#username').val();
	var filename = $('#filename').val();
	var statusInt = $('#downloadStatus').val();
	
	
	aoData.push({name:"startTimeStr",value:startTimeStr},
			{name:"endTimeStr",value:endTimeStr},
			{name:"username",value:username},
			{name:"statusInt",value:statusInt},
			{name:"filename",value:filename});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});
}
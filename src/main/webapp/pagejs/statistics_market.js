$(function(){
	//初始化
	getData("day");
	
	//点击按钮选择不同的时间段
	$(".statisticType div").click(function(){
		$(this).addClass("active").siblings().removeClass("active");
		var statisticType = $(this).attr("id");
		getData(statisticType);
	});
	
	//自定义时间段查询
	$("#statistic").click(function(){
		getData("day");
	});
});

//获取统计数据
function getData(statisticType){
	$.get("statisticOrder.do", {
		"statisticType": statisticType, 
		"startTime": $("#startTime").val(),
		"endTime": $("#endTime").val()}, 
		function(data){
			//购买统计数据
			var gtitle = [];
			var gcount = [];
			var gnum = 0;
			//租赁统计数据
			var rtitle = [];
			var rcount = [];
			var rnum = 0;
			$.each(data.result, function(i,v){
				if(i == "general"){
					$.each(v, function(key, value){
						gtitle.push(key);
						gcount.push(value);
						gnum += value;
					});

					var option = setOption(gtitle, gcount);
					
					var general = echarts.init(document.getElementById('general'));
					
					general.setOption(option);
					$("#generalNum").text(gnum);
				}else{
					$.each(v, function(key, value){
						rtitle.push(key);
						rcount.push(value);
						rnum += value;
					});

					var option = setOption(rtitle, rcount);
					
					var rent = echarts.init(document.getElementById('rent'));
					
					rent.setOption(option);
					$("#rentNum").text(rnum);
				}
			});
	});
}

//设置统计图样式
function setOption(title, count){
	var option = {
	    color: ['#3398DB'],
	    tooltip : {
	        trigger: 'axis',
	        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
	            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
	        }
	    },
	    grid: {
	        left: '3%',
	        right: '4%',
	        bottom: '3%',
	        containLabel: true
	    },
	    xAxis : [
	        {
	            type : 'category',
	            data : title,
	            axisTick: {
	                alignWithLabel: true
	            }
	        }
	    ],
	    yAxis : [
	        {
	            type : 'value'
	        }
	    ],
	    series : [
	        {
	            name:'数量',
	            type:'bar',
	            barWidth: '60%',
	            data: count
	        }
	    ]
	};
	return option;
}
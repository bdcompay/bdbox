var oTable;
$(function() {
					//DataTable
					oTable = $('.messageTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'getEntBoxs.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "boxSerialNumber",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "name",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "boxStatusTypeStr",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "user",
																			"bSortable" : false
																		},	
																		{
																			"sWidth" : "100px",
																			"sClass" : "center",
																			"mDataProp" : "snNumber",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "69px",
																			"sClass" : "opera",
																			"mDataProp" : "cardNumber",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "69px",
																			"sClass" : "opera",
																			"mDataProp" : "freq",
																			"bSortable" : false
																		},
																		{
																			"sWidth" : "69px",
																			"sClass" : "opera",
																			"mDataProp" : "cardTypeStr",
																			"bSortable" : false
																		},
																		
																		{
																			"sWidth" : "70px",
																			"sClass" : "center",
																			"mDataProp" : "id",
																			"fnRender" : function(obj) {
																				var id = obj.aData['id'];
																				var boxSerialNumber = obj.aData['boxSerialNumber'];
																				
																				var result = "<a href=\"javascript:void(0);\" onclick=\"unbindEntAndBOx("+boxSerialNumber+")\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#updateModal\">解除绑定</a>"; 																		 
																				return result;
																			},
																			"bSortable" : false
																		}]
													}));

					
					$("#query").click(function test() {
						oTable.fnDraw();
					});	
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	var id =  $("#entid").val();
	var boxSerialNumber = $("#boxSerialNumber").val();
	var boxName = $("#boxName").val();
	var boxStatusTypeStr = $("#boxStatusTypeStr").val();
	var userName = $("#userName").val();
	var snNumber = $("#snNumber").val();
	var cardNumber = $("#cardNumber").val();
	
	aoData.push(
			{name:"id",value:id},
			{name:"boxSerialNumber",value:boxSerialNumber},
			{name:"boxName",value:boxName},
			{name:"boxStatusTypeStr",value:boxStatusTypeStr},
			{name:"userName",value:userName},
			{name:"snNumber",value:snNumber},
			{name:"cardNumber",value:cardNumber});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});
}

/*
 * 判断是不是不为空
 * 字符串为null或字符串去掉前后空格后长度为0返回:false
 * 否则返回：true
 */
function is_noEmpty($str){
	if($str==null) return false;
	if($str.trim().length==0) return false;
	return true;
}

//参数验证正确
function showok(element){
	var error = "#"+element+"Error";
	$("#"+element).removeClass('input_error');
	$(error).text("*");
}

//参数验证错误提醒
function showerror(element,errormessage){
	var error = "#"+element+"Error";
	$("#"+element).addClass('input_error');
	$(error).text("*"+errormessage);
}


//参数验证是否通过标志
	var flag1 = false;	//北斗盒子id



//验证北斗盒子ID,只能填写数字和英文“,”逗号。
function checkBoxid($id){
	var reg = /^(\d{2,},)*\d+$/;
	if(!is_noEmpty($("#"+$id).val())){
		showerror($id,"不能为空");
	}else if(!reg.test($("#"+$id).val())){
		showerror($id,"出现非法字符");
	}else{
		showok($id);
		flag1= true;
		return ;
	}
	flag1 = false;
}
$("#bindBox").blur(function(){
	var $id = $.trim($(this).attr("id"));
	checkBoxid($id);
});


//绑定提交
$(document).on("click","#bindSubmit",function(){
	//重复提交
	var disabled = $(this).attr("disabled");
	if(disabled=="disabled") return false;
	//验证参数
	checkBoxid("bindBox");
	if(!(flag1)) return false;
	//禁止按钮，防止重复提交
    $(this).attr("disabled","disabled");
    
    var entid = $("#entid").val();
	var boxids = $("#bindBox").val();
	var data = {"entUsreId":entid,"boxNumbers":boxids};
	$.ajax({
		"type":"POST",
		"url":"bindEntAndBox.do",
		"data":data,
		"datatype":"json",
		"async":false,
		"success":function(res){
			if(res.status=="OK"){
				if(res.result.length>0){
					alert(res.result);
				}
				oTable.fnDraw();
				$("#bindBox").val("");
			}else{
				alert(res.message);
			}
		}
	});
	$(this).removeAttr("disabled");
	$('#bindModal').modal('hide');
});

//解除绑定
function unbindEntAndBOx(boxSerialNumber){
	if(confirm("你确定要解除绑定吗？")){
		var entid = $("#entid").val();
		var data = {"entUsreId":entid,"boxNumber":boxSerialNumber};
		$.ajax({
			"type":"POST",
			"url":"unbindEntAndBox.do",
			"data":data,
			"datatype":"json",
			"async":false,
			"success":function(res){
				if(res.status=="OK"){
					oTable.fnDraw();
				}else{
					alert(res.message);
				}
			}
		});
	}
}
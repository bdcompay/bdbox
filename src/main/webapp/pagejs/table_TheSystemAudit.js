$(function() {
	//DataTable 
   	var oTable = $('.userTable')
			.dataTable(
					$
						 .extend(
							 	dparams, {
										"sAjaxSource" : 'listRealNametable.do',
										"fnServerData" : retrieveData,// 自定义数据获取函数
										"aoColumns" : [
												{
													"sWidth" : "50px",
													"sClass" : "center",
													"mDataProp" : "id",
													"bSortable" : false
												},
												{
													"sWidth" : "100px",
													"sClass" : "center",
													"mDataProp" : "name",
													"bSortable" : false
												}, 
												{
													"sWidth" : "100px",
													"sClass" : "center",
													"mDataProp" : "idcard",
													"bSortable" : false
												},
												{
													"sWidth" : "100px",
													"sClass" : "center",
													"mDataProp" : "phone",
													"bSortable" : false
												},
												{
													"sWidth" : "100px",
													"sClass" : "center",
													"mDataProp" : "headphotourl",
													"bSortable" : false
												},
												{
													"sWidth" : "100px",
													"sClass" : "center",
													"mDataProp" : "zhengphotourl",
													"bSortable" : false
												}, 
												{
													"sWidth" : "100px",
													"sClass" : "center",
													"mDataProp" : "fanphotourl",
													"bSortable" : false
												},
												{
													"sWidth" : "100px",
													"sClass" : "center",
													"mDataProp" : "check",
													"bSortable" : false
												},
												{
													"sWidth" : "100px",
													"sClass" : "center",
													"mDataProp" : "reason",
													"bSortable" : false
												},
												{
													"sWidth" : "100px",
													"sClass" : "opera",
													"mDataProp" : "handle",
													"fnRender" : function(obj) {
														var id = obj.aData['id'];
														var handle = obj.aData['handle'];
														var result="";
 														if(handle=='0'){ 
 															result = "<a class=\"btn btn-primary\" href=\"table_detail.html?id="+id+"\">查看详情</a>"; 																		 
 														  return result;
 														}else if(handle=='1'){
 															result="<a class=\"btn btn-primary\" href=\"table_detail.html?id="+id+"\">查看详情</a>";
 															return result;
 														} 
													},
													"bSortable" : false 
												}]
									}));

	
	$("#mycheck").click(function test() {
		oTable.fnDraw();
	});
					
});


//自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
 

	aoData.push();
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}

 

function detailed(id){
	$.post("detailed.do",{
		"id":id  
	},function(){ 
	},"json");
}


 
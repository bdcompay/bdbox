/**************************************其它******************************************/
 
 
$(function() {
					//DataTable
					var oTable = $('.msgTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'getMsgAll.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																{
																	"sWidth" : "50px",
																	"sClass" : "center",
																	"mDataProp" : "id",
																	"bSortable" : false
																},
																{
																	"sWidth" : "200px",
																	"sClass" : "center",
																	"mDataProp" : "msgContent",
																	"bSortable" : false
																}, 
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "toMob",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "mailType",
																	"bSortable" : false
																},
																{
																	"sWidth" : "80px",
																	"sClass" : "opera",
																	"mDataProp" : "createdTime",
																	"bSortable" : false
																}]
													}));

					
					$("#mycheck").click(function test() {
						oTable.fnDraw();
					});
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) { 
	
	var mob=$("#phoneNum").val();
	var msgContent=$("#msgContent").val();
	var startTime=$("#startTime").val();
	var endTime=$("#endTime").val();
  	
	aoData.push({
		"name" : "mob",
		"value" : mob
		},
		{
		"name" : "content",
		"value" : msgContent
		},
		{
		"name" : "startTime",
		"value" : startTime
		},
		{
		"name" : "endTime",
		"value" : endTime
	});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/**************************************DataTable******************************************/



 
 
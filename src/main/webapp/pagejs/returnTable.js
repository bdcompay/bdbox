/**************************************其它******************************************/
var oTable2;
//信息弹出层
function popupDiv(div_id) {   
    var div_obj = $("#"+div_id);  
    var windowWidth = document.body.clientWidth;       
    var windowHeight = document.body.clientHeight;  
    var popupHeight = div_obj.height();       
    var popupWidth = div_obj.width();    
    //添加并显示遮罩层   
    $("<div id='mask'></div>").addClass("mask")   
                              .width(windowWidth + document.body.scrollWidth)   
                              .height(windowHeight + document.body.scrollHeight)   
                              .click(function() {hideDiv(div_id); })   
                              .appendTo("body")   
                              .fadeIn(200);

							  
    div_obj.css({"position": "fixed"})   
           .animate({left: windowWidth/2-popupWidth/2,    
                     top: windowHeight/2-popupHeight/2, opacity: "show" }, "speedy");   
                    
}   

//信息弹出层隐藏
function hideDiv(div_id) {   
    $("#mask").remove();   
    $("#" + div_id).animate({opacity: "hide" }, "speedy");   
}  

/****************************************************************************/
var flag1 = false;
var flag2 = false;
//退货申请不通过
function checkcause(){
	var $cause =$.trim($('#cause').val());
	if($cause.length==0){
		$("#causeError").text("请输入申请不通过原因");
		flag1 = false;
	}else if($cause.length<5){
		$("#causeError").text("最低长度为5");
		flag1 = false;
	}else{
		$("#causeError").text("");
		flag1 = true;
	}
}
$('#cause').on({
	focus : function(e) {
	},
	blur : function(e) {
		checkcause();
	}
});
//产品审核不通过
function checkproductcause(){
	var $cause =$.trim($('#productcause').val());
	if($cause.length==0){
		$("#productcauseError").text("请输入退货产品不通过原因");
		flag2 = false;
	}else if($cause.length<5){
		$("#productcauseError").text("最低长度为5");
		flag2 = false;
	}else{
		$("#productcauseError").text("");
		flag2 = true;
	}
}
$('#productcause').on({
	focus : function(e) {
	},
	blur : function(e) {
		checkproductcause();
	}
});

/** 退货退款 */
//扣减金额验证
function checkdeductionMoney(){
	var buymoney = $('#buyMoney').val();
	var deductionMoney =$.trim($('#deductionMoney').val());
	var num = deductionMoney - buymoney;
	
	var r = /^\d+(\.\d{2})?$/;
	if(!r.test(deductionMoney)){
		$("#deductionMoneyError").text("有非法字符或格式不对，正确格式：12.00");
		flag1 = false;
	}else if(num>0){
		$("#deductionMoneyError").text("扣减金额只能小于购买金额");
		flag1 = false;
	}else {
		$("#deductionMoneyError").text("");
		returnsmoney();
		flag1 = true;
	}
}
$('#deductionMoney').on({
	focus : function(e) {
	},
	blur : function(e) {
		checkdeductionMoney();
	}
});

//扣减原因
function checkdeductionCause(){
	var deductionCause =$.trim($('#deductionCause').val());
	var deductionMoney =$('#deductionMoney').val();
	if(deductionMoney==0){
		flag2 = true;
		$("#deductionCauseError").text("");
	}else{
		if(deductionCause.length==0){
			$("#deductionCauseError").text("请输入扣减原因");
			flag2 = false;
		}else{
			$("#deductionCauseError").text("");
			flag2 = true;
		}
	}
	
}
$('#deductionCause').on({
	focus : function(e) {
	},
	blur : function(e) {
		checkdeductionCause();
	}
});

//退货金额
function returnsmoney(){
	var buymoney = $('#buyMoney').val();
	var deductionMoney =$('#deductionMoney').val();
	
	if(deductionMoney.length==0){
		deductionMoney = 0;
	}
	var returnmoney = formatNumber((buymoney - deductionMoney),2);
	
	var strs = returnmoney.split(",");
	var restr = "";
	for(var i=0;i<strs.length;i++){
		restr += strs[i];
	}
	$('#returnMoney').val(restr);
}


var rid = "";

//显示填写退货不通过原因弹出层
$(document).on("click",".nopass",function(){
	rid = $(this).attr("fid");
	$.ajax({
		type:"POST",
		url:"getReturnsDto.do",
		data:{"id":rid},
		datatype:"json",
		success:function(res){
			var res2 = res.result;
			if(res2.returnsStatusType=='APPLYAUDIT'){
				return ;
			}
			location.reload();
		}
	});
});

//审核通过
$(document).on("click",".pass",function(){
	rid = $(this).attr("fid");
	var auditResult = "OK";
	auditApply(rid,auditResult);
});

//审核不通过
$(document).on("click","#nopassreturensbtn",function(){
	checkcause();
	if(!flag1){
		return false;
	}
	
	var auditResult = "NO";
	var cause = $("#cause").val();
	auditApply(rid,auditResult,cause);
});

//审核退货申请
function auditApply(id,auditResult,cause){
	var data = {"id":id,"auditResult":auditResult,"cause":cause};
	$.ajax({
		"type":"POST",
		"url":"auditApply.do",
		"data":data,
		"datatype":"json",
		"success":function(res){
			if("OK"==res.status){
				//oTable2.fnDraw();
				window.location.href=window.location.href;
			}else{
				alert("操作失败");
				window.location.href=window.location.href;
			}
		}
	});
}

//确定收货
$(document).on("click", ".confirmreturn", function(e){
	var id = $(this).attr("fid");
	var type = "GENERAL";
	$.ajax({
		"type":"POST",
		"url":"receiveProduct.do",
		"data":{"id":id,"type":type},
		"datatype":"json",
		"success":function(res){
			if("OK"==res.status){
				oTable2.fnDraw();
			}else{
				alert("操作失败");
				oTable2.fnDraw();
			}
		}
	});
});

//产品审核不通过弹出层
$(document).on("click",".productnopass",function(e){
	rid = $(this).attr("fid");
	//popupDiv('auditnopass');
	
});

//产品有问题，退货失败
$(document).on("click","#auditnopassbtn",function(e){
	checkproductcause();
	if(!flag2){
		return false;
	}
	var auditResult = "NO";
	var explain = $("#productcause").val();
	$.ajax({
		"type":"POST",
		"url":"auditProduct.do",
		"data":{"id":rid,"auditResult":auditResult,"explain":explain},
		"datatype":"json",
		"success":function(res){
			if("OK"==res.status){
				window.location.href=window.location.href;
			}else{
				alert("操作失败");
			}
		}
	});
});

//产品审核通过，退款弹出层
$(document).on("click",".productpass",function(e){
	rid = $(this).attr("fid");
	$.ajax({
		"type":"POST",
		"url":"getReturns.do",
		"data":{"id":rid},
		"datatype":"json",
		"success":function(res){
			if("OK"==res.status){
				
				var result = res.result;
				
				
				var fnum = formatNumber(result.buyMoney,2);
				
				
				var strs = fnum.split(",");
				var restr = "";
				for(var i=0;i<strs.length;i++){
					restr += strs[i];
				}
				$("#buyMoney").val(restr);
				$("#payType").val(result.payType);
				$("#orderNo").val(result.orderNum);
				returnsmoney();
			}else{
				alert("操作失败");
				return false;
			}
		}
	});
	//popupDiv('returnrefund');
	
});

//退货退款
$(document).on("click","#returnrefundbtn",function(e){
	checkdeductionMoney();
	checkdeductionCause();
	if(!flag1||!flag2){
		return false;
	}
	var id = rid;
	var buyMoney = $("#buyMoney").val();
	var deductionMoney = $("#deductionMoney").val();
	var deductionCause = $("#deductionCause").val();
	var returnMoney = $("#returnMoney").val();
	var payType = $("#payType").val();
 	if(payType == "WECHAT"){ 
		$.ajax({
			"type":"POST",
			"url":"returnsRefund.do",
			"data":{"id":id,"buyMoney":buyMoney,"deductionMoney":deductionMoney,"deductionCause":deductionCause,"returnMoney":returnMoney},
			"datatype":"json",
			"success":function(res){
				if("OK"==res.status){
					//oTable2.fnDraw();
					window.location.href=window.location.href;
				}else{
					alert("操作失败");
					//oTable2.fnDraw();
					window.location.href=window.location.href;
				}
			}
		});
	} else if(payType == "ALIPAY"){
		$("#totalFee").val(returnMoney);
		$("#refundForm").submit();
		window.setTimeout(show,3000); 
		window.location.href=window.location.href;
	} else if(payType == "UNIONPAY"){
		var orderNo = $("#orderNo").val();
		$.get("unionpay/refund", {"orderId":orderNo}, function(data){
			alert(data.message);
			oTable2.fnDraw();
		});
	}
	
 
	
});

//查看物流
$(document).on("click",".querylogistics",function(){
	var id = $(this).attr("fid");
	getExpress(id);
});

//跳转页面查看物流
function checkExpress(expressName, expressNum){
	window.open("http://www.kuaidi100.com/chaxun?com="+expressName+"&nu="+expressNum+"");
}


//显示物流信息
function getExpress(id){
    //清空表格信息
	$("#tab").empty();
	//发起物流信息查询
	$.post("getExpressDtoById.do","id="+id,function(res){
			var result = res.result.express;
			var message = result.expressMessage;
			var express_info = "";
			if(message!=null){
				message = "{"+message+"}";
				var msg = eval('('+message+')');
				var ex_max_len = msg.express.length;
				var express = new Array(ex_max_len);
				$.each(msg.express, function(i, v){
					if(i == ex_max_len-1){
						express[ex_max_len-1] = "<tr>"+
							"<td class='row1'>"+v.time+"</td>"+
							"<td class='status-first'>&nbsp;</td>"+
							"<td>"+v.content+"</td>"+
						"</tr>";
					} else if(i == 0 && result.expressStatus != "签收"){
						express[0] = "<tr class='last'>"+
							"<td class='row1' style='color: #FF8c00;'>"+v.time+"</td>"+
							"<td class='status-wait'>&nbsp;</td>"+
							"<td style='color: #FF8c00;'>"+v.content+"</td>"+
						"</tr>";
					} else if(i == 0 && result.expressStatus == "签收"){
						express[0] = "<tr class='last'>"+
							"<td class='row1' style='color: #FF8c00;'>"+v.time+"</td>"+
							"<td class='status-check'>&nbsp;</td>"+
							"<td style='color: #FF8c00;'>"+v.content+"</td>"+						
							"</tr>";
					} else {
						express[i] = "<tr>"+
							"<td class='row1'>"+v.time+"</td>"+
							"<td class='status'>&nbsp;</td>"+
							"<td>"+v.content+"</td>"+
						"</tr>";
					}
				});
				for(var i=express.length-1; i>=0 ;i--){
					express_info += express[i];
				}
			}else {
				express_info = "<div style='color: #FF8c00; text-align: center; font-size: 18px; font-weight: bold;'>暂无物流信息...</div>";
			}

			$("#tab").append(express_info);
			//点击查看物流模态框
			//popupDiv('exppop_div');
		},"json");
}

//查询退款信息
$(document).on("click",".queryrefund",function(){
	var id = $(this).attr("fid");
	$.ajax({
		"type":"POST",
		"url":"queryReturnsRegund.do",
		"data":{"id":id},
		"datatype":"json",
		"success":function(res){
			if("OK"==res.status){
				oTable2.fnDraw();
				//window.location.href=window.location.href;
			}else{
				alert("退货正在处理中...");
				oTable2.fnDraw();
			}
		}
	});
});

//查看退货详情
$(document).on("click",".querydetail",function(){
	var id = $(this).attr("fid");
	$.ajax({
		"type":"POST",
		"url":"getReturnsDto.do",
		"data":{"id":id},
		"datatype":"json",
		"success":function(res){
			var result = res.result;
			$('#rid').val(result.id);
			
			 $('#detailreturnsNum').val(result.returnsNum);
			 $('#detailorderNum').val(result.orderNum);
			 $('#detailordreId').val(result.ordreId);
			 $('#detailuserId').val(result.userId);
			 $('#detailproductId').val(result.productId);
			 
			 $('#detailproductName').val(result.productName);
			 $('#detailbuyTime').val(result.buyTime);
			 $('#detailcount').val(result.count);
			 $('#detailprice').val(result.price);
			 $('#detailreturnMoney').val(result.returnMoney);
			 
			 $('#detailreturnsCause').val(result.returnsCause);
			 
			var returnsStatusType = result.returnsStatusType;
			var returnsStatusTypeStr = "";
		
			if(returnsStatusType=="APPLYAUDIT"){
				returnsStatusTypeStr = "申请审核中";
			}else if(returnsStatusType=="CANCEL"){
				returnsStatusTypeStr = "已取消";
			}else if(returnsStatusType=="APPLYPASS"){
				returnsStatusTypeStr = "申请通过";
			}else if(returnsStatusType=="APPLYNOTPASS"){
				returnsStatusTypeStr = "申请未通过";
			}else if(returnsStatusType=="EXPRESS"){
				returnsStatusTypeStr = "物流配送中";
			}else if(returnsStatusType=="AUDIT"){
				returnsStatusTypeStr = "商品审核中";
			}else if(returnsStatusType=="AUDITPASS"){
				returnsStatusTypeStr = "申请通过";
			}else if(returnsStatusType=="AUDITNOTPASS"){
				returnsStatusTypeStr = "审核未通过";
			}else if(returnsStatusType=="REFUND"){
				returnsStatusTypeStr = "退款中";
			}else if(returnsStatusType=="FINISH"){
				returnsStatusTypeStr = "已完成";
			}else if(returnsStatusType=="REFUNDFAIL"){
				returnsStatusTypeStr = "退款失败";
			}
			 $('#detailreturnsStatusType').val(returnsStatusTypeStr);
			 
			 $('#detailapplySubmitTime').val(result.applySubmitTimeStr);
			 $('#detailapplyAuditTime').val(result.applyAuditTimeStr);
			 $('#detailexpressNum').val(result.expressNum);
			 
			 $('#detailexpressNumWriteTime').val(result.expressNumWriteTimeStr);
			 $('#detailexpressId').val(result.expressId);
			 $('#detailbuyMoney').val(result.buyMoney);
			 $('#detaildeductionMoney').val(result.deductionMoney);
			 $('#detaildeductionCause').val(result.deductionCause);
			 
			 $('#detailrefundId').val(result.refundId);
			 $('#detailfinishTime').val(result.finishTimeStr);
			 $('#detailexplain').val(result.explain);
		}
	});
	
	//popupDiv('detailpop_div');
});


//退出退货详情
$(document).on("click",".returnrefundbtn",function(){
	//hideDiv('detailpop_div');
});


/**************************************其它******************************************/

$(function() {
		//DataTable
		  oTable2 = $('.orderTable').dataTable($.extend(
										dparams,
										{
											"sAjaxSource" : 'listReturens.do',
											"fnServerData" : retrieveData2,// 自定义数据获取函数
											"aoColumns" : [
													{
														"sWidth" : "150px",
														"sClass" : "center",
														"mDataProp" : "returnsNum",
														"bSortable" : false
													},
													{
														"sWidth" : "120px",
														"sClass" : "center",
														"mDataProp" : "orderNum",
														"bSortable" : false
													},
													{
														"sWidth" : "70px",
														"sClass" : "center",
														"mDataProp" : "productName",
														"bSortable" : false,
														 "fnRender" : function(obj) {
																var productName = obj.aData['productName'];
																result = '<div style="width:70px;word-wrap:break-word ">'+productName+'</div>';
																return result;
															} 
													},
													{
														"sWidth" : "60px",
														"sClass" : "center",
														"mDataProp" : "username",
														"bSortable" : false
													},
													{
														"sWidth" : "60px",
														"sClass" : "center",
														"mDataProp" : "price",
														"bSortable" : false
													},
													{
														"sWidth" : "50px",
														"sClass" : "center",
														"mDataProp" : "count",
														"bSortable" : false
													},
													{
														"sWidth" : "80px",
														"sClass" : "center",
														"mDataProp" : "applySubmitTimeStr",
														"bSortable" : false
													},
													{
														"sWidth" : "80px",
														"sClass" : "center",
														"mDataProp" : "returnsCause",
														"bSortable" : false,
														"fnRender" : function(obj) {
															var returnsCause = obj.aData['returnsCause'];
															result = '<div style="width:60px;word-wrap:break-word ">'+returnsCause+'</div>'
															return result;
														}
													},
													{
														"sWidth" : "50px",
														"sClass" : "center",
														"mDataProp" : "buyMoney",
														"bSortable" : false
													},
													{
														"sWidth" : "50px",
														"sClass" : "center",
														"mDataProp" : "deductionMoney",
														"bSortable" : false
													},
													{
														"sWidth" : "80px",
														"sClass" : "center",
														"mDataProp" : "deductionCause",
														"bSortable" : false,
														"fnRender" : function(obj) {
															var deductionCause = obj.aData['deductionCause'];
															result = '<div style="width:80px;word-wrap:break-word ">'+deductionCause+'</div>'
															return result;
														}
													},
													{
														"sWidth" : "50px",
														"sClass" : "center",
														"mDataProp" : "returnMoney",
														"bSortable" : false
													},
													{
														"sWidth" : "120px",
														"sClass" : "center",
														"mDataProp" : "expressNum",
														"bSortable" : false
														
													},
													{
														"sWidth" : "120px",
														"sClass" : "center",
														"mDataProp" : "returnsStatusType",
														"fnRender" : function(obj) {
															var productBuyType = obj.aData['returnsStatusType'];
															var result = "无";
										
															if(productBuyType=="APPLYAUDIT"){
																result = "申请审核中";
															}else if(productBuyType=="CANCEL"){
																result = "已取消";
															}else if(productBuyType=="APPLYPASS"){
																result = "申请通过";
															}else if(productBuyType=="APPLYNOTPASS"){
																result = "申请未通过";
															}else if(productBuyType=="EXPRESS"){
																result = "物流配送中";
															}else if(productBuyType=="AUDIT"){
																result = "商品审核中";
															}else if(productBuyType=="AUDITPASS"){
																result = "申请通过";
															}else if(productBuyType=="AUDITNOTPASS"){
																result = "审核未通过";
															}else if(productBuyType=="REFUND"){
																result = "退款中";
															}else if(productBuyType=="FINISH"){
																result = "已完成";
															}else if(productBuyType=="REFUNDFAIL"){
																result = "退款失败";
															}
															return result;
														},
														"bSortable" : false
													},
													{
														"sWidth" : "150px",
														"sClass" : "center",
														"mDataProp" : "id",
														"fnRender" : function(obj) {
															
															var explain = obj.aData['explain'];
															var id = obj.aData['id'];
															var productBuyType = obj.aData['returnsStatusType'];
															var expressNum = obj.aData['expressNum'];
															var expressName = obj.aData['expressName'];
															if(expressName == "" || expressName == null){
																expressName = "shunfeng";
															}
															if(productBuyType=="APPLYAUDIT"  || productBuyType=="申请审核中"){
																var result = "";
																result+="<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor pass\" data-toggle=\"modal\"  data-target=\"#pass\">通过</a>";
																result+="\t<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor nopass\" data-toggle=\"modal\"  data-target=\"#nopasspop-div\">不通过</a>";
																return result;
															}else if(productBuyType=="CANCEL" || productBuyType=="已取消"){
																//return "<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor confirmreturns\" >确定退货</a>"
																//result = "已取消";
															}else if(productBuyType=="APPLYPASS" || productBuyType=="申请通过"){
																return "<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor nopass\"  data-toggle=\"modal\"  data-target=\"#nopasspop-div\">修改状态</a>";
																//result = "申请通过";
															}else if(productBuyType=="APPLYNOTPASS"  || productBuyType=="申请未通过"){
																return "<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor pass\" >修改状态</a>";
																//result = "申请未通过";
															}else if(productBuyType=="EXPRESS" || productBuyType=="物流配送中"){
																var result = "";
																result+="<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor confirmreturn\" >确定收货</a>";
																//result+="\t<a href=\"javascript:void(0);\" fid="+id+" onclick='checkExpress("+expressNum+")' class=\"fontcolor querylogistics\" data-toggle=\"modal\"  data-target=\"#show_exp\" >查看物流</a>";
																result+="\t<a href=\"javascript:void(0);\" fid="+id+" onclick=\"checkExpress('"+expressName+"', "+expressNum+")\" class=\"fontcolor\">查看物流</a>";
																return result;
																//result = "物流配送中";
															}else if(productBuyType=="AUDIT" || productBuyType=="商品审核中"){
																var result = "";
																result+="<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor productpass\" data-toggle=\"modal\"  data-target=\"#productpass\" >产品通过</a>";
																result+="\t<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor productnopass\"  data-toggle=\"modal\"  data-target=\"#productnopass\" >产品不通过</a>";
																return result;
																//result = "商品审核中";
															}else if(productBuyType=="AUDITPASS" || productBuyType=="审核通过"){
																//result = "审核通过";
															}else if(productBuyType=="AUDITNOTPASS" || productBuyType=="审核未通过"){
																return "<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor productpass\" data-toggle=\"modal\"  data-target=\"#productpass\" >产品通过</a>";
																//return "<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor querydetail\" data-toggle=\"modal\"  data-target=\"#saveModal\" >查看详细</a>";
																//result = "审核未通过";
															}else if(productBuyType=="REFUND" || productBuyType=="退款中"){
																return "<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor queryrefund\"  data-toggle=\"modal\"  data-target=\"#queryrefund\" >查询退款</a>";
																//result = "退款中";
															}else if(productBuyType=="FINISH" || productBuyType=="已完成"){
																return "<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor querydetail\" data-toggle=\"modal\"  data-target=\"#saveModal\"  >查看详细</a>";
																//result = "已完成";
															}else if(productBuyType=="REFUNDFAIL" || productBuyType=="退款失败"){
																var result = "";
																result+="<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor productpass\" data-toggle=\"modal\"  data-target=\"#productpass\" >产品通过</a>";
																result+="\t<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor productnopass\"  data-toggle=\"modal\"  data-target=\"#productnopass\" >产品不通过</a>";
																return result;
																//"<a href=\"javascript:void(0);\" fid="+id+" class=\"fontcolor querydetail\" data-toggle=\"modal\"  data-target=\"#saveModal\" >查看详细</a>";
																//result = "退款失败";
															}
															return "";
														
														},
														"bSortable" : false 
													}]
										}));
		
		$("#mycheck").click(function test() {
			oTable2.fnDraw();
		});
		
});

//审核退款
function shipmentsOrder(orderNo) {
 	if (window.confirm('该操作是执行退款审核，确定要执行么？')) {
 		//alert(orderNo);
		$.post("shipmentsOrder.do","orderNo="+orderNo,function(data){ 
 			if(data=='true'){
				alert("操作成功！");
				//oTable2.fnDraw();
				window.location.href=window.location.href;
 			}else{
 				alert("操作失败！");
 				//oTable2.fnDraw();
 				window.location.href=window.location.href;
			} 
		});  	
	}
}

/*//退款
function refund(orderNo) {
 	if (window.confirm('该操作是执行退款，确定要退款么？')) {
		$.post("refund.do","orderNo="+orderNo,function(data){ 
 			if(data=='true'){
				alert("操作成功！");
				oTable2.fnDraw();
				//window.location.href=window.location.href;
 			}else{
 				alert("操作失败！");
 				oTable2.fnDraw();
 				//window.location.href=window.location.href;

			} 
		});  	
	}
}*/

/*//查询退款
function queryRefund(orderNo) {
 		$.post("queryRefund.do","orderNo="+orderNo,function(data){ 
 			if(data=='true'){
				alert("操作成功！");
				oTable2.fnDraw();
				//window.location.href=window.location.href;
 			}else{
 				alert("操作失败！");
 				oTable2.fnDraw();
 				//window.location.href=window.location.href;

			} 
		});  	
 }
*/
//显示物流信息
/*function getExpress(){
	var oid = $("#proId").val();
    //清空表格信息
	$("#tab").empty();
	//发起物流信息查询
	$.post("getExpressInfo.do","oid="+oid,function(result){
			var message = result.expressMessage;
			var express_info = "";
			if(message!=null){
				message = "{"+message+"}";
				var msg = eval('('+message+')');
				var ex_max_len = msg.express.length;
				var express = new Array(ex_max_len);
				$.each(msg.express, function(i, v){
					if(i == ex_max_len-1){
						express[ex_max_len-1] = "<tr>"+
							"<td class='row1'>"+v.time+"</td>"+
							"<td class='status-first'>&nbsp;</td>"+
							"<td>"+v.context+"</td>"+
						"</tr>";
					} else if(i == 0 && result.expressStatus != "签收"){
						express[0] = "<tr class='last'>"+
							"<td class='row1' style='color: #FF8c00;'>"+v.time+"</td>"+
							"<td class='status-wait'>&nbsp;</td>"+
							"<td style='color: #FF8c00;'>"+v.context+"</td>"+
						"</tr>";
					} else if(i == 0 && result.expressStatus == "签收"){
						express[0] = "<tr class='last'>"+
							"<td class='row1' style='color: #FF8c00;'>"+v.time+"</td>"+
							"<td class='status-check'>&nbsp;</td>"+
							"<td style='color: #FF8c00;'>"+v.context+"</td>"+
						"</tr>";
					} else {
						express[i] = "<tr>"+
							"<td class='row1'>"+v.time+"</td>"+
							"<td class='status'>&nbsp;</td>"+
							"<td>"+v.context+"</td>"+
						"</tr>";
					}
				});
				for(var i=express.length-1; i>=0 ;i--){
					express_info += express[i];
				}
			}else {
				express_info = "<div style='color: #FF8c00; text-align: center; font-size: 18px; font-weight: bold;'>暂无物流信息...</div>";
			}
			$("#tab").append(express_info);
		},"json");
}*/

//自定义数据获取函数
function retrieveData2(sSource, aoData, fnCallback) {
	var returnsNum = $("#proNumbers").val();//用户
	var orderNum = $("#ordNumber").val();//订单编号
	var returnsStatus = $("#returnsStatus").val();//订单状态
	var startTimeStr = $("#startTimeStr").val();
	var endTimeStr = $("#endTimeStr").val();
	var orderstutas = $("#orderstutas").val(); 
 	aoData.push({
		"name" : "returnsNum",
		"value" : returnsNum
	}, {
		"name" : "orderNum",
		"value" : orderNum
	}, {
		"name" : "returnsStatus",
		"value" : returnsStatus
	}, {
		"name" : "startTimeStr",
		"value" : startTimeStr
	}, {
		"name" : "endTimeStr",
		"value" : endTimeStr
	},{
		"name" : "orderstutas",
		"value" : orderstutas
	});
$.ajax({
"type" : "GET",
"url" : sSource,
"dataType" : "json",
"data" : aoData,
"success" : function(resp) {
fnCallback(resp);
},
"error" : function(resp) {
}
});

}

//数字格式化，如：2000000格式化为 2,000,000
function formatNumber(num, precision, separator) {
    var parts;
    // 判断是否为数字
    if (!isNaN(parseFloat(num)) && isFinite(num)) {
        // 把类似 .5, 5. 之类的数据转化成0.5, 5, 为数据精度处理做准, 至于为什么
        // 不在判断中直接写 if (!isNaN(num = parseFloat(num)) && isFinite(num))
        // 是因为parseFloat有一个奇怪的精度问题, 比如 parseFloat(12312312.1234567119)
        // 的值变成了 12312312.123456713
        num = Number(num);
        // 处理小数点位数
        num = (typeof precision !== 'undefined' ? num.toFixed(precision) : num).toString();
        // 分离数字的小数部分和整数部分
        parts = num.split('.');
        // 整数部分加[separator]分隔, 借用一个著名的正则表达式
        parts[0] = parts[0].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + (separator || ','));

        return parts.join('.');
    }
    return NaN;
}
/**************************************DataTable******************************************/
$(".form_date").datetimepicker({
	language: "zh-CN",
	weekStart: 1,
	todayBtn: 1,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	minView: 2,
	forceParse: 0,
	format: "yyyy/mm/dd"
});

$(function(){
	var busLicStatus = $("#busLicStatus").val();
	var taxRegStatus = $("#taxRegStatus").val();
	var generalTaxStatus = $("#generalTaxStatus").val();
	var orgCodeStauts = $("#orgCodeStauts").val();
	var invoiceDatumStatus = $("#invoiceStatus").val();
	var busLicRemark = $("#i_busLicRemark").val();
	var taxRegRemark = $("#i_taxRegRemark").val();
	var orgCodeRemark = $("#i_orgCodeRemark").val();
	var generalTaxRemark = $("#i_generalTaxRemark").val();
	var invoiceDatumRemark = $("#i_invoiceDatumRemark").val();
	
	if(busLicRemark!=" "){
		$("#busLicRemark").val(busLicRemark);
	}
	if(taxRegRemark!=" "){
		$("#taxRegRemark").val(taxRegRemark);
	}
	if(orgCodeRemark!=" "){
		$("#orgCodeRemark").val(orgCodeRemark);
	}
	if(generalTaxRemark!=" "){
		$("#generalTaxRemark").val(generalTaxRemark);
	}
	if(invoiceDatumRemark!=" "){
		$("#invoiceDatumRemark").val(invoiceDatumRemark);
	}
	if(busLicStatus == "true"){
		$("#b_i_ed").attr("checked", true);
		$("#b_l_ed").find("span").addClass("checked");
	}else{
		$("#b_i_no").attr("checked", true);
		$("#b_l_no").find("span").addClass("checked");
	}
	
	if(taxRegStatus == "true"){
		$("#t_i_ed").attr("checked",true);
		$("#t_l_ed").find("span").addClass("checked");
	}else{
		$("#t_i_no").attr("checked",true);
		$("#t_l_no").find("span").addClass("checked");
	}
	
	if(orgCodeStauts == "true"){
		$("#o_i_ed").attr("checked",true);
		$("#o_l_ed").find("span").addClass("checked");
	}else{
		$("#o_i_no").attr("checked",true);
		$("#o_l_no").find("span").addClass("checked");
	}
	
	if(generalTaxStatus == "true"){
		$("#g_i_ed").attr("checked",true);
		$("#g_l_ed").find("span").addClass("checked");
	}else{
		$("#g_i_no").attr("checked",true);
		$("#g_l_no").find("span").addClass("checked");
	}
	
	if(invoiceDatumStatus == "true"){
		$("#i_i_ed").attr("checked",true);
		$("#i_l_ed").find("span").addClass("checked");
	}else{
		$("#i_i_no").attr("checked",true);
		$("#i_l_no").find("span").addClass("checked");
	}
	var flag = false;
	$("#sub").click(function(){
		var id = $("#qcId").val();
		var invoice = $("#invoice").val();
		var busLic = $("#busLic input:checked").val();
		var taxReg = $("#taxReg input:checked").val();
		var orgCode = $("#orgCode input:checked").val();
		var generalTax = $("#generalTax input:checked").val();
		var invoiceDatum = $("#invoiceDatum input:checked").val();
		var busLicRemark = $("#busLicRemark").val();
		var taxRegRemark = $("#taxRegRemark").val();
		var orgCodeRemark = $("#orgCodeRemark").val();
		var generalTaxRemark = $("#generalTaxRemark").val();
		var invoiceDatumRemark = $("#invoiceDatumRemark").val();
		flag = true;
		if($("#busLic input:checked").length==0 || $("#taxReg input:checked").length==0 || 
				$("#orgCode input:checked").length==0 || $("#generalTax input:checked").length==0){
			alert("审核后请勾选通过或者不通过！");
			flag = false;
		}
		if(busLic == 0){
			if(busLicRemark == ""){
				alert("请输入营业执照证不通过原因！");
				flag = false;
			}
		}
		if(taxReg == 0){
			if(taxRegRemark == ""){
				alert("请输入税务登记证不通过原因！");
				flag = false;
			}
		}
		if(orgCode == 0){
			if(orgCodeRemark == ""){
				alert("请输入组织机构代码证不通过原因！");
				flag = false;
			}
		}
		if(generalTax == 0){
			if(generalTaxRemark == ""){
				alert("请输入一般纳税人资格证不通过原因！");
				flag = false;
			}
		}
		if(invoiceDatum == 0){
			if(invoiceDatumRemark == ""){
				alert("请输入开票资料不通过原因！");
				flag = false;
			}
		}
		if(flag){
			$.post("updateQC.do",{
				"id":id,
				"invoice":invoice,
				"busLicStatus":busLic==1?true:false,
				"taxRegStatus":taxReg==1?true:false,
				"orgCodeStauts":orgCode==1?true:false,
				"generalTaxStatus":generalTax==1?true:false,
				"invoiceDatumStatus":invoiceDatum==1?true:false,
				"busLicRemark":busLicRemark,
				"taxRegRemark":taxRegRemark,
				"orgCodeRemark":orgCodeRemark,
				"generalTaxRemark":generalTaxRemark,
				"invoiceDatumRemark":invoiceDatumRemark
			}, function(data){
				if(data.status == "OK")
					window.location.href = "table_ticketCheck.html";
				else
					alert("保存失败！");
			});
		}
	});
	
	
	
	$(".auditImg input").click(function(){
		var id = $(this).attr("id");
		var q_si = id.substring(0,4);
		var h_er = id.substring(4,6);
		if(h_er == "ed"){
			$("#"+q_si+"no").attr("checked", false);
			$("#"+q_si+"no").parent("span").removeClass("checked");
		}else{
			$("#"+q_si+"ed").attr("checked", false);
			$("#"+q_si+"ed").parent("span").removeClass("checked");
		}
	});
});


/**************************************其它******************************************/
//输入提示
$.getJSON("listAllUser.do", function(data){
	$("#updateUser").autocomplete({
		source:[data.result]
	}); 
});

/**************************************其它******************************************/
/**************************************DataTable******************************************/
$(function() {
					//DataTable
					var oTable = $('#boxTable')
							.dataTable(
									$
											.extend(
													dparams,
													{
														"sAjaxSource" : 'activity.do',
														"fnServerData" : retrieveData,// 自定义数据获取函数
														"aoColumns" : [
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "id",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "status",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "startTime",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "endTime",
																	"bSortable" : false
																},
																{
																	"sWidth" : "100px",
																	"sClass" : "center",
																	"mDataProp" : "remark",
																	"bSortable" : false
																},
																{
																	"sWidth" : "200px",
																	"sClass" : "opera",
																	"mDataProp" : "manager",
																	"fnRender" : function(obj) {
																		var id = obj.aData['id'];
																		var result = "<a href=\"javascript:void(0);\" onclick=\"update("+id+")\" class=\"btn btn-info\" data-toggle=\"modal\" data-target=\"#myModal\"><i class=\"halflings-icon white edit\"></i>修改活动</a>"
  																		return result;
																	},
																	"bSortable" : false
																	
																} ]
													}));

					
					$("#mycheck").click(function test() {
						oTable.fnDraw();
					});
					
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
 
	
	aoData.push({});
	$.ajax({
		"type" : "GET",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
		},
		"error" : function(resp) {
		}
	});

}
/**************************************DataTable******************************************/
function update(id){ 
	$.post("getActivity.do",{"id":id},function(res){
		if(res.result.status=="抢购"){
			$("#updateCardType option[value='RUSH']").attr("selected",true);
		}else if(res.result.status=="预约"){
			$("#updateCardType option[value='ORDER']").attr("selected",true);
		}else if(res.result.status=="普通购买"){
			$("#updateCardType option[value='COMMONBUY']").attr("selected",true);
		}   
		$("#startTimeStr").val(res.result.startTime);
		$("#endTimeStr").val(res.result.endTime);
		$("#updateFamily").val(res.result.id);  

	},"json");
}

$("#updateSubmit").click(function(){
	var updateCardType = $("#updateCardType").val();
	var updateFamily = $("#updateFamily").val();
	var startTimeStr = $("#startTimeStr").val();
	var endTimeStr = $("#endTimeStr").val();  
	if(startTimeStr==""||endTimeStr==""){
		alert("请输入活动的开始时间和结束时间！");
		return;
	}
	if(startTimeStr!=""&&endTimeStr!=""){
	var begin = new Date(startTimeStr);
		var end = new Date(endTimeStr);
		if(begin>end){
			alert("活动的结束时间不能小于开始时间，请重新输入！");
			return ;
		}
	}
	$.post("update_activity.do",{
		"id":updateFamily,
		"status":updateCardType,
		"startTime":startTimeStr,
		"endTime":endTimeStr  
	},function(res){
		if(res.status=="OK"){
			alert("修改成功！");
			window.location.href=window.location.href;
		}else{
			alert("修改失败！");
			return;
		}
	},"json");
	
});

$(".form_date").datetimepicker({
	language: "zh-CN",
	autoclose: true,
    todayBtn: true,
	format: "yyyy-mm-dd hh:ii:ss"
});

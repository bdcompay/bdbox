var geoMap=function(){}

/*初始化地图函数*/
geoMap.prototype.Init=function(){}
/*地图居中*/
geoMap.prototype.TakeCenter=function(longtitude/*经度*/, latitude/*纬度*/ ){}
/*地图定位*/
geoMap.prototype.TerLoc=function(flagCenter,flagAlarm/*string类型，报警标志,报警定位:1,其他:0*/,
			type/*定位类型,string表示,GPS/RD/RN*/, gsmsta/*string表示，GSM状态*/, bdsta/*string表示，北斗状态*/
			, logitude/*string表示，经度*/, latitude/*string表示，纬度*/, altitude/*string表示，高程*/, speed/*string表示，速度*/,
			direction/*string表示，方向*/, time/*string表示，定位时间*/, sim/*string表示，卡号*/, iconpath/*string表示，显示图标路径*/){}

geoMap.prototype.followLoc=function (lng/*定位的经度*/,lat/*定位的纬度*/,title/*标题，用作标记物标记*/,zoom/*地图级数*/,infomation/*信息窗*/){}

geoMap.prototype.locReplay=function(poindata,zoom,infostring/*可能用于信息窗或者title的信息*/ ){}


geoMap.prototype.clearall=function(){}


geoMap.prototype.setFence=function(){}

geoMap.prototype.getFence=function(){}

geoMap.prototype.modifyFence=function(){}
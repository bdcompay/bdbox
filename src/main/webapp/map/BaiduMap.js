function baidumid(tername,point){
	 //真实经纬度转成百度坐标
	 baicardid=tername;
	 setTimeout(function(){
	     BMap.Convertor.translate(point,0,baitermid);
	},50);
}

function baiduLocmid(marker,point){
	//真实经纬度转成百度坐标
	try{
	  setTimeout(function(){
	     BMap.Convertor.Btranslate(point,marker,0,baiterLocmid);
	  },50);
	}catch(e){
	  alert("网络不好，转换坐标出错了");
	}
}

function baiduCentermid(point){
   //真实经纬度转成百度坐标
   setTimeout(function(){
	     BMap.Convertor.translate(point,0,setMapCenter);
   }, 50);
}

function setMapCenter(point){
   map.setCenter(point);
}
function baitermid(point){   
	 var _sindex;
	 try{
		_sindex=baicardid.substring(baicardid.length-1,1);
	 }catch(e){}
	 if(BaiDuarr[_sindex].length){
	 }else{   
	   alert("温馨提示:卡号为【"+baicardid+"】的终端无定位信息");
	 }
	 for(var i=0;i<BaiDuarr[_sindex].length;i++){ 
		 //如果title符合
		 if(BaiDuarr[_sindex][i].getTitle()==baicardid){ 
			BaiDuarr[_sindex][i].BsetPosition(point);
			map.setCenter(point);
			if(map.getZoom()<15){
	    		    map.setZoom(15);	 
	    	}
			BaiDuarr[_sindex][i].box.open( BaiDuarr[_sindex][i] );	
		 }
	 }
}
function baiterLocmid(point,marker){
	marker.BsetPosition(point);
}

/*信息窗数组 百度地图使用*/
var infoBoxs = new Array();
var baiduMap=function(){}
var markerClusterer;/*=new BMapLib.MarkerClusterer(map,{markers:markers});*/
/*初始化地图函数*/
baiduMap.Init=function(from){
    MAPTYPE = 'baidu';       
    //初始化MAP
    map=new BMap.Map('map_canvas');
    //初始化控件
    map.centerAndZoom( new BMap.Point(108.206548,33.58389 ), 5 );
    var opts = { anchor: BMAP_ANCHOR_TOP_LEFT, offset: new BMap.Size( 10, 50 ) };
    if(from=="track"){//如果是用作定位跟踪的地图类型，则不加载一些控件
       map.addControl( new BMap.ScaleControl());// 添加比例尺控件
    }else{
	   map.addControl( new BMap.NavigationControl(opts));//添加平移缩放控件
       map.addControl( new BMap.ScaleControl());//添加比例尺控件
       map.addControl( new BMap.OverviewMapControl());//添加缩略地图控件        
    }
    map.addControl( new BMap.MapTypeControl( { mapTypes: [BMAP_NORMAL_MAP, BMAP_HYBRID_MAP] } ) );
    map.addControl( new BMap.MapTypeControl( { anchor: BMAP_ANCHOR_TOP_LEFT } ) );
    map.enableScrollWheelZoom();// 启用滚轮放大缩小。
    map.enableKeyboard();// 启用键盘操作。
    //初始化测量工具栏
    //测量面积不作接口
    map.addEventListener( "mousemove", function(e){
        if ( dstartsign == 1 ){
            if ( waittingmeansure == 1 ){

            }else{
                LabelArray[goni].setPosition( e.point );
                var area = BMapLib.GeoUtils.getPolygonArea( polygonarray[goni] );
                if ( area > 0 ){
                    //小数点的位置
                    var areastring = String( area );
                    var dotindex = areastring.indexOf( "." );
                    if ( String( areastring ).length > dotindex + 2 ){
                        areastring = areastring.substring( 0, dotindex + 2 );
                    }
                    finalsquare = areastring;
                }
                LabelArray[goni].setContent( finalsquare + "平方米(双击完成测量)" );
                polygonarray[goni].setPositionAt( i, new BMap.Point( e.point.lng, e.point.lat ) );
            }
        }
    });
    var polygon = new BMap.Polygon(
       [new BMap.Point( 116.399, 39.910),new BMap.Point( 116.412, 39.930)],
       { strokeColor: "red", strokeWeight: 1, strokeOpacity: 0.5 });
    map.addEventListener( "rightclick", function (){
    });
    var i = 1;
    //标记是否在划线
    var dsign = 0;
    //标记是否启动划线
    var dstartsign = 0;
    //是否待创建测面积标记符
    var waittingmeansure = 0;
    //面数组的标记
    var goni = 0;
    //放置点的数组
    var points = [];
    points[0] = new BMap.Point( 116.399, 39.910 );
    points[1] = new BMap.Point( 116.412, 39.930 );
    map.addEventListener( "click", function ( e ){
        //如果是启动  
        if ( dstartsign == 1 ){
            if ( i < 1 ){
                //如果是第一个点
                points[i] = new BMap.Point( e.point.lng, e.point.lat );
                points[i + 1] = new BMap.Point( e.point.lng, e.point.lat );
                polygonarray[goni].setPath( points );
                map.addOverlay( polygonarray[goni] );
                waittingmeansure = 0;
                i++;
            }else{
                points[i] = new BMap.Point( e.point.lng, e.point.lat );
                points.push( e.point );
                polygonarray[goni].setPath( points );
                LabelArray[goni].setContent( finalsquare + "平方米(双击完成测量)" );
                i++;
            }
        }
    });
    map.addEventListener( "dblclick",function(e){
        //如果是启动  
        if ( dstartsign == 1 ){
            var centerpoint = map.getCenter();
            activepolygon[goni] = 1;
            points[i] = e.point;
            polygonarray[goni].setPath( points );
            LabelArray[goni].setPosition( e.point );
            LabelArray[goni].setContent( finalsquare + "平方米(右击关闭)" );
            dstartsign = 0;
            map.setZoom( map.getZoom() - 1 );
            map.setCenter( centerpoint );
            LabelArray[goni].addEventListener( "rightclick", function ( e ){
                var labelindex = parseInt( this.getTitle() );
                map.removeOverlay( LabelArray[labelindex] );
                map.removeOverlay( polygonarray[labelindex] );
                activepolygon[labelindex] = 0;
                points.length = 0;
                polygonarray[labelindex].setPath( points );
            });
        }
    });
    //最终面积
    var finalsquare = 0;
    //使用div标签来
    // 定义一个控件类,即function
    function ZoomControl(){
        // 默认停靠位置和偏移量
        this.defaultAnchor = BMAP_ANCHOR_TOP_LEFT;
        this.defaultOffset = new BMap.Size( 150, 10 );
    }
    //通过JavaScript的prototype属性继承于BMap.Control
    ZoomControl.prototype = new BMap.Control();
    //自定义控件必须实现自己的initialize方法,并且将控件的DOM元素返回
    //在本方法中创建个div元素作为控件的容器,并将其添加到地图容器中
    ZoomControl.prototype.initialize=function(map){    
            // 创建一个DOM元素
            var div = document.createElement( "div" );
            // 添加文字说明
            div.appendChild( document.createTextNode( "测量面积" ));
            // 设置样式
            div.style.cursor = "pointer";
            div.style.border = "1px solid gray";
            div.style.backgroundColor = "#cbdbf7";
            div.style.fontSize = "12px";
            div.style.width = "60px";
            div.style.height = "22px";
            div.style.lineHeight = "22px";
            div.style.textAlign = "center";
            div.style.panddingLeft = "15px";
            //绑定事件,点击一次启用一个
            div.onclick =function (e){
	            //循环查看是否有侧面工具可以使用，有就激活
	            for(var usei=0;usei<10;usei++){
	                 if ( activepolygon[usei] == 0){
	                        goni = usei;
	                        waittingmeansure = 1;
	                        map.addOverlay( LabelArray[goni] );
	                        break;
	                 }
	            }
	            if ( goni >= 10 ){
	                alert( "测量面积数量超出限制，请删除一些面积测量后再" );
	            }else{
	                //激活
	                dstartsign = 1;
	                i = 0;
	                map.addOverlay( polygonarray[goni] );
	            }
	            points.length = 0;
	            points[0] = new BMap.Point( 116.3964, 39.9093 );
	            points[1] = new BMap.Point( 116.3964, 39.9093 );
            }
            //添加DOM元素到地图中
            map.getContainer().appendChild( div );
            //将DOM元素返回
            return div;
        }
        //创建控件
        var myZoomCtrl = new ZoomControl();
        //添加到地图当中
        map.addControl( myZoomCtrl );
        //Label数组
        var LabelArray = [];
        //第一个Label
        var carlabel = new BMap.Label( "测量", { offset: new BMap.Size( -15, 10 ), position: map.getCenter() } );
        LabelArray.push( carlabel );
        carlabel.setTitle( "0" );
        //第二个Label
        var carlabel_2 = new BMap.Label( "测量", { offset: new BMap.Size( -15, 10 ), position: map.getCenter() } );
        LabelArray.push( carlabel_2 );
        carlabel_2.setTitle( "1" );
        //第三个Label
        var carlabel_3 = new BMap.Label( "测量", { offset: new BMap.Size( -15, 10 ), position: map.getCenter() } );
        LabelArray.push( carlabel_3 );
        carlabel_3.setTitle( "2" );
        //第四个Label
        var carlabel_4 = new BMap.Label( "测量", { offset: new BMap.Size( -15, 10 ), position: map.getCenter() } );
        LabelArray.push( carlabel_4 );
        carlabel_4.setTitle( "3" );
        //第五个Label_5
        var carlabel_5 = new BMap.Label( "测量", { offset: new BMap.Size( -15, 10 ), position: map.getCenter() } );
        LabelArray.push( carlabel_5 );
        carlabel_5.setTitle( "4" );
        //第六个Label_6
        var carlabel_6 = new BMap.Label( "测量", { offset: new BMap.Size( -15, 10 ), position: map.getCenter() } );
        LabelArray.push( carlabel_6 );
        carlabel_6.setTitle( "5" );
        //第七个Label_7
        var carlabel_7 = new BMap.Label( "测量", { offset: new BMap.Size( -15, 10 ), position: map.getCenter() } );
        LabelArray.push( carlabel_7 );
        carlabel_7.setTitle( "6" );
        //第八个Label
        var carlabel_8 = new BMap.Label( "测量", { offset: new BMap.Size( -15, 10 ), position: map.getCenter() } );
        LabelArray.push( carlabel_8 );
        carlabel_8.setTitle( "7" );
        //第九个Label
        var carlabel_9 = new BMap.Label( "测量", { offset: new BMap.Size( -15, 10 ), position: map.getCenter() } );
        LabelArray.push( carlabel_9 );
        carlabel_9.setTitle( "8" );
        //第十个Label
        var carlabel_10 = new BMap.Label( "测量", { offset: new BMap.Size( -15, 10 ), position: map.getCenter() } );
        LabelArray.push( carlabel_10 );
        carlabel_10.setTitle( "9" );
        //面数组
        var polygonarray = [];
        //第一个测面
        var polygon = new BMap.Polygon(
        	[new BMap.Point( 116.399, 39.910 ),new BMap.Point( 116.412, 39.930)],
        	{strokeColor: "red", strokeWeight: 2, strokeOpacity: 0.5 });
        polygonarray.push( polygon );
        //第二个测面
        var polygon_2 = new BMap.Polygon(
        	[new BMap.Point( 116.399, 39.910 ),new BMap.Point( 116.412, 39.930 )],
            {strokeColor: "red", strokeWeight: 2, strokeOpacity: 0.5 } );
        polygonarray.push(polygon_2);
        //第三个测面
        var polygon_3 = new BMap.Polygon(
        	[new BMap.Point( 116.399, 39.910 ),new BMap.Point( 116.412, 39.930 )],
        	{strokeColor: "red", strokeWeight: 2,strokeOpacity:0.5});
        polygonarray.push( polygon_3 );
        //第四个测面
        var polygon_4 = new BMap.Polygon(
        	[new BMap.Point( 116.399, 39.910 ),new BMap.Point( 116.412, 39.930 )],
        	{strokeColor: "red", strokeWeight: 2, strokeOpacity: 0.5 });
        polygonarray.push( polygon_4 );
        //第五个测面
        var polygon_5 = new BMap.Polygon(
    		[new BMap.Point( 116.399, 39.910),new BMap.Point( 116.412, 39.930)],
    		{ strokeColor: "red", strokeWeight: 2, strokeOpacity: 0.5 });
        polygonarray.push( polygon_5 );
        //第六个测面
        var polygon_6 = new BMap.Polygon(
           [new BMap.Point( 116.399, 39.910),new BMap.Point( 116.412, 39.930)],
           {strokeColor: "red", strokeWeight: 2, strokeOpacity: 0.5 } );
        polygonarray.push( polygon_6 );
        //第七个测面
        var polygon_7 = new BMap.Polygon(
            [new BMap.Point( 116.399, 39.910),new BMap.Point( 116.412, 39.930 )],
            {strokeColor: "red", strokeWeight: 2, strokeOpacity: 0.5 });
        polygonarray.push( polygon_7 );
        //第八个测面
        var polygon_8 = new BMap.Polygon(
        	[new BMap.Point( 116.399, 39.910),new BMap.Point( 116.412, 39.930 )],
        	{strokeColor: "red", strokeWeight: 2, strokeOpacity: 0.5 });
        polygonarray.push( polygon_8 );
        //第九个测面
        var polygon_9 = new BMap.Polygon(
        	[new BMap.Point( 116.399, 39.910),new BMap.Point( 116.412,39.930)], 
            {strokeColor: "red", strokeWeight: 2, strokeOpacity: 0.5 });
        polygonarray.push( polygon_9 );
        //第十个测面
        var polygon_10 = new BMap.Polygon(
        	[new BMap.Point(116.399, 39.910),new BMap.Point(116.412,39.930)],
        	{strokeColor: "red", strokeWeight: 2, strokeOpacity: 0.5 });
        polygonarray.push( polygon_10 );
        //数组用于标记活跃的面
        var activepolygon = [];
        for ( var ai = 0; ai < 10; ai++){
            activepolygon.push( 0 );
        }
        //测量面积
        //测量距离
        var myDis = new BMapLib.DistanceTool( map );
        //定义一个控件类,即function
        function ZoomControl_1(){
            // 默认停靠位置和偏移量
            this.defaultAnchor = BMAP_ANCHOR_TOP_LEFT;
            this.defaultOffset = new BMap.Size( 220, 10 );
        }
        //通过JavaScript的prototype属性继承于BMap.Control
        ZoomControl_1.prototype = new BMap.Control();
        //自定义控件必须实现自己的initialize方法,并且将控件的DOM元素返回
        //在本方法中创建个div元素作为控件的容器,并将其添加到地图容器中
        ZoomControl_1.prototype.initialize = function ( map ){
            //创建一个DOM元素
            var div = document.createElement( "div" );
            //添加文字说明
            div.appendChild( document.createTextNode( "测量距离" ) );
            //设置样式
            div.style.cursor = "pointer";
            div.style.border = "1px solid gray";
            div.style.backgroundColor = "#BCD2EE";
            div.style.fontSize = "12px";
            div.style.width = "60px";
            div.style.height = "22px";
            div.style.lineHeight = "22px";
            div.style.textAlign = "center";
            div.style.panddingLeft = "15px";
            // 绑定事件,点击一次放大两级
            div.onclick = function ( e ){
                myDis.open();
            }
            //添加DOM元素到地图中
            map.getContainer().appendChild( div );
            //将DOM元素返回
            return div;
        }
        // 创建控件
        var myZoomCtrl_length = new ZoomControl_1();
        // 添加到地图当中
        map.addControl( myZoomCtrl_length );
		function ZoomControl_2(){
            //默认停靠位置和偏移量
            this.defaultAnchor = BMAP_ANCHOR_TOP_LEFT;
            this.defaultOffset = new BMap.Size( 290, 10 );
        }
        // 通过JavaScript的prototype属性继承于BMap.Control
        ZoomControl_2.prototype = new BMap.Control();
		//通过JavaScript的prototype属性继承于BMap.Control
        //ZoomControl_1.prototype = new BMap.Control();
        // 自定义控件必须实现自己的initialize方法,并且将控件的DOM元素返回
        // 在本方法中创建个div元素作为控件的容器,并将其添加到地图容器中
        ZoomControl_2.prototype.initialize = function ( map ){
            // 创建一个DOM元素
            var div = document.createElement( "div" );
            // 添加文字说明
            div.appendChild( document.createTextNode( "坐标定位" ) );
            // 设置样式
            div.style.cursor = "pointer";
            div.style.border = "1px solid gray";
            div.style.backgroundColor = "#BCD2EE";
            div.style.fontSize = "12px";
            div.style.width = "60px";
            div.style.height = "22px";
            div.style.lineHeight = "22px";
            div.style.textAlign = "center";
            div.style.panddingLeft = "15px";
            // 绑定事件,点击一次放大两级
            div.onclick = function ( e ){
				LongLat();
            }
            // 添加DOM元素到地图中
            map.getContainer().appendChild( div );
            // 将DOM元素返回
            return div;
        }
        // 创建控件
        var myZoomCtrl_length_2 = new ZoomControl_2();
        // 添加到地图当中
        map.addControl( myZoomCtrl_length_2 );
		//小提示
		function ZoomControl_3(){
            // 默认停靠位置和偏移量
            this.defaultAnchor = BMAP_ANCHOR_TOP_LEFT;
            this.defaultOffset = new BMap.Size( 360, 10 );
        }
        if(from=="mainmonitoring"){
		   try{
 	          webbegin();
 	       }catch(e){ 
 	          //alert("调用命令失败");
 	       }
		}
        //测量距离 
        markerClusterer = new BMapLib.MarkerClusterer(map, {markers:null});
        return true;
}
/*地图居中*/
baiduMap.TakeCenter=function(longtitude,latitude){ 
	var centerpoint=new BMap.Point(longtitude,latitude);
	baiduCentermid(centerpoint);
}

/*地图定位*/
baiduMap.TerLoc = function(flagCenter, flagAlarm/*string类型，报警标志,报警定位:1,其他:0*/,
		type/*定位类型,string表示,GPS/RD/RN*/, gsmsta/*string表示，GSM状态*/, bdsta/*string表示，北斗状态*/
		, logitude/*string表示，经度*/, latitude/*string表示，纬度*/,
		altitude/*string表示，高程*/, speed/*string表示，速度*/,
		direction/*string表示，方向*/, time/*string表示，定位时间*/, sim/*string表示，卡号*/,
		iconpath/*string表示，显示图标路径*/) {
	if (latitude == "" || logitude == "") {
		if (flagCenter == "1") {
			alert("温馨提示:卡号为" + sim + "的终端定位数据异常！");
		}
		return;
	}
	//遍历当前marker
    var mkrs = map.getOverlays();
	var mfound_flag=false;//标记是否找得到相同的Marker
	//经纬度转换百度坐标
	var poi=new BMap.Point(logitude,latitude);
	if(flagCenter=="1"){
		baidumid(sim,poi);
		return;
    }
	//居中情况开始
	if(flagCenter=="1"){
	   map.setCenter(poi);//居中显示最新的标注物
	   if(map.getZoom()<15){
		    map.setZoom(15);	 
	   }
	   //遍历点
	   var _sindex;
	   try{
		  _sindex=sim.substring(sim.length-1,1);
	   }catch(e){}
	   if(BaiDuarr[_sindex]){
	      for(var i=0;i<BaiDuarr[_sindex].length;i++){
		 	 var marker_flag=false;
			 var marker_title="btestno";
             //获取卡号(title)			 
			 marker_title=BaiDuarr[_sindex][i].getTitle();	 
			 //如果title符合
			 if(marker_title==sim){   
				 if(BaiDuarr[_sindex][i].state==1){					 
					  return;
				 }else{
					 //TODO
				     var html=["<div class='infoBoxContent' style='padding-bottom:10px'><div class='infoBoxtitle'><strong>终端信息</strong><span class='price'>("+sim+")</span></div>",
					      "<div class='infoBoxlist'><ul><li><div class='infoBoxleft'><span>SIM卡号：</span><span class='curr'>"+sim+"</span></div><div class='infoBoxleftdata'><span>定位类型：</span><span class='curr' title='"+type+"'>"+type+"</span></div></li>"
					 	 ,"<li><div class='infoBoxleft'><span>公网状态：</span><span class='curr'>"+gsmsta+"</span></div><div class='infoBoxleftdata'><span>北斗状态：</span><span class='curr'>"+bdsta+"</span></div></li>"
					 	 ,"<li><div class='infoBoxleft'><span>经度：</span><span>"+logitude+"</span></div><div class='infoBoxleftdata'><span>纬度：</span><span>"+latitude+"</span></div></li>"
					 	 ,"<li><div class='infoBoxleft'><span>高程：</span><span>"+altitude+"m</span></div><div class='infoBoxleftdata'><span>方向：</span><span>"+direction+"</span></div></li>"
					 	 ,"<li><div class='infoBoth'><span>速度：</span><span>"+speed+"km/h</span></div></li>"
					 	 ,"<li><div class='infoBoth'><span>时间：</span><span>"+time+"</span></div></li>"
					 	 ,"</ul></div>"
					 	 ,"</div>"];
					 BaiDuarr[_sindex][i].box= new BMapLib.InfoBox( map, html.join(""),{
							boxStyle : {
							background : "url('map/tipbox.gif') no-repeat center top",
							width : "293px",
							height : "170px"
						},
						closeIconMargin : "7px 7px 0 0",
					    enableAutoPan: true,
					    align:INFOBOX_AT_TOP,
						offset:new BMap.Size(100,0)
				     });
					 BaiDuarr[_sindex][i].box.open( BaiDuarr[_sindex][i] );		
				     BaiDuarr[_sindex][i].state=1;
					 /*infoBox要在Open之后才接受事件定义*/
				     BaiDuarr[_sindex][i].box.addEventListener( "close", function ( e ){//本事件在窗口信息被关闭时触发，以便控制INFOBOXS数组
					    BaiDuarr[_sindex][i].state=0;	 
				     });
					 return;
			      }
		    }
		}
      }
   }
   //居中情况结束
   //非居中情况开始
    var sindex;
	try{	
	  sindex=sim.substring(sim.length-1,1);
	}catch(e){}
	if(BaiDuarr[sindex]){
		for(var i=0;i<BaiDuarr[sindex].length;i++){
		 	var marker_flag=false;
			var marker_title="btestno";		
			marker_title=BaiDuarr[sindex][i].getTitle();	 
			//如果title符合,找到了相同的终端
			if(marker_title==sim){
			   var iconpathstring=iconpath+getBIcon(direction,bdsta,gsmsta);
			   BaiDuarr[sindex][i].setIcon(new BMap.Icon(iconpathstring, new BMap.Size(20,20)));
			   //新建信息窗，并根据marker的state标记来判断是否打开
			   //TODO
			   var html=["<div class='infoBoxContent' style='padding-bottom:10px'><div class='infoBoxtitle'><strong>终端信息</strong><span class='price'>("+sim+")</span></div>",
			      "<div class='infoBoxlist'><ul><li><div class='infoBoxleft'><span>SIM卡号：</span><span class='curr'>"+sim+"</span></div><div class='infoBoxleftdata'><span>定位类型：</span><span class='curr' title='"+type+"'>"+type+"</span></div></li>"
			 	 ,"<li><div class='infoBoxleft'><span>公网状态：</span><span class='curr'>"+gsmsta+"</span></div><div class='infoBoxleftdata'><span>北斗状态：</span><span class='curr'>"+bdsta+"</span></div></li>"
			 	 ,"<li><div class='infoBoxleft'><span>经度：</span><span>"+logitude+"</span></div><div class='infoBoxleftdata'><span>纬度：</span><span>"+latitude+"</span></div></li>"
			 	 ,"<li><div class='infoBoxleft'><span>高程：</span><span>"+altitude+"m</span></div><div class='infoBoxleftdata'><span>方向：</span><span>"+direction+"</span></div></li>"
			 	 ,"<li><div class='infoBoth'><span>速度：</span><span>"+speed+"km/h</span></div></li>"
			 	 ,"<li><div class='infoBoth'><span>时间：</span><span>"+time+"</span></div></li>"
			 	 ,"</ul></div>"
			 	 ,"</div>"];
		     if(BaiDuarr[sindex][i].state==1){
		    	 BaiDuarr[sindex][i].BSetInfo(html.join("")); 
		     }else{ 
	             BaiDuarr[sindex][i].BSetInfo(html.join("")); 
		     }
			 baiduLocmid(BaiDuarr[sindex][i],poi);
		     return;
		 }
	    }
	}else{
	}
	//如果没有找到相同的
	if(!mfound_flag){
	  if ( flagAlarm == "1" ){
        var marker = new BMap.Marker( poi );		
        marker.setAnimation( BMAP_ANIMATION_BOUNCE ); //跳动的动
        infoBox.open( marker );
      }else{   
	    //根据北斗状态、公网状态和运动方向来获取图标
		var iconpathstring=iconpath+getBIcon(direction,bdsta,gsmsta);
        var myIcon = new BMap.Icon( iconpathstring, new BMap.Size( 20,20 ) ); //创建图标
        var marker = new BMap.Marker( poi, { icon: myIcon} );
        var label = new BMap.Label(sim,{offset:new BMap.Size(20,0)});
        marker.setLabel(label);
	    marker.setTitle(sim);		
		marker.state=0;
        map.addOverlay(marker);
        baiduLocmid(marker,poi);
        /*开启可拖动*/
        marker.enableDragging();
        //TODO
		var html=["<div class='infoBoxContent' style='padding-bottom:10px'><div class='infoBoxtitle'><strong>终端信息</strong><span class='price'>("+sim+")</span></div>",
	      "<div class='infoBoxlist'><ul><li><div class='infoBoxleft'><span>SIM卡号：</span><span class='curr'>"+sim+"</span></div><div class='infoBoxleftdata'><span>定位类型：</span><span class='curr' title='"+type+"'>"+type+"</span></div></li>"
	 	 ,"<li><div class='infoBoxleft'><span>公网状态：</span><span class='curr'>"+gsmsta+"</span></div><div class='infoBoxleftdata'><span>北斗状态：</span><span class='curr'>"+bdsta+"</span></div></li>"
	 	 ,"<li><div class='infoBoxleft'><span>经度：</span><span>"+logitude+"</span></div><div class='infoBoxleftdata'><span>纬度：</span><span>"+latitude+"</span></div></li>"
	 	 ,"<li><div class='infoBoxleft'><span>高程：</span><span>"+altitude+"m</span></div><div class='infoBoxleftdata'><span>方向：</span><span>"+direction+"</span></div></li>"
	 	 ,"<li><div class='infoBoth'><span>速度：</span><span>"+speed+"km/h</span></div></li>"
	 	 ,"<li><div class='infoBoth'><span>时间：</span><span>"+time+"</span></div></li>"
	 	 ,"</ul></div>"
	 	 ,"</div>"];
        var infoBox =null;
	    /*一定要用以下这两行代码来先绑定marker和infoBox*/
		if(BaiDuarr[sindex]){
			BaiDuarr[sindex].push(marker);
			//聚类效果
			 markerClusterer.addMarkers(BaiDuarr[sindex]);
		}else{ 
		  BaiDuarr[sindex]=new Array();
		  BaiDuarr[sindex][0]= marker;
		  //聚类效果
		  markerClusterer.addMarkers(BaiDuarr[sindex]);
	    }
		var infocontent=html.join("");
		infoBox= new BMapLib.InfoBox(map,infocontent,{
				boxStyle : {
					background : "url('map/tipbox.gif') no-repeat center top",
					width : "293px",
					height : "170px"
				},
				closeIconMargin : "7px 7px 0 0",
			    enableAutoPan: true,
			    align:INFOBOX_AT_TOP,
				offset:new BMap.Size(100,0)
		      });
		marker.box=infoBox;
	    marker.addEventListener("click",function (){//本事件在标注被点击时出发，弹出Infobox
	        marker.box.open( marker );	
		    //state标记窗口是否被打开
			marker.state=1;
			/*infoBox要在Open之后才接受事件定义*/
		    infoBox.addEventListener("close",function(e){//本事件在窗口信息被关闭时触发，以便控制INFOBOXS数组
		       marker.state=0;	 
	        });
			
	    });
		//Marker扩展方法
		marker.BsetPosition=function(bpoint){
		  marker.setPosition(bpoint);
		  if(marker.state==1){
		   marker.box.open(marker);
		  }
		}
		marker.BSetInfo=function(info){   
		  if(marker.state==1){
		   marker.box.setContent(info);		   
		  }else{
			 infocontent=info;  
			 marker.box=new BMapLib.InfoBox( map, infocontent, {
				boxStyle : {
					background : "url('map/tipbox.gif') no-repeat center top",
					width : "293px",
					height : "170px"
				},
				closeIconMargin : "7px 7px 0 0",
			    enableAutoPan: true,
			    align:INFOBOX_AT_TOP,
				offset:new BMap.Size(100,0)
		      });
		  }
	    }
      }
	  return;
	}
}

/*跟踪定位*/
baiduMap.followLoc = function(lng/*定位的经度*/, lat/*定位的纬度*/,
		title/*标题，用作标记物标记*/, zoom/*地图级数*/, infomation/*信息窗*/) {
	/*如果地图跑出视野范围，点居中*/
	var newlnglat = new BMap.Point(lng, lat);
	if (!BInBounds(lng, lat)) {
		map.setCenter(newlnglat);
	}
	var BFinfo = new Array();
	/*提取信息窗信息*/
	var arrinfo = infomation.split(",");
	var html=["<div class='infoBoxContent' style='padding-bottom:10px'><div class='infoBoxtitle'><strong>终端信息</strong><span class='price'>("+title+")</span></div>",
     "<div class='infoBoxlist'><ul><li><div class='infoBoxleft'><span>SIM卡号：</span><span class='curr'>"+title+"</span></div><div class='infoBoxleftdata'><span>定位类型：</span><span class='curr' type='"+arrinfo[0]+"'>"+arrinfo[0]+"</span></div></li>"
	 ,"<li><div class='infoBoxleft'><span>公网状态：</span><span class='curr'>"+arrinfo[1]+"</span></div><div class='infoBoxleftdata'><span>北斗状态：</span><span class='curr'>"+arrinfo[2]+"</span></div></li>"
	 ,"<li><div class='infoBoxleft'><span>经度：</span><span>"+lng+"</span></div><div class='infoBoxleftdata'><span>纬度：</span><span>"+lat+"</span></div></li>"
	 ,"<li><div class='infoBoxleft'><span>高程：</span><span>"+arrinfo[4]+"m</span></div><div class='infoBoxleftdata'><span>方向：</span><span>"+arrinfo[5]+"</span></div></li>"
	 ,"<li><div class='infoBoth'><span>速度：</span><span>"+arrinfo[3]+"km/h</span></div></li>"
	 ,"<li><div class='infoBoth'><span>时间：</span><span>"+arrinfo[6]+"</span></div></li>"
	 ,"</ul></div>"
	 ,"</div>"];
	//如果是第一次跟踪定位
	if(BlinePois.length<1){
		//初始化Infobox
		BFInfo= new BMapLib.InfoBox( map,html.join( "" ), {
		   boxStyle : {
			 background : "url('map/tipbox.gif') no-repeat center top",
			 width : "293px",
			 height : "170px"
		   },
		   closeIconMargin : "7px 7px 0 0",
	       enableAutoPan: true,
	       align:INFOBOX_AT_TOP
     });
	 try{
		if(zoom!=map.getZoom()){
			 map.setZoom(zoom);  
		}
	 }catch(e){
	   if(map.getZoom()<15){
		map.setZoom(15);  
	   }	  
	 }
   }else{
	 BFInfo._setContent(html.join(""));
   }
   var mkrs = map.getOverlays();
   var flag_found = false;
   for ( var i = 0; i < mkrs.length; i++ ){
       //alert("现在的infoxbox总数:"+infoBoxs.length);
       //这里遍历infoBoxs数组，如果发现当前覆盖物中有Infobox则跳过
		function bTitle(_title,ci){
		  try{
		     var btitle=mkrs[ci].getTitle();
		     if(btitle==_title){
			  return true;
		     }else{return false;}
		  }catch(e){return false;}	
	    }
        if(bTitle(title,i)){
		   //找到了Markers,重置位置
		   flag_found=true;
		   if(arrinfo[7]!='empty'){   
		      
		       var iconstring=getBIcon(arrinfo[5],arrinfo[1],arrinfo[2]);
			   mkrs[i].setIcon(new BMap.Icon(arrinfo[7]+iconstring,new BMap.Size(20,20)));
		   }else{
			  mkrs[i].setIcon(arrinfo[7]+'lanse2.gif',new BMap.Size(20,20));	
		   }
		   mkrs[i].setPosition(newlnglat);
		   if(bfopenflag){
		      BFInfo.open(mkrs[i]);
	       }if(BlinePois.length>1){
			   /*线的点数不为零*/
			   BlinePois[length-1]=newlnglat;
			   BlinePois.push(newlnglat);
			   Bline.setPath(BlinePois);
		   }else{
			  /*如果线的点数为零，或者等于一*/
			  BlinePois.push(newlnglat);
			  /*重复一次，为线的初始化*/
			  BlinePois.push(newlnglat);
			  Bline=new BMap.Polyline(BlinePois, {strokeColor:"red", strokeWeight:6, strokeOpacity:0.5});
			  map.addOverlay(Bline);
		   }
		}     
   }
   if(!flag_found){
	  //如果找不到
	  BlinePois.length=0;
	  var iconstring=getBIcon(arrinfo[5],arrinfo[1],arrinfo[2]);
	  var ficon=new BMap.Icon(iconstring,new BMap.Size(20,20));
	  var bmarker=new BMap.Marker(newlnglat,{icon:ficon,title:title});
	  var label = new BMap.Label(title,{offset:new BMap.Size(20,0)});
      bmarker.setLabel(label);
	  if(arrinfo[7]!='empty'){   
			   bmarker.setIcon(new BMap.Icon(arrinfo[7]+iconstring,new BMap.Size(20,20)));
	  }else{}
	  map.addOverlay(bmarker);
	  BlinePois.push(newlnglat);
	  /*重复一次，为线的初始化*/
	  BlinePois.push(newlnglat);
	  Bline=new BMap.Polyline(BlinePois, {strokeColor:"red", strokeWeight:6, strokeOpacity:0.5});
      map.addOverlay(Bline);
	  if(bfopenflag){
		   BFInfo.open( bmarker );
	  }
	  bmarker.addEventListener("click", function (){//本事件在标注被点击时出发，弹出Infobox
       BFInfo.open( bmarker );
		bfopenflag=true;
      });
      BFInfo.addEventListener("close",function(e){//本事件在窗口信息被关闭时触发，以便控制INFOBOXS数组
	    bfopenflag=false;
      });
      BFInfo.addEventListener( "open", function (e){//本事件在窗口信息打开时触发，以便控制INFOBOXS数组       
       bfopenflag=true;
      });	  
   }	
	
}

var beidoureplaylushu;
function repalyback(poindata,zoom,infostring){   
      var bdInfoArray=new Array();
	  //提取点
	  var baiduarrallpoint = poindata.split( ";" );
      var baiduarrPois = new Array();
      for ( var i = 0; i < baiduarrallpoint.length-1; i++){   
	      //alert(baiduarrallpoint[i]);
          var arrpos = baiduarrallpoint[i].split( "," );
          var logitude = parseFloat( arrpos[0] );
          var latitude = parseFloat( arrpos[1] );
		   //alert(logitude);
		  var  infoarray=new Array();
		  infoarray[0]=arrpos[2];//定位类型
          infoarray[1]=arrpos[3];//GSM状态
		  infoarray[2]=arrpos[4];//北斗状态
		  infoarray[3]=arrpos[5];//速度			
		  infoarray[4]=arrpos[6];//高度
		  infoarray[5]=arrpos[7];//方向
		  infoarray[6]=arrpos[8];//时间
		  infoarray[7]=arrpos[9];//图标
		  infoarray[8]=arrpos[10];//GPS经度
		  infoarray[9]=arrpos[11];//GPS纬度
		  if(logitude==""||latitude==""||logitude==0){
			  continue;	
			  //alert(logitude);
		  }else{
            baiduarrPois.push(new BMap.Point(logitude,latitude));
			bdInfoArray.push(infoarray);
		  }
      }
      var logotitle=infostring;
	  //划线
	  var flightPath = new BMap.Polyline(baiduarrPois, {strokeColor:"#111", strokeWeight:3, strokeOpacity:0.5});
      map.addOverlay(flightPath)
	  map.setCenter(baiduarrPois[0]);
      //TODO
	  var ghtml="";
	  var g=["<div class='infoBoxContent' style='padding-bottom:3px;width:250px;height:105px;'>"
	     ,"<div class='infoBox'><ul>"
	 	 ,"<li><div class='infoBoxleft'><span>经度：</span><span>000</span></div><div class='infoBoxleftdata'><span>纬度：</span><span>000</span></div></li>"
	 	 ,"<li><div class='infoBoxleft'><span>高程：</span><span>000m</span></div><div class='infoBoxleftdata'><span>方向：</span><span>000</span></div></li>"
	 	 ,"<li><div class='infoBoth'><span>速度：</span><span>000km/h</span></div></li>"
	 	 ,"<li><div class='infoBoth'><span>时间：</span><span>000</span></div></li>"
	 	 ,"</ul></div>"
	 	 ,"</div>"];
	  var replayinfoArray=new Array();
	  for(var rgi =0;rgi<baiduarrPois.length;rgi++){
	   var newpointinfo=new Object();;
	   newpointinfo.lng=baiduarrPois[rgi].lng;
	   newpointinfo.lat=baiduarrPois[rgi].lat;
	   //TODO
	   newpointinfo.html=["<div class='infoBoxContent' style='padding-bottom:3px;width: 250px;height:105px;'>"
	     ,"<div class='infoBox'><ul>"
	     ,"<li><div class='infoBoth'><span>卡号：</span><span class='curr'>"+logotitle+"</span></div></li>"
	 	 ,"<li><div class='infoBoxleft'><span>经度：</span><span>"+bdInfoArray[rgi][8]+"</span></div><div class='infoBoxleftdata'><span>纬度：</span><span>"+bdInfoArray[rgi][9]+"</span></div></li>"
	 	 ,"<li><div class='infoBoxleft'><span>高程：</span><span>"+bdInfoArray[rgi][4]+"m</span></div><div class='infoBoxleftdata'><span>方向：</span><span>"+bdInfoArray[rgi][5]+"</span></div></li>"
	 	 ,"<li><div class='infoBoth'><span>速度：</span><span>"+bdInfoArray[rgi][3]+"km/h</span></div></li>"
	 	 ,"<li><div class='infoBoth'><span>时间：</span><span>"+bdInfoArray[rgi][6]+"</span></div></li>"
	 	 ,"</ul></div>"
	 	 ,"</div>"].join("");
       newpointinfo.pauseTime=0;
	   replayinfoArray.push(newpointinfo);
	  }
	  //划线
	  map.setViewport(baiduarrPois);
	  lushu=new BMapLib.LuShu(map,baiduarrPois,{
            defaultContent:ghtml,
            speed:_locspeed,
            landmarkPois:replayinfoArray});
      //逐个点
      var starticon=new BMap.Icon(bdInfoArray[0][7]+"start.png",new BMap.Size(30,30));
	  var bpstart=new BMap.Marker(baiduarrPois[0],{icon:starticon});
	  map.addOverlay(bpstart);
	  /*起点Label*/
	  var startlabel = new BMap.Label("起点",{offset:new BMap.Size(20,-10)});
      bpstart.setLabel(startlabel);
	  var endicon=new BMap.Icon(bdInfoArray[0][7]+"end.png",new BMap.Size(30,30));
	  var bpend=new BMap.Marker(baiduarrPois[baiduarrPois.length-1],{icon:endicon});
	  map.addOverlay(bpend);
	  /*终点Label*/
	  var endlabel = new BMap.Label("终点",{offset:new BMap.Size(-20,-10)});
      bpend.setLabel(endlabel);
      //循环画点
	  var runsign=0;
      function BaiduRunCar(){ 
		 if(runsign>=baiduarrPois.length){
		   return;
		 }
		 //TODO
		 gehtml=["<div class='infoBoxContent' style='padding-bottom:10px'><div class='infoBoxtitle'><strong>终端信息</strong><span class='price'>("+infostring+")</span></div>",
         "<div class='infoBoxlist'><ul><li><div class='infoBoxleft'><span>SIM卡号：</span><span class='curr'>"+infostring+"</span></div><div class='infoBoxleftdata'><span>定位类型：</span><span class='curr' title='"+bdInfoArray[runsign][0]+"'>"+bdInfoArray[runsign][0]+"</span></div></li>"
		 ,"<li><div class='infoBoxleft'><span>公网状态：</span><span class='curr'>"+bdInfoArray[runsign][1]+"</span></div><div class='infoBoxleftdata'><span>北斗状态：</span><span class='curr'>"+bdInfoArray[runsign][2]+"</span></div></li>"
		 ,"<li><div class='infoBoxleft'><span>经度：</span><span>"+ bdInfoArray[runsign][8]+"</span></div><div class='infoBoxleftdata'><span>纬度：</span><span>"+bdInfoArray[runsign][9]+"</span></div></li>"
		 ,"<li><div class='infoBoxleft'><span>高程：</span><span>"+bdInfoArray[runsign][4]+"m</span></div><div class='infoBoxleftdata'><span>方向：</span><span>"+bdInfoArray[runsign][5]+"</span></div></li>"
		 ,"<li><div class='infoBoth'><span>速度：</span><span>"+bdInfoArray[runsign][3]+"km/h</span></div></li>"
		 ,"<li><div class='infoBoth'><span>时间：</span><span>"+bdInfoArray[runsign][6]+"</span></div></li>"
		 ,"</ul></div>"
		 ,"</div>"];
		 //此处应该新增信息点
		 var myIcon_1 = new BMap.Icon(bdInfoArray[0][7]+"sred.png", new BMap.Size(15,15));
		 var baidurunmarker_1 = new BMap.Marker(baiduarrPois[runsign],{icon:myIcon_1});  // 创建标注
		 map.addOverlay(baidurunmarker_1); 
		 var infoBox_1 = new BMapLib.InfoBox( map,gehtml.join( "" ), {
			boxStyle : {
				background : "url('map/tipbox.gif') no-repeat center top",
				width : "293px",
				height : "170px"
			},
			closeIconMargin : "7px 7px 0 0",
			enableAutoPan: true,
			align:INFOBOX_AT_TOP
		  });
		  baidurunmarker_1.addEventListener("click", function (){//本事件在标注被点击时出发，弹出Infobox
			      infoBox_1.open( baidurunmarker_1 );
		  });
		  baidurunmarker_1.addEventListener("remove", function (){//本事件在标注被移除时出发，关闭Infobox,移出时主要发生在标注的位置发生改变时
		  });
		  runsign++;
		  setTimeout(BaiduRunCar,10,null);
		}
	 BaiduRunCar();	
	 try{ 
	   if(diag){diag.close();}
	 }catch(e){
	 }
}

/*轨迹回放*/
baiduMap.locReplay=function(poindata,zoom,infostring/*可能用于信息窗或者title的信息*/){   
	//打开数据导入提醒窗口
	diag.show();
	//alert(poindata);
	baiduMap.clearall();
	//baireplayback(poindata,zoom,infostring);
	BMap.Convertor.ReplayGrouptranslate(poindata,infostring,zoom,repalyback/*baireplayback*/);
	  
}
/*清空地图*/
baiduMap.clearall=function(){
	//清除所有覆盖物
	map.clearOverlays();	
	//清空所有百度使用的数组
	BaiDuarr=new Array(new Array(),new Array());
	infoBoxs.length=0;
	BlinePois.length=0;
	//标识符回归
	fbopenflag=true;	
	markerClusterer.clearMarkers();
}
/*启动设置围栏*/
baiduMap.setFence=function(backcallfunction/*回调函数*/){
	 //设置默认鼠标
	map.setDefaultCursor("../core/map/green.cur"); 
	//添加点击事件
	 var i = 0;
    //标记是否在划线
    var dsign = 0;
    //标记是否启动划线
    var dstartsign = 1;
    //是否待创建测面积标记符
    var waittingmeansure = 0;
    //面数组的标记
    var goni = 0;
    //放置点的数组
    var points = [];
	var pmarkerarr=new Array();
    points[0] = new BMap.Point( 116.399, 39.910 );
    points[1] = new BMap.Point( 116.412, 39.930 );
    var fencepoly=new BMap.Polygon(points);
    map.addEventListener( "click", function ( e ){   
        //如果是启动  
        if ( dstartsign == 1){
            if ( i < 1 ){   
                //如果是第一个点
                points[i] = new BMap.Point( e.point.lng, e.point.lat );
                points[i + 1] = new BMap.Point( e.point.lng, e.point.lat );
                fencepoly.setPath( points );
                map.addOverlay( fencepoly );
                waittingmeansure = 0;
				var pmarker=new BMap.Marker(points[i],{icon:new BMap.Icon("../core/map/point.png",new BMap.Size(10,10))});
				pmarker.enableDragging();
				pmarker.setTitle(i);
				map.addOverlay(pmarker);
				pmarkerarr.push(pmarker);
				pmarker.addEventListener("dragging", function(e){
				   	fencepoly.setPositionAt(pmarker.getTitle(),pmarker.getPosition());
				   	try{
		                   backcallfunction(getFence());
		            }catch(e){}
				 });
                i++;
            }else{
                points[i] = new BMap.Point( e.point.lng, e.point.lat );
                points.push( e.point );
                fencepoly.setPath( points );
			    var pmarker=new BMap.Marker(points[i],{icon:new BMap.Icon("../core/map/point.png",new BMap.Size(10,10))});
				pmarker.enableDragging();
				pmarker.setTitle(i);
				map.addOverlay(pmarker);
				pmarkerarr.push(pmarker);
				pmarker.addEventListener("dragging" ,function(e){
				   	fencepoly.setPositionAt(pmarker.getTitle(),pmarker.getPosition());
				   	try{
		                   backcallfunction(getFence());
		            }catch(e){}
				 });
                i++;
            }
        }
    } );
    map.addEventListener( "rightclick", function ( e ){
        //如果是启动  
        if ( dstartsign == 1){ 
          if(i<2){
   		   alert("少于3个点不能构成区域");  
   		  }else{
            var centerpoint = map.getCenter();
            points[i] = e.point;
            fencepoly.setPath( points ); 				
			var pmarker=new BMap.Marker(points[i],{icon:new BMap.Icon("http://api.map.baidu.com/images/blank.gif",new BMap.Size(10,10))});
				pmarker.enableDragging();
				pmarker.setTitle(i);
				pmarkerarr.push(pmarker);
				map.addOverlay(pmarker);
				pmarker.addEventListener("dragging" ,function(e){
				   	 fencepoly.setPositionAt(pmarker.getTitle(),pmarker.getPosition());
				   	 try{
		                   backcallfunction(getFence());
		             }catch(e){}
				 });
            dstartsign = 0;
            map.setCenter( centerpoint );
           }
		
		 }
    });
	map.addEventListener("mousemove",function(e){
        if(dstartsign == 1 ){
              if(i>0){             
               fencepoly.setPositionAt( i, new BMap.Point( e.point.lng, e.point.lat ) );
               try{
                 backcallfunction(getFence());
               }catch(e){}
              } 
        } 
    });
}
/*获取围栏坐标String*/
baiduMap.getFence=function(){
	var overlays=map.getOverlays();
	for(var i=0;i<overlays.length;i++){
		if(overlays[i] instanceof  BMap.Polygon){
			 var path=overlays[i].getPath();
			 if(path.length<3){
		       continue;
			 }
			 var pathstring="";
			 for(var j=0;j<path.length;j++){  
			    if(pathstring!=""){
				  pathstring=pathstring+";";	
			    }
				pathstring=pathstring+path[j].lng+","+path[j].lat;
			 }
			 return pathstring;
	    }
	}
}
/*修改围栏*/
baiduMap.modifyFence=function(opts/*围栏点数组*/,backcallfunction){
	/*清空地图*/
	BClearall();
    /*分解点*/
	var pointstrings=opts.split(';');
	var bpoints=new Array();
	for(var pi=0;pi<pointstrings.length;pi++){
	   var bpointstr= pointstrings[pi].split(',');
	   var BPoint=new BMap.Point(bpointstr[0],bpointstr[1]);
	   bpoints.push(BPoint);
	}
	var bpolygon=new BMap.Polygon(bpoints);
	map.addOverlay(bpolygon);
	map.setViewport(bpoints);
	for(var pindex=0;pindex<bpoints.length;pindex++){
		var qmarker=new BMap.Marker(bpoints[pindex],{icon:new BMap.Icon("http://api.map.baidu.com/images/blank.gif",new BMap.Size(10,10))});
		qmarker.enableDragging();
		qmarker.setTitle(pindex);
		map.addOverlay(qmarker);
		qmarker.addEventListener("dragging" ,function(e){   
		bpolygon.setPositionAt(this.getTitle(),this.getPosition());
		 	try{
                backcallfunction(getFence());
            }catch(e){}
		});
	 }
}
/*推送进数组*/
baiduMap.type="baidu";
maps.push(baiduMap);
var sogouMap=function(){}
/*初始化地图函数*/
sogouMap.Init=function(from){
	 MAPTYPE = 'sogou';
		sogouMarkers.length=0;
     //导入搜狗js支持项
     var script = document.createElement( "script" );
     script.type = "text/javascript";
     script.onGo2mapApiLoad = initialize;
     script.src = "http://api.go2map.com/maps/js/api_v2.5.1.js";
     document.body.appendChild( script );

     //初始化MAP
		 //测量工具
    /* var homeControlDiv = document.createElement( 'DIV' );
     homeControlDiv.style.position = "absolute";
     homeControlDiv.style.left = "190px";
     homeControlDiv.style.top = "10px";
     var homeControl = new HomeControl( homeControlDiv, map );
     //将自定义的DIV添加到地图容器
     map.getContainer().appendChild( homeControlDiv );*/


     function initialize()
     {
         map = null;
         var clearid = document.getElementById( "map_canvas" );
         clearid.innerHTML = "";
        map = new sogou.maps.Map( document.getElementById( "map_canvas" ), {} );
     }
    
     //自定义控件
     function HomeControl( controlDiv, map )
     {

         // 为自定义控件设置CSS样式
         controlDiv.style.padding = '0px';

         var controlUI = document.createElement( 'DIV' );
         controlUI.style.backgroundColor = '#cbdbf7';
         controlUI.style.borderStyle = 'solid';
         controlUI.style.borderWidth = '1px';
         controlUI.style.cursor = 'pointer';
         controlUI.style.textAlign = 'center';
         controlUI.title = '点击开始测量距离';
         controlDiv.appendChild( controlUI );
         //为自定义控件设置文本
         var controlText = document.createElement( 'DIV' );           
         controlText.style.fontSize = '12px';
         controlText.style.paddingLeft = '2px';
         controlText.style.paddingRight = '2px';
         controlText.innerHTML = '测量距离';
         controlUI.appendChild( controlText );
         
         // 添加一个侦听器，使得点击后调用方法设置地图到         
         sogou.maps.event.addDomListener( controlUI, 'click', function ()
         {              
             var r = new Ruler( { 'map': map } );                
             r.open();
             
         } );

     }  
     if(from=="mainmonitoring")
		 {
		   	  try
	        {
		  
	        window.parent.webbegin();
	
	        }
	        catch(e)
	        { 
		  
	        alert("调用命令失败");
			
	         }
		 }
      return true;	
}
/*地图居中*/
sogouMap.TakeCenter=function(longtitude/*经度*/,latitude/*纬度*/ ){
	 var sogoucLatlng=new sogou.maps.LatLng(latitude,longtitude,true);
		map.setCenter(sogoucLatlng,map.getZoom());	
}
/*地图定位*/
sogouMap.TerLoc=function(flagCenter,flagAlarm/*string类型，报警标志,报警定位:1,其他:0*/,
			type/*定位类型,string表示,GPS/RD/RN*/, gsmsta/*string表示，GSM状态*/, bdsta/*string表示，北斗状态*/
			, logitude/*string表示，经度*/, latitude/*string表示，纬度*/, altitude/*string表示，高程*/, speed/*string表示，速度*/,
			direction/*string表示，方向*/, time/*string表示，定位时间*/, sim/*string表示，卡号*/, iconpath/*string表示，显示图标路径*/){
	
	//alert(sogouMarkers.length);
	//标记是否找到相同的Marker
	var flag_sougousame=false;
	//提取信息中的经纬度，组成搜狗点
    var sougounewLatlng=new sogou.maps.LatLng(latitude,logitude,true);
	 //判断是否属于居中
    if(flagCenter=="1")
	{
	   map.setCenter(sougounewLatlng);//居中显示最新的标注物
	   if(map.getZoom()<15)
		   {
		    map.setZoom(15);
		   }
   	}
	
	//遍历搜狗Marker数组，然后找到相同title的，找到，则变化位置；找不到，就新建
	for(var sogoui=0;sogoui<sogouMarkers.length;sogoui++)
	{ 
	   
	   if(sogouMarkers.length>0)
	   {
		  
	   if(sogouMarkers[sogoui].getTitle()==sim)
	   {   
	   
		 //找到title相同的，重置位置，跳出循			
		  sogouMarkers[sogoui].setPosition(sougounewLatlng);   
		  var iconstring=getBIcon(direction,bdsta,gsmsta);
		  //sogouMarkers[sogoui].setIcons(null);
		
			var image2 = new sogou.maps.MarkerImage(iconpath+iconstring,
					// 标记图标宽20像素，高32像素
					new sogou.maps.Size(20, 20),
					// 原点在图片左上角，设为(0,0)
					new sogou.maps.Point(0,0),
					// 锚点在小旗的旗杆脚上，相对图标左上角位置为(0,32)
					new sogou.maps.Point(0, 10),
					// 如果是合并的图片，必须设置此项，指定图片大小
					// 如果是一张图片一个图标，此项可缺省。
					new sogou.maps.Size(20, 20));
			
		  //sogouMarkers[sogoui].setIcon(image2);
		  //sogouMarkers[sogoui].setIcon(image1);
		  sogouMarkers[sogoui].setIcons(new Array(image2,image2));
		  
		  if(flagCenter=="1")
			  {
			   sogouMarkers[sogoui].box.open(map,sogouMarkers[sogoui]);
			  }
		  //修改信息窗				 
		  //标记能找到相同的Marker
		  flag_sougousame=true;
		  break;
	   }
	  }
	}
	
	//判断是否找到相同的Marker
	if(!flag_sougousame)
	{  //找不到相同的，要新建一个marker
		var iconstring=getBIcon(direction,bdsta,gsmsta);
		var image = new sogou.maps.MarkerImage(iconpath+iconstring,
	// 标记图标宽20像素，高32像素
	new sogou.maps.Size(20, 20),
	// 原点在图片左上角，设为(0,0)
	new sogou.maps.Point(0,0),
	// 锚点在小旗的旗杆脚上，相对图标左上角位置为(0,32)
	new sogou.maps.Point(0, 10),
	// 如果是合并的图片，必须设置此项，指定图片大小
	// 如果是一张图片一个图标，此项可缺省。
	new sogou.maps.Size(20, 20));	
	//信息窗
var html =
["<div class='infoBoxContent'>",
"<div class='infoBoxlist'><ul><li><table border='0' width='270'><tr><td width='145'><div class='infoBoxleft'></div><div class='infoBoxleft'>SIM卡号:</div><div class='infoBoxleftdata'>" + sim + "</div></td><td><div class='infoBoxleft'>定位类型:</div><div class='infoBoxleftdata'>" + type + "</div></td></tr></table></li>"
, "<li><table border='0' width='270'><tr><td width='145'><div class='infoBoxleft'></div><div class='infoBoxleft'>公网状态:</div><div class='infoBoxleftdata'>" + gsmsta + "</div></td><td><div class='infoBoxleft'>北斗状态:</div><div class='infoBoxleftdata'>" + bdsta + "</div></td></tr></table></li>"
, "<li class='last'><table border='0' width='270'><tr><td width='145'><div class='infoBoxleft'></div><div class='infoBoxleft'>经度:</div><div class='rmb'>" + logitude + "</div></td><td><div class='infoBoxleft'></div><div class='infoBoxleft'>纬度:</div><div class='rmb'>" + latitude + "</div></table></li>"
, "<li class='last'><table border='0' width='270'><tr><td width='145'><div class='infoBoxleft'></div><div class='infoBoxleft'>高程:</div><div class='rmb'>" + altitude + "m</div></td><td><div class='infoBoxleft'></div><div class='infoBoxleft'>方向:</div><div class='rmb'>" + direction + "</div></td></tr></table></li>"
, "<li class='last'><table border='0' width='270'><tr><td width='145'><div class='infoBoxleft'></div><div class='infoBoxleft'>速度:</div><div class='rmb'>" + speed + "km/h</div></td></tr></table></li>"
, "<li class='last'><table border='0' width='270'><tr><td width='145'><div class='infoBoxleft'></div><div class='infoBoxleft'>时间:</div><div class='rmb'>" + time + "</div></td><td></td></tr></table></li>"
, "</ul></div>"
, "</div>"];

//创建标记
function createMarker(flagCenter,map,infowindow,title,position,content)
{
  var  marker = new sogou.maps.Marker({
    position:position,
    map: map,
	//设置信息窗上方标题栏的文字
   // title:sim,
	 disableLabel:false,
	//如果为 true，则可拖动标记。默认值为 false。
	 draggable:false,
	 //设置Label对象的参数。
	//设置成初始即可见，相对于Marker居下对齐，
	//align 取值：TOP|RIGHT|BOTTOM|LEFT ，对应标记上、右、下、左的位置。
	label:{visible:true,align:"RIGHT"}
});
 
 marker.setIcon(image);
 marker.setTitle(title);
// marker.setTitle(sim);
 infowindow.setContent(content);
	infowindow.setTitle("终端卡号：（"+ title+ "）");
sogou.maps.event.addListener(marker, 'click', function() {
	
	infowindow.open(map,marker);
	
});
if(flagCenter=="1")
	{
	infowindow.setContent(content);
	infowindow.setTitle("终端卡号：（"+ title+ "）");
	infowindow.open(map,marker);
	}
marker.box=infowindow;
sogouMarkers.push(marker);
}


   var infowindow = new sogou.maps.InfoWindow();
    createMarker(flagCenter,map,infowindow,sim,new sogou.maps.LatLng(latitude,logitude,true),html[0]+html[1]+html[2]+html[3]+html[4]+html[5]+html[6]+html[7]);
   
	
}
else
{
	//alert("SAME")
	
}
	
}
/*跟踪定位*/
sogouMap.followLoc=function(lng/*定位的经度*/,lat/*定位的纬度*/,title/*标题，用作标记物标记*/,zoom/*地图级数*/,infomation/*信息窗*/){
	  /*如果地图跑出视野范围，点居中*/
	 var newlnglat=new sogou.maps.LatLng(lat,lng);
    if(!BInBounds(lng,lat))
	 {
	   map.setCenter(newlnglat,map.getZoom());	 
	 }
	 //如果点数组长度大于1	 
    if(BlinePois.length>1)
	 { 
	   var flag_same=true;
	   var stitle="";
	   try
	   {
		    stitle=BMarker.getTitle();
			
		}
		catch(e)
		{
			
	    }
		if(stitle=="")
		{
	       //没有Marker
		   //需要新建
	    }
		else
		{
		 BMarker.setTitle(title);
		 BMarker.setPosition(newlnglat);
		 BlinePois[BlinePois.length-1]=newlnglat;
		 BlinePois.push(newlnglat);
		 Bline.setPath(BlinePois);	
	   }
		
    } 	
	 else
	 {
	    //这里是第一次进行追踪	 		
		BMarker=new sogou.maps.Marker({icon:"src/lanse2.gif",label:{visible:true,align:"RIGHT"},title:title,position:newlnglat,map:map});		
		//划线
		BlinePois.push(newlnglat);
		BlinePois.push(newlnglat);		
		 Bline = new sogou.maps.Polyline({
	  //点
     path:BlinePois,
	  //线条颜色
     strokeColor: "#FF0000",
	  //透明度
     strokeOpacity: 0.5,
	  //划线的宽度
     strokeWeight: 1
     });

     Bline.setMap(map);
	 }	 
	
}
/*轨迹回放*/
sogouMap.locReplay=function(poindata,zoom,infostring/*可能用于信息窗或者title的信息*/ ){
	//提取点
    var sogouarrallpoint = poindata.split( ";" );
    var sogouarrPois = new Array();
    var sInfoarray=new Array();
    for ( var i = 0; i < sogouarrallpoint.length; i++ )
    {
        var arrpos = sogouarrallpoint[i].split( "," );
        var logitude = parseFloat( arrpos[0] );
        var latitude = parseFloat( arrpos[1] );
        var infoarray=new Array();
        infoarray[0]=arrpos[2];//定位类型
        infoarray[1]=arrpos[3];//GSM状态
		infoarray[2]=arrpos[4];//北斗状态
		infoarray[3]=arrpos[5];//速度			
		infoarray[4]=arrpos[6];//高度
		infoarray[5]=arrpos[7];//方向
		infoarray[6]=arrpos[8];//时间
		infoarray[7]=arrpos[9];//图标
        sogouarrPois[i]= new sogou.maps.LatLng(latitude,logitude, true );
        sInfoarray[i]=infoarray;
    }
	
	var startmarker=new sogou.maps.Marker(
	{
	position:sogouarrPois[0],
    map: map,
	//设置信息窗上方标题栏的文字
    icon:"src/start.png",
	title:"起点" ,
    label:{visible:true,align:"RIGHT"}
	});
	
		var endmarker=new sogou.maps.Marker(
	{
	position:sogouarrPois[sogouarrPois.length-1],
    map: map,
	//设置信息窗上方标题栏的文字
    icon:"src/end.png",
	title:"终点" ,
    label:{visible:true,align:"RIGHT"}
	});		
 var logotitle=infostring;	  
   //划线
  var flightPath = new sogou.maps.Polyline({
 //点
  path:sogouarrPois,
  //线条颜色
  strokeColor: "#FF0000",
  //透明度
  strokeOpacity: 0.5,
  //划线的宽度
  strokeWeight:0.5
  });

  flightPath.setMap(map);
  map.setCenter(sogouarrPois[0]);
   //新建一个marker
  
   var iconstring=getBIcon(sInfoarray[0][5],sInfoarray[0][2],sInfoarray[0][1]);
   var image = new sogou.maps.MarkerImage(sInfoarray[0][7]+iconstring,
	// 标记图标宽20像素，高32像素
	new sogou.maps.Size(20, 20),
	// 原点在图片左上角，设为(0,0)
	new sogou.maps.Point(0,0),
	// 锚点在小旗的旗杆脚上，相对图标左上角位置为(0,32)
	new sogou.maps.Point(10, 10),
	// 如果是合并的图片，必须设置此项，指定图片大小
	// 如果是一张图片一个图标，此项可缺省。
	new sogou.maps.Size(20, 20));
	
     var  sogourunmarker = new sogou.maps.Marker({
    position:sogouarrPois[0],
    map: map,
	//设置信息窗上方标题栏的文字
    icon:image,
	title:logotitle ,
    label:{visible:true,align:"RIGHT"}
	    
	 });

   var runsign=0;
    /////sogouRuncar函数Begin//////
   function SogouRunCar()
   { 
    
	 if(runsign>=sogouarrPois.length)
	  {
		  //如果大于，就不执行
		  
	 }
     else
	{
	  //如果小于，则继续执行
	  sogourunmarker.setPosition(sogouarrPois[runsign]);
	  var iconpath=sInfoarray[runsign][7]+getBIcon(sInfoarray[runsign][5],sInfoarray[runsign][2],sInfoarray[runsign][1]);
	  sogourunmarker.setIcon(iconpath);	
	  runsign++;
	  setTimeout(SogouRunCar,1000,null);
	 // alert("Hello Boy");	  
	}
  }
  /////sogouRuncar函数end//////	
 SogouRunCar();	
	
}
/*清空地区*/
sogouMap.clearall=function(){
	  map.clearAll();	
	  BlinePois.length=0;
	  Bline=null;
	
}
/*启动设置围栏*/
sogouMap.setFence=function(backcallfunction/*回调函数*/){}
/*获取围栏String*/
sogouMap.getFence=function(){}
/*修改围栏*/
sogouMap.modifyFence=function(opts/*围栏点数组*/,backcallfunction){}
/*推送进数组*/
sogouMap.type="sogou";
maps.push(sogouMap);
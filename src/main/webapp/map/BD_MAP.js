/*全局变量*/
/*地图，共用*/
var map;
/*地图类型，共用*/
var MAPTYPE='baidu';
/*信息窗数组 百度地图使用*/
var infoBoxs = new Array();
var maps=new Array();
/*天地图使用*/
var markers;
var layer;
/*标记google轨迹回放的标记物*/
var gOverlays=new Array();
/*用来存放Markers的数组，目前支持搜狗和谷歌使用*/
var sogouMarkers=new Array();
 /*跟踪定位点数组,共用*/
var BlinePois=new Array();
  /*跟踪定位线，共用*/
var Bline;
  //跟踪定位的Marker
 var BFMarker;
 var BFInfo;/*定位跟踪的Infobox*/ 
 var bfopenflag=true; /*标记百度跟踪定位信息窗是否打开*/
//轨迹回放接口
function locreplay(poindata,zoom,infostring){//轨迹生成
   for(var mapi=0;mapi<maps.length;mapi++){
     if(maps[mapi].type==MAPTYPE){
       maps[mapi].locReplay(poindata,zoom,infostring);
       break;
     }
   }
}
//定位居中
function TakeCenter( longtitude,latitude){
   for(var mapi=0;mapi<maps.length;mapi++){
     if(maps[mapi].type==MAPTYPE){
       maps[mapi].TakeCenter(longtitude,latitude);
       break;
     }
   }
}
var BaiDuarr=new Array(new Array(),new Array());
//实时定位接口开始
function addnewterloc(flagCenter,flagAlarm/*string类型，报警标志,报警定位:1,其他:0*/,
			type/*定位类型,string表示,GPS/RD/RN*/, gsmsta/*string表示，GSM状态*/, bdsta/*string表示，北斗状态*/
			, logitude/*string表示，经度*/, latitude/*string表示，纬度*/, altitude/*string表示，高程*/, speed/*string表示，速度*/,
			direction/*string表示，方向*/, time/*string表示，定位时间*/, sim/*string表示，卡号*/, iconpath/*string表示，显示图标路径*/){
 for(var mapi=0;mapi<maps.length;mapi++){
     if(maps[mapi].type==MAPTYPE){
       maps[mapi].TerLoc(flagCenter,flagAlarm/*string类型，报警标志,报警定位:1,其他:0*/,
			type/*定位类型,string表示,GPS/RD/RN*/, gsmsta/*string表示，GSM状态*/, bdsta/*string表示，北斗状态*/
			, logitude/*string表示，经度*/, latitude/*string表示，纬度*/, altitude/*string表示，高程*/, speed/*string表示，速度*/,
			getDirection(direction)/*string表示，方向*/, time/*string表示，定位时间*/, sim/*string表示，卡号*/, iconpath/*string表示，显示图标路径*/);
       break;
     }
   }
}
//改变地图大小
function Changemap( newwidth, newhight ){
    var getdiv = document.getElementById( "map_canvas" );
    getdiv.style.width = newwidth + "px";
    getdiv.style.height = newhight + "px";
}

//初始化接口
/*初始化应该做的工作是动态导入JS*/
function InitMapType(maptype ,from){  
  for(var mapi=0;mapi<maps.length;mapi++){
     if(maps[mapi].type==maptype){
       maps[mapi].Init(from);
       break;
     }
   }
}

function getType(type) {
	switch (type) {
	  case 1:
		  return "RD";
	  case 2:
		  return "RN";
	  case 3:
		  return "GPS";
	  default:
		  return "";
	}
}

function getStatus(status) {
	switch (status) {
	  case 1:
		  return "在线";
	  case 2:
		  return "离线";
	  default:
		  return "";
	}
}

function getDirection(direction) {
	if (direction>=270+22.5 || direction<=22.5){ 
		return "正北";
	}else if (direction>=22.5 && direction<=45+22.5){
		return "东北";
	}else if (direction>=45+22.5 && direction<=90+22.5){
		return "正东";
	}else if (direction>=90+22.5 && direction<=180-22.5){
		return "东南";
	}else if (direction>=180-22.5 && direction<=180+22.5){
		return "正南";
	}else if (direction>=180+22.5 && direction<=225){
		return "西南";
	}else if (direction>=225 && direction<=270-22.5){
		return "正西";
	}else if (direction>=270-22.5 && direction<=270+22.5){
		return "西北";
	}else{
		return "";
	}
}

function getIconPath(gsmsta,bdsta){
	if(gsmsta=="1"&&bdsta=="1"){
		return "green.png";
	}
	if(gsmsta=="2"&&bdsta=="1"){
		return "blue.png";
	}
	if(gsmsta=="2"&&bdsta=="2"){
		return "red.png";
	}
	if(gsmsta=="1"&&bdsta=="2"){
		return "orange.png";
	}
	return "orange.png";
}
  
  
/*清除地图上所有标记*/
function BClearall(){
  for(var mapi=0;mapi<maps.length;mapi++){
     if(maps[mapi].type==MAPTYPE){
       maps[mapi].clearall();
       break;
     }
  }
}

/*跟踪定位*/
function BFollowLoate(lng/*定位的经度*/,lat/*定位的纬度*/,title/*标题，用作标记物标记*/,zoom/*地图级数*/,infomation/*信息窗*/){  
  for(var mapi=0;mapi<maps.length;mapi++){
     if(maps[mapi].type==MAPTYPE){
       maps[mapi].followLoc(lng/*定位的经度*/,lat/*定位的纬度*/,title/*标题，用作标记物标记*/,zoom/*地图级数*/,infomation/*信息窗*/);
       break;
     }
  }
}
  
/*判断点是否在地图视野上*/
function BInBounds(blng,blat){   
 /*for(var mapi=0;mapi<maps.length;mapi++){
     if(maps[mapi].type==MAPTYPE){
       maps[mapi].clearall();
       break;
     }
   }*/
}
  
//根据方向、北斗状态、公网状态来获取图标
function getBIcon(direction_1/*运动方向*/,bdstate_1/*北斗状态*/,gsmstate_1/*公网状态*/){  
	 var iconstring=geticon(direction_1/*运动方向*/,bdstate_1/*北斗状态*/,gsmstate_1/*公网状态*/);
	 if(iconstring!=null){
	    return iconstring;	 
	 }
	 else{
		 return "lanse2.gif";
	 }
}

//1.direction 运动方向;2.bdstate 北斗状态;3.gsmstate 公网状态;
function geticon(direction,bdstate,gsmstate){
	//TODO
	switch (direction) {
	  case "东北":
	     if(bdstate=="在线"){
		 	if(gsmstate=="在线"){
				return "greenne.png";
			}else if(gsmstate=="离线"){
				return "bluene.png";
			}
	     }else if(bdstate=="离线"){
			if(gsmstate=="在线"){
				return "orne.png";
			}else if(gsmstate=="离线"){
				return "out.png";
			}
	     }
	  case "正东":
		 if(bdstate=="在线"){
		    if(gsmstate=="在线"){
				return "greene.png";
		    }else if(gsmstate=="离线"){
				return "bluee.png";
		    }
		 }else if(bdstate=="离线"){
		    if(gsmstate=="在线"){
				return "ore.png";
		    }else if(gsmstate=="离线"){
				return "out.png";
		    }
		 }
	  case "东南":
		 if(bdstate=="在线"){
		 	if(gsmstate=="在线"){
				return "greense.png";
			}else if(gsmstate=="离线"){
				return "bluese.png";
			}
		 }else if(bdstate=="离线"){
			if(gsmstate=="在线"){
				return "orse.png";
			}else if(gsmstate=="离线"){
				return "out.png";
			}
		 }
	  case "正南":
		 if(bdstate=="在线"){
		 	if(gsmstate=="在线"){
				return "greens.png";
			}else if(gsmstate=="离线"){
				return "blues.png";
			}
		 }else if(bdstate=="离线"){
			if(gsmstate=="在线"){
				return "ors.png";
			}else if(gsmstate=="离线"){
				return "out.png";
			}
		 }
	  case "西南":
		 if(bdstate=="在线"){
		 	if(gsmstate=="在线"){
				return "greensw.png";
			}else if(gsmstate=="离线"){
				return "bluesw.png";
			}
		 }else if(bdstate=="离线"){
			if(gsmstate=="在线"){
				return "orsw.png";
			}else if(gsmstate=="离线"){
				return "out.png";
			}
		 }
	  case "正西":
		 if(bdstate=="在线"){
		 	if(gsmstate=="在线"){
				return "greenw.png";
			}else if(gsmstate=="离线"){
				return "bluew.png";
			}
		 }else if(bdstate=="离线"){
			if(gsmstate=="在线"){
				return "orw.png";
			}else if(gsmstate=="离线"){
				return "out.png";
			}
		 }
	  case "西北":
		 if(bdstate=="在线"){
		 	if(gsmstate=="在线"){
				return "greennw.png";
			}else if(gsmstate=="离线"){
				return "bluenw.png";
			}
		 }else if(bdstate=="离线"){
			if(gsmstate=="在线"){
				return "ornw.png";
			}else if(gsmstate=="离线"){
				return "out.png";
			}
		 }
	  case "正北":
		 if(bdstate=="在线"){
		 	if(gsmstate=="在线"){
				return "greenn.png";
			}else if(gsmstate=="离线"){
				return "bluen.png";
			}
		 }else if(bdstate=="离线"){
			if(gsmstate=="在线"){
				return "orn.png";
			}else if(gsmstate=="离线"){
				return "out.png";
			}
		 }
	}
}
	 
/*显示点和围栏*/
function DisplayFence(plat/*目标点纬度*/,plng/*目标点经度*/,opts/*围栏点数组*/){
	
}

/*在地图上设定围栏，返回值为用字符串表示的围栏数组*/
function setFence(backcallfunction/*回调函数*/){
	
}
/*修改围栏，返回值为用字符串表示的围栏数组*/
function ModifyFence(opts/*围栏点数组*/,backcallfunction){
	
}
/*获取围栏*/
/*返回参数实例‘113.23421,23.10903;113.32409,23.234222;113.45232,23.334521’*/
function getFence(){
	
}
//
function GBMarker(map,options)
{
  
 
   var myOptions = 

            {
			 content: options.content
			,disableAutoPan: false
            ,maxWidth: 0
			,pixelOffset: new google.maps.Size(-120, -145)
            ,zIndex: null
			,boxStyle: { 
			  background: "url('tipbox.gif') no-repeat"
			 ,opacity: 1.0
			 ,width: "250px"
			 }
			,closeBoxMargin: "1px 0px 3px 3px"
			,closeBoxURL: options.iconpath+"close.png"
			,infoBoxClearance: new google.maps.Size(1, 1)
			,isHidden: false
			,pane: "floatPane"
			,enableEventPropagation: false
		};

       // var ginfowindow=new GInfoBox(myOptions);
    var ginfobox_=new GInfoBox(myOptions);
    var gmarker_=new MyMarker(map,{image:options.image,latlng:options.latlng,labelText:options.labelText,title:options.title,clickFun:function(){ginfobox_.open(map,gmarker_)} });
	this.gmarker=gmarker_;
	this.infobox=ginfobox_;
	this.mapp=map;
	
}
GBMarker.prototype.setPosition=function(latlng){
this.gmarker.setPosition(latlng);
 if(this.infobox.closeListener_)
 {
   this.infobox.setPosition(latlng);
 }
}
GBMarker.prototype.getPosition=function(){return this.gmarker.getPosition()}
GBMarker.prototype.setTitle=function(title){this.gmarker.setTitle(title);}
GBMarker.prototype.getTitle=function(){return this.gmarker.getTitle()}
GBMarker.prototype.getContent=function(){return this.infobox.getContent();}
GBMarker.prototype.setContent=function(content){this.infobox.setContent(content)}
GBMarker.prototype.setIcon=function(icon){this.gmarker.setIcon(icon)}
GBMarker.prototype.getIcon=function(){return this.gmarker_.getIcon()}
GBMarker.prototype.openInfo=function(){this.infobox.open(this.mapp, this.gmarker)}
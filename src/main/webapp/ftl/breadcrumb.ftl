


<!-- start: 左侧栏 -->
			<div id="sidebar-left" class="span2" >
			<div class="nav-collapse sidebar-nav">
			<div class="panel">
				<div class="panel-heading mout" role="tab" style="">
					<i class="icon-list-alt"></i><span class="hidden-tablet"> 销售</span>
				</div>
				<div class="panel-collapse collapse" style="background: #fff;">
					<div class="panel-body">
						<ul>
							<li><a href="table_order.html"><span class="hidden-tablet"> 购买订单管理</span></a></li>
							<li><a href="table_rentOrder.html"><span class="hidden-tablet"> 租用订单管理</span></a></li>
							<li><a href="table_returns.html"><span class="hidden-tablet"> 退货管理</span></a></li>
							<li><a href="table_rentReturns.html"><span class="hidden-tablet"> 租用归还管理</span></a></li>
							<li><a href="table_bdCood.html"><span class="hidden-tablet"> 北斗码管理</span></a></li>    
							<li><a href="table_TheSystemAudit.html"><span class="hidden-tablet"> 实名认证审核</span></a></li> 
							<li><a href="table_ticketCheck.html"><span class="hidden-tablet"> 增票资质审核</span></a></li>
							<li><a href="table_UserRegistration.html"><span class="hidden-tablet"> 用户登记信息</span></a></li>  						 
							<li><a href="table_apply.html"><span class="hidden-tablet"> 预约管理</span></a></li>						
							<li><a href="table_product.html"><span class="hidden-tablet"> 产品管理</span></a></li>
							<li><a href="table_activity.html"><span class="hidden-tablet"> 产品活动管理</span></a></li>
							<li><a href="table_recipients.html"><span class="hidden-tablet"> 寄件人信息管理</span></a></li>
							<li><a href="table_Notification.html"><span class="hidden-tablet"> 管理员通知管理</span></a></li> 
							<li><a href="table_customer.html"><span class="hidden-tablet"> 客户通知管理</span></a></li>
							<li><a href="table_messageRecord.html"><span class="hidden-tablet"> 客户短信通知管理</span></a></li>
							<li><a href="table_FAQ.html"><span class="hidden-tablet">  FAQ管理</span></a></li>
 						</ul>
					</div>
				</div>
			</div>
			<div class="panel">
				<div class="panel-heading mout" role="tab" style="">
					<i class="icon-list-alt"></i><span class="hidden-tablet"> 运维</span>
				</div>
				<div class="panel-collapse collapse" style="background: #fff;">
					<div class="panel-body">
						<ul>
							<li><a href="table_message.html"><span class="hidden-tablet"> 消息管理</span></a></li> 
							<li><a href="table_user.html"><span class="hidden-tablet"> 用户中心</span></a></li>
							<li><a href="table_entUser.html"><span class="hidden-tablet">企业用户管理</span></a></li>
							<li><a href="table_entsendmsg.html"><span class="hidden-tablet">企业发送消息记录</span></a></li>
							<li><a href="table_announcement.html"><span class="hidden-tablet">  公告管理</span></a></li>
							<li><a href="table_box.html"><span class="hidden-tablet"> 盒子管理</span></a></li> 
							<li><a href="table_location.html"><span class="hidden-tablet"> 位置管理</span></a></li>
							<li><a href="boxStorage.html"><span class="hidden-tablet"> 盒子数据初始化</span></a></li>
							<li><a href="table_suggestion.html"><span class="hidden-tablet"> 意见反馈</span></a></li>
							<li><a href="table_SdkUserSend.html"><span class="hidden-tablet"> Sdk用户下发</span></a></li>
 							<li><a href="table_cardlocreplay.html"><span class="hidden-tablet">  轨迹回放</span></a></li>
							<li><a href="table_phoneRestrict.html"><span class="hidden-tablet">  家人号码限制管理</span></a></li>
							<li><a href="table_pushLocation_record.html"><span class="hidden-tablet"> 推送位置记录</span></a></li>
							<li><a href="table_pushMessage_record.html"><span class="hidden-tablet"> 推送消息记录</span></a></li>
							<!--<li><a href="test.html"><span class="hidden-tablet"> 测试</span></a></li>-->
						</ul>
					</div>
				</div>
			</div>
			
			<div class="panel">
				<div class="panel-heading mout" role="tab" style="">
					<i class="icon-list-alt"></i><span class="hidden-tablet"> 统计</span>
				</div>
				<div class="panel-collapse collapse" style="background: #fff;">
					<div class="panel-body">
						<ul>
							<li><a href="table_bdCoodUser.html"><span class="hidden-tablet">  推荐人信息统计</span></a></li>
							<li><a href="table_userreglog.html"><span class="hidden-tablet"> 用户注册记录</span></a></li>
							<li><a href="statistic_userreg.html"><span class="hidden-tablet"> 统计注册数量</span></a></li>
							<li><a href="statistic_usermsg.html"><span class="hidden-tablet"> 统计用户消息</span></a></li>
							<li><a href="table_fileDownloadLog.html"><span class="hidden-tablet"> 文件下载记录</span></a></li>
							<li><a href="statistic_fileDownloadLog.html"><span class="hidden-tablet"> 统计下载数量</span></a></li>
							<li><a href="statistic_entWentData.html"><span class="hidden-tablet"> 企业周统计</span></a></li>
							<li><a href="statistics_market.html"><span class="hidden-tablet"> 销售统计</span></a></li>
						</ul>
					</div>
				</div>
			</div>
				
				<!--	<ul class="nav nav-tabs nav-stacked main-menu">
						<li><a href="table_order.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 购买订单管理</span></a></li>
						<li><a href="table_rentOrder.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 租用订单管理</span></a></li>
						<li><a href="table_returns.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 退货管理</span></a></li>
						<li><a href="table_rentReturns.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 租用归还管理</span></a></li>
						<li><a href="table_bdCood.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 北斗码管理</span></a></li>  
						
						<li><a href="table_announcement.html"><i class="icon-list-alt"></i><span class="hidden-tablet">  公告管理</span></a></li>
						<li><a href="table_FAQ.html"><i class="icon-list-alt"></i><span class="hidden-tablet">  FAQ管理</span></a></li> 
						
						<li><a href="table_bdCoodUser.html"><i class="icon-list-alt"></i><span class="hidden-tablet">  推荐人信息统计</span></a></li>  
						
						<li><a href="table_TheSystemAudit.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 实名认证审核</span></a></li> 
						<li><a href="table_ticketCheck.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 增票资质审核</span></a></li>
						
						<li><a href="table_UserRegistration.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 用户登记信息</span></a></li>  						 
						<li><a href="table_apply.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 预约管理</span></a></li>						
						<li><a href="table_product.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 产品管理</span></a></li>
						<li><a href="table_activity.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 产品活动管理</span></a></li>
						
						<li><a href="table_message.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 消息管理</span></a></li> 
						<li><a href="table_user.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 用户中心</span></a></li>
						
						<li><a href="table_userreglog.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 用户注册记录</span></a></li>
						<li><a href="statistic_userreg.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 统计注册数量</span></a></li>
						<li><a href="statistic_usermsg.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 统计用户消息</span></a></li>
						<li><a href="table_fileDownloadLog.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 文件下载记录</span></a></li>
						<li><a href="statistic_fileDownloadLog.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 统计下载数量</span></a></li>
						
						<li><a href="table_box.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 盒子管理</span></a></li> 
						<li><a href="table_location.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 位置管理</span></a></li>
						<li><a href="boxStorage.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 盒子数据初始化</span></a></li>
						<li><a href="table_suggestion.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> 意见反馈</span></a></li>
						<li><a href="table_SdkUserSend.html"><i class="icon-list-alt"></i><span class="hidden-tablet"> Sdk用户下发</span></a></li>  
						 
						
 						
					</ul>-->
				</div>
			</div>
<!-- end: 左侧栏 -->

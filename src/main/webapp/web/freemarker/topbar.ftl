<!-- topbar starts -->
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="brand" href="#"><i class="icon-flag"></i> 疯向标</a>
      <ul class="nav">
        <li><a href="#whats-new">首页</a></li>
        <li class="dropdown">
          <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">活动管理 <i class="icon-caret-down"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li class="nav-header">活动</li>
            <li><a href="${ctx}/web/activity-query"><i class="icon-edit"></i> 活动查询</a></li>
            <li><a href="${ctx}/web/activity-edit"><i class="icon-save"></i> 添加新活动</a></li>
          </ul>
        </li>
         <li><a href="${ctx}/web/activity-allentryformquery">最新报名信息</a></li>
         <li class="dropdown">
          <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">优惠券管理 <i class="icon-caret-down"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="${ctx}/web/coupon-query"><i class="icon-edit"></i> 优惠券查询</a></li>
            <li><a href="${ctx}/web/coupon-edit"><i class="icon-save"></i> 添加新优惠券</a></li>
          </ul>
        </li>
                 <li class="dropdown">
          <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">外卖信息管理 <i class="icon-caret-down"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="${ctx}/web/takeout-query"><i class="icon-edit"></i> 外卖信息查询</a></li>
            <li><a href="${ctx}/web/takeout-edit"><i class="icon-save"></i> 添加新的外卖信息</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">数据统计 <i class="icon-caret-down"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#icons-new">(建设中...)</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">客户端管理 <i class="icon-caret-down"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#icons-new">(建设中...)</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">系统配置 <i class="icon-caret-down"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#icons-new">(建设中...)</a></li>
          </ul>
        </li>
        <li><a href="#license">版权协议</a></li>
      </ul>
      <ul class="nav pull-right">
         <li class="dropdown">
          <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-envelope"></i> ${(user.username)!} <i class="icon-caret-down"></i></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="${ctx}/web/user-modPwd"><i class="icon-save"></i> 修改密码</a></li>
            <li><a href="#"><i class="icon-save"></i> 个人信息</a></li>
           	<li class="divider"></li>
            <li><a href="javascript:;" onclick="logout()"><i class="icon-play"></i> 退出</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</div>
	<!-- topbar ends -->
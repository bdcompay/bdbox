<link href="${ctx}/source/css/bootstrap.min.css" rel="stylesheet">
<link href="${ctx}/source/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="${ctx}/source/css/font-awesome.min.css" rel="stylesheet">
<link href="${ctx}/source/css/common.css" rel="stylesheet">
<link type="text/css" rel="stylesheet" href="${ctx}/source/css/bootstrap-datetimepicker.min.css">
<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- The fav icon -->
<link rel="shortcut icon" href="img/favicon.ico">
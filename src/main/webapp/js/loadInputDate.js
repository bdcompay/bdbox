$(document).ready(function(){
	//加载日期框日期
	loadInputDate();
    $('.start').focus(function(e){
	   WdatePicker({dateFmt:"yyyy-MM-dd HH:mm:ss",maxDate:'%y-%M-%d'});
    });
    $('.end').focus(function(){
	   WdatePicker({dateFmt:"yyyy-MM-dd HH:mm:ss",minDate:$('.start').val(),maxDate:'%y-%M-{%d+1}'});
    });
});
//加载日期框日期
function loadInputDate(){
	var $start=$('.start');
	var $end=$('.end');
	if($start.val()==''){$start.val(todayToDateStr());}
	if($end.val()==''){$end.val(tomorrowToDateStr());}
}
/**
 * 返回明天的日期转换成字符串形式
 */
function tomorrowToDateStr() {
	var dt=new Date().getTime()+24*60*60*1000;
	var tdt=new Date(dt);
	var str=dateToStr(tdt);
	return str;
}
/**
 * 返回当前日期的字符串形式
 */
function todayToDateStr() {
	var dt= new Date();
	var str=dateToStr(dt);
	return str;
}
/**
 * 日期格式转换成字符串格式
 */
function dateToStr(getInputDate) {
	//获得输入的年
	var getInputYear=getInputDate.getUTCFullYear();
	//获得输入的月
	var getInputMonth=getInputDate.getMonth()+1;
	//获得输入的天
	var getInputDay=getInputDate.getDate();
	if (getInputYear<1000) {
		getInputYear=1900+getInputYear;
	}
	//当月份小于10时，月份前面补零
	if(getInputMonth<10){
		getInputMonth="0"+getInputMonth;
	}
	//当日期小于10时，日期前面补零
	if(getInputDay<10){
		getInputDay="0"+getInputDay;
	}
	var dtStr=getInputYear+"-"+getInputMonth+"-"+getInputDay+" 00:00:00";
	return dtStr;
}
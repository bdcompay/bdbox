﻿(function() {//闭包
	function load_script(xyUrl, callback) {
		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = xyUrl;
		//借鉴了jQuery的script跨域方法
		script.onload = script.onreadystatechange = function() {
			if ((!this.readyState || this.readyState === "loaded" || this.readyState === "complete")) {
				callback && callback();
				script.onload = script.onreadystatechange = null;
				if (head && script.parentNode) {
					head.removeChild(script);
				}
			}
		};
		head.insertBefore(script, head.firstChild);
	}

	function translate(point, type, i, title, callback) {
		var callbackName = 'cbk_' + Math.round(Math.random() * 10000);//随机函数名
		var xyUrl = "http://api.map.baidu.com/ag/coord/convert?from=" + type
				+ "&to=4&x=" + point.lng + "&y=" + point.lat
				+ "&callback=BMap.Convertor." + callbackName;
		//动态创建script标签
		load_script(xyUrl);
		BMap.Convertor[callbackName] = function(xyResult) {
			delete BMap.Convertor[callbackName];//调用完需要删除改函数
			var point = new BMap.Point(xyResult.x, xyResult.y);
			callback && callback(point, i, title);
		};
	}

	function Btranslate(point, data, type, callback) {
		var callbackName = 'cbk_' + Math.round(Math.random() * 10000); //随机函数名
		var xyUrl = "http://api.map.baidu.com/ag/coord/convert?from=" + type
				+ "&to=4&x=" + point.lng + "&y=" + point.lat
				+ "&callback=BMap.Convertor." + callbackName;
		//动态创建script标签
		load_script(xyUrl);
		BMap.Convertor[callbackName] = function(xyResult) {
			delete BMap.Convertor[callbackName]; //调用完需要删除改函数
			// var opoint =point;
			var point2 = new BMap.Point(xyResult.x, xyResult.y);
			callback && callback(point2, data);
		};
	}
	function ERPtranslate(point, type, csgTerminalstatus, callback) {
		var callbackName = 'cbk_' + Math.round(Math.random() * 10000); //随机函数名
		var xyUrl = "http://api.map.baidu.com/ag/coord/convert?from=" + type
				+ "&to=4&x=" + point.lng + "&y=" + point.lat
				+ "&callback=BMap.Convertor." + callbackName;
		//动态创建script标签
		load_script(xyUrl);
		BMap.Convertor[callbackName] = function(xyResult) {
			delete BMap.Convertor[callbackName]; //调用完需要删除改函数
			var point2 = new BMap.Point(xyResult.x, xyResult.y);
			callback && callback(point2,csgTerminalstatus);
		};
	}

	function ReplayGrouptranslate(pointsdata, title, zoom, callback) {

		/*pointsdata*/
		var type = 0;
		var baiduarrallpoint = pointsdata.split(";");
		var baiduarrPois = new Array();
		var InfoArray = new Array();
		/*拆分*/
		for (var i = 0; i < baiduarrallpoint.length; i++) {
			if (baiduarrallpoint[i] == "" || baiduarrallpoint[i].length < 10) {
				continue;
			}
			var arrpos = baiduarrallpoint[i].split(",");
			var logitude = parseFloat(arrpos[0]);
			var latitude = parseFloat(arrpos[1]);
			var infoarray = new Array();
			infoarray[0] = arrpos[2];//定位类型
			infoarray[1] = arrpos[3];//GSM状态
			infoarray[2] = arrpos[4];//北斗状态
			infoarray[3] = arrpos[5];//速度			
			infoarray[4] = arrpos[6];//高度
			infoarray[5] = arrpos[7];//方向
			infoarray[6] = arrpos[8];//时间
			infoarray[7] = arrpos[9];//图标
			infoarray[8] = arrpos[0];//经度
			infoarray[9] = arrpos[1];//纬度			
			baiduarrPois.push(new BMap.Point(logitude, latitude));
			InfoArray[i] = infoarray;
		}

		var allcount = 0;

		function getindex(brpoint, index) {
			var callbackName = 'cbk_' + Math.round(Math.random() * 100000); //随机函数名
			var xyUrl = "http://api.map.baidu.com/ag/coord/convert?from="
					+ type + "&to=4&x=" + brpoint.lng + "&y=" + brpoint.lat
					+ "&callback=BMap.Convertor." + callbackName;
			//动态创建script标签
			load_script(xyUrl);
			BMap.Convertor[callbackName] = function(xyResult) {
				delete BMap.Convertor[callbackName]; //调用完需要删除改函数

				var point2 = new BMap.Point(xyResult.x, xyResult.y);
				baiduarrPois[index] = point2;
				allcount++;
				if (allcount == baiduarrPois.length) {
					/*完成最后一个点转换，就执行回调函数*/
					//alert(InfoArray[index][6]);
					var datastring = "";
					for (var j = 0; j < baiduarrPois.length; j++) {
						datastring += baiduarrPois[j].lng + ","
								+ baiduarrPois[j].lat + "," + InfoArray[j][0]
								+ "," + InfoArray[j][1] + "," + InfoArray[j][2]
								+ "," + InfoArray[j][3] + "," + InfoArray[j][4]
								+ "," + InfoArray[j][5] + "," + InfoArray[j][6]
								+ "," + InfoArray[j][7] + "," + InfoArray[j][8]
								+ "," + InfoArray[j][9] + ";";
					}
					callback && callback(datastring, zoom, title);

				}
			};

		}

		for (var k = 0; k < baiduarrPois.length; k++) {

			getindex(baiduarrPois[k], k);

		}
	}

	window.BMap = window.BMap || {};
	BMap.Convertor = {};
	BMap.Convertor.translate = translate;
	BMap.Convertor.Btranslate = Btranslate;
	BMap.Convertor.ERPtranslate = ERPtranslate;
	BMap.Convertor.ReplayGrouptranslate = ReplayGrouptranslate;
})();